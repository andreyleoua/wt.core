<?php

use Wt\Core\App\App;
use Wt\Core\App\ModuleEntity;
use Wt\Core\Facade\DefaultBitrixFacade;
use Wt\Core\Facade\FactoryFacade;
use Wt\Core\Facade\DefaultFactoryFacade;
use Wt\Core\Facade\ServiceFacade;
use Wt\Core\Facade\DefaultServiceFacade;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Lexer;
use Wt\Core\Support\LexerCollection as LexerCollection;
use Wt\Core\Support\Logger;

/**
 * @param null $serviceName
 * @return App
 * @throws null
 */
function app($serviceName = null){
    //$args = func_get_args();
    //$mixedService = array_shift($args);
    $app = App::getInstance('wt.core');

    if(!$serviceName){
        return $app;
    }

    $factoryMode = str_starts_with($serviceName, 'factory:');
    /** @var Lexer $lexer */
    $lexer = $app->container()->make('appLexer');
    if( $factoryMode ){
        return $lexer->callNextStr($serviceName, $app);
    }

    return $lexer->callNextStr($serviceName, $app->service());
}

/**
 * @return Logger
 * @throws null
 */
function logger(){
    return resolve(Logger::class);
}

/**
 * @param $strClass
 * @param array $args
 * @return mixed
 * @throws Exception
 */
function resolve($strClass, $args = []){
    if( !$args ){
        return app()->service($strClass);
    }
    
    return app()->service($strClass, $args);
}

/**
 * @return FactoryFacade|DefaultFactoryFacade
 */
function factory(){
    return app()->factory();
}

/**
 * @return ServiceFacade|DefaultServiceFacade
 */
function service(){
    return app()->service();
}

/**
 * @param array $arData
 * @return Collection
 * @throws null
 */
function collect($arData = []){
	return resolve(Collection::class, [$arData]);
}

/**
 * @param string $config
 * @param mixed $default
 * @return LexerCollection
 */
function config($config = null, $default = []){
    return app()->config($config, $default);
}

function container($containerName = ''){
    if( $containerName ){
        return app()->container()->make($containerName);
    }

    return app()->container();
}

/**
 * @return DefaultBitrixFacade
 */
function bitrix(){
    return app()->service()->bitrix();
}

if(!function_exists('__getModuleEntity')){
    /**
     * @param $path
     * @return ModuleEntity
     */
    function __getModuleEntity($path){
        static $autoloadDirList = [];

        $module = (array)json_decode(file_get_contents($path.'/.module.json'), true);
        $module['dir'] = $path;

        if(!class_exists('Bitrix\Main\Application', FALSE)){
            if(!in_array($module['dir'], $autoloadDirList)){
                $autoloadDirList[] = $module['dir'];
                spl_autoload_register(function ($className) use ($module) {
                    return __kitModuleLoader($className, $module['namespace'], $module['dir']);
                });
            }
        }

        return new ModuleEntity($module);
    }
}

if(!function_exists('__kitModuleLoader')){
    function __kitModuleLoader($className, $namespace, $path) {

        if ((class_exists($className,FALSE)) || (stripos($className, $namespace) !== 0)) {
            //    Either already loaded, or not a Core class request
            return FALSE;
        }
        $pClassBxFilePath = $path . DIRECTORY_SEPARATOR . 'lib' .
            str_replace('\\',DIRECTORY_SEPARATOR,str_ireplace($namespace, '', strtolower($className))) .
            '.php';
        if (file_exists($pClassBxFilePath) && is_readable($pClassBxFilePath)) {
            require($pClassBxFilePath);
            return null;
        }

        $pClassFilePath = $path . DIRECTORY_SEPARATOR . 'lib' .
            str_replace('\\',DIRECTORY_SEPARATOR,str_ireplace($namespace, '', $className)) .
            '.php';
        if (file_exists($pClassFilePath) && is_readable($pClassFilePath)) {
            require($pClassFilePath);
            return null;
        }

        return FALSE;
    }
}