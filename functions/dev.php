<?php
if (!function_exists('dd')) {
    function dd()
    {
        $args = func_get_args();
        foreach ($args as $data) {
            p($data, false);
        }
        die;
    }
}
if (!function_exists('p')) {
    function p($obj, $admOnly = true, $d = false)
    {
        $printCallback = function ($str){
            $t = print_r($str, true);
            $t = htmlspecialchars($t);
            if(is_array($str) || is_object($str)){
                $t = preg_replace("/([\w\\\\]+) Object\n/si", "<span style='color:#A6E22E;'><b>$1</b> <span style='color:#66D9EF;'>Object</span></span>\n", $t);
                $t = preg_replace('/\[([\w.:]+)\]/', '[<span style="color:#E6DB74;">$1</span>]', $t);
                $t = preg_replace("/Array\n/", "<span style=\"color:#66D9EF;\">Array</span>\n", $t);
                $t = preg_replace('/\[(.*?):(.*?):(private|protected|public)\]/', "<span style=\"color:#f0f0f0;\">$1</span>:<span style=\"color:#A6E22E;\">$2</span>:<span style=\"color:#F92672;\">$3</span>", $t);
                $t = preg_replace('/\[(.*?):(private|protected|public)\]/', "<span style=\"color:#f0f0f0;\">$1</span>:<span style=\"color:#F92672;\">$2</span>", $t);
            }

            echo $t;
        };

        pre($obj, $admOnly, $printCallback, [
            'color' => '#fff',
            'borderColor' => '#535353',
            'bgColor' => '#676767',
        ]);

        if ($d) {
            die();
        }

    }
}
if (!function_exists('dump')) {
    function dump()
    {
        if($GLOBALS['APPLICATION']){
            $GLOBALS['APPLICATION']->RestartBuffer();
        }
        $args = func_get_args();
        foreach ($args as $data) {
            p($data, false);
        }
    }
}

if (!function_exists('pre')) {
    /**
     * @param $str
     * @param bool $onlyAdmin
     * @param null $printCallback
     * @param array $option
     */
    function pre($str, $onlyAdmin = true, $printCallback = null, $option = []) {
        $isCli = \php_sapi_name() == 'cli';
        $defaultOption = [
            'color' => '#333',
            'borderColor' => '#8892BF',
            'bgColor' => '#EDEEF8',
        ];
        $option = array_merge($defaultOption, $option);
        if(!is_callable($printCallback)){
            $printCallback = function ($str){
                $t = print_r($str, true);
                $t = htmlspecialchars($t);
                echo $t;
            };
        }

        if(!$isCli) {
            $isAdmin = true;
            global $USER;
            if (!is_object($USER) && class_exists('\CUser')) {
                $USER = new \CUser();
            }
            if (is_callable([$USER, 'IsAdmin'])) {
                $isAdmin = $USER->IsAdmin();
            }
            if (!($isAdmin || !$onlyAdmin)) {
                return;
            }
        }

        $bt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);

        $btLine = [];
        foreach ($bt as $item){
            if(in_array($item['function'], [__FUNCTION__, 'dd', 'p', 'dump', 'prex', 'pred'])) {
                $btLine = $item;
            } else {
                break;
            }
        }

        $dRoot = $_SERVER['DOCUMENT_ROOT'];
        $dRoot = str_replace('/', "\\", $dRoot);
        $btLine['file'] = str_replace($dRoot, '', $btLine['file']);
        $dRoot = str_replace("\\", '/', $dRoot);
        $btLine['file'] = str_replace($dRoot, '', $btLine['file']);

        $type = gettype($str);
        $addInfo = '';
        switch ($type) {
            case 'unknown type':
            case 'resource (closed)':
                $str = $type;
                break;
            case 'boolean':
                $str = $str?'true':'false';
                break;
            case 'object':
                $addInfo = '#' . spl_object_id($str);
                break;
            case 'NULL':
                $str = 'null';
                break;
            case 'array':
                $addInfo = 'Count: ' . count($str);
                break;
            case 'string':
                $addInfo = 'Length: ' . mb_strlen($str);
                if($str === '') {
                    $str = '""';
                }
                break;
        }

        if($isCli){
            $data = [
                $btLine['file'] . ' [' . $btLine['line'] . ']',
                'Type: ' . $type,
            ];
            if($addInfo){
                $data[] = $addInfo;
            }

            echo implode('; ', $data) . PHP_EOL;

            if(is_array($str)){
                $str = json_encode($str, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            }
            echo $str . PHP_EOL;
            return;
        }
        echo PHP_EOL;
        ?><div style="font-size:12px;line-height:1em;font-family:monospace;width:100%;color:<?=$option['color']?>;background:<?=$option['bgColor']?>;border:1px solid <?=$option['borderColor']?>;word-break:break-word;text-align:left;"><?
            ?><div style="padding: 2px 10px;font-size:10px;background:<?=$option['borderColor']?>;color:#E2E4EF;"><?
                ?>File:&nbsp;<?=$btLine['file']?>:<?=$btLine['line']?>; Type:&nbsp;<?=$type?>; <?=$addInfo?><?
            ?></div><?
            ?><pre style="padding:2px 10px;white-space:pre-wrap;margin:0;line-height:1em;"><?=PHP_EOL?><?$printCallback(($str));?><?=PHP_EOL?></pre><?
        ?></div><?
        echo PHP_EOL;
    }
}
if (!function_exists('prex')) {
    /**
     * @param $str
     * @param bool $onlyAdmin
     */
    function prex($str, $onlyAdmin = true) {
        pre($str, $onlyAdmin, function ($str){
            print_r(var_export($str, true));
        });
    }
}
if (!function_exists('pred')) {
    /**
     * @param $str
     * @param bool $onlyAdmin
     */
    function pred($str, $onlyAdmin = true) {
        pre($str, $onlyAdmin, function ($str){
            var_dump($str);
        });
    }
}

if (!function_exists('p2f')) {
    function p2f($obj)
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/logs/data.log', print_r($obj, 1) . PHP_EOL, FILE_APPEND);
    }
}

if (!function_exists('test_assert')) {
    function test_assert($bool, $testName = '')
    {
        if ($bool === true) {
            $str = "<p>$testName: <b style=\"color:green\">Yes</b></p>";
        } else {
            $str = "<p>$testName: <b style=\"color:red\">No</b></p>";
        }

        echo $str;
    }
}