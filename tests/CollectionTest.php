<?php

use Wt\Core\Support\Collection;
use Wt\Core\Unit\UnitTestCase;

/**
 * 
 */
class CollectionTest extends UnitTestCase
{
	public function testSetValue()
	{
		$c = new Collection();
		$c->set('1', '1');
		$c->set(0, 0);
		$c->set(null, 2);

		$this->assertSame(['1' => '1', 0 => 0, 2 => 2], $c->toArray(), 'Set collection new value');
		
		$c->set('1', '21');
		$c->set(0, '000');
		$this->assertSame(['1' => '21', 0 => '000', 2 => 2], $c->toArray(), 'Change collection isset value');
		
		$c->put(0, '0000');
		$this->assertSame(['1' => '21', 0 => '0000', 2 => 2], $c->toArray(), 'Change collection isset value method "put"');
		
		$c->push('3');
		$this->assertSame(['1' => '21', 0 => '0000', 2 => 2, 3 => '3'], $c->toArray(), 'Change collection add item "push" method');
	}

	public function testSetValueAsArray()
	{
		$c = new Collection();
		$c['1'] = '1';
		$c[0] = 0;
		$c[] = 2;

		$this->assertSame(['1' => '1', 0 => 0, 2 => 2], $c->toArray(), 'Set collection new value');
		
		$c['1'] = '21';
		$c[0] = '000';
		$this->assertSame(['1' => '21', 0 => '000', 2 => 2], $c->toArray(), 'Change collection isset value');
	}

	public function testHasValue()
	{
		$c = new Collection();

		$c[-1] = null;
		$c->set('_fff', '');

		$this->assertFalse($c->has(0), 'Has key 0 not isset');
		$this->assertFalse($c->has('1'), 'Has key "1" not isset');
		$this->assertFalse($c->has('fff'), 'Has key "ffff" not isset');
		$this->assertFalse($c->has(null), 'Has key null not isset');
		
		$this->assertTrue($c->has(-1), 'Has key -1 isset');
		$this->assertTrue($c->has('-1'), 'Has key "-1" isset');
		$this->assertTrue($c->has('_fff'), 'Has key "_fff" isset');

		$c[] = 0;
		$c[] = null;
		$this->assertTrue($c->has(0), 'Has key 0 isset');
		$this->assertTrue($c->has(1), 'Has key 0 isset');
	}

	public function testGetValue()
	{
		$c = new Collection([
			0,
			'1',
			'4' => '4',
			'fff' => 'fff',
			'null' => null,
			'false' => false
		]);

		$this->assertSame(0, $c->get(0), 'Get value from key 0');
		$this->assertSame("1", $c->get(1), 'Get value from key 1');
		$this->assertSame("1", $c->get("1"), 'Get value from key "1"');
		$this->assertSame("fff", $c->get("fff"), 'Get value from key "fff"');
		$this->assertSame(null, $c->get(null), 'Get value from key null');
		$this->assertSame(null, $c->get('null'), 'Get value from key "null"');
		$this->assertSame(false, $c->get('false'), 'Get value from key "false"');
		$this->assertSame(null, $c->get('tmp'), 'Get value from not exists key "tmp"');
		$this->assertSame('default', $c->get('tmp', 'default'), 'Get default value("default") from not exists key "tmp"');
		$this->assertSame(false, $c->get('tmp', false), 'Get default value(false) from not exists key "tmp"');
		$this->assertSame(null, $c->get('tmp', null), 'Get default value(null) from not exists key "tmp"');
	}

	public function testGetValueAsArray()
	{
		$c = new Collection([
			0,
			'1',
			'4' => '4',
			'fff' => 'fff',
			'null' => null,
			'false' => false
		]);

		$this->assertSame(0, $c[0], 'Get value from key 0');
		$this->assertSame("1", $c[1], 'Get value from key 1');
		$this->assertSame("1", $c["1"], 'Get value from key "1"');
		$this->assertSame("fff", $c["fff"], 'Get value from key "fff"');
		$this->assertSame(null, $c[null], 'Get value from key null');
		$this->assertSame(null, $c['null'], 'Get value from key "null"');
		$this->assertSame(false, $c['false'], 'Get value from key "false"');
		$this->assertSame(null, $c['tmp'], 'Get value from not exists key "tmp"');
	}

	public function testDeleteValue()
	{
		$c = new Collection([
			0,
			'1',
			'4' => '4',
			'fff' => 'fff',
			'null' => null,
			'false' => false
		]);

		unset($c['fff']);
		$this->assertSame([0, '1', '4' => '4', 'null' => null, 'false' => false], $c->toArray(), 'Unset value as array');
		
		$c->forget('null');
		$this->assertSame([0, '1', '4' => '4', 'false' => false], $c->toArray(), 'Unset value use forget method');

		$c->delete('0');
		$this->assertSame([1 => '1', '4' => '4', 'false' => false], $c->toArray(), 'Unset value use delete method');

		$c->unset('4');
		$this->assertSame([1 => '1', 'false' => false], $c->toArray(), 'Unset value use unset method');

		$c->delete(['1', 'false']);
		$this->assertSame([], $c->toArray(), 'Unset multikey use delete method');
	}

	public function testConstruct()
	{
		$r = new Collection([]);
		$this->assertInstanceOf(Collection::class, $r, 'Class must be collection instances');
	}

    public function testJson()
    {
        $r = new Collection(['name' => 'yuma']);
        $this->assertSame('{"name":"yuma"}', json_encode($r), 'json_encode');
        $r = new Collection(['name' => 'Кирилица']);
        $this->assertSame('{"name":"\u041a\u0438\u0440\u0438\u043b\u0438\u0446\u0430"}', json_encode($r), 'json_encode');

        $r = new Collection(['name' => new Collection(['name' => 'yuma'])]);
        $this->assertSame('{"name":{"name":"yuma"}}', json_encode($r), 'json_encode');
    }

    public function testMerge()
    {
        $r = new Collection(['name' => 'yuma']);
        $c = $r->merge(['age' => 18]);
        $this->assertSame(['name' => 'yuma', 'age' => 18], $c->all(), 'merge');
        $c = $r->merge([18]);
        $this->assertSame(['name' => 'yuma', 18], $c->all(), 'merge');


        $r = new Collection([12 => 'data1']);
        $c = $r->merge(['13' => 'data2']);
        $this->assertSame(['data1', 'data2'], $c->all(), 'merge intKey');


        $r = new Collection(['s12' => 'data1']);
        $c = $r->merge(['s13' => 'data2']);
        $this->assertSame(['s12' => 'data1', 's13' => 'data2'], $c->all(), 'merge intKey');
    }

    public function testSlice()
    {

        $c = new Collection(['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5]);
        $this->assertSame(['C' => 3, 'D' => 4, 'E' => 5], $c->slice(2)->toArray(), 'Slice 1');
        $this->assertSame(['C' => 3, 'D' => 4], $c->slice(2, 2)->toArray(), 'Slice 2');
        $this->assertSame(['C' => 3, 'D' => 4, 'E' => 5], $c->slice(2, 4)->toArray(), 'Slice 3');
        $this->assertSame(['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5], $c->slice(0, 10)->toArray(), 'Slice 4');
        $this->assertSame(['A' => 1], $c->slice(0, 1)->toArray(), 'Slice');
        $this->assertSame(['A' => 1, 'B' => 2, 'C' => 3], $c->slice(0, -2)->toArray(), 'Slice 5');
        $this->assertSame(['B' => 2, 'C' => 3], $c->slice(1, -2)->toArray(), 'Slice 6');

        $c = new Collection([10 => 1, 20 => 2, 30 => 3, 40 => 4, 50 => 5]);
        $this->assertSame([3, 4, 5], $c->slice(2, null, false)->toArray(), 'Slice 7');
        $this->assertSame([30 => 3, 40 => 4, 50 => 5], $c->slice(2, null, true)->toArray(), 'Slice 8');
    }

    public function testPlus()
    {

        $r = new Collection([12 => 'data1']);
        $c = $r->plus(['13' => 'data2']);
        $this->assertSame([12 => 'data1', 13 => 'data2'], $c->all(), 'plus intKey');

        $r = new Collection([12 => 'data1']);
        $c = $r->plus(['12' => 'data2']);
        $this->assertSame([12 => 'data1'], $c->all(), 'plus intKey');

        $r = new Collection([12 => 'data1']);
        $c = $r->plus(['12' => 'data2', '13' => 'data3']);
        $this->assertSame([12 => 'data1', 13 => 'data3'], $c->all(), 'plus intKey');


        $r = new Collection(['s12' => 'data1']);
        $c = $r->plus(['s13' => 'data2']);
        $this->assertSame(['s12' => 'data1', 's13' => 'data2'], $c->all(), 'plus intKey');
    }

    public function testLik()
    {
        $r = new Collection(['name' => 'yuma']);
        $c = &$r->get('name');
        $r->set('name', 'kate');
        $this->assertSame('kate', $c, 'get with link');

        $r = new Collection(['name' => 'yuma']);
        $c = $r->get('name');
        $r->set('name', 'kate');
        $this->assertSame('yuma', $c, 'get without link');


        $r = new Collection(['name' => 'yuma']);
        $c = &$r->first();
        $r->set('name', 'kate');
        $this->assertSame('kate', $c, 'first with link');

        $r = new Collection(['name' => 'yuma']);
        $c = $r->first();
        $r->set('name', 'kate');
        $this->assertSame('yuma', $c, 'first without link');

        $r = new Collection(['age' => 18, 'name' => 'yuma']);
        $c = &$r->last();
        $r->set('name', 'kate');
        $this->assertSame('kate', $c, 'last with link');

        $r = new Collection(['age' => 18, 'name' => 'yuma']);
        $c = $r->last();
        $r->set('name', 'kate');
        $this->assertSame('yuma', $c, 'last without link');
    }

    public function testPull()
    {
        $collection = new Collection(['product_id' => 'prod-100', 'name' => 'Desk']);
        $this->assertSame('Desk', $collection->pull('name'), 'pull');
        $this->assertSame([
            'product_id' => 'prod-100'
        ], $collection->all(), 'pull');

    }

    public function testMapWithKeys()
    {
        $collection = new Collection([
            [
                'name' => 'John',
                'department' => 'Sales',
                'email' => 'john@example.com',
            ],
            [
                'name' => 'Jane',
                'department' => 'Marketing',
                'email' => 'jane@example.com',
            ]
        ]);
        $keyed = $collection->mapWithKeys(function ($item, $key) {
            return [$item['email'] => $item['name']];
        });

        $this->assertSame([
            'john@example.com' => 'John',
            'jane@example.com' => 'Jane',
        ], $keyed->all(), 'mapWithKeys');
    }

    public function testOnly()
    {
        $collection = new Collection([
            'product_id' => 1,
            'name' => 'Desk',
            'price' => 100,
            'discount' => false
        ]);
        $filtered = $collection->only(['product_id', 'name']);

        $this->assertSame(['product_id' => 1, 'name' => 'Desk'], $filtered->all(), 'only');
    }

    public function testToJson()
    {
        $array = [
            'pointClasses' => [
                new Point(7, -5),
                new Point(10, -5),
            ]
        ];
        $collection = new Collection($array);

        $this->assertSame('{"pointClasses":["PointCoordinates: (7; -5)","PointCoordinates: (10; -5)"]}', $collection->toJson(), 'toJson by __toString');
        /**
         * \Bitrix\Main\Web\Json::encode($array) = {"pointClasses":[{},{}]}
         *
         * Битриксовый метод валится на данном тесте
         */
    }

    public function testPrepend()
    {
        $array = [
            10 => 10,
            20 => 20,
        ];
        $collection = new Collection($array);
        $collection->prepend(1);
        $this->assertSame('{"0":1,"10":10,"20":20}', $collection->toJson(), 'prepend');

        $array = [
            10 => 10,
            20 => 20,
        ];
        $collection = new Collection($array);
        $collection->prepend(1, 1);
        $this->assertSame('{"1":1,"10":10,"20":20}', $collection->toJson(), 'prepend');
        $collection->prepend(2, 1);
        $this->assertSame('{"1":2,"10":10,"20":20}', $collection->toJson(), 'prepend');
        $collection->put(100, 100);
        $collection->prepend(101, 100);
        $this->assertSame('{"100":101,"1":2,"10":10,"20":20}', $collection->toJson(), 'prepend');
    }

    public function testPop()
    {
        $array = [
            10 => 10,
            20 => 20,
        ];
        $collection = new Collection($array);

        $this->assertSame(20, $collection->pop(), 'pop');
        $this->assertSame('{"10":10}', $collection->toJson(), 'pop');


        $array = [];
        $collection = new Collection($array);

        $this->assertSame(null, $collection->pop(), 'pop');

    }

    public function testShift()
    {
        $array = [
            10 => 10,
            20 => 20,
        ];
        $collection = new Collection($array);

        $this->assertSame(10, $collection->shift(), 'shift');
        $this->assertSame('{"20":20}', $collection->toJson(), 'shift');


        $array = [];
        $collection = new Collection($array);

        $this->assertSame(null, $collection->shift(), 'shift');

    }
    public function testPluck()
    {

        $collection = new Collection([
            'a' => new Collection(['id'=> 1, 'name' => 'a1']),
            'b' => new Collection(['id'=> 2, 'name' => 'a2']),
        ]);

        $this->assertSame([1 => 'a1', 2 => 'a2'], $collection->pluck('name', 'id')->all(), 'pluck');

        $collection = new Collection([
            new Collection(['id'=> 1,]),
            'a' => new Collection(['id'=> 2, 'name' => 'a2']),
            new Collection(['id'=> 3, 'name' => '']),
            new Collection(['id'=> 4, 'name' => 'a4']),
        ]);

        $this->assertSame([2 => 'a2', 3 => '', 4 => 'a4'], $collection->pluck('name', 'id')->all(), 'pluck');

    }
}


/**
 * для теста метода toJson
 * Class Point
 */
class Point {
    private $x;
    private $y;
    public function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }
    public function __toString() {
        return "PointCoordinates: ($this->x; $this->y)";
    }
}