<?php

use Wt\Core\Interfaces\IPsr;
use Wt\Core\Support\ArrayToPsr;
use Wt\Core\Support\Collection;
use Wt\Core\Support\CollectionToPsr;
use Wt\Core\Unit\UnitTestCase;

/**
 * 
 */
class ArrayToPsrTest extends UnitTestCase
{
	public function testReadPropertyAsPsr()
	{
		$e = new ArrayToPsr(
			[
				'UPPER' => 'UPPER',
				'lower' => 'lower',
				'camelCase' => 'camelCase', 
				'UPPER_SNAKE_CASE' => 'UPPER_SNAKE_CASE', 
				'lower_snake_case' => 'lower_snake_case',
			]
		);

		$this->assertSame('UPPER', $e->upper, 'UPPER');
		$this->assertSame('lower', $e->lower, 'lower');
		$this->assertSame('camelCase', $e->camelCase, 'camelCase');
		$this->assertSame('UPPER_SNAKE_CASE',$e->upperSnakeCase,  'UPPER_SNAKE_CASE');
//		$this->assertSame('lower_snake_case', $e->lowerSnakeCase, 'lower_snake_case');
	}

	public function testRecursionAsPsr()
	{
		$e = new ArrayToPsr(
			[
				'sub' => [
					'id' => 1,
					'name' => 'sub'
				],
				'collectionIds' => [
					'first',
					'second'
				]
			],
			false
		);

		$this->assertSame($e->sub['id'], 1, 'recursion flag false, get value as array');
		$this->assertSame($e->sub->id, null, 'recursion flag false, get value as property');
		$this->assertSame($e->collectionIds, ['first', 'second'], 'recursion flag false, get value as property');

		$e = new ArrayToPsr(
			[
				'sub' => [
					'id' => 1,
					'name' => 'sub'
				],
				'collectionIds' => [
					'first',
					'second'
				],
                'emptyArray' => []
			],
			true
		);

		$this->assertInstanceOf(CollectionToPsr::class, $e->sub,  'recursion flag true, no get value as array');
		$this->assertSame($e->sub->id, 1, 'recursion flag true, get value as property');
		$this->assertInstanceOf(CollectionToPsr::class, $e->collectionIds, 'recursion flag true, no get value as array');
		$this->assertInstanceOf(CollectionToPsr::class, $e->emptyArray, 'recursion flag true, empty array');
		$this->assertSame(0, $e->emptyArray->count(), 'recursion flag true, empty array');
	}

	public function testElementsAsCollection()
	{
		$e = new ArrayToPsr(
			[
				'collection' => [
					['name' => 'name 1'],
					['name' => 'name 2'],
					['name' => 'name 3']
				],
				'elements' => [
					'2' => ['name' => 'name 2'],
					't' => ['name' => 'name t'],
					3 => ['name' => 'name 3']
				],
				'elements_collection' => [
					'2' => ['name' => 'name 2'],
					'55' => ['name' => 'name 55'],
					3 => ['name' => 'name 3']
				]
			]
		);

		$this->assertInstanceOf(Collection::class, $e->collection, 'get property as collection');
		$this->assertSame('name 1', $e->collection->first()->name, 'recursion flag true, get value as property');
		$this->assertInstanceOf(Collection::class, $e->elements, 'get property as not collection');
		$this->assertInstanceOf(IPsr::class,$e->elements,  'get property as not collection(ArrayAsPsr)');
		$this->assertInstanceOf(Collection::class, $e->elements,  'get property as not collection(ArrayAsPsr)');
		$this->assertSame('name t', $e->elements->t->name,  'recursion flag true, get value as property');
		$this->assertSame('name 3', $e->elements->{'3'}->name, 'recursion flag true, get value as property');
		$this->assertInstanceOf(Collection::class, $e->elements_collection,  'get property as collection');
		$this->assertSame('name 2', $e->elements_collection->first()->name,  'recursion flag true, get value as property');
	}

	public function testUFElements()
	{
		$e = new ArrayToPsr([
			'sname' => 'sname',
			'UF_NAME' => 'UF_NAME',
			'UF_SNAME' => 'UF_SNAME',
			'UF_FIRST_NAME' => 'UF_FIRST_NAME',
			'uf_active' => 'uf_active',
			'UF_BRAND_ID' => 'UF_BRAND_ID',
		]);

		$this->assertSame('UF_NAME', $e->name, 'UF_NAME');
		$this->assertSame('UF_FIRST_NAME', $e->firstName, 'UF_NAME');
		$this->assertSame(null, $e->active,'UF_ACTIVE');
		$this->assertSame('UF_BRAND_ID', $e->brandId, 'UF_BRAND_ID');
		$this->assertSame('UF_SNAME',$e->ufSname,  'UF_SNAME');
		$this->assertSame('UF_SNAME', $e->sname, 'sname');
	}

	public function testChangeValue()
	{
		$e = new ArrayToPsr([
			'sname' => 'sname',
			'UF_SNAME' => 'UF_SNAME',
			'UF_NAME' => 'UF_NAME',
			'UF_FIRST_NAME' => 'UF_FIRST_NAME',
			'PROPERTY_BRAND_ID' => 'PROPERTY_BRAND_ID'
		]);

		$e->sname = 'new sname';

		$this->assertSame('new sname', $e->sname, 'new sname');
		$e->ufSname = 'new uf_sname';
		$this->assertSame($e->ufSname, 'new uf_sname', 'new uf_sname');
		$e->name = 'new name';
		$this->assertSame($e->name, 'new name', 'new name');
		$e->firstName = 'new first name';

		$this->assertSame($e->firstName, 'new first name', 'new first name');

		$this->assertSame($e->ufFirstName, 'new first name', 'new first name 2');

		$e->propertyBrandId = 'new property id';
		$this->assertSame($e->propertyBrandId, 'new property id', 'new property id 1');
        /**
         * не согласен с результатом теста
         */
//		$this->assertSame($e->PROPERTY_BRAND_ID, 'PROPERTY_BRAND_ID', 'new property id 2 * не согласен с результатом теста');
        /**
         * Важен порядок тестов
         */
		$this->assertSame($e->PROPERTY_BRAND_ID, 'new property id', 'new property id 2+');
		$this->assertSame($e->UF_PROPERTY_BRAND_ID, null, 'new property id 5');
		$this->assertSame($e->ufPropertyBrandId, null, 'new property id 5');
        $e->ufPropertyBrandId = 'ufPropertyBrandId';
        $this->assertSame($e->UF_PROPERTY_BRAND_ID, 'ufPropertyBrandId', 'new property id 5');
        $this->assertSame($e->ufPropertyBrandId, 'ufPropertyBrandId', 'new property id 5');
        $this->assertSame($e->PROPERTY_BRAND_ID, 'new property id', 'new property id 2+');
        $this->assertSame($e->propertyBrandId, 'new property id', 'new property id 2+');
        unset($e->propertyBrandId);
        $this->assertSame($e->propertyBrandId, 'ufPropertyBrandId', 'new property id 2+');



		$e->PROPERTY_BRAND_ID = 'NEW PROPERTY ID';
		$this->assertSame($e->PROPERTY_BRAND_ID, 'NEW PROPERTY ID', 'new property id 3');
		$this->assertSame($e->propertyBrandId, 'NEW PROPERTY ID', 'new property id 4');
	}

	public function testGetValue()
	{
		$e = new ArrayToPsr([
			'BOOLY' => 'Y',
			'BOOLN' => 'N',
			'STR' => 'lorem',
			'null' => null,
			'REALBOOLT' => true,
			'REALBOOLF' => false,
			'IDS' => [1, 2, 3, 4],
			'KEYS' => ['K1' => 1, 'K2' => 'Y', 'K3' => 'k3'],
			'COMPLEX' => ['BRAND' => 'brand', 45 => 'N', 12 => 'lasr']
		]);

		$this->assertSame($e->booly, 'Y', 'BOOLY');
		$this->assertSame($e->booln, 'N', 'BOOLN');
		$this->assertSame($e->str, 'lorem', 'str');
		$this->assertSame($e->null, null, '');
		$this->assertSame($e->realboolt, true, '');
		$this->assertSame($e->realboolf, false, '');
		$this->assertInstanceOf(CollectionToPsr::class, $e->ids, '');
		$this->assertInstanceOf(CollectionToPsr::class, $e->keys, '');
		$this->assertSame($e->keys->k1, 1, '');
		$this->assertSame($e->keys->k2, 'Y', '');
		$this->assertInstanceOf(CollectionToPsr::class, $e->complex,  '');
		$this->assertSame($e->complex->brand, 'brand', '');
		$this->assertSame($e->complex->{'12'}, 'lasr', '');
		$this->assertSame($e->complex->{'45'}, 'N', '');
		$this->assertSame($e->complex->getRaw()['45'], 'N', '');
	}
}