<?php

use Wt\Core\Type\Arr;
use Wt\Core\Unit\UnitTestCase;


class ArrayTest extends UnitTestCase
{

    public function testMatch()
    {
        $array = [
            'A1' => [
                'B1' => [
                    'C1' => [
                        'D1' => '1',
                        'D2' => '2',
                        'D3' => '3',
                    ],
                    'C2' => [
                        'D1' => '4',
                        'D2' => '5',
                        'D3' => '6',
                    ],
                    'C3' => [
                        'D1' => '7',
                        'D2' => '8',
                        'D3' => '9',
                    ],
                ],
                'B2' => [
                    'C1' => [
                        'D1' => '10',
                        'D2' => '11',
                        'D3' => '12',
                    ],
                    'C2' => [
                        'D1' => '13',
                        'D2' => '14',
                        'D3' => 'D3+',
                    ],
                    'C3' => [
                        'D1' => '15',
                        'D2' => '16',
                        'D3' => '17',
                    ],
                ],
            ],
            'A2' => [
                'B1' => [
                    'C1' => [
                        'D1' => '18',
                        'D2' => '19',
                        'D3' => '20',
                    ],
                    'C2' => [
                        'D1' => '21',
                        'D2' => '22',
                        'D3' => '23',
                    ],
                    'C3' => [
                        'D1' => '24',
                        'D2' => '25',
                        'D3' => '26',
                    ],
                ],
                'B2' => [
                    'C1' => [
                        'D1' => '27',
                        'D2' => '28',
                        'D3' => '29',
                    ],
                    'C2' => [
                        'D1' => '30',
                        'D2' => '31',
                        'D3' => '32',
                    ],
                    'C3' => [
                        'D1' => '33',
                        'D2' => '34',
                        'D3' => '35',
                    ],
                ],
            ],
        ];

        $temp = $array['A1']['B1'];

        $this->assertSame(array_keys([null => $temp])[0], '', 'array_keys([null=>$temp])[0] === \'\'');
        $this->assertSame(array_keys(['' => $temp])[0], '', 'array_keys([\'\'=>$temp])[0] === \'\'');
        $this->assertSame(arr::getBranch('.A1.B1.C1.D1')[0], '', 'arr::getBranch(\'.A1.B1.C1.D1\')[0] === \'\'');


        $this->assertSame(Arr::match($temp, []), true, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['C1.D1' => 1]), true, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['*.D1' => 1]), true, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['*.D2' => 8]), true, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['C2.*' => 5]), true, 'Arr::match');
        $this->assertSame(Arr::match($array, ['A1.B1.C1' => $array['A1']['B1']['C1']]), true, 'Arr::match');
        $this->assertSame(Arr::match($array, ['A1.B1.C1.D1' => 1]), true, 'Arr::match');
        $this->assertSame(Arr::match($array, ['A1.B1.C1.D1' => '1']), true, 'Arr::match');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '35']), true, 'Arr::match');

        $this->assertSame(Arr::match(['' => $array], ['.A1.B1.C1.D1' => '1']), true, 'Arr::match пустой ключ [\'\' => $array], [\'.A1.B1.C1.D1\' => \'1\']) === true');
        $this->assertSame(Arr::match(['' => $array], ['0.A1.B1.C1.D1' => '1']), false, 'Arr::match пустой ключ [\'\' => $array], [\'0.A1.B1.C1.D1\' => \'1\']) === false');


        $this->assertSame(Arr::match($array, ['A1.B1.C1.D1.' => '1']), false, 'Arr::match');
        $this->assertSame(Arr::match($array, ['A1..B1.C1.D1' => '1']), false, 'Arr::match');

        $this->assertSame(Arr::match($temp, ['C1.D1' => 2]), false, 'Arr::match');


        $this->assertSame(Arr::match($temp, ['C1.D1' => 1, 'C2.D3' => 6]), true, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['C1.D1' => 1, 'C2.D2' => 6]), false, 'Arr::match');
        $this->assertSame(Arr::match($temp, ['C1.D1' => 1, '*.*' => 9]), true, 'Arr::match');

        $this->assertSame(Arr::match($temp, ['logic' => 'or', ['C1.D1' => 1], ['C2.D2' => 6]]), true, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '35', ['logic' => 'or', ['A1.B1.C1.D1' => 1], ['A1.B1.C2.D2' => 6]]]), true, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '35', ['logic' => 'or', ['A1.B1.C1.D1' => 2], ['A1.B1.C2.D2' => 6]]]), false, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '40', ['logic' => 'or', ['A1.B1.C1.D1' => 1], ['A1.B1.C2.D2' => 6]]]), false, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '35', ['logic' => 'or', ['A1.B1.C1.D1' => 2/*1*/, 'A2.B2.C3.D2' => '34'], ['A1.B1.C2.D2' => 6/*5*/]]]), false, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['A2.B2.C3.D3' => '35', ['logic' => 'or', ['A1.B1.C1.D1' => 1, 'A2.B2.C3.D2' => '34'], ['A1.B1.C2.D2' => 6/*5*/]]]), true, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['logic' => 'or', ['*.*.*.*' => -15], ['A2.B2.C3.D3' => '35']]), true, 'Arr::match or');
        $this->assertSame(Arr::match($array, ['logic' => 'or', ['*.*.*.*' => -15], ['*.*.*.*' => '35']]), true, 'Arr::match or');

        $temp = $array['A1']['B1'];

        //pre($temp);
    }
    public function testIsValidKey()
    {
        $this->assertSame(Arr::isValidKey('key', 'key'), true, 'точное совпадение');
        $this->assertSame(Arr::isValidKey('key', 'Key'), false, 'проверка регистра');
        $this->assertSame(Arr::isValidKey('key', ['KEY', 'Key', 'key']), true, 'маска из массива');
        $this->assertSame(Arr::isValidKey('keY', ['KEY', 'Key', 'key']), false, 'маска из массива');
        $this->assertSame(Arr::isValidKey('KEY', ['*']), true, 'Звездочка по умолчанию');
        $this->assertSame(Arr::isValidKey('KEY', ['*'], false, false), false, 'Звездочка отключена');
        $this->assertSame(Arr::isValidKey('*', [1, '*'], false, false), true, 'Звездочка отключена и ключ *');
        $this->assertSame(Arr::isValidKey('key', ['*'], false, false), false, 'Звездочка отключена и ключ не *');
        $this->assertSame(Arr::isValidKey('34632', '/346[32]\d/'), true, 'regexp');
        $this->assertSame(Arr::isValidKey('34632A', '/346[32]\d/'), true, 'regexp');
        $this->assertSame(Arr::isValidKey('3463', '/346[32]\d$/'), false, 'regexp');
        $this->assertSame(Arr::isValidKey('34632A', '/346[32]\d/', false), false, 'regexp off');
        $this->assertSame(Arr::isValidKey('34632A', '/.*/'), true, 'regexp /.*/');
        $this->assertSame(Arr::isValidKey('', ''), true, 'сравнение пустых строк');
        $this->assertSame(Arr::isValidKey('', null), true, 'сравнение пустой строки и null');
        $this->assertSame(Arr::isValidKey(null, ''), true, 'сравнение null и пустой строки');
        $this->assertSame(Arr::isValidKey(0, ''), false, 'сравнение 0 и пустой строки');
        $this->assertSame(Arr::isValidKey(0, '0'), true, 'сравнение 0 и (string)0');
        $this->assertSame(Arr::isValidKey('', 0), false, 'сравнение пустой строки и 0');
        $this->assertSame(Arr::isValidKey('', '0'), false, 'сравнение пустой строки и (string)0');
        $this->assertSame(Arr::isValidKey('6', 6), true, 'сравнение числа и строки');
        $this->assertSame(Arr::isValidKey(6, '6'), true, 'сравнение числа и строки');
        $this->assertSame(Arr::isValidKey(6.1, 6.1), false, 'float как ключ, Данный тип не может быть ключем. поэтому он априори не может быть валидным');
        $this->assertSame(Arr::isValidKey('6.1', '6.1'), true, 'сравнение float как строка');
        $this->assertSame(Arr::isValidKey('key', ''), false, 'маска пустая строка');
        $this->assertSame(Arr::isValidKey('key', []), true, 'маска пустой массив');
        $this->assertSame(Arr::isValidKey(new Error(), new Error()), false, 'Объекты');
    }

    public function testSetBranch()
    {
        $array = [];
        $result = Arr::setBranch($array, ['KEY'], 1);
        $this->assertSame($array, array ( 'KEY' => 1), 'Arr::setBranch($array, [\'KEY\'], 1)');

        $array = [];
        $result = Arr::setBranch($array, ['KEY', 'key2'], 1);
        $this->assertSame($array, array ( 'KEY' => array ( 'key2' => 1, ), ), 'Arr::setBranch($array, [\'KEY\', \'key2\'], 1)');

    }

    public function testSet()
    {
        $array = [];
        $result = Arr::set($array, 'KEY', 1);
        $this->assertSame($array, array ( 'KEY' => 1, ), 'Arr::set($array, \'KEY\', 1)');

        $result = Arr::set($array, 'KEY', 1);
        $this->assertSame($array, array ( 'KEY' => 1, ), 'Arr::set()');

        $result = Arr::set($array, 'KEY', 2);
        $this->assertSame($array, array ( 'KEY' => 2, ), 'Arr::set()');

        $result = Arr::set($array, 'key', 2);
        $this->assertSame($array, array ( 'KEY' => 2,'key' => 2, ), 'Arr::set()');

        $array = [];
        $result = Arr::set($array, [
            'document' => 1,
            'dom.div' => 2,
            'dom.span' => 2,
            'dom.picture.img' => 3,
        ]);
        $this->assertSame($array, array ('document'=>1,'dom' => ['div' => 2, 'span' => 2, 'picture' => ['img'=>3]]), 'Arr::set()');

        $array = [];
        $result = Arr::set($array,'html.body.div.form.input', 5);
        $this->assertSame($array, array ('html'=>['body'=>['div'=>['form'=>['input'=>5]]]]), 'html.body.div.form.input = 5');
    }


    public function testHasRecursion()
    {
        $arrayNoRec = ['name1' => [[5]],];

        $array2 = [];
        $array1 = [
            'name1' => &$array2,
        ];
        $array2 = [
            'name2' => &$array1
        ];
        $this->assertSame(true, Arr::hasRecursion($array1), 'test hasRec must by true');
        $this->assertSame(false, Arr::hasRecursion($arrayNoRec), 'test hasRec $arrayNoRec');
        $this->assertSame($array1, $array1['name1']['name2'], 'test recursion');
        $this->assertSame(true, in_array($array1['name1']['name2'], [$array1]), 'test recursion by in_array');
    }
}