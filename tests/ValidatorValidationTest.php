<?php

use Wt\Core\Unit\UnitTestCase;
use Wt\Core\Validator\Rule;
use Wt\Core\Validator\Validation;
use Wt\Core\Validator\ValidationForm;
use Wt\Core\Validator\ValidationCollection;


class ValidatorValidationTest extends UnitTestCase
{
	public function testRule()
	{

        $rule = new Rule\Min(10);

        $this->assertSame(true, $rule->validate(11), 'Rule\Min');
        $this->assertSame(true, $rule->validate(10), 'Rule\Min');
        $this->assertSame(false, $rule->validate(5), 'Rule\Min');

        $rule = new Rule\Max(10);
        $this->assertSame(false, $rule->validate(11), 'Rule\Max');
        $this->assertSame(true, $rule->validate(10), 'Rule\Max');
        $this->assertSame(true, $rule->validate(5), 'Rule\Max');

		$rule = new Rule\Collection([
            new Rule\Min(3),
            new Rule\Max(20),
        ]);
        $this->assertSame(false, $rule->validate(2), 'Rule\Collection [Min, Max]');
        $this->assertSame(true, $rule->validate(3), 'Rule\Collection [Min, Max]');
        $this->assertSame(true, $rule->validate(5), 'Rule\Collection [Min, Max]');
        $this->assertSame(true, $rule->validate(20), 'Rule\Collection [Min, Max]');
        $this->assertSame(false, $rule->validate(21), 'Rule\Collection [Min, Max]');

		$rule = new Rule\Collection([
            new Rule\Min(3, true),
            new Rule\Max(20, true),
        ]);
        $this->assertSame(false, $rule->validate(2), 'Rule\Collection [Min, Max] Strict');
        $this->assertSame(false, $rule->validate(3), 'Rule\Collection [Min, Max] Strict');
        $this->assertSame(true, $rule->validate(5), 'Rule\Collection [Min, Max] Strict');
        $this->assertSame(false, $rule->validate(20), 'Rule\Collection [Min, Max] Strict');
        $this->assertSame(false, $rule->validate(21), 'Rule\Collection [Min, Max] Strict');

        $rule = new Rule\LogicOr([
            new Rule\Collection([
                new Rule\Min(3),
                new Rule\Max(20),
            ]),
            new Rule\Collection([
                new Rule\Min(40),
                new Rule\Max(50),
            ])
        ]);
        $this->assertSame(false, $rule->validate(2), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(3), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(5), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(20), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(false, $rule->validate(21), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(40), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(45), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(true, $rule->validate(50), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');
        $this->assertSame(false, $rule->validate(100), 'Rule\LogicOr [Rule\Collection [Min, Max], Rule\Collection [Min, Max]]');

        /**
         *  нет правил и валидация прошла
         */
        $rule = new Rule\LogicOr([]);
        $this->assertSame(true, $rule->validate(2), 'empty Rule\LogicOr');

        $rule = new Rule\Collection([]);
        $this->assertSame(true, $rule->validate(2), 'empty Rule\Collection');


        $rule = new Rule\Count(0);
        $this->assertSame(true, $rule->validate([]), 'Rule\Count');
        $rule = new Rule\Count(3);
        $this->assertSame(true, $rule->validate([1,2,3]), 'Rule\Count');
        $rule = new Rule\Count(3);
        $this->assertSame(false, $rule->validate([1,2]), 'Rule\Count');

        $rule = new Rule\CountFromTo(2, 50);
        $this->assertSame(false, $rule->validate([]), 'Rule\CountFromTo');
        $rule = new Rule\CountFromTo(3, 5, true);
        $this->assertSame(false, $rule->validate([1,2,3]), 'Rule\CountFromTo');
        $rule = new Rule\CountFromTo(3, 5);
        $this->assertSame(true, $rule->validate([1,2,3,4]), 'Rule\CountFromTo');


        $rule = new Rule\EqualTo('test');
        $this->assertSame(true, $rule->validate('test'), 'Rule\EqualTo');
        $rule = new Rule\EqualTo('test');
        $this->assertSame(false, $rule->validate('test2'), 'Rule\EqualTo');
        $rule = new Rule\EqualTo(10, true);
        $this->assertSame(false, $rule->validate('10'), 'Rule\EqualTo');

        $rule = new Rule\InArray(['t1', 't2']);
        $this->assertSame(true, $rule->validate('t1'), 'Rule\InArray');
        $this->assertSame(false, $rule->validate('10'), 'Rule\InArray');

        $rule = new Rule\Required();
        $r = [];
        $this->assertSame(false, $rule->validate($r['asd']), 'Rule\Required');
        $this->assertSame(false, $rule->validate(null), 'Rule\Required');
        $this->assertSame(true, $rule->validate('10'), 'Rule\Required');
        $this->assertSame(false, $rule->validate(''), 'Rule\Required');
        $this->assertSame(true, $rule->validate(0), 'Rule\Required');
        $this->assertSame(false, $rule->validate([]), 'Rule\Required');
        $this->assertSame(true, $rule->validate([1]), 'Rule\Required');

        $rule = new Rule\Callback(function ($value){
            return (bool)$value;
        });
        $this->assertSame(true, $rule->validate(1), 'Rule\Callback');
        $this->assertSame(false, $rule->validate(0), 'Rule\Callback');
	}

	public function testValidation()
	{
        $validation = new Validation(
            new Rule\Collection([
                new Rule\Min(3),
                new Rule\Max(20),
            ]),
            'Значение должно быть от 3 до 20',
            'testCode'
        );

        $this->assertSame(true, $validation->validate(15), 'Validation');
        $this->assertSame(false, $validation->validate(60), 'Validation');
        $this->assertSame('testCode', $validation->getErrors(60)->first()->getCode(), 'Validation');
	}

    public function testValidationCollection()
    {
        $vc = new ValidationCollection([
            new Validation(
                new Rule\Collection([
                    new Rule\Min(3),
                    new Rule\Max(20),
                ]),
                'Значение должно быть от 3 до 20',
                'VC1000'
            ),
            new Validation(
                new Rule\Regex('/^[1]+$/'),
                'Выражение должно состоять только из 1',
                'VC1001'
            )
        ]);
        $e = $vc->getErrors(12);

        $this->assertSame('VC1001', $e->first()->getCode(), 'ValidationCollection');
        $this->assertSame(1, $e->count(), 'ValidationCollection');

        $e = $vc->getErrors(200);

        $this->assertSame('VC1000', $e->first()->getCode(), 'ValidationCollection');
        $this->assertSame(2, $e->count(), 'ValidationCollection');

        $e = $vc->getErrors(11);

        $this->assertSame(0, $e->count(), 'ValidationCollection');



        $vc = new ValidationCollection([
            new Validation(
                new Rule\Collection([
                    new Rule\Min(3),
                    new Rule\Max(20),
                ]),
                'Значение должно быть от 3 до 20',
                'VC1000'
            ),
            new Validation(
                new Rule\Regex('/^[1]+$/'),
                'Выражение должно состоять только из 1',
                'VC1001'
            ),
        ], 'Значение не валидно. Введите другое', 'VC1002');

        $e = $vc->getErrors(101);
        $this->assertSame('VC1002', $e->first()->getCode(), 'ValidationCollection');
        $this->assertSame(1, $e->count(), 'ValidationCollection');
    }

	public function testValidationForm()
	{

	    $validationForm = new ValidationForm([
            'name' => new ValidationCollection([
                new Validation(
                    new Rule\Regex('/^[\w]+$/'),
                    'Имя должно содержать только букбы',
                    'VF1001'
                ),
                new Validation(
                    new Rule\StrLenMax(6),
                    'Длина имени превышает 6 символов',
                    'VF1002'
                ),
                new Validation(
                    new Rule\StrLenMin(3),
                    'Длина имени должна быть больше 3 символов',
                    'VF1003'
                )
            ]),
	        'disc' => new Validation(
                new Rule\Collection([
                    new Rule\Min(3),
                    new Rule\Max(20),
                ]),
                'Скидка должна быть от 3 до 20 %',
                'VF1010'
            ),
	        'msg' => new Validation(
                new Rule\StrLenMax(30),
                'Длина текста превышает 30 символов',
                'VF1020'
            ),
        ]);

        $e = $validationForm->getErrors([
            'name' => '**',
            'disc' => 2,
            'msg' => '0123456789012345678901234567890123456789',
        ]);

        $this->assertSame('VF1001', $e->get('name')->getCode(), 'ValidationCollection');



        $validationForm = new ValidationForm([
            'name' => new ValidationCollection([
                new Validation(
                    new Rule\Regex('/^[\w]+$/'),
                    'Имя должно содержать только букбы',
                    'VF1001'
                ),
                new Validation(
                    new Rule\StrLenMax(6),
                    'Длина имени превышает 6 символов',
                    'VF1002'
                ),
                new Validation(
                    new Rule\StrLenMin(3),
                    'Длина имени должна быть больше 3 символов',
                    'VF1003'
                )
            ], '', 'VF1000'),
            'disc' => new Validation(
                new Rule\Collection([
                    new Rule\Min(3),
                    new Rule\Max(20),
                ]),
                'Скидка должна быть от 3 до 20 %',
                'VF1010'
            ),
            'msg' => new Validation(
                new Rule\StrLenMax(30),
                'Длина текста превышает 30 символов',
                'VF1020'
            ),
        ]);

        $e = $validationForm->getErrors([
            'name' => '**',
            'disc' => 2,
            'msg' => '89*',
        ]);

        $this->assertSame('VF1000', $e->get('name')->getCode(), 'ValidationCollection');



        $validationForm = new ValidationForm([
            'name' => new Validation(
                new Rule\Required(),
                'Это обязатлеьное поле',
                'VF1040'
            ),
        ]);
        $e = $validationForm->getErrors([
            'name' => [],
            'disc' => 2,
            'msg' => '89*',
        ]);
        $this->assertSame('VF1040', $e->get('name')->getCode(), 'ValidationCollection');

        $e = $validationForm->getErrors([
            'name' => '',
            'disc' => 2,
            'msg' => '89*',
        ]);
        $this->assertSame('VF1040', $e->get('name')->getCode(), 'ValidationCollection');

        $e = $validationForm->getErrors([
            'name' => 0,
            'disc' => 2,
            'msg' => '89*',
        ]);
        $this->assertSame([], $e->toArray(), 'ValidationCollection');


        $validationForm = new ValidationForm([
            'name' => new Validation(
                new Rule\Callback(function (){

                }),
                'Это обязатлеьное поле',
                'VF1040'
            ),
        ]);

	}
}