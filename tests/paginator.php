<?php
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Wt\Core\Services\Paginator;

\Bitrix\Main\Loader::includeModule('wt.core');

$p = new Paginator(11);


test_assert($p->getTotalCountItems() == 11, '$p->getTotalCountItems() == 11');
test_assert($p->getItemsPerPage() == 10, '$p->getItemsPerPage() == 10');
test_assert($p->getTotalPages() == 2, '$p->getTotalPages() == 2');
test_assert($p->getCurrentPage() == 1, '$p->getCurrentPage() == 1');
test_assert($p->getOffset() == 0, '$p->getOffset() == 0');
$p->setItemsPerPage(3);
test_assert($p->getItemsPerPage() == 3, '$p->setItemsPerPage(3); $p->getItemsPerPage() == 3');
test_assert($p->getTotalPages() == 4, '$p->getTotalPages() == 4');
$p->setCurrentPage(2);
test_assert($p->getCurrentPage() == 2, '$p->setCurrentPage(2);$p->getCurrentPage() == 2');
test_assert($p->getOffset() == 3, '$p->getOffset() == 3');
$p->setCurrentPage(20);
test_assert($p->getCurrentPage() == 1, '$p->setCurrentPage(20);$p->getCurrentPage() == 1');


$p = new Paginator(31);
$p->setItemsPerPage(3);
$p->setCurrentPage(5);
p($p->getSmartPagenInfo1(3), 0);

$p->setCurrentPage(5);
dd($p->getSmartPagenInfo(3));