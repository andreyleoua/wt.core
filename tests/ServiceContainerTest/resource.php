<?php


class UserSingle
{
    protected function __construct(){}
}

class User {}

class UserA {
    protected $user;
    public function __construct($user)
    {
        $this->user = $user;
    }
}

class ExtUser extends UserA {}

class UserB
{
    public $name;
    public $lname;
    public function __construct($name, $lastName = ''){
        $this->name = $name;
        $this->lname = $lastName;
    }
}

class UserName {
    public $user;
    public function __construct($user='')
    {
        $this->user = $user;
    }
}

class UserNameLast {
    public $user;
    public $last;
    public function __construct($user='', $last = '')
    {
        $this->user = $user;
        $this->last = $last;
    }
}