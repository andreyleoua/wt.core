<?php


use Wt\Core\Assets\TAssets;
use Wt\Core\Templater\Templater;
use Wt\Core\Templater\TTemplater;
use Wt\Core\Unit\UnitTestCase;


class AssetsTest extends UnitTestCase
{

    use TAssets;


    public function testAssets()
    {
        $this->assets()->setAssets([
            'css' => 'style.css',
            'js' => 'script.js',
            'plugin' => 'test',
        ]);
        $this->assertInstanceOf(\Wt\Core\Assets\AssetsPull::class, $this->assets()->getPluginManager()->getAssetsPull(), 'AssetsPull::class');
        $this->assets()->setAssets([
            'css' => 'style2.css',
            'js' => [
                'script2.js',
                'script3.js',
            ],
            'plugin' => 'test2',
        ]);

        $this->assets()->getPluginManager()->unsetPlugin('test2');
        $this->assets()->getPluginManager()->unsetPlugin('test');



    }
}