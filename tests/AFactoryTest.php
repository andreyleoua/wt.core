<?php

use Wt\Core\Interfaces\IEntity;
use Wt\Core\Unit\UnitTestCase;

/**
 * 
 */
class AFactoryTest extends UnitTestCase
{


    public function testAbstractFactory()
    {
        $this->assertSame(true, app()->factory()->main()->getSiteFactory()->getConfig() instanceof \Wt\Core\Support\LexerCollection, 'config class');
    }

    public function testAgentFactory()
    {
        $this->assertSame('main', app()->factory()->main()->getAgentFactory()->getCollection()->first()->getModuleId(), 'agent');
    }

    public function testCache()
    {
        //$factory = app()->factory()->main()->getIBlockTypeFactory()->cache(['hit', 'bx_1hour']);
        $factory = app()->factory()->main()->getIBlockFactory()->cache(['hit', 'bx_1hour']);
        $factoryNoCache = app()->factory()->main()->getIBlockFactory()->cache([]);
        $this->assertSame('Wt\Core\Factory\IBlockFactory', get_class($factoryNoCache), 'cache');


        $this->assertSame('Wt\Core\FactoryProxy\ArrayCacheFactory', get_class($factory), 'cache');
        $this->assertSame('Wt\Core\FactoryProxy\BitrixCacheFactory', get_class($factory->getFactory()), 'cache');


        $collection = $factory->getCollection();
        $entity = $collection->first();
        if($collection->count()){
            $this->assertSame('Wt\Core\Collection\IBlockCollection', get_class($collection), 'cache');
            $this->assertSame('Wt\Core\Entity\IBlockEntity', get_class($entity), 'cache');
            $this->assertSame('Wt\Core\FactoryProxy\ArrayCacheFactory', get_class($entity->getFactory()), 'cache');
            $this->assertSame('Wt\Core\FactoryProxy\BitrixCacheFactory', get_class($entity->getFactory()->getFactory()), 'cache');
        }
        else {
            $this->assertSame(null, $entity, 'null entity');
        }

    }

    public function testHlBlock()
    {
        $hlBlockCollection = app()->factory()->main()->getHlBlockFactory()->getCollection();
        if($hlBlockCollection->count()){
            $tableName = $hlBlockCollection->first()->getTableName();
            $hlBlockCollection->first()->getEntityName();
            $hlBlockCollection->last()->getORMDataManager()::getEntity();
            $id = $hlBlockCollection->last()->getId();
            $code = $hlBlockCollection->last()->getEntityName();

            $hlBlockCollection = app()->factory()->main()->getHlBlockFactory()->getEntityByXmlId($tableName);
            $this->assertSame(true, $hlBlockCollection instanceof IEntity, 'getEntityByXmlId');

            $hlBlockCollection = app()->factory()->main()->getHlBlockFactory()->getEntityByCode($code);
            $this->assertSame(true, $hlBlockCollection instanceof IEntity, 'getEntityByCode');

            $hlBlockCollection = app()->factory()->main()->getHlBlockFactory()->getEntityById($id);
            $this->assertSame(true, $hlBlockCollection instanceof IEntity, 'getEntityById');
        }

    }

}