<?php

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Wt\Core\Services\LexerCollection;

\Bitrix\Main\Loader::includeModule('wt.core');

$c = app('lcollection', [
    'test' => 'test',
    'name' => 'name',
    'code' => '',
    'properties' => [
        ['id' => 1],
        ['id' => 2],
        ['id' => 3],
    ],
    'items' => [
        12 => ['id' => 12],
        112 => ['id' => 112],
        122 => ['id' => 122]
    ],
    'property_code' => [
        'brand' => [
            'id' => 1,
            'name' => 'brand',
            'values' => [
                'ikea',
                'nokia'
            ]
        ],
        'article' => [
            'id' => 2,
            'name' => 'article',
            'value' => '12333.54'
        ],
        'cml2links' => [
            'id' => 2,
            'name' => 'cml2links',
            'value' => [
                1233,
                5468
            ],
            'display_property' => [
                1233 => [
                    'id' => 1233,
                    'name' => 'item 1'
                ],
                5468 => [
                    'id' => 5468,
                    'name' => 'item 2'
                ],
            ]
        ]
    ]
]);

test_assert($c instanceof LexerCollection, '$c instanceof LexerCollection');
test_assert($c->lexer('name') == 'name', '$c->lexer("name") == "name"');
test_assert($c->lexer("code") === "", '$c->lexer("code") == ""');
test_assert($c->lexer("no_field") === null, '$c->lexer("no_field") == null');
test_assert($c->lexer("no_field", "def_value") == "def_value", '$c->lexer("no_field") == def_value');
test_assert($c->lexer("properties.0") instanceof LexerCollection, '$c->lexer("properties.0") instanceof LexerCollection::class');
test_assert($c->lexer("properties.0.id") == 1, '$c->lexer("properties.0.id") == 1');
test_assert($c->lexer("properties.1") instanceof LexerCollection, '$c->lexer("properties.1") instanceof LexerCollection::class');
test_assert($c->lexer("items.12.id") == 12, '$c->lexer("items.12.id") == 12');
test_assert($c->lexer("property_code.brand.name") == 'brand', '$c->lexer("property_code.brand.name") == "brand"');
test_assert($c->lexer("property_code.brand.code") === null, '$c->lexer("property_code.brand.code") === null');
test_assert($c->lexer("property_code.brand.values")->toArray() == ["ikea", "nokia"], '$c->lexer("property_code.brand.values")->toArray() == ["ikea", "nokia"]');
test_assert($c->lexer("property_code.brand.values.0") == "ikea", '$c->lexer("property_code.brand.values.0") == "ikea"');
test_assert($c->lexer("property_code.cml2links.value") instanceof LexerCollection, '$c->lexer("property_code.cml2links.value") instanceof LexerCollection');
test_assert($c->lexer("property_code.cml2links.value")->toArray() == [1233, 5468], '$c->lexer("property_code.cml2links.value")->toArray() == [1233, 5468]');
test_assert($c->lexer("property_code.cml2links.values") === null, '$c->lexer("property_code.cml2links.values") === null');
test_assert($c->lexer("property_code.cml2links.values", new LexerCollection()) instanceof LexerCollection, '$c->lexer("property_code.cml2links.values", new LexerCollection()) instanceof LexerCollection');
test_assert($c->lexer("property_code.cml2link.values", '') === '', '"property_code.cml2link.values", "") === ""');
test_assert($c->lexer("property_code.cml2links.display_property.5468.name") == "item 2", '$c->lexer("property_code.cml2links.display_property.5468.name") == "item 2"');



