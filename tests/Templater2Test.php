<?php


use Wt\Core\Templater\Templater;
use Wt\Core\Templater\TTemplater;
use Wt\Core\Unit\UnitTestCase;


class Templater2Test extends UnitTestCase
{

    use TTemplater;

    public function render1($text)
    {
        ?>
        <p><?=$text?></p>
        <?
    }

    public function testContext()
    {
        $templater = $this->templater();

        $this->assertSame('Nikolai', $templater->getTemplate('name'), 'get simple template');

        $this->assertSame(htmlspecialchars('<p>Super</p>'), htmlspecialchars(trim($templater->getCallTemplateOb('render1', ['Super']))), 'get Call template 1');
    }
}