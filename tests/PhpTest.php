<?php


use Wt\Core\Unit\UnitTestCase;


class PhpTest extends UnitTestCase
{
    public function testCorePhp()
    {
        $this->assertSame(gettype(null), 'NULL','gettype(null) === \'NULL\'');
        $this->assertSame((array)null, [], '(array)null === []');
        $this->assertSame((array)0, [0], '(array)0 === [0]');
        $this->assertSame((string)null, '', '(string)null === \'\'');
        $this->assertSame(['' => 1], ['' => 1], '[\'\' => 1] === [\'\' => 1]');
        $this->assertSame([null => 1], ['' => 1], '[null => 1] === [\'\' => 1]');
    }

}