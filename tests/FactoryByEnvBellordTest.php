<?php

use Wt\Core\Factory\IBlock\PropertyValueFactory;
use Wt\Core\Factory\IBlock\PropertyFactory;
use Wt\Core\Factory\Shop\CatalogFactory;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Unit\UnitTestCase;

/**
 * 
 */
class FactoryByEnvBellordTest extends UnitTestCase
{
    public function checkEnv()
    {
        return app()->env() === 'bellord';
    }


    public function testIBlock()
    {
        if (!$this->checkEnv()) {
            return;
        }

    }

    public function testIBlockElementPropertyCollection()
    {
        if(!$this->checkEnv()){
            return;
        }


        /**
         * обязательно так
         */
        (app()->factory()->catalog()->getProductFactory()
            ->getEntityById(66)->
            getOffersCollection()->first()->getProductCatalogEntity()->getRaw());
        app()->factory()->catalog()->getProductFactory()
            ->getEntityById(66)
            ->getOffersCollection()
            ->first()
            ->getProductCatalogEntity()
            ->getMeasureEntity()
            ->fields()
            ->symbolRus;

        $propertyValueCollection = app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection();
        $this->assertSame('E', $propertyValueCollection->get(28)->getPropertyEntity()->getBaseType(), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->getPropertyEntity()->isBaseType('E'), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->getPropertyEntity()->isBaseType(['E']), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->getPropertyEntity()->isBaseType(['E', 'L']), 'property type');
        $this->assertSame('E', $propertyValueCollection->get(28)->getBaseType(), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->isBaseType('E'), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->isBaseType(['E']), 'property type');
        $this->assertSame(true, $propertyValueCollection->get(28)->isBaseType(['E', 'L']), 'property type');

        $this->assertSame(10, app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getFactory()->getBlockId(), 'проверка на переопределение factoryInstance в абстрактной фабрике');
        /**
         * нужна проверка количества запросов к БД
         */
        $this->assertSame('2 месяца', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('WARRANTY')->fields()->value, 'getByCode field value');
        $this->assertSame('2 месяца', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('WARRANTY')->fields()->value, 'getByCode field value');


        $x = new PropertyValueFactory(10, 66);
        $x = $x->getCollection();
        $this->assertSame(['2 месяца'], $x->getByCode('WARRANTY')->fields()->value, 'getByCode field value');

    }

    public function testIBlockProperty()
    {
        if(!$this->checkEnv()){
            return;
        }

        $x = new \Wt\Core\Factory\IBlock\PropertyFactory(10);
        $this->assertSame('Производитель', $x->getEntityById(28)->fields()->name, 'Property name');
        $this->assertSame('В наличии', $x->getEntityByCode('CML2_AVAILABLE')->fields()->name, 'Property name');
        $this->assertSame(true, $x->getEntityByCode('NAZNAZHENIE')->isMultiple(), 'isMultiple');
        $this->assertSame(false, $x->getEntityByCode('ATT_BRAND')->isMultiple(), 'isMultiple');


        $x = new \Wt\Core\Factory\IBlock\PropertyFactory(10);
        $x = $x->getCollection();
        $this->assertSame('27', $x->first()->fields()->id, 'collection first id');


        $m = app()->factory()->iBlock()->getEntityById(10);
        /**
         * @var $ibpcf PropertyFactory
         */
        $ibpcf = $m->getFactoryInstance(PropertyFactory::class, [10]);
        $ibpce = $ibpcf->getCollection();
        $this->assertSame('Суммарная мощность, Вт.', $ibpce->getByCode('TOTAL_OUTPUT_POWER')->fields()->name, 'getByCode');
        $this->assertSame('Назначение', $ibpce->getByXmlId(229)->fields()->name, 'getByCode');
    }

	public function testCatalogFactoryModal()
	{
	    if(!$this->checkEnv()){
	        return;
        }

		$this->assertSame(10, app()->factory()->catalog()->getBlockId(), 'IBlock must by equal 10');
		$this->assertSame(app()->factory()->catalog()->getSectionFactory() instanceof IFactory, true, 'getSectionFactory must by instanceof IFactory');
		$this->assertSame(app()->factory()->catalog()->getElementFactory() instanceof IFactory, true, 'getElementFactory must by instanceof IFactory');
		$this->assertSame(app()->factory()->catalog()->getProductFactory() instanceof IFactory, true, 'getProductFactory must by instanceof IFactory');
	}

	public function testCatalogProductFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(50);

		$this->assertSame($cpe->fields()->name, 'Часы Armani Gold!!!', 'Check field name');



        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);
		$this->assertSame($cpe->getUrl(), '/catalog/odezhda/aksessuary/vya.html', 'getUrl');
		$this->assertSame($cpe->getAdminUrl(), '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=10&type=catalog&ID=95&lang=ru&from_module=iblock&find_section_section=11', 'getAdminUrl');
	}

	public function testCatalogProductCatalogFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(50);

		$this->assertSame((int)$cpe->getProductCatalogEntity()->fields()->quantity, 10, 'quantity');
	}

	public function testCatalogProductStoreFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);
        $this->assertSame((int)$cpe->getProductStoreCollection()->first()->fields()->storeId, 1, 'storeId');


        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);
        $psce = $cpe->getProductStoreCollection();
        $this->assertSame(9998, (int)$psce->first()->fields()->amount, 'amount');
        $this->assertSame(2, (int)$psce->count(), 'collection count');
        $this->assertSame(10098, (int)$psce->getTotalAmount(), 'collection getTotalAmount');

	}

	public function testCatalogProductPriceCollectionFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);

        $ppce = $cpe->getProductPriceCollection();

        $this->assertSame((float)$ppce->first()->fields()->price, 1000.00, 'collection first price');
        $this->assertSame((int)$ppce->count(), 8, 'collection count');
        $this->assertSame($ppce->first()->getPriceTypeEntity()->getRaw()['NAME'], 'BASE', 'collection count');
	}

	public function testCatalogGroupCollectionFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);

        $ppce = $cpe->getProductPriceCollection();
        $this->assertSame($ppce->first()->getPriceTypeEntity()->getRaw()['NAME'], 'BASE', 'field name');
        $this->assertSame($ppce->first()->getPriceTypeEntity()->fields()->base, 'Y', 'field base');
        $this->assertSame($ppce->first()->getPriceTypeEntity()->isBase(), true, 'field isBase');

	}

	public function testCatalogVatFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);

        $cve = $cpe->getProductCatalogEntity()->getVatEntity();
        $this->assertSame(2, (int)$cve->fields()->id, 'field id');
        $this->assertSame('НДС 18%', $cve->fields()->name, 'field name');
        $this->assertSame(18.0, (float)$cve->fields()->rate, 'field rate');

	}

	public function testMeasureFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);

        $cme = $cpe->getProductCatalogEntity()->getMeasureEntity();
        $this->assertSame('Литр', $cme->fields()->measureTitle, 'field measureTitle');

	}

	public function testSectionFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $cpf = app()->factory()->catalog()->getProductFactory();
        $cpe = $cpf->getEntityById(95);

        $this->assertSame('aksessuary', $cpe->getSectionEntity()->fields()->code, 'field code');

	}

	public function testUserFactory()
	{
	    if(!$this->checkEnv()){
	        return;
        }

        $mainAF = new \Wt\Core\AbstractFactory\MainAbstractFactory(new \Wt\Core\Support\LexerCollection());
        $uf = $mainAF->getUserFactory();
        $ue = $uf->getEntityById(1);

        $this->assertSame(true, $ue->isAdmin(),  'isAdmin');
        $this->assertSame('A Bellord', $ue->getFullName(),  'getFullName');



        $ue = app()->factory()->main()->getUserFactory()->getEntityById(3);

        $this->assertSame(false, $ue->isAdmin(),  'isAdmin');
        $this->assertSame('Sidire2815@chordmi.com', $ue->getFullName(),  'getFullName');
        $this->assertSame('+79682222211', $ue->getPhoneAuthNumber(),  'getPhoneAuthNumber');

	}

	public function testIblockModel()
    {
        if(!$this->checkEnv()){
            return;
        }

        $iBlockManualFactory = new \Wt\Core\ManualFactory\IBlockManualFactory(app()->config('IBlockAbstractFactory', []));

        $this->assertSame('RUB', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductPriceCollection()->first()->fields()->currency, 'Check currency');
        $this->assertSame('RUB', (new \Wt\Core\Factory\Shop\ProductFactory(11))->cache(['hit'])->getEntityById(74)->getProductPriceCollection()->first()->getCurrencyId(), 'Check currency');
        $this->assertSame('RUB',(new \Wt\Core\Factory\Shop\ProductPriceFactory(11, 74))->cache(['hit'])->getCollection()->first()->getCurrencyId(),'Check currency');

        $this->assertSame('BASE', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductPriceCollection()->first()->getPriceTypeEntity()->fields()->name, 'test');
        $this->assertSame('12', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductCatalogEntity()->getQuantity(), 'quantity');

        $this->assertSame('11', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->fields()->iblockId, 'iblockId');


        $this->assertSame('НДС 18%', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductCatalogEntity()->getVatEntity()->fields()->name, 'CatalogVat name');
        $this->assertSame('м', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductCatalogEntity()->getMeasureEntity()->fields()->symbolRus, 'Measure symbolRus');
        $this->assertSame('200', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductStoreCollection()->first()->fields()->amount, 'amount');
        $this->assertSame('склад', $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->getProductStoreCollection()->first()->getStoreEntity()->fields()->title, 'Store title');
        $this->assertSame('Мужская обувь', $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getSectionEntity()->fields()->name, 'Product Section name');
        $this->assertSame('/catalog/odezhda/muzhskaya-odezhda/muzhskaya-obuv/', $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getSectionEntity()->getUrl(), 'Product Section url');
        $this->assertSame('Одежда', $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getSectionEntity()->getRoot()->fields()->name, 'Product Root Section name');
        $this->assertSame('s1', collect($iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getIBlockEntity()->getSites())->last(), 'IBlock lid ');
        $this->assertSame('Товары DELUXE', app()->factory()->main()->getIBlockFactory()->getEntityById(10)->fields()->name, 'Iblock name');
        $this->assertSame('X', app()->factory()->main()->getIBlockFactory()->getEntityById(10)->getCatalogEntity()->fields()->catalogType, 'Iblock Catalog catalogType');
        $this->assertSame(true, app()->factory()->main()->getIBlockFactory()->getEntityById(10)->getCatalogEntity()->canContainOffers(), 'Catalog canContainOffers');
        $this->assertSame(true, $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->canContainOffers(), 'Product canContainOffers');
        $this->assertSame(false, $iBlockManualFactory->getEntityById(11)->getProductFactory()->getEntityById(74)->canContainOffers(), 'Product canContainOffers');
        $this->assertSame('[74,75,76,77,78,79]', collect($iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getOffersIds())->toJson(), 'getOffersIds');
        $this->assertSame(6, $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getOffersCollection()->count(), 'OffersCollection count');
        $this->assertSame(' СНИКЕРЫ LINE UP 40', $iBlockManualFactory->getEntityById(10)->getProductFactory()->getEntityById(66)->getOffersCollection()->first()->fields()->name, 'OffersCollection first name');



        $iBlockFactory = new \Wt\Core\Factory\IBlockFactory();
        $this->assertSame('s2',collect( $iBlockFactory->cache(['hit'])->getEntityById(7)->getSites())->last(), 'IBlock lid last');

    }

    public function testUserAbstractFactory()
    {
        if(!$this->checkEnv()){
            return;
        }

        $maf = new Wt\Core\AbstractFactory\MainAbstractFactory(app()->config('MainAbstractFactory', []));

        $this->assertSame('A Bellord', $maf->getUserFactory()->getEntityById(1)->getFullName(), 'getFullName');
        $this->assertSame('+79681111111', $maf->getUserFactory()->getEntityById(1)->getPhoneAuthNumber(), 'getPhoneAuthNumber');
        $this->assertSame(true, $maf->getUserFactory()->getEntityById(1)->inGroups(1), 'inGroups');
        $this->assertSame(true, $maf->getUserFactory()->getEntityById(1)->inGroups([2, 2, 1]), 'inGroups');
        $this->assertSame(false, $maf->getUserFactory()->getEntityById(1)->inGroups([2, 2, 1, 30]), 'inGroups');

    }


    public function testMeasureCollectionFactory()
    {
        if(!$this->checkEnv()){
            return;
        }

        $this->assertSame('м', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getOffersCollection()->first()->getProductCatalogEntity()->getMeasureEntity()->fields()->symbolRus, 'symbolRus');

    }

    public function testPropertyFactory()
    {
        if (!$this->checkEnv()) {
            return;
        }
        $this->assertSame('SEASON', app()->factory()->iBlock()->getEntityById(10)->getPropertyFactory()->getEntityByCode('SEASON')['CODE'], '');
    }

    public function testMultiSelect()
    {
        if(!$this->checkEnv()){
            return;
        }

        $this->assertSame('39', app()->factory()->iBlock()->getEntityById(11)->getProductFactory()->getEntityById(77)->getPropertyValueCollection()->getByCode('SIZE_SHOES')->fields()->value, 'multi select');
        $this->assertSame('Лето', app()->factory()->iBlock()->getEntityById(10)->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SEASON')->fields()->value, 'multi select');
        $this->assertSame('48', app()->factory()->iBlock()->getEntityById(11)->getProductFactory()->getEntityById(77)->getPropertyValueCollection()->getByCode('SIZE_CLOTHES')->fields()->value, 'multi select');
        $this->assertSame('Франция', app()->factory()->iBlock()->getEntityById(10)->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('COUNTRY_BRAND')->fields()->value, 'multi select');

        $this->assertSame('Лето', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByIds([34, 35, 44])->getByCode('SEASON')->getDisplayValue(), 'multi select');

    }

    public function testProperty()
    {
        if(!$this->checkEnv()){
            return;
        }

        $catalog = app()->factory()->iBlock()->getEntityById(10);
        $element = $catalog->getProductFactory()->getEntityById(66);
        $properties = $element->getPropertyValueCollection();

        $brand = $properties->getByCode('VIDEO');
        $this->assertSame(4, collect($brand->getValue())->count(), 'E+ Display');

        $brand = $properties->getByCode('ATT_BRAND');

        $this->assertSame('D&G', $brand->getDisplayValue(), 'E Display');


        $catalog = app()->factory()->iBlock()->getEntityById(11);
        $element = $catalog->getProductFactory()->getEntityById(77);

        $properties = $element->getPropertyValueCollection();

        $this->assertSame('43', $properties->getByCode('SIZE_SHOES')->getDisplayValue(), 'prop type L getDisplayValue');
        $this->assertSame('54', $properties->getByCode('SIZE_CLOTHES')->getDisplayValue(), 'prop type L getDisplayValue');


        $this->assertSame('2 месяца', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('WARRANTY')->getValue(), 'S');
        $this->assertSame('{"255":"V1","256":"V2","257":"V3","258":"V4"}', collect(app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('VIDEO')->getValue())->toJson(), 'S+');
        $this->assertSame('14', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('PRODUCT_DAY')->getValue(), 'L');
        $this->assertSame('["53","10","13"]', collect(app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('OFFERS')->getValue())->values()->toJson(), 'L+');
        $this->assertSame([
            53 => 'Уценка -20%',
            10 => 'Хит продаж',
            13 => 'Уценка -10%',
        ], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('OFFERS')->getDisplayValue(), 'L+ Display');

        $this->assertSame('200', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTION')->getValue(), 'G');
        $propertyValueEntity = app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SHOW_SKU_TABLE');
        $this->assertSame([
            51 => 'да',
        ], $propertyValueEntity->getDisplayValue(), 'L+ Display');
        $this->assertSame('["51"]', collect($propertyValueEntity->getValue())->values()->toJson(), 'L+');
        $this->assertSame([
            187 => 'Кухонные плиты',
            188 => 'Стиральные машины',
        ], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTIONS')->getDisplayValue(), 'G+ Display');
        /**
         * @var $sectionEntity \Wt\Core\Entity\Iblock\SectionEntity
         */
        $sectionEntity = app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTION')->getValueEntity();
        $this->assertSame('Клавиатуры', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTION')->getDisplayValue(), 'G');
        $this->assertSame('Компьютеры', $sectionEntity->getRoot()->fields()->name, 'G Root Name');
        $this->assertSame(['187','188'], collect(app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTIONS')->getValue())->values()->all(), 'G+');
        $this->assertSame([
            187 => 'Кухонные плиты',
            188 => 'Стиральные машины',
        ], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SECTIONS')->getDisplayValue(), 'G+ Display');


        $this->assertSame('27', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('COLLECTION')->getValue(), 'E');
        $this->assertSame('For men', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('COLLECTION')->getDisplayValue(), 'E');

        $this->assertSame([265=>'60', 266=>'59',], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SIMILAR_PRODUCT')->getValue(), 'E+');
        $this->assertSame([
            60 => 'Косметичка Chanel',
            59 => 'Губная помада Chanel',
        ], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SIMILAR_PRODUCT')->getDisplayValue(), 'E+ Display 2');

        $this->assertSame([
            'NAME' => '63',
            'BIND' => 'супер рубаха',
        ], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('TABLE')->getValue(), 'S:gtable');
        $this->assertSame(
            [
                'NAME' => '63',
                'BIND' => 'супер рубаха',
            ],
            app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('TABLE')->fields()->value,
            'S:gtable 2'
        );
        $this->assertSame(array (
            0 =>
                array (
                    'NAME' => 'E1',
                    'ELEMENT' => '45',
                ),
            1 =>
                array (
                    'NAME' => 'E2',
                    'ELEMENT' => '47',
                ),
        ), collect(app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('MULTI_TABLE')->getValue())->values()->all(), 'S:gtable+');

        //F
        $this->assertSame('2111', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SERT')->getValue(), 'F');
        $this->assertSame('/upload/iblock/eaa/eaaa83ba1c077858e7c572ab39355d77.txt', app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('SERT')->getDisplayValue(), 'F Display');

        //F+
        $propValue = app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('DOCS');
        $this->assertSame(['2112', '2113'], collect($propValue->getValue())->values()->all(), 'F+');
        $this->assertSame([
            '/upload/iblock/e81/e819a1874e655795c9738b3bd5a975e6.txt',
            '/upload/iblock/353/353a4c11c62c6da5cd79ab18225b5426.txt',
        ], collect($propValue->getDisplayValue())->values()->all(), 'F+ Display');

        //NUM_MULTY
        //{"363":"5","364":"8.5","365":"9,3"}
        $this->assertSame('{"363":"5","364":"8.5","365":"9,3"}', collect(app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('NUM_MULTY')->fields()->value)->toJson(), 'N+');
        $this->assertSame([363=>'5', 364=>'8.5', 365=>'9,3'], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('NUM_MULTY')->getValue(), 'N+ 2');
        $this->assertSame([363=>5.0, 364=>8.5, 365=>9.3], app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('NUM_MULTY')->getDisplayValue(), 'N+ Display');
    }

    public function testFileEntity()
    {
        if(!$this->checkEnv()){
            return;
        }
        /** @var \Wt\Core\Entity\FileEntity $fileEntity */
        $fileEntity = app()->factory()->catalog()->getProductFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('MORE_PHOTO')->getValueEntity()->first();
        $this->assertSame(true, $fileEntity->isImage(), 'is image');
        $this->assertSame(2114, $fileEntity->getId(), 'get image id');
        $this->assertSame('png', $fileEntity->getExt(), 'get file ext');
        $this->assertSame('/upload/iblock/38f/38f0c6b364864252335fb129662df546.png', $fileEntity->getSrc(), 'get file src');
        $this->assertSame(115343, $fileEntity->getSize(), 'get file size');
        $this->assertSame('112.64 КБ', $fileEntity->getSizeFormat(), 'get file size');


    }

    public function testHlBlock()
    {
        if(!$this->checkEnv()){
            return;
        }
        $hlBlock = new \Wt\Core\Factory\HlBlockFactory();
        $this->assertSame(2, $hlBlock->getEntityByXmlId('b_hlbd_testspravochnik')->getId(), 'test hl');
        $this->assertSame(3, $hlBlock->getEntityByCode('TSVET')->getId(), 'test hl');
        $this->assertSame('b_chastota_protsessora', $hlBlock->getEntityById(5)->getTableName(), 'test hl');
    }

    public function testPropertyDirectory()
    {
        if(!$this->checkEnv()){
            return;
        }
        $directory = app()->factory()->catalog()->getElementFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('REF');

        $this->assertSame('первый', $directory->getDisplayValue(), '');
        $this->assertSame('первый', $directory->getValueEntity()->fields()->name, '');

        $directoryMultiple = app()->factory()->catalog()->getElementFactory()->getEntityById(66)->getPropertyValueCollection()->getByCode('MREF');

        $this->assertSame([386=>'NlDd32Ba', 387=>'AZK7JvY2'], $directoryMultiple->getValue(), '');
        $this->assertSame([1 => 'два', 2 => 'три'], $directoryMultiple->getDisplayValue(), '');
        $this->assertSame(2, $directoryMultiple->getValueEntity()->count(), '');
        $this->assertSame('три', $directoryMultiple->getValueEntity()->getByXmlId('AZK7JvY2')->fields()->name, '');

    }

    public function testHL()
    {
        if(!$this->checkEnv()){
            return;
        }
        $this->assertSame(4, app()->factory()->main()->getHlBlockFactory()->getEntityByXmlId('b_razmer')->getId(), '');
        $this->assertSame('XXS', app()->factory()->hlBlock()->getEntityByCode('RAZMER')
            ->getElementFactory()
            ->getEntityByXmlId('471689da-1a41-11e9-baf2-e0d55e7b67e6')->fields()->name, '');

        $this->assertSame(array ( 9 => 'XXS', 18 => 'XXXL', ), app()->factory()->hlBlock()->getEntityByCode('RAZMER')
            ->getElementFactory()
            ->getEntityByIds([
                9,
                18,
            ])->map(function (/** @var $entity \Wt\Core\Entity\HlBlock\ElementEntity */ $entity){
                return $entity->fields()->name;
            })->toArray(), '');


        $this->assertSame(24, app()->factory()->hlBlock()->getEntityByXmlId('b_razmer')->getElementFactory()->getCollection()->count(), 'count hl elements');
    }

    public function testCompany()
    {
        if(!$this->checkEnv()){
            return;
        }
        $this->assertSame('Компания Белая', app()->factory()->shop()->getCompanyFactory()->getEntityById(1)->fields()->name, '');
        $this->assertSame([
            'Компания Белая',
            'Компания Красная',
        ], app()->factory()->shop()->getCompanyFactory()->getEntityByIds([1, 2])->pluck('name')->all(), 'sort sale:company');
    }


    public function testCurrency()
    {
        if(!$this->checkEnv()){
            return;
        }
        $this->assertSame('840', app()->factory()->shop()->getCurrencyFactory()->getEntityById('USD')->fields()->numcode, '');
        $this->assertSame('RUB', app()->factory()->shop()->getCurrencyFactory()->getEntityById('RUB')->getId(), '');

        $this->assertSame([
            'BYN',
            'RUB',
            'USD',
        ], (new \Wt\Core\Factory\Shop\CurrencyFactory())->getEntityByIds([
            'USD',
            'BYN',
            'RUB',
        ])->pluck('ID')->all(), 'no cache sort');

        $this->assertSame([
            'USD',
            'BYN',
            'RUB',
        ], app()->factory()->shop()->getCurrencyFactory()->getEntityByIds([
            'USD',
            'BYN',
            'RUB',
        ])->pluck('ID')->all(), 'cache sort (если Error то возможно кеш конфиг не подрубился');
    }


    public function testSite()
    {
        if(!$this->checkEnv()){
            return;
        }

        $this->assertSame('DELUXE (Сайт по умолчанию)', app()->factory()->main()->getSiteFactory()->getEntityById('s1')->fields()->name, 'site s1 name');
        $this->assertSame('DELUXE (Сайт по умолчанию)', app()->factory()->main()->getSiteFactory()->getEntityByIds(['s1'])->first()->fields()->name, 'site s1 name');
        $this->assertSame('sotbit', app()->factory()->main()->getSiteFactory()->getEntityByIds(['s1', 's2'])->last()->fields()->name, 'site s2 name');
    }

    public function testCurrencyCollection()
    {
        if (!$this->checkEnv()) {
            return;
        }
        $currencyCollection = app()->factory()->shop()->getCurrencyFactory()->getCollection();
        $this->assertSame('933', $currencyCollection->getByCode('BYN')->fields()->numcode, 'collection test');

    }

    public function testHlProps()
    {
        if(!$this->checkEnv()){
            return;
        }

        $c = app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUserFieldValueCollection();

        $this->assertSame([
            'VALUE' =>
                [
                    1 => 'a:2:{s:2:"ID";s:1:"1";s:4:"NAME";s:3:"Car";}',
                    2 => 'a:2:{s:2:"ID";s:1:"2";s:4:"NAME";s:5:"Phone";}',
                ],
        ], $c
            ->getByCode('UF_TABLE_M')
            ->getRestoredValue(), 'gtable multiple: restored value');

        $this->assertSame([
            1 =>
                [
                    'ID' => '1',
                    'NAME' => 'Car',
                ],
            2 =>
                [
                    'ID' => '2',
                    'NAME' => 'Phone',
                ],
        ], $c
            ->getByCode('UF_TABLE_M')
            ->getValue(), 'gtable multiple: value');

        $this->assertSame([
            'NAME' => 'сайт',
            'LINK' => 'core.ru',
        ], $c
            ->getByCode('UF_TABLE')
            ->getValue(), 'gtable single: value');

        $this->assertSame('/upload/uf/284/284b13179737270fcbdf2ad711178df7.png',
            $c
            ->getByCode('UF_FILE')
            ->getDisplayValue(), 'file');

        $this->assertSame([
            5 => 'первый',
            7 => 'третий',
        ], app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_LIST')
            ->getDisplayValue(), 'list m');

        $this->assertSame('тест', app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_ELEMENT')
            ->getDisplayValue(), 'element');

        $this->assertSame([
            677 => 'Cтол обеденный "Гольс"',
            655 => 'Cтол обеденный "Малага" 160см',
            656 => 'Cтол обеденный "Малага" квадратный',
        ], app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_ELEMENT_M')
            ->getDisplayValue(), 'element m');

        $this->assertSame(array ( // id не вернула тк нет привязки к iblock, по сути кастулем получаем названия
            0 => 'Cтол обеденный "Вельс"',
            1 => 'Cтол обеденный "Гольс"',
        ), app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_ELEMENT_M_NO_IB')
            ->getDisplayValue(), 'element m без привязки');

        $this->assertSame('Женская одежда', app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_SECTION')
            ->getDisplayValue(), 'section');

        $this->assertSame(array (
            3 => 'Блузки',
            4 => 'Верхняя одежда',
        ), app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_SECTION_M')
            ->getDisplayValue(), 'section m');

        $this->assertSame(array ( // id не вернула тк нет привязки к iblock, по сути кастулем получаем названия
            0 => 'Женская одежда',
            1 => 'Блузки',
        ), app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_SECTION_M_NO_IB')
            ->getDisplayValue(), 'section m без привязки');

        $this->assertSame('XXXL', app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_HL_ELEMENT')
            ->getDisplayValue(), 'hl element');

        $this->assertSame(array (
            1 => 'L',
            4 => 'M',
            6 => 'S',
            8 => 'XS',
            9 => 'XXS',
            20 => 'XL',
        ), app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_HL_ELEMENT_M')
            ->getDisplayValue(), 'hl element m');

        $this->assertSame(array (
            8 => 'minsk',
            9 => 'Брест',
        ), app()->factory()->hlBlock()
            ->getEntityById(8)
            ->getElementFactory()
            ->getEntityById(1)
            ->getUfValueCollection()
            ->getByCode('UF_LIST_M')
            ->getDisplayValue(), 'ListM [UF_LIST_M]');
    }


    public function testSectionChain()
    {
        if (!$this->checkEnv()) {
            return;
        }

        $sectionCollection = app()->factory()->iBlock()->getEntityById(10)->getElementFactory()->getEntityByCode('chasy-armani')->getSectionChain();
        $this->assertSame([
            'custom_label_0' => 'Одежда',
            'custom_label_1' => 'Часы',
        ], $sectionCollection->pluck('NAME')->mapWithKeys(function($item, $key){
            return ["custom_label_{$key}" => $item];
        })->toArray(), 'getSectionChain');


        $this->assertSame([
            'custom_label_0' => 'Одежда',
            'custom_label_1' => 'Часы',
            'custom_label_2' => '',
            'custom_label_3' => '',
            'custom_label_4' => '',
        ], $sectionCollection->pluck('NAME')->pad(5, '')->mapWithKeys(function($item, $key){
            return ["custom_label_{$key}" => $item];
        })->toArray(), 'getSectionChain');

    }

    public function testSection()
    {
        if (!$this->checkEnv()) {
            return;
        }

        $iblock = app()->factory()->main()->getIBlockFactory()->getEntityById(10);

        $this->assertSame(true, $iblock->isSectionProperty(), 'section property');


//        dd(CIBlockSectionPropertyLink::GetArray(10, 224));

//        app()->factory()->main()->getIBlockFactory()->getEntityById(10)->isSectionProperty()

    }

    public function testIBlockPropertyEnum()
    {
        if (!$this->checkEnv()) {
            return;
        }

    }


    public function testUserGroup()
    {
        if (!$this->checkEnv()) {
            return;
        }

        $this->assertSame('Администраторы', app()->factory()->main()->getUserGroupFactory()->getCollection()->first()->name, 'user group');
    }

    public function testIBlockTypeFactory()
    {
        if (!$this->checkEnv()) {
            return;
        }

        $this->assertSame('1c_catalog', app()->factory()->main()->getIBlockTypeFactory()->getCollection()->first()->id, 'IBlockType');
    }

    public function testIBInheritedCustomProperty()
    {
        if (!$this->checkEnv()) {
            return;
        }

        $iblock = app()->factory()->iBlock()->getEntityById(13);

        $element = $iblock->getElementFactory()->getEntityById(142);
        $iProp = $element->getPropertyValueCollection()->getByCode('INH_PROP');


        $this->assertSame('Часы Vercage Gold й123 Часы', $iProp->getDisplayValue(), 'S:IBlockTemplate getDisplayValue');



        $iProp = $element->getPropertyValueCollection()->getByCode('ELEMENT_META_TEMPLATE_LIST');
        $this->assertSame([
            'ELEMENT_META_TITLE' => '{=this.Name}',
            'ELEMENT_META_KEYWORDS' => '{=this.Code}',
            'ELEMENT_META_DESCRIPTION' => '{=parent.Name}',
            'ELEMENT_PAGE_TITLE' => '{=lower this.Name} {=this.property.STRING_PROPERTY}',
        ], $iProp->getValue(), 'S:IBlockElementMetaTemplateList getValue');

        $this->assertSame([
            'ELEMENT_META_TITLE' => 'Часы Vercage Gold',
            'ELEMENT_META_KEYWORDS' => 'chasy-versace',
            'ELEMENT_META_DESCRIPTION' => 'Часы',
            'ELEMENT_PAGE_TITLE' => 'часы vercage gold й123',
        ], $iProp->getDisplayValue(), 'S:IBlockElementMetaTemplateList DisplayValue');

        $iProp = $element->getPropertyValueCollection()->getByCode('SECTION_META_TEMPLATE_LIST');
        $this->assertSame([
            'SECTION_META_TITLE' => '{=lower this.Name}',
            'SECTION_META_KEYWORDS' => '{=this.Code}',
            'SECTION_META_DESCRIPTION' => '{=iblock.Name}',
            'SECTION_PAGE_TITLE' => '{=concat this.sections.name this.name " / "}',
        ], $iProp->getValue(), 'S:IBlockElementMetaTemplateList getValue');

        $this->assertSame([
            'SECTION_META_TITLE' => 'часы vercage gold',
            'SECTION_META_KEYWORDS' => 'chasy-versace',
            'SECTION_META_DESCRIPTION' => 'test',
            'SECTION_PAGE_TITLE' => 'Одежда / Часы / Часы Vercage Gold',
        ], $iProp->getDisplayValue(), 'S:IBlockElementMetaTemplateList DisplayValue');

        $iProp = $element->getPropertyValueCollection()->getByCode('PHONES');

        $this->assertSame([
            '{"ICON":"\/bitrix\/images\/wt.core\/iconset\/header_phones\/mts.svg","PHONE":"+7111","HREF":"tel:+7111"}',
            '{"ICON":"\/bitrix\/images\/wt.core\/iconset\/header_phones\/a1.svg","PHONE":"+7222","HREF":"tel:+7222"}',
        ], collect($iProp->getValue())->values()->map('json_encode')->all(), 'S:Phone getValue');
        $this->assertSame([
            [
                'ICON' => '/bitrix/images/wt.core/iconset/header_phones/mts.svg',
                'PHONE' => '+7111',
                'HREF' => 'tel:+7111',
            ],
            [
                'ICON' => '/bitrix/images/wt.core/iconset/header_phones/a1.svg',
                'PHONE' => '+7222',
                'HREF' => 'tel:+7222',
            ],
        ], collect($iProp->getDisplayValue())->values()->all(), 'S:Phone getDisplayValue');

    }
}