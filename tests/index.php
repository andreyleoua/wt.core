<?php
define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);
define('NOT_CHECK_PERMISSIONS',true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('wt.core');

ini_set('force_display_errors', 'On');
ini_set('default_enable', 'Off');
error_reporting(E_ALL^E_NOTICE);



$unitTestSuite = new \Wt\Core\Unit\UnitTestSuite();
$unitTestSuite->loadByDir(__DIR__);
$unitTestSuite->run();
$unitTestSuite->render();
