<?php

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Wt\Core\Services\ArrayCache;

\Bitrix\Main\Loader::includeModule('wt.core');

$ac = new Wt\Core\Services\ArrayCache;

test_assert($ac->get("test") === null, '$ac->get("test") === null');
test_assert($ac->get("test", "def") === "def", '$ac->get("test") === "def"');
test_assert($ac->has("test") === false, '$ac->has("test") === false');

$ac->put('test', 'test');

test_assert($ac->get("test", "def") === "test", '$ac->get("test") === "test"');
test_assert($ac->has("test") === true, '$ac->has("test") === true');
$ac->forget('test');
test_assert($ac->has("test") === false, '$ac->has("test") === false');
$ac->put('test', null);
test_assert($ac->has("test") === true, '$ac->has("test") === true');
test_assert($ac->get("test", "def") === "def", '$ac->get("test", "def") === "def"');
$ac->forget('test');
$ac->remember('test', 0, 'data');
test_assert($ac->get("test") === "data", '$ac->get("test") === "data"');
$ac->remember('test', 0, 'data1');
test_assert($ac->get("test") === "data", '$ac->get("test") === "data"');
$ac->forget('test');
$ac->remember('test', 0, function(){ return 'data1'; });
test_assert($ac->get("test") === "data1", '$ac->get("test") === "data1"');