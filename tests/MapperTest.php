<?php


use Wt\Core\Support\Mapper;

class MapperTest extends \Wt\Core\Unit\UnitTestCase
{
    public function testMapper()
    {
        $m = new Mapper();

        $this->assertInstanceOf(Mapper::class, $m,  "Make new pammer class");
        $this->assertEquals($m->get('test'), null, 'Get map without default');
        $this->assertEquals($m->get('test', 'default'), 'default', 'Get map use default param');

        $m->add('test', 'test_val');
        $this->assertEquals($m->get('test'), 'test_val', 'Get map use by key');
        $this->assertEquals($m->getByValue('test_val'), 'test', 'Get map use by value');
        $m->add('1', '11');
        $this->assertEquals($m->keys(), ['test', '1'], 'Get map keys');
        $this->assertEquals($m->values(), ['test_val', '11'], 'Get map values');
        $this->assertEquals($m->toArray(), ['test' => 'test_val', '1' => '11'], 'Get map to array');
        $this->assertEquals($m->toArrayFill(), ['test_val' => 'test', '11' => '1'], 'Get map to fill array');
        $m->remove('test');
        $this->assertEquals($m->get('test'), null, 'Get map remove key');
        $m->fromArray([2 => 22, 1 => 111]);
        $this->assertEquals($m->toArray(), [2 => 22, 1 => 111], 'Make from array');
        $m->extendFromArray([3 => 33]);
        $this->assertEquals($m->toArray(), [2 => 22, 1 => 111, 3 => 33], 'Extend from array');
        $m->clear();
        $this->assertEquals($m->toArray(), [], 'Clear mapper');
    }
}