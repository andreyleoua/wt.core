<?php
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


\Bitrix\Main\Loader::includeModule('wt.core');


//test_assert(app() instanceof \Wt\Core\App\App, 'app() == \Wt\Core\App\App');
//test_assert(app('factory') instanceof \Wt\Core\App\AbstractFactory, 'app("factory") == \Wt\Core\App\AbstractFactory');
//test_assert(app('no_service') === null , "app('no_service') === null");
//test_assert(app()->factory() instanceof \Wt\Core\App\AbstractFactory, 'app()->factory() == \Wt\Core\App\AbstractFactory');
//test_assert(app()->factory() === app('factory'), 'app()->factory() === app("factory")');
//test_assert(app('service') instanceof \Wt\Core\App\AbstractService, 'app("service") == \Wt\Core\App\AbstractService');
//test_assert(app()->service() instanceof \Wt\Core\App\AbstractService, 'app()->service() == \Wt\Core\App\AbstractService');
//test_assert(app('factory:product') instanceof \Wt\Core\Factory\ProductFactory, 'app(\'factory:product\') == \Wt\Core\App\Factory\ProductFactory');
//test_assert(app()->factory('product') instanceof \Wt\Core\Factory\ProductFactory, 'app()->factory("product") == \Wt\Core\App\Factory\ProductFactory');
//test_assert(app()->factory()->product() instanceof \Wt\Core\Factory\ProductFactory, 'app()->factory()->product() == \Wt\Core\App\Factory\ProductFactory');
//test_assert(factory('product') instanceof \Wt\Core\Factory\ProductFactory, "factory('product') instanceof \Wt\Core\Factory\ProductFactory");
//test_assert(app('factory:product:1') instanceof \Wt\Core\Services\Product, 'app(\'factory:product:1\') == \Wt\Core\App\Services\Product');
//test_assert(app()->factory('product')->getEntityById(1) instanceof \Wt\Core\Services\Product, 'app()->factory("product")->getEntityById(1) == \Wt\Core\App\Services\Product');
//test_assert(app()->factory()->product()->getEntityById(1) instanceof \Wt\Core\Services\Product, 'app()->factory()->product()->getEntityById(1) == \Wt\Core\App\Services\Product');
//test_assert(app('factory:product:1') !== app()->factory()->product()->getEntityById(1), 'app(\'factory:product:1\') !== app()->factory()->product()->getEntityById(1)');
//test_assert(app('factory:product:2') != app()->factory()->product()->getEntityById(1), 'app(\'factory:product:2\') !== app()->factory()->product()->getEntityById(1)');
//test_assert(app('factory:product:getEntityById', 2)->getData() == 2, "app('factory:product:getEntityById', 2)->getData() == 2");
//test_assert(app('factory:product:2')->getData() == 2, "app('factory:product:2')->getData() == 2");
//test_assert(app()->factory()->product()->getEntityById(2)->getData() == 2, "app()->factory()->product()->getEntityById(2) == 2");
//test_assert(app('collection') instanceof \Wt\Core\Services\Collection, "app('collection') instanceof \Wt\Core\Services\Collection");
//test_assert(app('service:collection') instanceof \Wt\Core\Services\Collection, "app('service:collection') instanceof \Wt\Core\Services\Collection");
//test_assert(app('collection', [1, 2, 3]) == new \Wt\Core\Services\Collection([1, 2, 3]), "app('collection', [1, 2, 3]) == new \Wt\Core\Services\Collection([1, 2, 3])");
//test_assert(app('factory:product:2:getData') == 2, "app('factory:product:2:getData') == 2");
//test_assert(app('factory:product:2:setParams', 2) == 2, "app('factory:product:2:setParams', 2) == 2");
//test_assert(app('factory:product:2:labels:count') == 3, "app('factory:product:2:labels:count') == 3");
//test_assert(app()->factory()->product()->getEntityById(2)->labels()->count() == 3, "app()->factory()->product()->getEntityById(2)->labels()->count() == 3");
//test_assert(app('objectPool') instanceof Wt\Core\App\ObjectPool, "app('objectPool') instanceof Wt\Core\App\ObjectPool");
//test_assert(app('appLexer') instanceof \Wt\Core\App\AppLexer, "app('appLexer') instanceof \Wt\Core\App\AppLexer");
//test_assert(app('c') instanceof \Wt\Core\Services\Collection, "app('c') instanceof \Wt\Core\Services\Collection");
//test_assert(app('service:c') instanceof \Wt\Core\Services\Collection, "app('service:c') instanceof \Wt\Core\Services\Collection");
//test_assert(app()->service('c') instanceof \Wt\Core\Services\Collection, "app()->service('c') instanceof \Wt\Core\Services\Collection");
//test_assert(app('pf:2') instanceof \Wt\Core\Services\Product, "app('pf:2') instanceof \Wt\Core\Services\Product");
//test_assert(app('pf:2:name') == 'test product', "app('pf:2:name') == 'test product'");
//test_assert(app('pf:2:images') instanceof \Wt\Core\Services\Collection, "app('pf:2:images') instanceof \Wt\Core\Services\Collection");
//test_assert(app('pf:2:pic:src') === '/test.jpg', "app('pf:2:pic:src') === 'test.jpg'");
//test_assert(app()->factory()->product()->getEntityById(2)->images->first()->src === '/test.jpg', "app()->factory()->product()->getEntityById(2)->images->first()->src === '/test.jpg'");
//test_assert(app('pf:2:images:first:src') === '/test.jpg', "app('pf:2:images:first:src') === '/test.jpg'");
//test_assert(app('pf:2')->lexer('images:first:src') === '/test.jpg', "app('pf:2')->lexer('images:first:src') === '/test.jpg'");
//test_assert(app('getProductById:2') instanceof \Wt\Core\Services\Product, "app('getProductById:2') instanceof \Wt\Core\Services\Product");
//test_assert(app('getProductByIds', [1, 2]) instanceof \Wt\Core\Services\Collection, "app('getProductByIds',[1,2]) instanceof \Wt\Core\Services\Product");
//test_assert(app('pf', 2) instanceof \Wt\Core\Services\Product, "app('pf', 2) instanceof \Wt\Core\Services\Product");
//test_assert(app('pf', [1, 2]) instanceof \Wt\Core\Services\Collection, "app('pf',[1,2]) instanceof \Wt\Core\Services\Product");


//app('appLexer')->setDelimiter('.');
//test_assert(app('factory.product.2.labels.count') == 3, "app('factory.product.2.labels.count') == 3");



/*$myCollect = app('lc', ['l1' => app('lc', ['l1-2'=> app('lc', ['l1-2-3' => 'level 3'])])]);

$myLexer = new \Wt\Core\App\AppLexer();
$myLexer->setDelimiter(' -> ');
$result = $myLexer->callNextStr('l1 -> l1-2 -> l1-2-3', $myCollect, []);
test_assert($result === 'level 3', "custom lexer('l1 -> l1-2 -> l1-2-3') === 'level 3'");*/
