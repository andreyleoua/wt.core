<?php
/**
 * Created by PhpStorm.
 * Company: ittown.by
 * Project: module.webteam.by
 * User: Andrey Leo
 * Date: 5/28/21
 * Time: 2:14 AM
 * All rights reserved
 */

use Wt\Core\App\ServiceContainer;
use Wt\Core\Unit\UnitTestCase;

include_once __DIR__ . '/ServiceContainerTest/resource.php';

class ServiceContainerTest extends UnitTestCase
{
    public function testReadPropertyAsPsr()
    {
        $c = new ServiceContainer();
        $this->assertInstanceOf(ServiceContainer::class, $c, 'ServiceContainer');

        try{
            $this->assertInstanceOf(\App\NoUser::class, $c->make(\App\NoUser::class), '$c->make(\App\NoUser::class) myst be exception');
        } catch(\Exception $e){
            $this->assertTrue(true, '$c->make(\App\NoUser::class) myst be exception');
        }

        $this->assertInstanceOf(User::class, $c->make(User::class),  'User::class instanceof \Wt\Core\Services\Collection::class no container');
        $this->assertNotSame($c->make(User::class), $c->make(User::class), 'User::class no container');

        $c->bind(User::class);
        $this->assertTrue($c->has(User::class), 'User in container');
        $this->assertTrue($c->has(UserB::class), 'UserB::class not in container but allow instantiate real class');
        $this->assertFalse($c->has(UserSingle::class), '$c->has(UserSingle::class) not in container but real class not allow instantiate!');
        $this->assertFalse($c->has(App\UserNo::class), '!$c->has(App\UserNo::class) not in container and not real class');

        $c->bind("App\MyPseudoUser", User::class);
        $this->assertTrue($c->has("App\MyPseudoUser"), '$c->has("App\MyPseudoUser") in container');
        $this->assertInstanceOf( User::class, $c->make("App\MyPseudoUser"),'$c->make("App\MyPseudoUser") instanceof App\User');

        $c->bind("MyUser", User::class);
        $this->assertTrue($c->has("MyUser"), '$c->has("MyUser") small name');
        $this->assertInstanceOf(User::class, $c->make("MyUser"), '$c->make("MyUser") instanceof \Wt\Core\Services\Collection::class');

        $c->bind(UserB::class);
        try{
            $this->assertTrue($c->make(UserB::class), '$c->make(UserB::class) must be arguments');
        } catch(\Error $e) {
            $this->assertTrue(true, '$c->make(App\UserB::class) must be arguments');
        } catch(\Exception $e) {
            $this->assertTrue(true, '$c->make(App\UserB::class) must be arguments');
        }

        $this->assertInstanceOf(UserB::class, $c->make(UserB::class, ["name" => "name"]), '$c->make("App\UserB", ["name" => "name"]) instanceof App\UserB');
        $this->assertEquals($c->make(UserB::class, ["name" => "name"])->name, "name", '$c->make(App\UserB::class, ["name" => "name"])->name == "name"');
        $this->assertNotEquals($c->make(UserB::class, ["name" => "name", "lname" => "last name"])->lname, "last name", '$c->make(App\UserB::class, ["name" => "name", "lname" => "last name"])->lname != "last name"');
        $this->assertEquals($c->make(UserB::class, ["name" => "name", "lastName" => "last name"])->lname, "last name", '$c->make(App\UserB::class, ["name" => "name", "lastName" => "last name"])->lname == "last name"');
        $this->assertEquals($c->make(UserB::class, ["name" => "name", 1 => "last name"])->lname, "last name", '$c->make(App\UserB::class, ["name" => "name", 1 => "last name"])->lname == "last name"');
        $this->assertEquals($c->make(UserB::class, ["name", "last name"])->lname, "last name", '$c->make(App\UserB::class, ["name", "last name"])->lname == "last name"');

        $this->assertInstanceOf(UserName::class, $c->make(UserName::class), 'Make without required params object');
        $this->assertEquals($c->make(UserName::class, ['name'])->user, 'name', 'Make without required params object and set this params');
        $this->assertEquals($c->make(UserNameLast::class, ['name'])->user, 'name', 'Make without required params object and set this params');
        $this->assertEquals($c->make(UserNameLast::class, ['name'])->user, 'name', 'Make without required params object and set this params');
        $this->assertEquals($c->make(UserNameLast::class, ['name'])->last, '', 'Make without required params object and set this params');
        $this->assertEquals($c->make(UserNameLast::class, ['name', 'last'])->last, 'last', 'Make without required params object and set this params');

        $c->bind("TestUserBindContainer", function($c){
            $this->assertInstanceOf(ServiceContainer::class, $c,  '$c instanceof App\Container');
            return new UserB('');
        });
        $c->make("TestUserBindContainer");

        $c->bind(App\User::class, UserB::class);
        $this->assertInstanceOf(UserB::class, $c->make(App\User::class, ["name"]), '$c->make(App\User::class, ["name") instanceof App\UserB hinting class');

        $c->bind(User::class);
        $c->alias('User', User::class);
        $this->assertTrue($c->has("User"), '$c->alias("User", App\User::class) asliasing class');
        $this->assertInstanceOf(User::class,$c->make("User"),  '$c->make("User") instanceof App\User');

        $c->extend('User', UserA::class);
        $this->assertInstanceOf(UserA::class, $c->make("User"), '$c->extend("User", App\UserA::class) decorating');

        $c->extend(UserA::class, ExtUser::class);
        $this->assertInstanceOf(ExtUser::class, $c->make("User"), '$c->make("User") instanceof App\ExtUser decorating alias');
        $this->assertInstanceOf(ExtUser::class, $c->make(User::class), '$c->make(App\User::class) instanceof App\ExtUser decorating no alias');
        $this->assertInstanceOf(ExtUser::class, $c->make(UserA::class), '$c->make(App\UserA::class) instanceof App\ExtUser decorating sub class no container');

        $c->unBind("User");
        $c->unBind(User::class);
        try{
            $this->assertTrue($c->make("User"), '$c->unBind("User") unbind exception');
        }catch(\Exception $e){
            $this->assertTrue(true, '$c->unBind("User") unbind exception');
        }
        $this->assertFalse($c->hasIn(User::class), 'App\User::class not in container');
        $this->assertInstanceOf(User::class, $c->make(User::class), 'App\User::class maked without container');

        $c->singleton("S1User", User::class);
        $this->assertSame($c->make("S1User"), $c->make("S1User"), '$c->make("S1User") === $c->make("S1User") singleton');
        $c->singleton("S2User", new \User);
        $this->assertSame($c->make("S2User"), $c->make("S2User"), '$c->make("S2User") === $c->make("S2User") singleton');

        $c->singleton("S3User", function($c){
            return new \User;
        });
        $this->assertSame($c->make("S3User"), $c->make("S3User"), '$c->make("S3User") === $c->make("S3User") singleton');
    }
}