<?php


use Wt\Core\Templater\Templater;
use Wt\Core\Templater\TTemplater;
use Wt\Core\Unit\UnitTestCase;


class TemplaterTest extends UnitTestCase
{

    use TTemplater;

    public $templateCallbackHandler;

    public function contextMethod($num)
    {
        echo $num * 3;
    }

    public function render1($text)
    {
        ?>
        <p><?=$text?></p>
        <?
    }

    public function testContext()
    {
        $templater = $this->templater();

        $this->templateCallbackHandler = function ($text){
            echo 'text:'.$text;
        };

        $this->assertSame('Nikolai', $templater->getTemplate('name'), 'get simple template');
        $this->assertSame('15', $templater->getCallTemplateOb('contextMethod', [5]), 'get template with using context');
        $this->assertSame(htmlspecialchars('<p>Super</p>'), htmlspecialchars(trim($templater->getCallTemplateOb('render1', ['Super']))), 'get Call template 1');
        $this->assertSame('text:Super', $templater->getCallTemplateOb('templateCallbackHandler', ['Super']), 'get Call template 2');

    }

    public function testNamespace()
    {
        $templater = new Templater(__DIR__ . '/templater.dev', 'main');


        $this->assertSame('this kit namespace', $templater->getTemplate('kit:testKit'), 'namespace kit');
        $this->assertSame('this main namespace', $templater->getTemplate('testMain'), 'namespace main - default');
        $this->assertSame(
            \Bitrix\Main\IO\Path::normalize((__DIR__)) . '/templater.dev/main/templates/sun.php',
            $templater->getTemplatePath('sun'),
            'TemplatePath'
        );
        $this->assertSame(
            \Bitrix\Main\IO\Path::normalize((__DIR__)) . '/templater.dev/kit/templates/sun.php',
            $templater->getTemplatePath('kit:sun'),
            'TemplatePath'
        );
        $this->assertSame(true, $templater->isFileTemplateExist('testMain'), 'template exists');
        $this->assertSame(false, $templater->isFileTemplateExist('sun'), 'template exists');

    }
}