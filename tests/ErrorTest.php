<?php


use Wt\Core\Collection\ErrorCollection;
use Wt\Core\Support\Error;
use Bitrix\Main\Error as BxError;

class ErrorTest extends \Wt\Core\Unit\UnitTestCase
{
    public function testError()
    {
        $error = new Error('Ошибка', 100, [
            'k1' => 'test'
        ]);
        $this->assertSame(100, $error->getCode(), 'code');
        $this->assertSame('Ошибка', $error->getMessage(), 'message');
        $this->assertSame([
            'k1' => 'test'
        ], $error->getData(), 'data');

        $errorCollection = new ErrorCollection([
            100 => 'e1',
            'E200' => 'e2',
            [
                'message' => 'e3'
            ],
        ]);

        $this->assertSame('', $errorCollection->first()->getCode(), 'code 1');
        $this->assertSame('E200', $errorCollection->eq(1)->getCode(), 'code 2');
        $this->assertSame('e1', $errorCollection->first()->getMessage(), 'message 1');
        $this->assertSame('e3', $errorCollection->last()->getMessage(), 'message 2');
        $errorCollection->push('e4');
        $this->assertSame('e4', $errorCollection->last()->getMessage(), 'message 3');
        $errorCollection->push(1000);
        $this->assertSame(1000, $errorCollection->last()->getMessage(), 'message 4');
        $errorCollection->push([
            'code' => 70,
            'message' => 'e70',
        ]);
        $this->assertSame('e70', $errorCollection->last()->getMessage(), 'message 5');
        $this->assertSame(70, $errorCollection->last()->getCode(), 'code 3');
        $errorCollection->add('e8');
        $this->assertSame('e8', $errorCollection->last()->getMessage(), 'message 6');
        $errorCollection->add([
            'message' => 'e9'
        ]);
        $this->assertSame('e9', $errorCollection->last()->getMessage(), 'message 7');
        $errorCollection->add([
            [
                'message' => 'e10'
            ],
        ]);
        $this->assertSame('e10', $errorCollection->last()->getMessage(), 'message 8');
        $errorCollection->add([
            [
                'message' => 'e11'
            ],
            'e12'
        ]);
        $this->assertSame('e12', $errorCollection->last()->getMessage(), 'message 9');
        $errorCollection->add(new Error('e13'));
        $this->assertSame('e13', $errorCollection->last()->getMessage(), 'message 10');
        $errorCollection->add([
            [
                'message' => 'e14'
            ],
            new Error('e15')
        ]);
        $errorCollection->add([
            new BxError('e16', 10, [1])
        ]);
        $this->assertSame('e16', $errorCollection->last()->getMessage(), 'message 11');
        $this->assertSame(10, $errorCollection->last()->getCode(), 'code 4');
        if(method_exists(BxError::class, 'getCustomData')){
            $this->assertSame([1], $errorCollection->last()->getData(), 'data');
        } else {
            $this->assertSame([], $errorCollection->last()->getData(), 'data: old version bitrix');
        }


        $bxErrorCollection = new \Bitrix\Main\ErrorCollection([
            new BxError('e17'),
            new BxError('e18'),
        ]);

        $errorCollection->add($bxErrorCollection);
        $this->assertSame('e18', $errorCollection->last()->getMessage(), 'message 12');
    }
}