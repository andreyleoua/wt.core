<?php

use Wt\Core\DebugControl\TimeDebugControl;
use Wt\Core\Entity\AEntity;

require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/prolog_before.php');


$d = new TimeDebugControl();
$d->begin();

$factory = app()->factory()->main()->getIBlockFactory()->cache(['hit', 'bx_1hour']);
collect(json_decode('[1,2,3,4,5,6,7,8,9,10,11,12,15,16]'))->each(function ($id) use ($factory){
    $factory->getEntityById($id);
});
$collection = $factory->getCollection();
//pre(spl_object_id($collection), 0);


$collection->each(function (AEntity $entity) use ($factory){
    $firstEntity = $factory->getEntityById($entity->getId());
    //pre($firstEntity->getName(), 0);
});


$d->end();
echo $d;
pre('best 0.005', 0);
pre('best 0.003 v6', 0);
//dd($firstEntity->fields()->iblockTypeId);


