<?php

use Wt\Core\Support\CollectionToPsr;
use Wt\Core\Unit\UnitTestCase;

/**
 * 
 */
class CollectionToPsrTest extends UnitTestCase
{
	public function testGetValue()
	{
		$c = new CollectionToPsr();
		$c->set('FIRST_NAME', 'Ivan');
		$c->set('SECOND_NAME', 'Ivanov');

		$this->assertSame('Ivan', $c->firstName, 'get firstName by psr');
		$this->assertSame('Ivanov', $c->secondName, 'get secondName by psr');
	}

}