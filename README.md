# wt.core [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/andreyleoua/wt.core/)
## assets source [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/bellord13/kit/)



## Environment file `.env`

По умолчанию `local`

Разположение: `src/.env`
```sh
app()::env();
app()::environment();
```

## Extensions directory `src`
Для изменения расположения директории `src` нужно определеить константу `WT_CORE_APP_EXT_DIR`
```php
define('WT_CORE_APP_EXT_DIR', $_SERVER["DOCUMENT_ROOT"] . '/local/modules/wt.dev/src');
```

## Config files
- default_config.php
- src/config.php
- src/`env`_config.php

```sh
app()->config()->get();
```


## Templater
Вызов
```php
echo app()->service()->templater()->get('kit:input', []);
```
Конфигурирование
```php
    'templater' => [
        'path' => '/local/templater.dev',
        'namespace' => 'main',
    ],
```

## Assets 
Вызов
```php
app()->service()->assets()->setPlugins([
    'kit:flex-kit',
    'config',
    'kit:core',
    'kit:bsCore',
    'kit:collapse',
    'kit:site',
    'kit:toolkit',
    'theme',
]);
// ...
app()->service()->assets()->render();
```
Конфигурирование
```php
    'assets' => [
        'path' => '/local/templater.dev',
        'namespace' => 'main',
    ],
    'plugins' => [
        'main' => [ // $pluginName
            'path' => '/local/templates/main/../',
            'css' => [

            ],
            'js' => [

            ],
            'rel' => [

            ]
        ],
    ],
```

Порядок определения пути для ресурсов плагина

> Если в конфигураторе плагина указан `path`, то путь определяется от этого параметра, иначе от `config.assets.path + config.assets.namespace + /assets/ + $pluginName`

## Factory

**MainAbstractFactory**

```php
app()->factory()->mainAbstractFactory()
```
Создает фабрики: IBlockFactory, HLBlockFactory, FileFactory, UserFactory, UserGroupFactory, UserGroupCollectionFactory, StoreFactory, MeasureFactory, MeasureCollectionFactory, VatFactory, PriceTypeFactory

**IBlockFactoryModel**
```php
app()->factory()->iBlockFactoryModel()
    ->getEntityById()
    ->getEntityByCode()
    ->getEntityByXmlId()
```
Создает фабрики: **

**HLBlockFactoryModel**
```php
app()->factory()->hlBlockFactoryModel()
    ->getEntityById()
    ->getEntityByCode() // name
    ->getEntityByXmlId() // tableName
```
Создает фабрики: HLBlockElementFactory, HLBlockElementCollectionFactory

## Ajax

## Validator