<?php

use Wt\Core\Tools;

IncludeModuleLangFile(__FILE__);

return [
    'MAIN' => [
        'TITLE' => GetMessage('MAIN_OPTIONS_PARAMETERS'),
        'THEME' => 'Y',
        'OPTIONS' => [
            'APP_LOADED' => [
                'TITLE' => 'Apps',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/apps.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'SYSTEM' => [
                'TITLE' => 'System',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/system.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'PHP' => [
                'TITLE' => 'Php',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/php.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'BITRIX' => [
                'TITLE' => 'Bitrix',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/bitrix.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'MYSQL' => [
                'TITLE' => 'Mysql',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/mysql.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'GIT' => [
                'TITLE' => 'Git',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/git.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'CRONTAB' => [
                'TITLE' => 'Crontab',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/crontab.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],
            'BX_CRON' => [
                'TITLE' => 'BitrixCron',
                'TYPE' => 'content',
                'INCLUDEFILE' => Tools::removeDocRoot(__DIR__ . '/include/bitrix.cron.php'),
                'SHOW_EDIT_BUTTON' => 'N',
                'HINT' => '',
            ],

        ],
    ],
//    'TEST' => array(
//        'TITLE' => 'test',
//        'OPTIONS' => array(
//            'TEST' => array(
//                'TITLE' => 'test_setting',
//                'TYPE' => 'text',
//                'DEFAULT' => '',
//            ),
//        ),
//    ),
//    'CACHE' => array(
//        'TITLE' => GetMessage('CACHE_OPTIONS'),
//        'THEME' => 'Y',
//        'OPTIONS' => array(
//            'CACHE_STORAGE' => array(
//                'TITLE' => GetMessage('CACHE_STORAGE'),
//                'TYPE' => 'selectbox',
//                'LIST' => array(
//                    'file' => 'file',
//                    'memcached' => 'memcached',
//                    'memcached_cluster' => 'memcached_cluster',
//                ),
//                'DEFAULT' => 'file',
//            ),
//            'CACHE_STORAGE_TEMPLATE' => array(
//                'TITLE' => GetMessage('CACHE_STORAGE_TEMPLATE'),
//                'TYPE' => 'tableRowInclude',
//                'DEFAULT' => 'file',
//            ),
//        ),
//    ),
];