<?php

/**
 * @var Wt\Core\Admin\Templater $this
 */

use Wt\Core\App\App;
use Wt\Core\Unit\UnitTestSuite;

$apps = App::getInstances();

if($apps){
    ?><table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
    <tr style="text-align: left">
        <th>Id</th>
        <th>Env</th>
        <th>Version</th>
        <th>Date</th>
        <th>Last Version</th>
        <th>GitUrl</th>
        <th>branch</th>
        <th>hash</th>
        <th>Tests</th>
    </tr><?php
    foreach ($apps as $app){
        $remote = $app->module()->getRemoteModuleEntity();
        $lastVersionTemplate = '';
        if($remote){
            $checkVersion = CheckVersion($app->module()->getVersion(), $remote->getVersion());
            $lastVersionTemplate =
                $this->getCheckTemplate($checkVersion) .
                (!$checkVersion
                    ?$remote->getVersion() . $this->getHintTemplate("{$remote->getVersion()}<br>{$remote->fields()->date}")
                    :'');
        }
        ?><tr>
        <td><?=$app->module()->getId()?></td>
        <td><?=$app->env()?></td>
        <td><?=$app->module()->getVersion()?></td>
        <td><?=$app->module()->getDate()->format('Y-m-d H:i:s')?></td>
        <td><?=$lastVersionTemplate?></td>
        <td><a href="<?=$app->module()->fields()->gitUrl?>" target="_blank"><?=$app->module()->fields()->gitUrl?></a></td>
        <td><?=$app->module()->getBranch()?></td>
        <td style="font-family: monospace;"><?=$app->module()->getCommitShortHash()?></td>
        <?php
        $testManager = new UnitTestSuite();
        $testManager->loadByDir($app->module()->getDir() . DIRECTORY_SEPARATOR . 'tests');
        $src = $app->module()->getSrc() . '/tests/';
        echo "<!-- {$app->module()->getId()} TEST BEGIN -->";
        $testManager->run();
        echo "<!-- {$app->module()->getId()} TEST END -->";
        ?>
        <td>
            <a target="_blank" href="<?=$src?>">
                <span class="errortext"><?=$testManager->failed()?></span> + <span class="oktext"><?=$testManager->passed()?></span>
            </a>
        </td>
        </tr><?php
    }
    ?></table><?php
}