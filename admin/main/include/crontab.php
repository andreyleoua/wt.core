<?php

use Wt\Core;

$cron = new Core\Support\Crontab();

if($cron->isInstalled()){
    ?>
    <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
    <tr>
    <th>minute</th>
    <th>hour</th>
    <th>day of the month</th>
    <th>month</th>
    <th>day of the week</th>
    <th>command</th>
    </tr><?php
    foreach ($cron->getList() as $entity) {
        ?><tr>
        <td align="center"><?= $entity['m'] ?></td>
        <td align="center"><?= $entity['h'] ?></td>
        <td align="center"><?= $entity['dom'] ?></td>
        <td align="center"><?= $entity['mon'] ?></td>
        <td align="center"><?= $entity['dow'] ?></td>
        <td><?= $entity['command'] ?></td>
        </tr><?php
    }
    ?></table><?php
} else {
    ShowNote('Crontab not installed', 'errortext');
}