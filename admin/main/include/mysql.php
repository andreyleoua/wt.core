<?php
/**
 * @var Wt\Core\Admin\Templater $this
 */
use Wt\Core\Support\System;
use Wt\Core\Tools;

if(!function_exists('get_table_size')){
    function get_table_size($reg = ''){
        global $DB;
        if ($DB->type != 'MYSQL')
            return 0;

        static $CACHE;

        if (!$CACHE)
        {
            $CACHE = [];
            $sql = 'SHOW TABLE STATUS';
            $res = $DB->Query($sql);

            while($row = $res->Fetch())
                $CACHE[$row['Name']] = $row['Data_length'];
        }

        $size = 0;
        foreach($CACHE as $table => $s)
            if (!$reg || preg_match('#'.$reg.'#i', $table))
                $size += $s;
        return $size;
    }
}

$system = new System();
global $DB;
$mysqlInfo = [];
$mysqlInfo['mysql installed'] = "{$this->getCheckTemplate($system->mysqlInstalled())}";
$mysqlInfo['mysql version'] = "{$system->getMysqlVersion()}";
$mysqlInfo['mysql version (Bitrix API)'] = $DB->GetVersion();
$mysqlInfo['Tables Name'] = $DB->DBName;
$mysqlInfo['Tables Host'] = $DB->DBHost;
$mysqlInfo['Tables Login'] = $DB->DBLogin;
$mysqlInfo['Tables Size'] = Tools::getNiceMemorySize(get_table_size());
$m = $DB->Query("SHOW VARIABLES LIKE 'max_allowed_packet';")->Fetch()['Value'];
$mysqlInfo['max_allowed_packet'] = Tools::getNiceMemorySize($m);
$m = $DB->Query("SHOW VARIABLES LIKE 'max_connections';")->Fetch()['Value'];
$mysqlInfo['max_connections'] = $m;


?>
<a target="_blank" href="/bitrix/admin/perfmon_db_server.php">Монитор производительности: сервер БД</a>
<table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
<?php
foreach ($mysqlInfo as $key => $value) {
    ?><tr><td><?= $key ?></td><td><?= $value ?></td></tr><?php
}
?></table>