<?php
/**
 * @var Wt\Core\Admin\Templater $this
 */

use Wt\Core;
use Bitrix\Main;

$system = new Core\Support\System();
global $DB;
$bitrixInfoList = [];
$bitrixInfoList['bitrixVMVersion'] = $system->getBitrixVMVersion();
$bitrixInfoList['SM_VERSION'] = defined('SM_VERSION')?SM_VERSION:'';
$bitrixInfoList['SM_VERSION_DATE'] = defined('SM_VERSION_DATE')?SM_VERSION_DATE:'';
$bitrixInfoList['DB Type'] = $DB->type;
$bitrixInfoList['SQL запрос'] = '<a href="/bitrix/admin/sql.php" target="_blank">/bitrix/admin/sql.php</a>';
$bitrixInfoList['Tables'] = '<a href="/bitrix/admin/perfmon_tables.php" target="_blank">/bitrix/admin/perfmon_tables.php</a>';
$bitrixInfoList['ErrorLogFile'] = 'not config';
$exceptionHandling = Main\Config\Configuration::getValue('exception_handling');
if (is_array($exceptionHandling['log']) && is_array($exceptionHandling['log']['settings']))
{
    if($exceptionHandling['log']['settings']['file']){
        $errorLogFileSrc = '/' . $exceptionHandling['log']['settings']['file'];
        $fileExists = file_exists($_SERVER['DOCUMENT_ROOT'] . $errorLogFileSrc);
        $fileSize = 0;
        if($fileExists){
            $fileSize = filesize($_SERVER['DOCUMENT_ROOT'] . $errorLogFileSrc);
        }
        $printSize = Core\Tools::getNiceMemorySize($fileSize);
        $bitrixInfoList['ErrorLogFile'] = "<a href=\"$errorLogFileSrc\" target=\"_blank\">$errorLogFileSrc [$printSize]</a>";
    }
}

?>
    <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
<?php
foreach ($bitrixInfoList as $key => $value) {
    ?><tr><td><?= $key ?></td><td><?= $value ?></td></tr><?php
}
?></table>
<br>
<a href="/bitrix/admin/perfmon_table.php?PAGEN_1=1&SIZEN_1=20&set_filter=Y&adm_filter_applied=0&table_name=b_module_to_module&find_type=ID&find_TO_MODULE_ID=wt.core"
    target="_blank">
    Зарегистрированные события в системе (таблица b_module_to_module)
</a>