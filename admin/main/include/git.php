<?php
/**
 * @var Wt\Core\Admin\Templater $this
 */
use Wt\Core\Support\System;

$system = new System();

$phpInfo = [];
$phpInfo['git installed'] = "{$this->getCheckTemplate($system->gitInstalled())}";
$phpInfo['git version'] = "{$system->getGitVersion()}";


?>
<table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
<?php
foreach ($phpInfo as $key => $value) {
    ?><tr><td><?= $key ?></td><td><?= $value ?></td></tr><?php
}
?></table>