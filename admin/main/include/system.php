<?php

use Wt\Core\Support\System;
use Wt\Core\Support\Cli;
use Wt\Core\Tools;

$system = new System();
$cli = new Cli();

$serverInfoList = [];
$serverInfoList['whoami'] = $system->whoAmI();
$serverInfoList['document root'] = $system->getDocumentRoot();
$serverInfoList['OS'] = $system->getOS();
$serverInfoList['uName'] = $system->uName();
$serverInfoList['PhysicalMemory'] = Tools::getPrintSize($system->getUsedPhysicalMemory())
    . ' / '
    . Tools::getPrintSize($system->getTotalPhysicalMemory())
    . ' (' . $system->getUsedPhysicalMemoryPercent() . '%' . ')';
$serverInfoList['VirtualMemory'] = Tools::getPrintSize($system->getUsedVirtualMemory())
    . ' / '
    . Tools::getPrintSize($system->getTotalVirtualMemory())
    . ' (' . $system->getUsedVirtualMemoryPercent() . '%' . ')';
$serverInfoList['TLS version'] = $system->getTlsVersion();
$serverInfoList['OpenSSL version text'] = $system->getOpenSSLVersionText();
$serverInfoList['OpenSSL version number'] = $system->getOpenSSLVersionNumber();
$serverInfoList['release'] = $cli->exec('cat /etc/*release*')->getOutputHtml();

?>
    <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
<?php
foreach ($serverInfoList as $key => $value) {
    ?><tr><td><?= $key ?></td><td><?= $value ?></td></tr><?php
}
?></table>