<?php

/**
 * @var Wt\Core\Admin\Templater $this
 */

use Wt\Core\Support\Crontab;
use Wt\Core\Support\System;

$cron = new Crontab();
$system = new System();


$testConfig = [
    [
        'message' => 'Отключен запуск агентов в прологе',
        'description' => 'Если опция check_agents = Y или не опреденена (по умолчанию - Y), то в пррологе (main.include) производится запуск агентов. <br>Для отключения: <code>\COption::SetOptionString("main", "check_agents", "N");</code>',
        'test' => function(){
            return COption::GetOptionString('main', 'check_agents', 'Y') === 'N';
        },
    ],
    [
        'message' => 'Отключено разделение агентов на переодические / непереодические через опцию agents_use_crontab модуля main',
        'description' => 'Если опция agents_use_crontab = Y (по умолчанию - N), то исполнение агентов будет разделено на переодические или непереодические константой BX_CRONTAB. <br>Необходимо Options.main.agents_use_crontab = N',
        'test' => function(){
            return COption::GetOptionString('main', 'agents_use_crontab', 'N') === 'N';
        },
    ],
    [
        'message' => 'Отключено разделение агентов на переодические / непереодические через константу BX_CRONTAB_SUPPORT',
        'description' => 'Константа BX_CRONTAB_SUPPORT должна быть не определена или равна true. <br>Определение константы в файле <a target="_blank" href="/bitrix/admin/fileman_file_edit.php?path=%2Fbitrix%2Fphp_interface%2Fdbconn.php&full_src=Y">/bitrix/php_interface/dbconn.php</a> должно иметь следующий вид: <br><code>if(!(defined("CHK_EVENT") && CHK_EVENT===true))<br>define("BX_CRONTAB_SUPPORT", true);</code>',
        'test' => function(){
            return !defined('BX_CRONTAB_SUPPORT') || (BX_CRONTAB_SUPPORT === true);
        },
    ],
    [
        'message' => 'Константа BX_CRONTAB не определена',
        'description' => 'Если константа BX_CRONTAB равна true, то в работу пойдут непереодические агенты, иначе - переодические агенты.<br>Работает если BX_CRONTAB_SUPPORT = true или agents_use_crontab = Y',
        'test' => function(){
            return !defined('BX_CRONTAB');
        },
    ],
    [
        'message' => 'Константа DisableEventsCheck не определена',
        'description' => 'Если константа DisableEventsCheck равна true, то отправка почты отключена',
        'test' => function(){
            return !(defined('DisableEventsCheck') && DisableEventsCheck === true);
        },
    ],
    [
        'message' => 'Константа NO_AGENT_CHECK не определена или не равна true',
        'description' => '',
        'test' => function(){
            return !(defined('NO_AGENT_CHECK') && NO_AGENT_CHECK===true);
        },
    ],
    [
        'message' => 'Константа BX_CLUSTER_GROUP не определена или равна 1',
        'description' => '',
        'test' => function(){
            return !(defined('BX_CLUSTER_GROUP') && BX_CLUSTER_GROUP !== 1);
        },
    ],
    [
        'message' => 'Наличие кастомного файла cron_events.php',
        'description' => '/bitrix/php_interface/cron_events.php',
        'test' => function(){
            return file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/cron_events.php');
        },
    ],
    [
        'message' => 'Установлено задание в крон на запуск кастомного файла cron_events.php',
        'description' => "Необходимо установить задание крон <br><code>* * * * * {$system->getPhpPath()} -f {$_SERVER['DOCUMENT_ROOT']}/bitrix/php_interface/cron_events.php</code>",
        'test' => function() use ($system) {
            $cron = new Crontab();
            return $cron->isInstalled()
                && $cron->isExists("{$system->getPhpPath()} -f {$_SERVER['DOCUMENT_ROOT']}/bitrix/php_interface/cron_events.php");
        },
    ],
    [
        'message' => 'Missing default bitrix file cron_events.php in cron',
        'description' => "Необходимо удалить стандартный битриксовый файл из крон заданий<br>{$_SERVER['DOCUMENT_ROOT']}/bitrix/modules/main/tools/cron_events.php",
        'test' => function() use ($system) {
            $cron = new Crontab();
            return $cron->isInstalled()
                && !$cron->isExists("{$system->getPhpPath()} -f {$_SERVER['DOCUMENT_ROOT']}/bitrix/modules/main/tools/cron_events.php");
        },
    ],
    [
        'message' => 'Отсутствуют системные почтовые сообщения в таблице b_event',
        'description' => 'Флаг N поля SUCCESS_EXEC в таблице b_event свидетельствует о том, что не было попыток отправить данные из этой строки',
        'test' => function(){
            global $DB;
            $res = $DB->Query("SELECT COUNT(1) AS A FROM b_event WHERE SUCCESS_EXEC = 'N'");
            $f = $res->Fetch();
            return !($f['A'] > 0);
        },
    ],
    [
        'message' => 'Последний непереодический агент отпработал менее часа назад',
        'description' => 'Если все пунты прошли проверку кроме этого, то см. dbconn.php',
        'test' => function(){
            return $GLOBALS['DB']->Query('SELECT LAST_EXEC, ID, IS_PERIOD FROM b_agent WHERE LAST_EXEC > NOW() - INTERVAL 1 HOUR AND IS_PERIOD="N" ORDER BY LAST_EXEC DESC LIMIT 1')->Fetch();
        },
    ],
];


?>
    <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
        <?php
$rsTest = [];
foreach ($testConfig as $key => $value) {
    ?><tr>
    <td style="vertical-align: top; font-size: 1.25em;text-align: center;"><?=$this->getCheckTemplate($rsTest[$key] = call_user_func_array($value['test'], []))?></td>
    <td><?= $value['message'] ?><br><small style="color: #5e5e5e;"><?= $value['description'] ?></small></td>
    </tr><?php
}
?></table><?php

 ?>
<div class="adm-info-message">
    <a target="_blank" href="/bitrix/admin/agent_list.php">Агенты</a>
    <br>
    Переодические агенты - агенты, время запуска которого определяется как время <b>начала</b> работы метода + интервал. В классе CAgent: IS_PERIOD = Y
    <br>
    Непереодические агенты - агенты, время запуска которого определяется как время <b>окончания</b> работы метода + интервал. В классе CAgent: IS_PERIOD = N
    <br>
    <br>
    Статьи:
    <br>
    <a target="_blank" href="https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2943">Запуск агентов из cron</a>
    <br>
    <a target="_blank" href="https://dev.1c-bitrix.ru/community/webdev/user/25773/blog/10059/">Выполнение всех агентов на cron. Чтобы почта была на хитах</a>
</div>