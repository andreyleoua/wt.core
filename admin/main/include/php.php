<?php
/**
 * @var Templater $this
 */

use Wt\Core\Admin\Templater;
use Wt\Core\Support\Cli;
use Wt\Core\Support\System;
use Wt\Core\Tools;

$system = new System();
$cli = new Cli();
$cliPhp = app()->service()->cliPhp();

$phpInfo = [];
$phpInfo['api'] = [
    'web' => $system->getPhpSApiName(),
    'cli' => $cliPhp->codeExec('echo @php_sapi_name();')->getOutputString(),
];
$phpInfo['path'] = [
    'web' => '',
    'cli' => $system->getPhpPath(),
];
$phpInfo['version'] = [
    'web' => $system->getPhpVersion(),
    'cli' => $cliPhp->codeExec('echo @phpversion();')->getOutputString(),
];
$phpInfo['version ext'] = [
    'web' => '',
    'cli' => $cliPhp->optionExec('-v')->getOutputHtml(),
];
$phpInfo['Thread Safety'] = [
    'web' => PHP_ZTS?'Yes':'No',
    'cli' => $cliPhp->codeExec("echo PHP_ZTS?'Yes':'No';")->getOutputString(),
];
$phpInfo['LoadedIniFile'] = [
    'web' => $system->getPhpLoadedIniFile(),
    'cli' => $cliPhp->codeExec('echo @php_ini_loaded_file();')->getOutputString(),
];
$phpInfo['ScannedIniFiles'] = [
    'web' => collect($system->getPhpScannedIniFiles())->join('<br>'),
    'cli' => $cliPhp
        ->codeExec("echo implode('<br>', array_map('trim', explode(',', php_ini_scanned_files())));")
        ->getOutputHtml(),
];
$phpInfo['max_execution_time'] = [
    'web' => ini_get('max_execution_time'),
    'cli' => $cliPhp
        ->codeExec("echo ini_get('max_execution_time');")
        ->getOutputHtml(),
];

$phpInfo['memory_limit'] = [
    'web' => (function() use ($cli, $system){
        $rawMem = ini_get('memory_limit');
        return Tools::getPrintSize($system->getPhpMemoryLimit($rawMem)) . " ($rawMem)";
    })(),
    'cli' => (function() use ($cliPhp, $system){
        $rawMem = $cliPhp->codeExec("echo @ini_get('memory_limit');")->getOutputString();
        return Tools::getPrintSize($system->getPhpMemoryLimit($rawMem)) . " ($rawMem)";
    })(),
];
$webExt = $system->getPhpExtensions();
$cliExt = $cliPhp->getExtensions();
$webExtPublicList = [];
foreach ($webExt as $ext) {
    $webExtPublicList[] = in_array($ext, $cliExt)?$ext:"<span style=\"color: green;font-weight: bold;\">$ext</span>";
}
$cliExtPublicList = [];
foreach ($cliExt as $ext) {
    $cliExtPublicList[] = in_array($ext, $webExt)?$ext:"<span style=\"color: green;font-weight: bold;\">$ext</span>";
}
$phpInfo['extensions'] = [
    'web' => implode(', ', $webExtPublicList),
    'cli' => implode(', ', $cliExtPublicList),
];

$webIniAll = $system->getPhpIniAll(null, false);
$cliIniAll = $cliPhp->getIniAll(null, false);
$iniAllList = [];
$iniParamEquals = function ($v1, $v2){
    if(strtolower($v1) === 'on') $v1 = '1';
    if(strtolower($v1) === 'off') $v1 = '0';
    if(strtolower($v2) === 'on') $v2 = '1';
    if(strtolower($v2) === 'off') $v2 = '0';

    return $v1 === $v2;
};
foreach ($webIniAll as $param => $value) {
    $iniAllList[$param]['web'] = $iniParamEquals($cliIniAll[$param], $value)?$value:"<span style=\"color: red;font-weight: bold;\">$value</span>";
}
foreach ($cliIniAll as $param => $value) {
    $iniAllList[$param]['cli'] = $iniParamEquals($webIniAll[$param], $value)?$value:"<span style=\"color: red;font-weight: bold;\">$value</span>";
}

?>
<table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
    <tr><td style="text-align: center" colspan="3">
            <a href="/bitrix/admin/phpinfo.php?test_var1=AAA&test_var2=BBB" target="_blank">phpinfo</a>,
            <a href="/bitrix/admin/php_command_line.php" target="_blank">Командная PHP-строка</a>
            <br>
            <a href="/bitrix/admin/perfmon_php.php" target="_blank">Монитор производительности: Настройки PHP</a>
        </td></tr><?php
    foreach ($phpInfo as $key => $value) {
        ?><tr><td><?= $key ?></td><td><?= $value['web'] ?></td><td><?= $value['cli'] ?></td></tr>
        <?php
    }
    $extensionList = ['curl', 'xml', 'xmlwriter', 'xmlreader', 'iconv', 'mbstring', 'ftp', 'gd'];
    ?><tr><td style="text-align: center" colspan="3">Extensions Checker</td></tr><?php
    foreach ($extensionList as $key => $extension) {
        ?><tr>
        <td><?= $extension ?></td>
        <td><?= "{$this->getCheckTemplate(extension_loaded($extension))}" ?></td>
        <td><?= (function(/** @var Templater $adminTemplater */ $adminTemplater) use ($cli, $cliPhp, $system, $extension){
                $check = (int)$cliPhp
                    ->codeExec("echo extension_loaded('$extension')?'1':'0';")
                    ->getOutputHtml();
                return $adminTemplater->getCheckTemplate($check);
            })($this) ?></td>
        </tr><?php
    }
    ?></table>

<div class="accordion">
    <div class="accordion-item">
        <div class="accordion-header">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                INI
            </button>
        </div>
        <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse">
            <div class="accordion-body">
                <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
                    <tr><td style="text-align: center" colspan="3">INI</td></tr><?php
                    foreach ($iniAllList as $key => $value) {
                        ?><tr style="color: grey"><td><?= $key ?></td><td><?= $value['web'] ?></td><td><?= $value['cli'] ?></td></tr>
                        <?php
                    }
                    ?></table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    kit.ready('collapse');
</script>
