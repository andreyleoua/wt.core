<?php

use Bitrix\Main\Localization\Loc;
use Wt\Core\Admin\GlobalMenuItem;

Loc::loadMessages(__FILE__);

//return [
////    'parent_menu' => 'global_menu_'.WT_DEV_MODULE_CLASS,
////    'sort'        => 10,
////    'url'         => WT_DEV_MODULE_ID.'_main.php',
////    'text'        => 'Основные настройки',
////    'title'       => 'Основные настройки',
////    'icon'        => 'sys_menu_icon',
////    'page_icon'   => 'sys_menu_icon',
////    'items_id'    => 'menu_custom',
////];
return (new GlobalMenuItem())
    ->setSort(10)
    ->setTitle('Informer')
    ->setUrl();