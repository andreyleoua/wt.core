<?php

namespace Wt\Core;

/**
 * 
 */
class EventDispatcher
{
    protected $eventHandlers;

    public function __construct()
    {
        $this->eventHandlers = app('collection');
    }

    public function addListener($eventName, $listner, $sort = 100)
    {
        if(!$this->eventHandlers->has($eventName)){
            $this->eventHandlers->set($eventName, app('collection'));
        }

        $item = [
            'listner' => $listner,
            'sort' => $sort
        ];

        $this->eventHandlers->get($eventName)->push((object)$item);
    }

    public function dispatch($eventName, $event)
    {
        $_event = $event;

        if( !$this->eventHandlers->has($eventName) ){
            return;
        }

        $eventListners = $this->eventHandlers->get($eventName)->sortBy(function($a, $b){
            if( $a->sort == $b->sort ){
                return 0;
            }

            return ($a->sort > $b->sort)?1:-1;
        });

        foreach($eventListners as $eventItem){
            $chkEvent = call_user_func_array($eventItem->listner, [$_event]);
            if($chkEvent instanceOf Event){
                $_event = $chkEvent;
            }

            if( $_event->isPropagationStopped() ){
                break;
            }
        };
    }
}