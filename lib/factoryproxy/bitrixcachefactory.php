<?php


namespace Wt\Core\FactoryProxy;


use Wt\Core\Cache\BitrixCache;
use Wt\Core\Entity\AEntity;
use Wt\Core\Interfaces\IFactory;

class BitrixCacheFactory extends ACacheFactoryProxy
{
    public function __construct(BitrixCache $bxCache)
    {
        $this->cache = $bxCache;
    }

    public function setFactory(IFactory $factory)
    {
        parent::setFactory($factory);

        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
    }

    protected function saveEntityInCache($entity)
    {
        $raw = $this->getRawByEntity($entity);
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
        if($r = parent::saveEntityInCache($raw)){
            $indexValues = $this->__mapper()->getIndexValues($raw['ID']);
            if($indexValues){
                $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/index');
                foreach ($indexValues as $altKey => $id) {
                    $this->cache->put($altKey, $id);
                }
                $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
            }
        }
        return $r;
    }

    public function getEntityByXmlId($xmlId)
    {
        $id = $this->__mapper()->getIdCacheKey($xmlId, 'xml_id');

        if( !$id ) {
            $cacheKeyLocal = $this->getCacheKey($xmlId, 'xml_id');
            $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/index');
            /**
             * $this->cache->has($cacheKeyLocal)
             * не используем тк приводит к двойному чтению из файла и unserialize
             */
            if($id = $this->cache->get($cacheKeyLocal)){
                $this->__mapper()->put($cacheKeyLocal, $id);
            }
            $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
        }

        return parent::getEntityByXmlId($xmlId);
    }

    public function getEntityByCode($code)
    {
        $id = $this->__mapper()->getIdCacheKey($code, 'code');

        if( !$id ) {
            $cacheKeyLocal = $this->getCacheKey($code, 'code');
            $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/index');
            /**
             * $this->cache->has($cacheKeyLocal)
             * не используем тк приводит к двойному чтению из файла и unserialize
             */
            if( $id = $this->cache->get($cacheKeyLocal)){
                $this->__mapper()->put($cacheKeyLocal, $id);
            }
            $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
        }

        return parent::getEntityByCode($code);
    }

    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/collection');
        $collection = parent::getCollection($filter, $order, $select, $limit, $offset);
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
        return $collection;
    }

    public function clearCache()
    {
        $this->__mapper()->clear();
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/index');
        $this->cache->clear();
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/collection');
        $this->cache->clear();
        $this->cache->changePath('BitrixCacheFactory/'.$this->__getCachePrefix().'/entity');
        $this->cache->clear();

        $this->getFactory()->clearCache();
    }
}