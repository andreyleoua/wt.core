<?php


namespace Wt\Core\FactoryProxy;


use Wt\Core\Cache\ArrayCache;

class ArrayCacheFactory extends ACacheFactoryProxy
{
    public function __construct(ArrayCache $arrayCache)
    {
        $this->cache = $arrayCache;
    }
}