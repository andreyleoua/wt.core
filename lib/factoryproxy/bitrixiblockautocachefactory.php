<?php


namespace Wt\Core\FactoryProxy;


use Wt\Core\Cache\BitrixIblockCache;
use Wt\Core\factory\ABxFactory;
use Wt\Core\Interfaces\ICacheFactory;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Support\Mapper;

class BitrixIblockAutoCacheFactory extends BitrixCacheFactory
{
    public function __construct(BitrixIblockCache $bxCache, Mapper $mapper)
    {
        $this->cache = $bxCache;
        $this->cacheKeyMapper = $mapper;
    }

    public function setFactory(IFactory $realFactory)
    {
        parent::setFactory($realFactory); // TODO: Change the autogenerated stub

        if( $realFactory instanceof ABxFactory ){
            $this->cache->iblockId = $realFactory->getIblockId();
        }

        $this->cache->changePath('Iblock_'.$this->cache->iblockId.'_AutoCacheFactory/'.$this->__getCachePrefix());
    }
}