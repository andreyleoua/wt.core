<?php


namespace Wt\Core\FactoryProxy;


use Wt\Core\Cache\ACache;
use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Support\FactoryMapper;
use Wt\Core\Interfaces\ICacheFactory;
use Wt\Core\Interfaces\IEntity;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Interfaces\IProxyFactory;

abstract class ACacheFactoryProxy implements ICacheFactory, IProxyFactory, IFactory
{
    /**
     * @var ACache
     */
    public $cache;

    /**
     * @return FactoryMapper
     */
    public function __mapper()
    {
        return $this->factory->__mapper();
    }

    /**
     * @var ICacheFactory|AFactory
     */
    protected $factory;

    protected $cachePrefix = 'abstract';

    protected $ttlInited = false;

    public function __getCachePrefix()
    {
        return $this->cachePrefix;
    }

    /**
     * @param IFactory $factory realFactory
     * @return void
     */
    public function setFactory(IFactory $factory)
    {
        $this->factory = $factory;
        $this->cachePrefix = $this->factory->__getCachePrefix();
        $this->cache->setPrefix($this->factory->__getCachePrefix());
    }

    public function getFactory()
    {
        return $this->factory;
    }

    public function setTtl($ttl)
    {
        if( $ttl > 0 && !$this->ttlInited ){
            $this->cache->defaultTtl = $ttl;
            $this->ttlInited = true;
        }
    }

    protected function getCacheKey($value, $type = 'id')
    {
        return $this->__getCachePrefix().':'.$type.':'.$value;
    }

    protected function saveEntityInCache($entity)
    {
        $raw = $this->getRawByEntity($entity);

        if( !isset($raw['ID']) ){
            return false;
        }

        $cacheKey = $this->getCacheKey($raw['ID']);

        return $this->cache->put($cacheKey, $raw) !== false;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->factory, $name], $arguments);
    }

    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }

        if($this->factory->__isCollectionQuery()){
            return $this->getCollection()->get($id);
        }

        // $id = (int)$id; //uuid
        $cacheKey = $this->getCacheKey($id);
        if( !$this->cache->has($cacheKey) ){
            /** @var AEntity|null $entity */
            if($entity = $this->factory->getEntityById($id)){
                $this->saveEntityInCache($entity);
                $entity->setFactory($this);
            }
            return $entity;
        }

        return $this->getEntityByResult($this->cache->get($cacheKey, []));
    }

    public function getEntityByCode($code)
    {
        if($this->factory->__isCollectionQuery()){
            return $this->getCollection()->getByCode($code);
        }

        $cacheKey = $this->__mapper()->getIdCacheKey($code, 'code');

        if( !$cacheKey || !$this->cache->has($cacheKey) ){
            $entity = $this->factory->getEntityByCode($code);
            if($entity){
                $entity->setFactory($this);
                $this->saveEntityInCache($entity);
            }
            return $entity;
        }

        return $this->getEntityByResult($this->cache->get($cacheKey, []));
    }

    public function getEntityByXmlId($xmlId)
    {
        if($this->factory->__isCollectionQuery()){
            return $this->getCollection()->getByXmlId($xmlId);
        }

        $cacheKey = $this->__mapper()->getIdCacheKey($xmlId, 'xml_id');

        if( !$cacheKey || !$this->cache->has($cacheKey) ){
            $entity = $this->factory->getEntityByXmlId($xmlId);
            if($entity){
                $entity->setFactory($this);
                $this->saveEntityInCache($entity);
            }
            return $entity;
        }

        return $this->getEntityByResult($this->cache->get($cacheKey, []));
    }

    public function getEntityByResults(array $arResults)
    {
        /**
         * @var AEntityCollection $collection
         */
        $collection = resolve($this->factory->__getCollectionClass(), [$arResults]);
        $collection->walk([$this, 'getEntityByResult']);
        $collection->__setFactory($this);

//        foreach ($arResults as $id => $arResult){
//            $entity = $this->getEntityByResult($arResult);
//            $collection->put($id, $entity);
//        }

        return $collection;
    }

    public function getEntityByIds(array $ids)
    {
        $noCacheIds = [];

        foreach ($ids as $id){
            $cacheKey = $this->getCacheKey($id);
            if( !$this->cache->has($cacheKey) ){
                $noCacheIds[] = $id;
            }
        }
        $itemsData = null;
        if( $noCacheIds ){
            $itemsData = $this->factory->getEntityByIds($noCacheIds);
            foreach ($itemsData as $item){
                $this->saveEntityInCache($item);
            }
        }

        $result = [];
        foreach ($ids as $id){
            if($itemsData && $itemsData->has($id)){
                $result[$id] = $itemsData->get($id);
                if($result[$id] instanceof IEntity){
                    $result[$id] = $result[$id]->getRaw();
                }
            } else {
                if(($cacheKey = $this->getCacheKey($id)) && ($value = $this->cache->get($cacheKey, []))){
                    $result[$id] = $value;
                }
            }
        }

        // return collect($result)->map([$this, 'getEntityByResult']);
        /** @var AEntityCollection $collection */
        $collection = resolve($this->factory->__getCollectionClass(), [$result]);
        $collection->__setFactory($this);
        return $collection->walk([$this, 'getEntityByResult']);
    }

    public function getEntityByResult(array $arResult)
    {
        /**
         * not !$arResult['ID']
         */
        if(!$arResult){
            return null;
        }

        /**
         * @todo $saveInCache
         * @var AEntity $item
         */
        $item = $this->factory->getEntityByResult($arResult);

        $item->setFactory($this);
// !!!
//        if(!$item->hasFactory()) {
//            $item->setFactory($this);
//        }
        return $item;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return AEntityCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        static $cache = [];

        $cacheKey = $this->getCacheKey(md5(serialize(func_get_args())), 'collection');

        $classId = \spl_object_id($this);
        if(isset($cache[$classId][$cacheKey])) {
            return $cache[$classId][$cacheKey];
        }

        /** @var AEntityCollection $collection */
        $cacheReult = $this->cache->get($cacheKey, []);
        if( !$cacheReult && ($collection = call_user_func_array([$this->factory, 'getCollection'], func_get_args())) ){

            if(!$collection->count()){
                return $this->getEntityByResults([]);
            }
            $this->cache->put($cacheKey, $collection->getRaw());

            if(
                !$this->factory->__isCollectionQuery()
                && (
                    !$select || ($select == ['*']) || ($select == ['*', 'UF_*']) || ($select == ['UF_*'])
                )
            ){
                $collection->each(function (AEntity $entity){
                    $this->saveEntityInCache($entity);
                });
            }

            $collection->each(function (AEntity $entity){
                $entity->setFactory($this);
            });
            $collection->__setFactory($this);
            return $cache[$classId][$cacheKey] = $collection;
        }

        return $cache[$classId][$cacheKey] = $this->getEntityByResults($cacheReult);
    }


    protected function getRawByEntity($entity)
    {
        if(is_array($entity)){
            return $entity;
        } elseif($entity instanceof IEntity){
            return $entity->getRaw();
        }
        return [];
    }

    public function clearCacheById($id, $proxy = true)
    {
//        $cacheKey = $this->getCacheKey($id, 'id');
//
//        foreach ($this->cacheKeyMapper->getReferenceKeys($cacheKey) as $mapperKey){
//            if( $this->cache->has($mapperKey) ){
//                $this->cache->forget($mapperKey);
//            }
//        }
//        $this->cacheKeyMapper->removeReferences($cacheKey);
//
//        if( $this->cache->has($cacheKey) ){
//            $this->cache->forget($cacheKey);
//        }
//
//        if(!$proxy){
//            return;
//        }
//
//        if($this->getFactory()){
//            $this->getFactory()->clearCacheById($id);
//        }
    }

    public function getRealFactory()
    {
        $f = $this->getFactory();
        while($f instanceof IProxyFactory){
            $f = $f->getFactory();
        }
        return $f;
    }

    public function clearCache()
    {
        $this->cache->clear();
        $this->__mapper()->clear();
        $this->getFactory()->clearCache();
    }
}