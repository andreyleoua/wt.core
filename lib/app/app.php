<?php


namespace Wt\Core\App;


use Wt\Core\Assets\Assets;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Interfaces\IServiceContainer;
use Wt\Core\Settings\Setting;
use Wt\Core\Support\LexerCollection;
use Wt\Core\Facade\DefaultDefines;
use Wt\Core\Facade\Defines;
use Wt\Core\Facade\DefaultServiceFacade;
use Wt\Core\Facade\ServiceFacade;
use Wt\Core\Facade\DefaultFactoryFacade;
use Wt\Core\Facade\FactoryFacade;
use Wt\Core\Templater\Templater;

class App implements IApp
{

    /**
     * @var $instances App[]
     */
    protected static $instances;

    /**
     * @var IServiceContainer
     */
    protected $container;

    /**
     * @var ModuleEntity
     */
    protected $module;

    public $__env = 'local';

    public function __construct(ModuleEntity $moduleEntity, IServiceContainer $container)
    {
        $this->module = $moduleEntity;
        $this->container = $container;
        $this->module()->setApp($this);
    }

    /**
     * @return ModuleEntity
     */
    public function module()
    {
        return $this->module;
    }

    /**
     * @param $moduleId
     * @return App
     */
    public static function getInstance($moduleId)
    {
        if( !self::$instances[$moduleId] ){
            throw new \Exception("Application for module $moduleId do not make");
        }

        return self::$instances[$moduleId];
    }

    public static function make(ModuleEntity $moduleEntity, IServiceContainer $container)
    {
        $appClass = $moduleEntity->getNamespace() . '\\App\\App';
        if(is_a($appClass, IApp::class, true)) {
            self::$instances[$moduleEntity->getId()] = new $appClass($moduleEntity, $container);
        } else {
            self::$instances[$moduleEntity->getId()] = new self($moduleEntity, $container);
        }
        return self::$instances[$moduleEntity->getId()];
    }

    /**
     * @param string $factoryName
     * @return FactoryFacade | DefaultFactoryFacade
     */
    public function factory($factoryName = '')
    {
        if( $factoryName ){
            if( strpos($factoryName, 'Factory') ){
                return $this->container()->make($factoryName);
            }

            return $this->container()->make($factoryName.'Factory');
        }
        return $this->container()->make($this->module()->getId().':factory');
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return LexerCollection|mixed
     */
    public function config($name = '', $default = null)
    {
        /**
         * @var LexerCollection $config
         */
        $config = $this->container->make($this->module()->getId().':config');
        if($name != ''){
            return $config->get($name, $default);
        }

        return $config;
    }

    /**
     * @return Defines|DefaultDefines
     */
    public function defines()
    {
        return $this->container()->make($this->module()->getId().':defines');
    }

    /**
     * @return ServiceContainer
     */
    public function container()
    {
        return $this->container;
    }

    /**
     * @param string $serviceName
     * @param array $args
     * @return ServiceFacade | DefaultServiceFacade
     */
    public function service($serviceName = '', $args = [])
    {
        $serviceName = trim($serviceName);
        if( !$serviceName ){
            $serviceName = $this->module()->getId().':services';
        }
        
        return $this->container()->make($serviceName, $args);
    }

    /**
     * @param null $env
     * @return bool|string
     */
    public function env($env = null)
    {
        $defaultEnv = $this->__env;

        if( is_array($env) ){
            return in_array($defaultEnv, $env);
        }

        if( $env ){
            return $defaultEnv == $env;
        }

        return $defaultEnv;
    }

    /**
     * @param string $name
     * @param null $default
     * @return Setting|mixed
     */
    public function setting($name = '', $default = null)
    {
        $setting = $this->container()->make($this->module()->getId().':settings');
        if($name != ''){
            return $setting->get($name, $default);
        }

        return $setting;
    }

    /**
     * @return App[]
     */
    public static function getInstances()
    {
        return self::$instances;
    }

    /**
     * @return Assets
     */
    public function assets()
    {
        return $this->container()->make($this->module()->getId().':assets');
    }

    /**
     * @return Templater
     */
    public function templater()
    {
        return $this->container()->make($this->module()->getId().':templater');
    }
}