<?php


namespace Wt\Core\App;


use Wt\Core\Exceptions\NotFoundException;
use Wt\Core\Interfaces\IServiceContainer;

class ServiceContainer implements IServiceContainer
{
    private $singleton = [];
    private $services = [];
    private $extends = [];
    private $aliases = [];

    public function bind($strClass, $object = null)
    {
        $this->services[$strClass] = $object;
        
        if( !$object ){
            $this->services[$strClass] = $strClass;
        }
        
        return $this;
    }

    public function singleton($strClass, $object = null)
    {
        $this->singleton[$strClass] = $strClass;
        
        return $this->bind($strClass, $object);
    }

    public function unBind($strClass)
    {
        if( isset($this->services[$strClass]) ){
            unset($this->services[$strClass]);
        }
        if( isset($this->singleton[$strClass]) ){
            unset($this->singleton[$strClass]);
        }
        if( isset($this->aliases[$strClass]) ){
            unset($this->aliases[$strClass]);
        }
        if( isset($this->extends[$strClass]) ){
            unset($this->extends[$strClass]);
        }
    }

    public function make($strClass, $args = [])
    {
        $item = $this->resolve($strClass, $args);

        if ($item instanceof \ReflectionClass) {
            $item = $this->getInstance($item, $args);
        }

        $strExtClass = $strClass;
        // Тут есть какой-то глубокий сакральный смысл!
        if( in_array($strClass, $this->aliases) ){
            $strExtClass = array_search($strClass, $this->aliases);
            if( !isset($this->extends[$strExtClass]) ){
                $strExtClass = $strClass;
            }
        } elseif( isset($this->aliases[$strClass]) ){
            $strExtClass = $this->aliases[$strClass];
            if( !isset($this->extends[$strExtClass]) ){
                $strExtClass = $strClass;
            }
        }

        $extClass = null;
        if( isset($this->extends[$strExtClass]) ){
            $extClass = $this->extends[$strExtClass];
        }
        if( $extClass != null ){
            if( is_callable($extClass) ){
                $item = call_user_func_array($extClass, [$item, $this]);
            } else {
                $clName = $extClass;
                $item = $this->make($clName, [$item]);
            }
        }

        return $item;
    }

    public function alias($aliasName, $realService)
    {
        if( $this->has($realService) ){
            $this->services[$aliasName] = &$this->services[$realService];
            $this->aliases[$aliasName] = $realService;
        } else {
            $this->bind($aliasName, $realService);
        }

        return $this;
    }

    public function extend($service, $callable)
    {
        $this->extends[$service] = $callable;
    }

    public function hasIn($id)
    {
        return isset($this->services[$id]);
    }

    public function has($id)
    {
        try {
            $item = $this->resolve($id);
        } catch (NotFoundException $e) {
            return false;
        }
        if ($item instanceof \ReflectionClass) {
            return $item->isInstantiable();
        }
        return isset($item);
    }

    public function set($key, $value)
    {
        $this->services[$key] = $value;
        return $this;
    }

    private function resolve($id, $args = [])
    {
        try {
            $name = $id;
            if( is_scalar($id) && isset($this->aliases[$id]) ){
                $id = $this->aliases[$id];
            }
            if ( is_scalar($id) && isset($this->services[$id]) ) {
                $name = $this->services[$id];
                if (is_callable($name) && !$this->singleton[$id]) {
                    return $name($this);
                }
            }
            if( is_scalar($id) && isset($this->singleton[$id]) && $this->singleton[$id] ){
                if( is_object($this->services[$id]) && !($this->services[$id] instanceof \Closure) ){
                    return $this->services[$id];
                }
                if( is_callable($this->services[$id]) ){
                    $name = $this->services[$id];
                    $this->services[$id] = $name($this);
                    if( is_object($this->services[$id]) && !($this->services[$id] instanceof \Closure) ){
                        return $this->services[$id];
                    }
                }
                $rc = new \ReflectionClass($this->services[$id]);
                $this->services[$id] = $this->getInstance($rc, $args);
                return $this->services[$id];
            }

            return (new \ReflectionClass($name));
        } catch (\Throwable $t) {
            $className = is_string($id)?$id:'';
            throw new NotFoundException('resolve: ' . $t->getMessage() . '; id: ' . $className, $t->getCode(), $t);
        }
    }

    private function getInstance(\ReflectionClass $item, $userArgs = [])
    {
        $constructor = $item->getConstructor();

        if (is_null($constructor) || $constructor->getNumberOfParameters() == 0) {
            return $item->newInstance();
        }

        $args = [];

        foreach ($constructor->getParameters() as $param) {
            $type = $param->getType();
            if( !$type ){
                $type = $param;
            }
            /**
             * [1]
             * @var string $typeName или класс или название переменной
             */
            $typeName = is_callable([$type, 'getName'])?$type->getName():$param->getClass()->name;
            /**
             * [2]
             * комент на случай если [1] будет бажить
             * не проверялась!
             */
            //$typeName = PHP_VERSION_ID < 70100 ? $type->getType()->__toString() : $param->getType()->getName();

            if( isset($userArgs[$param->getName()]) || array_key_exists($param->getName(), $userArgs) ){
                $args[] = $userArgs[$param->getName()];
            }elseif( isset($userArgs[$param->getPosition()]) || array_key_exists($param->getPosition(), $userArgs) ){
                $args[] = $userArgs[$param->getPosition()];
                unset($userArgs[$param->getPosition()]);
            }elseif( $this->has($typeName) ){
                $args[] = $this->make($typeName);
            }elseif( $userArgs && isset($userArgs[count($args)-$type->getPosition()]) ){ // ??
                $args[] = $userArgs[count($args)-$type->getPosition()];
                unset($userArgs[count($args)-$type->getPosition()]);
            }elseif( $param->isOptional() ){
                continue;
            }
        }

        try {
            $instance = $item->newInstanceArgs($args);
        } catch (\Exception $e) {
            throw new \Exception("ReflectionClass::newInstanceArgs: [{$item->getName()}] {$e->getMessage()}", $e->getCode(), $e);
        } catch (\Throwable $t) {
            throw new \Exception("ReflectionClass::newInstanceArgs: [{$item->getName()}] {$t->getMessage()}", $t->getCode(), $t);
        }

        return $instance;
    }
}