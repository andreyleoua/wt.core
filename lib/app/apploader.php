<?php


namespace Wt\Core\App;


use Bitrix\Main\Loader;
use Wt\Core\Assets\Assets;
use Wt\Core\Assets\PluginManager;
use Wt\Core\Facade\DefaultDefines;
use Wt\Core\Facade\DefaultFactoryFacade;
use Wt\Core\Facade\DefaultServiceFacade;
use Wt\Core\Facade\FactoryFacade;
use Wt\Core\Facade\ServiceFacade;
use Wt\Core\Facade\Defines;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Interfaces\IServiceProvider;
use Wt\Core\Migrations\EventMigration;
use Wt\Core\Migrations\FileMigration;
use Wt\Core\Migrations\MigrationService;
use Wt\Core\Migrations\RouterMigration;
use Wt\Core\Migrations\StorageMigration;
use Wt\Core\Settings\Setting;
use Wt\Core\Providers\DefaultFactoryProvider;
use Wt\Core\Providers\DefaultServiceProvider;
use Wt\Core\Support\LexerCollection;
use Wt\Core\Templater\Templater;
use Wt\Core\Tools;

class AppLoader
{
    protected $envFile = '.env';
    /**
     * @var IApp
     */
    protected $app;

    public function __construct(IApp $app)
    {
        $this->app = $app;
    }

    /**
     * @param $app IApp
     */
    public static function load(IApp $app)
    {
        $appLoader = new self($app);
        /**
         * @var $container ServiceContainer
         */
        $container = $app->container();

        $appLoader->initEnv();

        $container->singleton(get_class($container), $container);
        $appLoader->initSettings();
        $appLoader->initConfig();
        $appLoader->initDefines();
        $appLoader->initModuleInstaller();
        $appLoader->initMigration();
        $appLoader->initAssets();
        $appLoader->initTemplater();

        $config = $appLoader->app->config();

        $appLoader->loadBxModules($config->get('autoload_modules', []));
        $appLoader->registerBxEvents($config->get('events', []));

        $appLoader->initFacades([
            'services' => 'ServiceFacade',
            'factory' => 'FactoryFacade',
        ]);

        $appLoader->initDefaultProviders(['DefaultFactoryProvider', 'DefaultServiceProvider']);

        $appLoader->initProviders();
    }

    protected function initEnv()
    {
        $envFile = $this->app->module()->getExtDir().DIRECTORY_SEPARATOR.$this->envFile;
        if( is_readable($envFile) ){
            if( $env = trim(file_get_contents($envFile)) ){
                $this->app->__env = $env;
            }
        }
    }

    protected function initMigration()
    {
        $container = $this->app->container();
        $container->singleton($this->app->module()->getId() . ':migration', function (){
            $migrationService = new MigrationService($this->app->module());
            $migrationService->setMigrations([
                new StorageMigration($this->app->module()),
                new EventMigration($this->app->module()),
                new FileMigration($this->app->module()),
                new RouterMigration($this->app->module()),
            ]);

            return $migrationService;
        });
    }

    protected function initAssets()
    {
        $container = $this->app->container();
        $config = $this->app->config();

        $container->singleton($this->app->module()->getId() . ':assets', function() use ($config, $container) {
            $src = Tools::removeDocRoot(dirname($this->app->module()->getDir()));
            $namespace = $this->app->module()->getId();
            /**
             * @var $pluginManager PluginManager
             */
            $pluginManager = $container->make(PluginManager::class, [
                $src,
                $namespace
            ]);
            $pluginManager->registerPluginList($config->get('plugins', []));
            $assets = $container->make(Assets::class, [$pluginManager]);
            return $assets;
        });
    }

    protected function initTemplater()
    {
        $container = $this->app->container();

        $container->singleton($this->app->module()->getId() . ':templater', function() use ($container) {
            $path = dirname($this->app->module()->getDir());
            $namespace = $this->app->module()->getId();
            /**
             * @var $templater Templater
             */
            $templater = $container->make(Templater::class, [
                $path,
                $namespace
            ]);
            return $templater;
        });
    }

    protected function initModuleInstaller()
    {
        $container = $this->app->container();
        $app = $this->app;
        $container->singleton($app->module()->getId() . ':installer', function () use ($app){
            return \CModule::CreateModuleObject($app->module()->getId());
        });
    }

    protected function initSettings()
    {
        $setting = new Setting($this->app->module()->getId());
        $container = $this->app->container();
        $container->singleton($this->app->module()->getId() . ':settings', $setting);
    }

    protected function initConfig()
    {
        $config = [];
        $arDefConfig = [];
        if( is_readable($this->app->module()->getDir().'/default_config.php') ){
            $arDefConfig = include $this->app->module()->getDir().'/default_config.php';
            if( !is_array($arDefConfig) ){
                $arDefConfig = [];
            }
        }
        $config = array_merge($config, $arDefConfig);

        $arConfig = [];
        if( is_readable($this->app->module()->getExtDir().'/config.php') ){
            $arConfig = include $this->app->module()->getExtDir().'/config.php';
            if( !is_array($arConfig) ){
                $arConfig = [];
            }
        }
        $config = array_merge($config, $arConfig);

        $arEnvConfig = [];
        if( is_readable($this->app->module()->getExtDir().'/'.$this->app->env().'_config.php') ){
            $arEnvConfig = include $this->app->module()->getExtDir().'/'.$this->app->env().'_config.php';
            if( !is_array($arEnvConfig) ){
                $arEnvConfig = [];
            }
        }
        $config = array_merge($config, $arEnvConfig);

        $config = new LexerCollection($config);

        $container = $this->app->container();
        $container->singleton($this->app->module()->getId() . ':config', $config);
    }

    protected function initDefines()
    {
        $container = $this->app->container();
        $defines = null;

        $strClassName = ucfirst($this->app->env()).'Defines';
        $strClass = $this->app->module()->getNamespace() . '\\Facade\\' . $strClassName;
        $path = $this->app->module()->getExtDir().'/'.$strClassName.'.php';
        if( is_readable($path) ){
            include_once $path;

            if( class_exists($strClass) ){
                $defines =  new $strClass;
            }
        }

        $strClassName = 'Defines';
        $strClass = $this->app->module()->getNamespace() . '\\Facade\\' . $strClassName;
        $path = $this->app->module()->getExtDir().'/'.$strClassName.'.php';
        if(is_readable($path)) {
            include_once $path;
            if( class_exists($strClass) ){
                $defines = new $strClass;
            }
        }

        if(!$defines) {
            $defines = new DefaultDefines();
        }

        $container->singleton($this->app->module()->getId() . ':defines', $defines);
    }

    protected function initDefaultProviders($list)
    {
        foreach ($list as $strClassName){
            $strClass = $this->app->module()->getNamespace() . '\\Providers\\' . $strClassName;
            if(class_exists($strClass)){
                /**
                 * @var $realProvider IServiceProvider
                 */
                $realProvider = new $strClass();
                $realProvider->register($this->app);
            }
        }
    }

    protected function initProviders()
    {
        /**
         * @var $config LexerCollection
         */
        $config = $this->app->config();

        $providers = $config->get('providers', [])->toArray();
        $providers = array_unique($providers);

        foreach ($providers as $provider){
            if( class_exists($provider) && is_a($provider, IServiceProvider::class, true) ){
                /**
                 * @var $realProvider DefaultServiceProvider
                 */
                $realProvider = new $provider;
                $realProvider->register($this->app);
            }
        }
    }

    protected function initFacades($list)
    {
        $container = $this->app->container();
        foreach ($list as $containerKey => $strClassName) {

            $instance = null;
            $strClass = $this->app->module()->getNamespace() . '\\Facade\\' . $strClassName;
            $path = $this->app->module()->getExtDir().'/'.$strClassName.'.php';
            if( is_readable($path) ){
                include $path;

                if( class_exists($strClass) ){
                    $instance = new $strClass($this->app);
                }
            }

            if(!$instance){
                $strDefaultClassName = 'Default' . $strClassName;
                $strDefaultClass = $this->app->module()->getNamespace() . '\\Facade\\' . $strDefaultClassName;
                if(class_exists($strDefaultClass)){
                    $instance = new $strDefaultClass($this->app);
                }
            }

            $container->singleton($strClass, $instance);
            $container->alias($this->app->module()->getId() . ':' . $containerKey, $strClass);
        }

    }

    protected function loadBxModules($modules)
    {
        if( $modules->isNotEmpty() ){
            foreach($modules as $module){
                if( !Loader::includeModule($module) ){
                    throw new \Exception("Please install module \"$module\"", 100);
                }
            }
        }
    }

    /**
     * @param LexerCollection $events
     */
    protected function registerBxEvents($events)
    {
        if(!class_exists('Bitrix\Main\EventManager')){
            return;
        }
        $autoloaderClass = $this->app->module()->getNamespace().'\\Handlers\\Autoloader';
        if(is_callable([$autoloaderClass, 'OnPageStart'])){
            // Пришьем по умолчанию загрузчик
            if( $events->isEmpty() || !$events->has('main:OnPageStart') ){
                $events->set('main:OnPageStart', new LexerCollection([[$autoloaderClass, 'OnPageStart']]));
            }

            $onPageStartHandlers = $events->get('main:OnPageStart');
            $isAutoloaderOnPageStartInclude = false;
            foreach ($onPageStartHandlers as $oneHandler){
                if( (ltrim($oneHandler[0], '\\') == $autoloaderClass) && $oneHandler[1] == 'OnPageStart' ){
                    $isAutoloaderOnPageStartInclude = true;
                }
            }
            if(!$isAutoloaderOnPageStartInclude){
                $onPageStartHandlers->push(new LexerCollection([$autoloaderClass, 'OnPageStart']));
            }
        }

        $eventManager = \Bitrix\Main\EventManager::getInstance();
        foreach ($events as $bxEvent => $arHandlers) {
            $arBxEvent = explode(':', $bxEvent);

            $handlerExec = $arHandlers->toArray();

            if( is_array($handlerExec[0]) ){
                foreach ($handlerExec as $_handlerExec) {
                    $eventManager->addEventHandler($arBxEvent[0], $arBxEvent[1], $_handlerExec);
                }
            } else {
                $eventManager->addEventHandler($arBxEvent[0], $arBxEvent[1], $handlerExec);
            }
        }
    }
}