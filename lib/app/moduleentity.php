<?php


namespace Wt\Core\App;


use Bitrix\Main;
use CModule;
use DateTimeImmutable;
use Wt\Core\IFields\ModuleFields;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Migrations\MigrationService;
use Wt\Core\Support\Cli;
use Wt\Core\Support\Entity;
use Wt\Core\Tools;

class ModuleEntity extends Entity
{
    protected $fileName = '.module.json';
    protected $dateFormat = 'Y-m-d H:i:s';

    protected function getCase()
    {
        return null;
    }

    /**
     * @var IApp
     */
    protected $app;

    public function __construct($rawData)
    {
        parent::__construct($rawData);
    }

    public function save()
    {
        $filePath = $this['dir'] . DIRECTORY_SEPARATOR . $this->fileName;
        $data = $this->getRaw();
        unset($data['dir']);
        unset($data['path']);
        unset($data['extDir']);
        $json_string = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        file_put_contents($filePath, $json_string);
    }

    public function getPrefix()
    {
        return $this->getRaw('prefix');
    }

    public function getLangPrefix()
    {

        if(!$this->has('langPrefix')){
            $this->fields()->langPrefix = strtoupper($this->fields()->prefix);
        }

        return $this->get('langPrefix');
    }

    public function getId()
    {
        return $this->fields()->id;
    }

    public function __upVersion()
    {
        $data = new \DateTime();
        $data->setTime($data->format('H'), $data->format('i'), 0, 0);
        $this->fields()->date = $data->format($this->dateFormat);
        $ar = [1, 0, 0];
        $ar = (array)explode('.', $this->getVersion()) + $ar;
        $ar[2]++;
        $this->fields()->version = implode('.', $ar);
        return $this;
    }

    public function getNamespace()
    {
        return $this->fields()->namespace;
    }

    /**
     * @param string $branch
     * @return ModuleEntity
     * @throws null
     */
    public function getRemoteModuleEntity(string $branch = 'master')
    {
        if(!$this->fields()->gitUrl){
            return null;
        }
        $httpClient = new \Bitrix\Main\Web\HttpClient();
        $data = $httpClient->get($this->fields()->gitUrl."raw/$branch/{$this->fileName}");
        if($data === false){
            return null;
        }
        $module = json_decode($data, true);
        if(!is_array($module)){
            return null;
        }

        return resolve(ModuleEntity::class, [$module]);
    }

    public function getAutoloadPath()
    {
        return $this->getPath() . DIRECTORY_SEPARATOR  . 'lib';
    }

    public function getPath()
    {
        return $this->getDir();
    }

    public function getSrc()
    {
        return Tools::removeDocRoot($this->getPath());
    }

    public function getDir()
    {
        return $this->fields()->dir;
    }

    protected function loadExtDir()
    {
        $extDirKey1 = strtoupper($this['prefix']).'_APP_EXT_DIR';
        $extDirKey2 = strtolower($this['prefix']).'_app_ext_dir';
        $extDirKey3 = $this['prefix'].'_APP_EXT_DIR';
        $this['extDir'] = (defined($extDirKey1)?constant($extDirKey1):null)
            ?:(defined($extDirKey2)?constant($extDirKey2):null)
                ?:(defined($extDirKey3)?constant($extDirKey3):null)
                    ?:$this['dir'] . DIRECTORY_SEPARATOR . 'src';
    }

    public function getExtDir()
    {
        if(!$this->has('extDir')){
            $this->loadExtDir();
        }
        return $this->fields()->extDir;
    }

    public function getDate()
    {
        return DateTimeImmutable::createFromFormat($this->dateFormat, $this->fields()->date);
    }

    /**
     * @param bool $extResult
     * @return array|bool
     * @throws null
     */
    public function checkDeps($extResult = false)
    {
        $r = [];
        foreach ($this->get('deps', []) as $moduleId){
            $r[$moduleId] = Main\Loader::includeModule($moduleId);
            if (!$r[$moduleId] && !$extResult) {
                return false;
            }
        }
        return $r;
    }

    public function getMessage($code, $aReplace=null)
    {
        return GetMessage($this->getLangCode($code), $aReplace);
    }

    /**
     * возвращает код с префиксом
     *
     * @param $code
     * @return string
     */
    public function getLangCode($code)
    {
        if(preg_match('/^'.$this->fields()->prefix.'_/i', $code)){
            return $code;
        }
        return  strtoupper($this->fields()->prefix . '_' . $code);
    }

    public function getVersion()
    {
        return $this->fields()->version;
    }

    /**
     * @return MigrationService
     */
    public function migration()
    {
        return $this->app->container()->make($this->getId() . ':migration');
    }

    public function setApp(IApp $app)
    {
        $this->app = $app;
    }

    /**
     * @return IApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @return CModule
     */
    public function getInstaller()
    {
        return $this->app->container()->make($this->getId() . ':installer');
    }

    public function getBranch()
    {
        $branch = null;
        /** @var Cli $cli */
        $cli = resolve(Cli::class);
        $cli->cd($this->getPath());
        /**
         * ['  master', '* test/0001',]
         */
        $r = $cli->exec('git rev-parse --abbrev-ref HEAD');
        if($r->isSuccess()) {
            $branch = $r->getOutputString();
        }

        return $branch;
    }

    public function getCommitShortHash()
    {
        $hash = '';
        /** @var Cli $cli */
        $cli = resolve(Cli::class);
        $cli->cd($this->getPath());
        $r = $cli->exec('git rev-parse --short HEAD');
        if($r->isSuccess()) {
            $hash = trim($r->getOutputString());
        }

        return $hash;
    }

    public function getCommitHash()
    {
        $hash = '';
        /** @var Cli $cli */
        $cli = resolve(Cli::class);
        $cli->cd($this->getPath());
        $r = $cli->exec('git rev-parse HEAD');
        if($r->isSuccess()) {
            $hash = trim($r->getOutputString());
        }

        return $hash;
    }

    /**
     * @return ModuleFields
     */
    public function fields()
    {
        return parent::fields();
    }
}