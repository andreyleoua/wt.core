<?php


namespace Wt\Core\App;


abstract class ADefines
{
    private $mapConstants = [];
    private $propertyIdMapCode = [];
    private $propertyCodeMapId = [];
    private $reflection;

    protected $codeAliases = [];

    public function __construct()
    {
        $this->reflection = new \ReflectionClass($this);
        $this->parseConstants($this->reflection->getConstants());
    }

    public function get($name)
    {
        $key = str_replace('.', '_', strtoupper($name));

        if( isset($this->mapConstants[$key]) ){
            return $this->mapConstants[$key];
        }

        return null;
    }

    public function initProperties()
    {
        // ...
    }

    public function iblockIdByCode($code)
    {
        $upperCode = strtoupper($code);
        if( isset($this->mapConstants['IBLOCK_'.$upperCode.'_ID']) ){
            return $this->mapConstants['IBLOCK_'.$upperCode.'_ID'];
        }

        return null;
    }

    public function propertyIdByCode($code)
    {
        $upperCode = strtoupper($code);
        if( isset($this->propertyCodeMapId[$upperCode]) ){
            return $this->propertyCodeMapId[$upperCode];
        }

        return null;
    }

    public function propertyCodeById($id)
    {
        return $this->propertyIdMapCode[$id];
    }

    public function __get($name){
        if( isset($this->constants[$name]) ){
            return $this->constants[$name];
        }
    }

    protected function parseConstants(array $getConstants)
    {
        foreach ($getConstants as $constant => $val){
            if(preg_match('/^PROPERTY_(.*)+?_ID/', $constant, $m)){
                $this->propertyCodeMapId[$m[1]] = $val;
                $this->propertyIdMapCode[$val]= $m[1];
                if( isset($this->codeAliases[$m[1]]) ){
                    $this->propertyCodeMapId[$this->codeAliases[$m[1]]] = $val;;
                }
            }

            $arItems = explode('_', $constant);

            $allCombinations = call_user_func_array([$this, 'getCombinations1'], $arItems);
            foreach ($allCombinations as $item){
                $this->mapConstants[$item] = $val;
            }
        }
    }

    protected function getCombinations1()
    {
        $result = [];
        $arInput = func_get_args();

        foreach ($arInput as $pIdx => $primaryKey){
            $str = $primaryKey;
            foreach ($arInput as $sIdx => $name){
                if( $pIdx != $sIdx ){
                    $str .= '_'.$name;
                }
            }
            $result[] = $str;
        }

        $arResult = $result;
        foreach ($this->codeAliases as $realCode => $alias){
            foreach ($result as $item){
                $arResult[] = str_replace($realCode, $alias, $item);
            }
        }

        return $arResult;
    }

    protected function getCombinations()
    {
        $arrays = func_get_args();
        $result = [[]];
        foreach ($arrays as $property => $property_values) {
            $tmp = [];
            foreach ($result as $result_item) {
                foreach ($property_values as $property_value) {
                    $tmp[] = array_merge($result_item, [$property => $property_value]);
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}