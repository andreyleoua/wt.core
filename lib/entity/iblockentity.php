<?php

namespace Wt\Core\Entity;

use Wt\Core\Entity\Shop\CatalogEntity;
use Wt\Core\Factory\IBlockFactory;
use Wt\Core\IFields\IBlockFields;

class IBlockEntity extends AEntity
{

    /**
     * @return IBlockFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') === 'Y';
    }

    public function isSectionProperty()
    {
        return $this->getRaw('SECTION_PROPERTY', 'N') === 'Y';
    }

//    public function getSectionPropertyLinkArray($sectionId = 0, $bNewSection = false)
//    {
//        return \CIBlockSectionPropertyLink::GetArray($this->getId(), $sectionId, $bNewSection);
//    }

    public function getCode()
    {
        return $this->getRaw('CODE');
    }

    public function getName()
    {
        return $this->getRaw()['NAME'];
    }

    public function getTypeId()
    {
        return $this->getRaw()['IBLOCK_TYPE_ID'];
    }

    public function getAdminUrl()
    {
        return "/bitrix/admin/iblock_edit.php?type={$this->getTypeId()}&ID={$this->getId()}&admin=Y";
    }

    /**
     * @return array
     */
    public function getSites()
    {
        $arSites = [];
        //$rsSites = \CIBlock::GetSite($this->getId());

        $connection = app()->service()->bitrix()->gDb();

        $strSql = 'SELECT BS.* FROM b_iblock_site BS WHERE BS.IBLOCK_ID=' .$this->getId();
        $rsSites = $connection->Query($strSql);

        while($arSite = $rsSites->Fetch()) {
            $arSites[] = $arSite['SITE_ID'];
        }
        return $arSites;
    }

    /**
     * @return CatalogEntity
     * @throws null
     */
    public function getCatalogEntity()
    {
        $factory = app()->factory()->shop()->getCatalogFactory();
        return $factory->getEntityById($this->getId());
    }

    /**
     * @return IBlockFields
     */
    public function fields()
    {
        return parent::fields();
    }
}