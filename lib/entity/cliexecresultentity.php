<?php


namespace Wt\Core\Entity;


use Wt\Core\Support\Entity;

class CliExecResultEntity extends Entity
{

    public function getCommand()
    {
        return $this->rawData['command'];
    }

    public function getCode()
    {
        return $this->rawData['code'];
    }

    public function getStatus()
    {
        return $this->rawData['status'];
    }

    public function getOutputArray()
    {
        return is_array($this->rawData['outputArray'])?$this->rawData['outputArray']:[];
    }

    public function convertTo($encoding = 'UTF-8')
    {
        $outputArray = $this->getOutputArray();
        if(extension_loaded('iconv')){
            foreach ($outputArray as &$str){
                $enc = mb_detect_encoding($str);
                $str = iconv($enc?:'CP866', $encoding, $str);
            }
        }
        $this->rawData['outputArray'] = $outputArray;
        return $this;
    }

    public function getOutputString()
    {
        return implode(PHP_EOL, $this->getOutputArray());
    }

    public function getOutputHtml()
    {
        return nl2br($this->getOutputString());
    }

    public function isSuccess()
    {
        if($this->has('status')){
            return $this->get('status') == 'success';
        }
        return $this->getCode() == 0;
    }

    public function getComment()
    {
        return $this->rawData['comment'];
    }

    public function getDir()
    {
        return $this->rawData['dir'];
    }
}