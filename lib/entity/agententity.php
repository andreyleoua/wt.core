<?php


namespace Wt\Core\Entity;


use Bitrix\Main\Type\DateTime;
use CAgent;
use Wt\Core\Factory\AgentFactory;
use Wt\Core\IFields\AgentFields;

class AgentEntity extends AEntity
{

    public function delete()
    {
        if ($this->getId()) {
            CAgent::Delete($this->getId());
        }
    }

    public function update($data)
    {
        if ($this->getId()) {
            CAgent::Update($this->getId(), $data);
        }
    }

    /**
     * @return AgentFactory|null
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    protected $cacheEntity;

    /**
     * @return AgentEntity
     */
    public function find()
    {
        if ($this->cacheEntity) {
            return $this->cacheEntity;
        }
        $filter = [
            'NAME' => '%' . trim($this->getName(), " \t\n\r\0\x0B\\;") . '%',
        ];
        if ($this->getModuleId()) {
            $filter['MODULE_ID'] = $this->getModuleId();
        }

        if ($this->getFactory()) {
            $this->cacheEntity = $this->getFactory()->getCollection($filter)->first();
        } else {
            $this->cacheEntity = app()->factory()->main()->getAgentFactory()->getCollection($filter)->first();
        }

        return $this->cacheEntity;
    }

    public function isExists()
    {
        //        $filter = [
        //            'NAME' => '%' . trim($this->getName(), " \t\n\r\0\x0B\\;"). '%',
        //        ];
        //        if($this->getModuleId()){
        //            $filter['MODULE_ID'] = $this->getModuleId();
        //        }
        //        $res = \CAgent::GetList([], $filter);
        //        if ($res && $ar = $res->Fetch()) {
        //            return true;
        //        }
        return $this->getId() || $this->find();
    }

    public function getName()
    {
        return $this->getRaw('NAME', '');
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') === 'Y';
    }

    /**
     * Переодические агенты - агенты, время запуска которого определяется как время НАЧАЛА работы метода + интервал. IS_PERIOD = Y
     * Непереодические агенты - агенты, время запуска которого определяется как время ОКОНЧАНИЯ работы метода + интервал. IS_PERIOD = N
     * @return bool
     */
    public function isPeriod()
    {
        return $this->getRaw('IS_PERIOD') === 'Y';
    }

    public function getModuleId()
    {
        return $this->getRaw('MODULE_ID', '');
    }

    public function getInterval()
    {
        return $this->getRaw('AGENT_INTERVAL', 60);
    }

    public function isWorking()
    {
        if (!$this->getId()) {
            return false;
        }
        if ($this->isRunning()) {
            return true;
        }
        if ($this->getRaw('LAST_EXEC')) {
            $dataTime = new DateTime($this->getRaw('LAST_EXEC'));
            if (time() > $dataTime->getTimestamp() + 600 + $this->getInterval()) {
                return false;
            }
        } elseif ($this->getRaw('NEXT_EXEC')) {
            $dataTime = new DateTime($this->getRaw('NEXT_EXEC'));
            if ($dataTime->getTimestamp() + 60 < time()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        if (!$this->getId()) {
            return false;
        }
        global $DB;
        $strSql = "SELECT ID, RUNNING FROM b_agent WHERE ID={$this->getId()}";
        $dbResult = $DB->Query($strSql);
        $agentArray = $dbResult->Fetch();
        return is_array($agentArray) && $agentArray['RUNNING'] === 'Y';
    }

    public function extendDataCheck($sec = 600)
    {
        global $DB;
        $strSql = "UPDATE b_agent SET DATE_CHECK=DATE_ADD(now(), INTERVAL $sec SECOND) WHERE ID={$this->getId()}";
        $DB->Query($strSql);
    }

    /**
     * @return AgentFields
     */
    public function fields()
    {
        return parent::fields();
    }
}