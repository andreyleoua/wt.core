<?php

namespace Wt\Core\Entity\IBlock;

use Exception;
use Wt\Core\Collection\IBlock\ElementCollection;
use Wt\Core\Collection\IBlock\PropertyCollection;
use Wt\Core\Collection\IBlock\SectionCollection;
use Wt\Core\Collection\IBlock\SectionPropertyCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\IBlockEntity;
use Wt\Core\Factory\IBlock\SectionFactory;
use Wt\Core\IFields\IBlock\SectionFields;

class SectionEntity extends AEntity
{
    protected $sectionPageUrl = '';

	public function hasParent()
	{
		return $this->getParentId() > 0;
	}

	public function getName()
    {
        return $this->getRaw('NAME');
    }

	public function getCode()
    {
        return $this->getRaw('CODE');
    }

    /**
     * @return $this
     */
	public function getRoot()
    {
        if( $this->hasParent() ){
            return $this->getParent()->getRoot();
        }

        return $this;
    }

    public function isRoot()
    {
        return !$this->hasParent();
    }

    /**
     * @return bool
     */
	public function isActive()
	{
        return $this->getRaw('ACTIVE', 'N') === 'Y';
	}

    /**
     * @return $this|null
     */
	public function getParentId()
	{
        return (int)$this->getRaw('IBLOCK_SECTION_ID', 0);
	}

    /**
     * @return $this|null
     * @throws Exception
     */
	public function getParent()
	{
        if(!$this->getBlockId()){
            throw new Exception('IBlock id is empty');
        }
        if(!$this->getParentId()){
            return null;
        }
        $af = app()->factory()->iBlock()->getEntityById($this->getBlockId());
        return $af->getSectionFactory()->getEntityById($this->getParentId());
	}

    /**
     * @return SectionCollection|SectionEntity[]
     * @throws Exception
     */
	public function getChildren()
	{
        if(!$this->getBlockId()){
            throw new Exception('IBlock id is empty');
        }
        if(!$this->getId()){
            throw new Exception('Id is empty');
        }
        $af = app()->factory()->iBlock()->getEntityById($this->getBlockId());
        return $af->getSectionFactory()->getChildrenCollection($this->getId());
	}

	public function getAdminUrl()
    {
        $lang = LANGUAGE_ID;
        return '/bitrix/admin/iblock_section_edit.php?'
            . "IBLOCK_ID={$this->getBlockId()}&type={$this->getIBlockEntity()->getTypeId()}&ID={$this->getId()}&find_section_section={$this->getParentId()}&lang=$lang";
    }

	public function getContentAdminUrl()
    {
        return "/bitrix/admin/iblock_list_admin.php?IBLOCK_ID={$this->getBlockId()}&type={$this->getIBlockEntity()->getTypeId()}&find_section_section={$this->getId()}&SECTION_ID={$this->getId()}&apply_filter=Y";
    }

    /**
     * Не включает текущий раздел!!!
     *
     * @return SectionCollection|SectionEntity[]
     * @throws null
     */
    public function getSectionChain($withCurrent = false)
    {
//        $af = app()->factory()->iBlock()->getEntityById($this->getBlockId());
//        $result = $af->getSectionFactory()->getEntityByResults([]);

        $result = $this->getFactory()->getEntityByResults([]);

        if($section = $this->getParent()){
            $result->prepend($section, $section->getId());

            while (($section = $section->getParent()) && !$result->has($section->getId())){
                $result->prepend($section, $section->getId());
            }
        }
        if($withCurrent){
            $result->set($this->getId(), $this);
        }

        return $result;
    }

    /**
     * @return SectionPropertyCollection|SectionPropertyEntity[]
     */
    public function getSectionPropertyCollection()
    {
        $af = app()->factory()->iBlock()->getEntityById($this->getBlockId());
        $sectionPropertyFactory = $af->getSectionPropertyFactory($this->getId());
        return $sectionPropertyFactory->getCollection();
    }

    /**
     * @return PropertyCollection|PropertyEntity[]
     */
    public function getPropertyCollection()
    {
        return $this->getSectionPropertyCollection()->getPropertyCollection();
    }

    /**
     * @return SectionPropertyCollection|SectionPropertyEntity[]
     * @throws null
     */
    public function getInheritedSectionPropertyCollection()
    {
        $af = app()->factory()->iBlock()->getEntityById($this->getFactory()->getBlockId());
        $sectionPropertyFactory = $af->getSectionPropertyFactory(0);
        $sectionPropertyCollection = $sectionPropertyFactory->getCollection();
        $sectionPropertyCollection->__setFactory($sectionPropertyFactory->cache([]));

        return $this->getSectionChain(true)->reduce(function(SectionPropertyCollection $sectionPropertyCollection, SectionEntity $entity){
            return $sectionPropertyCollection->plus($entity->getSectionPropertyCollection()->all());
        }, $sectionPropertyCollection);
    }

    /**
     * @return PropertyCollection|PropertyEntity[]
     */
    public function getInheritedPropertyCollection()
    {
        return $this->getInheritedSectionPropertyCollection()->getPropertyCollection();
    }

    /**
     * @return SectionFactory
     * @throws null
     */
	public function getFactory()
    {
        return parent::getFactory(); // TODO: Change the autogenerated stub
    }
    /**
     *
     * @return IBlockEntity
     */
    public function getIBlockEntity()
    {
        $factory = factory()->main()->getIBlockFactory();
        return $factory->getEntityById($this->getBlockId());
    }

    public function getBlockId()
    {
        return (int)$this->getRaw('IBLOCK_ID', 0);
    }

    /**
     *
     * @param bool $includeSubsections
     * @return array
     */
    public function getElementIds($includeSubsections = false)
    {
        $block = app()->factory()->iBlock()->getEntityById($this->getBlockId());

        $collection = $block->getElementFactory()->getCollection([
            'SECTION_ID' => $this->getId(),
            'INCLUDE_SUBSECTIONS' => $includeSubsections?'Y':'N',
        ], [], ['ID']);

        return $collection->pluck('ID')->toArray();
    }

    /**
     *
     * @param bool $includeSubsections
     * @return ElementCollection|ElementEntity[]
     */
    public function getElementCollection($includeSubsections = false)
    {
        $block = app()->factory()->iBlock()->getEntityById($this->getBlockId());

        return $block->getElementFactory()->getCollection([
            'SECTION_ID' => $this->getId(),
            'INCLUDE_SUBSECTIONS' => $includeSubsections?'Y':'N',
        ]);
    }

    public function update($arData)
    {
        if( isset($arData['ID']) ){
            unset($arData['ID']);
        }

        $bs = new \CIBlockSection();
        $bs->Update($this->getRaw()['ID'], $arData);

        //$this->reInitRawData(array_merge($this->getRaw(), $arData));
    }

    public function getUrl()
    {
        return $this->getSectionPageUrl();
    }

    /**
     * @return string
     * @deprecated
     */
    public function getSectionPageUrl()
    {
        if( $this->sectionPageUrl ){
            return $this->sectionPageUrl;
        }

        $sectionCodePath = '';

        $arItem = $this->getRaw();
        if( str_contains($arItem['SECTION_PAGE_URL'], '#SECTION_CODE_PATH#') ){
            $sectionCodePath = $this->getSectionCodePath();
        }

        $arItem['SECTION_PAGE_URL'] = str_replace(
            [
                '#SITE_DIR#',
                '#ID#',
                '#SECTION_ID#',
                '#CODE#',
                '#SECTION_CODE#',
                '#SECTION_CODE_PATH#'
            ],
            [
                '/',
                $arItem['ID'],
                $arItem['ID'],
                $arItem['CODE'],
                $arItem['CODE'],
                $sectionCodePath
            ],
            $arItem['SECTION_PAGE_URL']
        );
        $arItem['SECTION_PAGE_URL'] = preg_replace('~//+~', '/', $arItem['SECTION_PAGE_URL']);
        $this->sectionPageUrl = $arItem['SECTION_PAGE_URL'];
        $this->rawData['SECTION_PAGE_URL'] = $arItem['SECTION_PAGE_URL'];

        return $this->sectionPageUrl;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getSectionCodePath()
    {
        return $this->getSectionChain(true)->pluck('CODE')->filter()->join('/');
    }

    /**
     * @return SectionFields
     */
    public function fields()
    {
        return $this;
    }
}