<?php

namespace Wt\Core\Entity\IBlock;

use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\IBlock\SectionPropertyFactory;
use Wt\Core\IFields\IBlock\SectionPropertyFields;

class SectionPropertyEntity extends AEntity
{

    /**
     * @return SectionPropertyFactory
     * @throws null
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    public function getIBlockId()
    {
        return $this->getRaw('IBLOCK_ID');
    }

    /**
     * равно Id
     *
     * @return mixed
     */
    public function getPropertyId()
    {
        return $this->getRaw('PROPERTY_ID');
    }

    public function getSectionId()
    {
        return $this->getRaw('SECTION_ID');
    }

    /**
     * @return PropertyEntity|null
     */
    public function getPropertyEntity()
    {
        $af = app()->factory()->iBlock()->getEntityById($this->getIBlockId());
        return $af->getPropertyFactory()->getEntityById($this->getPropertyId());
    }

    public function getFilterHint()
    {
        return $this->getRaw('FILTER_HINT', '');
    }

    /**
     * Показать в умном фильтре
     *
     * @return bool
     */
    public function isSmartFilter()
    {
        return $this->getRaw('SMART_FILTER', 'N') === 'Y';
    }

    /**
     * Вид в умном фильтре
     *
     * Число
     * A - Число от-до, с ползунком
     * B - Число от-до
     *
     * Список, Справочник
     * F - Флажки
     * K - Радиокнопки
     * P - Выпадающий список
     *
     * Справочник
     * G - Флажки с картинками
     * H - Флажки с названиями и картинками
     * R - Выпадающий список с названиями и картинками
     *
     * @return string
     */
    public function getDisplayType()
    {
        return $this->getRaw('DISPLAY_TYPE', '');
    }

    /**
     * Показать развёрнутым
     *
     * @return bool
     */
    public function isDisplayExpanded()
    {
        return $this->getRaw('DISPLAY_EXPANDED', 'N');
    }

    /**
     * @return SectionPropertyFields
     */
    public function fields()
    {
        return parent::fields();
    }
}