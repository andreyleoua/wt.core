<?php

namespace Wt\Core\Entity\IBlock;

use CIBlockElement;
use Wt\Core\Collection\IBlock\SectionCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\Shop\CatalogEntity;
use Wt\Core\Entity\IBlockEntity;
use Wt\Core\Factory\IBlock\ElementFactory;
use Wt\Core\Factory\IBlock\PropertyValueFactory;
use Wt\Core\IFields\IBlock\ElementFields;
use Wt\Core\Property\PropertyValueEntity;
use Wt\Core\Property\PropertyValueCollection;
use Wt\Core\Support\Error;
use Wt\Core\Support\Result;

class ElementEntity extends AEntity
{

    /**
     * @var SectionEntity $sectionEntity
     */
    protected $sectionEntity;

    public function getName()
    {
        return $this->getRaw('NAME', '');
    }

    public function getCode()
    {
        return $this->getRaw('CODE', '');
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') === 'Y';
    }

    /**
     * @return SectionEntity
     * @throws null
     */
	public function getSectionEntity()
	{
	    if(!$this->sectionEntity && $this->getSectionId()){
            $factory = app()->factory()->iBlock()->getEntityById($this->getFactory()->getBlockId())->getSectionFactory();
	        $this->sectionEntity = $factory->getEntityById($this->getSectionId());
        }
		return $this->sectionEntity;
	}

    /**
     * @return SectionCollection
     * @throws null
     */
	public function getSectionChain()
    {
        /**
         * @var SectionCollection $result
         */
        $result = resolve(SectionCollection::class, [[]]);

        if($section = $this->getSectionEntity()){
            $result->prepend($section, $section->getId());

            while (($section = $section->getParent()) && !$result->has($section->getId())){
                $result->prepend($section, $section->getId());
            }
        }

        return $result;
    }

    public function getBlockId()
    {
        $blockId = (int)$this->getRaw('IBLOCK_ID', 0);
        if(!$blockId && $this->getFactory()){
            $blockId = $this->getFactory()->getBlockId();
        }
        return $blockId;
    }

    /**
     * @return IBlockEntity
     */
	public function getIBlockEntity()
	{
        $factory = app()->factory()->main()->getIBlockFactory();
		return $factory->getEntityById($this->getBlockId());
	}

    /**
     * @return CatalogEntity
     */
	public function getCatalogEntity()
	{
        $factory = app()->factory()->shop()->getCatalogFactory();
		return $factory->getEntityById($this->getBlockId());
	}

    /**
     * @return bool
     */
    public function canContainOffers()
    {
        return $this->getCatalogEntity()->canContainOffers();
    }

    /**
     * @return PropertyValueFactory
     */
    public function getPropertyValueFactory()
    {
        return $this->getFactory()->getFactoryInstance(PropertyValueFactory::class, [$this->getBlockId(), $this->getId()]);
    }

    /**
     * @return PropertyValueCollection
     */
    public function getPropertyValueCollection()
    {
        $propertyFactory = app()->factory()->iBlock()->getEntityById($this->getBlockId())->getPropertyFactory();
        $propertyCollection = $propertyFactory->getCollection();
//        pre($propertyCollection->toArray());
        $propertyValueCollection = $this->getPropertyValueFactory()->getCollection();
//        pre($propertyValueCollection->getRaw());

        $list = $propertyCollection
            ->map(function (PropertyEntity $propertyEntity) use ($propertyValueCollection) {
                $restoredValue = $propertyEntity->getRestoredValue($propertyValueCollection->get($propertyEntity->getId()));
                //[VALUE] =>
                //[VALUE_ENUM] => 0
                //[VALUE_NUM] => 0
                //[DESCRIPTION] =>
                return new PropertyValueEntity($restoredValue, $this->getPropertyValueFactory(), $this, $propertyEntity);
            })->all();
        return new PropertyValueCollection($list, $this->getPropertyValueFactory(), $this);
    }

    public function getSectionIds()
    {
        $result = [];
        $dbElementGroups = CIBlockElement::GetElementGroups(
            $this->getId(),
            true,
            ['ID', 'IBLOCK_ID']
        );

        while ($elementGroup = $dbElementGroups->GetNext(true, false)) {
            $sid = $elementGroup['ID'];
            $bid = $elementGroup['IBLOCK_ID'];
            if($bid == $this->getBlockId()){
                $result[$sid] = $sid;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getSecondarySectionIdList()
    {
        return $this->getSectionIds();
    }

    public function getSectionId()
    {
        return (int)$this->getRaw('IBLOCK_SECTION_ID', 0);
    }

    /**
     * @deprecated not use
     * @return mixed
     */
    public function getPrimarySectionId()
    {
        return $this->getRaw('IBLOCK_SECTION_ID', 0);
    }

    /**
     * @param array $secondarySectionIdList
     * @return Result
     */
    public function updateSecondarySectionIdList($secondarySectionIdList)
    {
        $id = $this->getId();
        $result = Result::make();
        $result->setId($id);
        if(!$this->getSectionId()){
            $result->setError('Element not primary section');
            return $result;
        }
        $fields = [
            'IBLOCK_SECTION_ID' => $this->getSectionId(),
            'IBLOCK_SECTION' => collect($secondarySectionIdList)->prepend($this->getSectionId())->toArray(),
        ];
        return $this->update($fields);
    }

    /**
     * @param $data
     * @return Result
     */
	public function update($data)
	{
        $data['IBLOCK_ID'] = $this->getBlockId();
	    $id = $this->getId();
	    $result = Result::make();
        $result->setId($id);
        $result->setData($data);
        if(!$id){
            $result->addError(new Error('Update Error! Id is empty!'));
            return $result;
        }
		$bs = new CIBlockElement();
		$r = $bs->Update($id, $data);
		if(!$r){
		    if($bs->LAST_ERROR) {
                $result->addError($bs->LAST_ERROR);
            } else {
                $result->addError(new Error('Update Error!'));
            }
        }
        return $result;
	}


	protected $url;

    public function getUrl()
    {
        if( $this->url ){
            return $this->url;
        }

        $sectionCode = '';
        $sectionCodePath = '';

        $arItem = $this->getRaw();

        if(str_contains($arItem['DETAIL_PAGE_URL'], '#SECTION_CODE_PATH#') && $arItem['IBLOCK_SECTION_ID']){
            $sectionEntity = $this->getSectionEntity();
            $sectionCode = $sectionEntity->getCode();
            $sectionCodePath =  $sectionEntity->getSectionCodePath();
        } elseif( str_contains($arItem['DETAIL_PAGE_URL'], '#SECTION_CODE#') && $arItem['IBLOCK_SECTION_ID'] ){
            $sectionEntity = $this->getSectionEntity();
            $sectionCode = $sectionEntity->getCode();
        }

        $arItem['DETAIL_PAGE_URL'] = str_replace(
            [
                '#SITE_DIR#',
                '#ID#',
                '#SECTION_ID#',
                '#CODE#',
                '#ELEMENT_CODE#',
                '#SECTION_CODE#',
                '#SECTION_CODE_PATH#'
            ],
            [
                '/',
                $arItem['ID'],
                $arItem['IBLOCK_SECTION_ID'],
                $arItem['CODE'],
                $arItem['CODE'],
                $sectionCode,
                $sectionCodePath
            ],
            $arItem['DETAIL_PAGE_URL']
        );
        $arItem['DETAIL_PAGE_URL'] = preg_replace('~//+~', '/', $arItem['DETAIL_PAGE_URL']);
        $this->url = $arItem['DETAIL_PAGE_URL'];
        return $this->url;
    }

    public function getAdminUrl()
    {
        $languageId = LANGUAGE_ID;
        $iblockTypeId = $this->getRaw()['IBLOCK_TYPE_ID'];
        $id = $this->getId();
        $iblockSectionId = $this->getSectionId();
        $iblockId = $this->getBlockId();

        $url = "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=$iblockId&type=$iblockTypeId&ID=$id&find_section_section=$iblockSectionId";

        if(0) {
            /**
             * Получение ссылки методами битрикс. Результат будет только при наличии прав доступа
             */
            $arButtons = \CIBlock::GetPanelButtons(
                $this->getBlockId(),
                $this->getId(),
                $this->getSectionId(),
                ['RETURN_URL' => '/']
            );

            $uri = new \Bitrix\Main\Web\Uri($arButtons['edit']['edit_element']['ACTION_URL']);
            $uri->addParams([
                'find_section_section' => $this->getSectionId()
            ]);
            $uri->deleteParams(['bxpublic', 'return_url', 'filter_section', 'force_catalog']);
            $url = $uri->getUri();
        }
        return $url;
    }

    /**
     * @return ElementFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    /**
     * @return ElementFields
     */
    public function fields()
    {
        return parent::fields();
    }
}