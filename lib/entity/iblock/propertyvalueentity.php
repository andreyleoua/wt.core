<?php

namespace Wt\Core\Entity\IBlock;

use Bitrix\Main\Result;
use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\IBlock\PropertyValueFactory;
use Wt\Core\Factory\IBlock\PropertyFactory;
use Wt\Core\IFields\IBlock\PropertyValueFields;

class PropertyValueEntity extends AEntity
{
    /**
     * @param $value
     * @return Result
     */
	public function setValue($value)
    {
        $result = new Result();

        //Способ 1
        $prop = [];
        $prop[$this->getId()] = $value;
        \CIBlockElement::SetPropertyValuesEx($this->getElementId(), $this->getFactory()->getBlockId(), $prop);

        //Способ 2
//        $el = new \CIBlockElement;
//        $arLoadProductArray = Array(
//            'PROPERTY_VALUES' => [$this->getIBlockProperty()->getId() => $value],
//        );
//        $res = $el->Update($this->getFactory()->getElementId(), $arLoadProductArray, true, true, true);
//        if(!$res){
//            $result->addError(new Error($el->LAST_ERROR));
//        }

        $this->clearCache();

        return $result;
    }

    public function getValue()
    {
        return $this->getRaw('VALUE');
    }

    public function getElementId()
    {
        return $this->getFactory()->getElementId();
    }

    /**
     * @return PropertyFactory
     */
    public function getPropertyFactory()
    {
        if($this->getFactory()){
            return app()->factory()->iBlock()->getEntityById($this->getFactory()->getBlockId())->getPropertyFactory();
        }
        return null;
    }

    public function getDescription()
    {
        return $this->getRaw('DESCRIPTION');
    }

    /**
     * @return PropertyValueFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    /**
     * @return PropertyValueFields
     */
    public function fields()
    {
        return parent::fields();
    }

    public function clearCache()
    {
        $this->getFactory()->clearCacheById($this->getId(), $this->getElementId());
    }

    /**
     * @deprecated
     * @return void
     */
    public function getCode()
    {
        // TODO: Implement getCode() method.
    }
}