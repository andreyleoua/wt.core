<?php

namespace Wt\Core\Entity\IBlock;

use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\APropertyEntity;
use Wt\Core\Factory\IBlock\PropertyFactory;
use Wt\Core\IFields\IBlock\PropertyFields;
use Wt\Core\Tools;

class PropertyEntity extends APropertyEntity
{

    public function getName()
    {
        return $this->getRaw('NAME');
    }

    /**
     * @return PropertyFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    public function getCode()
    {
        return $this->getRaw()['CODE'];
    }

    public function isMultiple(): bool
    {
        return $this->getRaw('MULTIPLE', 'N') === 'Y';
    }

    public function isRequired(): bool
    {
        return $this->getRaw('IS_REQUIRED', 'N') === 'Y';
    }

    /**
     * UserID - Привязка к пользователю
     * DateTime - Дата/Время
     * EList - Привязка к элементам в виде списка
     * FileMan - Привязка к файлу (на сервере)
     * map_yandex - Привязка к Yandex.Карте
     * HTML - HTML/текст
     * map_google - Привязка к карте Google Maps
     * ElementXmlID - Привязка к элементам по XML_ID
     * Sequence - Счетчик
     * EAutocomplete - Привязка к элементам с автозаполнением
     * SKU - Привязка к товарам (SKU)
     * video - Видео
     * TopicID - Привязка к теме форума
     * directory - Привязка к справочнику
     *
     * @return string
     */
    public function getUserType(): string
    {
        return (string)$this->getRaw('USER_TYPE', '');
    }

    public function getHint(): string
    {
        return (string)$this->getRaw('HINT');
    }

    public function getAdminUrl()
    {
        return "/bitrix/admin/iblock_edit_property.php?ID={$this->getId()}&IBLOCK_ID={$this->getFactory()->getBlockId()}";
    }

    /**
     * S - строка
     * N - число
     * L - список
     * F - файл
     * G - привязка к разделу
     * E - привязка к элементу
     *
     * @return string
     */
    public function getBaseType(): string
    {
        return (string)$this->getRaw('PROPERTY_TYPE');
    }

    public function getTimestampX()
    {
        $dateTimeValue = $this->get('TIMESTAMP_X', '');
        try {
            $time = new \Bitrix\Main\Type\DateTime($dateTimeValue, 'Y-m-d H:i:s');
            $time->toUserTime();
        } catch (\Throwable $throwable){
            $time = new \Bitrix\Main\Type\DateTime();
            $time->toUserTime();
        }
        return $time;
    }

    /**
     * @return array
     */
    protected function __getSettings()
    {
        $userTypeSettings = Tools::coalesce($this->getRaw('USER_TYPE_SETTINGS_LIST'), $this->getRaw('USER_TYPE_SETTINGS', 'a:0:{}'));

        if(is_string($userTypeSettings) && $userTypeSettings){
            $userTypeSettings = unserialize($userTypeSettings);
        }

        return (array)$userTypeSettings;
    }

    public function getSettings($key = null, $def = null)
    {
        $setting = $this->__getSettings();
        if(is_null($key)){
            return $setting;
        }
        if(isset($setting[$key])){
            return $setting[$key];
        }
        return value($def);
    }

    /**
     * @return PropertyFields
     */
    public function fields()
    {
        return parent::fields();
    }

    /**
     * Возвращаемое значение: массив с обязательным полем VALUE
     * Для IBlockPropertyValue также присутствует поле DESCRIPTION
     * суть метода: получить унифицированное значение propertyValue полученное из бд или кеша
     * с учетом multiple
     *
     * @param $rawValue
     * @return array
     */
    public function getRestoredValue($rawValue)
    {
        if($rawValue instanceof AEntity){
            $rawValue = $rawValue->getRaw();
        }
        $result = [];

        if($this->isMultiple()){
            $result['VALUE'] = [];
            $result['VALUE_ENUM'] = [];
            $result['VALUE_NUM'] = [];
            $result['DESCRIPTION'] = [];
        } else {
            $result['VALUE'] = '';
            $result['VALUE_ENUM'] = 0;
            $result['VALUE_NUM'] = 0;
            $result['DESCRIPTION'] = '';
        }

        if(is_array($rawValue)) {
            if($this->isMultiple()){
                $result['VALUE'] = (array)$rawValue['VALUE'];
                $result['VALUE_ENUM'] = (array)$rawValue['VALUE_ENUM'];
                $result['VALUE_NUM'] = (array)$rawValue['VALUE_NUM'];
                $result['DESCRIPTION'] = (array)$rawValue['DESCRIPTION'];
            } else {
                if($rawValue['VALUE']){
                    $result['VALUE'] = reset($rawValue['VALUE']);
                }
                if($rawValue['VALUE_ENUM']){
                    $result['VALUE_ENUM'] = (int)reset($rawValue['VALUE_ENUM']);
                }
                if($rawValue['VALUE_NUM']){
                    $result['VALUE_NUM'] = (float)reset($rawValue['VALUE_NUM']);
                }
                if($rawValue['DESCRIPTION']){
                    $result['DESCRIPTION'] = (string)reset($rawValue['DESCRIPTION']);
                }
            }
        }

        $result['VALUE'] = $this->getValueConvertFromDB($result['VALUE']);

        return $result;
    }

    protected function getValueConvertFromDB($storageValue)
    {
        if($this->isMultiple()){
            $values = [];
            foreach ($storageValue as $key => $value) {
                $values[$key] = $this->convertFromDB($value)['VALUE'];
            }
            return $values;
        }
        return $this->convertFromDB($storageValue)['VALUE'];
    }

    /**
     * @return array
     */
    public function getUserTypeHandlers()
    {
        if(!$this->getUserType()){
            return [];
        }
        return \CIBlockProperty::GetUserType($this->getUserType());
    }

    protected function convertFromDB($value, $description = '')
    {
        $valueEx = ['VALUE' => $value, 'DESCRIPTION' => $description];

        $arUserType = $this->getUserTypeHandlers();
        if (isset($arUserType['ConvertFromDB']) && is_callable($arUserType['ConvertFromDB']))
        {
            $valueEx = call_user_func_array($arUserType['ConvertFromDB'], [$this->getRaw(), $valueEx]);
        } elseif(
            isset($arUserType['CLASS_NAME']) &&
            class_exists($arUserType['CLASS_NAME']) &&
            method_exists($arUserType['CLASS_NAME'], 'ConvertFromDB')
        ) {
            $valueEx = call_user_func_array([$arUserType['CLASS_NAME'], 'ConvertFromDB'], [$this->getRaw(), $valueEx]);
        }

        return $valueEx;
    }

    public function getDefaultValue()
    {
        $value = $this->getRaw('DEFAULT_VALUE');
        if($this->isMultiple()){
            if($value === '' || $value === null){
                $value = [];
            } else {
                $value = (array)$value;
            }
        }
        return $value;
    }

    public function isWithDescription()
    {
        return $this->getRaw('WITH_DESCRIPTION', 'N') == 'Y';
    }
}