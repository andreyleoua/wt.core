<?php


namespace Wt\Core\Entity;


use Exception;
use Wt\Core\Factory\AFactory;
use Wt\Core\IFields\AFields;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Interfaces\IFactoryEntity;
use Wt\Core\Property\PropertyManager;
use Wt\Core\Property\PropertyValueEntity;
use Wt\Core\Property\PropertyValueCollection;
use Wt\Core\Support\Entity;
use Wt\Core\Support\Result;

abstract class AEntity extends Entity implements IFactoryEntity
{
    /**
     * @return string
     * @deprecated undefined
     */
    public function getName()
    {
        return '';
    }

    protected $factory;

    /**
     * @param $data
     * @return Result
     * @deprecated notUse
     */
    public function update($data)
    {
        return $this->getFactory()->update($this->getId(), $data);
    }

    /**
     * @return Result
     * @deprecated notUse
     */
    public function delete()
    {
        return $this->getFactory()->delete($this->getId());
    }

    public function setFactory(IFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return AFactory
     * @throws null
     */
    public function getFactory()
    {
        return $this->factory;
    }

    public function hasFactory()
    {
        return !!$this->factory;
    }

    public function clearCache()
    {
        $this->getFactory()->clearCacheById($this->getId());
    }

    /**
     * uuid
     *
     * @return int|string
     */
    public function getId()
    {
        $id = $this->fields()->id;
        if(is_numeric($id)){
            return (int)$id;
        }
        return $id;
    }

    /**
     * @return AFields
     */
    public function fields()
    {
        return parent::fields();
    }

    /**
     * @return PropertyValueCollection|PropertyValueEntity[]
     * @throws Exception
     * @deprecated not use
     */
    public function getFieldValueCollection()
    {
        $map = $this->getFactory()->__getOrmMap();
        $list = [];
        foreach ($map as $ormField){
//            $isScalar = $ormField instanceof \Bitrix\Main\ORM\Fields\ScalarField;
            $isScalar = $ormField instanceof \Bitrix\Main\Entity\ScalarField;
            if(!$isScalar) continue;

            /**
             * @var PropertyManager $m
             * @var StorageFieldEntity $storageFieldEntity
             */
            //$m = resolve(PropertyManager::class . ':service');
            $storageFieldEntity = resolve(StorageFieldEntity::class);
            $storageFieldEntity->setOrmField($ormField);
            $storageFieldEntity->setFactory($this->getFactory());
            $restoredValue = $storageFieldEntity->getRestoredValue($this->getRaw($ormField->getName()));
            $userFieldEntity = $storageFieldEntity->makeUserFieldEntity();
            $list[$ormField->getName()] = new PropertyValueEntity($restoredValue, $this->getFactory(), $this, $userFieldEntity);
        }

        return new PropertyValueCollection($list, $this->getFactory(), $this);
    }

    /**
     * @return PropertyValueCollection|PropertyValueEntity[]
     * @deprecated not use
     */
    public function getUserFieldValueCollection()
    {

        $userFieldEntityId = $this->getFactory()->__getUserFieldEntityId();
        $list = [];

        if($userFieldEntityId){
            $userFieldFactory = app()->factory()->main()->getUserFieldFactory($userFieldEntityId);
            $userFieldCollection = $userFieldFactory->getCollection();

            $list = $userFieldCollection
                ->map(function (APropertyEntity $userFieldEntity) {
                    $valueCollection = $this;
                    $restoredValue = $userFieldEntity->getRestoredValue($valueCollection[$userFieldEntity->getCode()]); // FIELD_NAME ??
                    return new PropertyValueEntity($restoredValue, $this->getFactory(), $this, $userFieldEntity);
                })->all();
        }

        return new PropertyValueCollection($list, $this->getFactory(), $this);
    }
}
