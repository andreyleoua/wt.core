<?php

namespace Wt\Core\Entity\Main;

use Wt\Core\Media\ImageResizer;
use Wt\Core\Media\ResizeLevel;

class ImageEntity extends FileEntity
{
    protected $resizer;

    public function getHeight()
    {
        return $this->getRaw('HEIGHT', 0);
    }

    public function getWidth()
    {
        return $this->getRaw('WIDTH', 0);
    }

    /**
     * @return ImageResizer
     * @throws null
     */
    public function getResizer()
    {
        if(!$this->resizer){
            $this->resizer = resolve(ImageResizer::class, [$this]);
        }
        return $this->resizer;
    }

    public function getTemplate($size = ResizeLevel::ORIGIN)
    {
        $template = '';
        if($this->isImage()){
            $arParams = [
                'SRC' => $this->getResizer()->getSrc($size),
                'ALT' => $this->fields()->description,
            ];
            $template = $this->getFactory()->getTemplater()->get('kit:picture', $arParams);
        }
        return $template;
    }

    public function render($size = ResizeLevel::ORIGIN)
    {
        echo $this->getTemplate($size);
    }
}