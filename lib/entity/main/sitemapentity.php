<?php

namespace Wt\Core\Entity\Main;

use Wt\Core\Entity\AEntity;
use Wt\Core\IFields\Main\SitemapFields;
use Bitrix\Seo\Sitemap;

class SitemapEntity extends AEntity
{
    public function getName()
    {
        return $this->getRaw('NAME');
    }

    public function getSiteId()
    {
        return $this->getRaw('SITE_ID');
    }

    public function getSettings()
    {
        return $this->getRaw('SETTINGS');
    }

    public function getUrl()
    {
        return "/bitrix/admin/seo_sitemap_edit.php?ID={$this->getId()}";
    }

    /**
     * @see \Bitrix\Seo\Sitemap\Job::STATUS_REGISTER
     *
     * @return void
     * @throws \Bitrix\Main\SystemException
     */
    public function make()
    {
        $sitemapId = $this->getId();

        Sitemap\Job::clearBySitemap($sitemapId);
        $job = Sitemap\Job::addJob($sitemapId);

        while ($job->doStep()->isSuccess() && $job->getData()['status'] === 'P'){
            $data = $job->getData();
            unset($data['formattedStatusMessage']);
            pre($data);
        }
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') === 'Y';
    }

    /**
     * @return SitemapFields
     */
    public function fields()
    {
        return parent::fields();
    }
}