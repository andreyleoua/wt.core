<?php


namespace Wt\Core\Entity;


use Bitrix\Highloadblock\HighloadBlockLangTable;
use Wt\Core\Collection\UserFieldCollection;
use Wt\Core\Factory\UserFieldFactory;
use Wt\Core\IFields\HlBlockFields;

class HlBlockEntity extends AEntity
{

    public function getCode()
    {
        return $this->getRaw()['NAME'];
    }

    public function getEntityName()
    {
        return $this->getRaw()['NAME'];
    }

    public function getName()
    {

        if (class_exists('\Bitrix\Highloadblock\HighloadBlockLangTable')) {
            $res = HighloadBlockLangTable::getList([
                'filter' => [
                    'ID' => $this->getId(),
                    'LID' => LANGUAGE_ID,
                ],
            ]);

            if ($r = $res->fetch()) {
                return $r['NAME'];
            }
        }
        return $this->getEntityName();
    }

    public function getTableName()
    {
        return $this->getRaw()['TABLE_NAME'];
    }

    /**
     * @return string|\Bitrix\Main\ORM\Data\DataManager|\Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    public function getORMDataManager()
    {
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->getRaw());
        return $entity->getDataClass();
    }

    public function getUserFieldEntityId()
    {
        if (is_callable(['\Bitrix\Highloadblock\HighloadBlockTable', 'compileEntityId'])) {
            return \Bitrix\Highloadblock\HighloadBlockTable::compileEntityId($this->getId());
        }
        return 'HLBLOCK_' . $this->getId();
    }

    /**
     * @return UserFieldFactory
     */
    protected function getUserFieldFactory()
    {
        return app()->factory()->main()->getUserFieldFactory($this->getUserFieldEntityId());
        //return $this->getFactory()->getFactoryInstance(UserFieldFactory::class, [$this->getEntityId()]);
    }

    /**
     * @return UserFieldCollection|UserFieldEntity[]
     */
    public function getUserFieldCollection()
    {
        return $this->getUserFieldFactory()->getCollection();
    }

    /**
     * @return HlBlockFields
     */
    public function fields()
    {
        return parent::fields();
    }
}