<?php

namespace Wt\Core\Entity;

use Wt\Core\Interfaces\TPsr;

/**
 * Class Image
 * @property string src
 * @package Wt\Core\Entity
 */
class Image 
{
	protected static $imageCache = [];

	use TPsr;

	public function __construct(array $data)
	{
		$this->rawData = $data;
		$this->initPsr(false);
	}

	public function bxResize($width, $height, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL)
	{
		return $this->resizeBx($width, $height, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL);
	}

	public function resizeBx($width, $height, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL)
	{
		$width = (int)$width;
		$height = (int)$height;
		if( $width < 1 || $height < 1 ){
			throw new \Exception("Error Resize image can not be resizeble", 1);
		}

		$uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");

        $io = \CBXVirtualIo::GetInstance();
        $cacheImageFile = "/".$uploadDirName."/resize_cache/".$this->subdir."/".$width."_".$height."_".$resizeType."/".$this->fileName;

        if( static::$imageCache[$cacheImageFile] ){
        	return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
        }

        static::$imageCache[$cacheImageFile] = $cacheImageFile;
        if (!file_exists($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"].$cacheImageFile))){
        	if( \CFile::ResizeImageFile($this->fullPath(), $destPath = $_SERVER["DOCUMENT_ROOT"].$cacheImageFile, array('width'=>$width,'height'=>$height), $resizeType)){
        		static::$imageCache[$cacheImageFile] = $cacheImageFile;
        	}
        }

        return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
	}

	public function resize($width, $height)
	{
		$width = (int)$width;
		$height = (int)$height;

		if( $width < 1 || $height < 1 ){
			throw new \Exception("Error Resize image can not be resizeble", 1);
		}

		$uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");

		$io = \CBXVirtualIo::GetInstance();
        $cacheImageFile = "/".$uploadDirName."/resize_cache/".$this->subdir."/".$width."_".$height."_3/".$this->fileName;

        if( static::$imageCache[$cacheImageFile] ){
        	return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
        }

        static::$imageCache[$cacheImageFile] = $cacheImageFile;

        if (!$io->FileExists($io->RelativeToAbsolutePath($cacheImageFile))){

    		$this->contentType = $this->getMimeType();

        	if( $this->contentType == 'image/jpeg' ){
				$origin_image = imagecreatefromjpeg($this->fullPath());
			} elseif( $this->contentType == 'image/png' ){
				$origin_image = imagecreatefrompng($this->fullPath());
			} elseif( $this->contentType == 'image/gif' ){
				$origin_image = imagecreatefromgif($this->fullPath());
			}

			$im = imagecreatetruecolor($width, $height);
			$transColor = imagecolorallocate($im, 255, 255, 255);

			imagefill($im, 0, 0, $transColor);
			// Сделаем фон прозрачным
			imagecolortransparent($im, $transColor);

			$originProportional = $this->width / $this->height;
			$resultProportinal = $width / $height;

			if( $this->width > $this->height ){
				$des_width = $width;
				$des_height = $des_width / $originProportional;
				$pos_x = 0;
				$pos_y = $height / 2 - $des_height / 2;
			} elseif( $this->height > $this->width ){
				$des_height = $height;
				$des_width = $des_height * $originProportional;
				$pos_y = 0;
				$pos_x = $width / 2 - $des_width / 2;
			} elseif( $this->height == $this->width ){
				$des_width = $width;
				$des_height = $des_width / $originProportional;
				$pos_x = 0;
				$pos_y = $height / 2 - $des_height / 2;
			}

			imagecopyresampled($im, $origin_image, $pos_x, $pos_y, 0, 0, $des_width, $des_height, $this->width, $this->height);

			$cacheInfo = pathinfo($io->RelativeToAbsolutePath($cacheImageFile));
			if( !$io->DirectoryExists($cacheInfo['dirname']) ){
				$io->CreateDirectory($cacheInfo['dirname']);
			}

			// Сохраним изображение
			if( $this->contentType == 'image/jpeg' ){
				imagejpeg($im, $_SERVER["DOCUMENT_ROOT"].$cacheImageFile, 85);
			} elseif( $this->contentType == 'image/png' ){
				imagepng($im, $_SERVER["DOCUMENT_ROOT"].$cacheImageFile);
			} elseif( $this->contentType == 'image/gif' ){
				imagegif($im, $_SERVER["DOCUMENT_ROOT"].$cacheImageFile);
			}
			
			imagedestroy($im);

    		static::$imageCache[$cacheImageFile] = $cacheImageFile;
        }

        return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
	}

	public function textWatermark($text, $color, $position = "br", $size = null)
	{
		$arImgFilter = array(
	        "font" => $_SERVER["DOCUMENT_ROOT"]."/local/modules/ittown.core/assets/watermark.ttf",
	        "position" => $position,
	        'type' => 'text',
	        'text' => $text,
	        'color' => $color,
	        'size' => 'big'
	        // "use_copyright"  => "N"
    	);

    	if( $size && $size > 0){
    		$arImgFilter["coefficient"] = $size;
    		unset($arImgFilter['size']);
    	}

	    $uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");
        $pathInfo = pathinfo($this->src);
        $imageHash = md5($this->src);
        
        $cacheImageFile = "/".$uploadDirName."/resize_cache/".$this->subdir."/".$this->width."_".$this->height."_"."twt"."/".md5($imageHash.$text.$color.$position).".".pathinfo($this->src)['extension'];

	    return app('factory:image')->getInstanceBySrc($this->generateImage($cacheImageFile, array('width'=>$this->width,'height'=>$this->height), BX_RESIZE_IMAGE_PROPORTIONAL, $arImgFilter));
	}

	public function imageWatermark($imageSrc, $position = "br", $size = null, $alpha = 75){
		$arImgFilter = array( 
			"name" => "watermark", 
			"position" => $position, 
			"size" => "real",      // ( доступные варианты big|medium|small|real; real доступен только для type=image )
			"type" => "image",
			"alpha_level" => $alpha,
			"file" => $_SERVER['DOCUMENT_ROOT'].$imageSrc
		);

		if( $size && $size > 0){
    		// $arImgFilter["coefficient"] = $size;
    		$arSize = "big";
    		if( $size <= 33 ){
    			$s = "small";
    		}elseif($size > 33 && $size <= 66){
    			$s = "medium";
    		}
    		$arImgFilter['size'] = $s;
    	}

    	$uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");
        $pathInfo = pathinfo($this->src);
        $imageHash = md5($this->src);
        $cacheImageFile = "/".$uploadDirName."/resize_cache/".$this->subdir."/".$this->width."_".$this->height."_"."iwt"."/".md5($imageHash.$imageSrc.$position.$size.$alpha).".".$pathInfo['extension'];
        $cacheInfo = pathinfo($io->RelativeToAbsolutePath($cacheImageFile));

		return app('factory:image')->getInstanceBySrc($this->generateImage($cacheInfo, array('width'=>$this->width,'height'=>$this->height), BX_RESIZE_IMAGE_PROPORTIONAL, $arImgFilter));
	}

	public function fullPath()
	{
	    return $_SERVER['DOCUMENT_ROOT'].$this->src;
	}

	public function getMimeType()
	{
		return mime_content_type($this->fullPath());
	}

	public function optimize()
	{
		$uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");
        $pathInfo = pathinfo($this->src);
        $imageHash = md5($this->src);
        $resizeDir = 'resize_cache/';
        if( strpos($this->subdir, $resizeDir) !== false ){
        	$resizeDir = '';
        }
	    $cacheImageFile = "/".$uploadDirName."/".$resizeDir.$this->subdir."/opti_".$imageHash.".".'webp';//pathinfo($this->src)['extension'];
		
		if(static::$imageCache[$cacheImageFile]){
			return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
		}

		$io = \CBXVirtualIo::GetInstance();
		if ($io->FileExists($io->RelativeToAbsolutePath($cacheImageFile)) && !isset($_GET['clear_cache']) && $_GET['clear_cache'] != 'Y'){
			return app('factory:image')->getInstanceBySrc($cacheImageFile, false);
		}


		$imagick = new \Imagick();

	    $rawImage = file_get_contents($this->fullPath());

	    $imagick->readImageBlob($rawImage);
	    $imagick->stripImage();

	    // Define image
	    $width      = $imagick->getImageWidth();
	    $height     = $imagick->getImageHeight();

	    // Compress image
	    $imagick->setImageCompressionQuality(85);

	    $image_types = getimagesize($this->fullPath());

	    // Get thumbnail image
	    $imagick->thumbnailImage($width, $height);

	    // Set image as based its own type
	    /*if ($image_types[2] === IMAGETYPE_JPEG)
	    {
	        $imagick->setImageFormat('jpeg');

	        $imagick->setSamplingFactors(array('2x2', '1x1', '1x1'));

	        $profiles = $imagick->getImageProfiles("icc", true);

	        $imagick->stripImage();

	        if(!empty($profiles)) {
	            $imagick->profileImage('icc', $profiles['icc']);
	        }

	        $imagick->setInterlaceScheme(\Imagick::INTERLACE_PLANE);
	        $imagick->setColorspace(\Imagick::COLORSPACE_SRGB);
	        $imagick->setImageCompression(\Imagick::COMPRESSION_JPEG);
	    }
	    else if ($image_types[2] === IMAGETYPE_PNG) 
	    {
	        $imagick->setImageFormat('png');
	    }
	    else if ($image_types[2] === IMAGETYPE_GIF) 
	    {
	        $imagick->setImageFormat('gif');
	    }*/
	    $imagick->setImageFormat('webp');
		$imagick->setImageCompressionQuality(50);
		$imagick->setOption('webp:lossless', 'true');

	    // Get image raw data
	    $rawData = $imagick->getImageBlob();

	    // Destroy image from memory
	    $imagick->destroy();
	    
	    $cacheInfo = pathinfo($io->RelativeToAbsolutePath($cacheImageFile));
		if( !$io->DirectoryExists($cacheInfo['dirname']) ){
			$io->CreateDirectory($cacheInfo['dirname']);
		}
	    file_put_contents($_SERVER["DOCUMENT_ROOT"].$cacheImageFile, $rawData);

	    static::$imageCache[$cacheImageFile] = $cacheImageFile;

	    return app('factory:image')->getInstanceBySrc(static::$imageCache[$cacheImageFile], false);
	}

	protected function generateImage($destinationPath, array $sizes, $proportionalType, array $filter)
	{
		if( static::$imageCache[$destinationPath] ){
			return static::$imageCache[$destinationPath];
		}

		static::$imageCache[$destinationPath] = $destinationPath;

		$fullDestinationPath = $_SERVER['DOCUMENT_ROOT'].$destinationPath;
		if (!file_exists(\CBXVirtualIo::GetInstance()->GetPhysicalName($fullDestinationPath)) || true){
			$result = \CFile::ResizeImageFile($this->fullPath(), $fullDestinationPath, $sizes, $proportionalType, $filter);
			if( $result ){
				static::$imageCache[$destinationPath] = $destinationPath;
			} else {
				throw new \Exception("Error generate new image", 1);
			}
		}

		return static::$imageCache[$destinationPath];
	}
}