<?php


namespace Wt\Core\Entity;


use Wt\Core\Form\Field\DefaultField;
use Wt\Core\Interfaces\IPropertyEntity;
use Wt\Core\Property\PropertyManager;
use Wt\Core\PropertyAdapter\APropertyAdapter;

abstract class APropertyEntity extends AEntity implements IPropertyEntity
{
    abstract public function getRestoredValue($rawValue);

    public function __getDevType()
    {
        return $this->getFullType() . ($this->isMultiple()?'+':'');
    }

    public function getFullType($normalize = false)
    {
        $str = implode(':', array_filter([
            $this->getBaseType(),
            $this->getUserType(),
        ]));
        if($normalize){
            return preg_replace('/[^A-Za-z0-9]/', '', $str);
        }
        return $str;
    }

    /**
     * @param array|string $type
     * @return bool
     */
    public function isBaseType($type)
    {
        $compare = function ($type, $iterableType){
            return $type === strtolower($iterableType);
        };
        $currentType = strtolower($this->getBaseType());
        if(is_iterable($type)){
            foreach ($type as $iterableType){
                if($compare($currentType, $iterableType)){
                    return true;
                }
            }
            return false;
        }
        return $compare($currentType, $type);
    }

    /**
     * @param array|string $type
     * @return bool
     */
    public function isType($type)
    {
        $compare = function ($type, $iterableType){
            return $type === strtolower($iterableType);
        };
        $currentType = strtolower($this->getFullType());
        if(is_iterable($type)){
            foreach ($type as $iterableType){
                if($compare($currentType, $iterableType)){
                    return true;
                }
            }
            return false;
        }
        return $compare($currentType, $type);
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'Y') == 'Y';
    }

    /**
     * @return APropertyAdapter
     * @throws null
     */
    public function getAdapter()
    {
        /**
         * @var PropertyManager $m
         */
        $m = resolve(PropertyManager::class . ':service');
        return $m->getAdapter($this);
    }

    /**
     * @return DefaultField
     */
    public function getFormField()
    {
        return $this->getAdapter()->getFormField();
    }
}