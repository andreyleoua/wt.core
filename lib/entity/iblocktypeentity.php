<?php


namespace Wt\Core\Entity;


use Bitrix\IBlock\TypeLanguageTable;
use Wt\Core\Factory\IBlockTypeFactory;
use Wt\Core\IFields\IBlockTypeFields;

class IBlockTypeEntity extends AEntity
{
    /**
     * @return IBlockTypeFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    public function getName()
    {
        if(class_exists('\Bitrix\IBlock\TypeLanguageTable')) {
            $res = TypeLanguageTable::getList([
                'filter' => [
                    'IBLOCK_TYPE_ID' => $this->getId(),
                    'LANGUAGE_ID' => LANGUAGE_ID,
                ],
            ]);

            if ($r = $res->fetch()) {
                return $r['NAME'];
            }
        }
        return $this->getId();
    }

    /**
     * @return IBlockTypeFields
     */
    public function fields()
    {
        return parent::fields();
    }
}