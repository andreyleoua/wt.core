<?php


namespace Wt\Core\Entity\Shop;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\Shop\ProductPriceFactory;
use Wt\Core\IFields\Shop\ProductPriceFields;
use Wt\Core\Support\Result;

class ProductPriceEntity extends AEntity
{

    /**
     * @param $data
     * @return Result
     */
	public function update($data)
	{
	    $result = Result::make();
        $result->setId($this->getId());
        $result->setData($data);

        if(!\Bitrix\Main\Loader::includeModule('catalog')) {
            $result->setData($data);
            $result->addError('module catalog not include');
            return $result;
        }
        if (class_exists('Bitrix\Catalog\PriceTable') ) {
            $bxResult = \Bitrix\Catalog\PriceTable::update($this->getId(), $data);
            $result = Result::makeByBxResult($bxResult);
        }
        else {
            //\Bitrix\Catalog\Model\Price::getList
            $id = \CPrice::Update($this->getId(), $data);
            if(!$id){
                global $APPLICATION;
                $result->setError($APPLICATION->LAST_ERROR->GetString());
            }
        }
        return $result;
	}

    /**
     * @return ProductPriceFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    /**
     * @return PriceTypeEntity
     */
    public function getPriceTypeEntity()
    {
        $factory = app()->factory()->shop()->getPriceTypeFactory();
        return $factory->getEntityById($this->getPriceTypeId());
    }
    /**
     * @return CurrencyEntity
     */
    public function getCurrencyEntity()
    {
        $factory = app()->factory()->shop()->getCurrencyFactory();
        return $factory->getEntityById($this->getCurrencyId());
    }

    /**
     * @return int
     */
    public function getPriceTypeId()
    {
        return $this->getRaw('CATALOG_GROUP_ID');
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getRaw('PRODUCT_ID');
    }

    /**
     * @return int
     */
    public function getExtraId()
    {
        return $this->getRaw('EXTRA_ID');
    }

    /**
     * @return string
     */
    public function getCurrencyId()
    {
        return $this->getRaw('CURRENCY');
    }

    /**
     * @deprecated getCurrencyId
     * @return string
     */
    public function getCurrency()
    {
        return $this->getCurrencyId();
    }

    /**
     * @deprecated getCurrencyId
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->getCurrencyId();
    }

    /**
     * @return ProductPriceFields
     */
    public function fields()
    {
        return parent::fields();
    }
}