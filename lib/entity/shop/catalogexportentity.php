<?php

namespace Wt\Core\Entity\Shop;

use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\Shop\CatalogExportFactory;
use Wt\Core\IFields\Shop\CatalogExportFields;

class CatalogExportEntity extends AEntity
{

	public function getName()
    {
        return $this->fields()->name;
    }

	public function getFileName()
    {
        return $this->fields()->fileName;
    }

    public function getSetupVars()
    {
        $arSetupVars = [];
        parse_str($this->getRaw()['SETUP_VARS'], $arSetupVars);
        if (!is_array($arSetupVars)){
            $arSetupVars = [];
        }
        if(isset($arSetupVars['XML_DATA']) && is_string($arSetupVars['XML_DATA'])){
            $arSetupVars['XML_DATA'] = unserialize($arSetupVars['XML_DATA']);
        }
        return $arSetupVars;
    }

    /**
     * @return array
     */
    public function getSectionsIds()
    {
        $setupVars = $this->getSetupVars();
        $V = $setupVars['V'];
        $IBLOCK_ID = $setupVars['IBLOCK_ID'];
        $intMaxSectionID = 0;
        $bAllSections = False;
        $arSections = [];
        if (!empty($V) && is_array($V))
        {
            foreach ($V as $key => $value)
            {
                if (trim($value)== '0')
                {
                    $bAllSections = True;
                    break;
                }
                if (intval($value)>0)
                {
                    $arSections[] = intval($value);
                }
            }
        }


        $arSectionIDs = array();
        $arAvailGroups = array();
        if (!$bAllSections)
        {
            for ($i = 0, $intSectionsCount = count($arSections); $i < $intSectionsCount; $i++)
            {
                $sectionIterator = \CIBlockSection::GetNavChain($IBLOCK_ID, $arSections[$i], array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'LEFT_MARGIN', 'RIGHT_MARGIN'));
                $curLEFT_MARGIN = 0;
                $curRIGHT_MARGIN = 0;
                while ($section = $sectionIterator->Fetch())
                {
                    $section['ID'] = (int)$section['ID'];
                    $section['IBLOCK_SECTION_ID'] = (int)$section['IBLOCK_SECTION_ID'];
                    if ($arSections[$i] == $section['ID'])
                    {
                        $curLEFT_MARGIN = (int)$section['LEFT_MARGIN'];
                        $curRIGHT_MARGIN = (int)$section['RIGHT_MARGIN'];
                        $arSectionIDs[] = $section['ID'];
                    }
                    $arAvailGroups[$section['ID']] = array(
                        'ID' => $section['ID'],
                        'IBLOCK_SECTION_ID' => $section['IBLOCK_SECTION_ID'],
                        'NAME' => $section['NAME']
                    );
                    if ($intMaxSectionID < $section['ID'])
                        $intMaxSectionID = $section['ID'];
                }
                unset($section, $sectionIterator);

                $filter = array("IBLOCK_ID"=>$IBLOCK_ID, ">LEFT_MARGIN"=>$curLEFT_MARGIN, "<RIGHT_MARGIN"=>$curRIGHT_MARGIN, "ACTIVE"=>"Y", "IBLOCK_ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y");
                $sectionIterator = \CIBlockSection::GetList(array("LEFT_MARGIN"=>"ASC"), $filter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
                while ($section = $sectionIterator->Fetch())
                {
                    $section["ID"] = (int)$section["ID"];
                    $section["IBLOCK_SECTION_ID"] = (int)$section["IBLOCK_SECTION_ID"];
                    $arSectionIDs[] = $section["ID"];
                    $arAvailGroups[$section["ID"]] = $section;
                    if ($intMaxSectionID < $section["ID"])
                        $intMaxSectionID = $section["ID"];
                }
                unset($section, $sectionIterator);
            }
            if (!empty($arSectionIDs))
                $arSectionIDs = array_unique($arSectionIDs);
        }
        else
        {
            $filter = array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "IBLOCK_ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y");
            $sectionIterator = \CIBlockSection::GetList(array("LEFT_MARGIN"=>"ASC"), $filter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));
            while ($section = $sectionIterator->Fetch())
            {
                $section["ID"] = (int)$section["ID"];
                $section["IBLOCK_SECTION_ID"] = (int)$section["IBLOCK_SECTION_ID"];
                $arAvailGroups[$section["ID"]] = $section;
                if ($intMaxSectionID < $section["ID"])
                    $intMaxSectionID = $section["ID"];
            }
            unset($section, $sectionIterator);

            if (!empty($arAvailGroups))
                $arSectionIDs = array_keys($arAvailGroups);
        }

        return $arSectionIDs;
    }

    public function getFilter()
    {
        $setupVars = $this->getSetupVars();
        $V = $setupVars['V'];
        $IBLOCK_ID = $setupVars['IBLOCK_ID'];
        $bAllSections = False;
        if (!empty($V) && is_array($V))
        {
            foreach ($V as $key => $value)
            {
                if (trim($value) == '0')
                {
                    $bAllSections = True;
                    break;
                }
            }
        }

        $filterAvailable = (isset($setupVars['FILTER_AVAILABLE']) && $setupVars['FILTER_AVAILABLE'] == 'Y');
        $arSectionIDs = $this->getSectionsIds();

        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID);
        if (!$bAllSections && !empty($arSectionIDs))
        {
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
            $arFilter["SECTION_ID"] = $arSectionIDs;
        }
        $arFilter["ACTIVE"] = "Y";
        $arFilter["ACTIVE_DATE"] = "Y";
        if ($filterAvailable)
            $arFilter['CATALOG_AVAILABLE'] = 'Y';

        return $arFilter;
    }

    public function getRunFilePath()
    {
        $strFile = CATALOG_PATH2EXPORTS.$this->fields()->fileName. '_run.php';
        if (!file_exists($_SERVER['DOCUMENT_ROOT'].$strFile)){
            $strFile = CATALOG_PATH2EXPORTS_DEF.$this->fields()->fileName. '_run.php';
            if (!file_exists($_SERVER['DOCUMENT_ROOT'].$strFile)){
                return false;
            }
        }

        return $_SERVER['DOCUMENT_ROOT'] . $strFile;
    }

    public function export()
    {
        $result = null;
        $runFilePath = $this->getRunFilePath();
        if(!$runFilePath){
            return null;
        }
        if(!file_exists($runFilePath)){
            return null;
        }
        $arSetupVars = [];
        $intSetupVarsCount = 0;
        if($this->fields()->defaultProfile != 'Y'){
            parse_str($this->fields()->setupVars, $arSetupVars);
            if (!empty($arSetupVars) && is_array($arSetupVars))
            {
                $intSetupVarsCount = extract($arSetupVars, EXTR_SKIP);
            }
        }
        $PROFILE_ID = $this->getId();
        global $DB;
        \CCatalogDiscountSave::Disable();
        $result = include($runFilePath);
        \CCatalogDiscountSave::Enable();

        \CCatalogExport::Update($this->getId(), [
                '=LAST_USE' => $DB->GetNowFunction(),
            ]
        );

        return $result;
    }

    /**
     * @return CatalogExportFactory
     */
    public function getFactory()
    {
        return parent::getFactory(); // TODO: Change the autogenerated stub
    }

    /**
     * @return CatalogExportFields
     */
    public function fields()
    {
        return parent::fields(); // TODO: Change the autogenerated stub
    }
}