<?php


namespace Wt\Core\Entity\Shop;


use CCatalogProduct;
use CCatalogSku;
use Wt\Core\Collection\FileCollection;
use Wt\Core\Collection\Shop\ProductCollection;
use Wt\Core\Collection\Shop\ProductStoreCollection;
use Wt\Core\Collection\Shop\ProductPriceCollection;
use Wt\Core\Entity\FileEntity;
use Wt\Core\Entity\IBlock\ElementEntity;
use Wt\Core\Entity\UserEntity;
use Wt\Core\Factory\Shop\ProductCatalogFactory;
use Wt\Core\Factory\Shop\ProductFactory;
use Wt\Core\Factory\Shop\ProductPriceFactory;
use Wt\Core\Factory\Shop\ProductStoreFactory;

/**
 * Продукт
 * содержит в себе:
 * - fields
 * - props
 * - price
 * - catalog
 * - store
 *
 */
class ProductEntity extends ElementEntity
{
    /**
     * @return ProductFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    public function getAdminUrl()
    {
        $languageId = LANGUAGE_ID;
        $iblockTypeId = $this->getRaw('IBLOCK_TYPE_ID', '');

        return "/bitrix/admin/cat_product_edit.php?IBLOCK_ID={$this->getBlockId()}&type=$iblockTypeId&ID={$this->getId()}&find_section_section={$this->getSectionId()}";
    }

    /**
     * @return ProductCatalogEntity
     */
    public function getProductCatalogEntity()
    {
        /**
         * @var ProductCatalogFactory $factory
         */
        $factory = $this->getFactory()->getFactoryInstance(ProductCatalogFactory::class);
        return $factory->getEntityById($this->getId());
    }

    /**
     * @return FileEntity
     * @throws null
     */
    public function getPreviewPictureEntity()
    {
        $fileFactory = app()->factory()->main()->getFileFactory();
        $entity = $fileFactory->getEntityById($this->fields()->previewPicture);
        if(!$entity->isEmpty()){
            return $entity;
        }
        $entity = $fileFactory->getEntityById($this->fields()->detailPicture);
        if(!$entity->isEmpty()){
            return $entity;
        }
        $entity = $this->getMoreImageFirst();
        if(!is_null($entity) && !$entity->isEmpty()){
            return $entity;
        }
        return resolve(FileEntity::class, [[]]);
    }

    /**
     * @return FileEntity
     * @throws null
     */
    public function getDetailPictureEntity()
    {
        $fileFactory = app()->factory()->main()->getFileFactory();
        $entity = $fileFactory->getEntityById($this->fields()->detailPicture);
        if(!$entity->isEmpty()){
            return $entity;
        }
        $entity = $fileFactory->getEntityById($this->fields()->previewPicture);
        if(!$entity->isEmpty()){
            return $entity;
        }
        $entity = $this->getMoreImageFirst();
        if(!is_null($entity) && !$entity->isEmpty()){
            return $entity;
        }
        return resolve(FileEntity::class, [[]]);
    }

    /**
     * @return FileEntity|null
     */
    private function getMoreImageFirst()
    {
        $moreImagePropertyCode = $this->getFactory()->getConfig()->get('moreImagePropertyCode');
        if(!$moreImagePropertyCode) {
            return null;
        }
        $moreImageEntity = $this->getPropertyValueCollection()->getByCode($moreImagePropertyCode);
        if(!$moreImageEntity || $moreImageEntity->isEmpty() || !$moreImageEntity->isMultiple() || !$moreImageEntity->isBaseType('F')){
            return null;
        }
        /**
         * @var FileCollection $fileCollection
         */
        $fileCollection = $moreImageEntity->getValueEntity();
        if(!$fileCollection || $fileCollection->isEmpty()) {
            return null;
        }
        return $fileCollection->first();
    }


    /**
     * @return ProductStoreCollection
     */
    public function getProductStoreCollection()
    {
        /**
         * @var ProductStoreFactory $factory
         */
        $factory = $this->getFactory()->getFactoryInstance(ProductStoreFactory::class, [$this->getFactory()->getBlockId(), $this->getId()]);
        return $factory->getCollection();
    }


    /**
     * @return ProductPriceCollection|ProductPriceEntity[]
     */
    public function getProductPriceCollection()
    {
        /**
         * @var ProductPriceFactory $factory
         */
        $factory = $this->getFactory()->getFactoryInstance(ProductPriceFactory::class, [$this->getFactory()->getBlockId(), $this->getId()]);

        return $factory->getCollection();
    }


    /**
     * @return ProductFactory
     */
    protected function getProductFactory($iBlockId)
    {
        return app()->factory()->iBlock()->getEntityById($iBlockId)->getProductFactory();
    }

    /**
     * @param array $skuFilter
     * @return ProductCollection|ProductEntity[]
     */
    public function getOffersCollection(array $skuFilter = ['ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y'])
    {
        return $this->getProductFactory($this->getCatalogEntity()->getBlockId())
            ->getEntityByIds($this->getOffersIds($skuFilter));
    }

    public function isOfferExist()
    {
        $offers = CCatalogSku::getExistOffers($this->getId(), $this->fields()->iblockId);
        return is_array($offers) && $offers[$this->getId()];
    }

    /**
     * @param array $skuFilter
     * @return array
     */
    public function getOffersIds(array $skuFilter = ['ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y'])
    {
        $arOffers = CCatalogSku::getOffersList(
            $productID = $this->getId(), // массив ID товаров
            $this->getBlockId(), // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
            $skuFilter, // дополнительный фильтр предложений. по умолчанию пуст.
            $fields = [],  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
            $propertyFilter = [] /* свойства предложений. имеет 2 ключа:
                                                       ID - массив ID свойств предложений
                                                              либо
                                                       CODE - массив символьных кодов свойств предложений
                                                             если указаны оба ключа, приоритет имеет ID*/
        );

        $offers = [];
        if(isset($arOffers[$productID]) && is_array($arOffers[$productID])) {
            foreach ($arOffers[$productID] as $offer){
                $offers[] = $offer['ID'];
            }
        }

        return $offers;
    }

    /**
     * @param UserEntity|int $userEntity
     * @param int $quantity
     * @param string $siteId
     * @return ProductPriceOptimalEntity
     * @throws null
     */
    public function getOptimalPrice($userEntity = null, $quantity = 1, $siteId = null)
    {
        if(!$siteId) {
            $siteId = false;
        }

        if(is_null($userEntity)){
            $userEntity = app()->service()->user();
        } elseif(!empty($userEntity) && (is_numeric($userEntity) || is_string($userEntity))){
            $userEntity = app()->factory()->main()->getUserFactory()->getEntityById($userEntity);
        }

        $arPrice = CCatalogProduct::GetOptimalPrice($this->getId(), $quantity, $userEntity->getGroups(), $renewal = 'N', $priceList = [], $siteId);
        if (!$arPrice || count($arPrice) <= 0)
        {
            if ($nearestQuantity = CCatalogProduct::GetNearestQuantityPrice($this->getId(), $quantity, $userEntity->getGroups()))
            {
                $quantity = $nearestQuantity;
                $arPrice = CCatalogProduct::GetOptimalPrice($this->getId(), $quantity, $userEntity->getGroups(), $renewal);
            }
        }

        $data = array_merge((array)$arPrice['PRICE'], (array)$arPrice['RESULT_PRICE']);
        $data['PRODUCT_ID'] = $arPrice['PRODUCT_ID'];
        $data['DISCOUNT_SIMPLE_LIST'] = $arPrice['DISCOUNT'];
        $data['DISCOUNT_LIST'] = $arPrice['DISCOUNT_LIST'];

        return resolve(ProductPriceOptimalEntity::class, [$data]);
    }
}