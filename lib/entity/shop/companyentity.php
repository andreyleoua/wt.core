<?php


namespace Wt\Core\Entity\Shop;


use Wt\Core\Entity\AEntity;
use Wt\Core\IFields\Shop\CompanyFields;

class CompanyEntity extends AEntity
{

    public function getName()
    {
        return $this->getRaw()['NAME'];
    }

    public function isActive()
    {
        return $this->getRaw()['ACTIVE'] == 'Y';
    }

    /**
     * @return CompanyFields
     */
    public function fields()
    {
        return parent::fields();
    }
}