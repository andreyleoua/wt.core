<?php


namespace Wt\Core\Entity\Shop;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\Shop\ProductPriceFactory;
use Wt\Core\IFields\Shop\ProductPriceOptimalFields;

/**
 * Сомнительная реализация, но что поделаешь... Битрикс дает один высер сплошной
 */
class ProductPriceOptimalEntity extends AEntity
{

    /**
     * @return ProductPriceFactory
     * @deprecated notUse
     */
    public function getFactory()
    {
        return parent::getFactory();
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return (float)$this->getRaw('PRICE', 0);
    }

    /**
     * @return PriceTypeEntity
     */
    public function getPriceTypeEntity()
    {
        $factory = app()->factory()->shop()->getPriceTypeFactory();
        return $factory->getEntityById($this->getPriceTypeId());
    }

    /**
     * @return int
     */
    public function getPriceTypeId()
    {
        return $this->getRaw('CATALOG_GROUP_ID');
    }

    /**
     * @return CurrencyEntity
     * @throws null
     */
    public function getCurrencyEntity()
    {
        $factory = app()->factory()->shop()->getCurrencyFactory();
        return $factory->getEntityById($this->getCurrencyId());
    }

    /**
     * @return string
     */
    public function getCurrencyId()
    {
        return $this->getRaw('CURRENCY');
    }

    /**
     * @return string
     * @deprecated getCurrencyId
     */
    public function getCurrencyCode()
    {
        return $this->getCurrencyId();
    }

    /**
     * @param $currencyId
     * @return float|int
     */
    public function convertTo($currencyId)
    {
        return \CCurrencyRates::ConvertCurrency($this->getPrice(), $this->getCurrencyId(), $currencyId);
    }


    /**
     * @return ProductPriceOptimalFields
     */
    public function fields()
    {
        return parent::fields();
    }
}