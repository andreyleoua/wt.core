<?php

namespace Wt\Core\Entity\Shop;

use Wt\Core\Entity\AEntity;
use Wt\Core\IFields\Shop\BasketFields;

class BasketEntity extends AEntity
{

    public function getProductId()
    {
        return (int)$this->getRaw('PRODUCT_ID', 0);
    }

    /**
     * @return BasketFields
     */
    public function fields()
    {
        return parent::fields();
    }
}