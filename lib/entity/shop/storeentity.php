<?php


namespace Wt\Core\Entity\Shop;


use Wt\Core\Entity\AEntity;
use Wt\Core\IFields\Shop\StoreFields;

class StoreEntity extends AEntity
{
    public function getName()
    {
        return $this->getRaw('TITLE', '');
    }

    public function getCode()
    {
        return $this->getRaw('CODE', '');
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') == 'Y';
    }

    public function isMatchingSite($siteId = null)
    {
        if(constant('ADMIN_SECTION') === true){
            return true;
        }
        if(!$siteId) {
            $siteId = SITE_ID;
        }
        $storeSiteId = $this->getRaw('SITE_ID');
        return !$storeSiteId || $storeSiteId == $siteId;
    }

    /**
     * Для отгрузки
     * @return bool
     */
    public function isShippingCenter()
    {
        return $this->getRaw()['SHIPPING_CENTER'] == 'Y';
    }


    /**
     * Пункт выдачи
     * @return bool
     */
    public function isIssuingCenter()
    {
        return $this->getRaw()['ISSUING_CENTER'] == 'Y';
    }

    /**
     * @return StoreFields
     */
    public function fields()
    {
        return parent::fields();
    }
}