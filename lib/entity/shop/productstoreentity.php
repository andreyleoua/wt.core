<?php


namespace Wt\Core\Entity\Shop;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\Shop\ProductStoreFactory;
use Wt\Core\IFields\Shop\ProductStoreFields;

class ProductStoreEntity extends AEntity
{

    /**
     * @return ProductStoreFactory
     */
	public function getFactory()
    {
        return parent::getFactory();
    }

    public function getStoreId()
    {
        return $this->getRaw('STORE_ID', 0);
    }

    public function getAmount()
    {
        return (float)$this->getRaw('AMOUNT', 0);
    }

    /**
     * @return StoreEntity
     */
    public function getStoreEntity()
    {
        $factory = app()->factory()->shop()->getStoreFactory();
        return $factory->getEntityById($this->getStoreId());
    }

	public function isActive()
    {
        return $this->getStoreEntity()->isActive();
    }

	public function isMatchingSite($siteId = null)
    {
        return $this->getStoreEntity()->isMatchingSite($siteId);
    }

    /**
     * @return ProductStoreFields
     */
    public function fields()
    {
        return parent::fields();
    }
}