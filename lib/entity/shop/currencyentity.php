<?php

namespace Wt\Core\Entity\Shop;

use Wt\Core\Entity\AEntity;
use Wt\Core\IFields\Shop\CurrencyFields;

class CurrencyEntity extends AEntity
{

    public function isBase()
    {
        return $this->getRaw('BASE', 'N') === 'Y';
    }

    public function getBaseRate()
    {
        return (float)$this->getRaw('CURRENT_BASE_RATE');
    }

    public function getName()
    {
        if (class_exists('\CCurrencyLang')) {
            $r = \CCurrencyLang::GetByID($this->getId(), LANGUAGE_ID);

            if ($r) {
                return $r['FULL_NAME'];
            }
        }
        return $this->getRaw('CURRENCY');
    }

    /**
     * @return CurrencyFields
     */
    public function fields()
    {
        return parent::fields();
    }
}