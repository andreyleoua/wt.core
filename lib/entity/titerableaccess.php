<?php

namespace Wt\Core\Entity;

use ArrayAccess;
use Countable;
use Iterator;
use IteratorAggregate;
use JsonSerializable;
use Serializable;
use Wt\Core\Tools;

/**
 * @implements ArrayAccess & IteratorAggregate & Countable & JsonSerializable & Serializable
 */
trait TIterableAccess
{
    protected $rawData;

    public function offsetExists($offset): bool
    {
        return isset($this->rawData[$offset]) || array_key_exists($offset, $this->rawData);
    }
    public function &offsetGet($offset): mixed
    {
        return $this->rawData[$offset];
    }
    public function offsetSet($offset, $value): void
    {
        if(  !is_null($offset) )
            $this->rawData[$offset] = $value;
        else
            $this->rawData[] = $value;

        $this->onChangeConsistentState();
    }
    public function offsetUnset($offset): void
    {
        if( $this->offsetExists($offset) ){
            unset($this->rawData[$offset]);

            $this->onChangeConsistentState();
        }
    }
    public function getIterator(): Iterator
    {
        return new \ArrayIterator( $this->rawData );
    }
    public function jsonSerialize(): mixed
    {
        return Tools::jsonSerialize($this->rawData);
    }

    public function serialize()
    {
        return serialize($this->getRaw());
    }

    public function unSerialize($data) {
        $this->setRaw(unserialize($data));
    }

    public function __sleep()
    {
        return Tools::jsonSerialize($this->rawData);
    }

    /**
     * @return array
     */
    public function getRaw()
    {
        return $this->rawData;
    }
    /**
     * @todo $options
     * @return false|string
     */
    public function toJson()
    {
        return json_encode($this);
    }
    public function isEmpty()
    {
        return $this->count() == 0;
    }
    public function isNotEmpty()
    {
        return !$this->isEmpty();
    }
    public function count(): int
    {
        return count($this->rawData);
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->rawData = [];
        $this->onChangeConsistentState();
        return $this;
    }
    /**
     * рекурсия по массиву
     * @return array
     */
    public function toArray()
    {
        $res = [];
        foreach ($this->rawData as $k => $item) {
            $res[$k] = $item;
            if(is_object($item) && method_exists($item, 'toArray')){
                $res[$k] = call_user_func([$item, 'toArray']);
            }
        }
        return $res;
    }

    public function __toString()
    {
        return $this->toJson();
    }

    protected function iterableToArrayRecursive($obj)
    {
        $res = [];
        foreach ($obj as $k => $item) {
            if(is_callable([$item, 'toArray'])){
                $item = call_user_func([$item, 'toArray']);
            } elseif(is_array($item)) {
                $item = $this->iterableToArrayRecursive($item);
            }
            $res[$k] = $item;
        }

        return $res;
    }

    protected function setRaw($raw)
    {
        $this->rawData = $raw;
        $this->onChangeConsistentState();
        return $this;
    }


    public function __serialize(): array
    {
        return Tools::jsonSerialize($this->rawData);
    }

    public function __unserialize(array $data): void
    {
        $this->rawData = $data;
    }

    protected function onChangeConsistentState(){}
}