<?php


namespace Wt\Core\Entity;


use Wt\Core\IFields\UserFieldEnumFields;

class UserFieldEnumEntity extends AEntity
{

    public function isDefault()
    {
        return $this->getRaw('DEF', 'N') === 'Y';
    }

    public function getName()
    {
        return $this->getValue();
    }

    public function getValue()
    {
        return $this->getRaw('VALUE');
    }

    /**
     * @return UserFieldEnumFields
     */
    public function fields()
    {
        return parent::fields();
    }
}