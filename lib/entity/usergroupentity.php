<?php


namespace Wt\Core\Entity;


use Wt\Core\IFields\UserGroupFields;

class UserGroupEntity extends AEntity
{
    public function getName()
    {
        return $this->getRaw('NAME');
    }

    public function getCode()
    {
        return $this->getRaw('STRING_ID');
    }

    public function getDescription()
    {
        return $this->getRaw('DESCRIPTION');
    }

    public function isActive()
    {
        return $this->getRaw('ACTIVE', 'N') === 'Y';
    }

    public function isSystem()
    {
        return $this->getRaw('IS_SYSTEM', 'N') === 'Y';
    }

    public function isAdmin()
    {
        return $this->getId() === 1;
    }

    public function isAnonymous()
    {
        return $this->getRaw('ANONYMOUS', 'N') === 'Y';
    }

    public function getAdminUrl()
    {
        return "/bitrix/admin/group_edit.php?ID={$this->getId()}";
    }

    /**
     * @return UserGroupFields
     */
    public function fields()
    {
        return parent::fields();
    }
}