<?php


namespace Wt\Core\DebugControl;


class MemoryDebugControl extends ADebugControl
{
    protected $real_usage;

    public function __construct($real_usage = null)
    {
        $this->real_usage = $real_usage;
    }

    public function begin()
    {
        $this->reset();
        $this->begin = memory_get_usage($this->real_usage);
        return $this;
    }

    public function end()
    {
        $this->end = memory_get_usage($this->real_usage);
        $this->calc();
        return $this;
    }

    public function __toString()
    {
        if(is_null($this->result)){
            return '';
        }
        return $this->getPrintSize((float)$this->result);
    }

    protected function getPrintSize(int $bytes)
    {
        if ($bytes < 0) {$m = '-';} else {$m = '';}
        $bytes = abs($bytes);
        if ($bytes<1024) return $m.$bytes.'B';
        else if ($bytes<1048576) return $m.round ($bytes/1024,2).'KB';
        else if ($bytes<1073741824) return $m.round ($bytes/1048576,2).'MB';
        else return $m.round($bytes/1073741824,2).'GB';
    }
}