<?php


namespace Wt\Core\DebugControl;


class PeakMemoryDebugControl extends MemoryDebugControl
{
    public function reset()
    {
        if(is_callable('memory_reset_peak_usage')){
            \memory_reset_peak_usage();
        }
        return parent::reset(); // TODO: Change the autogenerated stub
    }

    public function begin()
    {
        $this->reset();
        $this->begin = 0;
        return $this;
    }

    public function end()
    {
        $this->end = memory_get_peak_usage($this->real_usage);
        $this->calc();
        return $this;
    }
}