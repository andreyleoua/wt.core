<?php


namespace Wt\Core\DebugControl;


use Wt\Core\Interfaces\IDebugControl;

class EmptyDebugControl implements IDebugControl
{
    public function begin(){}
    public function end(){}
    public function getData(){}
    public function __toString(){return '';}
    public function reset(){}
    public function print($return = false){}
}