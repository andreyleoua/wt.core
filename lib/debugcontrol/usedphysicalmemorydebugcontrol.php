<?php


namespace Wt\Core\DebugControl;


use Wt\Core\Support\System;

class UsedPhysicalMemoryDebugControl extends ADebugControl
{

    /**
     * @var System
     */
    protected $system;

    public function __construct()
    {
        $this->system = new System();
    }

    public function begin()
    {
        $this->reset();
        $this->begin = $this->system->getUsedPhysicalMemory();
        return $this;
    }

    public function end()
    {
        $this->end = $this->system->getUsedPhysicalMemory();
        $this->calc();
        return $this;
    }

    public function getData()
    {
        $data = parent::getData();
        $data['totalPhysicalMemory'] = $this->system->getTotalPhysicalMemory();
        return $data;
    }

    public function __toString()
    {
        if(is_null($this->begin)){
            return '';
        }

        $total = $this->system->getTotalPhysicalMemory();
        $totalPrint = $this->getPrintSize($total);
        $beginPercent = (int)($this->begin / $total * 100);
        $beginPrint = $this->getPrintSize($this->begin);

        if(is_null($this->end)){
            return "$beginPrint / $totalPrint";
        }

        $endPercent = (int)($this->end / $total * 100);
        $rPercent = $endPercent - $beginPercent;
        $delta = $this->result;
        $rPercentPrint = (($delta>=0)?'+':'-') . abs($rPercent) . '%';
        $endPrint = $this->getPrintSize($this->end);
        $deltaPrint = (($delta>=0)?'+ ':'- ') . $this->getPrintSize(abs($delta));
        return "$beginPrint $deltaPrint ($rPercentPrint) / $totalPrint";
    }

    protected function getPrintSize(int $bytes)
    {
        if ($bytes < 0) {$m = '-';} else {$m = '';}
        $bytes = abs($bytes);
        if ($bytes<1024) return $m.$bytes.'B';
        else if ($bytes<1048576) return $m.round ($bytes/1024,2).'KB';
        else if ($bytes<1073741824) return $m.round ($bytes/1048576,2).'MB';
        else return $m.round($bytes/1073741824,2).'GB';
    }
}