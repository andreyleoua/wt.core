<?php


namespace Wt\Core\DebugControl;


use CDebugInfo;

class SqlDebugControl extends ADebugControl
{
    /** @var CDebugInfo */
    protected $debug;

    public function begin()
    {
        $this->reset();
        global $DB;
        $GLOBALS['DB']->ShowSqlStat = true;
        $application = \Bitrix\Main\Application::getInstance();
        $connection  = $application->getConnection();
        $connection->setTracker(new \Bitrix\Main\Diag\SqlTracker());
        \Bitrix\Main\Data\Cache::setShowCacheStat(true);
        $this->debug = new CDebugInfo();
        $this->debug->Start();
        $this->begin = 0;
        return $this;
    }

    public function end()
    {
        $this->result = $this->debug->Output();
        global $APPLICATION;
        $APPLICATION->arIncludeDebug[$this->debug->index] = $this->debug->arResult;
        $this->end = 0;
        $this->calc();
        return $this;
    }

    protected function calc()
    {
    }

    public function __toString()
    {
        if(is_null($this->result)){
            return '';
        }
        return (string)$this->result;
    }
}