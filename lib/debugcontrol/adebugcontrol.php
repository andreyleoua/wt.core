<?php


namespace Wt\Core\DebugControl;


use Wt\Core\Interfaces\IDebugControl;

abstract class ADebugControl implements IDebugControl, \JsonSerializable
{

    protected $begin = null;
    protected $end = null;
    protected $result = null;

    /**
     * @deprecated use begin()
     */
    public function start()
    {
        return $this->begin();
    }

    /**
     * @deprecated use __toString() or print(true)
     */
    public function get()
    {
        return $this->__toString();
    }

    protected function calc()
    {
        $this->result = $this->end - $this->begin;
    }

    public function getData()
    {
        return [
            'begin' => $this->begin,
            'end' => $this->end,
            'result' => $this->result,
            'print' => $this->__toString(),
        ];
    }

    public function getResult()
    {
        return $this->result;
    }

    public function __toString(){return '';}

    public function reset()
    {
        $this->begin = null;
        $this->end = null;
        $this->result = null;
        return $this;
    }

    public function print($return = false)
    {
        if($return){
            return $this->__toString();
        }
        echo $this->__toString();
        return $this;
    }

    public function toArray()
    {
        return $this->getData();
    }

    public function jsonSerialize()
    {
        return $this->getData();
    }
}