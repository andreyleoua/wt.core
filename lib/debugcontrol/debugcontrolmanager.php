<?php


namespace Wt\Core\DebugControl;


use Wt\Core\Interfaces\IDebugControl;

class DebugControlManager implements IDebugControl, \JsonSerializable
{
    /**
     * @var IDebugControl[]
     */
    protected $debugControls = [];
    protected $isRunning;

    public function isRunning($val = null)
    {
        if(!is_null($val)){
            $this->isRunning = (bool)$val;
        }
        return $this->isRunning;
    }

    /**
     * @deprecated use begin()
     */
    public function start()
    {
        return $this->begin();
    }

    /**
     * @param $name
     * @return IDebugControl|null
     */
    public function getDebugControl($name)
    {
        if(isset($this->debugControls[$name])){
            return $this->debugControls[$name];
        }
        return null;
    }

    public function addDebugControl(IDebugControl $debug, $name = null)
    {
        if(is_null($name)){
            $this->debugControls[] = $debug;
        } else {
            $this->debugControls[$name] = $debug;
        }
        return $this;
    }

    public function addDebugControls($debugControls)
    {
        foreach ($debugControls as $name => $debugControl){
            $this->addDebugControl($debugControl, $name);
        }
        return $this;
    }

    public function begin()
    {
        $this->isRunning(true);
        foreach ($this->debugControls as $debugControl){
            $debugControl->begin();
        }
        return $this;
    }

    public function end()
    {
        if(!$this->isRunning()){
            /**
             * @todo throw
             */
            return $this;
        }
        $this->isRunning(false);
        foreach ($this->debugControls as $debugControl){
            $debugControl->end();
        }
        return $this;
    }

    public function getData()
    {
        $data = [];
        foreach ($this->debugControls as $name => $debugControl){
            $data[$name] = $debugControl->getData();
        }
        return $data;
    }

    public function reset()
    {
        $this->isRunning(false);
        foreach ($this->debugControls as $debugControl){
            $debugControl->reset();
        }
        return $this;
    }

    public function __toString()
    {
        $strArray = [];
        foreach ($this->debugControls as $name => $debugControl){
            if(is_string($name)){
                $strArray[] = "$name: {$debugControl->__toString()}";
            } else {
                $strArray[] = $debugControl->__toString();
            }
        }
        return implode(', ', $strArray);
    }

    public function print($return = false)
    {
        if($return){
            return $this->__toString();
        }
        echo $this->__toString();
        return null;
    }

    public function toArray()
    {
        return $this->getData();
    }

    public function jsonSerialize()
    {
        return $this->getData();
    }
}