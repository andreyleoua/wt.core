<?php

namespace Wt\Core\DebugControl;

use Error;
use Exception;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Logger;

/**
 * Логирует записи вида через сервис логгер
 *
 * 2022-09-21 17:45:33 [INFO]: test >> pid: 2684, memory_limit: 1536M
 * 2022-09-21 17:45:35 [INFO]: test << pid: 2684, memory_limit: 1536M, time: 1.656s, memory: 96MB, memoryReal: 96MB, memoryPeak: 123.82MB, memoryPeakReal: 198MB
 */
class CheckpointsDebugControl
{
    protected $name;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var DebugControlManager
     */
    protected $debugControlManager;
    protected $isRunning = false;
    protected $data = [];
    protected $additionalData = [];

    public function __construct($name)
    {
        $this->name = $name;
        $this->data = collect();
        $this->additionalData = collect([
            'pid' => getmypid(),
            'mem_limit' => ini_get('memory_limit'),
        ]);
        $this->logger = app()->service()->logger();
        $this->debugControlManager = new DebugControlManager();
        $this->debugControlManager->addDebugControls([
            'time' => new TimeDebugControl(),
            'memory' => new MemoryDebugControl(),
            'memoryReal' => new MemoryDebugControl(true),
            'memoryPeak' => new PeakMemoryDebugControl(),
            'memoryPeakReal' => new PeakMemoryDebugControl(true),
        ]);


        set_error_handler([$this, '__handleError']);
        set_exception_handler([$this, '__handleException']);
        register_shutdown_function([$this, '__handleFatalError']);
    }

    /**
     * @deprecated use begin()
     */
    public function start()
    {
        return $this->begin();
    }

    public function getName()
    {
        return $this->name;
    }

    protected function logger()
    {
        return $this->logger;
    }

    /**
     * @return Collection
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    public function isRunning()
    {
        return $this->isRunning;
    }

    public function begin()
    {
        $this->isRunning = true;
        $this->debugControlManager->begin();
        $this->logger()->info(
            $this->getName() . ' >> ' . $this->dataToString($this->getAdditionalData()),
            $this->getData()->toArray()
        );

        return $this;
    }

    public function end()
    {
        $this->isRunning = false;
        $this->debugControlManager->end();
        $this->logger()->info(
            $this->getName() . ' << ' . $this->dataToString($this->getAdditionalData()) . ', ' . $this->debugControlManager->print(true),
            $this->getData()->toArray()
        );

        return $this;
    }

    protected function dataToString($data)
    {
        $strArray = [];
        foreach ($data as $key => $value){
            if(is_numeric($key)){
                $strArray[] = "$value";
                continue;
            }
            $strArray[] = "$key: $value";
        }

        return implode(', ', $strArray);
    }

    /**
     * @param Exception|Error $exception Exception object.
     *
     * @return void
     */
    public function __handleException($exception)
    {
        if($this->isRunning()) {
            $error = [
                'handle' => 'Exception',
                'class' => get_class($exception),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ];

            $this->getData()->set('error', $error);
            $this->end();
        }

        \Bitrix\Main\Application::getInstance()->getExceptionHandler()->handleException($exception);
    }

    public function __handleError($severity, $message, $file, $line)
    {
        if(!$this->isRunning()){
            return;
        }

        $exception = new \ErrorException($message, 0, $severity, $file, $line);
        if (($severity & (E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR))) {
            $error = [
                'handle' => 'Error',
                'class' => get_class($exception),
                'severity' => $exception->getSeverity(),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
            ];

            $this->getData()->set('error', $error);
            $this->end();
        }
    }

    public function __handleFatalError()
    {
        if(!$this->isRunning()){
            return;
        }
        $error = error_get_last();
        if ($error)
        {
            if (($error['type'] & (E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR)))
            {
                $error = array_merge(['handle' => 'FatalError'], $error);

                $this->getData()->set('error', $error);

                $this->end();
            }
        }
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        return $this->data;
    }
}