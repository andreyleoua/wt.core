<?php


namespace Wt\Core\DebugControl;


class TimeDebugControl extends ADebugControl
{

    public function begin()
    {
        $this->reset();
        $this->begin = microtime(true);
        return $this;
    }

    public function end()
    {
        $this->end = microtime(true);
        $this->calc();
        return $this;
    }

    public function __toString()
    {
        if(is_null($this->result)){
            return '';
        }
        return round($this->result, 3) . 's';
    }
}