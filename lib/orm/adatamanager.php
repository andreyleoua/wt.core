<?php

namespace Wt\Core\ORM;

use Bitrix\Main;

Main\Localization\Loc::loadMessages(__FILE__);

/**
 * getMap
 * in_form(show_in_detail_admin_page) bool default:true
 * list_edit(edit_in_list_admin_page) bool default:false
 * field_type string checkbox|text|calendar
 * hint string
 * password bool
 */
abstract class ADataManager extends Main\Entity\DataManager
{

    /**
     * @return string|void
     * @abstract
     */
    public static function getTitle()
    {
        return static::getTableName();
    }

    public static function getRowTitle()
    {
        return 'Element';
    }

    public static function getRowDescription(){
        return 'Element fields';
    }

    /**
     * Returns DB table name for entity
     *
     * @return string
     * @abstract
     */
    public static function getTableName()
    {
        return '';
    }

    /**
     * Returns entity map definition.
     * //unique
     *
     * @return array
     * @abstract
     */
    public static function getMap()
    {
        return [];
    }

    public static function createTable()
    {
        $tableEntity = static::getEntity();
        if(!static::isTableExists()) {
            $tableEntity->createDbTable();
        }
    }

    public static function isTableExists()
    {
        $tableEntity = static::getEntity();
        $tableName = static::getTableName();
        $conn = $tableEntity->getConnection();
        return $conn->isTableExists($tableName);
    }

    public static function dropTable()
    {
        $tableEntity = static::getEntity();
        $tableName = static::getTableName();
        $conn = \Bitrix\Main\Application::getInstance()->getConnection();
        try {
            $conn->dropTable($tableName);
        } catch (Main\Db\SqlQueryException $e) {
            $tableNameQuote = $conn->getSqlHelper()->quote($tableName);
            $conn->query("DROP TABLE IF EXISTS {$tableNameQuote};");
        }
    }

    public static function truncateTable()
    {
        $tableName = static::getTableName();
        $connection = static::getEntity()->getConnection();
        return $connection->truncateTable($tableName);
    }

    public  static function createIndexes()
    {
        // nothing by default
    }

    protected static function clearCache()
    {
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        if(property_exists($connection, 'column_cache')){
            if(isset($connection->column_cache[static::getTableName()])){
                unset($connection->column_cache[static::getTableName()]);
            }
        }
        if(is_callable([$connection, 'clearCaches'])){
            $connection->clearCaches();
        }
    }

    public static function addNewFields()
    {
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        static::clearCache();
        $tableName = $entity->getDBTableName();
        $sqlHelper = $connection->getSqlHelper();
        $dbFields = $connection->getTableFields($tableName);

        foreach ($entity->getFields() as $field)
        {
            $fieldName = $field->getName();

            if (!isset($dbFields[$fieldName]) && $field instanceof Main\Entity\ScalarField)
            {
                $columnName = $field->getColumnName();
                $columnType = $sqlHelper->getColumnTypeByField($field);

                $sql =
                    'ALTER TABLE ' . $sqlHelper->quote($tableName)
                    . ' ADD COLUMN ' . $sqlHelper->quote($columnName) . ' ' . $columnType;
                app()->service()->logger()->info('addNewFields', [$sql]);
                $connection->queryExecute($sql);
            }
        }
    }

    public static function deleteOldColumns()
    {
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        static::clearCache();
        $tableName = $entity->getDBTableName();
        $sqlHelper = $connection->getSqlHelper();
        $tableFields = $connection->getTableFields($tableName);
        $mapFields = $entity->getFields();

        $fixMapFields = [];
        foreach ($mapFields as $mapField){
            $fixMapFields[$mapField->getName()] = $mapField;
        }

        foreach ($tableFields as $tableField)
        {
            $tableFieldName = $tableField->getName();

            if (!isset($fixMapFields[$tableFieldName]))
            {

                $sql =
                    'ALTER TABLE ' . $sqlHelper->quote($tableName)
                    . ' DROP ' . $sqlHelper->quote($tableFieldName) . ';';

                app()->service()->logger()->info('deleteOldColumns', [$sql]);
                $connection->queryExecute($sql);
            }
        }
    }

    public static function dropIndexes($indexes)
    {
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        $tableName = $entity->getDBTableName();
        $sqlHelper = $connection->getSqlHelper();

        foreach ($indexes as $index)
        {
            try
            {
                $connection->queryExecute(sprintf(
                    'DROP INDEX %s ON %s',
                    $sqlHelper->quote($index),
                    $sqlHelper->quote($tableName)
                ));
            }
            catch (Main\DB\SqlQueryException $exception)
            {
                // not exists
            }
        }
    }

    public static function updateFieldsTypes()
    {
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        $sqlHelper = $connection->getSqlHelper();
        $tableName = $entity->getDBTableName();
        $storedTypes = static::getTableColumnTypes();
        $fieldTypes = static::getColumnTypes();

        $prevColumnName = null;
        $columnNamePull = [];

        foreach ($fieldTypes as $columnName => $fieldType)
        {
            $storedType = $storedTypes[$columnName]??null; // int(11)

            if (!$storedType) {
                $after = '';
                if(!is_null($prevColumnName) && in_array($prevColumnName, $columnNamePull)){
                    $after = " AFTER {$sqlHelper->quote($prevColumnName)}";
                }
                $sql = "ALTER TABLE {$sqlHelper->quote($tableName)} ADD {$sqlHelper->quote($columnName)} {$fieldType} NOT NULL {$after};";
                app()->service()->logger()->info('updateFieldsTypes', [$sql]);
                $connection->queryExecute($sql);

            }
            elseif(!static::typeComparison($fieldType, $storedType)) {
                $sql = "ALTER TABLE {$sqlHelper->quote($tableName)} MODIFY COLUMN {$sqlHelper->quote($columnName)} {$fieldType}";
                app()->service()->logger()->info('updateFieldsTypes', [$sql, $storedType]);
                $connection->queryExecute($sql);
            }
            $columnNamePull[] = $columnName;
            $prevColumnName = $columnName;
        }
    }

    public static function getColumnTypes()
    {
        $result = [];
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        $sqlHelper = $connection->getSqlHelper();
        foreach ($entity->getScalarFields() as $fieldName => $field)
        {
            $columnName = $field->getColumnName();
            $fieldType = $sqlHelper->getColumnTypeByField($field);
            $result[$columnName] = $fieldType;
        }

        return $result;
    }

    public static function getTableColumnTypes()
    {
        $result = [];
        $entity = static::getEntity();
        $connection = $entity->getConnection();
        $sqlHelper = $connection->getSqlHelper();
        $tableName = $entity->getDBTableName();
        $queryColumns = $connection->query(sprintf(
            'SHOW COLUMNS FROM %s',
            $sqlHelper->quote($tableName)
        ));

        while ($column = $queryColumns->fetch())
        {
            $result[$column['Field']] = $column['Type'];
        }

        return $result;
    }

    protected static function typeComparison($type1, $type2)
    {
        $re = '/([A-z]+)(?:\((\d+)\))*/m';
        $m1 = [];
        preg_match($re, $type1, $m1);
        $m2 = [];
        preg_match($re, $type2, $m2);

        $re = '/text/i';
        $isText = preg_match($re, $type1, $matches0) && preg_match($re, $type2, $matches0);

        return strtolower($m1[1]) === strtolower($m2[1]) || $isText;
    }
}