<?php
/**
 * @var \Wt\Core\Templater\Shell $this
 * @var \Wt\Core\Form\Form $form
 */

$form = $this->getContext();
?>
<div class="adm-workarea">
    <script>document.body.id = document.body.id || "bx-admin-prefix";</script>
    <form <?=\Wt\Core\Templater\Tools::getAttrByArray($form->getAttributes())?>>
        <div class="adm-detail-content-item-block">
        <table class="adm-detail-content-table edit-table"><?
            foreach ($form->getFieldCollection() as $field){
                $field->render('edit');
            }
            ?></table>
        </div>
    </form>
</div>