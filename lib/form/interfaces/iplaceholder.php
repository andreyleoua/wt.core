<?php

namespace Wt\Core\Form\Interfaces;

interface IPlaceholder
{
    public function getPlaceholder();
    public function setPlaceholder($value);
}