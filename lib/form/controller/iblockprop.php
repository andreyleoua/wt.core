<?php


namespace Wt\Core\Form\Controller;


class IBlockProp extends Base
{

    public $iblockId;
    public $elementId;
    public $propCode;

    function __construct($iblockId, $elementId, $propCode)
    {
        $this->iblockId = $iblockId;
        $this->elementId = $elementId;
        $this->propCode = $propCode;
    }

    /**
     * PROPERTY_VALUE_ID => fileId
     */
    function getValue()
    {
        if(!$this->elementId) {
            return null;
        }
        \Bitrix\Main\Loader::includeModule('iblock');
        $arr = [];
        $dbProps = \CIBlockElement::GetProperty($this->iblockId, $this->elementId, 'sort', 'asc', ['CODE' => $this->propCode]);
        while ($arItem = $dbProps->Fetch()) {
            if ($arItem['VALUE']) {
                $arr[$arItem['PROPERTY_VALUE_ID']] = $arItem['VALUE'];
            }
        }
        return $arr;
    }

    function save($requestValue)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $filePropMap = $this->getValue();
        $delList = [];
        foreach ((array)$filePropMap as $valPropId => $fileId) {
            if(!in_array($fileId, $requestValue)) {
                $delList[$valPropId] = array(
                    "VALUE" => array("del" => "Y")
                );
            } else {
                unset($requestValue[$fileId]);
            }
        }
        $el = new \CIBlockElement;
        if($delList) {
            $el->SetPropertyValueCode(
                $this->elementId,
                $this->propCode,
                $delList
            );
        }
        $res = \CIBlockElement::SetPropertyValueCode($this->elementId, $this->propCode, $requestValue);
        return $res;
    }

}