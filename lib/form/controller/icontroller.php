<?php


namespace Wt\Core\Form\Controller;


interface IController
{

    function getValue();

    function save($requestValue);
}