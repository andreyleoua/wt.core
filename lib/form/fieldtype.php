<?php


namespace Wt\Core\Form;


/**
 * @see https://www.w3schools.com/html/html_form_input_types.asp
 */
class FieldType
{
    const button = 'button';
    const checkbox = 'checkbox';
    const color = 'color';
    const date = 'date';
    const datetime_local = 'datetime-local';
    const email = 'email';
    const file = 'file';
    const hidden = 'hidden';
    const image = 'image';
    const month = 'month';
    const number = 'number';
    const password = 'password';
    const radio = 'radio';
    const range = 'range';
    const reset = 'reset';
    const search = 'search';
    const select = 'select';
    const submit = 'submit';
    const tel = 'tel';
    const text = 'text';
    const textarea = 'textarea';
    const time = 'time';
    const url = 'url';
    const week = 'week';
}