<?php


namespace Wt\Core\Form;


class InputFile
{

    public $fieldName = [];
    public $multiple = true;
    public $required = false;
    public $hint = '';
    public $moduleId = 'main';
    public $maxFileSize = 1048576 * 5;
    public $allowUpload = 'A';
    /**
     * @var $controller Controller\Base
     */
    private $controller;
    private $request = [];

    function __construct($fieldName, $request = null)
    {
        $this->fieldName = $fieldName;

        $this->request = $request;
        if(is_null($this->request)){
            $this->request = $_REQUEST;
        }
    }

    function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return array
     */
    function getRequestValue()
    {
        $val = array_filter((array)$this->request[$this->fieldName]);
        $valDel = array_filter((array)$this->request[$this->fieldName.'_del']);
        $result = [];
        foreach ($val as $id) {
            $result[$id] = $id;
        }
        foreach ($valDel as $id) {
            unset($result[$id]);
        }
        return $result;
    }

    function getValue()
    {
        if($this->controller){
            return $this->controller->getValue();
        }
        return $this->multiple?[]:0;
    }

    function save()
    {
        if($this->controller){
            $this->controller->save($this->getRequestValue());
        }
    }

    function render()
    {
        /**
         * dependencies:
         * [component - bitrix:main.file.input, template - kit]
         */
        app()->service()->templater()->render('kit:inputFile', $arParams = [
            'MODULE_ID' => $this->moduleId,
            'VALUE' => $this->getValue(),
            'MULTIPLE' => $this->multiple,
            'NAME' => $this->fieldName,
            'REQUIRED' => $this->required,
            'HINT' => $this->hint,
            'MAX_FILE_SIZE' => $this->maxFileSize,
        ]);
    }
}