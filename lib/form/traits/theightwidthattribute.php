<?php


namespace Wt\Core\Form\Traits;


trait THeightWidthAttribute
{
    public function getHeight()
    {
        return $this->getRaw('height');
    }

    public function setHeight($value)
    {
        $this->set('height', $value);
        return $this;
    }

    public function getWeight()
    {
        return $this->getRaw('width');
    }

    public function setWeight($value)
    {
        $this->set('width', $value);
        return $this;
    }
}