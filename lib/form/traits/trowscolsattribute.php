<?php


namespace Wt\Core\Form\Traits;


trait TRowsColsAttribute
{
    public function getCols()
    {
        return $this->getRaw('cols');
    }

    public function setCols($value)
    {
        $this->set('cols', $value);
        return $this;
    }

    public function getRows()
    {
        return $this->getRaw('rows');
    }

    public function setRows($value)
    {
        $this->set('rows', $value);
        return $this;
    }
}