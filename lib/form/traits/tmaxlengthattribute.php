<?php


namespace Wt\Core\Form\Traits;


trait TMaxLengthAttribute
{
    public function getMaxLength()
    {
        return $this->getRaw('maxlength');
    }

    public function setMaxLength($value)
    {
        $this->set('maxlength', $value);
        return $this;
    }
}