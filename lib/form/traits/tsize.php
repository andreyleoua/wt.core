<?php


namespace Wt\Core\Form\Traits;


trait TSize
{

    public function getSize()
    {
        return $this->getRaw('size');
    }

    public function setSize($value)
    {
        $this->set('size', $value);
        return $this;
    }
}