<?php


namespace Wt\Core\Form\Traits;


trait TOptionsAttribute
{
    public function setOptions($options = [])
    {
        $this->set('options', $options);
        return $this;
    }

    public function getOptions()
    {
        return $this->get('options', []);
    }
}