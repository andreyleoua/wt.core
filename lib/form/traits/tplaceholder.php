<?php


namespace Wt\Core\Form\Traits;


trait TPlaceholder
{
    public function getPlaceholder()
    {
        return value($this->get('placeholder', ''), $this);
    }

    public function setPlaceholder($value)
    {
        $this->set('placeholder', $value);
        return $this;
    }
}