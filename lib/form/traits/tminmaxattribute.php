<?php


namespace Wt\Core\Form\Traits;


trait TMinMaxAttribute
{
    public function getMax()
    {
        return $this->getRaw('max');
    }

    public function setMax($value)
    {
        $this->set('max', $value);
        return $this;
    }

    public function getMin()
    {
        return $this->getRaw('min');
    }

    public function setMin($value)
    {
        $this->set('min', $value);
        return $this;
    }
}