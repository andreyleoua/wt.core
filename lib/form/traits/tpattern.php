<?php


namespace Wt\Core\Form\Traits;


trait TPattern
{

    public function getPattern()
    {
        return $this->getRaw('pattern');
    }

    public function setPattern($value)
    {
        $this->set('pattern', $value);
        return $this;
    }
}