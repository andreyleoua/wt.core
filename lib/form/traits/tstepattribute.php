<?php


namespace Wt\Core\Form\Traits;


trait TStepAttribute
{
    public function getStep()
    {
        return $this->getRaw('step', 1);
    }

    public function setStep($value)
    {
        $this->set('step', $value);
        return $this;
    }
}