<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Interfaces;
use Wt\Core\Form\Traits;

class TextareaField extends DefaultField implements Interfaces\IRowsColsAttribute
{
    use Traits\TMaxLengthAttribute;
    use Traits\TPlaceholder;
    use Traits\TRowsColsAttribute;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::textarea,
        ]);
    }
}