<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;

class HiddenField extends DefaultField
{
    use Traits\TSize;
    use Traits\TPattern;
    use Traits\TPlaceholder;
    use Traits\TMaxLengthAttribute;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::hidden,
        ]);
    }
}