<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;

class ImageField extends DefaultField
{
    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::image,
        ]);
    }
}