<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;
use Wt\Core\Form\Interfaces;

class NumberField extends DefaultField implements Interfaces\IMinMaxAttribute, Interfaces\IStepAttribute
{
    use Traits\TMinMaxAttribute;
    use Traits\TStepAttribute;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::number,
            'step' => 1,
        ]);
    }
}