<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;

class ButtonField extends DefaultField
{
    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::button,
        ]);
    }
}