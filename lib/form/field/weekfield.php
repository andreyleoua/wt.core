<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;

class WeekField extends DefaultField
{
    use Traits\TMinMaxAttribute;
    use Traits\TStepAttribute;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::week,
        ]);
    }
}