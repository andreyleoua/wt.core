<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;
use Wt\Core\Form\Interfaces;

class SubmitField extends DefaultField implements Interfaces\IControlField
{
    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::submit,
            'value' => 'Send',
        ]);
    }
}