<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Interfaces;
use Wt\Core\Form\Traits;

class SelectField extends DefaultField implements Interfaces\IOptionsAttribute
{
    use Traits\TOptionsAttribute;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::select,
        ]);
    }
}