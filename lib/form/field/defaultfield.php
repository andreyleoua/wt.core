<?php

namespace Wt\Core\Form\Field;

use Wt\Core\Entity\APropertyEntity;
use Wt\Core\Form\FieldType;
use Wt\Core\Form\Form;
use Wt\Core\IFields\FormFieldFields;
use Wt\Core\Property\PropertyValueEntity;
use Wt\Core\Support\Entity;
use Wt\Core\Templater\Templater;
use Wt\Core\Form\Traits;
use Wt\Core\Form\Interfaces;

/**
 * @see https://www.w3schools.com/html/html_form_attributes.asp
 */
class DefaultField extends Entity
{

    protected $form;
    protected $params = [];
    /**
     * @var APropertyEntity
     */
    protected $propertyEntity;
    /**
     * @var PropertyValueEntity
     */
    protected $propertyValue;

    public function __construct($data = [], $params = [])
    {
        $this->params = $params;
        parent::__construct($data);
    }

    protected function getCase()
    {
        return 'kebab';
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param Form $form
     * @return DefaultField
     */
    public function setForm(Form $form)
    {
        $this->form = $form;
        return $this;
    }

    public function setPropertyEntity(APropertyEntity $propertyEntity)
    {
        $this->propertyEntity = $propertyEntity;
        return $this;
    }

    public function setPropertyValue(PropertyValueEntity $propertyValue)
    {
        $this->propertyValue = $propertyValue;
        return $this;
    }

    /**
     * @return APropertyEntity
     */
    public function getPropertyEntity()
    {
        return $this->propertyEntity;
    }

    /**
     * @return PropertyValueEntity|null
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * @return int|string
     */
    public function getParentEntityId()
    {
        if($this->getPropertyValue()){
            return $this->getPropertyValue()->getParentEntityId();
        }
        return 0;
    }

    public function isNew()
    {
        return !$this->getPropertyValue();
    }

    protected function getDefaultRaw()
    {
        return [
            'type' => FieldType::text,
        ];
    }

    protected $validator;//??

    public function setId($id)
    {
        $this->set('id', $id);
        return $this;
    }

    public function setType($type)
    {
        $this->set('type', $type);
        return $this;
    }

    public function getName()
    {
        return value($this->fields()->name);
    }

    public function setName($value)
    {
        $this->set('name', $value);
        return $this;
    }

    public function getDescription()
    {
        return value($this->get('description', ''));
    }

    public function setDescription($value)
    {
        $this->set('name', (string)$value);
        return $this;
    }

    public function getType()
    {
        return $this->get('type');
    }

    public function isHidden()
    {
        return $this->getType() === 'hidden';
    }

    public function getValue()
    {
        $value = value($this->getRaw('value'));
        return $value;
    }

    public function setValue($value)
    {
        $this->set('value', $value);
        return $this;
    }

    public function isDirty()
    {
        if($this->isMultiple()){
            return (bool)count($this->getValue());
        }
        if((string)$this->getValue() !== ''){
            return true;
        }
        if($this instanceof Interfaces\IPlaceholder && (string)$this->getPlaceholder() !== ''){
            return true;
        }
        return false;
    }

    public function getLabel()
    {
        return (string)value($this->get('label', ''));
    }

    public function setLabel($value)
    {
        $this->set('label', $value);
        return $this;
    }

    public function getId()
    {
        return $this->fields()->id;
    }

    public function getTitle()
    {
        return (string)$this->get('title', '');
    }

    public function setTitle($value)
    {
        $this->set('title', $value);
        return $this;
    }

    public function isReadonly()
    {
        return $this->fields()->readonly;
    }

    public function isRequired()
    {
        return $this->fields()->required;
    }

    public function setRequired($value)
    {
        return $this->set('required', (bool)$value);
    }

    public function isDisabled()
    {
        return $this->fields()->disabled;
    }

    public function setMultiple($value)
    {
        $this->set('multiple', $value);
        return $this;
    }

    public function isMultiple()
    {
        return $this->fields()->multiple;
    }

    public function isAutofocus()
    {
        return $this->fields()->autofocus;
    }

    public function setAutocomplete($value)
    {
        if(in_array($value, ['on', 'off'])){
            $this->set('autocomplete', $value);
            return $this;
        }
        $this->set('autocomplete', !!$value?'on':'off');
        return $this;
    }

    public function isAutocomplete()
    {
        $value = $this->fields()->autocomplete;
        return ($value === 'on') || ($value === true) || ($value === 1);
    }

    public function getAttributes()
    {
        $attributes = $this->getRaw();
        foreach ($attributes as $name => $value){
            $attributes[$name] = $value;
            switch ($name) {
                case 'autocomplete':
                    $attributes[$name] = $this->isAutocomplete()?'on':'off';
                    break;
                case 'autofocus':
                case 'multiple':
                case 'readonly':
                case 'disabled':
                case 'required':
                    $attributes[$name] = (bool)$value;
                    if($value){
                        $attributes[$name] = '';
                    } else {
                        unset($attributes[$name]);
                    }
                    break;
            }
        }
        return $attributes;
    }

    public function setAttributes($attrs = [])
    {
        foreach ($attrs as $name => $value)
        {
            $this->setAttribute($name, $value);
        }

        return $this;
    }

    public function setAttribute($name, $value)
    {
        $this->set($name, $value);
        return $this;
    }

    public function getAttribute($name)
    {
        return $this->getRaw($name, null);
    }

    /**
     * @return Templater
     */
    protected function getTemplater()
    {
        return app()->service()->templater();
    }

    /**
     * @param string $type view|edit|filter
     * @return string
     */
    protected function getTemplateName($type)
    {
        return $this->params['templates'][$type];
    }

    /**
     * @param $type
     * @return $this
     */
    public function render($type)
    {
        $result = $this->getTemplater()->getFileTemplate($this->getTemplateName($type), [], ['field' => $this]);
        if(!$result->isSuccess()){
            $result->getErrors()->render();
        } else {
            echo $result->getOb();
        }
        return $this;
    }

    /**
     * @param $type
     * @return string
     */
    public function getTemplate($type)
    {
        $result = $this->getTemplater()->getFileTemplate($this->getTemplateName($type), [], ['field' => $this]);
        if(!$result->isSuccess()){
            return $result->getErrors()->join('<br>');
        }
        return $result->getOb();
    }

    public function isSelectValue($value)
    {
        if($this->isMultiple()){
            return in_array($value, $this->getValue());
        }

        return $this->getValue() == $value;
    }

    /**
     * @return FormFieldFields
     */
    public function fields()
    {
        return parent::fields(); // TODO: Change the autogenerated stub
    }
}