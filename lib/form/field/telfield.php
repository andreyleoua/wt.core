<?php


namespace Wt\Core\Form\Field;


use Wt\Core\Form\FieldType;
use Wt\Core\Form\Traits;

class TelField extends DefaultField
{
    use Traits\TPattern;
    use Traits\TPlaceholder;

    protected function getDefaultRaw()
    {
        return array_merge(parent::getDefaultRaw(), [
            'type' => FieldType::tel,
        ]);
    }
}