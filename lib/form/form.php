<?php

namespace Wt\Core\Form;

use Wt\Core\Collection\Form\FieldCollection;
use Wt\Core\Form\Field\DefaultField;
use Wt\Core\IFields\FormFields;
use Wt\Core\Support\Entity;
use Wt\Core\Templater\Shell;
use Wt\Core\Templater\Tools;
use Wt\Core\Templater\TTemplater;

class Form extends Entity
{
    use TTemplater;

    const TARGET_BLANK = '_blank';
    const TARGET_SELF = '_self';
    const TARGET_PARENT = '_parent';
    const TARGET_TOP = '_top';

    const METHOD_GET = 'get';
    const METHOD_POST = 'post';

    const AUTOCOMPLETE_ON = 'on';
    const AUTOCOMPLETE_OFF = 'off';

    public function getCase()
    {
        return 'kebab';
    }

    /** @var FieldCollection */
    protected $fieldCollection;

    public function __construct($data = [])
    {
        parent::__construct($data);

        $this->fieldCollection = resolve(FieldCollection::class);
    }

    /**
     * @return FieldCollection|DefaultField[]
     */
    public function getFieldCollection()
    {
        return $this->fieldCollection;
    }

    public function isNoValidate()
    {
        return $this->fields()->novalidate;
    }

    public function setNoValidate($value)
    {
        return $this->set('novalidate', (bool)$value);
    }

    public function addField(Field\DefaultField $field)
    {
        $this->getFieldCollection()->push($field->setForm($this));
        return $this;
    }

    /**
     * @param FieldCollection|Field\DefaultField[] $fields
     * @return $this
     */
    public function addFields($fields)
    {
        foreach ($fields as $field){
            $this->addField($field);
        }
        return $this;
    }

    public function setValues($values = [])
    {
        foreach ((array)$values as $name => $value){
            $field = $this->getFieldCollection()->getByName($name);
            if($field){
                $field->setValue($value);
            }
        }
        return $this;
    }

    /**
     * @param $name
     * @return DefaultField|null
     */
    public function getFieldByName($name)
    {
        return $this->getFieldCollection()->getByName($name);
    }

    protected function getDefault()
    {
        return array_merge(parent::getDefaultRaw(), [
            'method' => static::METHOD_GET,
            'autocomplete' => static::AUTOCOMPLETE_OFF,
        ]);
    }

    public function getName()
    {
        return value($this->fields()->name);
    }

    public function setName($value)
    {
        $this->set('name', $value);
        return $this;
    }

    public function isAutocomplete()
    {
        $value = $this->fields()->autocomplete;
        return ($value === 'on') || ($value === true) || ($value === 1);
    }

    public function getAttributes()
    {
        $attributes = $this->getRaw();
        foreach ($attributes as $name => $value){
            $attributes[$name] = $value;
            switch ($name) {
                case 'autocomplete':
                    $attributes[$name] = $this->isAutocomplete()?'on':'off';
                    break;
                case 'novalidate':
                    $attributes[$name] = (bool)$value;
                    if((bool)$value){
                        $attributes[$name] = '';
                    } else {
                        unset($attributes[$name]);
                    }
                    break;
            }
        }
        return $attributes;
    }

    public function setAttributes($attrs = [])
    {
        foreach ($attrs as $name => $value)
        {
            $this->setAttribute($name, $value);
        }

        return $this;
    }

    public function setAttribute($name, $value)
    {
        $this->__set($name, $value);
        return $this;
    }

    public function getAttribute($name)
    {
        return $this->getRaw($name, null);
    }

    public function getAction()
    {
        return $this->fields()->action;
    }

    public function setAction($value)
    {
        $this->set('action', $value);
        return $this;
    }

    public function getMethod()
    {
        return $this->fields()->method;
    }

    public function setMethod($value)
    {
        $this->set('method', $value);
        return $this;
    }

    public function getTarget()
    {
        return $this->fields()->target;
    }

    public function setTarget($value)
    {
        $this->set('target', $value);
        return $this;
    }

    public function render()
    {
        $result = $this->templater()->getFileTemplate('form');
        if(!$result->isSuccess()){
            $result->getErrors()->render();
        }
        echo $result->getOb();
        return $this;
    }

    public function getTemplate()
    {
        $result = $this->templater()->getFileTemplate('form/default');
        return $result->getOb();
    }

    /**
     * @return FormFields
     */
    public function fields()
    {
        return parent::fields(); // TODO: Change the autogenerated stub
    }
}