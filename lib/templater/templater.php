<?php


namespace Wt\Core\Templater;


use Wt\Core\Interfaces\IRender;

class Templater implements IRender
{
    const TEMPLATE_PATH_HARD = 0;
    const TEMPLATE_PATH_SHORT = 1;
    const TEMPLATE_PATH_LONG = 2;

    public $context;

    protected $path;
    protected $namespace;
    protected $templatesPath = '/templates';
    protected $templatePathMode = self::TEMPLATE_PATH_SHORT;

    /** @var Shell $shell */
    protected $shell;


    /**
     * @param string $path
     * @param string $namespace
     * @param object|null $context
     */
    public function __construct($path, $namespace, $context = null)
    {
        $this->path = $path;
        $this->namespace = $namespace;
        $this->shell = new Shell($context);
    }

    public function getContext()
    {
        return $this->getShell()->getContext();
    }

    public function getTemplatePath($templateName)
    {
        $namespace = $this->namespace;
        $this->prepareTemplateName($templateName, $namespace);

        $templatePath = $this->normalizePath($this->path.'/'. $namespace.'/'. $this->templatesPath);
        $path = null;
        if($this->templatePathMode == self::TEMPLATE_PATH_HARD || $this->templatePathMode == self::TEMPLATE_PATH_LONG) {
            $path = $templatePath .'/'. $templateName. '/template.php';
        }
        elseif(($this->templatePathMode == self::TEMPLATE_PATH_HARD && !file_exists($path)) || $this->templatePathMode == self::TEMPLATE_PATH_SHORT) {
            $path = $templatePath .'/'. $templateName.'.php';
        }

        return $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function isFileTemplateExist($templateName)
    {
        $filePath = $this->getTemplatePath($templateName);
        return file_exists($filePath);
    }

    protected function prepareFileTemplateParams(&$templateName, &$arParams){}
    protected function prepareCallTemplateParams(&$callName, &$args){}

    public function getTemplate($templateName, $arParams = [], array $extract = [])
    {
        return $this->getFileTemplateOb($templateName, $arParams, $extract);
    }

    public function get($templateName, $arParams = [], array $extract = [])
    {
        return $this->getTemplate($templateName, $arParams, $extract);
    }

    public function render($templateName, $arParams = [], array $extract = [])
    {
        echo $this->getTemplate($templateName, $arParams, $extract);
    }

    public function getFileTemplateOb($templateName, $arParams = [], array $extract = [])
    {
        $result = $this->getFileTemplate($templateName, $arParams, $extract);
        return $result['ob'];
    }

    public function getFileTemplateReturn($templateName, $arParams = [], array $extract = [])
    {
        return $this->getFileTemplate($templateName, $arParams, $extract)['return'];
    }

    public function getFileTemplate($templateName, $arParams = [], $extract = [])
    {
        $this->prepareFileTemplateParams($templateName, $arParams);

        $filePath = $this->getTemplatePath($templateName);

        return $this->shell->getFileResult($filePath, $arParams, $extract);
    }

    public function getCallTemplateReturn($name, array $args = [])
    {
        return $this->getCallTemplate($name, $args)['return'];
    }

    public function getCallTemplateOb($name, array $args = [])
    {
        return $this->getCallTemplate($name, $args)['ob'];
    }

    public function getCallTemplate($callName, array $args = [])
    {
        if(!is_object($this->getContext())){
            return ['return' => false, 'ob' => 'context is not object', 'checkout' => []];
        }
        $this->prepareCallTemplateParams($callName, $args);

        if(!$callName) {
            return ['return' => false, 'ob' => 'Empty CallTemplateName!', 'checkout' => []];
        }

        if(is_callable([$this->getContext(), $callName])) {
            return $this->shell->getCallResult([$this->getContext(), $callName], $args);
        }

        if(property_exists($this->getContext(), $callName) && is_object($this->getContext()->{$callName})) {
            return $this->shell->getCallResult($this->getContext()->{$callName}, $args);
        }

        return ['return' => false, 'ob' => "$callName cannot be called!", 'checkout' => []];
    }

    /**
     * @return Shell
     */
    public function getShell()
    {
        return $this->shell;
    }

    /**
     * @param int $templatePathMode
     */
    public function setTemplatePathMode(int $templatePathMode)
    {
        $this->templatePathMode = $templatePathMode;
    }

    /**
     * @param $path
     * @return string
     */
    private function normalizePath($path)
    {
        return \Bitrix\Main\IO\Path::normalize($path);
    }

    private function prepareTemplateName(&$templateName, &$namespace = '')
    {
        if(str_contains($templateName, ':')) {
            $argsList = explode(':', $templateName);
            $namespace = $argsList[0];
            $templateName = $argsList[1];
        }
    }
}