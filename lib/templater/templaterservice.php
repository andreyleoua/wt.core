<?php


namespace Wt\Core\Templater;


class TemplaterService extends Templater
{

    public function __construct()
    {
        $config = app()->config();
        $templaterConfig = $config->get('templater', [])->toArray();

        $path = $_SERVER['DOCUMENT_ROOT'] . $templaterConfig['path'];
        $namespace = $templaterConfig['namespace'];

        parent::__construct($path, $namespace);
    }
}