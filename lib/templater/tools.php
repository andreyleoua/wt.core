<?php


namespace Wt\Core\Templater;


class Tools
{
    /**
     * Get html attributes string from array
     *
     * @param array $list
     *
     * @return string
     */
    public static function getAttrByArray($list)
    {
        $result = '';

        if(is_array($list)) {
            foreach ($list as $attr => $value) {
                $v = self::safeHtmlAttr($value);
                $result .= ' ' . $attr . (($v === '')?'':'="' . $v . '"');
            }
            return $result;
        }

        return (string)$list;
    }

    /**
     * Replace char " to &quot;
     *
     * @param $str
     *
     * @return mixed
     */
    public static function safeHtmlAttr($str)
    {
        return str_replace('"', '&quot;', $str);
    }

    public static function safeUrl($url)
    {
        return str_replace(' ', '%20', $url);
    }
}
