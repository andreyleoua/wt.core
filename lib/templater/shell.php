<?php


namespace Wt\Core\Templater;


use Wt\Core\Interfaces\IDebugControl;
use Wt\Core\DebugControl\EmptyDebugControl;
use Wt\Core\Support\Error;

class Shell
{
    protected $context;

    /**
     * @var IDebugControl
     */
    protected $debugControl;

    /**
     * @param $context
     */
    public function __construct($context)
    {
        $this->setDebugControl(resolve(EmptyDebugControl::class));
        $this->setContext($context);
    }

    public function __call($name, $args)
    {
        if( is_callable([$this->context, $name]) ){
            return call_user_func_array([$this->context, $name], $args);
        }
        return $this;
    }

    public function __get($name)
    {
        if( property_exists($this->context, $name) ){
            return $this->context->$name;
        }
        return "$name не найден";
    }

    public function setContext($context = false)
    {
        if(is_object($context)) {
            $this->context = $context;
        }
        return $this;
    }

    public function getContext()
    {
        return $this->context;
    }

    protected function isObStartAllowed()
    {
        if(defined('BX_BUFFER_SHUTDOWN') && BX_BUFFER_SHUTDOWN === true){
            return false;
        }
        if(defined('BX_LOCAL_REDIRECT') && BX_LOCAL_REDIRECT === true){
            return false;
        }

        return true;
    }

    /**
     * Работа с файлами
     * @param $filePath
     * @param array $arParams
     * @param array $extract
     * @return mixed
     */
    public function getFileOb($filePath, $arParams = [], array $extract = [])
    {
        return $this->getFileResult($filePath, $arParams, $extract)['ob'];
    }

    public function getFileReturn($filePath, $arParams = [], array $extract = [])
    {
        return $this->getFileResult($filePath, $arParams, $extract)['return'];
    }

    public function getFileResult($filePath, $arParams = [], array $extract = [])
    {
        $__result = new Result(['return' => null, 'ob' => '', 'filePath' => $filePath]);

        if(!$this->isObStartAllowed()) {
            //throw new \Exception('bitrix buffer shutdown');
            $__result->addError('Output Buffer is not allowed');
            return $__result;
        }
        static $cache = [];
        static $cacheFileExist = [];

        if(!isset($cacheFileExist[$filePath])) {
            $cacheFileExist[$filePath] = file_exists($filePath);
        }
        if(!$cacheFileExist[$filePath]) {
            throw new \Exception('File not found: '.$filePath);
            //$__result->addError('File not found: '.$filePath);
            return $__result;
        }

        $__return = null;

        $this->debugControl->begin();

        ob_start();
        try {
            extract($extract, EXTR_SKIP);
            if(!isset($cache[$filePath])) {
                $cache[$filePath] = file_get_contents($filePath);
            }
            $__return = eval('?>'.$cache[$filePath]);

        } catch (\Throwable $t) {
            $__error = new Error($t);
            $__error->setFile($filePath);
            $__result->setError($__error);
        }
        $__ob = ob_get_clean();

        $this->debugControl->end();

        if($__result->isSuccess()){
            $__result->setReturn($__return);
            $__result->setOb($__ob);
        }

        if(!($this->debugControl instanceof EmptyDebugControl)) {
            $__result['debugControl'] = $this->debugControl->__toString();
        }

        return $__result;
    }
    /**
     * Работа с функциями
     * @param callable $call
     * @param array $args
     * @return mixed
     */
    public function getCallOb($call, array $args = [])
    {
        return $this->getCallResult($call, $args)['ob'];
    }


    public function getCallReturn($call, array $args = [])
    {
        return $this->getCallResult($call, $args)['return'];
    }

    /**
     * @param callable $call
     * @param array $args
     * @return Result
     */
    public function getCallResult($call, array $args = [])
    {
        $__return = null;

        $this->debugControl->begin();
        $__result = new Result();

        ob_start();
        try {
            $__return = call_user_func_array($call, $args);
        } catch (\Throwable $t) {
            // Executed only in PHP 7, will not match in PHP 5.x
            $__error = new Error($t);
            $__result->setError($__error);
        } catch (\Exception $e) {
            // Executed only in PHP 5.x, will not be reached in PHP 7
            $__error = new Error($e);
            $__result->setError($__error);
        }
        $__ob = ob_get_clean();

        $this->debugControl->end();

        if($__result->isSuccess()){
            $__result->setReturn($__return);
            $__result->setOb($__ob);
        }

        if(!($this->debugControl instanceof EmptyDebugControl)) {
            $__result['debugControl'] = $this->debugControl->__toString();
        }

        return $__result;
    }

    public function setDebugControl(IDebugControl $debugControl)
    {
        $this->debugControl = $debugControl;
    }

    /**
     * @return IDebugControl
     */
    public function getDebugControl()
    {
        return $this->debugControl;
    }
}