<?php


namespace Wt\Core\Templater;

use Wt\Core;

class Result extends Core\Support\Result
{
    public function getReturn()
    {
        return $this->get('return');
    }

    public function setReturn($value)
    {
        $this->set('return', $value);
        return $this;
    }

    public function getOb()
    {
        return $this->get('ob', '');
    }

    public function setOb($value)
    {
        $this->set('ob', $value);
        return $this;
    }
}