<?php


namespace Wt\Core\Templater;


use Wt\Core\Tools;

trait TTemplater
{

    /**
     * @var $templater Templater
     */
    private $templater;

    public function templater()
    {
        if(is_null($this->templater)){
            $className = self::class?:__CLASS__;
            $namespace = basename(Tools::normalizePath($className));
            $cl = new \ReflectionClass($className);
            $path = dirname($cl->getFileName());
            if(!file_exists($path . '/' . $namespace)) {
                $namespace = strtolower($namespace);
            }
            $this->templater = resolve(Templater::class, [$path, $namespace, $this]);
        }

        return $this->templater;
    }
}