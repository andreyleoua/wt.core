<?php


namespace Wt\Core\Cache;


use Bitrix\Main;

class BitrixCache extends ACache
{
	public $defaultTtl = 3600;

    public $obCache;
	protected $cachePath;

	public function __construct()
	{
		$this->obCache = Main\Data\Cache::createInstance();
		$this->changePath('static');
	}

	public function changePath($extPath)
    {
		$this->cachePath = '!' . app()->module()->getId() . '/' . $extPath;
    }

	public function get($key, $default = null)
    {
        if( $this->obCache->initCache($this->defaultTtl, $key, $this->cachePath) ){
    		$data = $this->obCache->getVars();
    		return $data['result'];
    	}

    	return value($default);
    }

    public function put($key, $value, $ttl = null)
    {
        if( is_null($ttl) ){
            $ttl = $this->defaultTtl;
        }

    	$this->obCache->forceRewriting(true);
    	$r = $this->obCache->startDataCache($ttl, $key, $this->cachePath);
        $this->obCache->endDataCache(['result' => value($value)]);
    	$this->obCache->forceRewriting(false);
    	return $r;
    }

    public function has($key, $ttl = null)
    {
        if( is_null($ttl) ){
            $ttl = $this->defaultTtl;
        }
    	if( !$this->obCache->InitCache($ttl, $key, $this->cachePath) ){
    		return false;
    	}

    	return true;
    }

    public function remember($key, $ttl = null, $default = null)
    {
        if( is_null($ttl) ){
            $ttl = $this->defaultTtl;
        }
    	if( !$this->has($key, $ttl) ){
    		$this->put($key, value($default), $ttl);
    	}

		return $this->get($key);
    }

    public function forget($key)
    {
        if( $this->has($key) ){
            $this->obCache->Clean($key, $this->cachePath);
        }
    }

    public function clear()
    {
        $this->obCache->cleanDir($this->cachePath);
    }
}