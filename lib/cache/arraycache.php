<?php


namespace Wt\Core\Cache;


class ArrayCache extends ACache
{
    public static $cache = [];

    public function get($key, $default = null)
    {
    	if( isset(static::$cache[$key]) ){
    		return static::$cache[$key];
    	}

    	return $default;
    }

    public function put($key, $value, $ttl = null)
    {
    	static::$cache[$key] = $value;
    }

    public function has($key)
    {
    	return isset(static::$cache[$key]);
    }

    public function remember($key, $ttl, $default = null)
    {
    	if( !$this->has($key) ){
    		if( is_callable($default) ){
    			$default = call_user_func_array($default, []);
    		}
    		$this->put($key, $default);
    	}

		return $this->get($key);
    }

    public function forget($key)
    {
        if( $this->has($key) ){
            unset(static::$cache[$key]);
        }
    }

    public function clear()
    {
        static::$cache = [];
    }
}