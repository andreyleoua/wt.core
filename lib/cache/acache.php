<?php


namespace Wt\Core\Cache;


use Wt\Core\Interfaces\ICache;

abstract class ACache implements ICache
{
    protected $prefix;

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function set($key, $value, $ttl = null)
    {
        $this->put($key, $value, $ttl);
    }

    public function clear()
    {
        throw new \Exception('This method wont usage');
    }
}