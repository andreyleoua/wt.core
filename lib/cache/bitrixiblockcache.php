<?php


namespace Wt\Core\Cache;


class BitrixIblockCache extends BitrixCache
{
    public $iblockId = 0;

    public function __construct()
    {
        $this->obCache = \Bitrix\Main\Data\Cache::createInstance();
        $this->cachePath = 'wt.core/' . SITE_ID . '/auto';
    }

    public function put($key, $value, $ttl = null)
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($this->cachePath);
        $this->obCache->forceRewriting(true);
        $this->obCache->startDataCache($this->defaultTtl, $key, $this->cachePath);
        \CIBlock::registerWithTagCache($this->iblockId);
        //$CACHE_MANAGER->RegisterTag("iblock_id_".$this->iblockId);
        $CACHE_MANAGER->RegisterTag("iblock_id_new");
        $CACHE_MANAGER->EndTagCache();
        $this->obCache->endDataCache(array('result' => $value));
        $this->obCache->forceRewriting(false);
    }
}