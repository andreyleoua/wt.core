<?php


namespace Wt\Core\Log;


use \Bitrix\Main\Localization\Loc;
use Wt\Core\ORM\ADataManager;

Loc::loadMessages(__FILE__);

class DbLoggerTable extends ADataManager
{
    /**
     * Returns DB table name for entity
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'bx_logger';
    }

    public static function getRowTitle()
    {
        // TODO: Implement getRowTitle() method.
    }

    public static function getRowDescription()
    {
        // TODO: Implement getRowDescription() method.
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID',
            ),
            'level' => array(
                'data_type' => 'string',
                'title' => 'Level',
            ),
            'message' => array(
                'data_type' => 'text',
                'title' => 'Message',
            ),
            'context' => array(
                'data_type' => 'text',
                'title' => 'Context',
            ),
            'userId' => array(
                'data_type' => 'integer',
                'title' => 'User ID',
            ),
            'ip' => array(
                'data_type' => 'string',
                'title' => 'IP',
            ),
            'createdAt' => array(
                'data_type' => 'datetime',
                'title' => 'CREATED_AT',
            ),
            'updatedAt' => array(
                'data_type' => 'datetime',
                'title' => 'UPDATED_AT',
            )
        );
    }
}