<?php


namespace Wt\Core\Log;


use Bitrix\Main\Entity\AddResult;
use Wt\Core\Tools;

class DbLogger extends ALogger
{

    /**
     * @var DbLoggerTable
     */
    protected $table;

    public function __construct(DbLoggerTable $dbLoggerTable)
    {
        $this->table = $dbLoggerTable;

        $tableEntity = $this->table::getEntity();
        $tableName = $this->table::getTableName();
        $conn = \Bitrix\Main\Application::getInstance()->getConnection();
        if(!$conn->isTableExists($tableName)) {
            $tableEntity->createDbTable();
        }
    }

    public function getTable()
    {
        return $this->table;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        try {
            $result = $this->table::add($data = [
                'level' => $level,
                'message' => $message,
                'context' => serialize($context),
                'ip' => (string)Tools::getIp(),
                'pid' => getmypid(),
                'createdAt' => new \Bitrix\Main\Type\DateTime(),
                'userId' => (string)app()->service()->user()->getId(),
            ]);
        }
        catch (\Exception $e)
        {
            $result = new AddResult();
            app()->service()->logger()->error('dbLogger add', [
                $e->getMessage()
            ]);
        }
        if(!$result->isSuccess()){
            app()->service()->logger()->error('dbLogger add', $result->getErrorMessages());
        }
    }
}