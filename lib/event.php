<?php

namespace Wt\Core;

/**
 * 
 */
class Event
{
    private $propaginationStop = false;

    public function isPropagationStopped()
    {
        return $this->propaginationStop;
    }

    public function stopPropagation()
    {
        $this->propaginationStop = true;
    }
}