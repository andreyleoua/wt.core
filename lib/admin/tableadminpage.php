<?php
/**
 * use classes; dont change! dont refactoring!
 *
 * @global $by
 * @global $order
 */

namespace Wt\Core\Admin;


use Bitrix\Iblock\Grid\ActionType;
use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Data\AddResult as ORMAddResult;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\ORM\Data\Result as ORMResult;
use Bitrix\Main\Entity\Result;
use Bitrix\Main\ORM\Data\UpdateResult as ORMUpdateResult;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\ORM\Fields\EnumField as ORMEnumField;
use Bitrix\Main\Entity\EnumField;
use Bitrix\Main\ORM\Fields\Field as ORMField;
use Bitrix\Main\Entity\Field;
use Bitrix\Main\ORM\Fields\ScalarField as ORMScalarField;
use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\Type\Date;
use CAdminContextMenu;
use CAdminFilter;
use CAdminForm;
use CAdminList;
use CAdminListRow;
use CAdminMessage;
use CAdminResult;
use CAdminSorting;
use Wt\Core\ORM\ADataManager;
use Wt\Core\Tools;

class TableAdminPage extends ATableAdminPage
{

    protected $tableRowUrl;
    /**
     * @var CAdminList $adminList
     */
    protected $adminList;

    public function __construct(ADataManager $table, array $request = [], $tableRowUrl = '')
    {
        $this->tableRowUrl = $tableRowUrl;
        $this->request = $request;
        $this->table = $table;
    }

    public function getFindFormId()
    {
        return 'find_form_' . $this->getTable()::getTableName();
    }

    /**
     * @param array $data
     * @return \Bitrix\Main\Web\Uri
     */
    protected function getTableRowUri($data = [])
    {
        $uri = new \Bitrix\Main\Web\Uri($this->tableRowUrl);
        $uri->deleteParams(array_merge([], ['lang', 'action']));
        $uri->addParams($data);
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        return $uri;
    }

    public function setPageMetaData()
    {
        global $APPLICATION;
        $APPLICATION->SetTitle($this->getTable()::getTitle());
    }

    protected function getRequestFilteredFieldKeys()
    {
        $fields = [];

        //checks required fields
        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            if ($field instanceof ScalarField)
            {
                switch (get_class($field)){
                    case \Bitrix\Main\Entity\DateField::class:
                    case \Bitrix\Main\Entity\DateTimeField::class:
                        $fields[] = 'find_' . $field->getName() . '_from';
                        $fields[] = 'find_' . $field->getName() . '_to';
                        break;
                    default:
                        $fields[] = 'find_' . $field->getName();
                        break;
                }
            }
        }
        return $fields;
    }

    protected function getFilter()
    {
        $arFilter = [];

        if($this->request['del_filter'] == 'Y') {
            return $arFilter;
        }

        //checks required fields
        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            if (!($field instanceof Field))
            {
                continue;
            }
            $key = $field->getName();
            switch (get_class($field)){
                case \Bitrix\Main\Entity\DateField::class:
                case \Bitrix\Main\Entity\DateTimeField::class:
                    $findKey = 'find_' . $field->getName() . '_from';
                    if(isset($this->request[$findKey]) && ($this->request[$findKey] != '')) {
                        $arFilter['>=' . $key] = $this->request[$findKey];
                    }
                    $findKey = 'find_' . $field->getName() . '_to';
                    if(isset($this->request[$findKey]) && ($this->request[$findKey] != '')) {
                        $arFilter['<=' . $key] = $this->request[$findKey];
                    }
                    break;
                case  \Bitrix\Main\Entity\ScalarField::class:
                    $findKey = 'find_' . $field->getName();
                    if(isset($this->request[$findKey]) && ($this->request[$findKey] != '')) {
                        $arFilter['%' . $key] = $this->request[$findKey];
                    }
                    break;
                default:
                    $findKey = 'find_' . $field->getName();
                    if(isset($this->request[$findKey]) && ($this->request[$findKey] != '')) {
                        $arFilter[$key] = $this->request[$findKey];
                    }
                    break;
            }
        }
        return $arFilter;
    }

    protected function clearOptions()
    {
        $sTableID = $this->getTable()::getTableName();
        $by_name="by";
        $ord_name="order";
        global $APPLICATION;
        $uniq = md5($APPLICATION->GetCurPage());
        if(isset($GLOBALS[$by_name])) {
            unset($GLOBALS[$by_name]);
        }
        if(isset($_SESSION["SESS_SORT_BY"][$uniq])){
            unset($_SESSION["SESS_SORT_BY"][$uniq]);
        }
        \CUserOptions::DeleteOptionsByName('list', $sTableID);

        return $this;
    }

    protected function _start($by_initial=false, $order_initial=false)
    {
        $this->setPageMetaData();
        $sTableID = $this->getTable()::getTableName();
        $oSort = new CAdminSorting($sTableID, $by_initial, $order_initial);
        $this->adminList = new CAdminList($sTableID, $oSort);
        $this->adminList->InitFilter($this->getRequestFilteredFieldKeys());
        $arFilter = [];

        if(count($this->adminList->arFilterErrors) == 0) {
            $arFilter = $this->getFilter();
        }

        // Processing with actions
        global $DB;
        if($this->adminList->EditAction()) {
            foreach($this->request['FIELDS'] as $ID=>$arFields) {
                if(!$this->adminList->IsUpdated($ID)) continue;
                $DB->StartTransaction();
                $ID = IntVal($ID);
                if(($rsData = $this->getTable()::getById($ID)) && ($arData = $rsData->Fetch())) {
                    foreach($arFields as $key=>$value) $arData[$key]=$value;
                    unset($arData["PHRASES_COUNT"]);
                    if(!$this->getTable()::update($ID, $arData)) {
                        $this->adminList->AddGroupError(GetMessage("rub_save_error"), $ID);
                        $DB->Rollback();
                    }
                } else {
                    $this->adminList->AddGroupError(GetMessage("rub_save_error")." ".GetMessage("rub_no_rubric"), $ID);
                    $DB->Rollback();
                }
                $DB->Commit();
            }
        }

        if(($arID = $this->adminList->GroupAction())) {
            if($this->request['action_target']=='selected') {
                $params = [
                    'filter' => $arFilter,
                ];
                if($oSort->getField()){
                    $params['order'] = [$oSort->getField() => $oSort->getOrder()];
                }
                $rsData = $this->getTable()::getList($params);
                $arID = [];
                while($arRes = $rsData->Fetch()) {
                    $arID[] = $arRes[$this->getPrimaryCode()];
                }
            }
            foreach($arID as $ID) {
                if(strlen($ID)<=0) continue;
                $ID = IntVal($ID);
                switch($this->getAction()) {
                    case static::ACTION_DELETE:
                        @set_time_limit(0);
                        $DB->StartTransaction();
                        if(!$this->getTable()::delete($ID)) {
                            $DB->Rollback();
                            $this->adminList->AddGroupError(GetMessage("rub_del_err"), $ID);
                        }
                        $DB->Commit();
                        break;
                    case static::ACTION_ACTIVATE:
                    case static::ACTION_DEACTIVATE:
                        if(($rsData = $this->getTable()::getById($ID)) && ($arFields = $rsData->Fetch())) {
                            $r = $this->getTable()::update($ID, [
                                'ACTIVE' => ($this->getAction() == static::ACTION_ACTIVATE)?'Y':'N',
                            ]);
                            if(!$r->isSuccess()) {
                                $this->adminList->AddGroupError(GetMessage("rub_save_error"), $ID);
                            }
                        } else {
                            $this->adminList->AddGroupError(GetMessage("rub_save_error")." ".GetMessage("rub_no_rubric"), $ID);
                        }
                        break;
                }
            }
        }

        $table_name = $this->getTable()::getTableName();
        $arChildren = [];
        $arParents = [];
        if(Loader::includeModule('perfmon')){
            $obSchema = new \CPerfomanceSchema;
            $arChildren = $obSchema->GetChildren($table_name);
            $arParents = $obSchema->GetParents($table_name);
        }


        // Get items list
        $order = [];
        if(
            is_callable([$oSort, 'getField']) && is_callable([$oSort, 'getOrder']) &&
            $oSort->getField() && $oSort->getOrder()
        ){
            $order[$oSort->getField()] = $oSort->getOrder();
        } elseif($GLOBALS[$oSort->by_name] && $GLOBALS[$oSort->ord_name]){
            $order[$GLOBALS[$oSort->by_name]] = $GLOBALS[$oSort->ord_name];
        }

        $rsData = $this->getTable()::getList([
            'order' => $order,
            'filter' => $arFilter,
        ]);
        $rsData = new CAdminResult($rsData, $sTableID);
        $rsData->NavStart();
        $this->adminList->NavText($rsData->GetNavPrint(GetMessage("rub_nav")));
        $intProfilesCount = IntVal($rsData->NavRecordCount);


        if (isset($this->request["mode"]) && $this->request["mode"] == "settings") {
            app()->service()->bitrix()->restartBuffer();
        }
        $this->adminList->AddHeaders($this->getHeaderParams());

        // Build items list
        $ZeroDate = preg_replace('#[A-z]#i','0',FORMAT_DATETIME);

        while ($data = $rsData->NavNext(true, "f_")) {
            $f_id = $data[$this->getPrimaryCode()];
            $row = &$this->adminList->AddRow($f_id, $data);

            $entity = $this->getTable()::getEntity();
            foreach ($entity->getFields() as $field)
            {
                if (!($field instanceof ScalarField))
                {
                    continue;
                }
                $fieldEdit = !$field->hasParameter('list_edit')?true:$field->getParameter('list_edit');
                $userEdit = !$field->hasParameter('user_edit')?true:$field->getParameter('user_edit');
                $attributes = $field->getParameter('attributes');
                if(!is_array($attributes)){
                    $attributes = $fieldEdit?[]:false;
                }
                $fieldType = $this->getFieldType($field);

                if ($field->isPrimary())
                {
                    $row->AddViewField($this->getPrimaryCode(), "<a href='{$this->getTableRowUri([$field->getName() => $data[$field->getName()]])->getUri()}'>{$data[$field->getName()]}</a>");
                } elseif (!$userEdit) {
                    $row->AddViewField($field->getName(), "{$data[$field->getName()]}");
                } else
                    if (array_key_exists($field->getName(), $arParents) && $this->tableExists($arParents[$field->getName()]["PARENT_TABLE"])){
                        $val = '<a onmouseover="addTimer(this)" onmouseout="removeTimer(this)" href="/bitrix/admin/perfmon_table.php?set_filter=Y&table_name='.$arParents[$field->getName()]["PARENT_TABLE"].'&find='.urlencode($data[$field->getName()]).'&find_type='.$arParents[$field->getName()]["PARENT_COLUMN"].'">'.$data[$field->getName()].'</a>';
                        $row->AddViewField($field->getName(), $val);
                    } else
                        if($field->getParameter('password')){
                            if($this->isEdit()){
                                $sHTML = '<input type="password" name="FIELDS['.$f_id.']['.$field->getName().']" value="'.htmlspecialchars($field->modifyValueBeforeSave($row->arRes[$field->getName()], $data)).'" />';
                                $row->AddEditField($field->getName(), $sHTML);
                            } else {
                                $row->AddField($field->getName(), $data[$field->getName()]?'*****':'', false, true);
                            }
                        } else {
                            switch ($fieldType){
                                case 'select':
                                    $row->AddSelectField($field->getName(), $this->getSelectOptions($field, $data), $attributes);
                                    break;
                                case 'checkbox':
                                    $row->AddCheckField($field->getName(), $attributes);
                                    break;
                                case 'text':
                                    $sHTML = '<textarea rows="4" cols="40" name="FIELDS['.$f_id.']['.$field->getName().']">'.htmlspecialchars($field->modifyValueBeforeSave($row->arRes[$field->getName()], $data)).'</textarea>';
                                    $row->AddEditField($field->getName(), $sHTML);
                                    break;
                                case 'calendar':
                                    $row->AddCalendarField($field->getName(), $attributes, (bool)$field->getParameter('use_time'));
                                    break;
                                //                    default:
                                //                        $row->AddInputField($field->getName(),array("SIZE" => "30"));
                                //                        break;
                                default:
                                    $row->AddField($field->getName(), $data[$field->getName()], true, true);
                                    break;
                            }
                        }

                if($field->hasParameter('admin_view') && is_callable($field->getParameter('admin_view'))){
                    $val = (string)call_user_func_array($field->getParameter('admin_view'), [
                        $field,
                        $data[$field->getName()],
                        $data,
                        $entity,
                        $template = $row->aFields[$field->getName()]['view']['value']
                    ]);
                    $val = ($val == '')?$template:$val;
                    $row->AddViewField($field->getName(), $val);
                }
            }


            $row->AddActions($this->getTableRowContextMenu($row));
        }

        // List Footer
        $this->adminList->AddFooter(
            array(
                array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
                array("counter"=>true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"),
            )
        );
        $this->adminList->AddGroupActionTable(Array(
            "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
            "activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
            "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
        ));

        // Context menu
        $this->adminList->AddAdminContextMenu($this->getTopMenu());

        return $this;
    }

    public function start($by_initial=false, $order_initial=false)
    {
        if(!$this->tests()){
            return $this;
        }
        try {
            call_user_func_array([$this, '_start'], func_get_args());
        } catch (\Bitrix\Main\DB\SqlException $exception) {
            if($this->getTable()::isTableExists()){
                $this->getTable()::deleteOldColumns();
                $this->getTable()::updateFieldsTypes();
                $this->getTable()::addNewFields();
            } else {
                $this->getTable()::createTable();
                $this->getTable()::createIndexes();
            }
            call_user_func_array([$this, '_start'], func_get_args());
        } catch (\Bitrix\Main\SystemException $exception) {
            $this->clearOptions();
            call_user_func_array([$this, '_start'], func_get_args());
        }

        return $this;
    }

    /**
     * top context menu
     * @return array
     */
    protected function getTopMenu()
    {
        $result = [];
        $result[] = array(
            "TEXT" => 'Add',
            "LINK" => $this->getTableRowUri()->getUri(),
            "ICON" => "btn_new",
        );
        if(Loader::includeModule('perfmon')){
            $tableName = $this->getTable()::getTableName();
            $result[] = [
                'TEXT' => 'perfmon',
                'LINK' => "/bitrix/admin/perfmon_table.php?table_name=$tableName",
                'ICON' => '',
            ];
        }
        return $result;
    }

    /**
     * Build context menu
     * @param CAdminListRow $row
     * @return array
     */
    protected function getTableRowContextMenu($row)
    {
        $arActions = Array();
        $arActions[] = array(
            "ICON" => "edit",
            "DEFAULT"=>true,
            "TEXT" => 'edit',
            "ACTION"=>$this->adminList->ActionRedirect($this->getTableRowUri([$this->getPrimaryCode() => $row->id])->getUri())
        );
        $arActions[] = array(
            "ICON" => "copy",
            "DEFAULT"=>true,
            "TEXT" => 'copy',
            "ACTION"=>$this->adminList->ActionRedirect($this->getTableRowUri(['action' => 'copy', $this->getPrimaryCode() => $row->id])->getUri())
        );
        $arActions[] = array(
            "ICON" => "delete",
            "DEFAULT"=>false,
            "TEXT" => 'delete',
            "ACTION" => "if(confirm('Delete {$row->id}?')&&'u254'=='u254') ".$this->adminList->ActionDoGroup($row->id, "delete")
        );
        $arActions[] = array("SEPARATOR"=>true);

        if(isset($row->arRes["ACTIVE"])) {

            if ($row->arRes["ACTIVE"] == "Y") {
                $arActive = array(
                    "ICON" => 'edit',
                    "TEXT" => $this::ACTION_DEACTIVATE,
                    "ACTION" => $this->adminList->ActionDoGroup($row->id, $this::ACTION_DEACTIVATE),
                    "ONCLICK" => "",
                );
            } else {
                $arActive = array(
                    "ICON" => 'edit',
                    "TEXT" => $this::ACTION_ACTIVATE,
                    "ACTION" => $this->adminList->ActionDoGroup($row->id, $this::ACTION_ACTIVATE),
                    "ONCLICK" => "",
                );
            }
            $arActions[] = $arActive;
        }


        if(is_set($arActions[count($arActions)-1], "SEPARATOR")) {
            unset($arActions[count($arActions)-1]);
        }

        return $arActions;
    }

    public function show()
    {
        // Start output
        if($this->isAjax()){
            app()->service()->bitrix()->restartBuffer();
        }

        if(!$this->tests(true)){
            return $this;
        }

        $this->adminList->CheckListMode();
        //        $adminList->DisplayList();
        //        $adminList->Display();

        \CAjax::Init();
        \CJSCore::Init(array("ajax", "popup"));

        $sTableID = $this->getTable()::getTableName();
        $entity = $this->getTable()::getEntity();
        $popup = [];
        foreach ($entity->getFields() as $field)
        {
            if (!($field instanceof ScalarField))
            {
                continue;
            }
            $popup[] = $field->getTitle();
        }
        // Output filter
        $oFilter = new CAdminFilter($sTableID."_filter", $popup);
        global $APPLICATION;
        ?>
        <script>
            var toolTipCache = new Array;

            function drawTooltip(result, _this)
            {
                if (!_this) _this = this;

                _this.toolTip = BX.PopupWindowManager.create(
                    'table_tooltip_' + (parseInt(Math.random() * 100000)), _this,
                    {
                        autoHide: true,
                        closeIcon: true,
                        closeByEsc: true,
                        content: result
                    }
                );

                _this.toolTip.show();
                toolTipCache[_this.href] = result;
            }

            function sendRequest()
            {
                if (this.toolTip)
                    this.toolTip.show();
                else if (toolTipCache[this.href])
                    drawTooltip(toolTipCache[this.href], this);
                else
                    BX.ajax.get(
                        this.href + '&sessid=' + BX.message('bitrix_sessid') + '&ajax_tooltip=y',
                        BX.proxy(drawTooltip, this)
                    );
            }

            function addTimer(p_href)
            {
                p_href.timerID = setTimeout(BX.proxy(sendRequest, p_href), 1000);
            }

            function removeTimer(p_href)
            {
                if (p_href.timerID)
                {
                    clearTimeout(p_href.timerID);
                    p_href.timerID = null;
                }
            }
        </script>
        <form name="<?=$this->getFindFormId()?>" method="get" action="<?=$APPLICATION->GetCurPage();?>">
            <?$oFilter->Begin();

            foreach ($entity->getFields() as $field)
            {
                if (!($field instanceof ScalarField))
                {
                    continue;
                }
                $fieldAdminType = $field->getParameter('field_type');
                switch ($fieldAdminType){
                    case 'checkbox':
                        ?>
                        <tr>
                            <td><?=$field->getTitle()?>:</td>
                            <td>
                                <?$arActiveValues = array(
                                    'reference' => array('Yes', 'No',),
                                    'reference_id' => array('Y','N')
                                );?>
                                <?=SelectBoxFromArray('find_' . $field->getName(), $arActiveValues, $this->request['find_' . $field->getName()], GetMessage('MAIN_ALL'), '');?>
                            </td>
                        </tr>
                        <?
                        break;
                    case 'calendar':
                        ?>
                        <tr>
                            <td><?=$field->getTitle()?>:</td>
                            <td><?=CalendarPeriod(
                                'find_' . $field->getName().'_from',
                                htmlspecialcharsbx($this->request['find_' . $field->getName().'_from']),
                                'find_' . $field->getName().'_to',
                                htmlspecialcharsbx($this->request['find_' . $field->getName().'_to']),
                                $this->getFindFormId(),
                                'Y'
                                )?></td>
                        </tr>
                        <?
                        break;
                    default:
                        ?>
                        <tr>
                            <td><b><?=$field->getTitle()?>:</b></td>
                            <td>
                                <input type="text" size="40" name="find_<?=$field->getName()?>" value="<?=htmlspecialcharsbx($this->request['find_' . $field->getName()]);?>" />
                            </td>
                        </tr>
                        <?
                        break;
                }
            }
            $oFilter->Buttons([
                'table_id'=>$sTableID,
                'url'=>$APPLICATION->GetCurPage(),
                'form'=> $this->getFindFormId()
            ]);?>
            <?$oFilter->End();?>
        </form>
        <?

        // Output
        $this->adminList->DisplayList();

        return $this;
    }

    /**
     * @return CAdminList
     */
    public function getAdminList()
    {
        return $this->adminList;
    }

    protected function getHeaderParams()
    {
        $fields = [];

        //checks required fields
        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            if ($field instanceof ScalarField)
            {
                $showList = true;
                if($field->hasParameter('show_list')){
                    $showList = $field->getParameter('show_list');
                }
                if(!$showList){
                    continue;
                }


                $fields[] = [
                    "id" => $field->getName(),
                    "content" => $field->getTitle(),
                    "sort" => $field->getName(),
                    "align" => $field->hasParameter('align')?$field->getParameter('align'):'left',
                    "default" => $field->hasParameter('default')?$field->getParameter('default'):true,
                ];
            }
        }
        return $fields;
    }

    /**
     * @return CAdminContextMenu
     */
    public function getAdminContextMenu()
    {

    }
}