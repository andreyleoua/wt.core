<?php


namespace Wt\Core\Admin;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Wt\Core\Agent\AAgentCase;

class Templater
{

    /**
     * @var SiteSettingsAdminPage $adminPage
     */
    protected $adminPage;

    public function __construct($adminPage)
    {
        $this->adminPage = $adminPage;
    }

    public function render()
    {

        extract($_REQUEST);
        global $APPLICATION;
        IncludeModuleLangFile(__FILE__);

        $moduleID = $this->adminPage->getModuleId();
        $arHideProps = array();
        \Bitrix\Main\Loader::includeModule($moduleID);


            $by = "id";
            $sort = "asc";

            $arSites = array();
            $db_res = \CSite::GetList($by, $sort, array("ACTIVE"=>"Y"));
            while($res = $db_res->Fetch()){
                $arSites[] = $res;
            }

            $arTabs = array();
            foreach($arSites as $key => $arSite){
                $arBackParams = $this->adminPage->getSettings($arSite["ID"]);
                $arTabs[] = array(
                    "DIV" => "edit".($key+1),
                    "TAB" => GetMessage("MAIN_OPTIONS_SITE_TITLE", array("#SITE_NAME#" => $arSite["NAME"], "#SITE_ID#" => $arSite["ID"])),
                    "ICON" => "settings",
                    // "TITLE" => GetMessage("MAIN_OPTIONS_TITLE"),
                    "PAGE_TYPE" => "site_settings",
                    "SITE_ID" => $arSite["ID"],
                    "SITE_DIR" => $arSite["DIR"],
                    //"TEMPLATE" => Admin::GetSiteTemplate($arSite["ID"]),
                    "OPTIONS" => $arBackParams,
                );
            }

            $tabControl = new \CAdminTabControl("tabControl", $arTabs);

            if($_SERVER['REQUEST_METHOD'] === 'POST' && strlen($Update.$Apply.$RestoreDefaults) > 0 && check_bitrix_sessid()){
                global $APPLICATION, $CACHE_MANAGER;

                if(strlen($RestoreDefaults) > 0){
                    Option::delete($this->adminPage->getModuleId());
                    $APPLICATION->DelGroupRight($this->adminPage->getModuleId());
                }
                else{
                    Option::delete($this->adminPage->getModuleId(), array("name" => "sid"));

                    foreach($arTabs as $key => $arTab){
                        $optionsSiteID = $arTab["SITE_ID"];
                        foreach($this->adminPage->getConfig() as $blockCode => $arBlock){
                            if(in_array($blockCode,$arHideProps)) continue;
                            foreach($arBlock["OPTIONS"] as $optionCode => $arOption){
                                if($arOption['TYPE'] === 'array'){
                                    $arOptionsRequiredKeys = array();
                                    $arOptionsKeys = array_keys($arOption['OPTIONS']);
                                    $itemsKeysCount = (int)Option::get($moduleID, $optionCode, '0', $optionsSiteID);
                                    $fullKeysCount = 0;

                                    if($arOption['OPTIONS'] && is_array($arOption['OPTIONS']))
                                    {
                                        foreach($arOption['OPTIONS'] as $_optionCode => $_arOption){
                                            if(strlen($_arOption['REQUIRED']) && $_arOption['REQUIRED'] === 'Y')
                                                $arOptionsRequiredKeys[] = $_optionCode;

                                        }
                                        if($arOption['MULTIPLE'] == 'Y') {
                                            for($itemKey = 0, $cnt = $itemsKeysCount + 50; $itemKey <= $cnt; ++$itemKey){
                                                $bFull = true;
                                                if($arOptionsRequiredKeys){
                                                    foreach($arOptionsRequiredKeys as $_optionCode){
                                                        if(!strlen($_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$itemKey.'_'.$optionsSiteID]))
                                                        {
                                                            $bFull = false;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if($bFull){
                                                    foreach($arOptionsKeys as $_optionCode){
                                                        $newOptionValue = $_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$itemKey.'_'.$optionsSiteID];
                                                        Option::set($moduleID, $optionCode.'_array_'.$_optionCode.'_'.$fullKeysCount, $newOptionValue, $optionsSiteID);
                                                        unset($_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$itemKey.'_'.$optionsSiteID]);
                                                        unset($_FILES[$optionCode.'_array_'.$_optionCode.'_'.$itemKey.'_'.$optionsSiteID]);
                                                    }

                                                    ++$fullKeysCount;
                                                }
                                            }
                                        } else {
                                            $bFull = true;
                                            if($arOptionsRequiredKeys){
                                                foreach($arOptionsRequiredKeys as $_optionCode){
                                                    if(!strlen($_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$optionsSiteID]))
                                                    {
                                                        $bFull = false;
                                                        break;
                                                    }
                                                }
                                            }
                                            if($bFull){
                                                foreach($arOptionsKeys as $_optionCode){
                                                    $newOptionValue = $_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$optionsSiteID];
                                                    Option::set($moduleID, $optionCode.'_array_'.$_optionCode, $newOptionValue, $optionsSiteID);
                                                    unset($_REQUEST[$optionCode.'_array_'.$_optionCode.'_'.$optionsSiteID]);
                                                    unset($_FILES[$optionCode.'_array_'.$_optionCode.'_'.$optionsSiteID]);
                                                }
                                            }
                                        }
                                    }
                                    if($arOption['MULTIPLE'] == 'Y') {
                                        Option::set($moduleID, $optionCode, $fullKeysCount, $optionsSiteID);
                                    }
                                }
                                else{
                                    $newVal = $_REQUEST[$optionCode."_".$optionsSiteID];

                                    if($arOption["TYPE"] == "checkbox"){
                                        if(!strlen($newVal) || $newVal != "Y")
                                            $newVal = "N";

                                        if(isset($arOption['DEPENDENT_PARAMS']) && $arOption['DEPENDENT_PARAMS'])
                                        {
                                            foreach($arOption['DEPENDENT_PARAMS'] as $keyOption => $arOtionValue)
                                            {
                                                if(isset($arTab["OPTIONS"][$keyOption]))
                                                {
                                                    $newDependentVal = $_REQUEST[$keyOption."_".$optionsSiteID];
                                                    if((!strlen($newDependentVal) || $newDependentVal != "Y") && $arOtionValue["TYPE"] == "checkbox"){
                                                        $newDependentVal = "N";
                                                    }

                                                    if($keyOption == "YA_COUNTER_ID" && strlen($newDependentVal))
                                                        $newDependentVal = str_replace('yaCounter', '', $newDependentVal);

                                                    Option::set($moduleID, $keyOption, $newDependentVal, $optionsSiteID);
                                                }
                                            }
                                        }
                                    }
                                    elseif($arOption["TYPE"] == "file"){

                                        if($arOption['MULTIPLE'] == 'Y') {
                                            $arValueDefault = serialize(array());
                                            $newVal = unserialize(\COption::GetOptionString($moduleID, $optionCode, $arValueDefault, $optionsSiteID));

                                            $arValues = (array)$newVal;
                                            $arRawFiles = [];
                                            foreach ($_FILES[$optionCode."_".$optionsSiteID] as $key => $data) {
                                                foreach ($data as $ind => $value) {
                                                    $arRawFiles[$ind][$key] = $value;
                                                }
                                            }
                                            foreach ((array)$_REQUEST[$optionCode."_".$optionsSiteID.'_descr'] as $ind => $descr) {
                                                if((integer)$ind) {
                                                    $arRawFiles[$ind]['old_file'] = $ind;
                                                }
                                                $arRawFiles[$ind]['description'] = (string)$descr;
                                            }
                                            foreach ((array)$_REQUEST[$optionCode."_".$optionsSiteID.'_del'] as $ind => $del) {
                                                if((integer)$ind) {
                                                    $arRawFiles[$ind]['old_file'] = $ind;
                                                    $arRawFiles[$ind]['del'] = (string)$del;
                                                }
                                            }
                                            foreach ($arRawFiles as $arFile) {
                                                if($arFile['del'] == 'Y') {
                                                    $arValues = \Wt\Core\Type\Arr::removeValue($arValues, $ind);
                                                }
                                                $fileID = (integer)\CFile::SaveFile($arFile, $this->adminPage->getModuleId());
                                                \CFile::UpdateDesc($fileID, $arFile['description']);
                                                if($fileID) {
                                                    $arValues[] = $fileID;
                                                }
                                            }

                                            $newVal = $arValues;
                                        }
                                        else {
                                            $arValueDefault = 0;
                                            $newVal = unserialize(\COption::GetOptionString($moduleID, $optionCode, $arValueDefault, $optionsSiteID));


                                            if(isset($_REQUEST[$optionCode."_".$optionsSiteID.'_del']) || (isset($_FILES[$optionCode."_".$optionsSiteID]) && strlen($_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['0']))){
                                                $arValues = $newVal;
                                                $arValues = (int)$arValues;
                                                \CFile::Delete($arValues);
                                                $newVal = 0;
                                            }

                                            if(isset($_FILES[$optionCode."_".$optionsSiteID]) && (strlen($_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['n0']) || strlen($_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['0']))){
                                                $arValues = 0;
                                                $absFilePath = (strlen($_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['n0']) ? $_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['n0'] : $_FILES[$optionCode."_".$optionsSiteID]['tmp_name']['0']);
                                                $arOriginalName = (strlen($_FILES[$optionCode."_".$optionsSiteID]['name']['n0']) ? $_FILES[$optionCode."_".$optionsSiteID]['name']['n0'] : $_FILES[$optionCode."_".$optionsSiteID]['name']['0']);
                                                $arOriginalDescr = (strlen($_REQUEST[$optionCode."_".$optionsSiteID.'_descr']['n0']) ? $_REQUEST[$optionCode."_".$optionsSiteID.'_descr']['n0'] : $_REQUEST[$optionCode."_".$optionsSiteID.'_descr']['0']);
                                                if(file_exists($absFilePath)){
                                                    $arFile = \CFile::MakeFileArray($absFilePath);
                                                    $arFile['name'] = $arOriginalName; // for original file extension

                                                    if($bIsIco = strpos($arOriginalName, '.ico') !== false){
                                                        $script_files = \COption::GetOptionString("fileman", "~script_files", "php,php3,php4,php5,php6,phtml,pl,asp,aspx,cgi,dll,exe,ico,shtm,shtml,fcg,fcgi,fpl,asmx,pht,py,psp,var");
                                                        $arScriptFiles = explode(',', $script_files);
                                                        if(($p = array_search('ico', $arScriptFiles)) !== false)
                                                            unset($arScriptFiles[$p]);

                                                        $tmp = implode(',', $arScriptFiles);
                                                        Option::set("fileman", "~script_files", $tmp);
                                                    }

                                                    if($fileID = \CFile::SaveFile($arFile, $this->adminPage->getModuleId())) {
                                                        $arValues = $fileID;
                                                        \CFile::UpdateDesc($fileID, $arOriginalDescr);
                                                    }

                                                    if($bIsIco)
                                                        Option::set("fileman", "~script_files", $script_files);
                                                }
                                                $newVal = (int)$arValues;
                                            }
                                        }
                                        $newVal = serialize($newVal);
                                        Option::set($moduleID, $optionCode, $newVal, $optionsSiteID);

                                        unset($arTab["OPTIONS"][$optionCode]);
                                    }
                                    elseif($arOption["TYPE"] == "multiselectbox"){
                                        $newVal = @implode(",", $newVal);
                                    }

                                    if($arOption["TYPE"] != "file")
                                        $arTab["OPTIONS"][$optionCode] = $newVal;

                                    if(is_array($newVal)){
                                        $newVal = serialize($newVal);
                                    }

                                    Option::set($moduleID, $optionCode, $newVal, $optionsSiteID);

                                }
                            }
                        }

//                        \CBitrixComponent::clearComponentCache('bitrix:catalog.element', $optionsSiteID);
//                        \CBitrixComponent::clearComponentCache('bitrix:form.result.new', $optionsSiteID);
//                        \CBitrixComponent::clearComponentCache('bitrix:catalog.section', $optionsSiteID);
//                        \CBitrixComponent::clearComponentCache('bitrix:catalog.bigdata.products', $optionsSiteID);
//                        \CBitrixComponent::clearComponentCache('bitrix:catalog.store.amount', $optionsSiteID);
//                        \CBitrixComponent::clearComponentCache('bitrix:menu', $optionsSiteID);
                        $arTabs[$key] = $arTab;
                    }
                }

                // clear composite cache
//                if($compositeMode = Admin::IsCompositeEnabled()){
//                    $obCache = new \CPHPCache();
//                    $obCache->CleanDir('', 'html_pages');
//                    Admin::EnableComposite($compositeMode === 'AUTO_COMPOSITE');
//                }

                $APPLICATION->RestartBuffer();
            }

            \CAjax::Init();
            ?>
            <?if(!count($arTabs)):?>
                <div class="adm-info-message-wrap adm-info-message-red">
                    <div class="adm-info-message">
                        <div class="adm-info-message-title"><?=GetMessage("ASPRO_NEXT_NO_SITE_INSTALLED", array("#SESSION_ID#"=>bitrix_sessid_get()))?></div>
                        <div class="adm-info-message-icon"></div>
                    </div>
                </div>
            <?else:?>
                <?$tabControl->Begin();?>

                <form method="post" class="next_options" enctype="multipart/form-data" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>">
                    <?=bitrix_sessid_post();?>
                    <?
                    $moduleSaleInstalled = \CModule::IncludeModule('sale');


                    foreach($arTabs as $key => $arTab){
                        $tabControl->BeginNextTab();
                        if($arTab["SITE_ID"]){
                            $optionsSiteID = $arTab["SITE_ID"];
                            foreach($this->adminPage->getConfig() as $blockCode => $arBlock)
                            {?>
                                <?if(in_array($blockCode,$arHideProps)) continue;?>
                                <tr class="heading"><td colspan="2"><?=$arBlock["TITLE"]?></td></tr>
                                <?
                                foreach($arBlock["OPTIONS"] as $optionCode => $arOption)
                                {
                                    if(array_key_exists($optionCode, $arTab["OPTIONS"]) || $arOption["TYPE"] == 'note' || $arOption["TYPE"] == 'includefile')
                                    {
                                        $arControllerOption = \CControllerClient::GetInstalledOptions($module_id);

                                        if($arOption['TYPE'] === 'array_new_ver')
                                        {
                                            $itemsCount = Option::get($moduleID, $optionCode, 0, $optionsSiteID);
                                            if($arOption['OPTIONS'] && is_array($arOption['OPTIONS']))
                                            {
                                                $arOptionsKeys = array_keys($arOption['OPTIONS']);
                                                $newItemHtml = '';
                                                ?>
                                                <tr>
                                                    <td style="text-align:center;">
                                                        <?=$arOption["TITLE"]?>
                                                        <div class="item array <?=($itemsCount ? '' : 'empty_block');?> js_block" data-class="<?=$optionCode;?>">
                                                            <div >
                                                                <div class="aspro-admin-item">
                                                                    <?if($optionCode !== 'HEADER_PHONES2'):?>
                                                                        <div class="wrapper has_title no_drag">
                                                                            <div class="inner_wrapper">
                                                                                <?foreach($arOptionsKeys as $_optionKey):?>
                                                                                    <div class="inner">
                                                                                        <div class="title_wrapper"><div class="subtitle"><?=$arOption['OPTIONS'][$_optionKey]['TITLE']?></div></div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <?$this->ShowAdminRow(
                                                                                                    $optionCode.'_array_'.$_optionKey.'_#INDEX#',
                                                                                                    $arOption['OPTIONS'][$_optionKey],
                                                                                                    $arTab,
                                                                                                    $arControllerOption
                                                                                                );?>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                <?endforeach;?>
                                                                            </div>
                                                                        </div>
                                                                    <?endif;?>
                                                                    <?for($itemIndex = 0; $itemIndex <= $itemsCount; ++$itemIndex):?>
                                                                        <?$bNew = $itemIndex == $itemsCount;?>
                                                                        <?if($bNew):?><?ob_start();?><?endif;?>
                                                                        <div class="wrapper">
                                                                            <div class="inner_wrapper">
                                                                                <?foreach($arOptionsKeys as $_optionKey):?>
                                                                                    <div class="inner">
                                                                                        <table>
                                                                                            <tr>
                                                                                        <?$this->ShowAdminRow(
                                                                                            $optionCode.'_array_'.$_optionKey.'_'.($bNew ? '#INDEX#' : $itemIndex),
                                                                                            $arOption['OPTIONS'][$_optionKey],
                                                                                            $arTab,
                                                                                            $arControllerOption
                                                                                        );?>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                <?endforeach;?>
                                                                                <div class="remove" title="<?=Loc::getMessage("REMOVE_ITEM")?>"></div>
                                                                                <div class="drag" title="<?=Loc::getMessage("TRANSFORM_ITEM")?>"></div>
                                                                            </div>
                                                                        </div>
                                                                        <?if($bNew):?><?$newItemHtml = ob_get_clean();?><?endif;?>
                                                                    <?endfor;?>
                                                                </div>
                                                                <div class="new-item-html" style="display:none;"><?=str_replace('no_drag', '', $newItemHtml)?></div>
                                                                <div>
                                                                    <a href="javascript:;" class="adm-btn adm-btn-save adm-btn-add"><?=GetMessage('OPTIONS_ADD_BUTTON_TITLE')?></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?}
                                        }
                                        elseif($arOption['TYPE'] === 'array'){
                                            $itemsKeysCount = Option::get($moduleID, $optionCode, 0, $optionsSiteID);
                                            if($arOption['OPTIONS'] && is_array($arOption['OPTIONS'])){
                                                $arOptionsKeys = array_keys($arOption['OPTIONS']);

                                                if($arOption['MULTIPLE'] === 'Y'){

                                                    ?>
                                                    <tr>
                                                        <td style="text-align:center;" colspan="2"><?=$arOption["TITLE"]?></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="2">
                                                        <table class="aspro-admin-item-table wt-admin-table">
                                                            <tr style="text-align:center;">
                                                                <?
                                                                for($itemKey = 0, $cnt = $itemsKeysCount; $itemKey <= $cnt; ++$itemKey){
                                                                $_arParameters = array();
                                                                foreach($arOptionsKeys as $_optionKey){
                                                                    $_arParameters[$optionCode.'_array_'.$_optionKey.'_'.($itemKey != $cnt ? $itemKey : 'new')] = $arOption['OPTIONS'][$_optionKey];
                                                                    if(!$itemKey){
                                                                        ?><th colspan="2"><?=$arOption['OPTIONS'][$_optionKey]['TITLE']?></th><?
                                                                    }
                                                                }
                                                                ?>
                                                            </tr>
                                                            <tr class="aspro-admin-item<?=(!$itemKey ? ' first' : '')?><?=($itemKey == $itemsKeysCount - 1 ? ' last' : '')?><?=($itemKey == $cnt ? ' new' : '')?>" data-itemkey="<?=$itemKey?>" style="text-align:center;"><?
                                                                foreach($_arParameters as $_optionCode => $_arOption){
                                                                    $this->ShowAdminRow($_optionCode, $_arOption, $arTab, $arControllerOption);
                                                                }
                                                                ?><td class="rowcontrol"><span class="up"></span><span class="down"></span><span class="remove"></span></td>
                                                            </tr><?
                                                            }
                                                            ?>
                                                            <tr style="text-align:center;">
                                                                <td><a href="javascript:;" class="adm-btn adm-btn-save adm-btn-add" title="<?=GetMessage('PRIME_OPTIONS_ADD_BUTTON_TITLE')?>"><?=GetMessage('OPTIONS_ADD_BUTTON_TITLE')?></a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    </tr><?
                                                }
                                                else {

                                                    ?>
                                                    <tr>
                                                        <td style="text-align:center;" colspan="2"><?=$arOption["TITLE"]?></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="2">
                                                        <table class="wt-admin-table table table-striped table-bordered table-sm" id="<?=$arOption['TABLE_ID']?>" style="width: 100%;">
                                                            <thead>
                                                            <tr class="tr_mdb">
                                                            <?
                                                            foreach((array)$arOption['HEAD'] as $tHead){
                                                                ?><th class="th_mdb th-sm" style="<?=$this->getStyleByArray($tHead['STYLE_LIST'])?>"><?=$tHead['TITLE']?></th><?
                                                            }
                                                            ?>
                                                            </tr>
                                                            </thead>
                                                            <?
                                                            $itemKey = 0;
                                                            $_arParameters = array();
                                                            foreach($arOptionsKeys as $_optionKey){
                                                                $_arParameters[$optionCode.'_array_'.$_optionKey] = $arOption['OPTIONS'][$_optionKey];
                                                            }
                                                            ?>
                                                            <tbody>
                                                            <?
                                                            foreach($_arParameters as $_optionCode => $_arOption){
                                                                ?><tr class="tr_mdb" data-itemkey="<?=$itemKey?>" style="text-align:center;"><?

                                                                    $this->ShowAdminRow($_optionCode, $_arOption, $arTab, $arControllerOption);
                                                                ?></tr><?
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    </tr><?
                                                }
                                            }
                                        }
                                        else {
                                            $style = "";
                                            ?>
                                            <tr data-optioncode="<?=$optionCode;?>" <?=$style;?>>
                                                <?php $this->ShowAdminRow($optionCode, $arOption, $arTab, $arControllerOption);?>
                                            </tr>
                                            <?if(isset($arOption['DEPENDENT_PARAMS']) && $arOption['DEPENDENT_PARAMS']): //dependent params?>
                                                <?foreach($arOption['DEPENDENT_PARAMS'] as $key => $arValue):?>
                                                    <?if(!isset($arValue['CONDITIONAL_VALUE']) || ($arValue['CONDITIONAL_VALUE'] && $arTab["OPTIONS"][$optionCode] == $arValue['CONDITIONAL_VALUE']))
                                                        $style = "style='display:table-row'";
                                                    else
                                                        $style = "style='display:none'";
                                                    ?>
                                                    <tr data-optioncode="<?=$key;?>" class="depend-block <?=$key?>" <?=((isset($arValue['CONDITIONAL_VALUE']) && $arValue['CONDITIONAL_VALUE']) ? "data-show='".$arValue['CONDITIONAL_VALUE']."'" : "");?> data-parent='<?=$optionCode."_".$arTab["SITE_ID"]?>' <?=$style;?>><?$this->ShowAdminRow($key, $arValue, $arTab, $arControllerOption);?></tr>
                                                <?endforeach;?>
                                            <?endif;?>
                                            <?
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if($_SERVER['REQUEST_METHOD'] == "POST" && strlen($Update.$Apply.$RestoreDefaults) && check_bitrix_sessid())
                    {
                        if(strlen($Update) && strlen($_REQUEST["back_url_settings"]))
                            LocalRedirect($_REQUEST["back_url_settings"]);
                        else
                            LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
                    }?>
                    <?$tabControl->Buttons();?>
                    <input type="submit" name="Apply" class="submit-btn" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
                    <?if(strlen($_REQUEST["back_url_settings"]) > 0): ?>
                        <input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?=htmlspecialcharsbx(\CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
                        <input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
                    <?endif;?>
                    <script type="text/javascript">
                        $(document).ready(function() {

                            $('.item.array .adm-btn-save.adm-btn-add').click(function(){
                                var _this = $(this);
                                var newItemHtml = _this.closest('.item.array').find('.new-item-html').html();
                                var $array = _this.closest('.item.array');
                                newItemHtml = newItemHtml.replace(/#INDEX#/g, $array.find('.aspro-admin-item .wrapper').length);
                                $(newItemHtml).appendTo($array.find('.aspro-admin-item'));
                                $array.removeClass('empty_block');
                            })
                            $('.aspro-admin-item-table .adm-btn-add').click(function() {
                                var $table = $(this).closest('.aspro-admin-item-table');
                                var $newItem = $table.find('.aspro-admin-item.new');
                                if($newItem.length){
                                    var lastItemKey = $table.find('.aspro-admin-item.last').length ? $table.find('.aspro-admin-item.last').attr('data-itemkey') * 1 : -1;
                                    var $clone = $newItem.clone().insertBefore($newItem).removeClass('new');
                                    $clone.attr('data-itemkey', lastItemKey + 1);
                                    $clone.find('td:not(.rowcontrol)').each(function(i) {
                                        var name = $(this).find('*[name]:first-of-type').attr('name');
                                        var newName = name.replace('_new_', '_' + (lastItemKey + 1) + '_');
                                        $(this).find('*[name]:first-of-type').attr('name', newName);
                                    });
                                }
                                $table.find('.aspro-admin-item').removeClass('first, last');
                                $table.find('.aspro-admin-item:not(.new)').first().addClass('first');
                                $table.find('.aspro-admin-item:not(.new)').last().addClass('last');
                            });

                            $(document).on('click', '.rowcontrol>span', function() {
                                var action = ($(this).hasClass('up') ? 'up' : ($(this).hasClass('down') ? 'down' : 'remove'));
                                var $table = $(this).closest('.aspro-admin-item-table');
                                var $item = $(this).closest('.aspro-admin-item');
                                var itemKey = $item.attr('data-itemkey');

                                if(action === 'up'){
                                    var prevItemKey = $item.prev().attr('data-itemkey');
                                    $item.find('td:not(.rowcontrol)').each(function(i) {
                                        var name = $(this).find('*[name]:first-of-type').attr('name');
                                        if(typeof(name) !== 'undefined'){
                                            var newName = name.replace('_' + itemKey + '_', '_' + prevItemKey + '_');
                                            $(this).find('*[name]:first-of-type').attr('name', newName);
                                            var name = $item.prev().find('td:not(.rowcontrol)').eq(i).find('*[name]:first-of-type').attr('name');
                                            var newName = name.replace('_' + prevItemKey + '_', '_' + itemKey + '_');
                                            $item.prev().find('td:not(.rowcontrol)').eq(i).find('*[name]:first-of-type').attr('name', newName);
                                        }
                                    });
                                    $item.attr('data-itemkey', prevItemKey);
                                    $item.prev().attr('data-itemkey', itemKey);
                                    $item.clone().insertBefore($item.prev());
                                }
                                else if(action === 'down'){
                                    var nextItemKey = $item.next().attr('data-itemkey');
                                    $item.find('td:not(.rowcontrol)').each(function(i) {
                                        var name = $(this).find('*[name]:first-of-type').attr('name');
                                        if(typeof(name) !== 'undefined'){
                                            var newName = name.replace('_' + itemKey + '_', '_' + nextItemKey + '_');
                                            $(this).find('*[name]:first-of-type').attr('name', newName);
                                            var name = $item.next().find('td:not(.rowcontrol)').eq(i).find('*[name]:first-of-type').attr('name');
                                            var newName = name.replace('_' + nextItemKey + '_', '_' + itemKey + '_');
                                            $item.next().find('td:not(.rowcontrol)').eq(i).find('*[name]:first-of-type').attr('name', newName);
                                        }
                                    });
                                    $item.attr('data-itemkey', nextItemKey);
                                    $item.next().attr('data-itemkey', itemKey);
                                    $item.clone().insertAfter($item.next());
                                }
                                $item.detach();
                                $table.find('.aspro-admin-item').removeClass('first').removeClass('last');
                                $table.find('.aspro-admin-item:not(.new)').first().addClass('first');
                                $table.find('.aspro-admin-item:not(.new)').last().addClass('last');
                            });

                        });

                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('form.next_options').submit(function(e) {
                                $(this).attr('id', 'next_options');
                                jsAjaxUtil.ShowLocalWaitWindow('id', 'next_options', true);
                                $(this).find('input').removeAttr('disabled');
                            });
                        })
                    </script>
                    <style>
                        .wt-admin-table tr:hover {
                            /*background-color: #e0e8ea;*/
                        }
                    </style>
                </form>
                <?$tabControl->End();?>
            <?endif;?>
        <?

    }


    public function ShowAdminRow($optionCode, $arOption, $arTab, $arControllerOption)
    {
        $arParams = $arOption["PARAMS"];
        $optionName = $arOption['TITLE'];
        $optionType = $arOption['TYPE'];
        $optionList = $arOption['LIST'];
        if(!is_iterable($arOption['LIST']) && is_callable($arOption['LIST'])){
            $optionList = call_user_func_array($arOption['LIST'], []);
        }
        $optionDefault = $arOption['DEFAULT'];
        $optionVal = $arTab['OPTIONS'][$optionCode];
        $optionSize = $arOption['SIZE'];
        $optionCols = $arOption['COLS'];
        $optionRows = $arOption['ROWS'];
        $optionChecked = $optionVal == 'Y' ? 'checked' : '';
        $optionDisabled = isset($arControllerOption[$optionCode]) || array_key_exists('DISABLED', $arOption) && $arOption['DISABLED'] == 'Y' ? 'disabled' : '';
        $optionSup_text = array_key_exists('SUP', $arOption) ? $arOption['SUP'] : '';
        $optionController = isset($arControllerOption[$optionCode]) ? "title='".GetMessage("MAIN_ADMIN_SET_CONTROLLER_ALT")."'" : "";
        $optionsSiteID = $arTab['SITE_ID'];
        $isArrayItem = strpos($optionCode, '_array_') !== false;
        $hintTemplate = $this->getHintTemplate($arOption['HINT']);

        if(isset($arOption['ALIAS_TEMPLATE_MAP']) && is_array($arOption['ALIAS_TEMPLATE_MAP'])){
            if($arOption['ALIAS_TEMPLATE_MAP'][$optionType] && is_file($arOption['ALIAS_TEMPLATE_MAP'][$optionType])) {
                include $arOption['ALIAS_TEMPLATE_MAP'][$optionType];
                return;
            } else {
                throw new \Exception('File not found ' . $arOption['ALIAS_TEMPLATE_MAP'][$optionType]);
            }
        }

        include __DIR__ . '/templates/render/'.$optionType.'.php';
    }


    public function __ShowFilePropertyField($name, $arOption, $values){
        global $bCopy, $historyId;
        if(!is_array($values)){
            $values = array($values);
        }

        if($bCopy || empty($values)){
            $values = array('n0' => 0);
        }

        $optionWidth = $arOption['WIDTH'] ? $arOption['WIDTH'] : 100;
        $optionHeight = $arOption['HEIGHT'] ? $arOption['HEIGHT'] : 100;

        if($arOption['MULTIPLE'] == 'Y') {
            $inputName = array();
            foreach($values as $key => $val){
                if(is_array($val)){
                    $inputName[$name.'['.$val.']'] = $val['VALUE'];
                }
                else{
                    $inputName[$name.'['.$val.']'] = $val;
                }
            }
            if($historyId > 0){
                echo \CFIleInput::ShowMultiple($inputName, $name.'[n#IND#]',
                    array(
                        'IMAGE' => $arOption['IMAGE'],
                        'PATH' => 'Y',
                        'FILE_SIZE' => 'Y',
                        'DIMENSIONS' => 'Y',
                        'IMAGE_POPUP' => 'Y',
                        'MAX_SIZE' => array(
                            'W' => $optionWidth,
                            'H' => $optionHeight,
                        ),
                    ),
                    false);
            }
            else{
                echo \CFIleInput::ShowMultiple($inputName, $name.'[n#IND#]',
                    array(
                        'IMAGE' => $arOption['IMAGE'],
                        'PATH' => 'Y',
                        'FILE_SIZE' => 'Y',
                        'DIMENSIONS' => 'Y',
                        'IMAGE_POPUP' => 'Y',
                        'MAX_SIZE' => array(
                            'W' => $optionWidth,
                            'H' => $optionHeight,
                        ),
                    ),
                    false,
                    array(
                        'upload' => true,
                        'medialib' => true,
                        'file_dialog' => true,
                        'cloud' => true,
                        'del' => true,
                        'description' => $arOption['WITH_DESCRIPTION'] == 'Y',
                    )
                );
            }
        } else {
            foreach($values as $key => $val){
                if(is_array($val)){
                    $file_id = $val['VALUE'];
                }
                else{
                    $file_id = $val;
                }
                if($historyId > 0){
                    echo \CFIleInput::Show($name.'['.$key.']', $file_id,
                        array(
                            'IMAGE' => $arOption['IMAGE'],
                            'PATH' => 'Y',
                            'FILE_SIZE' => 'Y',
                            'DIMENSIONS' => 'Y',
                            'IMAGE_POPUP' => 'Y',
                            'MAX_SIZE' => array(
                                'W' => $optionWidth,
                                'H' => $optionHeight,
                            ),
                        )
                    );
                }
                else{
                    echo \CFileInput::Show($name.'['.$key.']', $file_id,
                        array(
                            'IMAGE' => $arOption['IMAGE'],
                            'PATH' => 'Y',
                            'FILE_SIZE' => 'Y',
                            'DIMENSIONS' => 'Y',
                            'IMAGE_POPUP' => 'Y',
                            'MAX_SIZE' => array(
                                'W' => $optionWidth,
                                'H' => $optionHeight,
                            ),
                        ),
                        array(
                            'upload' => true,
                            'medialib' => true,
                            'file_dialog' => true,
                            'cloud' => true,
                            'del' => true,
                            'description' => $arOption['WITH_DESCRIPTION'] == 'Y',
                        )
                    );
                }
                break;
            }
        }
    }

    public function getStyleByArray($list)
    {
        $result = (string)$list;

        if(is_array($list)) {
            $result = '';

            foreach ($list as $attr => $value) {
                $result .= ' ' . $attr . ': ' . ($value) . ';';
            }
        }

        return $result;
    }

    public function CheckColor($strColor){
        $strColor = substr(str_replace('#', '', $strColor), 0, 6);
        $strColor = base_convert(base_convert($strColor, 16, 2), 2, 16);
        for($i = 0, $l = 6 - (function_exists('mb_strlen') ? mb_strlen($strColor) : strlen($strColor)); $i < $l; ++$i)
            $strColor = '0'.$strColor;
        return $strColor;
    }

    public function getNote($message, $messageAttrs = '')
    {
        $alertAttrs = '';
        return BeginNote($alertAttrs, $messageAttrs) . $message . EndNote();
    }

    public function renderCustomClassChecker($className, $originClassName, $iClassName)
    {
        $bInstanceofOriginClass = is_a($className, $originClassName, true);
        $bInstanceOfInterface = is_a($className, $iClassName, true);
        $loadedCustomClass = $bInstanceofOriginClass && $bInstanceOfInterface;
        ?>
        <tr>
            <td><?="Загружен пользовательский класс <b>$className</b>"?></td>
            <td><?=$this->getCheckTemplate($loadedCustomClass)?></td>
        </tr>
        <tr>
            <td style="padding-left: 2em;"><?="class instanceof $originClassName"?></td>
            <td><?=$this->getCheckTemplate($bInstanceofOriginClass)?></td>
        </tr>
        <tr>
            <td style="padding-left: 2em;"><?="class instanceof $iClassName"?></td>
            <td><?=$this->getCheckTemplate($bInstanceOfInterface)?></td>
        </tr>
        <?php
    }

    public function getHintTemplate($hint)
    {
        $id = ToLower(RandString(8));
        $hint = str_replace("\"", "\\\"", $hint);
        return "<span id=\"hint_" . $id . "\"></span><script>BX.hint_replace(BX(\"hint_" . $id . "\"), \"" . $hint . "\");</script>";
    }

    public function getCheckTemplate($check = true, $resolve = '', $reject = '')
    {
        global $APPLICATION;

        $css = <<<HTML_CSS
<style>
.kit-item-check {
    margin-bottom: -0.1em;
    position: relative;
    display: inline-block;
    background-repeat: no-repeat;
    width: 1em;
    height: 1em;
    background-position: center;
    background-size: contain;
}
.kit-item-check--success {
    color: green;
    background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAMlJREFUeNrEk+sNwjAMhG3EXMzCEkzBEszCRDwEUp4mlyaRG6nQKj+w1KpKdPf5EpdFhEZqR4M1bLDH63Q9btHUzHw+XDZ30A4sSJDWwVoxDjyIJx8dueBotQFoIXpyWWinJ1ruDVq2gqs0AS0JyBaxDYbxrQ2ko3GhSaFlcRJiLYux1gz6bGlTFI2qAOLJ0BEiNYO3f/ECTYkdAwCQnt5s8DT30poRRSNF4xRseZAe5lZpjPYRQ9H45ySWDjRtdiPfiv/+N34EGAAegQcn+2sjVwAAAABJRU5ErkJggg==");
}
.kit-item-check--error {
    color: red;
    background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAALFJREFUeNrUU9sKgzAMTYb4uD/za/0xBdmotsYeNJh1Bgd9WiHaSs4laWQRoZr1oMpVTdB8nLoOT62Ji9zze987BGeS7pVE0Ks1RkrLIq0hb24cypqSAinOM6UcrVvCzmzV9sigqBECPz2CQ41hU9UMEG/G3m1imKYrEFkyOHIJ3uOIJCmBWkp2h/LYJXgNg5Rq6AVKc27n24FRI6MmPw1S7gFntatBYm/A+P9/pk2AAQDF2ci+DyvULQAAAABJRU5ErkJggg==");
}
</style>
HTML_CSS;

        \Bitrix\Main\Page\Asset::getInstance()->addString($css, true);
        $checkTitle = $check?'yes':'no';
        $checkClass = $check?'kit-item-check--success':'kit-item-check--error';
        $style = $check?'':'';
        $label = $check?$resolve:$reject;
        return "<span><span style=\"{$style}\" class=\"kit-item-check {$checkClass}\" title=\"{$checkTitle}\"></span>$label</span>";
    }

    /**
     * @param string|int $iBlockId
     * @param array $propertyRawData
     * @param string|callable $resolve
     * @param string|callable $reject
     * @param string|callable $description
     * @return void
     */
    public function renderIBlockPropertyChecker($iBlockId, $propertyRawData = [], $resolve = '', $reject = '', $description = '')
    {
        $iBlockAF = app()->factory()->iBlock()->getEntityById($iBlockId);
        $basePropertyEntity = $iBlockAF->getPropertyFactory()->cache([])->getEntityByResult($propertyRawData);
        $propertyEntity = $iBlockAF->getPropertyFactory()->getEntityByCode($propertyRawData['CODE']);
        $link = $propertyRawData['NAME'];
        if($propertyEntity){
            $link = "[<a href='{$propertyEntity->getAdminUrl()}' target='_blank'>{$propertyEntity->getId()}</a>] {$propertyEntity->getName()}";
        }
        $defaultResolve = '';
        if(!is_string($resolve) && is_callable($resolve)){
            $resolve = $defaultResolve . call_user_func_array($resolve, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        $defaultReject = '';
        if(!is_string($reject) && is_callable($reject)){
            $reject = $defaultReject . call_user_func_array($reject, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        if(!is_string($description) && is_callable($description)){
            $description = call_user_func_array($description, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        ?>
        <tr>
            <td>
                <div><?=$link?></div>
                <div style="font-size: 0.9em;color: #3e3e3e;"><?=$description?></div>
            </td>
            <td>
                <div style="font-size: 0.9em;"><?=$propertyRawData['CODE']?></div>
            </td>
            <td>
                <?php
                if($propertyEntity){
                    if($basePropertyEntity->getFullType()){
                        if($basePropertyEntity->getFullType() == $propertyEntity->getFullType()){
                            echo $propertyEntity->getFullType();
                        }
                        if($basePropertyEntity->getFullType() != $propertyEntity->getFullType()){
                            ?>
                            <span class="errortext"><?=$propertyEntity->getFullType()?></span>
                            to
                            <span><?=$basePropertyEntity->getFullType()?></span><?php
                        }
                    }
                } else {
                    echo $basePropertyEntity->getFullType();
                }
                ?>
            </td>
            <td>
                <?php echo $this->getCheckTemplate(
                    $propertyEntity,
                    $resolve,
                    $reject
                );
                ?>
            </td>
        </tr>
        <?php
    }

    public function renderIBlockSectionUserFieldChecker($iBlockId, $propertyRawData = [], $resolve = '', $reject = '', $description = '')
    {
        $iBlockAF = app()->factory()->iBlock()->getEntityById($iBlockId);
        $code = $propertyRawData['FIELD_NAME']?:$propertyRawData['CODE'];
        $propertyEntity = $iBlockAF->getSectionUserFieldFactory()->getEntityByCode($code);
        $name = $propertyRawData['NAME']?:$propertyRawData['EDIT_FORM_LABEL']?:$propertyRawData['LIST_COLUMN_LABEL'];
        if($propertyEntity){
            if($propertyEntity->getName() !== $propertyEntity->getCode()){
                $name = $propertyEntity->getName();
            } else {
                $name = "<i>$name</i>";
            }
            $name = "[<a href='{$propertyEntity->getAdminUrl()}' target='_blank'>{$propertyEntity->getId()}</a>] {$name}";
        }
        $defaultResolve = '';
        if(!is_string($resolve) && is_callable($resolve)){
            $resolve = $defaultResolve . call_user_func_array($resolve, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        $defaultReject = '';
        if(!is_string($reject) && is_callable($reject)){
            $reject = $defaultReject . call_user_func_array($reject, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        if(!is_string($description) && is_callable($description)){
            $description = call_user_func_array($description, [$iBlockId, $propertyRawData, $propertyEntity]);
        }
        ?>
        <tr>
            <td>
                <div><?=$name?></div>
                <div style="font-size: 0.9em;color: #3e3e3e;"><?=$description?></div>
            </td>
            <td>
                <div style="font-size: 0.9em;"><?=$code?></div>
            </td>
            <td>
                <?php echo $this->getCheckTemplate(
                    $propertyEntity,
                    $resolve,
                    $reject
                );
                ?>
            </td>
        </tr>
        <?php
    }

    public function renderTabSetter($tabName, $ajaxUrl)
    {
        ?>
        <tr>
            <td>Вкладка <b><?=$tabName?></b></td>
            <td>
                <input type="button" class="adm-btn-barcode" value="Установить в общий профиль"
                       style="background-image: -webkit-linear-gradient(bottom, #cfc, #fff) !important;"
                       onclick="
                   var input = this;
                   input.disabled = true;
                   $.ajax({
                       url: '<?=$ajaxUrl?>',type: 'post',data: {common:1},dataType : 'json',
                       success: function(data){
                           kit.ready('bsToastService', function(){
                               var title = 'Установка общего профиля';
                               if(data.status == 'success'){kit.service.toast.success(title, data.message);}
                               else {kit.service.toast.error(title, data.message);}
                           });
                       },
                       error: function (jqXHR, textStatus, errorThrown) {
                           kit.ready('bsToastService', function(){ kit.service.toast.error('Error', er + ' ' + value) });
                       },
                       complete: function (jqXHR, textStatus) {
                           input.disabled = false;
                       }
                   });">

                <input type="button" class="adm-btn-barcode" value="Установить в личный профиль"
                       onclick="
                       var input = this;
                       input.disabled = true;
                       $.ajax({
                           url: '<?=$ajaxUrl?>', type: 'post', data: {common:0}, dataType : 'json',
                           success: function(data){
                               kit.ready('bsToastService', function(){
                               var title = 'Установка личного профиля';
                               if(data.status == 'success'){kit.service.toast.success(title, data.message);}
                               else {kit.service.toast.error(title, data.message);}
                               });
                           },
                           error: function (jqXHR, textStatus, errorThrown) {
                               kit.ready('bsToastService', function(){ kit.service.toast.error('Error', er + ' ' + value) });
                           },
                           complete: function (jqXHR, textStatus) {
                               input.disabled = false;
                           }
                       });">
            </td>
        </tr>
        <?php
    }

    public function renderTabInfo($label, $optionName, $removeAjaxUrl)
    {
        ?>

        <tr>
            <?php
            $row = (array)\CUserOptions::GetList([], [
                'CATEGORY' => 'form',
                'NAME' => $optionName . '_disabled',
                'USER_ID' => app()->service()->user()->getId(),
            ])->Fetch();
            $disabledData = (array)unserialize((string)$row['VALUE']);
            $disabled = $disabledData['disabled'] === 'Y';
            $enable = !$disabled;
            ?>
            <td>Включено использование профилей для <b><?=$label?></b></td>
            <td><?php echo $this->getCheckTemplate($enable, 'Да', 'Нет');?></td>
        </tr>
        <tr>
            <td>
                <a target="_blank" style="color: grey"
                   href="/bitrix/admin/perfmon_table.php?lang=ru&table_name=b_user_option&f_CATEGORY=form&f_NAME=<?=$optionName?>&apply_filter=Y">
                    b_user_option
                </a>
            </td>
            <td></td>
        </tr>
        <tr>
            <?php
            $commonProfile = \CUserOptions::GetList([], [
                'CATEGORY' => 'form',
                'NAME' => $optionName,
                'COMMON' => 'Y',
            ])->Fetch();
            ?>
            <td>Общий профиль для <b><?=$label?></b></td>
            <td><?php
                if($commonProfile){
                    ?>Присутствует<?php
                } else {
                    ?><div class="errortext">Отсутствует</div><?php
                }
                ?></td>
        </tr>
        <tr>
            <?php
            $commonProfile = \CUserOptions::GetList([], [
                'CATEGORY' => 'form',
                'NAME' => $optionName,
                'USER_ID' => app()->service()->user()->getId(),
                'COMMON' => 'N',
            ])->Fetch();
            ?>
            <td>Личный профиль для <b><?=$label?></b></td>
            <td><?php
                if($commonProfile){
                    ?>
                    <input type="button" class="adm-btn-barcode" value="Удалить личный профиль!"
                           style="background-image: -webkit-linear-gradient(bottom, #ffdada, #fff) !important;"
                           onclick="
                   this.disabled = true;
                   var input = this;
                   BX.ajax.loadJSON('<?=$removeAjaxUrl?>',{common:0},
                   function(data) {input.disabled = false; kit.ready('bsToastService', function(){
                       var title = 'Удаление личного профиля';
                       if(data.status == 'success'){kit.service.toast.success(title, data.message);}
                       else {kit.service.toast.error(title, data.message);}
                   });},
                   function(er, value) {input.disabled = false; kit.ready('bsToastService', function(){ kit.service.toast.error('Error', er + ' ' + value) });});">

                    <?php
                } else {
                    ?>Отсутствует<?php
                }
                ?></td>
        </tr>

        <?php
    }

    public function renderUserGroupChecker($rawData = [])
    {
        $code = $rawData['CODE']?:$rawData['STRING_ID'];
        ?>
        <tr>
            <?php
            $userGroupEntity = app()->factory()->main()->getUserGroupFactory()->cache([])->getCollection()->getByCode($code);
            $link = $rawData['NAME'];
            if($userGroupEntity){
                $link = '<a target="_blank" href="'.$userGroupEntity->getAdminUrl().'">['.$userGroupEntity->getId().'] '.$userGroupEntity->getName().'</a>';
            }
            ?>
            <td>Группа <b><?=$link?></b> [<?=$code?>]</td>
            <td>
                <?php echo $this->getCheckTemplate(
                    $userGroupEntity,
                    '',
                    '<span class="errortext">Отсутствует (Нужно создать)</span> <a target="_blank" href="/bitrix/admin/group_edit.php">Create</a>'
                );
                ?>
            </td>
        </tr>
        <?php
    }

    public function getAdminMessageTemplate($message)
    {
        ob_start();
        ?>
        <div class="adm-info-message-wrap">
            <div class="adm-info-message">
                <?=$message?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public function getAdminErrorMessageTemplate($message)
    {
        $mess = new \CAdminMessage(['MESSAGE' => $message, 'TYPE' => 'ERROR']);
        return $mess->Show();
    }

    public function getAdminSuccessMessageTemplate($message)
    {
        $mess = new \CAdminMessage(['MESSAGE' => $message, 'TYPE' => 'OK']);
        return $mess->Show();
    }

    public function renderPhpExtensionsChecker($extensions)
    {
        $phpExtensions = [];
        foreach ($extensions as $extension){
            $phpExtensions[$extension] = extension_loaded($extension);
        }

        ?>
        <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
        <?
        foreach ($phpExtensions as $key => $value) {
            ?><tr><td><?= $key ?></td><td><?= $this->getCheckTemplate($value) ?></td></tr><?
        }
        ?></table><?
    }

    /**
     * @return SiteSettingsAdminPage
     */
    public function getAdminPage()
    {
        return $this->adminPage;
    }

    /**
     * @param AAgentCase $agentCase
     * @return void
     */
    public function renderAgentCase(AAgentCase $agentCase, array $params = [])
    {
        $moduleId = $this->getAdminPage()->getModuleId();
        include __DIR__ . '/templates/agentCase.php';
    }
}