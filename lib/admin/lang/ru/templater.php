<?
$MESS["NO_RIGHTS_FOR_VIEWING"] = "Недостаточно прав для просмотра";
$MESS["MAIN_OPTIONS"] = "Настройки сайта &laquo;#SITE_NAME#&raquo; (#SITE_ID#)";
$MESS["MAIN_OPTIONS_SITE_TITLE"] = "Настройки сайта &laquo;#SITE_NAME#&raquo; (#SITE_ID#)";
$MESS["MAIN_OPTIONS_SHORT"] = "&laquo;#SITE_NAME#&raquo; (#SITE_ID#)";
$MESS["MAIN_OPT_APPLY"] = "Применить";
$MESS["MAIN_OPT_APPLY_TITLE"] = "Применить настройки сайта";
$MESS["MAIN_OPT_CANCEL"] = "Отменить";
$MESS["MAIN_OPT_CANCEL_TITLE"] = "Отменить настройки сайта";
$MESS["MAIN_OPTIONS_TITLE"] = "Основные настройки";
$MESS["TEMPLATE_OPTIONS"] = "Шаблон";
