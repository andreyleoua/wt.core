<?php

namespace Wt\Core\Admin;

use Wt\Core\App\App;
use Wt\Core\Tools;
use Wt\Core\Type\Arr;

class GlobalMenuBuilder
{
    const MENU_FILE_NAME = 'menu.php';

    protected $apps;
    protected $rootId;
    /**
     * @var GlobalMenuItem
     */
    protected $globalMenuItem;

    /**
     * @param App[] $apps
     * @param string $rootId
     */
    public function __construct(array $apps, string $rootId)
    {
        $this->apps = $apps;
        $this->rootId = $rootId;
    }

    protected function buildRootGlobalMenu()
    {
        $this->globalMenuItem = new GlobalMenuItem();
        $this->globalMenuItem
            ->setId($this->getRootId())
            ->setTitle('Root menu')
            ->setSort(150);
    }

    /**
     * @param App $app
     * @return GlobalMenuItem[]
     */
    protected function getAppMenuList(App $app)
    {
        return $this->getMenuListFromDir($app->module()->getPath() .'/admin', $app->module()->getPrefix());
    }

    protected function getMenuListFromDir($path, $parentMenuCode = null)
    {
        $menuList = [];
        $dirList = glob($path .'/*');

        if(!is_array($dirList)){
            return $menuList;
        }

        foreach ($dirList as $path) {

            $menu = $this->getMenuFromDir($path, $parentMenuCode);

            if($menu && $menu->isVisible()) {
                $menuList[] = $menu;
            }
        }

        return Arr::sort($menuList, ['sort' => SORT_ASC]);
    }

    protected function getMenuFromDir($path, $parentMenuCode = null)
    {
        if(!is_dir($path)){
            return null;
        }

        $menuCode = implode('.', array_filter([$parentMenuCode, basename($path)]));
        $menuPath = $path . '/' . self::MENU_FILE_NAME;
        if(!file_exists($menuPath)){
            return null;
        }
        /**
         * @var GlobalMenuItem $menu
         */
        $menu = include $menuPath;
        if(is_array($menu)){
            $menu = new GlobalMenuItem($menu);
        }
        if(!is_a($menu, GlobalMenuItem::class)){
            return null;
        }
        if($menu->getUrl() === '') {
//            $menuSrc = BX_ROOT . '/admin/'. str_replace('.', '_', $menuCode) . '.php';
            $menuSrc = BX_ROOT . '/admin/'. $menuCode . '.php';
            $menu->setUrl($menuSrc);
        }

        if($menu->getUrl() && (filter_var($menu->getUrl(), FILTER_VALIDATE_URL) === false)) {
            $adminPageRel = $menu->getUrl();
            $adminPage = $_SERVER['DOCUMENT_ROOT'] . $adminPageRel;
            if (!file_exists($adminPage) || $_REQUEST['install_admin_pages'] === 'force')
            {
                $cont = '<' . '?php ' . "require(\$_SERVER['DOCUMENT_ROOT'] . '".Tools::removeDocRoot($path)."/index.php');";
                $fopen = fopen($adminPage, 'w');
                if($fopen){
                    fwrite($fopen, $cont);
                    fclose($fopen);
                }
//                file_put_contents($adminPage, $cont);
            }
        }
        if(!$menu->getId()){
            $menu->setId($menuCode);
        }
        $menu->addChilds($this->getMenuListFromDir($path . '/submenu', $menuCode));

        return $menu;
    }

    public function makeTreeMenu()
    {
        $this->buildRootGlobalMenu();
        foreach ($this->apps as $app){
            $menuList = $this->getAppMenuList($app);
            $this->globalMenuItem->addChilds($menuList);
        }
        return $this->globalMenuItem;
    }

    /**
     * @return GlobalMenuItem
     */
    public function getGlobalMenuItem()
    {
        return $this->globalMenuItem;
    }

    public function prepareGlobalMenu(&$arGlobalMenu)
    {
        $arGlobalMenu['global_menu_' . $this->getRootId()] = $this->getGlobalMenuItem()->toArray();
    }

    /**
     * @return string
     */
    public function getRootId(): string
    {
        return $this->rootId;
    }
}


//        $aMenu = array(
//            'menu_id' => WT_DEV_MODULE_CLASS,
//            'sort' => 150,
//            'text' => Loc::getMessage(WT_DEV_MODULE_CLASS.'_GLOBAL_MENU_TEXT'),
//            'icon' => 'clouds_menu_icon',
//            'page_icon' => 'clouds_page_icon',
//            'items_id' => 'global_menu_'.WT_DEV_MODULE_CLASS,
//            'items' => array()
//        );
//        return [
//            'parent_menu' => 'global_menu_'.WT_DEV_MODULE_CLASS,
//            'sort'        => 10,
//            'url'         => WT_DEV_MODULE_ID.'_services.php',
//            'text'        => 'Услуги',
//            'title'       => 'Услуги',
//            'icon'        => 'sys_menu_icon',
//            'page_icon'   => 'sys_menu_icon',
//            'items_id'    => 'menu_custom',
//        ];