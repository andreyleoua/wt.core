<?php


namespace Wt\Core\Admin;


use Bitrix\Main\Error;
use Bitrix\Main\ORM\Data\AddResult as ORMAddResult;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\ORM\Data\Result as ORMResult;
use Bitrix\Main\Entity\Result;
use Bitrix\Main\ORM\Data\UpdateResult as ORMUpdateResult;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\ORM\Fields\EnumField as ORMEnumField;
use Bitrix\Main\Entity\EnumField;
use Bitrix\Main\ORM\Fields\Field as ORMField;
use Bitrix\Main\Entity\Field;
use Bitrix\Main\ORM\Fields\ScalarField as ORMScalarField;
use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Type\Date;
use CAdminContextMenu;
use CAdminForm;
use CAdminMessage;
use Wt\Core\ORM\ADataManager;
use Wt\Core\Tools;

class TableRowAdminPage extends ATableAdminPage
{


    protected $tableListUrl;


    public function __construct(ADataManager $table, array $request = [], $tableListUrl = '')
    {
        $this->tableListUrl = $tableListUrl;
        $this->request = $request;
        $this->table = $table;
    }

    public function setPageMetaData()
    {
        global $APPLICATION;

        $savePrefix = '';
        if($this->getSaveMode()){
            $savePrefix = ' - ' . $this->getSaveMode();
        }

        switch ($this->getAction()){
            case static::ACTION_ADD:
                $APPLICATION->SetTitle('Add ' . $this->getPrimaryValue() . $savePrefix);
                break;
            case static::ACTION_COPY:
                $APPLICATION->SetTitle('Copy ' . $this->getPrimaryValue() . $savePrefix);
                break;
            case static::ACTION_DELETE:
                $APPLICATION->SetTitle('Delete ' . $this->getPrimaryValue() . $savePrefix);
                break;
            case static::ACTION_EDIT:
                $APPLICATION->SetTitle('Edit ' . $this->getPrimaryValue() . $savePrefix);
                break;
            default:
                $APPLICATION->SetTitle('Action not found!');
                break;
        }

    }

    protected function &getAdminFormTab(CAdminForm $adminForm, $tabId)
    {
        foreach ($adminForm->tabs as &$tab){
            if($tab['DIV'] == $tabId){
                return $tab;
            }
        }
        return null;
    }

    /**
     * @param array $data
     * @return CAdminForm
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getAdminForm(array $data)
    {
        /**
         *	Create tab control with form
         */
        $tabId = $this->getTable()::getTableName() . '_tab';
        $arTabs = array(array('DIV'=>$tabId, 'TAB'=>$this->getTable()::getRowTitle(), 'TITLE'=>$this->getTable()::getRowDescription(), 'ICON'=>''));
        $adminForm = new AdminForm($this->getTable()::getTableName() . '_form', $arTabs);
        $adminForm->BeginPrologContent();

        $adminForm->EndPrologContent();
        $adminForm->BeginEpilogContent();
        // ...
        $adminForm->EndEpilogContent();

        global $APPLICATION;
        $uri = new \Bitrix\Main\Web\Uri($page = $APPLICATION->GetCurPage());
        $primaryKeys = $this->table::getEntity()->getPrimaryArray();
        $uri->deleteParams(array_merge($primaryKeys, ['lang', 'action']));
        foreach ($primaryKeys as $primaryKey){
            $uri->addParams([$primaryKey => $data[$primaryKey]]);
        }
        $uri->addParams(['action' => $this->getAction()]);
        $uri->addParams(['lang' => LANG]);
        $link = $uri->getUri();

        $adminForm->Begin(array(
            'FORM_ACTION' => $link,
        ));
        $adminForm->BeginNextFormTab();


        //checks required fields
        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            /** @var $field Field|ScalarField|EnumField*/
            $value = $data[$field->getName()];
            if(!mb_strlen($value)){
                $value = $field->getDefaultValue();
            }

            if(!is_a($field, ScalarField::class)){
                continue;
            }

            if ($field->isPrimary()){
                if (!$this->isNew()) {
                    $adminForm->AddHtmlField($field->getName(), $field->getTitle(), $value, $value);
                    $this->admin_view($adminForm, $field, $data, $entity);
                }
                continue;
            }

            $inForm = !($field->hasParameter('in_form') && !$field->getParameter('in_form'));
            if(!$inForm){
                continue;
            }
            $showDetail = true;
            if($field->hasParameter('show_detail')){
                $showDetail = $field->getParameter('show_detail');
            }
            if(!$showDetail){
                continue;
            }

            $userEdit = !$field->hasParameter('user_edit')?true:$field->getParameter('user_edit');
            $isSystem = !$field->hasParameter('is_system')?false:$field->getParameter('is_system');

            if($isSystem){
                $userEdit = false;
            }

            if($isSystem && $this->isNew()){
                continue;
            }

            if(!$userEdit){ // поле не для редактирования
                if (!$this->isNew()) {
                    $adminForm->AddHtmlField($field->getName(), $field->getTitle(), $value, $value);
                    $this->admin_view($adminForm, $field, $data, $entity);
                    continue;
                }
            }

            $fieldAdminType = $this->getFieldType($field);

            $beforeTitle = '#$beforeTitle#';

            switch ($fieldAdminType){
                case 'checkbox':
                    $adminForm->AddCheckBoxField($field->getName(), $beforeTitle.$field->getTitle(), $field->isRequired(), ['Y', 'N'], $data[$field->getName()]==='Y');
                    break;
                case 'text':
                    $adminForm->AddTextField($field->getName(), $beforeTitle.$field->getTitle(), $field->modifyValueBeforeSave($value, $data), ['cols' => 50, 'rows' => 5,], $field->isRequired());
                    break;
                case 'datetime':
                    $adminForm->AddDateTimeField($field->getName(), $beforeTitle.$field->getTitle(), $value, $field->isRequired());
                    break;
                case 'calendar':
                    $adminForm->AddCalendarField($field->getName(), $beforeTitle.$field->getTitle(), $value, $field->isRequired());
                    break;
                case 'enum':
                case 'select':
                    $selectOptions = $this->getSelectOptions($field, $data);
                    $adminForm->AddDropDownField($field->getName(), $beforeTitle.$field->getTitle(), $field->isRequired(), $selectOptions, $value, []);
                    break;
                default: // string
                    $adminForm->AddEditField($field->getName(), $beforeTitle.$field->getTitle(), $field->isRequired(), array('size'=>50, 'maxlength'=>255), $value);
                    break;
            }

            $this->modifyRow($adminForm, $tabId, $field, $data);
        }


        /**
         *	End form
         */
        $adminForm->Buttons(array(
            'disabled' => false,
            'back_url' => $this->getTableListUri()->getUri(),
        ));
        //$adminForm->Show();
        //$adminForm->ShowWarnings($adminForm->GetName(), $message);


        return $adminForm;
    }

    /**
     * @param $adminForm AdminForm
     * @param $field Field|ScalarField|EnumField
     * @param $data array
     * @param $entity \Bitrix\Main\Entity\Base
     */
    protected function admin_view($adminForm, $field, $data, $entity)
    {
        if($field->hasParameter('admin_view') && is_callable($field->getParameter('admin_view'))){
            $val = (string)call_user_func_array($field->getParameter('admin_view'), [
                $field,
                $data[$field->getName()],
                $data,
                $entity,
                $template = null,
            ]);
            if($val){
                $adminForm->AddViewField($field->getName(), $field->getTitle(), $val);
            }
        }
    }

    /**
     * @param AdminForm $adminForm
     * @param string $tabId
     * @param Field|ScalarField $field
     * @param array $data
     */
    protected function modifyRow($adminForm, $tabId, $field, $data = [])
    {
        $tab = &$this->getAdminFormTab($adminForm, $tabId);
        $beforeTitle = '#$beforeTitle#';
        $bTabValid = $tab && isset($tab['FIELDS'][$field->getName()]) && is_array($tab['FIELDS'][$field->getName()]);
        if(!$bTabValid){
            return;
        }

        $content = &$tab['FIELDS'][$field->getName()]['html'];

        /** hint */

        $hintHtml = '';
        $hint = $field->getParameter('hint');
        $hint = value($hint, $data);
        if($hint) {
            $hintHtml = ShowJSHint($hint, ['return' => true]);
        }
        $content = str_replace($beforeTitle, $hintHtml, $content);

    }

    protected function getDefaultFormFields()
    {
        $fields = [];

        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            if ($field instanceof ScalarField)
            {
                if(!is_null($field->getDefaultValue())){
                    $fields[$field->getName()] = $field->getDefaultValue();
                }
            }
        }
        return $fields;
    }

    protected function getRequestFormFields()
    {
        $fields = [];
        //checks required fields
        $entity = $this->getTable()::getEntity();
        foreach ($entity->getFields() as $field)
        {
            if ($field instanceof ScalarField)
            {
                $dataType = $field->getDataType();

                if(isset($this->request[$field->getName()]) && $this->request[$field->getName()] !== ''){
                    $fields[$field->getName()] = $this->request[$field->getName()];


                    switch ($dataType){
                        case 'date':
                            try {
                                $fields[$field->getName()] = new Date($fields[$field->getName()]);
                            } catch (\Exception $exception){
                                $fields[$field->getName()] = '';
                            }
                            break;
                        case 'datetime':
                            $fields[$field->getName()] = DateTime::createFromUserTime($fields[$field->getName()]);
                            break;
                    }
                    foreach ($field->getFetchDataModifiers() as $modifier)
                    {
                        $fields[$field->getName()] = call_user_func_array($modifier, array($fields[$field->getName()], $this, null, $field->getName()));
                    }

                }

                elseif(isset($this->request[$field->getName()])) { // isset need !!!
                    $fields[$field->getName()] = $field->getDefaultValue();
                    if(is_null($fields[$field->getName()])){
                        $fields[$field->getName()] = '';
                        switch ($dataType){
                            case 'float':
                            case 'integer':
                                $fields[$field->getName()] = 0;
                                break;
                        }
                    }
                }
            }
        }
        return $fields;
    }

    protected function _start()
    {
        $this->setPageMetaData();

        switch ($this->getAction()){
            case static::ACTION_DELETE:
                $r = $this->table::delete($this->getPrimaryValue());
                if($r->isSuccess()){
                    LocalRedirect($this->getTableListUri()->getUri());
                } else {
                    $this->showMessage($r->getErrorMessages());
                    $this->getAdminContextMenu()->Show();
                    $this->getAdminForm($r->getData())->Show();
                }
                break;
            case static::ACTION_COPY:
                switch ($this->getSaveMode()){
                    case static::SAVE_APPLY:
                        $arFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::add($arFields);
                        if($r->isSuccess()){
                            $this->afterSaveApplyProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arFields)->Show();
                        }
                        break;
                    case static::SAVE_CLOSE:
                        $arFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::add($arFields);
                        if($r->isSuccess()){
                            $this->afterSaveCloseProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arFields)->Show();
                        }
                        break;
                    default:
                        $r = $this->getRowByPrimary();
                        if(!$r->isSuccess()) {
                            $this->showMessage($r->getErrorMessages());
                        } else {
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($r->getData())->Show();
                        }
                        break;
                }
                break;
            case static::ACTION_EDIT:
                switch ($this->getSaveMode()){
                    case static::SAVE_APPLY:
                        $arFields = $arRequestFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::update($this->getPrimaryValue(), $arFields);
                        if($r->isSuccess()){
                            $this->afterSaveApplyProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arRequestFields)->Show();
                        }
                        break;
                    case static::SAVE_CLOSE:
                        $arFields = $arRequestFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::update($this->getPrimaryValue(), $arFields);
                        if($r->isSuccess()){
                            $this->afterSaveCloseProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arRequestFields)->Show();
                        }
                        break;
                    default:
                        $r = $this->getRowByPrimary();
                        if(!$r->isSuccess()) {
                            $this->showMessage($r->getErrorMessages());
                        } else {
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($r->getData())->Show();
                        }
                        break;
                }
                break;
            case static::ACTION_ADD:
                switch ($this->getSaveMode()){
                    case static::SAVE_APPLY:
                        $arFields = $arRequestFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::add($arFields);
                        if($r->isSuccess()){
                            $this->afterSaveApplyProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arFields)->Show();
                        }
                        break;
                    case static::SAVE_CLOSE:
                        $arFields = $arRequestFields = $this->getRequestFormFields();
                        unset($arFields[$this->getPrimaryCode()]);
                        $r = $this->table::add($arFields);
                        if($r->isSuccess()){
                            $this->afterSaveCloseProcessing($r);
                        } else {
                            $this->showMessage($r->getErrorMessages());
                            $this->getAdminContextMenu()->Show();
                            $this->getAdminForm($arFields)->Show();
                        }
                        break;
                    default:
                        $arFields = $this->getDefaultFormFields();
                        $this->getAdminContextMenu()->Show();
                        $this->getAdminForm($arFields)->Show();
                        break;
                }
                break;
            default:

                break;
        }
    }

    public function start()
    {
        if(!$this->tests()){
            return $this;
        }
        try {
            call_user_func_array([$this, '_start'], func_get_args());
        } catch (\Bitrix\Main\DB\SqlException $exception) {
            pre('TABLE EXCEPTION: ' . $exception->getMessage());
            if($this->getTable()::isTableExists()){
                $this->getTable()::deleteOldColumns();
                $this->getTable()::updateFieldsTypes();
                $this->getTable()::addNewFields();
            } else {
                $this->getTable()::createTable();
                $this->getTable()::createIndexes();
            }
            call_user_func_array([$this, '_start'], func_get_args());
        }

        return $this;
    }

    /**
     * @param $r UpdateResult|AddResult
     */
    protected function afterSaveCloseProcessing($r)
    {
        $primaryKeys = [];
        if(is_callable([$r, 'getPrimary'])){
            $primaryKeys = array_keys((array)$r->getPrimary());
        }
        $uri = new \Bitrix\Main\Web\Uri($this->tableListUrl);
        $uri->deleteParams(array_merge($primaryKeys, ['lang', 'action']));
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        $link = $uri->getUri();
        LocalRedirect($link);
    }

    /**
     * @return \Bitrix\Main\Web\Uri
     */
    protected function getTableListUri()
    {
        $uri = new \Bitrix\Main\Web\Uri($this->tableListUrl);
        $uri->deleteParams(array_merge([], ['lang', 'action']));
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        return $uri;
    }

    /**
     * @param $r UpdateResult|AddResult
     */
    protected function afterSaveApplyProcessing($r)
    {
        global $APPLICATION;
        $uri = new \Bitrix\Main\Web\Uri($page = $APPLICATION->GetCurPage());
        $uri->deleteParams(['lang']);
        if(is_callable([$r, 'getPrimary'])) {
            $primaryKeys = array_keys($r->getPrimary());
            $uri->deleteParams($primaryKeys);
            $uri->addParams($r->getPrimary());
        } else {
            $primaryKeys = ['ID', 'id'];
            $uri->deleteParams($primaryKeys);
            $uri->addParams(['ID' => $r->getId()]);
        }
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        $link = $uri->getUri();
        LocalRedirect($link);
    }

    /**
     * @return Result
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function getRowByPrimary()
    {
        $r = new Result();
        if(!$this->getPrimaryValue()) {
            $r->addError(new Error('Primary Value is Empty'));
            return $r;
        }
        $resProfile = $this->table::getByPrimary($this->getPrimaryValue());
        $arFields = $resProfile->fetch();
        if($arFields === false) {
            $r->addError(new Error('Element not fount: '. $this->getPrimaryCode().' = '. $this->getPrimaryValue() . '.'));
            return $r;
        }
        $r->setData($arFields);
        return $r;
    }

    public function getAdminContextMenu()
    {
        $aMenu = array();
        // MenuItem: Profiles
        $aMenu[] = array(
            'TEXT'	=> 'Return',
            'LINK'	=> $this->getTableListUri()->getUri(),
            'ICON'	=> 'btn_list',
        );
        $aMenu[] = array(
            "TEXT" => 'help',
            "ONCLICK" => "console.log('help');",
        );
        if ($this->getAction() === static::ACTION_EDIT) {
            $r = $this->getRowByPrimary();
            if($r->isSuccess()){
                $arFields = $r->getData();
                $aMenu[] = array(
                    'TEXT'	=> 'toolbar actions',
                    'LINK'	=> '',
                    'MENU' => array(
                        array(
                            'TEXT'	=> 'New',
                            'LINK'	=> $this->getCurPageUri(['action' => ''])->getUri(),
                            'ICON'	=> 'btn_new',
                        ),
                        array(
                            'TEXT'	=> 'Copy',
                            'LINK'	=> $this->getCurPageUri([
                                'action' => 'copy',
                                $this->getPrimaryCode() => $this->getPrimaryValue(),
                            ])->getUri(),
                            'ICON'	=> 'btn_new',
                        ),
                        array(
                            'TEXT'	=> 'Delete',
                            'LINK'	=> 'javascript:if (confirm("'.('delete ' . $arFields['name']).'")) window.location=\''.$this->getCurPageUri([
                                    'action' => 'delete',
                                    $this->getPrimaryCode() => $this->getPrimaryValue(),
                                ])->getUri().'\';',
                            'ICON'	=> 'btn_delete',
                        ),
                    ),
                );
            }
        }
        $context = new CAdminContextMenu($aMenu);
        return $context;
    }
}