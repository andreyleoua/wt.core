<?php
/**
 * @var AAgentCase $agentCase
 * @var Wt\Core\Admin\Templater $this
 * @var array $params
 * @var string $moduleId
 */

use Wt\Core\Agent\AAgentCase;
$moduleId = $this->getAdminPage()->getModuleId();

$test = true;
$data = [];
foreach ($agentCase->getAgentList() as $agentEntity){
    $entity = $agentEntity->find();
    $agent = [
        'NAME' => $agentEntity->getName(),
        'IS_EXISTS' => $entity && $entity->isExists(),
        'IS_RUNNING' => $entity && $entity->isRunning(),
        'ACTIVE' => $entity && $entity->isActive(),
    ];
    $data[] = $agent;
    $test = $test && $agent['IS_EXISTS'];
}

?>
<table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
    <tr>
        <td><?=get_class($agentCase)?></td>
        <td><?=$this->getCheckTemplate($test)?></td>
        <td><?php
            if(count($data)) {
                if ($test) {
                    ?><input type="button" class="adm-btn-barcode" value="Удалить агенты"
                             onclick="this.disabled = true; BX.ajax.loadJSON('<?=$params['link_remove']?>', {}, function(res) {BX.reload();});"><?php
                } else {
                    ?><input type="button" class="adm-btn-barcode" value="Установить агенты"
                             onclick="this.disabled = true; BX.ajax.loadJSON('<?=$params['link_add']?>', {}, function(res) {BX.reload();});"><?php
                }
            }
            ?></td>
    </tr>
    <tr>
        <td colspan="4">
            <?php if(count($data)) { ?>
                <table cellpadding="2" border="0" style="border-collapse: collapse; width: 100%;">
                    <tr>
                        <td style="font-size: 0.8em; color: #777;">Name</td>
                        <td style="font-size: 0.8em; color: #777;">exists</td>
                        <td style="font-size: 0.8em; color: #777;">active</td>
                        <td style="font-size: 0.8em; color: #777;">running</td>
                    </tr>
                    <?php
                    foreach ($data as $agent){
                        ?>
                        <tr>
                            <td style="font-size: 0.8em; color: grey;"><?=$agent['NAME']?></td>
                            <td style="font-size: 0.8em; color: grey;"><?=$this->getCheckTemplate($agent['IS_EXISTS'])?></td>
                            <td style="font-size: 0.8em; color: grey;"><?=$this->getCheckTemplate($agent['ACTIVE'])?></td>
                            <td style="font-size: 0.8em; color: grey;"><?=$this->getCheckTemplate($agent['IS_RUNNING'])?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <?php
            } else {
                ?><i>Agent list is empty</i><?php
            }
            ?>
        </td>
    </tr>
</table>
<br>
<a href="/bitrix/admin/agent_list.php?PAGEN_1=1&SIZEN_1=20&lang=ru&set_filter=Y&adm_filter_applied=0&find=<?=$moduleId?>&find_type=module_id" target="_blank">Таблица агентов</a>
