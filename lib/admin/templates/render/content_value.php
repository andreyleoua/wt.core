<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <?
    if(!is_array($arOption['INCLUDEFILE'])){
        $arOption['INCLUDEFILE'] = array($arOption['INCLUDEFILE']);
    }
    foreach($arOption['INCLUDEFILE'] as $includefile){
        $includefile = str_replace('//', '/', str_replace('#SITE_DIR#', $arTab['SITE_DIR'].'/', $includefile));
        $includefile = str_replace('//', '/', str_replace('#TEMPLATE_DIR#', $arTab['TEMPLATE']['DIR'].'/', $includefile));
        $includefile = str_replace('//', '/', str_replace('#LANGUAGE_ID#', LANGUAGE_ID.'/', $includefile));
        if(strpos($includefile, '#') === false){
            ?><div style="">
            <?
            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $includefile)) {
                global $APPLICATION;
                $APPLICATION->SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $includefile, '');
            } else {
                include $_SERVER['DOCUMENT_ROOT'] . $includefile;
            }?>
            </div>
            <?if($arOption['SHOW_EDIT_BUTTON'] == 'Y') { ?>
                <div style="text-align: center;margin-bottom: 20px;">
                    <?
                    $template = ((isset($arOption['TEMPLATE']) && strlen($arOption['TEMPLATE'])) ? $arOption['TEMPLATE'] : 'include_area.php');
                    $href = (!strlen($includefile) ? "javascript:;" : "javascript: new BX.CAdminDialog({'content_url':'/bitrix/admin/public_file_edit.php?site=" . $arTab['SITE_ID'] . "&bxpublic=Y&from=includefile&noeditor=N&templateID=" . $arTab['TEMPLATE']['ID'] . "&path=" . $includefile . "&lang=" . LANGUAGE_ID . "&template=" . $template . "&subdialog=Y&siteTemplateId=" . $arTab['TEMPLATE']['ID'] . "','width':'1009','height':'503'}).Show();");
                    ?><a class="adm-btn"
                         href="<?= $href ?>"
                         name="<?= htmlspecialcharsbx($optionCode) . "_" . $optionsSiteID ?>"
                         title="<?= GetMessage('OPTIONS_EDIT_BUTTON_TITLE') ?>"><?= GetMessage('OPTIONS_EDIT_BUTTON_TITLE') ?></a>
                </div>
                <?
            }
        }
    }
    ?>
</td>