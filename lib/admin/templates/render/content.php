<?php
/**
 * @var $hintTemplate
 * @var $optionName
 * @var $optionSup_text
 * @var $arOption
 * @var $arTab
 * @var $optionCode
 * @var $optionsSiteID
 */
?>
<td width="100%" colspan="2">
    <div style="text-align: center; padding-bottom: 10px">
        <span>
            <?=$hintTemplate?>
            <?=$optionName?>
            <?php if(strlen($optionSup_text)):?>
                <span class="required"><sup><?=$optionSup_text?></sup></span>
            <?php endif;?>
        </span>
    </div>
    <?php
    if(!is_array($arOption['INCLUDEFILE'])){
        $arOption['INCLUDEFILE'] = [$arOption['INCLUDEFILE']];
    }
    foreach($arOption['INCLUDEFILE'] as $includefile){
        $includefile = str_replace('//', '/', str_replace('#SITE_DIR#', $arTab['SITE_DIR'].'/', $includefile));
        $includefile = str_replace('//', '/', str_replace('#TEMPLATE_DIR#', $arTab['TEMPLATE']['DIR'].'/', $includefile));
        $includefile = str_replace('//', '/', str_replace('#LANGUAGE_ID#', LANGUAGE_ID.'/', $includefile));
        if(!str_contains($includefile, '#')){
            ?><div style="padding: 20px;border: 1px solid #e0e8ea; margin-bottom: 10px; background-color: #f9fbfb;">
            <?php
            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $includefile)) {
                global $APPLICATION;
                $APPLICATION->SaveFileContent($_SERVER['DOCUMENT_ROOT'] . $includefile, '');
            } else {
                include $_SERVER['DOCUMENT_ROOT'] . $includefile;
            }?>
            </div>
            <?php if($arOption['SHOW_EDIT_BUTTON'] == 'Y') { ?>
                <div style="text-align: center;margin-bottom: 20px;">
                    <?php
                    $template = ((isset($arOption['TEMPLATE']) && strlen($arOption['TEMPLATE'])) ? $arOption['TEMPLATE'] : 'include_area.php');
                    $href = (!strlen($includefile) ? 'javascript:;' : "javascript: new BX.CAdminDialog({'content_url':'/bitrix/admin/public_file_edit.php?site=" . $arTab['SITE_ID'] . "&bxpublic=Y&from=includefile&noeditor=N&templateID=" . $arTab['TEMPLATE']['ID'] . "&path=" . $includefile . "&lang=" . LANGUAGE_ID . "&template=" . $template . "&subdialog=Y&siteTemplateId=" . $arTab['TEMPLATE']['ID'] . "','width':'1009','height':'503'}).Show();");
                    ?><a class="adm-btn"
                         href="<?= $href ?>"
                         name="<?= htmlspecialcharsbx($optionCode) . "_" . $optionsSiteID ?>"
                         title="<?= GetMessage('OPTIONS_EDIT_BUTTON_TITLE') ?>"><?= GetMessage('OPTIONS_EDIT_BUTTON_TITLE') ?></a>
                </div>
                <?php
            }
        }
    }
    ?>
</td>