<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <?
    if(isset($arOption['TYPE_SELECT']))
    {
        if($arOption['TYPE_SELECT'] == 'STORES')
        {
            static $bStores;
            if ($bStores === null){
                $bStores = false;
                if(\Bitrix\Main\Loader::includeModule('catalog')){
                    if(class_exists('CCatalogStore')){
                        $dbRes = CCatalogStore::GetList(array(), array(), false, false, array());
                        if($c = $dbRes->SelectedRowsCount()){
                            $bStores = true;
                        }
                    }
                }
            }
            if(!$bStores)
                unset($optionList['STORES']);
        }
        elseif($arOption['TYPE_SELECT'] == 'IBLOCK')
        {
            $bIBlocks = false;
            \Bitrix\Main\Loader::includeModule('iblock');
            $rsIBlock=CIBlock::GetList(array("SORT" => "ASC", "ID" => "DESC"), array("LID" => $optionsSiteID));
            $arIBlocks=array();
            while($arIBlock=$rsIBlock->Fetch()){
                $arIBlocks[$arIBlock["ID"]]["NAME"]="(".$arIBlock["ID"].") ".$arIBlock["NAME"]."[".$arIBlock["CODE"]."]";
                $arIBlocks[$arIBlock["ID"]]["CODE"]=$arIBlock["CODE"];
            }
            if($arIBlocks)
            {
                $bIBlocks = true;
            }
        }
        elseif($arOption['TYPE_SELECT'] == 'GROUP')
        {
            static $arUserGroups;
            if($arUserGroups === null){
                $DefaultGroupID = 0;
                $rsGroups = CGroup::GetList($by = "id", $order = "asc", array("ACTIVE" => "Y"));
                while($arItem = $rsGroups->Fetch()){
                    $arUserGroups[$arItem["ID"]] = $arItem["NAME"];
                    if($arItem["ANONYMOUS"] == "Y"){
                        $DefaultGroupID = $arItem["ID"];
                    }
                }
            }
            $optionList = $arUserGroups;
        }
    }
    if(!is_array($optionList)) $optionList = (array)$optionList;
    $arr_keys = array_keys($optionList);
    ?>
    <select name="<?=htmlspecialcharsbx($optionCode)."_".$optionsSiteID?>" <?=$optionController?> <?=$optionDisabled?>>
        <?if($bIBlocks)
        {
            foreach($arIBlocks as $key => $arValue) {
                $selected="";
                if(!$optionVal){
                    $selected="selected";
                }elseif($optionVal && $optionVal==$key){
                    $selected="selected";
                }?>
                <option value="<?=$key;?>" <?=$selected;?>><?=htmlspecialcharsbx($arValue["NAME"]);?></option>
            <?}
        }
        elseif($optionCode == 'GRUPPER_PROPS')
        {
            foreach($optionList as $key => $arValue):
                $selected="";
                if($optionVal && $optionVal==$key)
                    $selected="selected";
                ?>
                <option value="<?=$key;?>" <?=$selected;?> <?=(isset($arValue['DISABLED']) ? 'disabled' : '');?>><?=htmlspecialcharsbx($arValue["TITLE"]);?></option>
            <?endforeach;?>
        <?}
        else
        {
            for($j = 0, $c = count($arr_keys); $j < $c; ++$j):?>
                <option value="<?=$arr_keys[$j]?>" <?if($optionVal == $arr_keys[$j]) echo "selected"?>><?=htmlspecialcharsbx((is_array($optionList[$arr_keys[$j]]) ? $optionList[$arr_keys[$j]]["TITLE"] : $optionList[$arr_keys[$j]]))?></option>
            <?endfor;
        }?>
    </select>
</td>