<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <input type="<?=$optionType?>" style="box-sizing: border-box;width:<?=((isset($arOption['PARAMS']) && isset($arOption['PARAMS']['WIDTH'])) ? ''.$arOption['PARAMS']['WIDTH'].'' : '100%');?>;" <?=$optionController?> size="<?=$optionSize?>" maxlength="255" value="<?=htmlspecialcharsbx($optionVal)?>" name="<?=htmlspecialcharsbx($optionCode)."_".$optionsSiteID?>" <?=$optionDisabled?> <?=($optionCode == "password" ? "autocomplete='off'" : "")?>>
</td>