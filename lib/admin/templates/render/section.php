<?

global $APPLICATION;
?>

<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <?$APPLICATION->IncludeComponent(
        'grain:links.edit',
        '',
        Array(
            "ACTIVE" => "Y",
            "AJAX_SEND_POST" => "N",
            "CACHE_TIME" => "0",
            "CACHE_TYPE" => "A",
            "CHECK_PERMISSIONS" => "Y",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO",
            "DATA_SOURCE" => "iblock_section",
            "EMPTY_SHOW_ALL" => "Y",
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "INCLUDE_SUBSECTIONS" => "Y",
            "INPUT_NAME" => htmlspecialcharsbx($optionCode)."_".$optionsSiteID,
            "MULTIPLE" => "N",
            "NAME_TRUNCATE_LEN" => "",
            "ON_AFTER_REMOVE" => "",
            "ON_AFTER_SELECT" => "",
            "PARENT_SECTION" => "0",
            "SCRIPTS_ONLY" => "N",
            "SECTION_URL" => "",
            "SHOW_URL" => "N",
            "SORT_BY1" => "SORT",
            "SORT_BY2" => "ID",
            "SORT_ORDER1" => "ASC",
            "SORT_ORDER2" => "ASC",
            "USE_AJAX" => "N",
            "USE_SEARCH" => "Y",
            "USE_SEARCH_COUNT" => "",
            "VALUE" => htmlspecialcharsbx($optionVal)
        )
    );
    ?>
</td>

