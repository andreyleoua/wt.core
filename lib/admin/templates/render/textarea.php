<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <textarea <?=$optionController?> <?=$optionDisabled?> rows="<?=$optionRows?>" cols="<?=$optionCols?>" name="<?=htmlspecialcharsbx($optionCode)."_".$optionsSiteID?>"><?=htmlspecialcharsbx($optionVal)?></textarea>
</td>