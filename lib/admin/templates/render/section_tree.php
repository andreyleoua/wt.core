<?php
/**
 * @var $hintTemplate
 * @var $optionName
 * @var $optionSup_text
 * @var $optionCode
 * @var $optionsSiteID
 * @var $optionVal
 * @var $arParams
 */
global $APPLICATION;
if(!is_array($optionVal)){
    if(is_string($optionVal)){
        $optionVal = (array)unserialize($optionVal);
    } else {
        $optionVal = [];
    }
}

$optionVal = array_values(array_unique($optionVal));

$fieldName = $optionCode . '_' . $optionsSiteID;
?>

<td width="40%" style="vertical-align: baseline;">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?php if(strlen($optionSup_text)){?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?php }?>
</td>
<td width="60%">
    <script type="text/javascript">
        var TreeSelected = [];
        <?php
            $intCountSelected = 0;
            if (!empty($optionVal))
            {
                foreach ($optionVal as $oneKey)
                {
                    ?>TreeSelected[<?php echo $intCountSelected ?>] = <?php echo intval($oneKey); ?>;
                    <?php
                    $intCountSelected++;
                }
            }
        ?>
        function ClearSelected()
        {
            BX.showWait();
            TreeSelected = [];
        }
    </script>
    <?php
    if ($intCountSelected)
    {
        foreach ($optionVal as $oneKey)
        {
            $oneKey = (int)$oneKey;
//            ?><!--<input type="hidden" value="--><?php // echo $oneKey; ?><!--" name="--><?php //=$fieldName?><!--[]" id="oldV--><?php // echo $oneKey; ?><!--">--><?php //
        }
        unset($oneKey);
    }
    ?><div id="tree"></div>
    <script type="text/javascript">
        BX.showWait();
        clevel = 0;

        function delOldV(obj)
        {

        }

        function buildNoMenu()
        {
            var buffer;
            buffer = '<?php echo GetMessageJS('CET_FIRST_SELECT_IBLOCK');?>';
            BX('tree', true).innerHTML = buffer;
            BX.closeWait();
        }

        function buildMenu()
        {
            initChildItems(0, true);
            BX.closeWait();
        }
        var openDepth = 2;

        function initChildItems(sectionId, recursive = false, depth = 0)
        {
            depth++;
            if (BX('table_' + sectionId))
            {
                return;
            }

            var i;
            var buffer;
            var imgSpace;
            var _key;
            var sect_obj;
            var newTree = [];

            buffer = '<table border="0" id="table_' + sectionId + '" cellspacing="0" cellpadding="0" class="'+(sectionId?'hide':'')+'">';

            for (i in Tree[sectionId]){
                newTree.push({'section_name': Tree[sectionId][i][0], 'section_id': i, 'active': Tree[sectionId][i][2]['ACTIVE']});
            }

            // newTree.sort(function (a, b) {
            //     if (a.section_name > b.section_name) {
            //         return 1;
            //     }
            //     if (a.section_name < b.section_name) {
            //         return -1;
            //     }
            //     return 0;
            // });

            for (_key in newTree){

                sect_obj = newTree[_key];

                activeClass = (sect_obj.active == 'Y')?'':' section-tree-menu--no-active';

                if (!Tree[sect_obj.section_id])
                {
                    space = '<input type="checkbox" name="<?=$fieldName?>[]" value="'+sect_obj.section_id+'" id="<?=$fieldName?>'+sect_obj.section_id+'"'+(BX.util.in_array(sect_obj.section_id,TreeSelected) ? ' checked' : '')+' onclick="delOldV(this);">';
                    space += '<label for="<?=$fieldName?>'+sect_obj.section_id+'"><font class="text '+activeClass+'">' + sect_obj.section_name + '</font></label>';
                    imgSpace = '';
                }
                else
                {
                    space = '<input type="checkbox" name="<?=$fieldName?>[]" value="'+sect_obj.section_id+'"'+(BX.util.in_array(sect_obj.section_id,TreeSelected) ? ' checked' : '')+' onclick="delOldV(this);">';
                    space += '<a href="javascript: collapse(' + sect_obj.section_id + ')"><font class="text '+activeClass+'"><span>' + sect_obj.section_name + '</span></font></a>';
                    imgSpace = '<img src="/bitrix/images/catalog/load/plus.gif" width="13" height="13" style="margin-top: 3px;" id="img_' + sect_obj.section_id + '" OnClick="collapse(' + sect_obj.section_id + ')">';
                }

                buffer += '<tr>';
                buffer += '<td width="20" align="center" valign="top">' + imgSpace + '</td>';
                buffer += '<td id="node_' + sect_obj.section_id + '">' + space + '</td>';
                buffer += '</tr>';
            }

            buffer += '</table>';

            if(sectionId === 0) {
                BX('tree', true).innerHTML = buffer;
            } else {
                BX('node_' + sectionId).innerHTML += buffer;
            }

            if(recursive){

                if(depth <= openDepth){
                    open(sectionId);
                }

                for (_key in newTree) {
                    sect_obj = newTree[_key];
                    initChildItems(sect_obj.section_id, recursive, depth);
                }
            }

        }

        function open(sectionId) {
            if(BX('table_' + sectionId)){
                BX('table_' + sectionId).classList.remove('hide');
            }
            if(BX('img_' + sectionId)){
                BX('img_' + sectionId).src = '/bitrix/images/catalog/load/minus.gif';
            }
        }

        function close(sectionId) {
            if(BX('table_' + sectionId)){
                BX('table_' + sectionId).classList.add('hide');
            }
            if(BX('img_' + sectionId)){
                BX('img_' + sectionId).src = '/bitrix/images/catalog/load/plus.gif';
            }
        }

        function collapse(sectionId)
        {
            if (!BX('table_' + sectionId))
            {
                initChildItems(sectionId);
                open(sectionId);
            }
            else
            {
                var tbl = BX('table_' + sectionId);
                if(tbl.classList.toggle('hide')){
                    BX('img_' + sectionId).src = '/bitrix/images/catalog/load/plus.gif';
                } else {
                    BX('img_' + sectionId).src = '/bitrix/images/catalog/load/minus.gif';
                }
                // tbl.parentNode.removeChild(tbl);
            }
        }
    </script>
    <?php if(1){?>
    <iframe src="<?=app()->module()->getSrc()?>/lib/admin/templates/render/section_tree_frame.php?IBLOCK_ID=<?=intval($arParams['IBLOCK_ID'])?>&<?php echo bitrix_sessid_get(); ?>" id="id_ifr" name="ifr" style="display:none"></iframe>
    <?php }?>
    <style>
        .section-tree-menu--no-active {
            color: #bdc6e0;
        }
        #tree a {
            text-decoration: none;
        }
        #tree table.hide {
            display: none;
        }
    </style>
</td>

