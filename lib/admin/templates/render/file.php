<?
use \Bitrix\Main\Config\Option;

/**
 * @var $this \Wt\Dev\AdminPage\Templater
 */

?>
<td width="40%">
    <?=$hintTemplate?>
    <?=$optionName?>
    <?if(strlen($optionSup_text)):?>
        <span class="required"><sup><?=$optionSup_text?></sup></span>
    <?endif;?>
</td>
<td width="60%">
    <?$val = unserialize(Option::get($this->adminPage->getModuleId(), $optionCode, serialize(array()), $optionsSiteID));

    $this->__ShowFilePropertyField($optionCode."_".$optionsSiteID, $arOption, $val);?>
</td>