<?php


namespace Wt\Core\Admin;


use \Bitrix\Main\Type\Collection,
	\Bitrix\Main\Loader,
	\Bitrix\Main\IO\File,
	\Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Config\Option;
use Wt\Core\App\App;
use Wt\Core\Tools;

Loc::loadMessages(__FILE__);


class SiteSettingsAdminPage extends AAdminPage
{

    const CONFIG_FILE_NAME = 'config.php';

    protected $config;
    protected $pageCode;

    /**
     * @param $moduleId
     * @param $pageCode
     */
    public function __construct($moduleId, $pageCode)
    {
        $this->pageCode = $pageCode;
        $this->setModuleId($moduleId);
        Loader::includeModule($moduleId);
    }

    public function getConfig()
    {
        if(is_null($this->config)){
            // \Bitrix\Main\IO\Path::normalize()
            // _normalizePath
            /**
             * @var App
             */
            $app = app()::getInstance($this->getModuleId());

            $pathRaw = $app->module()->dir.'/admin/'.str_replace('.', '/submenu/', $this->pageCode).'/'.self::CONFIG_FILE_NAME;
            $path = (realpath($pathRaw));
            if(is_readable($path)) {
                $this->config = include($path);
            } else {
                pre('CONFIG NOT FOUND: ' . $pathRaw);
            }
        }
        return $this->config;
    }

    public function render()
    {
        $templater = new Templater($this);
        $templater->render();
    }

    /**
     * @param string $SITE_ID
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public function getSettings($SITE_ID = 's1')
    {
        static $options = [];

        if(!isset($options[$this->moduleId][$this->pageCode][$SITE_ID]))
        {
            $arDefaultValues = $options[$this->moduleId][$this->pageCode][$SITE_ID] = $arNestedValues = array();
            $arValues = &$options[$this->moduleId][$this->pageCode][$SITE_ID];
            $bNestedParams = false;


            $config = $this->getConfig();
            if($config && is_array($config))
            {

                foreach($config as $blockCode => $arBlock)
                {
                    if(!($arBlock['OPTIONS'] && is_array($arBlock['OPTIONS'])))
                    {
                        continue;
                    }

                    foreach($arBlock['OPTIONS'] as $optionCode => $arOption)
                    {
                        if($arOption['TYPE'] == 'note' || $arOption['TYPE'] == 'includefile')
                        {
                            continue;
                        }
                        if($arOption['TYPE'] === 'array'){
                            $itemsKeysCount = Option::get($this->moduleId, $optionCode, '0', $SITE_ID);
                            if($arOption['OPTIONS'] && is_array($arOption['OPTIONS']))
                            {
                                if($arOption['MULTIPLE'] === 'Y'){
                                for($itemKey = 0, $cnt = $itemsKeysCount + 1; $itemKey < $cnt; ++$itemKey)
                                {
                                    $_arParameters = array();
                                    $arOptionsKeys = array_keys($arOption['OPTIONS']);
                                    foreach($arOptionsKeys as $_optionKey)
                                    {
                                        $arrayOptionItemCode = $optionCode.'_array_'.$_optionKey.'_'.$itemKey;
                                            $arValues[$arrayOptionItemCode] = Option::get($this->moduleId, $arrayOptionItemCode, $arOption['OPTIONS'][$_optionKey]['DEFAULT'], $SITE_ID);
                                            //$arDefaultValues[$arrayOptionItemCode] = $arOption['OPTIONS'][$_optionKey]['DEFAULT'];
                                        }
                                    }
                                } else {
                                    $_arParameters = array();
                                    $arOptionsKeys = array_keys($arOption['OPTIONS']);
                                    foreach($arOptionsKeys as $_optionKey)
                                    {
                                        $arrayOptionItemCode = $optionCode.'_array_'.$_optionKey;
                                        $arValues[$arrayOptionItemCode] = Option::get($this->moduleId, $arrayOptionItemCode, $arOption['OPTIONS'][$_optionKey]['DEFAULT'], $SITE_ID);
                                        //$arDefaultValues[$arrayOptionItemCode] = $arOption['OPTIONS'][$_optionKey]['DEFAULT'];
                                }
                            }

                            }
                            $arValues[$optionCode] = $itemsKeysCount;
                            $arDefaultValues[$optionCode] = 0;
                        }
                        if($arOption['TYPE'] === 'file'){
                            $arDefaultValues[$optionCode] = $arOption['DEFAULT'];
                            $arValues[$optionCode] = unserialize(Option::get($this->moduleId, $optionCode, $arOption['DEFAULT'], $SITE_ID));


                        }
                        else
                        if($arOption['TYPE'] === 'multiselectbox'){
                            $arDefaultValues[$optionCode] = $arOption['DEFAULT'];
                            $arValues[$optionCode] = explode(',', Option::get($this->moduleId, $optionCode, $arOption['DEFAULT'], $SITE_ID));
                            if(!is_array($arValues[$optionCode])) $arValues[$optionCode] = (array)$arValues[$optionCode];
                        }
                        else
                        {
                            $arDefaultValues[$optionCode] = $arOption['DEFAULT'];
                            $arValues[$optionCode] = Option::get($this->moduleId, $optionCode, $arOption['DEFAULT'], $SITE_ID);

                            if(isset($arOption['DEPENDENT_PARAMS']) && $arOption['DEPENDENT_PARAMS']) //get dependent params default value
                            {
                                foreach($arOption['DEPENDENT_PARAMS'] as $key => $arSubOption)
                                {
                                    $arDefaultValues[$key] = $arSubOption['DEFAULT'];
                                    $arValues[$key] = Option::get($this->moduleId, $key, $arSubOption['DEFAULT'], $SITE_ID);
                                }
                            }
                        }

                    }

                }
            }

//            if($arValues && is_array($arValues))
//            {
//                foreach($arValues as $optionCode => $arOption)
//                {
//                    if(!isset($arDefaultValues[$optionCode]))
//                        unset($arValues[$optionCode]);
//                }
//            }
            if($arDefaultValues && is_array($arDefaultValues))
            {
                foreach($arDefaultValues as $optionCode => $arOption)
                {
                    if(!isset($arValues[$optionCode]) || is_null($arValues[$optionCode]))
                        $arValues[$optionCode] = $arOption;
                }
            }

        }

        return $options[$this->moduleId][$this->pageCode][$SITE_ID];
    }

    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * @return string
     */
    public function getPageCode()
    {
        return $this->pageCode;
    }

}