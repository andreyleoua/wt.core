<?php


namespace Wt\Core\Admin;



use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Data\AddResult as ORMAddResult;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\ORM\Data\Result as ORMResult;
use Bitrix\Main\Entity\Result;
use Bitrix\Main\ORM\Data\UpdateResult as ORMUpdateResult;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\ORM\Fields\EnumField as ORMEnumField;
use Bitrix\Main\Entity\EnumField;
use Bitrix\Main\ORM\Fields\Field as ORMField;
use Bitrix\Main\Entity\Field;
use Bitrix\Main\ORM\Fields\ScalarField as ORMScalarField;
use Bitrix\Main\Entity\ScalarField;
use Bitrix\Main\Type\Date;
use CAdminContextMenu;
use CAdminFilter;
use CAdminForm;
use CAdminList;
use CAdminListRow;
use CAdminMessage;
use CAdminResult;
use CAdminSorting;
use Wt\Core\ORM\ADataManager;
use Wt\Core\Tools;

class ATableAdminPage extends AAdminPage
{

    const ACTION_DELETE = 'delete';
    const ACTION_ACTIVATE = 'activate';
    const ACTION_DEACTIVATE = 'deactivate';
    const ACTION_COPY = 'copy';
    const ACTION_EDIT = 'edit';
    const ACTION_ADD = 'add';

    const SAVE_CLOSE = 'save';
    const SAVE_APPLY = 'apply';


    protected $table;
    protected $request;

    /**
     * @return ADataManager
     */
    public function getTable()
    {
        return $this->table;
    }

    public function isNew()
    {
        $action = $this->getAction();

        return ($action === static::ACTION_COPY) || ($action === static::ACTION_ADD);
    }

    public function isEdit()
    {
        $action = $this->getAction();

        return ($action === static::ACTION_EDIT);
    }

    public function getAction()
    {
        if($this->request['action']==static::ACTION_DELETE || $this->request['action_button']==static::ACTION_DELETE){
            return static::ACTION_DELETE;
        } elseif($this->request['action']==static::ACTION_ACTIVATE || $this->request['action_button']==static::ACTION_ACTIVATE){
            return static::ACTION_ACTIVATE;
        } elseif($this->request['action']==static::ACTION_DEACTIVATE || $this->request['action_button']==static::ACTION_DEACTIVATE){
            return static::ACTION_DEACTIVATE;
        } elseif($this->request['action']==static::ACTION_COPY){
            return static::ACTION_COPY;
        } elseif($this->getPrimaryValue()) {
            return static::ACTION_EDIT;
        } else {
            return static::ACTION_ADD;
        }
    }

    protected function getPrimaryCode()
    {
        $entity = $this->getTable()::getEntity();
        $primaryCode = $entity->getPrimary();
        return $primaryCode;
    }

    protected function getPrimaryValue()
    {
        return $this->request[$this->getPrimaryCode()];
    }

    public function getSaveMode()
    {
        if(isset($this->request['save']) && trim($this->request['save']) != '') {
            return static::SAVE_CLOSE;
        } elseif(isset($this->request['apply']) && trim($this->request['apply']) != '') {
            return static::SAVE_APPLY;
        }
        return null;
    }

    protected function isAjax()
    {
        return Tools::isAjax() || in_array($this->request['mode'], ['list', 'frame', 'excel']);
    }

    protected function tableExists($tableName)
    {
        if(!$tableName){
            return false;
        }
        global $DB;
        static $c = [];
        if(!isset($c[$tableName])){
            $c[$tableName] = $DB->TableExists($tableName);
        }
        return $c[$tableName];
    }
    /**
     * @param $field EnumField
     * @param array $data
     * @return array
     */
    protected function getSelectOptions($field, $data = [])
    {
        $fieldOptions = $field->getParameter('options');
        $selectOptions = [];
        if($field instanceof EnumField){
            $selectOptions = array_combine($field->getValues(), $field->getValues());
        }
        if($fieldOptions) {
            $selectOptions = $this->getCallableValue($fieldOptions, $data);
        }
        return Tools::toArray($selectOptions);
    }

    protected function useAsCallable($value)
    {
        return !is_string($value) && is_callable($value);
    }

    protected function getCallableValue($value, ...$args)
    {
        if($this->useAsCallable($value)){
            return call_user_func_array($value, $args);
        }
        return $value;
    }


    /**
     * @param $field Field
     */
    protected function getFieldType($field)
    {
        $fieldAdminType = $field->getParameter('field_type');
        if(!is_null($fieldAdminType)){
            return $fieldAdminType;
        }
        return $field->getDataType();
    }

    /**
     * @param array $data
     * @return \Bitrix\Main\Web\Uri
     */
    protected function getUri($data = [])
    {
        global $APPLICATION;
        $uri = new \Bitrix\Main\Web\Uri($APPLICATION->GetCurPage());
        $uri->deleteParams(array_merge([], ['lang', 'action']));
        $uri->addParams($data);
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        return $uri;
    }
}