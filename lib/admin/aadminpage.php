<?php


namespace Wt\Core\Admin;


use CAdminMessage;
use Wt\Core\Tools;

class AAdminPage
{

    protected $moduleId;
    protected $moduleRights = 'W';

    public function checkPermission($minRight = 'W')
    {
        if(!$this->getModuleId()){
            //echo \CAdminMessage::ShowMessage(GetMessage('NO_RIGHTS_FOR_VIEWING'));
            return $this;
        }

        global $APPLICATION;
        $this->moduleRights = $APPLICATION->GetGroupRight($this->getModuleId(), $userGroups = false);
        if($this->moduleRights < $minRight) {
            $APPLICATION->RestartBuffer();
            $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isCanWrite(): bool
    {
        return $this->moduleRights >= 'W';
    }

    /**
     * @param mixed $moduleId
     * @return $this
     */
    public function setModuleId($moduleId)
    {
        $this->moduleId = $moduleId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * @param array $data
     * @return \Bitrix\Main\Web\Uri
     */
    protected function getCurPageUri($data = [])
    {
        global $APPLICATION;
        $uri = new \Bitrix\Main\Web\Uri($page = $APPLICATION->GetCurPage());
        $uri->deleteParams(array_merge(['lang'], array_keys($data)));
        $uri->addParams($data);
        $uri->addParams([
            'lang' => LANGUAGE_ID,
        ]);
        return $uri;
    }


    /**
     * @param string|iterable $message
     * @param string $type PROGRESS|OK|ERROR
     */
    protected function showMessage($message, $type = 'ERROR')
    {
        $Message = new CAdminMessage(array(
            'MESSAGE' => is_iterable($message)?implode('<br>', Tools::toArray($message)):$message,
            'TYPE' => $type,
        ));
        print $Message->Show();
    }


    protected function tests($renderErrors = false)
    {
        $testConfig = [
            [
                'message' => 'Класс \Bitrix\Main\Entity\Field должен иметь методы hasParameter(), getParameter(), setParameter()',
                'description' => 'Не реализованы в старых версиях битрикса<br>Расположен по адресу: <code>/bitrix/modules/main/lib/entity/field.php</code>',
                'test' => function(){
                    return method_exists(\Bitrix\Main\Entity\ScalarField::class, 'hasParameter')
                        && method_exists(\Bitrix\Main\Entity\ScalarField::class, 'getParameter')
                        && method_exists(\Bitrix\Main\Entity\ScalarField::class, 'setParameter');
                },
            ],
        ];

        $isSuccess = true;
        foreach ($testConfig as $key => $value) {
            $testConfig[$key]['success'] = call_user_func_array($value['test'], []);
            if(!$testConfig[$key]['success']){
                $isSuccess = false;
            }
        }
        if($renderErrors && !$isSuccess) {
            ?>
            <table cellpadding="5" border="1" style="border-collapse: collapse; width: 100%">
            <?
            foreach ($testConfig as $key => $value) {
                $templater = new Templater($this);
                ?>
                <tr>
                <td style="vertical-align: top; font-size: 1.25em;text-align: center;"><?= $templater->getCheckTemplate($value['success']) ?></td>
                <td><?= $value['message'] ?><br><small style="color: #5e5e5e;"><?= $value['description'] ?></small></td>
                </tr><?
            }
            ?></table><?
        }
        return $isSuccess;
    }
}