<?php


namespace Wt\Core\Admin;

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Wt\Core\Admin\GlobalMenuBuilder;
use Wt\Core\Tools;

Loc::loadMessages(__FILE__);


class EventHandler
{

	public static function onBuildGlobalMenu(&$arGlobalMenu, &$arModuleMenu)
    {
        $globalMenuBuilder = new GlobalMenuBuilder(app()::getInstances(), app()->module()->getPrefix());
        $globalMenuBuilder->makeTreeMenu();
        $rootMenuConfig = app()->config('root_menu', []);
        if($rootMenuLabel = $rootMenuConfig->get('label')){
            $globalMenuBuilder->getGlobalMenuItem()->setTitle($rootMenuLabel);
        }
        if($sort = $rootMenuConfig->get('sort')){
            $globalMenuBuilder->getGlobalMenuItem()->setSort($sort);
        }
        $strMenuBackgroundImageUrl = $rootMenuConfig->get('background_image_url');
        if($strMenuBackgroundImageUrl){
            Asset::getInstance()->addString('<style>
				.adm-main-menu-item.adm-'.$globalMenuBuilder->getRootId().' .adm-main-menu-item-icon {
					background:url("'.$strMenuBackgroundImageUrl.'") center center no-repeat;
					padding: 4px;
                    background-origin: content-box;
                    box-sizing: border-box;
				}
			</style>', true);
        }

        $globalMenuBuilder->prepareGlobalMenu($arGlobalMenu);


        $GLOBALS['APPLICATION']->SetAdditionalCss(Tools::removeDocRoot(__DIR__ . '/assets/admin/admin.css'));
    }
}
