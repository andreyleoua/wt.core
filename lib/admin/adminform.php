<?php


namespace Wt\Core\Admin;


use CAdminForm;

class AdminForm extends CAdminForm
{

    function AddDateTimeField($id, $label, $value, $required=false)
    {
        $html = CalendarDate($id, $value, $this->GetFormName(), 12);

        $value = htmlspecialcharsbx(htmlspecialcharsback($value));

        $this->tabs[$this->tabIndex]["FIELDS"][$id] = array(
            "id" => $id,
            "required" => $required,
            "content" => $label,
            "html" => '<td width="40%">'.($required? '<span class="adm-required-field">'.$this->GetCustomLabelHTML($id, $label).'</span>': $this->GetCustomLabelHTML($id, $label)).'</td><td>'.$html.'</td>',
            "hidden" => '<input type="hidden" name="'.$id.'" value="'.$value.'">',
        );
    }


    function AddHtmlField($id, $title, $value, $content = '')
    {
        $required = false;
        $html = '<input type="hidden" name="'.$id.'" value="'.$value.'">';
        $html .= $content;

        $this->tabs[$this->tabIndex]["FIELDS"][$id] = array(
            "id" => $id,
            "required" => $required,
            "content" => $content,
            "html" => '<td width="40%">'.($required? '<span class="adm-required-field">'.$this->GetCustomLabelHTML($id, $content).'</span>': $this->GetCustomLabelHTML($id, $title)).'</td><td>'.$html.'</td>',
            "hidden" => '<input type="hidden" name="'.$id.'" value="'.$value.'">',
        );
    }
}