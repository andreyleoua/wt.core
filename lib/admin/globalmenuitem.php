<?php


namespace Wt\Core\Admin;


use Wt\Core\Support\IterableAccess;

class GlobalMenuItem extends IterableAccess
{

    const ICON_CLOUDS_PAGE = 'clouds_page_icon';
    const ICON_SYS_MENU = 'sys_menu_icon';

    const GLOBAL_MENU_SERVICES = 'global_menu_services';



    public function __construct($data = [])
    {
        parent::__construct($data);

        if(!isset($this->rawData['icon'])) {
            $this->setIcon(self::ICON_SYS_MENU);
        }

        if(!isset($this->rawData['visible'])) {
            $this->setVisible(true);
        }
    }

    public function getId()
    {
        return $this->rawData['id'];
    }
    public function setId($id)
    {
        $this->rawData['id'] = $id;
        $this->rawData['menu_id'] = $id;
        $this->rawData['items_id'] = $id;
        return $this;
    }

    public function getTitle()
    {
        return $this->rawData['title'];
    }
    public function setTitle($title)
    {
        $this->rawData['title'] = $this->rawData['text'] = $title;
        return $this;
    }

    public function getIcon()
    {
        return $this->rawData['icon'];
    }
    public function setIcon($icon)
    {
        $this->rawData['page_icon'] = $this->rawData['icon'] = $icon;
        return $this;
    }

    public function getUrl()
    {
        return $this->rawData['url'];
    }

    /**
     * @param string $url [$url = '' - для автоформирования урла]
     * @return $this
     */
    public function setUrl($url = '')
    {
        $this->rawData['url'] = $url;
        return $this;
    }

    public function getSort()
    {
        return $this->rawData['sort'];
    }
    public function setSort($sort)
    {
        $this->rawData['sort'] = $sort;
        return $this;
    }

    public function getParentId()
    {
        return $this->rawData['parent_menu'];
    }
    public function setParentId($parentId)
    {
        $this->rawData['parent_menu'] = $parentId;
        return $this;
    }

    /**
     * @param GlobalMenuItem|array $item
     * @return GlobalMenuItem
     */
    public function addChild($item)
    {
        if(is_array($item)) {
            $item = new GlobalMenuItem($item);
        }
        $this->rawData['items'][] = $item;

        return $this;
    }

    /**
     * @param GlobalMenuItem[] $items
     * @return $this
     */
    public function addChilds($items)
    {
        if(!(is_iterable($items) || is_array($items))) {
            return $this;
        }

        foreach ($items as $item) {
            $this->addChild($item);
        }
        return $this;
    }

    public function setVisible($val)
    {
        $this->rawData['visible'] = (bool)$val;
        return $this;
    }

    public function isVisible()
    {
        global $APPLICATION;
        if($this->getUrl() == $APPLICATION->GetCurPage()) {
            return true;
        }
        return $this->rawData['visible'];
    }

    public function toArray()
    {
        return $this->iterableToArrayRecursive($this->rawData);
    }
}
