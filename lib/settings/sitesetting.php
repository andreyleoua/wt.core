<?php


namespace Wt\Core\Settings;


class SiteSetting extends Setting
{
    public function __construct($moduleId, $siteId)
    {
        $this->siteId = $siteId;
        parent::__construct($moduleId);
    }
}