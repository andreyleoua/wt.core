<?php


namespace Wt\Core\Settings;


use Bitrix\Main;

class Setting
{
    protected $siteId = '';
    protected $moduleId;

    public function __construct($moduleId)
    {
        $this->moduleId = $moduleId;
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|mixed
     * @throws null
     */
    public function getRaw($key = null, $default = null)
    {
        $list = Main\Config\Option::getForModule($this->getModuleId(), $this->siteId);
        if(is_null($key)){
            return $list;
        }
        if(isset($list[$key])){
            return $list[$key];
        }

        return $default;
    }

    public function get($key, $default = null)
    {
        return Main\Config\Option::get($this->getModuleId(), $key, $default, $this->siteId);
    }

    public function set($key, $value)
    {
        Main\Config\Option::set($this->getModuleId(), $key, $value, $this->siteId);
    }

    public function remove($key)
    {
        Main\Config\Option::delete($this->getModuleId(), [
            'name' => $key,
            'site_id' => $this->siteId,
        ]);
    }

    /**
     * @return string
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    public function clearCache()
    {
        $cache = Main\Application::getInstance()->getManagedCache();
        $cache->clean('b_option:' . $this->getModuleId(), 'b_option');
        return $this;
    }
}