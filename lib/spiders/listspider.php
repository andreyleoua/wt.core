<?php


namespace  Wt\Core\Spiders;


use Wt\Core\Settings\Setting;
use Wt\Core\Type\Str;

class ListSpider
{
    protected $context;
    protected $moduleId;
    protected $code;
    protected $period;
    protected $setting;
    protected $data = [];

    public function __construct($moduleId, $code, $period = 86400)
    {
        $this->moduleId = $moduleId;
        $this->code = $code;
        $this->period = $period;
        $this->setting = new Setting($moduleId);
    }

    protected function getKey($name)
    {
        return "spider.{$this->getCode()}.$name";
    }

    public function check($moduleId, $code)
    {
        return ($this->getModuleId() == $moduleId) && ($this->getCode() == $code);
    }

    public function getModuleId()
    {
        return $this->moduleId;
    }

    public function applyNext($callback, $args = [])
    {

    }

    public function next()
    {
        $canDo = $this->canDo();
        if(!$canDo){
            if($this->isExpired()){
                $this->reset();
                $canDo = true;
            }
        }
        if(!$canDo){
            return false;
        }

        $lastId = $this->getLastId();
        if(!$lastId){
            $this->setBeginTimestamp();
        }
        $this->do();

        if($this->getLastId() == $lastId){
            $this->setEndTimestamp();
        }
        return true;
    }

    protected function do()
    {
        foreach(GetModuleEvents(app()->module()->getId(), 'OnAppSpiderListDo', true) as $arEvent){
            $eventResult = ExecuteModuleEventEx($arEvent, array($this));
            if($eventResult === false){
                break;
            }
        }
    }

    public function canDo()
    {
        $resetTimestamp = $this->getResetTimestamp();
        $beginTimestamp = $this->getBeginTimestamp();
        $endTimestamp = $this->getEndTimestamp();

        $result = ($resetTimestamp >= $endTimestamp)
            || ($resetTimestamp >= $beginTimestamp);
        return $result;
    }

    public function isRun()
    {
        $beginTimestamp = $this->getBeginTimestamp();
        $endTimestamp = $this->getEndTimestamp();
        return $beginTimestamp > $endTimestamp;
    }

    public function isExpired()
    {
        $beginTimestamp = $this->getBeginTimestamp();
        $endTimestamp = $this->getEndTimestamp();
        return ($beginTimestamp <= $endTimestamp) && ($endTimestamp + $this->period < $this->time());
    }

    public function getLastId()
    {
        return $this->getSetting()->get($this->getKey('lastId'), '');
    }

    public function setLastId($lastId)
    {
        $this->getSetting()->set($this->getKey('lastId'), $lastId);
        return $this;
    }

    protected function setBeginTimestamp()
    {
        $this->getSetting()->set($this->getKey('beginTimestamp'), $this->time());
        foreach(GetModuleEvents(app()->module()->getId(), 'OnAppSpiderListBegin', true) as $arEvent){
            $eventResult = ExecuteModuleEventEx($arEvent, array($this));
            if($eventResult === false){
                break;
            }
        }
        return $this;
    }

    protected function getBeginTimestamp()
    {
        return (float)$this->getSetting()->get($this->getKey('beginTimestamp'), 0);
    }

    protected function setEndTimestamp()
    {
        $this->getSetting()->set($this->getKey('endTimestamp'), $this->time());

        foreach(GetModuleEvents(app()->module()->getId(), 'OnAppSpiderListEnd', true) as $arEvent){
            $eventResult = ExecuteModuleEventEx($arEvent, array($this));
            if($eventResult === false){
                break;
            }
        }

        return $this;
    }

    protected function getEndTimestamp()
    {
        return (float)$this->getSetting()->get($this->getKey('endTimestamp'), 0);
    }

    protected function setResetTimestamp()
    {
        $this->getSetting()->set($this->getKey('resetTimestamp'), $this->time());
        foreach(GetModuleEvents(app()->module()->getId(), 'OnAppSpiderListReset', true) as $arEvent){
            $eventResult = ExecuteModuleEventEx($arEvent, array($this));
            if($eventResult === false){
                break;
            }
        }
        return $this;
    }

    protected function getResetTimestamp()
    {
        return (float)$this->getSetting()->get($this->getKey('resetTimestamp'), 0);
    }

    public function reset()
    {
        $this->setLastId('');
        $this->setResetTimestamp();
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Setting
     */
    public function getSetting(): Setting
    {
        return $this->setting;
    }

    protected function time()
    {
        return round(microtime(true), 4);
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     * @return ListSpider
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return ListSpider
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}