<?php


namespace Wt\Core\Assets;


class Assets
{

    protected $pluginManager;

    public function __construct(PluginManager $pluginManager)
    {
        $this->pluginManager = $pluginManager;
    }

    public function getPluginManager()
    {
        return $this->pluginManager;
    }

    /**
     * @deprecated AssetsPull
     * @return AssetsPull
     */
    public function getAssetsManager()
    {
        return $this->getPluginManager()->getAssetsPull();
    }

    /**
     * @return AssetsPull
     */
    public function getAssetsPull()
    {
        return $this->getPluginManager()->getAssetsPull();
    }

    /**
     * @param array|string $plugins
     * @return $this
     */
    public function setPlugins($plugins)
    {
        if(!($plugins instanceof \iterable)) {
            $plugins = (array)$plugins;
        }
        collect($plugins)->map([$this, 'setPlugin']);
        return $this;
    }

    /**
     * @param string $pluginName
     * @return bool
     * @throws \Exception
     */
    public function setPlugin($pluginName)
    {
        return $this->getPluginManager()->setPlugin($pluginName);
    }

    /**
     * @param $type
     * @param $value
     * @param int $sort
     * @param string $plugin
     */
    public function setAsset($type, $value, $sort = 100, $plugin = '')
    {
        $this->getPluginManager()->setAsset($type, $value, $sort, $plugin);
    }

    public function setCss($path, $sort = 100, $plugin = '')
    {
        $this->getAssetsPull()->setValue('css', $path, $sort, $plugin);
    }

    public function setJs($path, $sort = 100, $plugin = '')
    {
        $this->getAssetsPull()->setValue('js', $path, $sort, $plugin);
    }

    public function setFont($path, $fontType, $sort = 100, $plugin = '')
    {
        $this->getAssetsPull()->setValue('font', [$path, $fontType], $sort, $plugin);
    }

    public function setString($string, $sort = 100, $plugin = '')
    {
        $this->getAssetsPull()->setValue('string', $string, $sort, $plugin);
    }

    public function setAssets(array $data)
    {
        $this->getPluginManager()->setAssets($data);
    }

    public function render($type = '')
    {
        $this->getAssetsPull()->render($type);
    }

    public function registerPlugin($pluginName, $config)
    {
        $this->getPluginManager()->register($pluginName, $config);
    }

    public function isPluginRegistered($pluginName)
    {
        return $this->getPluginManager()->isRegistered($pluginName);
    }
}