<?php


namespace Wt\Core\Assets;


use Wt\Core\Interfaces\IAssetType;
use Wt\Core\Support\Collection;
use Wt\Core\Tools;

class PluginManager
{

    protected $assetsPull;
    protected $registerPluginCollection;
    private $path;
    private $namespace;
    private $assetsPath = '/assets';

    public function __construct($path, $namespace, AssetsPull $assetsPull)
    {
        $this->path = $path;
        $this->namespace = $namespace;
        $this->assetsPull = $assetsPull;
        $this->registerPluginCollection = new Collection();
    }

    public function setAssets(array $data)
    {
        if(!$data['plugin']) {
            throw new \Exception('Empty plugin name');
        }

        if(!isset($data['path'])) {
            $src = Tools::normalizePath($this->getPath().'/'. $this->namespace.'/'. $this->assetsPath);
        } else {
            $src = $data['path'];
        }

        $plugin = (string)$data['plugin'];

        if(!file_exists(Tools::getDocumentRoot() . $src)){
            throw new \Exception("plugin $plugin in DR / $src not found");
        }

        /**
         * @var IAssetType $assetType
         */
        $sort = $data['sort'] ?? 100;
        foreach ($this->getAssetsPull()->getTypeList() as $assetType) {
            $type = $assetType->getType();
            if(!isset($data[$type])) {
                continue;
            }
            $list = (array)$data[$type];
            foreach ($list as $value) {
                $value = $assetType->getPrepareValue($value, $src);
                $this->setAsset($type, $value, $sort, $plugin);
            }
        }

        return true;
    }

    public function unsetPlugin($pluginName)
    {
        foreach ($this->getAssetsPull()->getValues() as $type => &$list) {
            foreach ($list as $key => $item) {
                if(strcasecmp($item['plugin'], $pluginName) == 0) {
                    $this->getAssetsPull()->unset($type, $key);
                }
            }
        }
    }

    public function registerPluginList($list)
    {
        foreach ($list as $name => $config) {
            $this->register($name, $config);
        }
    }

    public function isRegistered($pluginName)
    {
        return $this->registerPluginCollection->has($pluginName);
    }

    public function register($pluginName, $config)
    {
        if($this->isRegistered($pluginName)){
            return false;
        }
        $this->registerPluginCollection->set($pluginName, $config);
        return true;
    }

    /**
     * @deprecated old
     * @return AssetsPull
     */
    public function getAssetsManager()
    {
        return $this->assetsPull;
    }

    /**
     * @return AssetsPull
     */
    public function getAssetsPull()
    {
        return $this->assetsPull;
    }

    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param array|string $plugins
     * @return $this
     */
    public function setPlugins($plugins)
    {
        if(!($plugins instanceof \iterable)) {
            $plugins = (array)$plugins;
        }
        collect($plugins)->map([$this, 'setPlugin']);
        return $this;
    }

    /**
     * @param string $pluginName
     * @return bool
     * @throws \Exception
     */
    public function setPlugin($pluginName)
    {
        $namespace = $this->namespace;
        $this->preparePluginName($pluginName, $namespace);

        $path = Tools::normalizePath($this->path.'/'. $namespace.'/'. $this->assetsPath .'/'. $pluginName);

        if($pluginName == ''){
            throw new AssetException('Empty plugin name');
        }

        if(!$this->getRegisterPluginCollection()->has($pluginName)) {
            throw new AssetException("ERROR: plugin $pluginName not registered");
        }

        /**
         * @var Collection $data
         */
        $data = $this->getRegisterPluginCollection()->get($pluginName);
        $data = new Collection($data);
        $path = $data->get('path', $path);

        $realpath = realpath(Tools::getDocumentRoot() . $path);
        if(($realpath === false) && !$data->has('path')) {
            $altPath = Tools::normalizePath($this->path.'/'. strtolower($namespace).'/'. $this->assetsPath .'/'. $pluginName);
            $realpath = realpath(Tools::getDocumentRoot() . $altPath);
        }
        if($realpath === false) {
            throw new AssetException("Path $path not found for plugin $pluginName");
        }
        $data->set('plugin', $pluginName);
        $data->set('path', Tools::removeDocRoot($realpath));

        $this->setPlugins((array)$data->get('deps', []));

        return $this->setAssets($data->toArray());
    }

    public function setAsset($type, $value, $sort = 100, $plugin = '')
    {
        $this->getAssetsPull()->setValue($type, $value, $sort, $plugin);
    }

    public function render($type = '')
    {
        $this->getAssetsPull()->render($type);
    }

    private function preparePluginName(&$plugin, &$namespace = '')
    {
        if(str_contains($plugin, ':')) {
            $argsList = explode(':', $plugin);
            if(strlen($argsList[0])) {
                $namespace = $argsList[0];
                $plugin = $argsList[1];
            }
        }
    }

    /**
     * @return Collection
     */
    public function getRegisterPluginCollection()
    {
        return $this->registerPluginCollection;
    }
}