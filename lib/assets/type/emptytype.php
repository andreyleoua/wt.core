<?php


namespace Wt\Core\Assets\Type;


use Wt\Core\Interfaces\IAssetType;
use Wt\Core\Tools;

class EmptyType implements IAssetType
{
    protected $type;
    protected $format;
    protected $formatCallback = 'vsprintf';
    protected $usePath;
    protected $keyHashCallback;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     * @return $this
     */
    public function setFormat(string $format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUsePath()
    {
        return $this->usePath;
    }

    /**
     * @param mixed $usePath
     * @return $this
     */
    public function setUsePath(bool $usePath)
    {
        $this->usePath = $usePath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKeyHashCallback()
    {
        return $this->keyHashCallback;
    }

    /**
     * @param $callback
     * @return $this
     */
    public function setKeyHashCallback($callback)
    {
        $this->keyHashCallback = $callback;
        return $this;
    }

    /**
     * @param string $value
     * @return string
     */
    public function getKey($value)
    {
        if(is_iterable($value)) {
            $value = serialize($value);
        }

        if(is_callable($this->getKeyHashCallback())) {
            return call_user_func($this->getKeyHashCallback(), $value);
        }

        return (string)$value;
    }

    /**
     * @param string $value
     * @return string
     */
    public function getFormatTemplate($value)
    {
        $template = (string)$value;

        if($this->getFormat() && is_callable($this->getFormatCallback())) {
            $argsTemplateCallback = array_merge((array)$this->getFormat(), [(array)$value]);
            $template = call_user_func_array($this->getFormatCallback(), $argsTemplateCallback);
        }
        return $template;
    }

    public function getFormatCallback()
    {
        return $this->formatCallback;
    }

    public function setFormatCallback($callback)
    {
        $this->formatCallback = $callback;
        return $this;
    }

    public function getPrepareValue($value, $pluginPath)
    {
        if(!$this->getUsePath()) {
            return $value;
        }
        $path = '';
        if(is_array($value)) {
            if(!is_null($value['path'])) {
                $path = &$value['path'];
            }
        } else {
            $path = &$value;
        }
        $path = Tools::normalizePath($pluginPath . '/' . $path);
        return $value;
    }

    public function getTemplate($key, $data)
    {
        // TODO: Implement getTemplate() method.
    }
}