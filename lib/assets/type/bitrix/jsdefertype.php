<?php


namespace Wt\Core\Assets\Type\Bitrix;


class JsDeferType extends EmptyType
{
    protected $type = 'jsDefer';
    protected $format = '<script src="%s" defer></script>';
    protected $usePath = true;
}