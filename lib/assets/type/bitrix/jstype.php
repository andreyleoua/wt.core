<?php


namespace Wt\Core\Assets\Type\Bitrix;


class JsType extends EmptyType
{
    protected $type = 'js';
    protected $bxAssetMethod = 'addJs';
    protected $usePath = true;
}