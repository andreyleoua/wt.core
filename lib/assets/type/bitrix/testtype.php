<?php


namespace Wt\Core\Assets\Type\Bitrix;


class TestType extends EmptyType
{
    protected $type = 'test';
    protected $format = '<link rel="preload" href="%s" as="font" crossorigin="anonymous" />';
    protected $usePath = true;
}