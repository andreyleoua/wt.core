<?php


namespace Wt\Core\Assets\Type\Bitrix;


use Bitrix\Main\Page\Asset as BitrixAssetAlias;

class StringType extends EmptyType
{
    protected $type = 'string';
    protected $keyHashCallback = 'md5';


    public function getTemplate($key, $data)
    {

        $formatTemplate = $this->getFormatTemplate($data['value']);


        $templateCallback = [BitrixAssetAlias::getInstance(), $this->getBxAssetMethod()];
        if(is_callable($templateCallback)) {
            $templateCallbackArgs = [$formatTemplate];
            /**
             * @see \Bitrix\Main\Page\Asset::addString
             * @see \Bitrix\Main\Page\AssetLocation
             * @see https://dev.1c-bitrix.ru/api_d7/bitrix/main/page/asset/addstring.php
             */
            if('addString' == $this->getBxAssetMethod()) {
                if(in_array($data['sort'], $this->bxAssetLocation) && !constant('ADMIN_SECTION')) {
                    $templateCallbackArgs = [$formatTemplate, false, $data['sort']];
                }
            }

            $r = call_user_func_array($templateCallback, $templateCallbackArgs);

            BitrixAssetAlias::getInstance()->addString('<meta property="kit:asset:string" content="'.$key.'">');

        }

        return '';
    }
}