<?php


namespace Wt\Core\Assets\Type\Bitrix;


class JsStringType extends EmptyType
{
    protected $type = 'jsString';
    protected $format = '<script type="text/javascript" src="%s"></script>';
    protected $usePath = true;
}