<?php


namespace Wt\Core\Assets\Type\Bitrix;


class JsAsyncType extends EmptyType
{
    protected $type = 'jsAsync';
    protected $format = '<script src="%s" async></script>';
    protected $usePath = true;
}