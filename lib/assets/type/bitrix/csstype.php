<?php


namespace Wt\Core\Assets\Type\Bitrix;


class CssType extends EmptyType
{
    protected $type = 'css';
    protected $bxAssetMethod = 'addCss';
    //protected $format = '<link href="%s" type="text/css" rel="stylesheet" />';
    protected $usePath = true;
}