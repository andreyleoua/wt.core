<?php


namespace Wt\Core\Assets\Type\Bitrix;


class FontType extends EmptyType
{
    protected $type = 'font';
    protected $format = '<link rel="preload" href="%s" as="font" type="%s" crossorigin="anonymous" />';
    protected $usePath = true;
}