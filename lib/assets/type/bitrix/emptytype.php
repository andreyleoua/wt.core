<?php


namespace Wt\Core\Assets\Type\Bitrix;


use Bitrix\Main\Page\Asset as BitrixAssetAlias;
use Wt\Core\Assets\Type\EmptyType as EmptyTypeAlias;

class EmptyType extends EmptyTypeAlias
{
    protected $bxAssetLocation = [];
    public function __construct()
    {
        $assetLocationReflectionClass = new \ReflectionClass(\Bitrix\Main\Page\AssetLocation::class);
        $this->bxAssetLocation = $assetLocationReflectionClass->getConstants();
    }

    protected $bxAssetMethod = 'addString';

    /**
     * @return string
     */
    public function getBxAssetMethod()
    {
        return $this->bxAssetMethod;
    }

    /**
     * @param string $bxAssetMethod
     * @return $this
     */
    public function setBxAssetMethod(string $bxAssetMethod)
    {
        $this->bxAssetMethod = $bxAssetMethod;
        return $this;
    }

    public function getTemplate($key, $data)
    {
        /**
         * @global $APPLICATION \CMain
         */
        global $APPLICATION;
        $isAdminSection = defined('ADMIN_SECTION') && constant('ADMIN_SECTION');

        $formatTemplate = $this->getFormatTemplate($data['value']);

        if($formatTemplate !== '') {
            $key = $formatTemplate;
        }

        $templateCallback = [BitrixAssetAlias::getInstance(), $this->getBxAssetMethod()];
        if(is_callable($templateCallback)) {
            $templateCallbackArgs = [$key];
            /**
             * @see \Bitrix\Main\Page\Asset::addString
             * @see \Bitrix\Main\Page\AssetLocation
             * @see https://dev.1c-bitrix.ru/api_d7/bitrix/main/page/asset/addstring.php
             */
            if('addString' == $this->getBxAssetMethod()) {
                if(in_array($data['sort'], $this->bxAssetLocation) && !$isAdminSection) {
                    $templateCallbackArgs = [$key, false, $data['sort']];
                }
            }
            /**
             * @see \CAdminPage::ShowCSS
             * @see /bitrix/modules/main/interface/prolog_main_admin.php
             */
            if($isAdminSection && $this->getBxAssetMethod() == 'addCss'){
                $APPLICATION->SetAdditionalCSS($key);
            } else {
                $r = call_user_func_array($templateCallback, $templateCallbackArgs);
            }

            if($data['value']) {
                preg_match('/^[^#?]*/m', $data['value'], $matches);
                BitrixAssetAlias::getInstance()->addString('<meta property="kit:file-load" content="'.$matches[0].'">');
            }
        }

        return '';
    }
}