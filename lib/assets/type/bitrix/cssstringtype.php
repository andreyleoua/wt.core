<?php


namespace Wt\Core\Assets\Type\Bitrix;


class CssStringType extends EmptyType
{
    protected $type = 'cssString';
    protected $format = '<link href="%s" type="text/css" rel="stylesheet" />';
    protected $usePath = true;
}