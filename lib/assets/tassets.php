<?php

namespace Wt\Core\Assets;

use Wt\Core\Tools;

trait TAssets
{

    /**
     * @var Assets $templater
     */
    private $assets;

    public function assets()
    {
        if(is_null($this->assets)){
            $namespace = self::class;
            if($namespace == '') $namespace = __CLASS__;
            $namespace = basename(Tools::normalizePath($namespace));
            $cl = new \ReflectionClass(self::class);
            $path = Tools::removeDocRoot(dirname($cl->getFileName()));

            $pluginManager = resolve(PluginManager::class, [
                $path,
                $namespace
            ]);

            $this->assets = resolve(Assets::class, [$pluginManager]);
        }

        return $this->assets;
    }
}