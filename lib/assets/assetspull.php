<?php


namespace Wt\Core\Assets;


use Wt\Core\Interfaces\IAssets;
use Wt\Core\Interfaces\IAssetType;
use Wt\Core\Interfaces\IRender;
use Wt\Core\Tools;

class AssetsPull implements IAssets, IRender
{

    protected $values = [];

    /**
     * @var array
     */
    protected $typeList = [];

    public function __construct(){}

    public function setValue($type, $value, $sort = 100, $plugin = '')
    {
        $assetType = $this->typeList[$type];

        /**
         * @var $assetType IAssetType
         */
        if(!($this->typeList[$type] instanceof IAssetType)) {
            return false;
        }

        $key = $assetType->getKey($value);

        if($key == '') {
            return false;
        }

        if(!isset($this->values[$type])) {
            $this->values[$type] = [];
        }

        $this->values[$type][$key] = [
            'id' => $this->values[$type][$key]?:(count($this->values[$type]) + 1),
            'sort' => $sort,
            'plugin' => $plugin,
            'value' => $value,
        ];
        return true;
    }

    public function unset($type, $key)
    {
        unset($this->values[$type][$key]);
    }

    public function getValues($type = '')
    {
        if($type) {
            if(isset($this->values[$type])) {
                return $this->values[$type];
            } else {
                return [];
            }
        }
        return $this->values;
    }

    public function render($type = '', $arParams = [])
    {
        echo $this->getTemplate($type, $arParams);
    }

    private function getTemplate($type = '', $arParams = [])
    {
        $result = '';
        /**
         * @var $assetType IAssetType
         */
        foreach ($this->typeList as $_type => $assetType) {

            if($type != '' && $type != $_type) {
                continue;
            }

            $this->values[$_type] = $this->sort((array)$this->values[$_type], [
                'sort' => SORT_ASC,
                'id' => SORT_ASC,
            ]);

            foreach ($this->values[$_type] as $key => $data) {
                $result .= $assetType->getTemplate($key, $data);
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return $this->typeList;
    }

    /**
     * @param array $typeList
     * @return $this
     */
    public function setTypeList(array $typeList)
    {
        foreach ($typeList as $type) {
            if($type instanceof IAssetType) {
                $this->typeList[$type->getType()] = $type;
            }
        }
        return $this;
    }

    private function sort($array, $order)
    {
        $args = [];
        foreach ($order as $col => $sort) {
            $args[] = $col;
            if(!is_numeric($sort)) {
                $sort = trim(strtoupper($sort));
                if(!preg_match('/^(asc|desc)$/i', $sort)) {$sort = 'ASC';};
                $sort = constant('SORT_' . $sort);
            }
            $args[] = $sort;
        }
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($array as $key => $row){
                    $tmp[$key] = $row[$field];
                }
                $args[$n] = $tmp;
            }
        }
        $args[] = &$array;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
}