<?php


namespace Wt\Core\Assets;


class AssetsService extends Assets
{
    public function __construct()
    {

        $config = app()->config();
        $container = app()->container();
        $templaterConfig = $config->get('assets', [])->toArray();
        $path = $templaterConfig['path'];
        $namespace = $templaterConfig['namespace'];

        /**
         * @var $pluginManager PluginManager
         */
        $pluginManager = $container->make(PluginManager::class, [
            $path,
            $namespace
        ]);

        $pluginManager->registerPluginList($config->get('plugins', []));

        parent::__construct($pluginManager);
    }
}