<?php


namespace Wt\Core\Interfaces;


interface IAjaxHandler
{
	public function ajaxStart();
	public function setResponse(IResponse $response);
	public function responseSuccess($message);
	public function responseError($message);
	public function getResponse();
}