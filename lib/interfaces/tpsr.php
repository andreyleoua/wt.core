<?php


namespace Wt\Core\Interfaces;


use Wt\Core\Support\CollectionToPsr;
use Wt\Core\Type\Str;

/**
 * Трейт для преобразования битриксовых названий (SCREAMING_SNAKE_CASE) в нормальный вид по стандарту PSR (camelCase)
 **/
trait TPsr
{

    protected $recursion = false;
    protected $rawData;

    /**
     * Case для raw данных
     *
     * @return  string camel|snake|null
     */
    protected function getCase()
    {
        return 'screamingSnake';
    }

	protected function searchRawKey($psrKey)
    {
        $offsetExists = function ($offset) {
            return isset($this->rawData[$offset]) || array_key_exists($offset, $this->rawData);
        };

        $s = function ($name) use ($offsetExists) {
            $result = null;

//            $lc = mb_strtolower($name);
            $keys = [
                '~' . $name,
                $name,
//                '~' . $lc,
//                $lc,
                '~UF_' . $name,
                'UF_' . $name,
//                '~uf_' . $lc,
//                'uf_' . $lc
            ];

            foreach ($keys as $key){
                if($offsetExists($key)){
                    $result = $key;
                    break;
                }
            }

            return $result;
        };

        $rawKey = $this->getRawKey($psrKey);
        $result = $s($rawKey);
        if(is_null($result) && ($psrKey !== $rawKey)){
            $result = $s($psrKey);
        }

        if(is_null($result)){
            /**
             * @todo throw
             */
        }
        return $result;
    }

	public function __isset($name)
    {
        $key = $this->searchRawKey($name);
        return !is_null($key);
    }

    public function __unset($name)
    {
        $key = $this->searchRawKey($name);
        if(!is_null($key)){
            unset($this->rawData[$key]);
        }
    }

    /**
     *
     * @param string $name camelCase
     * @return mixed
     */
    public function &__get($name)
	{
        $key = $this->searchRawKey($name);
        if(is_null($key)){
            $v = null;
            return $v;
        }
		return $this->valuePsr($this->rawData[$key]);
	}

    protected function &valuePsr(&$value)
    {
        if( $this->recursion && is_array($value) ){
            $value = new CollectionToPsr($value, $this->recursion);
            return $value;
        }

        return $value;
    }

	public function __set($name, $value)
    {
        $key = $this->searchRawKey($name);
        if(!is_null($key)){
            $this->rawData[$key] = $value;
            return;
        }
        $this->rawData[$this->getRawKey($name)] = $value;
    }

    public function getRaw($key = null, $def = null)
	{
	    if(is_null($key)){
            return $this->rawData;
        }
	    if(isset($this->rawData[$key])){
	        return $this->rawData[$key];
        }
	    return value($def);
	}

	protected function getRawKey($psrKey)
    {
        $case = $this->getCase();
        if($case === 'snake'){
            return Str::snake($psrKey);
        } elseif($case === 'camel'){
            return Str::camel($psrKey);
        } elseif($case === 'screamingSnake'){
            return Str::screamingSnake($psrKey);
        } elseif($case === 'studly'){
            return Str::studly($psrKey);
        } elseif($case === 'kebab'){
            return Str::kebab($psrKey);
        }

        return $psrKey;
    }

    //abCd to AB_CD
    protected function camelToSnake($str, $upper = true)
    {
        return Str::camelToSnake($str, $upper);
    }

    /**
     * @return IFields
     */
    public function fields()
    {
        return $this;
    }

    /**
     * @return IFields
     * @deprecated use fields()
     */
    public function getFields()
    {
        return $this;
    }
}