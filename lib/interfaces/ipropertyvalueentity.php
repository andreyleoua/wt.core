<?php


namespace Wt\Core\Interfaces;


use Wt\Core\Entity\APropertyEntity;

interface IPropertyValueEntity
{
    public function getElementId();
    public function isActive();
    public function isRequired();
    public function isMultiple();
    public function getValue();
    public function getDefaultValue();
    public function getBaseType();
    public function getCode();
    public function getHint();
    public function getName();
    public function getUserType();
    public function getSettings();
    public function getDisplayValue();
    public function getValueEntity();
    /** @return APropertyEntity */
    public function getPropertyEntity();
    public function getPropertyFactory();
    /** @return array */
    public function getRestoredValue();
}