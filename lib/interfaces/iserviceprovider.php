<?php


namespace Wt\Core\Interfaces;


interface IServiceProvider
{
    public function register(IApp $app);
}