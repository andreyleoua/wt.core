<?php


namespace Wt\Core\Interfaces;


interface IResourceLimiter
{
    public function reset();
    public function setForce($force);
    public function isValid();
    public function next();
    public function doWhileTrue($callback, $args = []);
}