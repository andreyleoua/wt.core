<?php

namespace Ittown\Core\Contracts;

/**
 * Трейт для реднеринга класса через включаемый файл
 **/
trait TEasyRender
{
	public function render($templateName)
	{
		if( !strpos($templateName, '.php') ){
			$templateName .= '.php';
        }

		$GLOBALS['APPLICATION']->IncludeFile(SITE_TEMPLATE_PATH.DIRECTORY_SEPARATOR."block_templates".DIRECTORY_SEPARATOR.$templateName, ['arResult' => $this], ['SHOW_BORDER' => false]);
	}
}