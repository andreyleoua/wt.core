<?php


namespace Wt\Core\Interfaces;


interface IAssetType
{
    public function getType();
    public function setType(string $type);
    public function getFormatCallback();
    public function setFormatCallback($callback);
    public function getFormat();
    public function setFormat(string $format);
    public function getFormatTemplate($value);
    public function getUsePath();
    public function setUsePath(bool $usePath);
    public function getKeyHashCallback();
    public function setKeyHashCallback($callback);
    public function getKey($value);
    public function getPrepareValue($value, $pluginPath);
    public function getTemplate($key, $data);
}