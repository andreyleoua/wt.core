<?php


namespace Wt\Core\Interfaces;

/**
 * фабрика, которая может обернуть себя в проксирующий класс для кеширования результатов
 */
interface ICacheFactoryFacade
{
    public function cache($type);
}