<?php

namespace Wt\Core\Interfaces;


interface IResponse
{
	public function toArray();
}