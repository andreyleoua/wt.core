<?php


namespace Wt\Core\Interfaces;

/**
 * фабрика которая может быть закеширована
 */
interface ICacheFactory
{
    public function __getCachePrefix();
}