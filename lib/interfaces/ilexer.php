<?php


namespace Wt\Core\Interfaces;


interface ILexer
{
    public function lexer($mixedService, $args = []);
}