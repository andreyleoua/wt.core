<?php


namespace Wt\Core\Interfaces;


use Wt\Core\PropertyAdapter\APropertyAdapter;

interface IPropertyEntity
{
    public function isActive();
    public function isMultiple();
    public function isRequired();
    public function isBaseType($type);
    public function getName();
    public function getCode();
    public function getHint();
    public function getBaseType();
    public function getUserType();
    public function getDefaultValue();
    /** @return array */
    public function getSettings();
    /** @return APropertyAdapter */
    public function getAdapter();
}