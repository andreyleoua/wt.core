<?php

namespace Wt\Core\Interfaces;

interface IMigration
{
	public function up();
	public function down();
}