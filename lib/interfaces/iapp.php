<?php


namespace Wt\Core\Interfaces;


use Wt\Core\App\ADefines;
use Wt\Core\App\ModuleEntity;
use Wt\Core\App\ServiceContainer;
use Wt\Core\Settings\Setting;
use Wt\Core\Support\LexerCollection;

interface IApp
{
    /**
     * @return ModuleEntity
     */
    public function module();
    public function env();

    /**
     * @return ServiceContainer
     */
    public function container();
	public function factory($factoryName = '');
    public function service($serviceName = '', $args = []);

    /**
     * @return ADefines
     */
    public function defines();

    /**
     * @param string $name
     * @param null $default
     * @return LexerCollection|mixed
     */
    public function config($name = '', $default = null);

    /**
     * @param string $name
     * @param null $default
     * @return Setting|mixed
     */
    public function setting($name = '', $default = null);
}