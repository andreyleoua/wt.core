<?php


namespace Wt\Core\Interfaces;


interface ICache
{
    public function get($key, $default = null);
    public function put($key, $value, $ttl = null);
    public function has($key);
    public function remember($key, $ttl, $default = null);
    public function forget($key);
    public function clear();

    // abstract public function delete($key);

    // abstract public function getMultiple($keys, $default = null);

    // abstract public function setMultiple($values, $ttl = null);

    // abstract public function deleteMultiple($keys);
}