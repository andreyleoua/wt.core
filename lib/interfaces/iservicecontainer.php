<?php


namespace Wt\Core\Interfaces;


interface IServiceContainer
{
    public function make($strClass, $args = []);
    public function bind($strClass, $object = null);
    public function singleton($strClass, $object = null);
    public function alias($aliasName, $realService);
    public function has($id);
    public function set($key, $value);
    public function unBind($strClass);
}