<?php


namespace Wt\Core\Interfaces;


interface IProxyFactory
{
    public function setFactory(IFactory $factory);
    public function getFactory();
    public function __call($name, $arguments);
}