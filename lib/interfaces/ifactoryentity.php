<?php


namespace Wt\Core\Interfaces;


interface IFactoryEntity
{
    public function setFactory(IFactory $factory);
    public function getFactory();
    public function hasFactory();
}