<?php


namespace Wt\Core\Interfaces;


interface IReverseProxyFactory
{
    public function setParentFactory(IFactory $factory);
    public function getParentFactory();
}