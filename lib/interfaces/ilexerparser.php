<?php


namespace Wt\Core\Interfaces;


interface ILexerParser
{
    public function lexerCallNext($currentService, $next, $args = []);
}