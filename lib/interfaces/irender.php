<?php


namespace Wt\Core\Interfaces;


interface IRender
{
    public function render($templateName, $arParams = []);
}