<?php


namespace Wt\Core\Interfaces;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\AFactory;

interface IPropertyAdapter
{
    /** @return AFactory|null */
    public function getValueFactory();
    /**
     * @param IPropertyValueEntity $propertyValue
     * @return AEntity|AEntityCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue);

    public function getValue(array $restoredValue);
}