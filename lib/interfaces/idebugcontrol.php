<?php


namespace Wt\Core\Interfaces;


interface IDebugControl
{
    public function begin();
    public function end();
    public function print($return = false);
    public function getData();
    public function reset();
    public function __toString();
}