<?php


namespace Wt\Core\AbstractFactory;


use Wt\Core\Support\LexerCollection;

class ShopIBlockAbstractFactory extends AAbstractFactory
{
    protected $blockId;

    public function __construct($iBlockId, LexerCollection $config)
    {
        parent::__construct($config);
        $this->blockId = (int)$iBlockId;
    }

    /**
     * @return int
     */
    public function getBlockId()
    {
        return $this->blockId;
    }

}