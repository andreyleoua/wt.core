<?php


namespace Wt\Core\AbstractFactory;


use Wt\Core\Entity\HlBlockEntity;
use Wt\Core\Factory\HlBlock\ElementFactory;
use Wt\Core\Factory\HlBlockFactory;
use Wt\Core\Factory\UserFieldEnumFactory;
use Wt\Core\Factory\UserFieldFactory;
use Wt\Core\Support\LexerCollection;

class HlBlockAbstractFactory extends AAbstractFactory
{
    protected $blockId;

    public function __construct($hlBlockId, LexerCollection $config)
    {
        parent::__construct($config);
        $this->blockId = (int)$hlBlockId;
    }

    /**
     * @return int
     */
    public function getBlockId()
    {
        return $this->blockId;
    }

    public function getEntityId()
    {
        return $this->getHlBlockEntity()->getUserFieldEntityId();
    }

    /**
     * @return ElementFactory
     */
    public function getElementFactory()
    {
        return $this->getFactoryInstance(ElementFactory::class, [$this->getBlockId()]);
    }

    /**
     * @deprecated use getUserFieldFactory()
     * @return UserFieldFactory
     */
    public function getUfPropertyFactory()
    {
        return $this->getUserFieldFactory();
    }

    /**
     * @deprecated use app()->factory()->main()->getUserFieldFactory($entityId)
     * @return UserFieldFactory
     */
    public function getUserFieldFactory()
    {
        return app()->factory()->main()->getUserFieldFactory($this->getEntityId());
        //return $this->getFactoryInstance(UserFieldFactory::class, [$this->getEntityId()]);
    }

    /**
     * @deprecated use app()->factory()->main()->getUserFieldFactory($entityId)
     * @return UserFieldEnumFactory
     */
    public function getUserFieldEnumFactory($propertyId)
    {
        return app()->factory()->main()->getUserFieldEnumFactory($this->getEntityId(), $propertyId);
        //return $this->getFactoryInstance(UserFieldEnumFactory::class, [$this->getEntityId(), $propertyId]);
    }

    /**
     * @return HlBlockFactory
     */
    private function getHlBlockFactory()
    {
        return app()->factory()->main()->getHlBlockFactory();
    }

    /**
     * @return HlBlockEntity
     */
    private function getHlBlockEntity()
    {
        return $this->getHlBlockFactory()->getEntityById($this->getBlockId());
    }
}