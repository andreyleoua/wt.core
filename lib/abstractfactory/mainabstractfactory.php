<?php

namespace Wt\Core\AbstractFactory;

use Wt\Core\Factory\AgentFactory;
use Wt\Core\Factory\CultureFactory;
use Wt\Core\Factory\Main\FileFactory;
use Wt\Core\Factory\Main\ImageFactory;
use Wt\Core\Factory\Main\SitemapFactory;
use Wt\Core\Factory\SiteFactory;
use Wt\Core\Factory\LanguageFactory;
use Wt\Core\Factory\UserFieldEnumFactory;
use Wt\Core\Factory\UserFieldFactory;
use Wt\Core\Factory\UserGroupFactory;
use Wt\Core\Factory\HlBlockFactory;
use Wt\Core\Factory\IBlockFactory;
use Wt\Core\Factory\IBlockTypeFactory;
use Wt\Core\Factory\UserFactory;

class MainAbstractFactory extends AAbstractFactory
{
    /**
     * @return UserFactory
     */
    public function getUserFactory()
    {
        return $this->getFactoryInstance(UserFactory::class);
    }

    /**
     * @return UserGroupFactory
     */
    public function getUserGroupFactory()
    {
        return $this->getFactoryInstance(UserGroupFactory::class);
    }

    /**
     * @return IBlockFactory
     */
    public function getIBlockFactory()
    {
        return $this->getFactoryInstance(IBlockFactory::class);
    }

    /**
     * @return IBlockTypeFactory
     */
    public function getIBlockTypeFactory()
    {
        return $this->getFactoryInstance(IBlockTypeFactory::class);
    }

    /**
     * @return HlBlockFactory
     */
    public function getHlBlockFactory()
    {
        return $this->getFactoryInstance(HlBlockFactory::class);
    }

    /**
     * @return FileFactory
     */
    public function getFileFactory()
    {
        return $this->getFactoryInstance(FileFactory::class);
    }

    /**
     * @return ImageFactory
     */
    public function getImageFactory()
    {
        return $this->getFactoryInstance(ImageFactory::class);
    }

    /**
     * @return SiteFactory
     */
    public function getSiteFactory()
    {
        return $this->getFactoryInstance(SiteFactory::class);
    }

    /**
     * @return LanguageFactory
     */
    public function getLanguageFactory()
    {
        return $this->getFactoryInstance(LanguageFactory::class);
    }

    /**
     * @return AgentFactory
     */
    public function getAgentFactory()
    {
        return $this->getFactoryInstance(AgentFactory::class);
    }

    /**
     * @return UserFieldFactory
     */
    public function getUserFieldFactory($entityId)
    {
        return $this->getFactoryInstance(UserFieldFactory::class, [$entityId]);
    }

    /**
     * @return UserFieldEnumFactory
     */
    public function getUserFieldEnumFactory($entityId, $propertyId)
    {
        return $this->getFactoryInstance(UserFieldEnumFactory::class, [$entityId, $propertyId]);
    }

    /**
     * @return CultureFactory
     */
    public function getCultureFactory()
    {
        return $this->getFactoryInstance(CultureFactory::class);
    }

    /**
     * @return SitemapFactory
     */
    public function getSitemapFactory()
    {
        return $this->getFactoryInstance(SitemapFactory::class);
    }
}