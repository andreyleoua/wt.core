<?php


namespace Wt\Core\AbstractFactory;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\ICacheFactoryFacade;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Interfaces\IProxyFactory;
use Wt\Core\Interfaces\IReverseProxyFactory;
use Wt\Core\Support\LexerCollection;
use Wt\Core\Tools;

abstract class AAbstractFactory implements IFactory
{
    /**
     * @var LexerCollection
     */
    protected $config;

    public function __construct(LexerCollection $config)
    {
        $this->config = $config;
    }

    /**
     * @param null $name
     * @param null $def
     * @return LexerCollection
     */
    public function getConfig($name = null, $def = null)
    {
        if(!is_null($name)){
            return $this->config->get($name, $def);
        }
        return $this->config;
    }

    /**
     * @deprecated OLD
     */
    public function config()
    {
        throw new \Error('Used deprecated method AAbstractFactory::config()');
    }

    protected $factoryInstances = [];

    /**
     * @param string $className
     * @param array $args
     * @return AFactory
     * @throws null
     */
    public function getFactoryInstance($className, $args = [])
    {
        $className = ltrim($className, '\\');
        /**
         * @todo !!! слабое место !!! serialize(0) != serialize('0')
         * решено через str_replace(json(args))
         */
        $hash = '';
        if($args) {
            $hash = '#' . md5(str_replace( '"', '', Tools::json($args)));
        }

        $key = $className . $hash;

        if(!$this->factoryInstances[$key]){
            /**
             * @var AFactory $factory
             */
            $factory = $this->resolveFactory($className, $args);
            if(($factory instanceof IReverseProxyFactory) || ($factory instanceof IProxyFactory)) {
                $factory->setParentFactory($this);
            }
            $this->factoryInstances[$key] = $factory;
        }

        return $this->factoryInstances[$key];
    }
    /**
     * @param string $factoryClass
     * @param array $args
     * @return AFactory
     * @throws null
     */
    public function resolveFactory($factoryClass, $args = [])
    {
        $config = $this->getConfig($factoryClass, []);
        $alias = $config->get('alias', '');

        if($alias && is_a($alias, IFactory::class, true)){
            $factoryClass = $alias;
        }

        /**
         * @var AFactory $factory
         */
        $factory = resolve($factoryClass, $args);
        if($factory instanceof IReverseProxyFactory) {
            $factory->setParentFactory($this);
        }
        if($factory instanceof ICacheFactoryFacade) {
            return $factory->cache($config->get('cache', [])->toArray());
        }
        return $factory;
    }
}