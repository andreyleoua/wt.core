<?php


namespace Wt\Core\AbstractFactory;


use Wt\Core\Factory\IBlock\ElementFactory;
use Wt\Core\Factory\IBlock\PropertyEnumFactory;
use Wt\Core\Factory\IBlock\PropertyFactory;
use Wt\Core\Factory\IBlock\PropertyValueFactory;
use Wt\Core\Factory\IBlock\SectionFactory;
use Wt\Core\Factory\IBlock\SectionPropertyFactory;
use Wt\Core\Factory\Shop\ProductFactory;
use Wt\Core\Factory\UserFieldFactory;
use Wt\Core\Support\LexerCollection;

class IBlockAbstractFactory extends AAbstractFactory
{
    protected $blockId;

    public function __construct($iBlockId, LexerCollection $config)
    {
        parent::__construct($config);
        $this->blockId = (int)$iBlockId;
    }

    /**
     * @return int
     */
    public function getBlockId()
    {
        return $this->blockId;
    }

    /**
     * @return ProductFactory
     * @throws null
     */
    public function getProductFactory()
    {
        return $this->getFactoryInstance(ProductFactory::class, [$this->getBlockId()]);
    }

    /**
     * @return ElementFactory
     */
    public function getElementFactory()
    {
        return $this->getFactoryInstance(ElementFactory::class, [$this->getBlockId()]);
    }

    /**
     * @return SectionFactory
     */
    public function getSectionFactory()
    {
        return $this->getFactoryInstance(SectionFactory::class, [$this->getBlockId()]);
    }

    /**
     * @return PropertyFactory
     */
    public function getPropertyFactory()
    {
        return $this->getFactoryInstance(PropertyFactory::class, [$this->getBlockId()]);
    }

    /**
     * @return SectionPropertyFactory
     */
    public function getSectionPropertyFactory($sectionId = 0)
    {
        return $this->getFactoryInstance(SectionPropertyFactory::class, [$this->getBlockId(), $sectionId]);
    }

    /**
     * @return UserFieldFactory
     */
    public function getSectionUserFieldFactory()
    {
        return app()->factory()->main()->getUserFieldFactory('IBLOCK_'.$this->getBlockId().'_SECTION');
    }

    /**
     * @return PropertyEnumFactory
     */
    public function getPropertyEnumFactory($propertyId)
    {
        return $this->getFactoryInstance(PropertyEnumFactory::class, [$this->getBlockId(), $propertyId]);
    }

    /**
     * @return PropertyValueFactory
     */
    public function getPropertyValueFactory($elementId)
    {
        return $this->getFactoryInstance(PropertyValueFactory::class, [$this->getBlockId(), $elementId]);
    }
}