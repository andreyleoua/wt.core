<?php


namespace Wt\Core\AbstractFactory;


use Wt\Core\Factory\Shop\BasketFactory;
use Wt\Core\Factory\Shop\CatalogExportFactory;
use Wt\Core\Factory\Shop\CatalogFactory;
use Wt\Core\Factory\Shop\CatalogViewedProductFactory;
use Wt\Core\Factory\Shop\CompanyFactory;
use Wt\Core\Factory\Shop\CurrencyFactory;
use Wt\Core\Factory\Shop\ExtraFactory;
use Wt\Core\Factory\Shop\MeasureFactory;
use Wt\Core\Factory\Shop\PriceTypeFactory;
use Wt\Core\Factory\Shop\StoreFactory;
use Wt\Core\Factory\Shop\VatFactory;

class ShopAbstractFactory extends AAbstractFactory
{

    /**
     * @return CatalogFactory
     */
    public function getCatalogFactory()
    {
        return $this->getFactoryInstance(CatalogFactory::class);
    }

    /**
     * @return CatalogExportFactory
     */
    public function getCatalogExportFactory()
    {
        return $this->getFactoryInstance(CatalogExportFactory::class);
    }

    /**
     * @return StoreFactory
     */
    public function getStoreFactory()
    {
        return $this->getFactoryInstance(StoreFactory::class);
    }

    /**
     * @return MeasureFactory
     */
    public function getMeasureFactory()
    {
        return $this->getFactoryInstance(MeasureFactory::class);
    }

    /**
     * @return VatFactory
     */
    public function getVatFactory()
    {
        return $this->getFactoryInstance(VatFactory::class);
    }

    /**
     * @return PriceTypeFactory
     */
    public function getPriceTypeFactory()
    {
        return $this->getFactoryInstance(PriceTypeFactory::class);
    }

    /**
     * @return CompanyFactory
     */
    public function getCompanyFactory()
    {
        return $this->getFactoryInstance(CompanyFactory::class);
    }

    /**
     * @return CurrencyFactory
     */
    public function getCurrencyFactory()
    {
        return $this->getFactoryInstance(CurrencyFactory::class);
    }

    /**
     * @return ExtraFactory
     */
    public function getExtraFactory()
    {
        return $this->getFactoryInstance(ExtraFactory::class);
    }

    /**
     * @return BasketFactory
     */
    public function getBasketFactory()
    {
        return $this->getFactoryInstance(BasketFactory::class);
    }

    /**
     * @return CatalogViewedProductFactory
     */
    public function getCatalogViewedProductFactory()
    {
        return $this->getFactoryInstance(CatalogViewedProductFactory::class);
    }
}