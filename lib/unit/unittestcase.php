<?php


namespace Wt\Core\Unit;


use Wt\Core\Tools;

abstract class UnitTestCase
{
	private $passed = 0, $failed = 0, $total = 0;
	private $time_start;
	private $last_time;
	
	public $assertions = array();
	
	public function passed()
	{
		return $this->passed;
	}
	
	public function failed()
	{
		return $this->failed ? $this->failed : '0';
	}
	
	public function total()
	{
		return $this->total;
	}
	
	public function setup() {}
	
	public function teardown() {}
	
	private function stop_timer()
	{
		$time_stop = Tools::getMicroTime();

		$time_overall = number_format($time_stop - $this->time_start, 7);
						
		$this->last_time = $time_overall;
	}
		
	public function assertion($test, $message, $backtrace)
	{
		$pass = !!$test;
		++$this->total;
		
		if ( $pass ) 
			++$this->passed;
		else 
			++$this->failed;

		$this->log($pass, $message, $backtrace[1]['function']);
	}
	
	public function assertTrue($test, $message)
	{	
		$this->stop_timer();
		
		echo $this->value($test);
		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		echo 'actual: '.$this->value($test)."\n";

		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
			
		$this->assertion($test, $message, $backtrace);
	}
	
	public function assertFalse($test, $message)
	{
		$this->stop_timer();
		
		echo $this->value($test);
		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		echo 'actual: '.$this->value($test)."\n";
		
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
				
		return $this->assertion(!$test, $message, $backtrace);
	}
	
	public function assertEquals($expected, $actual, $message)
	{	
		$this->stop_timer();

		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		echo 'expected: '.$this->value($expected)."\n";
		echo 'actual: '.$this->value($actual)."\n";
		$result = $expected == $actual;
		
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = $result ? $message : $message;
		
		return $this->assertion($result, $message, $backtrace);
	}
	
	public function assertSame($expected, $actual, $message)
	{	
		$this->stop_timer();
        $backtrace = debug_backtrace();

		echo 'File: ' . basename($backtrace[0]['file']) . " [{$backtrace[0]['line']}] " . PHP_EOL;
		echo 'Message:<b> ' .$message.'</b>'.PHP_EOL;
		echo 'expected: '.$this->value($expected).PHP_EOL;
		echo 'actual: '.$this->value($actual).PHP_EOL;
		$result = $expected === $actual;

		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = $result ? $message : $message;
		
		return $this->assertion($result, $message, $backtrace);
	}

	public function assertNotSame($expected, $actual, $message)
	{	
		$this->stop_timer();
		
		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		echo 'expected: '.$this->value($expected)."\n";
		echo 'actual: '.$this->value($actual)."\n";
		$result = $expected !== $actual;
		
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = $result ? $message : $message;
		
		return $this->assertion($result, $message, $backtrace);
	}

	public function assertInstanceOf($expected, $actual, $message)
	{
		$this->stop_timer();

		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		if( is_object($expected) )
			echo 'expected: '.$this->value(get_class($expected))."\n";
		else
			echo 'expected: '.$this->value($expected)."\n";
		if( is_object($actual) )
			echo 'actual: '.$this->value(get_class($actual))."\n";
		else
			echo 'actual: '.$this->value($actual)."\n";
		
		$result = ($actual instanceof $expected) || $expected == get_class($actual);
		
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = $result ? $message : $message;

		return $this->assertion($result, $message, $backtrace);
	}

	public function assertNotInstanceOf($expected, $actual, $message)
	{
		$this->stop_timer();

        echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
        if( is_object($expected) )
            echo 'expected: '.$this->value(get_class($expected))."\n";
        else
            echo 'expected: '.$this->value($expected)."\n";
        if( is_object($actual) )
            echo 'actual: '.$this->value(get_class($actual))."\n";
        else
            echo 'actual: '.$this->value($actual)."\n";
		
		$result = !($actual instanceof $expected);
		
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = $result ? $message : $message;
		
		return $this->assertion($result, $message, $backtrace);
	}
	
	private function value($value)
	{
        return var_export($value, true);
		if( is_string($value) ){
			return $value;
		}
		
		if( is_array($value) || is_object($value) ){
			return print_r($value, TRUE);
		}

		if( is_bool($value) ){
			return ($value)?"true":"false";
		}

		if( $value === null ){
			return "<i>null</i>";
		}

		return $value;
	}
	
	public function assertNotEquals($expected, $actual, $message)
	{
		$this->stop_timer();
		
		echo "Message:<b> ".$message.'.</b>'.PHP_EOL;
		echo 'expected: '.$this->value($expected)."\n";
		echo 'actual: '.$this->value($actual)."\n";
	
		$result = $expected == $actual;
		$backtrace = debug_backtrace();
		$message = $backtrace[1]['function'].' "<strong>'.$message.'</strong>"';
		
		$message = !$result ? $message : $message;
		
		return $this->assertion(!$result, $message, $backtrace);
	}
	
	protected function log($pass, $message, $testName)
	{
		$console = $this->end_console();
		
		$this->assertions[] = array(
			'pass' => $pass ? 'pass' : 'fail',
			'message' => $message,
			'execution_time' => $this->last_time,
			'console' => strlen($console) ? $console : FALSE,
			'test_name' => $testName
		);
		
		$this->time_start = Tools::getMicroTime();
		$this->start_console();
	}
	
	protected function start_console()
	{
		ob_start();
	}
	
	protected function end_console()
	{
		$console = ob_get_contents();
		ob_end_clean();
		return $console;
	}
	
	public function run()
	{
		
		$array = get_class_methods($this);

		$array = array_filter($array, [$this, 'is_test']);
		
		foreach($array as $test) {
			$this->start_console();
			$this->setup();
			
			$this->time_start = Tools::getMicroTime();
			$this->$test();
			
			$this->teardown();

            $this->end_console();
		}
	}
	
	protected static function is_test($value)
	{
		return substr($value, 0, 4) == 'test';
	}
}