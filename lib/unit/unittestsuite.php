<?php


namespace Wt\Core\Unit;


use Wt\Core\Tools;

class UnitTestSuite
{
	private $cases = array();
	
	public $results = array();
	
	public function addTestCase(UnitTestCase $className)
	{
		$this->cases[] = $className;
	}
	
	public function run()
	{	
		$passed = 0;
		$failed = 0;
		$total = 0;

		$this->results = array(
			'total_passed' => 0,
			'total_failed' => 0,
			'total' => 0,
			'cases' => array()
		);
				
		foreach($this->cases as $testCase)
		{
			$time_start = Tools::getMicroTime();
			$testCaseObject = $testCase;
			$testCaseObject->run();
			$time_stop = Tools::getMicroTime();

			// $time_overall = bcsub($time_stop, $time_start, 6);
			$time_overall = number_format($time_stop - $time_start, 7);
			$passed += $testCaseObject->passed();
			$failed += $testCaseObject->failed();
			$total += $testCaseObject->total();
			$has_fails = $testCaseObject->failed() ? 'has_fails' : '';
			$has_passes = $testCaseObject->passed() ? 'has_passes' : '';

			$this->results['cases'][] = array(
				'name' => get_class($testCase),
				'execution_time' => $time_overall,
				'total_passed' => $testCaseObject->passed(),
				'total_failed' => $testCaseObject->failed(),
				'assertions' => $testCaseObject->assertions
			);
		}
		
		$this->results['total_passed'] += $passed;
		$this->results['total_failed'] += $failed;
		$this->results['total'] += $total;
	}

	public function passed()
    {
        return (int)$this->results['total_passed'];
    }

	public function failed()
    {
        return (int)$this->results['total_failed'];
    }

	public function total()
    {
        return (int)$this->results['total'];
    }

    public function loadByDir($dir)
    {
        $arFilesItems = glob($dir . DIRECTORY_SEPARATOR .'*Test.php');
        foreach ($arFilesItems as $file) {
            include_once $file;
            $file = basename($file);
            $clName = substr($file, 0, -4);
            $this->addTestCase(new $clName);
        }
        return $this;
    }

    public function render()
    {
        $unitTestSuite = $this;
        include __DIR__ . '/UnitTestSuite/templates/result.php';
    }
}