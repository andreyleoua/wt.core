<?php

use Wt\Core\Unit\UnitTestSuite;
/**
 * @var $unitTestSuite UnitTestSuite
 */


echo '<style>.closed .close-tests{display: none;}.action--click-showhide{cursor:pointer;}</style>';

echo "<div style='color:green;'>Total passed: <b>".$unitTestSuite->passed().'</b></div>';
echo "<div style='color:red;'>Total failed: <b>".$unitTestSuite->failed().'</b></div>';
foreach ($unitTestSuite->results['cases'] as $case) {
    echo '<div style="background-color:#eaeaea;" class="test-block"><b style="background-color:#ffa500;" class="action--click-showhide">'.$case['name'].'</b><div class="close-tests">';
    foreach ($case['assertions'] as $assertion) {
        if( $assertion['pass'] != 'pass' ){
            echo "&emsp;&emsp;<span><b>".$assertion['test_name'].'</b>: <span style="color:red;">failed</span></span><br>';
            echo "<pre style='padding:5px 40px 5px;margin:unset;background-color:#888;color:white;'>".$assertion['console'].'</pre>';
        } else {
            // print_r($assertion);
            echo "&emsp;&emsp;<span><b>".$assertion['test_name'].'</b>: <span style="color:green;">passed</span> ('.$assertion['console'].')</span><br>';
        }
    }
    echo "</div>";
    echo "<div style='color:green;'>&emsp;Total passed: <b>".$case['total_passed'].'</b></div>';
    echo "<div style='color:red;'>&emsp;Total failed: <b class='total-failed-in-block'>".$case['total_failed'].'</b></div>';
    echo "</div>";
}

?>

<script type="text/javascript">
    var failedsDom = document.querySelectorAll('.total-failed-in-block');
    [].forEach.call(failedsDom, (failedDom, i) => {
        var failedsTestsNum = parseInt(failedDom.innerText);
        var totalTestBlock = failedDom.closest('.test-block');
        if( failedsTestsNum === 0 ){
            totalTestBlock.classList = 'closed';
        }
        totalTestBlock.querySelector('.action--click-showhide').addEventListener('click', function(event){
            event.target.parentNode.classList.toggle('closed');
        });
    });
</script>