<?php


namespace Wt\Core\Property;


use Wt\Core\Assets\AssetsService;
use Wt\Core\Entity\APropertyEntity;
use Wt\Core\Entity\UserFieldEntity;
use Wt\Core\Entity\IBlock\PropertyEntity;
use Wt\Core\PropertyAdapter\APropertyAdapter;
use Wt\Core\PropertyAdapter\DefaultPropertyAdapter;
use Wt\Core\Templater\Templater;
use Wt\Core\Templater\TemplaterService;

class PropertyManager
{
    protected static $cid = 0;

    protected $profile;

    /**
     * @var TemplaterService
     */
    protected $templaterService;
    /**
     * @var AssetsService
     */
    protected $assetsService;

    public function __construct($profile, TemplaterService $templaterService, AssetsService $assetsService)
    {
        static::$cid++;
        $this->profile = $profile;
        $this->templaterService = $templaterService;
        $this->assetsService = $assetsService;
    }

    /**
     * @param APropertyEntity $propertyEntity
     * @return APropertyAdapter
     * @throws null
     */
    public function getAdapter2(APropertyEntity $propertyEntity)
    {

        $namespace = $this->templaterService->getNamespace();
        /**
         * @var Templater $templater
         */
        $templater = resolve(Templater::class, [$this->templaterService->getPath(), $namespace = 'kit', $propertyEntity]);

        $className = $this->getPropertyAdapterClassName($propertyEntity);

        $defaultPropertyAdapterParams = $this->profile->get(strtolower(DefaultPropertyAdapter::class), []);
        $propertyAdapterParams = $this->profile->get(strtolower($className), []);
        $propertyAdapterParams = $propertyAdapterParams->plus($defaultPropertyAdapterParams->all());

        return resolve($className, [$propertyEntity, $propertyAdapterParams, $templater, $this->assetsService]);
    }

    public function getAdapter(APropertyEntity $propertyEntity)
    {
        $className = $this->getPropertyAdapterClassName($propertyEntity);
        $id = $className . '_' . static::$cid . '_' . $propertyEntity->getId();

        if(app()->container()->hasIn($id)){
            return resolve($id);
        }
        $namespace = $this->templaterService->getNamespace();
        /**
         * @var Templater $templater
         */
        $templater = resolve(Templater::class, [$this->templaterService->getPath(), $namespace = 'kit', $propertyEntity]);


        $defaultPropertyAdapterParams = $this->profile->get(strtolower(DefaultPropertyAdapter::class), []);
        $propertyAdapterParams = $this->profile->get(strtolower($className), []);
        $propertyAdapterParams = $propertyAdapterParams->plus($defaultPropertyAdapterParams->all());

        $propertyAdapter = resolve($className, [$propertyEntity, $propertyAdapterParams, $templater, $this->assetsService]);

        app()->container()->singleton($id, $propertyAdapter);

        return resolve($id);
    }


    protected function getPropertyAdapterClassName(APropertyEntity $propertyEntity)
    {
        static $cache = [];

        $extName = $name = $propertyEntity->getFullType(true);

        if($cache[$name]){
            return $cache[$name];
        }
        /**
         * сделать дефаулты для каждого типа
         */
        if($propertyEntity instanceof UserFieldEntity){
            $extName = 'UserField\\' . $name;
        } elseif($propertyEntity instanceof PropertyEntity){
            $extName = 'IBlock\\' . $name;
        }

        $className = DefaultPropertyAdapter::class;
        $find = false;
        foreach (app()::getInstances() as $app){
            $_className = $app->module()->getNamespace() .
                '\\PropertyAdapter\\' .$extName . 'PropertyAdapter';
            if(class_exists($_className)){
                $className = $_className;
                $find = true;
                break;
            }
        }
        if(!$find){
            // pre("Class not exists {$extName}PropertyAdapter: {$propertyEntity->getName()}");
        }

        $cache[$name] = $className;

        return $className;
    }
}