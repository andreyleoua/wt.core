<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class IBlockElementMetaTemplateListProperty extends AIBlockTemplateProperty
{
	public static function OnIBlockPropertyBuildList()
	{
		return [
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'IBlockElementMetaTemplateList',
			'DESCRIPTION' => Loc::getMessage('WT_CORE_IBLOCK_ELEMENT_META_TEMPLATE_LIST_DESCRIPTION'),
			'ConvertFromDB' => [__CLASS__, 'ConvertFromDB'],
            'ConvertToDB' => [__CLASS__, 'ConvertToDB'],
			'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
			'GetPropertyFieldHtmlMulty' => [__CLASS__, 'GetPropertyFieldHtml'],
			'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
			'GetSettingsHTML' => [__CLASS__, 'GetSettingsHTML'],
			'PrepareSettings' => [__CLASS__, 'PrepareSettings'],
			//'GetPublicViewHTML' => [],
			//'GetPublicEditHTML' => [],
			//'GetAdminFilterHTML' => [],
			//'AddFilterFields' => [],
			//'GetUIFilterProperty' => [],
        ];
	}

	public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
        return '';
	}


    public static function ConvertToDB($arProperty, $value)
    {
        $return = false;

        if(
            is_array($value)
            && array_key_exists('VALUE', $value)
        )
        {
            $val = $value['VALUE'];
            $return = [
                'VALUE' => serialize($val),
            ];
            if (trim($value['DESCRIPTION']) != '')
                $return['DESCRIPTION'] = trim($value['DESCRIPTION']);
        }

        return $return;
    }

	public static function ConvertFromDB($arProperty, $value)
	{
        $return = $value;
        if (!is_array($value['VALUE']))
        {
            $return['VALUE'] = unserialize($value['VALUE']);
        }
        if (!is_array($return['VALUE']))
        {
            $return['VALUE'] = [];
        }

        return $return;
	}

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{

        if (!is_array($value['VALUE']))
            $value = static::ConvertFromDB($arProperty, $value);

		$bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
		$bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';

		if($bEditProperty || $bDetailPage){
			$iblockId = isset($arProperty['USER_TYPE_SETTINGS']) && isset($arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID']) ? $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'] : 0;
			if($iblockId){
				Loader::includeModule('iblock');
				require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/prolog.php');

				$entityType = isset($arProperty['USER_TYPE_SETTINGS']) && isset($arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE']) ? $arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE'] : 'E';

                /**
                 * @todo $arProperty['DEFAULT_VALUE']
                 */
                $data = [];
				if($bDetailPage){
				    foreach ($value['VALUE'] as $iCode => $iValue){
                        $data[$iCode] = [
                            'TEMPLATE' => $value && strlen($iValue) ? $iValue : (string)$arProperty['DEFAULT_VALUE'][$iCode],
                            'INHERITED' => $value && strlen($iValue) ? 'N' : 'Y',
                        ];
                    }
				}
				else{
                    foreach ($value['VALUE'] as $iCode => $iValue){
                        $data[$iCode] = [
                            'TEMPLATE' => $value && strlen($iValue) ? $iValue : '',
                            'INHERITED' => 'N',
                        ];
                    }
				}
				?>
				<?=static::iBlockInheritedPropertyInputListTemplate($iblockId, $arProperty['CODE'], $data, $entityType, ($bDetailPage ? Loc::getMessage('IBEL_E_SEO_OVERWRITE') : ''), $strHTMLControlName['VALUE'], $strHTMLControlName['VALUE'])?>
				<?
			}
			else{
				echo Loc::getMessage('IBINHERITED_PROP_ERROR_EMPTY_IBLOCK');
			}
		}
	}

	public static function PrepareSettings($arFields)
	{
		$arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE']) ? $arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE'] : 'E';

		$arFields['USER_TYPE_SETTINGS']['IBLOCK_ID'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['IBLOCK_ID']) ? intval($arFields['USER_TYPE_SETTINGS']['IBLOCK_ID']) : false;

		$arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID']) ? trim($arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID']) : false;

		$arFields['FILTRABLE'] = $arFields['SMART_FILTER'] = $arFields['SEARCHABLE'] = $arFields['MULTIPLE'] = $arFields['WITH_DESCRIPTION'] = 'N';
		$arFields['MULTIPLE_CNT'] = 1;

        return $arFields;
	}

	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
	{
		$arPropertyFields = array(
            'HIDE' => array(
            	'SMART_FILTER',
            	'FILTRABLE',
            	'SEARCHABLE',
            	'MULTIPLE_CNT',
            	'COL_COUNT',
            	'MULTIPLE',
            	'WITH_DESCRIPTION',
            	'FILTER_HINT',
            ),
            'SET' => array(
            	'SMART_FILTER' => 'N',
            	'FILTRABLE' => 'N',
            	'SEARCHABLE' => 'N',
            	'MULTIPLE_CNT' => '1',
            	'MULTIPLE' => 'N',
            	'WITH_DESCRIPTION' => 'N',
            	'DEFAULT_VALUE' => '',
            ),
        );

		$entityType = $arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE'];
		$html = '<tr><td width="40%">'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_TITLE').'</td>'.
			'<td><select name="'.$strHTMLControlName['NAME'].'[ENTITY_TYPE]"><option value="E" '.($entityType === 'E' ? 'selected' : '').'>'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_ELEMENT').'</option><option value="S" '.($entityType === 'S' ? 'selected' : '').'>'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_SECTION').'</option></select></td></tr>';

		$iblockId = $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'];
		$b_f = ($arProperty['PROPERTY_TYPE'] == 'G' || ($arProperty['PROPERTY_TYPE'] == 'E' && $arProperty['USER_TYPE'] == BT_UT_SKU_CODE) ? array('!ID' => $iblockId) : array());
		$html .= '<tr><td width="40%">'.Loc::getMessage('BT_ADM_IEP_PROP_LINK_IBLOCK').'</td>'.
			'<td>'.GetIBlockDropDownList($iblockId, $strHTMLControlName['NAME'].'[IBLOCK_TYPE_ID]', $strHTMLControlName['NAME'].'[IBLOCK_ID]', $b_f, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"').'</td></tr>';

		return $html;
	}

	protected static function iBlockInheritedPropertyInputListTemplate($iblock_id, $code, $data, $type, $checkboxLabel = '', $id = '', $name = '')
    {
        Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/iblock/admin/iblock_element_edit.php');
        $type = 'E';
        $r = '<table style="width: 100%;"><tr class="heading"><td>Настройки для элементов</td></tr></table>';
        $r .= static::iBlockInheritedPropertyInputTemplate($iblock_id, "ELEMENT_META_TITLE", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBEL_E_SEO_META_TITLE"));
        $r .= static::iBlockInheritedPropertyInputTemplate($iblock_id, "ELEMENT_META_KEYWORDS", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBEL_E_SEO_META_KEYWORDS"));
        $r .= static::iBlockInheritedPropertyInputTemplate($iblock_id, "ELEMENT_META_DESCRIPTION", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBEL_E_SEO_META_DESCRIPTION"));
        $r .= static::iBlockInheritedPropertyInputTemplate($iblock_id, "ELEMENT_PAGE_TITLE", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBEL_E_SEO_ELEMENT_TITLE"));
        //echo IBlockInheritedPropertyInput($iblock_id, "ELEMENT_META_TITLE", $data, $type, GetMessage("IBEL_E_SEO_OVERWRITE"));
        return $r;
    }

    protected static function iBlockInheritedPropertyInputTemplate($iblock_id, $iCode, $data, $type, $checkboxLabel = '', $id = '', $name = '', $title = '')
    {
        $result = '';
        if($title) {
            $result = '<span>'.$title.'</span><br>';
        }
        $result .= parent::iBlockInheritedPropertyInputTemplate($iblock_id, $iCode, $data, $type, $checkboxLabel, $id."[$iCode]", $name."[$iCode]");
        return $result;
    }

}
