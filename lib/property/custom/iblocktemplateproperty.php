<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class IBlockTemplateProperty extends AIBlockTemplateProperty
{
	public static function OnIBlockPropertyBuildList()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'IBlockTemplate',
			'DESCRIPTION' => Loc::getMessage('WT_CORE_IBLOCK_TEMPLATE_DESCRIPTION'),
			'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
			'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
			'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
			//'GetPublicViewHTML' => [],
			//'GetPublicEditHTML' => [],
			//'GetAdminFilterHTML' => [],
			//'AddFilterFields' => [],
			//'GetUIFilterProperty' => [],
		);
	}

	public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
        return '';
	}

	public static function ConvertFromDB($arProperty, $value)
	{
		return $value;
	}

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
		$bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';

		if($bEditProperty || $bDetailPage){
			$iblockId = isset($arProperty['USER_TYPE_SETTINGS']) && isset($arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID']) ? $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'] : 0;
			if($iblockId){
				Loader::includeModule('iblock');
				require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/prolog.php');

				$entityType = isset($arProperty['USER_TYPE_SETTINGS']) && isset($arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE']) ? $arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE'] : 'E';

				if($bDetailPage){
					$str_IPROPERTY_TEMPLATES = array(
						$arProperty['CODE'] => array(
							'TEMPLATE' => $value && strlen($value['VALUE']) ? $value['VALUE'] : $arProperty['DEFAULT_VALUE'],
							'INHERITED' => $value && strlen($value['VALUE']) ? 'N' : 'Y',
						)
					);
				}
				else{
					$str_IPROPERTY_TEMPLATES = array(
						$arProperty['CODE'] => array(
							'TEMPLATE' => $value && strlen($value['VALUE']) ? $value['VALUE'] : '',
							'INHERITED' => 'N',
						)
					);
				}
				?>
				<?=self::iBlockInheritedPropertyInputTemplate($iblockId, $arProperty['CODE'], $str_IPROPERTY_TEMPLATES, $entityType, ($bDetailPage ? Loc::getMessage('IBEL_E_SEO_OVERWRITE') : ''), $strHTMLControlName['VALUE'], $strHTMLControlName['VALUE'])?>
				<?
			}
			else{
				echo Loc::getMessage('IBINHERITED_PROP_ERROR_EMPTY_IBLOCK');
			}
		}
	}

	public static function PrepareSettings($arFields)
	{
		$arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE']) ? ($arFields['USER_TYPE_SETTINGS']['ENTITY_TYPE']) : 'E';

		$arFields['USER_TYPE_SETTINGS']['IBLOCK_ID'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['IBLOCK_ID']) ? intval($arFields['USER_TYPE_SETTINGS']['IBLOCK_ID']) : false;

		$arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID'] = isset($arFields['USER_TYPE_SETTINGS']) && isset($arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID']) ? trim($arFields['USER_TYPE_SETTINGS']['IBLOCK_TYPE_ID']) : false;

		$arFields['FILTRABLE'] = $arFields['SMART_FILTER'] = $arFields['SEARCHABLE'] = $arFields['MULTIPLE'] = $arFields['WITH_DESCRIPTION'] = 'N';
		$arFields['MULTIPLE_CNT'] = 1;

        return $arFields;
	}

	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
	{
		$arPropertyFields = array(
            'HIDE' => array(
            	'SMART_FILTER',
            	'FILTRABLE',
            	'SEARCHABLE',
            	'MULTIPLE_CNT',
            	'COL_COUNT',
            	'MULTIPLE',
            	'WITH_DESCRIPTION',
            	'FILTER_HINT',
            ),
            'SET' => array(
            	'SMART_FILTER' => 'N',
            	'FILTRABLE' => 'N',
            	'SEARCHABLE' => 'N',
            	'MULTIPLE_CNT' => '1',
            	'MULTIPLE' => 'N',
            	'WITH_DESCRIPTION' => 'N',
            	'DEFAULT_VALUE' => '',
            ),
        );

		$entityType = $arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE'];
		$html = '<tr><td width="40%">'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_TITLE').'</td>'.
			'<td><select name="'.$strHTMLControlName['NAME'].'[ENTITY_TYPE]"><option value="E" '.($entityType === 'E' ? 'selected' : '').'>'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_ELEMENT').'</option><option value="S" '.($entityType === 'S' ? 'selected' : '').'>'.Loc::getMessage('IBINHERITED_PROP_ENTITY_TYPE_SECTION').'</option></select></td></tr>';

		$iblockId = $arProperty['USER_TYPE_SETTINGS']['IBLOCK_ID'];
		$b_f = ($arProperty['PROPERTY_TYPE'] == 'G' || ($arProperty['PROPERTY_TYPE'] == 'E' && $arProperty['USER_TYPE'] == BT_UT_SKU_CODE) ? array('!ID' => $iblockId) : array());
		$html .= '<tr><td width="40%">'.Loc::getMessage('BT_ADM_IEP_PROP_LINK_IBLOCK').'</td>'.
			'<td>'.GetIBlockDropDownList($iblockId, $strHTMLControlName['NAME'].'[IBLOCK_TYPE_ID]', $strHTMLControlName['NAME'].'[IBLOCK_ID]', $b_f, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"').'</td></tr>';

		return $html;
	}

}
