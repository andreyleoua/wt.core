<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader;

class AIBlockTemplateProperty
{

	protected static function iBlockInheritedPropertyInputTemplate($iblock_id, $code, $data, $type, $checkboxLabel = '', $id = '', $name = '')
	{
	    $inherited = ($data[$code]["INHERITED"] !== "N") && ($checkboxLabel !== "");
	    $inputId = $id ? $id : "IPROPERTY_TEMPLATES_".$code;
	    $inputName = $name ? $name : "IPROPERTY_TEMPLATES[".$code."][TEMPLATE]";
	    $menuId = "mnu_".$inputId;
	    $resultId = "result_".$inputId;
	    $checkboxId = "ck_".$inputId;

	    if ($type === "S"){
	        $menuItems = \CIBlockParameters::GetInheritedPropertyTemplateSectionMenuItems($iblock_id, "InheritedPropertiesTemplates.insertIntoInheritedPropertiesTemplate", $menuId, $inputId);
	    }
	    else{
	        $menuItems = \CIBlockParameters::GetInheritedPropertyTemplateElementMenuItems($iblock_id, "InheritedPropertiesTemplates.insertIntoInheritedPropertiesTemplate", $menuId, $inputId);
	    }

	    $menuItems[count($menuItems) - 1]['MENU'][] = array(
	    	'TEXT' => Loc::getMessage('IBINHERITED_PROP_MENU_ITEM_IPV_TITLE'),
	    	'ONCLICK' => 'InheritedPropertiesTemplates.insertIntoInheritedPropertiesTemplate(\'{=this.inheritedproperty}\', \''.$menuId.'\', \''.$inputName.'\')',
	    );

	    $u = new \CAdminPopupEx($menuId, $menuItems, array("zIndex" => 2000));
	    $result = $u->Show(true)
	        .'<script>
	            window.ipropTemplates[window.ipropTemplates.length] = {
	            "ID": "'.$code.'",
	            "INPUT_ID": "'.$inputId.'",
	            "RESULT_ID": "'.$resultId.'",
	            "TEMPLATE": ""
	            };
	        </script>'
	        .'<input type="hidden" name="'.$inputName.'" value="'.htmlspecialcharsbx($data[$code]["TEMPLATE"]).'" />'
	        .'<textarea onclick="InheritedPropertiesTemplates.enableTextArea(\''.$inputId.'\')" name="'.$inputName.'" id="'.$inputId.'" '.($inherited? 'readonly="readonly"': '').' cols="55" rows="1" style="width:calc(100% - 60px)">'
	        .htmlspecialcharsbx($data[$code]["TEMPLATE"])
	        .'</textarea>'
	        .'<input style="float:right;margin-top: 0;height: 26px;" type="button" id="'.$menuId.'" '.($inherited? 'disabled="disabled"': '').' value="...">'
	        .'<br>'
	    ;

	    if ($checkboxLabel != "")
	    {
	        $result .= '<div style="display:none;"><input type="hidden" name="'.$checkboxId.'[INHERITED]" value="Y">'
	            .'<input type="checkbox" name="'.$checkboxId.'[INHERITED]" id="'.$checkboxId.'" value="N" '
	            .'onclick="InheritedPropertiesTemplates.updateInheritedPropertiesTemplates()" '.(!$inherited? 'checked="checked"': '').'>'
	            .'<label for="'.$checkboxId.'">'.$checkboxLabel.'</label><br></div>'
	        ;
	    }

	    if (preg_match("/_FILE_NAME\$/", $code))
	    {
	        $result .= '<input type="hidden" name="IPROPERTY_TEMPLATES['.$code.'][LOWER]" value="N">'
	            .'<input type="checkbox" name="IPROPERTY_TEMPLATES['.$code.'][LOWER]" id="lower_'.$code.'" value="Y" '
	            .'onclick="InheritedPropertiesTemplates.enableTextArea(\''.$inputId.'\');InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true)" '.($data[$code]["LOWER"] !== "Y"? '': 'checked="checked"').'>'
	            .'<label for="lower_'.$code.'">'.Loc::getMessage("IBLOCK_AT_FILE_NAME_LOWER").'</label><br>'
	        ;
	        $result .= '<input type="hidden" name="IPROPERTY_TEMPLATES['.$code.'][TRANSLIT]" value="N">'
	            .'<input type="checkbox" name="IPROPERTY_TEMPLATES['.$code.'][TRANSLIT]" id="translit_'.$code.'" value="Y" '
	            .'onclick="InheritedPropertiesTemplates.enableTextArea(\''.$inputId.'\');InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true)" '.($data[$code]["TRANSLIT"] !== "Y"? '': 'checked="checked"').'>'
	            .'<label for="translit_'.$code.'">'.Loc::getMessage("IBLOCK_AT_FILE_NAME_TRANSLIT").'</label><br>'
	        ;
	        $result .= '<input size="2" maxlength="1" type="text" name="IPROPERTY_TEMPLATES['.$code.'][SPACE]" id="space_'.$code.'" value="'.htmlspecialcharsbx($data[$code]["SPACE"]).'" '
	            .'onchange="InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true)">'.Loc::getMessage("IBLOCK_AT_FILE_NAME_SPACE").'<br>'
	        ;
	    }
	    $result .= '<b><div id="'.$resultId.'"></div></b>';

	    return $result;
	}

}
