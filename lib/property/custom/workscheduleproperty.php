<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;
use Bitrix\Main\Web\Json;
use CAdminCalendar;

Loc::loadMessages(__FILE__);

class WorkScheduleProperty extends AIBlockTemplateProperty
{
    public static function OnIBlockPropertyBuildList()
    {
        return array(
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'WorkSchedule',
            'DESCRIPTION' => app()->module()->getMessage('WORK_SCHEDULE_PROPERTY_DESCRIPTION'),
            'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
            'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
            //'GetPublicViewHTML' => [],
            //'GetPublicEditHTML' => [],
            //'GetAdminFilterHTML' => [],
            //'AddFilterFields' => [],
            //'GetUIFilterProperty' => [],
        );
    }

    public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return '';
    }

    public static function ConvertToDB($arProperty, $value)
    {
        $value['VALUE'] = Json::encode($value['VALUE']);

        return $value;
    }

    public static function ConvertFromDB($arProperty, $value)
    {
        if(!is_array($value['VALUE'])){
            $value['VALUE'] = strlen($value['VALUE']) ? $value['VALUE'] : '[]';

            try {
                $value['VALUE'] = Json::decode($value['VALUE']);
            }
            catch(\Exception $e) {
                $value['VALUE'] = [];
            }
        }
        return $value;
    }

    public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
        $bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';
        $fieldNamePrefix = $strHTMLControlName['VALUE'];

        $val = $value['VALUE'];

        $note = '<div class="adm-info-message-wrap"><div class="adm-info-message" style="margin-top: 0;">'.app()->module()->getMessage('WORK_SCHEDULE_PROPERTY_NOTE').'</div></div>';

        return $note . '<table><tr><th></th><td>от</td><td>до</td></tr>'.
                static::getWorkDayTemplate($fieldNamePrefix, 1, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 2, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 3, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 4, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 5, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 6, $val).
                static::getWorkDayTemplate($fieldNamePrefix, 0, $val).
            '</table>' .
            ($arProperty["WITH_DESCRIPTION"]=="Y" && '' != trim($strHTMLControlName["DESCRIPTION"]) ?
                '&nbsp;<input type="text" size="20" name="'.$strHTMLControlName["DESCRIPTION"].'" value="'.htmlspecialcharsbx($value["DESCRIPTION"]).'">'
                :''
            );
    }

    protected static function getWorkDayTemplate($fieldNamePrefix, $day, $val)
    {
        $dayLang = GetMessage('DOW_'.$day);
        $to = htmlspecialcharsbx($val[$day]['TO']);
        $from = htmlspecialcharsbx($val[$day]['FROM']);
        $isWeekendCheckedAttr = ($val[$day]['IS_WEEKEND'] == 'Y')?' checked':'';

        $checkbox = '<th><input type="hidden" name="{$fieldNamePrefix}[{$day}][IS_WEEKEND]" value="N"><input onchange="" type="checkbox" name="{$fieldNamePrefix}[{$day}][IS_WEEKEND]" value="Y" $isWeekendCheckedAttr></th>';
        return <<<HTML
<tr>
    <th>$dayLang</th>
    <td><input class="adm-input adm-input-calendar" type="text" name="{$fieldNamePrefix}[{$day}][FROM]" size="3" value="$from"></td>
    <td><input class="adm-input adm-input-calendar" type="text" name="{$fieldNamePrefix}[{$day}][TO]" size="3" value="$to"></td>
</tr>
HTML;
    }

    public static function PrepareSettings($arFields)
    {
        $arFields['FILTRABLE'] = $arFields['SMART_FILTER'] = $arFields['SEARCHABLE'] = $arFields['MULTIPLE'] = $arFields['WITH_DESCRIPTION'] = 'N';
        $arFields['MULTIPLE_CNT'] = 1;

        return $arFields;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            'HIDE' => array(
                'SMART_FILTER',
                'FILTRABLE',
                'SEARCHABLE',
                'MULTIPLE_CNT',
                'COL_COUNT',
                'MULTIPLE',
                'WITH_DESCRIPTION',
                'FILTER_HINT',
            ),
            'SET' => array(
                'SMART_FILTER' => 'N',
                'FILTRABLE' => 'N',
                'SEARCHABLE' => 'N',
                'MULTIPLE_CNT' => '1',
                'MULTIPLE' => 'N',
                'WITH_DESCRIPTION' => 'N',
                'DEFAULT_VALUE' => '',
            ),
        );

        $html = '';

        return $html;
    }

}
