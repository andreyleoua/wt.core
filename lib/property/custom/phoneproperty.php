<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader,
	Bitrix\Main\Web\Json,
    Wt\Core\Property\Custom\PhoneProperty\Iconset;

Loc::loadMessages(__FILE__);

class PhoneProperty
{
	public static function onIBlockPropertyBuildList()
    {
		return [
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'Phone',
            'DESCRIPTION' => app()->module()->getMessage('PHONE_PROPERTY_DESCRIPTION'),
			'PrepareSettings' => [__CLASS__, 'prepareSettings'],
			'GetSettingsHTML' => [__CLASS__, 'getSettingsHTML'],
			'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
			'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
			'ConvertToDB' => [__CLASS__, 'ConvertToDB'],
			'ConvertFromDB' => [__CLASS__, 'ConvertFromDB'],
        ];
	}

	public static function prepareSettings($arFields){
		$arFields['FILTRABLE'] = $arFields['SMART_FILTER'] = $arFields['SEARCHABLE'] = 'N';
		$arFields['MULTIPLE_CNT'] = 1;

        return $arFields;
	}

	public static function getSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields){
		$arPropertyFields = [
            'HIDE' => [
            	'SMART_FILTER',
            	'FILTRABLE',
            	//'DEFAULT_VALUE',
            	'SEARCHABLE',
            	'COL_COUNT',
            	'FILTER_HINT',
            ],
            'SET' => [
            	'SMART_FILTER' => 'N',
            	'FILTRABLE' => 'N',
            	'SEARCHABLE' => 'N',
            	'MULTIPLE_CNT' => '1',
            ],
        ];

		return '';
	}

	private static function setAssets()
    {
        static $cache;

        if(isset($cache)) {
            return;
        }

        $cache = [];

        Loader::includeModule('fileman');

        $moduleID = self::getModuleId();

        $src = \Wt\Core\Tools::removeDocRoot(__DIR__) . '/phoneproperty/assets';
        $srcModule = \Wt\Core\Tools::removeDocRoot(app()->module()->fields()->dir);

        $GLOBALS['APPLICATION']->AddHeadScript($src.'/sort/Sortable.js');
        \CJSCore::RegisterExt('iconset', [
            'js' => $src.'/iconset.js',
            'css' => $src.'/iconset.css',
            'lang' => $srcModule.'/lang/'.LANGUAGE_ID.'/lib/property/custom/phoneproperty/iconset.php',
        ]);

        \CJSCore::RegisterExt('regionphone', [
            'js' => $src.'/property/regionphone.js',
            'css' => $src.'/property/regionphone.css',
            'lang' => $srcModule.'/lang/'.LANGUAGE_ID.'/lib/property/custom/phoneproperty.php',
        ]);

        \CJSCore::Init(['iconset', 'regionphone', 'file_input']);
    }

	public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){

		$bAdminList = $strHTMLControlName['MODE'] === 'iblock_element_admin';
		$bMultiple = $arProperty['MULTIPLE'] === 'Y';
		$bWithDescription = $arProperty['WITH_DESCRIPTION'] === 'Y';

        self::setAssets();

		//$value = self::ConvertFromDB($arProperty, $value);

		ob_start();

		if($value){
			$icon = $value['VALUE']['ICON'];
			$iconName = $strHTMLControlName['VALUE'].'[ICON]';
			$phone = $value['VALUE']['PHONE'];
			$phoneName = $strHTMLControlName['VALUE'].'[PHONE]';
			$href = $value['VALUE']['HREF'];
			$hrefName = $strHTMLControlName['VALUE'].'[HREF]';
			$description = $value['DESCRIPTION'];
			$descriptionName = str_replace('VALUE', 'DESCRIPTION', $strHTMLControlName['VALUE']);
		}
		?>
        <span class="">
            <?if(0 && $icon):?>
                <span class=""><?=Iconset::showIcon($icon)?></span>
            <?endif;?>
            <span class=""><a href="<?=htmlspecialcharsbx($href)?>"><?=htmlspecialcharsbx($phone)?> <?if($description && $bWithDescription):?>(<?=$description?>)<?endif;?></a></span>

        </span>
		<?

		return ob_get_clean();
	}

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName){
		static $cache = [];

		$bAdminList = $strHTMLControlName['MODE'] === 'iblock_element_admin';
		$bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
		$bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';
		$bMultiple = $arProperty['MULTIPLE'] === 'Y';
		$bWithDescription = $arProperty['WITH_DESCRIPTION'] === 'Y';

        self::setAssets();

		//$value = self::ConvertFromDB($arProperty, $value);

		if($bAdminList){
			preg_match('/FIELDS\[([\D]+)(\d+)\]\[PROPERTY_'.$arProperty['ID'].'\]\[([^\]]*)\]\[VALUE\]/', $strHTMLControlName['VALUE'], $arMatch);
			$elementType = $arMatch[1];
			$elementId = $arMatch[2];
			$valueId = $arMatch[3];
			$tableId = 'tb'.md5($elementType.$elementId.':'.$arProperty['ID']);

			if(!$valueId){
				return '';
			}
		}
		else{
			preg_match('/PROP\['.$arProperty['ID'].'\]\[([^\]]*)\]\[VALUE\]/', $strHTMLControlName['VALUE'], $arMatch);
			$valueId = $arMatch[1];
			if($bEditProperty){
				$tableId = 'form_content';
			}
			else{
				$tableId = 'tb'.md5(htmlspecialcharsbx('PROP['.$arProperty['ID'].']'));
			}
		}

		if($value){
			$icon = is_array($value['VALUE']) ? $value['VALUE']['ICON'] : '';
			$iconName = $strHTMLControlName['VALUE'].'[ICON]';
			$phone = is_array($value['VALUE']) ? $value['VALUE']['PHONE'] : '';
			$phoneName = $strHTMLControlName['VALUE'].'[PHONE]';
			$href = is_array($value['VALUE']) ? $value['VALUE']['HREF'] : '';
			$hrefName = $strHTMLControlName['VALUE'].'[HREF]';
			$description = $value['DESCRIPTION'];
			$descriptionName = str_replace('VALUE', 'DESCRIPTION', $strHTMLControlName['VALUE']);
		}

		ob_start();
		?>
		<?if($bAdminList ? !in_array($elementId, $cache) : !in_array($arProperty['ID'], $cache)):?>
			<?
			if($bAdminList){
				$cache[] = $elementId;
			}
			else{
				$cache[] = $arProperty['ID'];
			}

			$GLOBALS['APPLICATION']->AddHeadString('<script>BX.ready(function(){new JRegionPhone(\''.$tableId.'\');});</script>');
			?>
			<?if($bEditProperty):?>
				<table><tbody><tr><td>
			<?endif;?>
		<?endif;?>
		<div class="aspro_property_regionphone_item<?=($bAdminList ? ' aspro_property_regionphone_item--admlistedit' : '')?>">
			<div class="wrapper">
				<div class="inner_wrapper">
					<div class="inner">
						<div class="value_wrapper">
							<input type="text" name="<?=$phoneName?>" value="<?=htmlspecialcharsbx($phone)?>" maxlength="255" placeholder="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_PHONE_TITLE'))?>" title="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_PHONE_TITLE'))?>" />
						</div>
					</div>
					<div class="inner">
						<div class="value_wrapper">
							<input type="text" name="<?=$hrefName?>" value="<?=htmlspecialcharsbx($href)?>" maxlength="255" placeholder="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_HREF_TITLE'))?>" title="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_HREF_TITLE'))?>" />
						</div>
					</div><br />
					<div class="inner">
						<div class="value_wrapper">
							<div class="iconset_value" data-code="header_phones" title="<?=app()->module()->getMessage('PHONE_ICON_TITLE')?>"><div class="iconset_value_wrap"><?=Iconset::showIcon($icon)?></div><input type="hidden" value="<?=htmlspecialcharsbx($icon)?>" name="<?=$iconName?>"></div>
						</div>
					</div>
					<div class="inner">
						<div class="value_wrapper">
							<?if($bWithDescription):?>
								<input type="text" name="<?=$descriptionName?>" value="<?=htmlspecialcharsbx($description)?>" maxlength="255" placeholder="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_DESCRIPTION_TITLE'))?>" title="<?=htmlspecialcharsbx(app()->module()->getMessage('PHONE_DESCRIPTION_TITLE'))?>" <?=($bEditProperty ? 'readonly disabled' : '')?> />
							<?else:?>
								<div class="description_disabled_note"><?=app()->module()->getMessage('PHONE_DESCRIPTION_FIELD_DISABLED_NOTE');?></div>
							<?endif;?>
						</div>
					</div>
					<?if(!$bEditProperty):?>
						<div class="remove" title="<?=app()->module()->getMessage('PHONE_DELETE_TITLE')?>"></div>
						<div class="drag" title="<?=app()->module()->getMessage('PHONE_DRAG_TITLE')?>"></div>
					<?endif;?>
				</div>
			</div>
		</div>
		<?if($bEditProperty):?>
			</td></tr></tbody></table>
		<?endif;?>
		<?

		return ob_get_clean();
	}

	public static function ConvertToDB($arProperty, $value){
		if(
			!is_array($value['VALUE']) ||
			!strlen($value['VALUE']['PHONE'])
		){
			return [
				'VALUE' => '',
				'DESCRIPTION' => '',
            ];
		}

		$value['VALUE'] = [
			'ICON' => strlen($value['VALUE']['ICON']) ? $value['VALUE']['ICON'] : '',
			'PHONE' => strlen($value['VALUE']['PHONE']) ? $value['VALUE']['PHONE'] : '',
			'HREF' => strlen($value['VALUE']['HREF']) ? $value['VALUE']['HREF'] : '',
        ];
		$value['VALUE'] = Json::encode($value['VALUE']);

		return $value;
	}

	public static function ConvertFromDB($arProperty, $value){
		if(!is_array($value['VALUE'])){
			$value['VALUE'] = strlen($value['VALUE']) ? $value['VALUE'] : '[]';
			
			try {
				$value['VALUE'] = Json::decode($value['VALUE']);
			}
			catch(\Exception $e) {
				$value['VALUE'] = [];
			}
		}

		if(
			!$value['VALUE'] ||
			!is_array($value['VALUE']) ||
			!strlen($value['VALUE']['PHONE'])
		){
			$value['VALUE'] = [
				'ICON' => '',
				'PHONE' => '',
				'HREF' => '',
            ];
		}

		return $value;
	}

	public static function getDefaultValue($arProperty){
		if(!is_array($arProperty['DEFAULT_VALUE'])){
			$defaultValue = strlen($arProperty['DEFAULT_VALUE']) ? $arProperty['DEFAULT_VALUE'] : '[]';

			try {
				$arProperty['DEFAULT_VALUE'] = Json::decode($defaultValue);
			}
			catch(\Exception $e) {
				$arProperty['DEFAULT_VALUE'] = [];
			}
		}

		if(
			!$arProperty['DEFAULT_VALUE'] ||
			!is_array($arProperty['DEFAULT_VALUE']) ||
			!strlen($arProperty['DEFAULT_VALUE']['PHONE'])
		){
			$arProperty['DEFAULT_VALUE'] = [
				'ICON' => '',
				'PHONE' => '',
				'HREF' => '',
            ];
		}

		return $arProperty['DEFAULT_VALUE'];
	}

	public static function getModuleId()
    {
		return app()->module()->getId();
	}
}
