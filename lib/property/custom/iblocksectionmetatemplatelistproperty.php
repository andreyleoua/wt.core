<?php


namespace Wt\Core\Property\Custom;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class IBlockSectionMetaTemplateListProperty extends IBlockElementMetaTemplateListProperty
{
	public static function OnIBlockPropertyBuildList()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'IBlockSectionMetaTemplateList',
			'DESCRIPTION' => Loc::getMessage('WT_CORE_IBLOCK_SECTION_META_TEMPLATE_LIST_DESCRIPTION'),
			'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtml'),
			'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
			'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
			'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
			//'GetPublicViewHTML' => [],
			//'GetPublicEditHTML' => [],
			//'GetAdminFilterHTML' => [],
			//'AddFilterFields' => [],
			//'GetUIFilterProperty' => [],
		);
	}


	protected static function iBlockInheritedPropertyInputListTemplate($iblock_id, $code, $data, $type, $checkboxLabel = '', $id = '', $name = '')
    {
        Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/iblock/admin/iblock_section_edit.php');
        $type = 'S';
        $r = '<table style="width: 100%;"><tr class="heading"><td>Настройки для разделов</td></tr></table>';
        $r .= self::iBlockInheritedPropertyInputTemplate($iblock_id, "SECTION_META_TITLE", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBSEC_E_SEO_META_TITLE"));
        $r .= self::iBlockInheritedPropertyInputTemplate($iblock_id, "SECTION_META_KEYWORDS", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBSEC_E_SEO_META_KEYWORDS"));
        $r .= self::iBlockInheritedPropertyInputTemplate($iblock_id, "SECTION_META_DESCRIPTION", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBSEC_E_SEO_META_DESCRIPTION"));
        $r .= self::iBlockInheritedPropertyInputTemplate($iblock_id, "SECTION_PAGE_TITLE", $data, $type, $checkboxLabel, $id, $name, GetMessage("IBSEC_E_SEO_SECTION_PAGE_TITLE"));
        return $r;
    }

}
