<?php

namespace Wt\Core\Property\UserField;

class HtmlUserField extends AUserField
{
    public static function getUserTypeDescription()
    {
        return [
            'USER_TYPE_ID' => 'html',
            'CLASS_NAME' => 'Wt\Core\Property\UserField\HtmlUserField',
            'DESCRIPTION' => 'Bx.App: Html (multiple)',
            'BASE_TYPE' => 'string',
        ];
    }

    public static function getDBColumnType($arUserField)
    {
        switch(strtolower(bitrix()->gDb()->type)) {
            case 'mssql':
            case 'mysql':
            case 'oracle':
                return 'longtext';
                break;
        }
        return 'longtext';
    }

    public static function getEditFormHTML($arUserField, $arHtmlControl, $bVarsFromForm = false)
    {
        if($arUserField['ENTITY_VALUE_ID']<1 && strlen($arUserField['SETTINGS']['DEFAULT_VALUE'])>0)
            $arHtmlControl['VALUE'] = htmlspecialcharsbx($arUserField['SETTINGS']['DEFAULT_VALUE']);
        if($arUserField['SETTINGS']['ROWS'] < 8)
            $arUserField['SETTINGS']['ROWS'] = 8;

        if($arUserField['MULTIPLE'] == 'Y')
            $name = preg_replace('/[\[\]]/i', '_', $arHtmlControl['NAME']);
        else
            $name = $arHtmlControl['NAME'];

        ob_start();

        \CFileMan::AddHTMLEditorFrame(
            $name,
            $arHtmlControl['VALUE'],
            $name. '_TYPE',
            strlen($arHtmlControl['VALUE'])? 'html' : 'text',
            [
                'height' => $arUserField['SETTINGS']['ROWS']*10,
            ]
        );
        if(0) {
            ?>
            <script>
                BX.fireEvent(BX('bxed_<?=$name?>_editor'), 'click');
                BX.fireEvent(BX('bxed_<?=$name?>_text'), 'click');
            </script>
            <?
        }

        if($arUserField['MULTIPLE'] == 'Y')
            echo '<input type="hidden" name="'.$arHtmlControl['NAME'].'" >';

        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    public static function onBeforeSave($arUserField, $value, $user_id = 0)
    {
        if($arUserField['MULTIPLE'] == 'Y')
        {
            foreach($_POST as $key => $val)
            {
                if( preg_match('/' .$arUserField['FIELD_NAME']. '_([0-9]+)_$/i', $key, $m) )
                {
                    $value = $val;
                    unset($_POST[$key]);
                    break;
                }
            }
        }
        return $value;
    }
}