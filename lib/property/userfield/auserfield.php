<?php

namespace Wt\Core\Property\UserField;

/**
 * @see \CUserTypeManager
 * @see \Bitrix\Main\UserField\Types\BaseType
 * @see \Bitrix\Main\UserField\Types\StringType
 */
abstract class AUserField
{
    abstract public static function getUserTypeDescription();
    abstract public static function getDBColumnType($arUserField);
    abstract public static function getEditFormHTML($arUserField, $arHtmlControl, $bVarsFromForm = false);

//    public static function prepareSettings(array $userField): array
//    {
//        return [];
//    }
//    public static function onBeforeSave($arUserField, $value, $user_id = 0)
//    {
//        return $value;
//    }
//
//    public static function onBeforeSaveAll($arUserField, $values, $user_id = 0)
//    {
//        return $values;
//    }
}