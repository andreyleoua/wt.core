## Формирование значений свойств

### Property value

* Storage 
* Restored
* Value
* Entity



* PropertyEntity->getRestoredValue($rawValue) array keys: VALUE
* Adapter->getValue(array $restoredValue) на базе Adapter->getValueConvertFromDB()
* Adapter->getValueEntity()



## Типы свойств

* для hlblock

    userTypes

        enumeration - Список
        double - Число
        integer - Целое число
        boolean - Да/Нет
        string - Строка
        file - Файл
        video - Видео
        iblock_section - Привязка к разделам инф. блоков
        iblock_element - Привязка к элементам инф. блоков
        string_formatted - Шаблон
        crm - Привязка к элементам CRM
        crm_status - Привязка к справочникам CRM
        address - Адрес
        resourcebooking - Бронирование ресурсов
        date - Дата
        datetime - Дата со временем
        money - Деньги
        vote - Опрос
        mail_message - Письмо (email)
        hlblock - Привязка к элементам highload-блоков
        url_preview - Содержимое ссылки
        url — Ссылка

* для iblock

    types

        * S - строка
        * N - число
        * L - список
        * F - файл
        * G - привязка к разделу
        * E - привязка к элементу
        
    userTypes

       * UserID - Привязка к пользователю
       * DateTime - Дата/Время
       * EList - Привязка к элементам в виде списка
       * FileMan - Привязка к файлу (на сервере)
       * map_yandex - Привязка к Яndex.Карте
       * HTML - HTML/текст
       * map_google - Привязка к карте Google Maps
       * ElementXmlID - Привязка к элементам по XML_ID
       * Sequence - Счетчик
       * EAutocomplete - Привязка к элементам с автозаполнением
       * SKU - Привязка к товарам (SKU)
       * video - Видео
       * TopicID - Привязка к теме форума
     

Типы (абстрактные) колонок таблицы БД - column_type:
Типы свойств - property_type:
Типы FormFields - field_type:



    PropertyValueEntity
        ->getStoredValue() // значение которое используется для сохранения в БД