<?php

namespace Wt\Core\Property\IBlock;

abstract class AProperty
{
    abstract public static function onIBlockPropertyBuildList();
}