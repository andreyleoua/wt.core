<?php

namespace Wt\Core\Property\IBlock;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;

Loc::loadMessages(__FILE__);

class FactoryProperty extends AProperty
{
    public static function onIBlockPropertyBuildList()
    {
        return [
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'iblock_factory',
            'DESCRIPTION' => 'Bx.App: Factory',
            'ConvertFromDB' => [__CLASS__, 'ConvertFromDB'],
            'ConvertToDB' => [__CLASS__, 'ConvertToDB'],
            'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
            'GetPropertyFieldHtmlMulty' => [__CLASS__, 'GetPropertyFieldHtml'],
            'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
            'GetSettingsHTML' => [__CLASS__, 'GetSettingsHTML'],
            'PrepareSettings' => [__CLASS__, 'PrepareSettings'],
            //'GetPublicViewHTML' => [],
            //'GetPublicEditHTML' => [],
            //'GetAdminFilterHTML' => [],
            //'AddFilterFields' => [],
            //'GetUIFilterProperty' => [],
        ];
    }

    public static function getAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        $fieldNamePrefix = $strHTMLControlName['VALUE'];
        $setting = $arProperty['USER_TYPE_SETTINGS'];

        $val = $value['VALUE'];
        $html = '';

        $abstractFactory = app()->factory()->iBlock()->getEntityById($arProperty['IBLOCK_ID']);
        $propertyEntity = $abstractFactory->getPropertyFactory()->getEntityById($arProperty['ID']);
        $valueFactory = $propertyEntity->getAdapter()->getValueFactory();
        $valueEntity = $valueFactory->getEntityById($val);
        if($valueEntity){
            $html .= "[{$valueEntity->getId()}] {$valueEntity->getName()}";
        }

        return $html;
    }

    public static function convertToDB($arProperty, $value)
    {
        if($arProperty['MULTIPLE'] == 'Y'){
            $value['VALUE'] = Json::encode($value['VALUE']);
        }

        return $value;
    }

    public static function convertFromDB($arProperty, $value)
    {
        if($arProperty['MULTIPLE'] == 'Y'){
            if(!is_array($value['VALUE'])){
                $value['VALUE'] = strlen($value['VALUE']) ? $value['VALUE'] : '[]';

                try {
                    $value['VALUE'] = Json::decode($value['VALUE']);
                }
                catch(\Exception $e) {
                    $value['VALUE'] = [];
                }
            }
        }
        return $value;
    }

    /**
     * Значение по умолчанию
     *
     * @param $arProperty
     * @param $value
     * @param $strHTMLControlName
     * @return string
     */
    public static function getPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
        $bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';
        $fieldNamePrefix = $strHTMLControlName['VALUE'];
        $setting = $arProperty['USER_TYPE_SETTINGS'];

        $val = $value['VALUE'];
        $html = '';

        $options = collect();
        $abstractFactory = app()->factory()->iBlock()->getEntityById($arProperty['IBLOCK_ID']);
        if($abstractFactory){
            $propertyEntity = $abstractFactory->getPropertyFactory()->getEntityById($arProperty['ID']);
            if($propertyEntity){
                $valueFactory = $propertyEntity->getAdapter()->getValueFactory();

                $options = $valueFactory->getCollection()->map(function($entity){
                    return "[{$entity->getId()}] {$entity->getName()}";
                });
            }
        }
        $options->prepend('not select', 0);

        $row = function($content){
            return $content;
        };
        $fieldService = app()->service()->form()->field();
        $manualFactoryField = $fieldService->select()
            ->setName($fieldNamePrefix)
            ->setOptions($options)
            ->setMultiple($arProperty['MULTIPLE'] === 'Y')
//            ->setLabel($arProperty['NAME'])
            ->setValue($val);
        $html .= $row($manualFactoryField->getTemplate('edit'));

        return $html;
    }

    public static function getPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName)
    {
        $bEditProperty = $strHTMLControlName['MODE'] === 'EDIT_FORM';
        $bDetailPage = $strHTMLControlName['MODE'] === 'FORM_FILL';
        $fieldName = $strHTMLControlName['VALUE'];

        $setting = $arProperty['USER_TYPE_SETTINGS'];
        $val = $value['VALUE'];

        return 'multy';
    }

    public static function prepareSettings($arFields)
    {
        $arFields['FILTRABLE'] = $arFields['SMART_FILTER'] = $arFields['SEARCHABLE'] = $arFields['WITH_DESCRIPTION'] = 'N';
        $arFields['MULTIPLE_CNT'] = 1;

        return $arFields;
    }

    public static function getSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = [
            'HIDE' => [
                'SMART_FILTER',
                'FILTRABLE',
                'SEARCHABLE',
                'MULTIPLE_CNT',
                'COL_COUNT',
                'MULTIPLE',
                'WITH_DESCRIPTION',
                'FILTER_HINT',
            ],
            'SET' => [
                'SMART_FILTER' => 'N',
                'FILTRABLE' => 'N',
                'SEARCHABLE' => 'N',
                'MULTIPLE_CNT' => '1',
                'MULTIPLE' => 'N',
                'WITH_DESCRIPTION' => 'N',
                'DEFAULT_VALUE' => '',
            ],
        ];
        $fieldNamePrefix = $strHTMLControlName['NAME'];
        $setting = $arProperty['USER_TYPE_SETTINGS'];

        $html = 'GetSettingsHTML';



        $entityType = $arProperty['USER_TYPE_SETTINGS']['ENTITY_TYPE'];
        $html = '';

        $row = function($content){
            return '<tr><td width="40%"></td><td>'.$content.'</td></tr>';
        };


        $fieldService = app()->service()->form()->field();
        $manualFactoryField = $fieldService->text()
            ->setName($fieldNamePrefix . '[MANUAL_FACTORY_CLASS]')
            ->setLabel('Manual factory class')
            ->setSize(40)
            ->setValue((string)$setting['MANUAL_FACTORY_CLASS']);
        $html .= $row($manualFactoryField->getTemplate('edit'));

        $manualFactoryEntityIdField = $fieldService->text()
            ->setName($fieldNamePrefix . '[MANUAL_FACTORY_ENTITY_ID]')
            ->setLabel('Manual factory entity id')
            ->setSize(5)
            ->setValue((string)$setting['MANUAL_FACTORY_ENTITY_ID']);
        $html .= $row($manualFactoryEntityIdField->getTemplate('edit'));

        $abstractFactoryField = $fieldService->text()
            ->setName($fieldNamePrefix . '[ABSTRACT_FACTORY_CLASS]')
            ->setLabel('Abstract factory class')
            ->setSize(40)
            ->setValue((string)$setting['ABSTRACT_FACTORY_CLASS']);
        $html .= $row($abstractFactoryField->getTemplate('edit'));

        $abstractFactoryArgsField = $fieldService->text()
            ->setName($fieldNamePrefix . '[ABSTRACT_FACTORY_ARGS]')
            ->setMultiple(true)
            ->setLabel('Abstract factory args')
            ->setSize(5)
            ->setValue($setting['ABSTRACT_FACTORY_ARGS']);
        $html .= $row($abstractFactoryArgsField->getTemplate('edit'));

        $factoryField = $fieldService->text()
            ->setName($fieldNamePrefix . '[FACTORY_CLASS]')
            ->setLabel('Factory class')
            ->setSize(40)
            ->setValue((string)$setting['FACTORY_CLASS']);
        $html .= $row($factoryField->getTemplate('edit'));

        $factoryArgsField = $fieldService->text()
            ->setName($fieldNamePrefix . '[FACTORY_ARGS]')
            ->setMultiple(true)
            ->setLabel('Factory args')
            ->setSize(5)
            ->setValue(is_array($setting['FACTORY_ARGS'])?$setting['FACTORY_ARGS']:[]);
        $html .= $row($factoryArgsField->getTemplate('edit'));

        return $html;
    }

}
