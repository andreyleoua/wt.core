<?php


namespace Wt\Core\Property;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\APropertyEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Form\Field\DefaultField;
use Wt\Core\IFields\PropertyValueFields;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\APropertyAdapter;
use Wt\Core\Support\Entity;

class PropertyValueEntity extends Entity implements IPropertyValueEntity
{
    /** @var AFactory */
    protected $factory;
    /** @var AEntity  */
    protected $parentEntity;
    /** @var APropertyEntity  */
    protected $propertyEntity;

    public function __construct($restoredValue, IFactory $factory, AEntity $parentEntity, APropertyEntity $propertyEntity)
    {
        $this->factory = $factory;
        $this->parentEntity = $parentEntity;
        $this->propertyEntity = $propertyEntity;

        parent::__construct($restoredValue);
    }

    /**
     * @return APropertyEntity
     */
    public function getPropertyEntity(): APropertyEntity
    {
        return $this->propertyEntity;
    }

    /**
     * @return AFactory
     */
    public function getPropertyFactory()
    {
        return $this->getPropertyEntity()->getFactory();
    }

    /**
     * @return AFactory
     */
    public function getParentFactory()
    {
        return $this->getParentEntity()->getFactory();
    }

    /**
     * @return AEntity
     */
    public function getParentEntity(): AEntity
    {
        return $this->parentEntity;
    }

    /**
     * @return int|string
     */
    public function getParentEntityId()
    {
        if($this->getParentEntity()){
            return $this->getParentEntity()->getId();
        }
        return 0;
    }

    public function getElementId()
    {
        return $this->getParentEntity()->getId();
    }

    /**
     * @return APropertyAdapter
     */
    public function getAdapter()
    {
        return $this->getPropertyEntity()->getAdapter();
    }

    /**
     * @return DefaultField
     */
    public function getFormField()
    {
        $field = $this->getAdapter()->getFormField();
        $field->setPropertyValue($this);
        $field->setValue($this->getValue());
        return $field;
    }

    /** @return string|array */
    public function getDisplayValue()
    {
        return $this->getPropertyEntity()->getAdapter()->getDisplayValue($this);
    }

    /** @return string|array 10 | [10,11] */
    public function getValue()
    {
        return $this->getRaw('~VALUE', function () {
            $value = $this->getPropertyEntity()->getAdapter()->getValue($this->getRestoredValue());
            $this->set('~VALUE', $value);
            return $value;
        });
    }

    /** @return array */
    public function getRestoredValue()
    {
        return $this->getRaw();
    }

    /**
     * @return AEntityCollection|AEntity|null
     */
    public function getValueEntity()
    {
        return $this->getPropertyEntity()->getAdapter()->getValueEntity($this);
    }

    /** @return string|array 10 | [10,11] */
    public function getDefaultValue()
    {
        return $this->getPropertyEntity()->getDefaultValue();
    }

    public function getBaseType()
    {
        return $this->getPropertyEntity()->getBaseType();
    }

    /**
     * @param array|string $type
     * @return bool
     */
    public function isBaseType($type)
    {
        return $this->getPropertyEntity()->isBaseType($type);
    }

    public function isMultiple()
    {
        return $this->getPropertyEntity()->isMultiple();
    }

    public function isRequired()
    {
        return $this->getPropertyEntity()->isRequired();
    }

    public function getHint()
    {
        return $this->getPropertyEntity()->getHint();
    }

    public function getName()
    {
        return $this->getPropertyEntity()->getName();
    }

    public function getUserType()
    {
        return $this->getPropertyEntity()->getUserType();
    }

    public function getSettings()
    {
        return $this->getPropertyEntity()->getSettings();
    }

    public function isActive()
    {
        return $this->getPropertyEntity()->isActive();
    }

    public function getCode()
    {
        return $this->getPropertyEntity()->getCode();
    }

    /**
     * @return AFactory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return PropertyValueFields
     */
    public function fields()
    {
        return parent::fields();
    }
}