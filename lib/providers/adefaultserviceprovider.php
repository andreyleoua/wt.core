<?php


namespace Wt\Core\Providers;


use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Wt\Core\App\ServiceContainer;
use Wt\Core\Assets\AssetsPull;
use Wt\Core\Assets\AssetsService;
use Wt\Core\EventDispatcher;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Interfaces\IServiceContainer;
use Wt\Core\Interfaces\IServiceProvider;
use Wt\Core\Cache\ArrayCache;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Lexer;
use Wt\Core\Support\LexerCollection;
use Wt\Core\Support\Logger;
use Wt\Core\Support\Paginator;
use Wt\Core\Support\Registry;
use Wt\Core\Assets;
use Wt\Core\Templater\Templater;
use Wt\Core\Templater\TemplaterService;

class ADefaultServiceProvider implements IServiceProvider
{

    /**
     * @var IApp
     */
    protected $app;

    public function register(IApp $app)
    {
        $this->app = $app;
        $this->registerSingletones();
        $this->registerAliases();
    }

    public function registerSingletones()
    {
        $container = $this->app->container();
        $config = $this->app->config();
        $singletoneClasses = $config->get('singleton', [])->toArray();
        foreach ($singletoneClasses as $alias => $className){
            if( is_numeric($alias) ){
                $container->singleton($className);
            } else {
                $container->singleton($alias, $className);
            }
        }
    }

    public function registerAliases()
    {
        $container = $this->app->container();
        $config = $this->app->config();
        $aliasesClasses = $config->get('aliases', [])->toArray();
        foreach ($aliasesClasses as $aliasClass => $realClass) {
            $container->alias($aliasClass, $realClass);
        }
    }
}