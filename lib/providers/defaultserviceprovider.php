<?php


namespace Wt\Core\Providers;


use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Wt\Core\App\ServiceContainer;
use Wt\Core\Assets\AssetsPull;
use Wt\Core\Assets\AssetsService;
use Wt\Core\Assets\PluginManager;
use Wt\Core\EventDispatcher;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Cache\ArrayCache;
use Wt\Core\Property\PropertyManager;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Lexer;
use Wt\Core\Support\Logger;
use Wt\Core\Support\Paginator;
use Wt\Core\Support\Registry;
use Wt\Core\Assets;
use Wt\Core\Support\System;
use Wt\Core\Templater\TemplaterService;
use Wt\Core\Media\ImageResizerService;

class DefaultServiceProvider extends ADefaultServiceProvider
{

    public function register(IApp $app)
    {
        parent::register($app);

        $container = $this->app->container();
        $config = $this->app->config();

        $container->singleton('container', $container);

        $this->registerBxInstances();

        $container->bind('collection', Collection::class);
        $container->bind('hit_cache', ArrayCache::class);
        $container->bind('array_cache', ArrayCache::class);

        $container->bind(Logger::class);
        $container->alias('Logger', Logger::class);
        $container->extend(Logger::class, function ($logger, $serviceContainer) use ($config) {
            $loggerConfig = $config->get('logger');
            /** @var Logger $logger */
            /** @var ServiceContainer $serviceContainer */
            $logger->setRoot($loggerConfig->get('root'));

            return $logger;
        });

        $container->singleton('logger:service', function($container) use ($config) {
            /** @var ServiceContainer $container */
            return $container->make(Logger::class);
        });

        $container->singleton(System::class);

        $container->singleton('registry', Registry::class);

        $container->bind('pagen', Paginator::class);

        $container->singleton('eventManager', EventDispatcher::class);

        $appLexer = $container->make(Lexer::class);
        $container->singleton('appLexer', $appLexer);

        $assetsPull = new AssetsPull();
        if(class_exists('Bitrix\Main\Application')){
            $assetsPull->setTypeList([
                new Assets\Type\Bitrix\CssType(),
                new Assets\Type\Bitrix\CssStringType(),
                new Assets\Type\Bitrix\JsType(),
                new Assets\Type\Bitrix\JsStringType(),
                new Assets\Type\Bitrix\JsAsyncType(),
                new Assets\Type\Bitrix\JsDeferType(),
                new Assets\Type\Bitrix\FontType(),
                new Assets\Type\Bitrix\StringType(),
            ]);
        }

        $container->singleton(AssetsPull::class, $assetsPull);
        $container->singleton(AssetsService::class);
        $container->alias('assets', AssetsService::class);
        $container->alias('assets_pro', function() use ($container, $config){
            $templaterConfig = $config->get('assets', [])->toArray();
            $path = $templaterConfig['path'];
            $namespace = $templaterConfig['namespace'];

            /**
             * @var PluginManager $pluginManager
             */
            $pluginManager = $container->make(PluginManager::class, [
                $path,
                $namespace
            ]);

            $pluginManager->registerPluginList($config->get('plugins', []));

            /**
             * @var Assets\Assets::class
             */
            return $container->make(Assets\Assets::class, [$pluginManager]);
        });

        $container->singleton(TemplaterService::class);
        $container->alias('templater', TemplaterService::class);

        $container->singleton(
            PropertyManager::class . ':service',
            function($container) use ($config) {
                /** @var ServiceContainer $container */
                $profileList = $config->get('property_adapter_profile_list', []);
                $profileId = $config->get('property_adapter_profile_id', 'default');
                $profile = $profileList->get($profileId, [])->lowerKeys();
                return $container->make(PropertyManager::class, [$profile]);
            });

        $container->singleton(ImageResizerService::class);
        $container->extend(ImageResizerService::class, function ($imageResizerService, $serviceContainer) use ($config) {
            /** @var ImageResizerService $imageResizerService */
            /** @var ServiceContainer $serviceContainer */
            $map = $config->get('image_resizer_map', [])->toArray();
            $imageResizerService->setMap($map);

            return $imageResizerService;
        });
    }

    public function registerBxInstances()
    {
        if(!class_exists('Bitrix\Main\Application')){
            return;
        }
        $container = $this->app->container();
        $container->singleton('bxApp', Application::getInstance());
        $container->singleton('bxRequest', Application::getInstance()->getContext()->getRequest());
        $container->singleton('bxResponse', Application::getInstance()->getContext()->getResponse());
        $container->singleton('bxServer', Application::getInstance()->getContext()->getServer());
        $container->singleton('bxConnection', Application::getInstance()->getConnection());
        $container->singleton('bxEventManager', EventManager::getInstance());
        $container->alias('bxDb', 'bxConnection');

        $container->alias('connection', 'bxConnection');
        $container->alias('server', 'bxServer');
        $container->alias('response', 'bxResponse');
        $container->alias('request', 'bxRequest');
    }
}