<?php


namespace Wt\Core\Providers;


use Wt\Core\AbstractFactory\ShopAbstractFactory;
use Wt\Core\App\ServiceContainer;
use Wt\Core\ManualFactory\HlBlockManualFactory;
use Wt\Core\ManualFactory\IBlockManualFactory;
use Wt\Core\AbstractFactory\MainAbstractFactory;
use Wt\Core\Interfaces\IApp;
use Wt\Core\Interfaces\IServiceProvider;

/**
 * @property ServiceContainer container
 */
class DefaultFactoryProvider implements IServiceProvider
{
    public function register(IApp $app)
    {
        $config = $app->config();
        $serviceContainer = $app->container();

        /**
         * @var MainAbstractFactory $mainAbstractFactory
         */
        $mainAbstractFactory = $serviceContainer->make(MainAbstractFactory::class, [$config->get('MainAbstractFactory', [])]);
        $serviceContainer->singleton('mainAbstractFactory', $mainAbstractFactory);

        /**
         * @var ShopAbstractFactory $shopAbstractFactory
         */
        $shopAbstractFactory = $serviceContainer->make(ShopAbstractFactory::class, [$config->get('ShopAbstractFactory', [])]);
        $serviceContainer->singleton('shopAbstractFactory', $shopAbstractFactory);

        /**
         * @var IBlockManualFactory $iBlockManualFactory
         */
        $iBlockManualFactory = $serviceContainer->make(IBlockManualFactory::class, [$config->get('IBlockManualFactory', [])]);
        $serviceContainer->singleton('iBlockManualFactory', $iBlockManualFactory);

        /**
         * @var HlBlockManualFactory $iBlockManualFactory
         */
        $hlBlockManualFactory = $serviceContainer->make(HlBlockManualFactory::class, [$config->get('HlBlockManualFactory', [])]);
        $serviceContainer->singleton('hlBlockManualFactory', $hlBlockManualFactory);

    }
}