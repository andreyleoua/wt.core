<?php


namespace Wt\Core\Type;


class Num
{

    /**
     * округление до меньшего
     * @param $num
     * @param int $decimals
     * @return float|int
     */
    public static function floor($num, $decimals = 0)
    {
        $num = (float)$num;
        $k = 10 ** (int)$decimals;
        $num = floor($k * $num) / $k;
        return $num;
    }

    /**
     * округление до большего
     * @param $num
     * @param int $decimals
     * @return float|int
     */
    public static function ceil($num, $decimals = 0)
    {
        $num = (float)$num;
        $k = 10 ** (int)$decimals;
        $num = ceil($k * $num) / $k;
        return $num;
    }

    public static function numberFormat($num, $decimals = 0, $decimal_separator = '.', $thousands_separator = '')
    {
        return number_format($num, $decimals, $decimal_separator, $thousands_separator);
    }

    public static function numberFormatFloor($num, $decimals = 0, $decimal_separator = '.', $thousands_separator = '')
    {
        return number_format(self::floor($num, $decimals), $decimals, $decimal_separator, $thousands_separator);
    }

    public static function numberFormatCeil($num, $decimals = 0, $decimal_separator = '.', $thousands_separator = '')
    {
        return number_format(self::ceil($num, $decimals), $decimals, $decimal_separator, $thousands_separator);
    }
}