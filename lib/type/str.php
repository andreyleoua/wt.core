<?php


namespace Wt\Core\Type;


class Str
{
    /**
     * The cache of snake-cased words.
     *
     * @var array
     */
    protected static $snakeCache = [];

    /**
     * The cache of camel-cased words.
     *
     * @var array
     */
    protected static $camelCache = [];

    /**
     * The cache of studly-cased words.
     *
     * @var array
     */
    protected static $studlyCache = [];

    public static $digit = '0123456789';
    public static $latinLower = 'abcdefghijklmnopqrstuvwxyz';
    public static $latinUpper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    static $chars = array(
        'lower' => 'abcdefghijklmnopqrstuvwxyz',
        'upper' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        'digits' => '0123456789',
        'extra' => ',.-()<>%/!"&=;:_[]{}#\\?\'+*',
    );

    /**
     * ^[\/#].*[\/#][xgmisuUAJD]*$
     * @param $str
     * @return bool
     */
    public static function is_regExp($str)
    {
        if(is_string($str) && (strlen($str) >= 2) && ($str[0] == mb_substr($str, -1)) && in_array($str[0], ['/'])) {
            return true;
        }
        return false;
    }

    /**
     * @return string|null
     */
    public static function getCharset()
    {
        $charset = defined('LANG_CHARSET')?constant('LANG_CHARSET'):null;
        if($charset === ''){
            $charset = null;
        }
        return $charset;
    }

    /**
     * на тесты ////
     *
     * @param string $str
     * @param string|array $begin
     * @param string $flags
     * @return bool
     */
    public static function beginWith($str, $begin = '', $flags = 'iu')
    {
        $str = strval($str);
        if(!strlen($str)) return false;
        $arBegin = array_filter((array)$begin, 'mb_strlen');
        foreach ($arBegin as $begin) {
            if (preg_match('/^'.quotemeta($begin).'.*/'.$flags, $str)){
                return true;
            }
        }
        return false;
    }

    public static function replaceKey2Value($text, $array, $keyWrap = '#')
    {
        $search  = [];
        $replace = [];

        foreach ((array)$array as $s => $r) {
            if(!strlen($s)) {
                continue;
            }
            $search[] = $keyWrap . $s . $keyWrap;
            $replace[] = $r;
        }

        $text    = strval($text);
        $output  = str_replace($search, $replace, $text);

        return $output;
    }


    public static function truncateText($strText, $intLen = 256)
    {
        $strText = trim($strText);
        $strlen = strlen($strText);
        if($strlen > max(3, $intLen) && $intLen > 3 ) {

            $raw = substr($strText, 0, $intLen - 3);

            return rtrim($raw, ".")."...";
        } else if ($intLen <= 3) {
            return '...';
        }
        else
            return $strText;
    }

    public static function random($length = 8, $characters = 'abcdefghijklmnopqrstuvwxyz')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function rot($str, $n = 13)
    {
        static $letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $n = (int)$n % 26;
        if (!$n) return $str;
        if ($n == 13) return str_rot13($str);
        for ($i = 0, $l = strlen($str); $i < $l; $i++) {
            $c = $str[$i];
            if ($c >= 'a' && $c <= 'z') {
                $str[$i] = $letters[(ord($c) - 71 + $n) % 26];
            } else if ($c >= 'A' && $c <= 'Z') {
                $str[$i] = $letters[(ord($c) - 39 + $n) % 26 + 26];
            }
        }
        return $str;
    }

    public static function getLength($string)
    {
        if (\function_exists('mb_strlen'))
        {
            $result = mb_strlen($string, static::getCharset());
        }
        else
        {
            $result = strlen($string);
        }

        return $result;
    }

    public static function getPosition($haystack, $needle, $offset = 0)
    {
        if (\function_exists('mb_strpos'))
        {
            $result = mb_strpos($haystack, $needle, $offset, static::getCharset());
        }
        else
        {
            $result = strpos($haystack, $needle, $offset);
        }

        return $result;
    }

    public static function getLastPosition($haystack, $needle, $offset = 0)
    {
        if (\function_exists('mb_strrpos'))
        {
            $result = mb_strrpos($haystack, $needle, $offset, static::getCharset());
        }
        else
        {
            $result = strrpos($haystack, $needle, $offset);
        }

        return $result;
    }

    public static function getPositionCaseInsensitive($haystack, $needle, $offset = 0)
    {
        if (\function_exists('mb_stripos'))
        {
            $result = mb_stripos($haystack, $needle, $offset, static::getCharset());
        }
        else
        {
            $result = stripos($haystack, $needle, $offset);
        }

        return $result;
    }

    public static function getSubstring($string, $from, $length = null)
    {
        if (\function_exists('mb_substr'))
        {
            $charset = static::getCharset();
            if($charset){
                $result = mb_substr($string, $from, $length, static::getCharset());
            } else {
                $result = mb_substr($string, $from, $length);
            }
        }
        else
        {
            $result = substr($string, $from, $length);
        }

        return $result;
    }

    public static function toUpper($string)
    {
        if (\function_exists('mb_strtoupper'))
        {
            $result = mb_strtoupper($string, static::getCharset());
        }
        else
        {
            $result = strtoupper($string);
        }

        return $result;
    }

    public static function toLower($string)
    {
        if (\function_exists('mb_strtolower'))
        {
            $result = mb_strtolower($string, static::getCharset());
        }
        else
        {
            $result = strtolower($string);
        }

        return $result;
    }

    public static function ucFirst($string)
    {
        return
            static::toUpper(static::getSubstring($string, 0, 1))
            . static::getSubstring($string, 1);
    }

    public static function padLeft($string, $length, $pad = ' ')
    {
        $stringLength = static::getLength($string);
        $lengthDiff = $length - $stringLength;

        if ($lengthDiff > 0)
        {
            $string = str_repeat($pad, $lengthDiff) . $string;
        }

        return $string;
    }

    public static function match($pattern, $subject, &$matches = null, $flags = 0, $offset = 0)
    {
        $needConvert = !\Bitrix\Main\Application::isUtfMode() && static::hasPatternUnicode($pattern);

        if ($needConvert)
        {
            $subject = \Bitrix\Main\Text\Encoding::convertEncoding($subject, static::getCharset(), 'UTF-8');
        }

        $result = preg_match($pattern, $subject, $matches, $flags, $offset);

        if ($result && $needConvert)
        {
            $matches = \Bitrix\Main\Text\Encoding::convertEncoding($matches, 'UTF-8', static::getCharset());
        }

        return $result;
    }

    protected static function hasPatternUnicode($pattern)
    {
        $wrapSymbol = static::getSubstring($pattern, 0, 1);
        $wrapClosePosition = static::getLastPosition($pattern, $wrapSymbol, 1);
        $result = false;

        if ($wrapClosePosition !== false)
        {
            $modifiers = static::getSubstring($pattern, $wrapClosePosition);
            $result = (static::getPosition($modifiers, 'u') !== false);
        }

        return $result;
    }

    public static function lcFirst($string)
    {
        return
            static::toLower(static::getSubstring($string, 0, 1))
            . static::getSubstring($string, 1);
    }

    //abCd to AB_CD
    public static function camelToSnake($str, $upper = true)
    {
        static $cache = [0=>[
            'id' => 'id',
            'name' => 'name',
            'code' => 'code',
            'xmlId' => 'xml_id',
        ],1=>[
            'id' => 'ID',
            'name' => 'NAME',
            'code' => 'CODE',
            'xmlId' => 'XML_ID',
            'langPrefix' => 'LANG_PREFIX',
            'prefix' => 'PREFIX',
            'extDir' => 'EXT_DIR',
            'env' => 'ENV',
            'dir' => 'DIR',
            'namespace' => 'NAMESPACE',
            'moduleId' => 'MODULE_ID',
        ]];

        if(isset($cache[$upper?1:0][$str])){
            return $cache[$upper?1:0][$str];
        }
        $s = $str;

        if((strpos($str, '_') === false) && preg_match('/[a-zа-яё]/', $str)){
            $s = mb_ereg_replace("([A-ZА-ЯЁ])", "_\\1", $str);
        }
        $s = trim($s, '_');


        $r = $upper?mb_strtoupper($s):mb_strtolower($s);

        $cache[$upper?1:0][$str] = $r;

        return $r;
    }

    //snakeToCamel => AB_CD to abCd
    public static function snakeToCamel($string)
    {
        return lcfirst(str_replace('_', '', ucwords(strtolower($string), '_')));
    }

    /**
     * Replace the given value in the given string.
     *
     * @param  string|iterable<string>  $search
     * @param  string|iterable<string>  $replace
     * @param  string|iterable<string>  $subject
     * @param  bool  $caseSensitive
     * @return string|string[]
     */
    public static function replace($search, $replace, $subject, $caseSensitive = true)
    {
        if ($search instanceof \Traversable) {
            $search = collect($search)->all();
        }

        if ($replace instanceof \Traversable) {
            $replace = collect($replace)->all();
        }

        if ($subject instanceof \Traversable) {
            $subject = collect($subject)->all();
        }

        return $caseSensitive
            ? str_replace($search, $replace, $subject)
            : str_ireplace($search, $replace, $subject);
    }

    /**
     * Wrap the string with the given strings.
     *
     * @param  string  $value
     * @param  string  $before
     * @param  string|null  $after
     * @return string
     */
    public static function wrap($value, $before, $after = null)
    {
        return $before.$value.(is_null($after)?$before:$after);
    }

    /**
     * Convert a string to kebab case.
     * kebab-case|dash-case
     *
     * @param  string  $value
     * @return string
     */
    public static function kebab($value)
    {
        return static::snake($value, '-');
    }

    /**
     * Convert a value to camel case.
     * camelCase
     *
     * @param  string  $value
     * @return string
     */
    public static function camel($value)
    {
        if (isset(static::$camelCache[$value])) {
            return static::$camelCache[$value];
        }

        return static::$camelCache[$value] = lcfirst(static::studly($value));
    }

    /**
     * Convert a value to studly caps case.
     * StudlyCaps|PascalCase
     *
     * @param  string  $value
     * @return string
     */
    public static function studly($value)
    {
        $key = $value;

        if (isset(static::$studlyCache[$key])) {
            return static::$studlyCache[$key];
        }

        $words = explode(' ', static::replace(['-', '_'], ' ', $value));

        $studlyWords = array_map(function($word) { return static::ucfirst($word); }, $words);

        return static::$studlyCache[$key] = implode($studlyWords);
    }

    /**
     * Convert a string to snake case.
     * snake_case
     *
     * @param  string  $value
     * @param  string  $delimiter
     * @return string
     */
    public static function snake($value, $delimiter = '_')
    {
        $key = $value;

        if (isset(static::$snakeCache[$key][$delimiter])) {
            return static::$snakeCache[$key][$delimiter];
        }

        if (! ctype_lower($value)) {
            $value = preg_replace('/\s+/u', '', ucwords($value));

            $value = static::toLower(preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
        }

        return static::$snakeCache[$key][$delimiter] = $value;
    }

    /**
     * SCREAMING_SNAKE_CASE
     *
     * @param $value
     * @return string
     */
    public static function screamingSnake($value)
    {
        return static::toUpper(static::snake($value, '_'));
    }

    /**
     * Limit the number of words in a string.
     *
     * @param  string  $value
     * @param  int  $words
     * @param  string  $end
     * @return string
     */
    public static function words($value, $words = 100, $end = '...')
    {
        preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

        if (! isset($matches[0]) || static::length($value) === static::length($matches[0])) {
            return $value;
        }

        return rtrim($matches[0]).$end;
    }

    /**
     * Return the length of the given string.
     *
     * @param  string  $value
     * @param  string|null  $encoding
     * @return int
     */
    public static function length($value, $encoding = null)
    {
        return mb_strlen($value, $encoding);
    }

    /**
     * Remove all strings from the casing caches.
     *
     * @return void
     */
    public static function flushCache()
    {
        static::$snakeCache = [];
        static::$camelCache = [];
        static::$studlyCache = [];
    }
}