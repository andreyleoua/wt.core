<?php

namespace Wt\Core\Type;

use ArrayAccess;
use Wt\Core\Support\Collection;

class Arr
{

    const  delimiter = '.';
    /**
     * определить значение инкримента
     * @param $increment
     * @param $array
     * @return int|string
     */
    public static function getIncrement($increment, $array)
    {
        if(is_string($increment) && strlen($increment)){
            $__temp_key = $increment; $i = 0;
            while(isset($array[$__temp_key.strval($i)])) {++$i;}
            $increment = $__temp_key.strval($i);
            return $increment;
        }
        $increment = (integer)$increment; while(isset($array[$increment])) {++$increment;}
        return $increment;
    }

    /**
     * Determine if the given key exists in the provided array.
     *
     * @param  \ArrayAccess|array  $array
     * @param  string|int  $key
     * @return bool
     */
    public static function exists($array, $key)
    {
        if ($array instanceof ArrayAccess) {
            return $array->offsetExists($key);
        }

        return array_key_exists($key, $array);
    }

    /**
     * Collapse an array of arrays into a single array.
     *
     * @param  array  $array
     * @return array
     */
    public static function collapse($array)
    {
        $results = [];

        foreach ($array as $values) {
            if ($values instanceof Collection) {
                $values = $values->all();
            } elseif (! is_array($values)) {
                continue;
            }

            $results = array_merge($results, $values);
        }

        return $results;
    }

    /**
     * Determine whether the given value is array accessible.
     *
     * @param  mixed  $value
     * @return bool
     */
    public static function accessible($value)
    {
        return is_array($value) || $value instanceof ArrayAccess;
    }

    /**
     * внимательно следить за входным значением $array
     * оно не дб "неопределено"
     *
     * @param $array
     * @param $key
     * @param $value
     * @param int $increment
     * @return bool|int|string
     */
    public static function add(&$array, $key, $value, $increment = 0)
    {

        if(!is_array($array)) return false;

        if( self::isKey($key) && !isset($array[$key]) ){
            $array[$key] = $value;
            return $key;
        }

        $newKey = self::getIncrement($increment, $array);
        $array[$newKey] = $value;
        return $newKey;
    }

    /**
     * @param array $array
     * @param int|string|array $setter - ключ, массив ключ-значение
     * @param mixed $value
     * @return bool
     */
    public static function set(array &$array, $setter = [], $value = null)
    {

        $arSetData = [];
        if($value !== null) {
            $arSetData[] = ['branch' => $setter, 'value' => $value];
        } else {
            if(is_array($setter)) {
                foreach ($setter as $br => $vl) {
                    $arSetData[] = ['branch' => $br, 'value' => $vl];
                }
            }
        }

        foreach ($arSetData as $setData) {
            $setData['branch'] = self::getBranch($setData['branch']);
            self::setBranch($array, $setData['branch'], $setData['value']);
        }
        return true;
    }



    /**
     * получить значение в массиве по ветке
     * @param $array
     * @param $branch
     * @param null $def
     * @return bool|mixed
     */
    public static function get($array, $branch, $def = null)
    {
        $result = $def;
        if (is_iterable($array)) {
            $result = $array;
            $branch = self::getBranch($branch);
            foreach($branch as $key) {
                if (
                    self::isKey($key, false, false, false) &&
                    is_iterable($result) &&
                    isset($result[$key])
                ) {
                    $result = $result[$key];
                } else {
                    $result = $def;
                    break;
                }
            }
        }
        return $result;
    }

    public static function getColumn($array, $branchCol, $value) {
        $result = [];
        if (!is_array($array)) {
            return $result;
        }
        foreach ($array as $ak => $av) {
            $colResult = self::get($av, $branchCol, null);
            if ($colResult == $value) {
                $result = $av;
                break;
            }
        }
        return $result;
    }

    /**
     * @param $array
     * @param $branchCol
     * @param $value
     * @return array|mixed
     */
    public static function getColumns($array, $branchCol, $value)
    {
        $result = [];
        if (!is_array($array)) {
            return $result;
        }
        foreach ($array as $ak => $av) {
            $colResult = self::get($av, $branchCol, null);
            if ($colResult == $value) {
                $result[$ak] = $av;
            }
        }
        return $result;
    }


    public static function match($array, $conditions = [])
    {
        if (!is_array($array)) {
            return false;
        }
        $or = self::matchCheckOr($conditions);

        if($or) {
            return self::matchOr($array, $conditions);
        } else {
            return self::matchAnd($array, $conditions);
        }
    }

    private static function matchCheckOr(&$conditions)
    {
        $or = false;
        if(isset($conditions['logic']) && $conditions['logic'] == 'or') {
            unset($conditions['logic']);
            $or = true;
        }
        return $or;
    }

    private static function matchAnd(array $array, array $conditions = [])
    {
        if(!$conditions) {
            return true;
        }

        $i1Result = true;
        foreach ($conditions as $branch => $value) {

            $or = self::matchCheckOr($value);

            if($or) {
                $i2Result = self::matchOr($array, $value);
            } else {
                $arVal = self::getValues($array, $branch);

                $i2Result = false;

                foreach ($arVal as $val) {
                    if ($i2Result) {
                        break;
                    }
                    if ($val == $value) {
                        $i2Result = true;
                    }
                }
            }

            if (!$i2Result) {
                $i1Result = false;
                break;
            }
        }
        return $i1Result;
    }

    private static function matchOr(array $array, array $conditions = [])
    {
        if(!$conditions) {
            return true;
        }

        $i1Result = false;
        foreach ($conditions as $conditionList) {

            $i2Result = self::match($array, $conditionList);

            if ($i2Result) {
                $i1Result = true;
                break;
            }
        }
        return $i1Result;
    }

    public static function getStringBranch($branch)
    {
        $branch = (array)$branch;
        $branch = implode(self::delimiter, $branch);
        return $branch;
    }

    /**
     * невалидный ключ пропускается - линк не меняется
     * @param array $array
     * @param array $branch
     * @param null $value
     * @return bool
     */
    public static function setBranch(array &$array, array $branch = [], $value = null)
    {
        $countBranch = count($branch);

        if(!$countBranch) {
            $array = $value;
            return true;
        }

        $i = 0;
        $link[1] = $array;
        foreach ($branch as $key) {
            if(++$i != $countBranch) {
                if(self::isKey($key)) {
                    if(!isset($link[$i][$key])) {
                        $link[$i][$key] = [];
                    }
                    $link[$i][$key] = (array)$link[$i][$key];
                    $link[$i + 1] = &$link[$i][$key];
                } else {
                    $link[$i + 1] = &$link[$i];
                }
            } else {
                self::setKey($link[$i], $key, $value);
            }
        }
        $array = $link[1];
        return true;
    }

    public static function getBranch($branch)
    {
        $branch = self::getBranchSimple($branch);
        $rBranch = [];
        foreach ($branch as $subBranch) {
            $rBranch = array_merge($rBranch, self::getBranchSimple($subBranch));
        }
        return $rBranch;
    }

    private static function getBranchSimple($branch)
    {
        if(is_string($branch)) {
            $branch = explode(self::delimiter, $branch);
        }
        $branch = (array)$branch;
        return $branch;
    }

    public static function setKey(array &$array, $key = '', $value = '')
    {
        $key = trim($key);
        if(self::isKey($key)) {
            $array[$key] = $value;
            return true;
        }
        if(!strlen($key)){
            $array = $value;
            return true;
        }
        return false;
    }
    /**
     * @param $key
     * @param bool $bRegExp
     * @param bool $bAll
     * @param bool $delimiter
     * @return bool
     */
    public static function isKey($key, $bRegExp = true, $bAll = true, $delimiter = true)
    {
        switch (gettype($key)) {
            case 'integer':
                return true;
                break;
            case 'string':
                if(!strlen($key)) return false;
                if($bRegExp && Str::is_regExp($key)) return false;
                if($bAll && ($key == '*')) return false;
                if($delimiter && (strpos($key, self::delimiter) !== false)) return false;
                return true;
                break;
            default:
                return false;
        }
    }

    /**
     * проверка соответствия ключа его маске
     * @param $key
     * @param string|integer|array $keyMask если массив то значения работаю как ИЛИ
     * @param bool $bRegExp
     * @param bool $bAll
     * @return bool
     */
    public static function isValidKey($key, $keyMask = [], $bRegExp = true, $bAll = true)
    {
        if(!(is_string($key) || is_int($key) || is_null($key))) {
            return false;
        }

        $key = (string)$key;
        if($keyMask === null) $keyMask = [null];

        $arKeyMask = (array)$keyMask;

        $cond = true;
        foreach ($arKeyMask as $keyMask) {
            $cond = false;
            $keyMask = (string)$keyMask;
            if($key === '' && $keyMask === '') return true;
            if($bRegExp && Str::is_regExp($keyMask) && preg_match($keyMask, $key)) return true;
            if($bAll && (string)$keyMask == '*') return true;
            if($keyMask == $key) return true;
        }

        return $cond;
    }

    /**
     * @param $array
     * @param $position
     * @param $insert
     */
    public static function insertAfter(&$array, $position, $insert)
    {
        if ( is_int($position) ) {
            $array = self::insertAfterPosition($array, $insert, $position);
            return;
        }

        $array = self::insertAfterKey($array, $insert, $position);
    }

    public static function insertAfterKey(array $array, $insert, $keyName)
    {
        if( !is_array($insert) ) {
            $insert = [$insert];
        }

        $pos = array_search($keyName, array_keys($array), true) + 1;

        return array_merge(
            array_slice($array, 0, $pos),
            $insert,
            array_slice($array, $pos)
        );
    }

    public static function insertAfterPosition(array $array, $insert, $position = 0)
    {
        $position = (int)$position;

        if( !is_array($insert) ){
            $insert = [$insert];
        }

        return array_slice($array, 0, ++$position) + $insert + array_slice($array, $position);
    }

    /**
     * вставить в массив значение в определенное место $position (номер или ключ)
     * @param array $array массив который будет изменен
     * @param $position string|int ключ или номер
     * @param $insert array ['key' => 'value']
     */
    public static function insert(&$array, $insert, $position = 0)
    {
        $insert = (array)$insert;

        $position = array_search($position, array_keys($array));

        $array = array_merge(
            array_slice($array, 0, $position),
            $insert,
            array_slice($array, $position)
        );
    }



    // $arr1 = array(
    // array('id'=>1,'name'=>'aA','cat'=>'cc'),
    // array('id'=>2,'name'=>'aa','cat'=>'dd'),
    // array('id'=>3,'name'=>'bb','cat'=>'cc'),
    // array('id'=>4,'name'=>'bb','cat'=>'dd')
    // );
    // $arr2 = sort($arr1, array('name'=>'DESC', 'cat'=>'ASC'));
    // print_r('<pre>');print_r($arr2);print_r('</pre>');
    /**
     * сортировка
     * при необходимости использовать array_values
     * @param $array
     * @param $order
     * @return array
     */
    public static function sort($array, $order)
    {
        $args = [];
        foreach ($order as $col => $sort) {
            $args[] = $col;
            if(!is_numeric($sort)) {
                $sort = trim(strtoupper($sort));
                if(!preg_match('/^(asc|desc)$/i', $sort)) {$sort = 'ASC';};
                $sort = constant('SORT_' . $sort);
            }
            $args[] = $sort;
        }
        foreach ($args as $n => $field) {
            if ($n % 2 === 0) {
                $tmp = array();
                foreach ($array as $key => $row) {
//                    $tmp[$key] = $row[$field];
                    $tmp[$key] = self::get($row, $field);
                }
                $args[$n] = $tmp;
            }
        }
        $args[] = &$array;

        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    public static function getValuesMatch($array, $branch, $conditions = [], $keyBranch = [], $bFKey = false, $inc = 0)
    {
        $arVal = self::getValues($array, $branch, $keyBranch, $bFKey, $inc);
        foreach ($arVal as $key => $val) {

            if(!self::match($val, $conditions)) {
                unset($arVal[$key]);
            }
        }
        return $arVal;
    }
    /**
     * собрать все значения в массиве по маске
     * будет дополнена маской по регулярному выражению
     *
     * @param array $array исходный массив
     * @param array $branch массив ключей, значения который попадут в выборку - маска. * - любой ключ. пр.: ['*', 'FIELDS', 'NAME']
     * @param array $keyBranch маска на ключ в результате;
     * @param bool $bFKey если true, то $keyBranch - маска от корня массива;
     * @param int|string $inc string: к строке добавляется порядковый номер начиная с 0, int: автоинкримент начиная с указанной позиции;
     *
     * @return array массив значений
     */
    public static function getValues($array, $branch = [], $keyBranch = [], $bFKey = false, $inc = 0) {
        $result = [];

        if (!is_array($array)) {
            return $result;
        }

        $branch = self::getBranch($branch);
        $keyBranch = self::getBranch($keyBranch);
        $bFKey = (boolean)$bFKey;

        $branchLength = count($branch);
        $result = [['_array' => $array, '_branch' => []]];
        foreach($branch as $branchPoint) {
            $tempResult = $result;
            $result = [];
            foreach ($tempResult as $k => $v ) {
                if (!is_array($v['_array'])){
                    continue;
                }

                foreach ($v['_array'] as $kKey => $vKey) {
                    // текущая ветка
                    $_branch = array_merge($v['_branch'], [$kKey]);
                    if(!self::isValidKey($kKey, $branchPoint)) continue;

                    // получаем маску для ключа в текущей ветке на последней итериации $branch
                    $_keyBranch = [];
                    if($branchLength == count($_branch)) {
                        $_keyBranch = $keyBranch;
                        if($bFKey) {
                            foreach ($_keyBranch as $num_keyBranchPoint => $_keyBranchPoint) {
                                //актуализируем маску текущим бренчем. Модим только вариативные значения
                                if(isset($_branch[$num_keyBranchPoint]) && !self::isKey($_keyBranchPoint)) {
                                    $_keyBranch[$num_keyBranchPoint] = $_branch[$num_keyBranchPoint];
                                }
                            }
                        }
                    }
                    // сохраняем промежуточное значение
                    if(
                        !empty($_keyBranch) &&
                        self::isKey($setKey = reset(self::getValues($bFKey?$array:$vKey, $_keyBranch)), false, false) &&
                        !isset($result[$setKey])
                    ) {
                        $result[$setKey] = ['_array' => $vKey, '_branch' => $_branch];
                    } elseif($inc !== false) {
                        $increment = self::getIncrement($inc, $result);
                        $result[$increment] = ['_array' => $vKey, '_branch' => $_branch];
                    }
                }
            }
        }

        foreach ($result as $k => &$v) {
            $v = $v['_array'];
        }

        return $result;
    }

    public static function getNormalize($array, $defArray)
    {
        $array = (array)$array;

        $result = array_intersect_key($array, $defArray);
        $result = $result + $defArray;

        return $result;
    }

    /**
     * возвращает массив значений длинной $count в случайном порядке с сохранением ключей
     *
     * $count == 0 получим []
     * $count === true получим Весь рандомный массив
     *
     * @param $array
     * @param int|bool $count
     * @return array
     */
    public static function getRand($array, $count = true)
    {

        $array = (array)$array;
        $arrayLen = count($array);

        if($count === true) {
            $count = $arrayLen;
        }

        $count = min(intval($count), $arrayLen);
        $result = [];

        if($count && ($count <= $arrayLen)) {
            $randomKeyArray = (array)array_rand($array, $count);
            foreach ($randomKeyArray as $key) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function groupByKey($array, $groupKey)
    {
        $result = [];
        if(!strlen($groupKey)) {
            return $result;
        }
        foreach($array as $key => $row) {
            $result[$row[$groupKey]][$key] = $row;
        }
        return $result;
    }

    public static function removeValue($array, $value, $all = true)
    {
        if(!$all){
            if(FALSE !== $key = array_search($value,$array)) unset($array[$key]);
            return $array;
        }
        foreach(array_keys($array,$value) as $key){
            unset($array[$key]);
        }
        return $array;
    }

    public static function hasRecursion($array, &$__pull = [])
    {
        if( !is_iterable($array) ){
            return false;
        }
        if(in_array($array, $__pull)) {
            return true;
        }
        $__pull[] = $array;
        foreach ($array as $key => $value) {
            if(self::hasRecursion($value, $__pull)) {
                return true;
            }
        }
        return false;
    }
}