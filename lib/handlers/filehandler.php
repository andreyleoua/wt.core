<?php

namespace Wt\Core\Handlers;

use CDiskQuota;
use CFile;
use COption;

class FileHandler
{
    /**
     * @param array $file
     * @param array $config array($arSize, $resizeType, array(), false, $arFilters, $bImmediate)
     * @param $callbackData
     * @param $bNeedResize
     * @param $sourceImageFile
     * @param $cacheImageFileTmp
     * @return true|mixed if return then break
     */
    public static function OnBeforeResizeImage(
        $file,
        $config,
        &$callbackData,
        &$bNeedResize,
        &$sourceImageFile,
        &$cacheImageFileTmp
    )
    {

    }

    protected static function getModifiWebPFileName($cacheImageFileTmp)
    {
        return preg_replace_callback('/(?<=\.)(\w+)$/', function($matches){
            return 'webp';
        }, $cacheImageFileTmp);
    }

    /**
     * @param array $file
     * @param array $config array($arSize, $resizeType, array(), false, $arFilters)
     * @param $callbackData
     * @param $cacheImageFile
     * @param $cacheImageFileTmp
     * @param $arImageSize
     * @return true|mixed if return then break
     */
    public static function OnAfterResizeImage(
        $file,
        $config,
        &$callbackData,
        &$cacheImageFile,
        &$cacheImageFileTmp,
        &$arImageSize
    )
    {

    }

    /**
     * @param array $fileData
     * @return true|mixed if return then break. $fileData[~src] = return
     */
    public static function OnGetFileSRC($fileData)
    {

    }

    /**
     * delete WebP
     *
     * @param array $fileData
     * @return mixed
     */
    public static function OnFileDelete($fileData)
    {
        $delete_size = 0;
        $io = \CBXVirtualIo::GetInstance();

        $fileData['FILE_NAME'] = static::getModifiWebPFileName($fileData['FILE_NAME']);

        $delete_size += CFile::ResizeImageDelete($fileData);


        $upload_dir = COption::GetOptionString('main', 'upload_dir', 'upload');
        $disk_space = COption::GetOptionInt('main', 'disk_space');

        $dname = $_SERVER['DOCUMENT_ROOT']. '/' .$upload_dir. '/' .$fileData['SUBDIR'];
        $fname = $dname. '/' .$fileData['FILE_NAME'];
        $f = $io->GetFile($fname);

        if($f->IsExists())
        {
            if ($disk_space > 0)
            {
                $fileSizeTmp = $f->GetFileSize();
                if ($io->Delete($f->GetPathWithName()))
                    $delete_size += $fileSizeTmp;
            }
            else
            {
                $io->Delete($f->GetPathWithName());
            }
        }

        $directory = $io->GetDirectory($dname);
        if($directory->IsExists() && $directory->IsEmpty())
            $directory->rmdir();

        /****************************** QUOTA ******************************/
        if($delete_size > 0 && COption::GetOptionInt('main', 'disk_space') > 0)
            CDiskQuota::UpdateDiskQuota('file', $delete_size, 'delete');
        /****************************** QUOTA ******************************/
    }
}