<?php

namespace Wt\Core\Handlers;

use Wt\Core\DebugControl\CheckpointsDebugControl;

class AutoBackupHandler
{
    public static function getDebugControl()
    {
        static $checkpointsDebugControl;
        if(!$checkpointsDebugControl){
            $checkpointsDebugControl = new CheckpointsDebugControl(static::class);
        }
        return $checkpointsDebugControl;
    }

    public static function onAutoBackupStart($NS)
    {
        $checkpointsDebugControl = static::getDebugControl();
        $checkpointsDebugControl->begin();
    }

    public static function onAutoBackupSuccess($NS)
    {
        $checkpointsDebugControl = static::getDebugControl();
        $checkpointsDebugControl->end();
    }

    public static function onAutoBackupError($NS)
    {
        $checkpointsDebugControl = static::getDebugControl();
        $checkpointsDebugControl->getData()->setArray($NS);
        $checkpointsDebugControl->end();
    }

    public static function onAutoBackupUnknownError($NS)
    {
        $checkpointsDebugControl = static::getDebugControl();
        $checkpointsDebugControl->begin();
        $checkpointsDebugControl->getData()->prepend('onAutoBackupUnknownError');
        $checkpointsDebugControl->end();
    }
}