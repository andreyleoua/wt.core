<?php

namespace Wt\Core\Handlers\Main;

class GroupHandler
{
    public static function onAfterAdd(&$arFields)
    {
        $factory = app()->factory()->main()->getUserGroupFactory();
        $factory->clearCache();
    }

    public static function onAfterUpdate($id, &$arFields)
    {
        $factory = app()->factory()->main()->getUserGroupFactory();
        $factory->clearCache();
    }

    public static function onDelete($id)
    {
        $factory = app()->factory()->main()->getUserGroupFactory();
        $factory->clearCache();
    }
}