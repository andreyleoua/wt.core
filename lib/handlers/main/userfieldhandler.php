<?php

namespace Wt\Core\Handlers\Main;

class UserFieldHandler
{
    public static function onAfterAdd($arFields)
    {
        $entityId = $arFields['ENTITY_ID'];
        if(!$entityId){
            return;
        }
        $userFieldFactory = app()->factory()->main()->getUserFieldFactory($entityId);
        $userFieldFactory->clearCache();
    }

    public static function onAfterUpdate($arFields, $id)
    {
        $entityId = $arFields['ENTITY_ID'];
        if(!$entityId){
            $rs = \CUserTypeEntity::GetList([], ['ID' => $id]);
            if($fields = $rs->Fetch())
            {
                $entityId = $fields['ENTITY_ID'];
            }
        }
        if(!$entityId){
            return;
        }
        $userFieldFactory = app()->factory()->main()->getUserFieldFactory($entityId);
        $userFieldFactory->clearCache();
    }

    public static function onAfterDelete($arField, $id)
    {
        $entityId = $arField['ENTITY_ID'];
        if(!$entityId){
            return;
        }
        $userFieldFactory = app()->factory()->main()->getUserFieldFactory($entityId);
        $userFieldFactory->clearCache();
    }
}