<?php

namespace Wt\Core\Handlers\IBlock;

class PropertyHandler
{
    public static function onDelete( $id )
    {
        $rsProperty = \CIBlockProperty::GetByID($id);
        $fileds = $rsProperty->Fetch();
        if(!$fileds){
            return;
        }
        $blockId = $fileds['IBLOCK_ID'];
        if(!$blockId){
            return;
        }
        $iBlockAF = app()->factory()->iBlock()->getEntityById($blockId);
        if(!$iBlockAF){
            return;
        }
        $iBlockAF->getPropertyFactory()->clearCache();
    }

    public static function onAfterUpdate(&$fileds)
    {
        $blockId = $fileds['IBLOCK_ID'];
        if(!$blockId){
            return;
        }
        $iBlockAF = app()->factory()->iBlock()->getEntityById($blockId);
        if(!$iBlockAF){
            return;
        }
        $iBlockAF->getPropertyFactory()->clearCache();
    }

    public static function OnAfterAdd(&$fileds)
    {
        $blockId = $fileds['IBLOCK_ID'];
        if(!$blockId){
            return;
        }
        $iBlockAF = app()->factory()->iBlock()->getEntityById($blockId);
        if(!$iBlockAF){
            return;
        }
        $iBlockAF->getPropertyFactory()->clearCache();
    }
}