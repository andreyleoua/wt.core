<?php


namespace Wt\Core\Handlers;


use \Bitrix\Main\Loader;

class Autoloader
{
	public static function OnPageStart(){}
	public static function OnBeforeProlog(){}
	public static function OnProlog()
    {
        if(defined('ADMIN_SECTION') && ADMIN_SECTION){
            $plugins = app()->config('set_admin_plugins', [])->toArray();
            app()->service()->assets()->setPlugins($plugins);
        }
    }
	public static function OnEpilog()
    {
        if(defined('ADMIN_SECTION') && ADMIN_SECTION){
            app()->service()->assets()->render();
        }
    }
	public static function OnLocalRedirect()
    {
	    if(!defined('BX_LOCAL_REDIRECT')) {
	        define('BX_LOCAL_REDIRECT', true);
        }
    }

	public static function loadCurrentModule()
	{
		Loader::includeModule('wt.core');
	}
}