<?php


namespace Wt\Core\Ajax;


use Wt\Core\Interfaces\IAjaxHandler;
use Wt\Core\Interfaces\IResponse;

class AjaxHandlerManager
{
    /**
     * @var AAjaxHandler $handler
     */
	protected $handler;
	
	protected $method = 'ajaxStart';

	public function __construct(IAjaxHandler $ajaxHandler)
	{
		$this->setHandler($ajaxHandler);
	}

	public function setMethod($method)
	{
		$this->method = $method;
	}

	public function setHandler(IAjaxHandler $ajaxHandler)
	{
		$this->handler = $ajaxHandler;
	}

	public function start()
	{
	    try
        {
            $response = $this->handler->{$this->method}();
        } catch (\Throwable $e) {
            $this->handler->responseError(($e->getCode()?"[{$e->getCode()}] ":'') . $e->getMessage(), 200);
            $this->handler->getResponse()->getErrors()->add($e);
            return $this->handler->getResponse();
        }
        if(is_null($response)){
            return $this->handler->getResponse();
        }

        return $response;
    }

	public function getHandler()
	{
		return $this->handler;
	}
}