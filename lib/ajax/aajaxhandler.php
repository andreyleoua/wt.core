<?php

namespace Wt\Core\Ajax;

use Wt\Core\Interfaces\IAjaxHandler;
use Wt\Core\Interfaces\IResponse;
use Wt\Core\Support\Response;

abstract class AAjaxHandler implements IAjaxHandler
{
    protected $request;

    /**
     * @var Response
     */
	protected $response;

	public function __construct()
    {
		$this->makeDefaultResponse();
		$this->makeDefaultRequest();
	}

	public function setResponse(IResponse $response)
	{
		$this->response = $response;
	}

	public function getRequest()
	{
		return $this->request;
	}

	protected function makeDefaultResponse()
    {
		$this->setResponse(resolve(Response::class));
        return $this;
	}

	protected function makeDefaultRequest()
    {
		$this->request = new \stdClass();
		foreach ($_REQUEST as $key => $value) {
			$m = $key;
			$this->request->$m = $value;
		}
	}

	public function setResponseData($data)
	{
		if( !is_array($data) )
			$data = [$data];
		$this->getResponse()->setData($data);
	}

	public function responseSuccess($message, $httpCode = 200)
	{
		$this->getResponse()->setHttpCode($httpCode);
		return $this->getResponse()->setSuccessMessage($message);
	}

	public function responseError($message, $httpCode = 200)
	{
		$this->getResponse()->setHttpCode($httpCode);
		return $this->getResponse()->setErrorMessage($message);
	}

    /**
     * @return Response
     */
    public function getResponse()
    {
		return $this->response;
	}

	public function __toString()
	{
		return $this->getResponse();
	}
}