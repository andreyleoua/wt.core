<?php


namespace Wt\Core\Ajax;


class Test extends AAjaxHandler
{
    public function ajaxStart()
    {
        $this->getResponse()->addData(['method' => 'ajaxStart']);
    }

    public function test()
    {
        $this->getResponse()->addData(['method' => 'test']);
        return $this->getResponse();
    }
}