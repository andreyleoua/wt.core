<?php


namespace Wt\Core\IFields;


class UserField
{
    public $id;
    public $xmlId;
    public $active;
    public $email;
    public $login;
    public $name;
    public $lastName;
    public $secondName;
    public $personalPhone;
    public $personalMobile;
    public $externalAuthId;
    public $phoneNumber;
}