<?php


namespace Wt\Core\IFields;


class FormFields
{
    public $acceptCharset;
    public $action;
    public $autocomplete;
    public $enctype;
    public $method;
    public $name;
    public $novalidate;
    public $rel;
    public $target;
}