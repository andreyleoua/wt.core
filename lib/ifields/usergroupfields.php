<?php


namespace Wt\Core\IFields;


class UserGroupFields
{
    public $id;
    public $timestampX;
    public $active;
    public $cSort;
    public $anonymous;
    public $isSystem;
    public $name;
    public $description;
    public $securityPolicy;
    public $stringId;
}