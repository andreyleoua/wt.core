<?php


namespace Wt\Core\IFields;


/**
 * https://dev.1c-bitrix.ru/api_help/main/reference/csite/index.php#flds
 */
class SiteFields
{
    public $id;
    public $lid;
    public $sort;
    public $def;
    public $active;
    public $name;
    public $dir;
    public $formatDate;
    public $formatDatetime;
    public $charset;
    public $docRoot;
    public $serverName;
    public $siteName;
    public $domains;
    public $cultureId;
    public $absDocRoot;
    public $siteUrl; // ABS_DOC_ROOT != $_SERVER["DOCUMENT_ROOT"]
}