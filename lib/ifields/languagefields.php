<?php


namespace Wt\Core\IFields;


/**
 * https://dev.1c-bitrix.ru/api_help/main/reference/clanguage/index.php
 *
 * array (
    'LID' => 'ru',
    'SORT' => '1',
    'DEF' => 'Y',
    'ACTIVE' => 'Y',
    'NAME' => 'Russian',
    'FORMAT_DATE' => 'DD.MM.YYYY',
    'FORMAT_DATETIME' => 'DD.MM.YYYY HH:MI:SS',
    'FORMAT_NAME' => '#NAME# #LAST_NAME#',
    'WEEK_START' => '1',
    'CHARSET' => 'UTF-8',
    'DIRECTION' => 'Y',
    'CULTURE_ID' => '1',
    'ID' => 'ru',
    'LANGUAGE_ID' => 'ru',
    )
 *
 */
class LanguageFields
{
    public $id;
    public $lid;
    public $sort;
    public $def;
    public $active;
    public $name;
    public $cultureId;
}