<?php


namespace Wt\Core\IFields;


class UserFieldEnumFields
{
    public $id;
    public $userFieldId;
    public $value;
    public $def;
    public $sort;
    public $xmlId;
}