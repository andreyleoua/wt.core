<?php


namespace Wt\Core\IFields;


class IBlockTypeFields
{
    public $id;
    public $sections;
    public $editFileBefore;
    public $editFileAfter;
    public $inRss;
    public $sort;
}