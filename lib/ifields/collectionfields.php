<?php


namespace Wt\Core\FactoryProxy;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Interfaces\IFields;
use Wt\Core\Type\Str;

class CollectionFields implements IFields
{

    /**
     * @var AEntityCollection
     */
    protected $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function __call($name, $arguments)
    {
         return call_user_func_array([$this->object, $name], $arguments);
    }

    public function __get($name)
    {
        $code = Str::camelToSnake($name, $upper = true);
        return $this->object->getByCode($code);
    }

}