<?php


namespace Wt\Core\IFields\Main;


class FileFields
{
    public $id;
    public $code; // path
    public $xmlId; // $externalId
    public $timestampX;
    public $moduleId;
    public $height;
    public $width;
    public $fileSize;
    public $contentType;
    public $subdir;
    public $fileName;
    public $originalName;
    public $description;
    public $handlerId;
    public $externalId;
}