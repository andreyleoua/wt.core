<?php

namespace Wt\Core\IFields\Main;

use Wt\Core\IFields\AFields;

class SitemapFields extends AFields
{
    public $id;
    public $timestampX;
    public $siteId;
    public $active;
    public $name;
    public $dateRun;
    public $settings;
}