<?php


namespace Wt\Core\IFields;


use Wt\Core\Interfaces\IFields;

class AFields implements IFields
{
    public $id;
    public $code;
    public $xmlId;
    public $name;
    public $sort;
    public $active;
}