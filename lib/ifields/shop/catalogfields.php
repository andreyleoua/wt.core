<?php

namespace Wt\Core\IFields\Shop;

class CatalogFields
{
    public $iblockId;
    public $productIblockId;
    public $skuPropertyId;
    public $vatId;
    public $catalogType;
    public $catalog; // Y|N
}