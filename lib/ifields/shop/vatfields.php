<?php


namespace Wt\Core\IFields\Shop;


class VatFields
{
    public $id;
    public $timestampX;
    public $active;
    public $sort;
    public $name;
    public $rate;
}