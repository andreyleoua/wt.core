<?php


namespace Wt\Core\IFields\Shop;


use Wt\Core\Interfaces\IFields;

class CurrencyFields implements IFields
{
    public $id; // = $currency
    public $code; // = $currency
    public $currency;
    public $amountCnt;
    public $amount;
    public $sort;
    public $dateUpdate;
    public $numcode;
    public $base;
    public $createdBy;
    public $dateCreate;
    public $modifiedBy;
    public $currentBaseRate;
}