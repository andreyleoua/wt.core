<?php


namespace Wt\Core\IFields\Shop;


class ProductCatalogFields
{
    public $id;
    public $quantity;
    public $priceType;
    public $vatId;
    public $vatIncluded;
    public $purchasingPrice;
    public $purchasingCurrency;
    public $quantityTrace;
    public $canBuyZero;
    public $negativeAmountTrace;
    public $type;
    public $measure;
}