<?php


namespace Wt\Core\IFields\Shop;


class ProductStoreFields
{
    public $id;
    public $amount;
    public $storeId;
    public $productId;
}