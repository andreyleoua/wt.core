<?php


namespace Wt\Core\IFields\Shop;


class StoreFields
{
    public $id;
    public $xmlId;
    public $active;
    public $sort;
    public $code;
    public $title;
    public $address;
    public $description;
    public $locationId;
    public $phone;
    public $schedule;
    public $email;
    public $issuingCenter;
    public $shippingCenter;
    public $siteId;
}