<?php


namespace Wt\Core\IFields\Shop;


class PriceTypeFields
{
    public $id;
    public $name; // Код
    public $base;
    public $sort;
    public $xmlId;
    public $timestampX;
    public $modifiedBy;
    public $dateCreate;
    public $createdBy;
}