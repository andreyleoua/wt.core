<?php


namespace Wt\Core\IFields\Shop;


class MeasureFields
{
    public $id;
    public $code; // 6
    public $measureTitle; // Метр
    public $symbolRus; // м
    public $symbolIntl; // m
    public $symbolLetterIntl; // MTR
    public $isDefault; // Y|N
    public $symbol; // м
}