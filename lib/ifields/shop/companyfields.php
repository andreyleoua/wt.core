<?php


namespace Wt\Core\IFields\Shop;


class CompanyFields
{
    public $id;
    public $code;
    public $xmlId;
    public $name;
    public $locationId;
    public $active;
    public $address;
    public $sort;
}