<?php


namespace Wt\Core\IFields\Shop;


class BasketFields
{
    public $id;
    public $lid;
    public $fuserId;
    public $orderId;
    public $productId;
    public $productPriceId;
    public $priceTypeId;
    public $name;
    public $currency;
    public $vatIncluded; // N|Y
    public $dateInsert;
    public $dateUpdate;
    public $dateRefresh;
    public $weight;
    public $quantity;
    public $delay; // N|Y
    public $summaryPrice;
    public $canBuy; // N|Y
    public $markingCodeGroup;
    public $module;
    public $productProviderClass;
    public $notes;
    public $detailPageUrl;
    public $discountPrice;
    public $catalogXmlId;
    public $productXmlId;
    public $discountName;
    public $discountValue;
    public $discountCoupon;
    public $vatRate;
    public $subscribe; // N|Y
    public $nSubscribe;
    public $reserved; // N|Y
    public $reserveQuantity;
    public $barcodeMulti; // N|Y
    public $customPrice; // N|Y
    public $dimensions;
    public $type;
    public $setParentId;
    public $measureCode;
    public $measureName;
    public $callbackFunc;
    public $orderCallbackFunc;
    public $cancelCallbackFunc;
    public $payCallbackFunc;
    public $recommendation;
    public $sort;
    public $xmlId;
}