<?php

namespace Wt\Core\IFields\Shop;

class CatalogViewedProductFields
{
    public $id;
    public $fuserId;
    public $dateVisit;
    public $productId;
    public $elementId;
    public $siteId;
    public $viewCount;
    public $recommendation;
}