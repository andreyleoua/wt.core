<?php


namespace Wt\Core\IFields\Shop;


class ProductPriceOptimalFields
{
    public $id;
    public $productId;
    public $elementIblockId; // noDB
    public $catalogGroupId; // тип цены
    public $priceTypeId; // noDB
//    public $quantityFrom;
//    public $quantityTo;
    public $basePrice; // noDB
    public $discountPrice; // noDB
    public $discount; // noDB float
    public $percent; // noDB
    public $vatRate; // noDB
    public $vatIncluded; // noDB
    public $unroundBasePrice; // noDB
    public $unroundDiscountPrice; // noDB
    public $discountSimpleList; // noDB array
    public $discountList; // noDB array
//    public $extraId;
    public $price;
    public $currency;
//    public $priceScale;
//    public $timestampX;
}