<?php


namespace Wt\Core\IFields\Shop;


class ProductPriceFields
{
    public $id;
    public $productId;
    public $catalogGroupId; // тип цены
    public $quantityFrom;
    public $quantityTo;
    public $extraId;
    public $price;
    public $currency;
    public $priceScale;
    public $timestampX;
}