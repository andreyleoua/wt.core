<?php


namespace Wt\Core\IFields;


class AgentFields
{
    public $id;
    public $moduleId;
    public $userId;
    public $login;
    public $userName;
    public $lastName;
    public $sort;
    public $name;
    public $active;
    public $running;
    public $lastExec;
    public $nextExec;
    public $dateCheck;
    public $agentInterval;
    public $isPeriod;
    public $retryCount;
}

//Array
//(
//    [ID] => 26
//    [MODULE_ID] => landing
//    [USER_ID] =>
//    [LOGIN] =>
//    [USER_NAME] =>
//    [LAST_NAME] =>
//    [SORT] => 100
//    [NAME] => Bitrix\Landing\Agent::clearRecycle();
//    [ACTIVE] => Y
//    [RUNNING] => N
//    [LAST_EXEC] => 22.06.2022 11:33:51
//    [NEXT_EXEC] => 22.06.2022 13:33:51
//    [DATE_CHECK] =>
//    [AGENT_INTERVAL] => 7200
//    [IS_PERIOD] => N
//    [RETRY_COUNT] => 0
//)