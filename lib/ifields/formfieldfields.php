<?php


namespace Wt\Core\IFields;


class FormFieldFields
{
    public $id;
    public $name;
    public $type;
    public $value;
    public $placeholder;
    public $autofocus;
    public $autocomplete;
    public $title;
    public $label;

    /** bool */
    public $readonly;
    public $disabled;
    public $required;
    public $multiple;
}