<?php


namespace Wt\Core\IFields\IBlock;


class PropertyFields
{
    public $id;
    public $timestampX;
    public $iblockId;
    public $name;
    public $active;
    public $sort;
    public $code;
    public $defaultValue;
    public $propertyType;
    public $rowCount;
    public $colCount;
    public $listType;
    public $multiple;
    public $xmlId;
    public $fileType;
    public $multipleCnt;
    public $tmpId;
    public $linkIblockId;
    public $withDescription;
    public $searchable;
    public $filtrable;
    public $isRequired;
    public $version;
    public $userType;
    public $userTypeSettings;
    public $hint;
}