<?php


namespace Wt\Core\IFields\IBlock;


use Wt\Core\IFields\AFields;

class SectionFields extends AFields
{
    public $timestampX;

    public $iblockId;

    public $iblockSectionId;

    public $active;

    public $globalActive;

    public $timeStampX;

    public $modifiedBy;

    public $dateCreate;
    
    public $picture;
}