<?php


namespace Wt\Core\IFields\IBlock;


class SectionPropertyFields
{
    public $id;
    public $iblockId;
    public $sectionId;
    public $propertyId;
    public $smartFilter;
    public $displayType;
    public $displayExpanded;
    public $filterHint;
}