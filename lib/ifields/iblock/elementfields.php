<?php


namespace Wt\Core\IFields\IBlock;


use Wt\Core\IFields\AFields;

class ElementFields extends AFields
{
    public $timestampX;
    public $timestampXUnix;

    public $iblockId;

    public $iblockSectionId;

    public $active;

    public $modifiedBy;

    public $dateCreate;
    public $dateCreateUnix;

    public $detailText;

    public $previewText;

    public $picture;
    public $previewPicture;
    public $detailPicture;
}