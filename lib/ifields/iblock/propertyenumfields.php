<?php


namespace Wt\Core\IFields\IBlock;


class PropertyEnumFields
{
    public $id;
    public $propertyId;
    public $value;
    public $def;
    public $sort;
    public $xmlId;
    public $tmpId;
    public $externalId;
    public $propertyName;
    public $propertyCode;
    public $propertySort;
}