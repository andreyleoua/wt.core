<?php


namespace Wt\Core\IFields\IBlock;


class PropertyValueFields
{
    public $id;
    public $value;
    public $valueEnum;
    public $valueNum;
    public $description;
}