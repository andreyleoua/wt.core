<?php


namespace Wt\Core\IFields\HlBlock;


class ElementFields
{
    public $id;
    public $xmlId;
    public $name;
    public $sort;
    public $link;
    public $def;
    public $file;
    public $description;
    public $fullDescription;
}