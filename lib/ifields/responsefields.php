<?php


namespace Wt\Core\IFields;


use Wt\Core\Support\Collection;

class ResponseFields
{
    public $status;
    /** @var Collection */
    public $errors;
    /** @var Collection */
    public $data;
    public $message;
    public $httpCode;
}