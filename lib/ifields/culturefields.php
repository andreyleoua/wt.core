<?php


namespace Wt\Core\IFields;


class CultureFields
{
    public $id;
    public $code;
    public $name;
    public $formatDate;
    public $formatDatetime;
    public $formatName;
    public $weekStart;
    public $charset;
    public $direction;
    public $shortDateFormat;
    public $mediumDateFormat;
    public $longDateFormat;
    public $fullDateFormat;
    public $dayMonthFormat;
    public $shortTimeFormat;
    public $longTimeFormat;
    public $amValue;
    public $pmValue;
    public $numberThousandsSeparator;
    public $numberDecimalSeparator;
    public $numberDecimals;
}