<?php


namespace Wt\Core\IFields;


use Wt\Core\Support\Collection;

class IBlockFields
{
    public $id;
    public $iblockTypeId;
    public $lid;
    public $code;
    public $name;
    public $active;
    public $rightsMode;
    public $sectionProperty;
    public $version;
    /**
     * @var $timestampX \Bitrix\Main\Type\DateTime
     */
    public $timestampX;
    // public $elementCnt;
}