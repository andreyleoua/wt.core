<?php


namespace Wt\Core\IFields;

/**
 * https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=3496
 */
class UserFieldFields
{
    public $id;
    public $code;
    public $fieldName;
    public $userTypeId;
    public $xmlId;
    public $sort;
    public $multiple;
    public $mandatory;
    /**
     * Показывать в фильтре списка. Возможные значения:
     * не показывать = N,
     * точное совпадение = I,
     * поиск по маске = E,
     * поиск по подстроке = S
     */
    public $showFilter;
    /** Не показывать в списке. Если передать какое-либо значение, то будет считаться, что флаг выставлен. */
    public $showInList;
    /** Пустая строка разрешает редактирование. Если передать какое-либо значение, то будет считаться, что флаг выставлен. */
    public $editInList;
    public $isSearchable;

    /**
     * массив
     *
     * 'DEFAULT_VALUE' => '',   // Значение по умолчанию
     * 'SIZE'          => '20', // Размер поля ввода для отображения
     * 'ROWS'          => '1',  // Количество строчек поля ввода
     * 'MIN_LENGTH'    => '0',  // Минимальная длина строки (0 - не проверять)
     * 'MAX_LENGTH'    => '0',  // Максимальная длина строки (0 - не проверять)
     * 'REGEXP'        => '',   // Регулярное выражение для проверки
     */
    public $settings;

    /** Подпись в форме редактирования */
    public $editFormLabel;
    /**
     * Заголовок в списке
     *
     * array(
        'ru'    => 'Пользовательское свойство',
        'en'    => 'User field',
        )
     */
    public $listColumnLabel;
    /**
     * Подпись фильтра в списке
     */
    public $listFilterLabel;
    public $errorMessage;
    public $helpMessage;
}