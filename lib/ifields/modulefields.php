<?php


namespace Wt\Core\IFields;


class ModuleFields
{
    public $id;
    public $prefix;
    public $langPrefix;
    public $dir;
    public $extDir;
    public $namespace;
    public $gitUrl;
    public $version;
    public $date;
}