<?php

namespace Wt\Core\Agent;

use Wt\Core\Entity\AgentEntity;

/**
 * 09/09/2023
 * 26/10/2023
 */
abstract class AAgentCase
{

    /**
     * должен венуть массив вида
     * [
     *      static function name 1 => [параметры агента],
     *      static function name N => [параметры агента],
     * ]
     *
     * @return array
     */
    abstract public function getAgentConfig();


    /**
     * отдает время релевантное для указанных $h:$m
     *
     * @param $h
     * @param $m
     * @return false|string
     */
    protected function getNextExec($h, $m)
    {
        $interval = 3600 * 24;
        $timestamp = mktime($h, $m, 0, date('n'), date('j'), date('Y'));
        if (time() > $timestamp) {
            $timestamp += $interval;
        }
        return ConvertTimeStamp($timestamp, 'FULL');
    }

    /**
     * отдвет время, которое наступит через $m минут
     *
     * @param int $minuts
     * @return false|string
     */
    protected function getNextExecLater($minuts = 1)
    {
        $interval = 60;
        $timestamp = mktime(
            (int)date('H'),
            (int)date('i') + $minuts,
            0,
            (int)date('n'),
            (int)date('j'),
            (int)date('Y')
        );
        if ($timestamp - time() < 5) {
            $timestamp += $interval;
        }
        return ConvertTimeStamp($timestamp, 'FULL');
    }

    protected function getTomorrowMidnightDataTime()
    {
        $timestamp = mktime(0, 0, 0, date('n'), (int)date('j') + 1);
        return ConvertTimeStamp($timestamp, 'FULL');
    }

    public function isExists()
    {
        $list = $this->getAgentList();

        if (!count($list)) {
            return false;
        }

        foreach ($list as $method => $agentEntity) {
            if (!$agentEntity->isExists()) {
                return false;
            }
        }
        return true;
    }

    public function isWorking()
    {
        $list = $this->getAgentList();

        if (!count($list)) {
            return false;
        }

        foreach ($list as $method => $agentEntity) {
            if (!$agentEntity->isWorking()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return AgentEntity[]
     */
    public function getAgentList()
    {
        $r = [];
        foreach ($this->getAgentConfig() as $method => $data) {

            if (!is_iterable($data)) {
                continue;
            }

            $def = [
                'MODULE_ID' => app()->module()->getId(),
                'AGENT_INTERVAL' => 86400,
                'IS_PERIOD' => 'Y',
                'ACTIVE' => 'Y',
                'SORT' => 500,
                'USER_ID' => false,
                'NEXT_EXEC' => $this->getTomorrowMidnightDataTime(),
            ];
            if (!array_key_exists('NAME', $data) && is_callable([static::class, $method])) {
                $data['NAME'] = static::class . "::$method(\$arAgent);";
            }

            $r[$method] = new AgentEntity(array_merge($def, $data));
        }
        return $r;
    }

    public function add()
    {
        $factory = app()->factory()->main()->getAgentFactory();

        $this->delete();

        foreach ($this->getAgentList() as $method => $agentEntity) {
            $factory->add($agentEntity->getRaw());
        }
    }

    public function delete()
    {
        $filter = ['NAME' => '%' . static::class . '%'];
        $factory = app()->factory()->main()->getAgentFactory();

        $factory->getCollection($filter)->each(function (AgentEntity $entity) {
            $entity->delete();
        });
    }
}