<?php


namespace Wt\Core\Factory;


use Exception;
use Wt\Core\Ajax\AjaxHandlerManager;
use Wt\Core\Ajax\ErrorHandler;
use Wt\Core\Interfaces\IAjaxHandler;
use Wt\Core\Support\Result;

class AjaxFactory
{

    /**
     * @param $className
     * @param $method
     * @return Result
     */
    protected function getAjaxManager($className, $method)
    {
        foreach (app()::getInstances() as $app){
            $config = $app->config('ajax', []);
            $namespaces = $config->get('namespaces_autoresolve', [$app->module()->getNamespace() . '\\Ajax'])->toArray();
            $cn = null;


            $tempClassName = $config->get('alias.'.$className);
            if( is_a($tempClassName, IAjaxHandler::class, true) && method_exists($tempClassName, $method) ){
                $cn = $tempClassName;
            } else {
                foreach ($namespaces as $namespace) {
                    $tempClassName = trim($namespace, '\\').'\\'.$className;
                    if( is_a($tempClassName, IAjaxHandler::class, true) && method_exists($tempClassName, $method) ){
                        $cn = $tempClassName;
                        break;
                    }
                }
            }

            if( $cn ){
                $result = new Result();
                try {
                    $ajaxManager = new AjaxHandlerManager(resolve($cn));
                    $ajaxManager->setMethod($method);
                    $result->setData([
                        'ajaxManager' => $ajaxManager,
                    ]);
                    return $result;
                } catch (Exception $e ) {
                    $result->setError('class signature error: ' .$e->getMessage());
                    return $result;
                }
            }
        }
        $result = new Result();
        $result->setError('Class or method not found');

        return $result;
    }

    /**
     * @param $path
     * @return AjaxHandlerManager
     * @throws Exception
     */
	public function getEntity($path)
    {
        $path = trim($path, '/');
        $arPath = explode('/', $path);

        $className = implode('\\', $arPath);
        $method = 'ajaxStart';

        $result = $this->getAjaxManager($className, $method);

        if(!$result->isSuccess()){
            $method = array_pop($arPath);
            $className = implode('\\', $arPath);
            $result = $this->getAjaxManager($className, $method);
        }

        if(!$result->isSuccess()){
            $defaultHandlerClass = app()->module()->getNamespace() . '\\Ajax\\ErrorHandler';
            /**
             * @var ErrorHandler $defaultHandler
             */
            $defaultHandler = resolve($defaultHandlerClass);
            $defaultHandler->responseError('Ajax Router Failed');
            $defaultHandler->getResponse()->getErrors()->setErrors($result->getErrors());
            $ajaxManager = new AjaxHandlerManager($defaultHandler);
            $ajaxManager->setMethod('ajaxStart');
            return $ajaxManager;
        }

		return $result->getData()['ajaxManager'];
	}
}