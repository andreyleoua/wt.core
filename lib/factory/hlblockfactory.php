<?php


namespace Wt\Core\Factory;


use Wt\Core\Collection\HlBlockCollection;
use Wt\Core\Entity\HlBlockEntity;

class HlBlockFactory extends AFactory
{

    protected function __getDepsModules()
    {
        return ['highloadblock'];
    }

    public function __getCachePrefix()
    {
        return 'hlblock/block';
    }

    public function __getEntityClass()
    {
        return HlBlockEntity::class;
    }

    public function __getCollectionClass()
    {
        return HlBlockCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request HlBlockFactory " . serialize($filter));

        $dbTable = \Bitrix\Highloadblock\HighloadblockTable::getList([
            'filter' => $filter,
            'order' => $order,
            'select' => $select?:['*'],
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $result = [];
        while($res = $dbTable->fetch()){
            if(isset($res['NAME'])){
                $res['CODE'] = $res['NAME'];
            }
            if(isset($res['TABLE_NAME'])){
                $res['XML_ID'] = $res['TABLE_NAME'];
            }
            $result[$res['ID']] = $res;
        }

        return $result;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return HlBlockCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return HlBlockEntity
     * @throws null
     */
    public function getEntityById($id)
    {
        $id = (int)$id;
        $res = $this->getList(['=ID' => $id]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return HlBlockCollection
     * @throws null
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['@ID' => $arIds]);
        }
        /**
         * @var HlBlockCollection $collection
         */
        $collection = resolve($this->__getCollectionClass(), [$res]);
        return $collection->walk([$this, 'getEntityByResult']);
    }

    /**
     * @param $name
     * @return HlBlockEntity
     * @throws null
     */
    public function getEntityByCode($name)
    {
        $res = $this->getList(['=NAME' => $name]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param $tableName
     * @return HlBlockEntity
     * @throws null
     */
    public function getEntityByXmlId($tableName)
    {
        $res = $this->getList(['=TABLE_NAME' => $tableName]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return HlBlockEntity
     * @throws \Exception
     */
    public function getEntityByResult(array $arResult)
    {
        if(isset($arResult['NAME'])){
            $arResult['CODE'] = $arResult['NAME'];
        }
        if(isset($arResult['TABLE_NAME'])){
            $arResult['XML_ID'] = $arResult['TABLE_NAME'];
        }
        return parent::getEntityByResult($arResult);
    }
}