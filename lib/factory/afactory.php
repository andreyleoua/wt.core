<?php


namespace Wt\Core\Factory;


use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Exception;
use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\FactoryProxy\ACacheFactoryProxy;
use Wt\Core\Interfaces\ICacheFactory;
use Wt\Core\Interfaces\ICacheFactoryFacade;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Interfaces\IReverseProxyFactory;
use Wt\Core\Support\FactoryMapper;
use Wt\Core\Support\Result;

abstract class AFactory implements IFactory, ICacheFactory, ICacheFactoryFacade, IReverseProxyFactory
{
    use TReverseProxyFactory;

    /** @var FactoryMapper  */
    protected $mapper;
    /** @var bool */
    protected $isCollectionQuery;

    protected $isBitrixModulesLoaded;

    public function __construct()
    {
        $this->mapper = resolve(FactoryMapper::class);
        $this->mapper->setFactory($this);
        $this->__includeDeps();
    }

    /**
     * @return FactoryMapper
     */
    public function __mapper()
    {
        return $this->mapper;
    }

    abstract public function __getEntityClass();
    abstract public function __getCachePrefix();
    public function __getUserFieldEntityId()
    {
        return null;
    }

    /**
     * @return \Bitrix\Main\ORM\Fields\Field[]|\Bitrix\Main\Entity\Field[]
     */
    public function __getOrmMap()
    {
        return [];
    }

    /**
     * @param $data
     * @return Result
     * @throws null
     * @deprecated not realized
     */
    public function add($data)
    {
        $result = Result::make();
        $result->addError($errMessage = 'error add: method add undefined');
        $result->setData(['fields' => $data]);
        throw new Exception($errMessage);

        return $result;
    }

    /**
     * @param $id
     * @param $data
     * @return Result
     * @throws null
     * @deprecated not realized
     */
    public function update($id, $data)
    {
        $result = Result::make();
        $result->setId($id);
        $result->setData(['fields' => $data]);
        $result->addError($errMessage = 'error update: method update undefined');
        throw new Exception($errMessage);

        return $result;
    }

    /**
     * @param $id
     * @return Result
     * @throws null
     * @deprecated not realized
     */
    public function delete($id)
    {
        $result = Result::make();
        $result->setId($id);
        $result->addError($errMessage = 'error delete: method delete undefined');

        throw new Exception($errMessage);

        return $result;
    }

    /**
     * @param string|int $id
     * @return AEntity|null
     */
    abstract public function getEntityById($id);
    abstract public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null);

    /**
     * суть в полечении Entity через коллекции в рамках CacheFactory
     * есть смысл ставить true в "ограниченных" списках: Enum, IBlock, IBlockType и тд.
     * В этом случае не будет отдельных запросов на отдельные элементы, а так же не будут формироваться кеши для Entity
     *
     * @return bool
     */
    public function __isCollectionQuery()
    {
        if(is_null($this->isCollectionQuery)){
            $this->isCollectionQuery = $this->getConfig('is_collection_query', false);
        }
        return $this->isCollectionQuery;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param int|null $limit
     * @param int|null $offset
     * @return AEntityCollection|AEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        $list = call_user_func_array([$this, 'getList'], func_get_args());

        return $this->getEntityByResults($list);
    }

    /**
     * @return array
     */
    protected function __getDepsModules()
    {
        return [];
    }

    /**
     * @throws LoaderException
     */
    protected function __includeDeps()
    {
        if(!is_null($this->isBitrixModulesLoaded)){
            return $this->isBitrixModulesLoaded;
        }
        $this->isBitrixModulesLoaded = true;
        foreach ($this->__getDepsModules() as $module){
            if(!Loader::includeModule($module)){
                $this->isBitrixModulesLoaded = false;
                throw new \Exception("module $module not include");
            }
        }
        return $this->isBitrixModulesLoaded;
    }

    public function __getCollectionClass()
    {
        return AEntityCollection::class;
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Result|\CDBResult|\mysqli_result $queryResult
     * @return AEntityCollection
     */
    public function getEntityByQueryResult($queryResult)
    {
        $result = [];
        if(is_array($queryResult->arResult)){
            reset($queryResult->arResult);
        }
        while($res = $queryResult->fetch()){
            $result[$res['ID']] = $res['ID'];
        }

        return $this->getEntityByIds($result);
    }

    /**
     * @param array $arResult
     * @return AEntity
     * @throws null
     */
    public function getEntityByResult(array $arResult)
    {
        /** @var AEntity $entity */
        $entity = resolve($this->__getEntityClass(), [$arResult]);
        $entity->setFactory($this);
        $this->__mapper()->makeMapper($entity);
        return $entity;
    }

    /**
     * @param array $arIds
     * @return AEntityCollection|AEntity[]
     * @throws null
     * @deprecated not support
     */
    public function getEntityByIds(array $arIds)
    {
        throw new Exception('This method is closed in AFactory', 1);
    }

    /**
     * @param $xmlId
     * @return AEntity
     * @throws null
     * @deprecated not support
     */
    public function getEntityByXmlId($xmlId)
    {
        throw new Exception('This method is closed in AFactory', 1);
    }

    /**
     * @param $code
     * @return AEntity
     * @throws null
     * @deprecated not support
     */
    public function getEntityByCode($code)
    {
        throw new Exception('This method is closed in AFactory', 1);
    }

    public function getEntityByResults(array $arResults)
    {
        /**
         * @var AEntityCollection $collection
         */
        $collection = resolve($this->__getCollectionClass(), [$arResults]);
        $collection->walk([$this, 'getEntityByResult']);
        $collection->__setFactory($this);

        return $collection;
    }

    /**
     * @param array $find
     * @param array $data
     * @return AEntity|null
     */
    public function firstOrCreate($find, $data = [])
    {

        $collection = $this->getCollection($find, [], [], 1, 0);
        if($collection->first()){
            return $collection->first();
        }
        $array = (is_array($data)&&$data)?$data:$find;
        $result = $this->add($array);

        if($result->isSuccess() && $result->getId()){
            return $this->getEntityByResult($result->getPrimary() + $result->getData()->toArray());
        }

        return null;
    }

    /**
     * @param array $find
     * @param array $data
     * @return Result
     * @throws null
     */
    public function updateOrCreate($find, $data = [])
    {

        $collection = $this->cache([])->getCollection($find, [], [], 1, 0);
        if($entity = $collection->first()){
            $result = $entity->update($data);
            return $result;
        }

        $array = (is_array($data)&&$data)?$data:$find;
        $result = $this->add($array);

        //if($result->isSuccess() && $result->getId()){
            //$entity = $this->cache([])->getEntityByResult($result->getPrimary() + $result->getData()->toArray());
        //}

        return $result;
    }

    public function instanceOf($className)
    {
        return $this instanceof $className;
    }

    public function clearCache()
    {
        return $this;
    }

    public function clearCacheById($id, $collectionId = null)
    {
        app()->config('cache_factory', [])->map(function($item){
            if(is_iterable($item)){
                return $item['class'];
            }
            return $item;
        })->filter(function($item){
            return class_exists($item);
        })->unique()->each(function ($item) use ($id) {
            /**
             * @var ACacheFactoryProxy $x
             */
            $x = resolve($item);
            $x->setFactory($this);
            $x->clearCacheById($id, false);
        });
    }

    /**
     * @param iterable $types
     * @return $this
     * @throws Exception
     */
    public function cache($types)
    {
        return CacheFactory::cache($this, $types);
    }

    public function __stat($return = false)
    {
        if($return){
            return (array)$GLOBALS['FACTORY_STAT'][$this->__getCachePrefix()];
        }
        return ++$GLOBALS['FACTORY_STAT'][$this->__getCachePrefix()]['COUNT'];
    }

    protected function __getArNavStartParams($limit = null, $offset = null)
    {
        if(is_null($limit) && is_null($offset)){
            $arNavStartParams = false;
        } else {
            $arNavStartParams = [];
            if(!is_null($limit)){
                $arNavStartParams['nPageSize'] = $limit;
                $arNavStartParams['iNumPage'] = round($offset / $limit) + 1;
            }
        }
        return $arNavStartParams;
    }
}