<?php

namespace Wt\Core\Factory;

use Bitrix\Main\Entity\DataManager;
use Wt\Core\Entity\AEntity;
use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Support\Result;

abstract class AORMFactory extends AFactory
{

    /**
     * @param $data
     * @return Result
     * @throws null
     */
    public function add($data)
    {
        $bxResult = $this->getDataManager()::add($data);
        return Result::makeByBxResult($bxResult);
    }

    /**
     * @param $id
     * @param $data
     * @return Result
     * @throws null
     */
    public function update($id, $data)
    {
        $bxResult = $this->getDataManager()::update($id, $data);
        return Result::makeByBxResult($bxResult);
    }

    /**
     * @param $id
     * @return Result
     * @throws null
     */
    public function delete($id)
    {
        $bxResult = $this->getDataManager()::delete($id);
        return Result::makeByBxResult($bxResult);
    }

    /**
     * @return string
     */
    abstract protected function __getDataManagerClass();

    /**
     * @return DataManager
     * @throws null
     */
    public function getDataManager()
    {
        app()->container()->singleton($this->__getDataManagerClass());
        return resolve($this->__getDataManagerClass());
    }

    /**
     * @return \Bitrix\Main\ORM\Query\Query|\Bitrix\Main\Entity\Query
     */
    public function getQuery()
    {
        $ormEntity = $this->getDataManager()::getEntity();
        $query = new \Bitrix\Main\Entity\Query($ormEntity);
        $query
            ->setSelect($ormEntity->getPrimaryArray());

        return $query;
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Query|\Bitrix\Main\Entity\Query|null $query
     * @return AEntityCollection|AEntity[]
     */
    public function getEntityByQuery($query = null)
    {
        if(is_null($query)){
            $query = $this->getQuery();
        }
        $ormEntity = $this->getDataManager()::getEntity();
        $query
            ->setSelect($ormEntity->getPrimaryArray());
        $queryResult = $query->exec();

        return $this->getEntityByQueryResult($queryResult);
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Result|\CDBResult|\mysqli_result $queryResult
     * @return AEntityCollection|AEntity[]
     */
    public function getEntityByQueryResult($queryResult)
    {
        $result = [];
        while($res = $queryResult->fetch()){
            $result[$res['ID']] = $res['ID'];
        }

        return $this->getEntityByIds($result);
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        if(!$select){
            $select = ['*'];
        }
        $queryResult = $this->getDataManager()::getList([
            'filter' => $filter,
            'order' => $order,
            'select' => $select,
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $result = [];
        while($res = $queryResult->fetch()){
            $id = $res[$this->getPrimary()];
            if(!is_null($id)){
                $result[$id] = $res;
            } else {
                $result[] = $res;
            }
        }

        return $result;
    }

    public function getPrimary()
    {
        return $this->getDataManager()::getEntity()->getPrimary();
    }

    /**
     * @param $id
     * @return AEntity|null
     */
    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }
        $res = $this->getList(['=ID' => $id]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param $code
     * @return AEntity|null
     * @deprecated if a method is needed, then redefine
     */
    public function getEntityByCode($code)
    {
        if(!$code){
            return null;
        }
        $res = $this->getList(['=CODE' => $code]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return AEntity|null
     * @deprecated if a method is needed, then redefine
     */
    public function getEntityByXmlId($xmlId)
    {
        if(!$xmlId){
            return null;
        }
        $res = $this->getList(['=XML_ID' => $xmlId]);
        if( $res ){
            return $this->getEntityByResult(reset($res));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return AEntityCollection|AEntity[]
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['@ID' => $arIds]);
        }
        return $this->getEntityByResults($res);
    }
}