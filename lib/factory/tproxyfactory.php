<?php


namespace Wt\Core\Factory;


use Wt\Core\Interfaces\IFactory;

trait TProxyFactory
{

    protected $factory;

    public function getFactory()
    {
        return $this->factory;
    }

    public function setFactory(IFactory $factory)
    {
        $this->factory = $factory;
    }

    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->factory, $method], $arguments);
    }
}