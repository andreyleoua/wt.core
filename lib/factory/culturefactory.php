<?php


namespace Wt\Core\Factory;


use Wt\Core\Collection\CultureCollection;
use Wt\Core\Entity\CultureEntity;

class CultureFactory extends AORMFactory
{

    public function __getEntityClass()
    {
        return CultureEntity::class;
    }

    public function __getCollectionClass()
    {
        return CultureCollection::class;
    }

    protected function __getDataManagerClass()
    {
        return \Bitrix\Main\Localization\CultureTable::class;
    }

    /**
     * @return \Bitrix\Main\Localization\CultureTable
     */
    public function getDataManager()
    {
        return parent::getDataManager();
    }

    public function __getCachePrefix()
    {
        return 'main/culture';
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param int|null $limit
     * @param int|null $offset
     * @return CultureCollection|CultureEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return CultureEntity
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param $code
     * @return CultureEntity
     */
    public function getEntityByCode($code)
    {
        return parent::getEntityByCode($code);
    }

    /**
     * @param array $arResult
     * @return CultureEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    /**
     * @param $types
     * @return CultureFactory
     * @throws null
     */
    public function cache($types)
    {
        return parent::cache($types);
    }
}