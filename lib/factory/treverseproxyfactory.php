<?php


namespace Wt\Core\Factory;


use Wt\Core\AbstractFactory\AAbstractFactory;
use Wt\Core\Interfaces\IProxyFactory;
use Wt\Core\Interfaces\IReverseProxyFactory;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Support\LexerCollection;
use Wt\Core\Tools;

trait TReverseProxyFactory
{

    protected $factoryInstances = [];
    protected $parentFactory;

    /**
     * @return IFactory
     */
    public function getParentFactory()
    {
        return $this->parentFactory;
    }

    public function setParentFactory(IFactory $factory)
    {
        $this->parentFactory = $factory;
    }

    /**
     * @param string $factoryClass
     * @param array $args
     * @return AFactory
     * @throws \Exception
     */
    public function resolveFactory($factoryClass, $args = [])
    {
        /**
         * @var AFactory $factory
         */
        $factory = resolve($factoryClass, $args);
        return $factory;
    }

    /**
     * @param callable $callback
     * @param array $args
     * @return AFactory
     */
    protected function reverseProxy($callback, $args = [])
    {
        /**
         * @var AFactory $result
         * @var AFactory $f
         */
        $result = null;
        if(is_string($callback)) {
            $factory = $this;
            $methodName = $callback;
        } elseif(is_array($callback)) {
            $factory = $callback[0];
            $methodName = $callback[1];
        } else {
            return $result;
        }

        $factoryChain = $this->getFactoryChain($factory);

        foreach ($factoryChain as $factory) {
            if(!is_callable([$factory, $methodName])){
                continue;
            }
            $result = call_user_func_array([$factory, $methodName], $args);
            if($result) {
                break;
            }
        }
        return $result;
    }

    /**
     * @param IReverseProxyFactory $factory
     * @return IReverseProxyFactory[]
     */
    protected function getFactoryChain($factory)
    {
        $factoryChain = [];
        if(!is_a($factory, IReverseProxyFactory::class)) {
            return $factoryChain;
        }
        // array_unshift($factoryChain, $factory);
        while($factory = $factory->getParentFactory()) {
            array_unshift($factoryChain, $factory);
            if(!is_a($factory, IReverseProxyFactory::class)) {
                break;
            }
        }
        return $factoryChain;
    }

    protected function getRootFactory()
    {
        $factoryChain = $this->getFactoryChain($this);
        $root = null;
        if($factoryChain) {
            $root = reset($factoryChain);
        }
        return $root;
    }

    /**
     * @param string $className
     * @param array $args
     * @return mixed
     * @throws \Exception
     */
    protected function __getFactoryInstance($className, $args = [])
    {
        /**
         * @todo !!! слабое место !!! serialize(0) != serialize('0')
         * решено через str_replace(json(args))
         */
        $hash = '';
        if($args) {
            $hash = '_' . md5(str_replace( '"', '', Tools::json($args)));
        }

        $className = ltrim($className, '\\');
        $key = $className . $hash;

        if(!$this->factoryInstances[$key]){
            $factory = $this->resolveFactory($className, $args);
            if(is_a($factory, IReverseProxyFactory::class) || is_a($factory, IProxyFactory::class)) {
                $factory->setParentFactory($this);
            }
            $this->factoryInstances[$key] = $factory;
        }

        return $this->factoryInstances[$key];
    }

    public function getFactoryInstance($className, $args = [])
    {
        $factory = $this->reverseProxy([$this, 'getFactoryInstance'], [$className, $args]);
        if(!$factory){
            $factory = $this->__getFactoryInstance($className, $args);
        }

        return $factory;
    }

    /**
     * @param null $name
     * @param null $def
     * @return LexerCollection|mixed
     */
    public function getConfig($name = null, $def = null)
    {
        /**
         * @var AAbstractFactory $root
         */
        $root = $this->getRootFactory();

        $config = new LexerCollection();
        if(is_a($root, AAbstractFactory::class)){
            $config = $root->getConfig(static::class, []);
        }
        if(!is_null($name)){
            return $config->get($name, $def);
        }
        return $config;
    }
}