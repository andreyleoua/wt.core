<?php


namespace Wt\Core\Factory;


use Exception;
use Wt\Core\FactoryProxy\ACacheFactoryProxy;
use Wt\Core\Interfaces\ICacheFactory;
use Wt\Core\Interfaces\IFactory;

class CacheFactory
{
    public static function clear($realFactory, $cacheTypes = [])
    {
        if(!$cacheTypes){
            $cacheTypes = app()->config('cache_factory', [])->keys()->toArray();
        }

        /** @var ACacheFactoryProxy $c */
        $c = static::cache($realFactory, $cacheTypes);
        $c->clearCache();
    }

    /**
     * @param ICacheFactory $realFactory
     * @param iterable $cacheTypes
     * @return IFactory
     * @throws Exception
     */
    public static function cache($realFactory, $cacheTypes = [])
    {
        $cc = collect($cacheTypes);
        $parentFactory = $realFactory;
        foreach($cc->reverse() as $cacheFactoryType){
            $cacheFactoryContainerKey = str_replace("\\", '_', get_class($realFactory)) .
                '_'.str_replace('/', '_', $realFactory->__getCachePrefix()) .
                '_'.$cacheFactoryType.'_factory';

            if( !container()->has($cacheFactoryContainerKey) ){
                self::registerCacheFactoryInServiceContainer($cacheFactoryType, $cacheFactoryContainerKey);
            }
            $factoryProxy = resolve($cacheFactoryContainerKey);
            $factoryProxy->setFactory($parentFactory);
            $parentFactory = $factoryProxy;
        }

        return $parentFactory;
    }

    /**
     * @throws Exception
     */
    protected static function registerCacheFactoryInServiceContainer($cacheType, $decorateFactoryName)
    {
        if(!config('cache_factory', [])->has($cacheType)){
            throw new Exception('CacheType "'.$cacheType.'" not found in config file');
        }

        $s = config('cache_factory', [])->get($cacheType);

        $className = null;
        if( is_string($s) ){
            $className = $s;
        }elseif( isset($s['class']) ){
            $className = $s['class'];
        }

        if(!$className){
            throw new Exception('CacheType "'.$cacheType.'" has not class');
        }

        /** @var ACacheFactoryProxy $cl */
        $cl = resolve($className);
        if( is_iterable($s) && isset($s['ttl']) ){
            $cl->setTtl($s['ttl']);
        }

        container()->singleton($decorateFactoryName, $cl);
    }
}