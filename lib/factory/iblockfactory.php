<?php


namespace Wt\Core\Factory;


use Wt\Core\Collection\IBlockCollection;
use Wt\Core\Entity\IBlockEntity;

class IBlockFactory extends AFactory
{

    protected function __getDepsModules()
    {
        return ['iblock'];
    }

    public function __getEntityClass()
    {
        return IBlockEntity::class;
    }

    public function __getCollectionClass()
    {
        return IBlockCollection::class;
    }

    public function __getCachePrefix()
    {
        return 'iblock/block';
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return array
     * @throws null
     */
    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request IBlockFactory " . serialize($filter), 0);
        $this->__stat();

        if(!$select){
            $select = ['ID', 'IBLOCK_TYPE_ID', 'LID', 'CODE', 'NAME', 'ACTIVE', 'SORT', 'XML_ID', 'RIGHTS_MODE', 'VERSION'];
        }
        $rs = \CIBlock::GetList(
            $order,
            $filter,
            false
        );
//        $rs = \Bitrix\Iblock\IblockTable::getList([
//            'filter' => $filter,
//            'select' => $arSelect,
//        ]);

        $list = [];
        while($arr = $rs->Fetch()){
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param $id
     * @return IBlockEntity
     * @throws null
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $code
     * @return IBlockEntity
     * @throws null
     */
    public function getEntityByCode($code)
    {
        $list = $this->getList(['CODE' => $code]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return IBlockEntity
     * @throws null
     */
    public function getEntityByXmlId($xmlId)
    {
        $list = $this->getList(['XML_ID' => $xmlId]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }


    /**
     * @param array $arIds
     * @return IBlockCollection|IBlockEntity[]
     * @throws null
     */
    public function getEntityByIds(array $arIds)
    {
        return $this->getCollection()->getByIds($arIds);
    }

    /**
     * @param array $arResult
     * @return IBlockEntity
     * @throws \Exception
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}