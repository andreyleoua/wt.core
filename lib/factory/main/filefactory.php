<?php

namespace Wt\Core\Factory\Main;

use Wt\Core\Collection\Main\FileCollection;
use Wt\Core\Entity\Main\FileEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Templater\TemplaterService;
use Wt\Core\Templater\Templater;

class FileFactory extends AFactory
{
    protected mixed $templater;

    /**
     * @return Templater
     * @throws null
     */
    public function getTemplater()
    {
        if(!$this->templater){
            $this->templater = resolve(TemplaterService::class);
        }
        return $this->templater;
    }

    public function __getCachePrefix()
    {
        return 'main/file';
    }

    public function __getEntityClass()
    {
        return FileEntity::class;
    }

    public function __getCollectionClass()
    {
        return FileCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p('real request FileFactory ' .serialize($filter));
        $list = [];
        if(!$order){
            $order = ['ID' => 'ASC'];
        }
        $rs = \CFile::GetList($order, $filter);
        if($rs) {
            while($arr = $rs->Fetch()){
                $list[$arr['ID']] = $arr;
            }
        }
        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return FileCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return FileEntity
     */
	public function getEntityById($id)
    {

	    $result = [];
	    if($id) {
            $list = $this->getList(['ID' => $id]);
            if (isset($list[$id])) {
                $result = $list[$id];
            }
        }

		return $this->getEntityByResult($result);
	}

    /**
     * @param $xmlId
     * @return FileEntity
     */
	public function getEntityByXmlId($xmlId)
    {

	    $result = [];
	    if($xmlId) {
            $list = $this->getList(['EXTERNAL_ID' => $xmlId]);

            if ($list) {
                $result = $list[array_key_first($list)];
            }
        }

		return $this->getEntityByResult($result);
	}

    /**
     * @param array $arIds
     * @return FileCollection
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['@ID' => $arIds]);
        }
        return $this->getEntityByResults($res);
    }

    /**
     * @param array $arResult
     * @return FileEntity
     */
	public function getEntityByResult(array $arResult)
	{
	    if($arResult['ID']){
	        if(!isset($arResult['XML_ID'])) {
                $arResult['XML_ID'] = $arResult['EXTERNAL_ID'];
            }
	        if(!isset($arResult['SRC'])) {
                $arResult['SRC'] = \CFile::GetFileSRC($arResult);
            }
        }

        return parent::getEntityByResult($arResult);
	}

    /**
     * @param string $src
     * @return FileEntity|null
     */
    public function getEntityBySrc($src)
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . $src;
        if(!file_exists($path)) {
            return null;
        }
        $mtime = filemtime($path);

        $externalId = md5($src . $mtime);

        $fileFactory = app()->factory()->main()->getFileFactory();
        $collection = $fileFactory->getCollection(['MODULE_ID'=>'alt_image', 'EXTERNAL_ID' => $externalId], [], [], 1);

        if($collection->count()) {
            return $collection->first();
        }

        $altFile = \CFile::MakeFileArray($path);
        $altFile['MODULE_ID'] = 'alt_image';
        $altFile['external_id'] = $externalId;
        $id = \CFile::SaveFile($altFile, 'alt_image');
        if(!$id){
            return null;
        }
        return $this->getEntityById($id);
    }
}