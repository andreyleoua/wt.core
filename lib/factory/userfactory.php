<?php


namespace Wt\Core\Factory;


use Wt\Core\Collection\UserCollection;
use Wt\Core\Entity\UserEntity;

class UserFactory extends AFactory
{
    public function __construct()
    {
        if(!$GLOBALS['USER']){
            $GLOBALS['USER'] = new \CUser();
        }
        parent::__construct();
    }

    public function __getCachePrefix()
    {
        return 'main/user';
    }

    public function __getEntityClass()
    {
        return UserEntity::class;
    }

    public function __getCollectionClass()
    {
        return UserCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request UserFactory " . serialize($filter));

//        $arSelect = ['*','PHONE_NUMBER' => 'PHONE_AUTH.PHONE_NUMBER', 'UF_*'];

        $dbTable = \Bitrix\Main\UserTable::getList([
            'filter' => $filter,
            'order' => $order,
            // 'select' => $arSelect,
            'limit' => $limit,
            'offset' => $offset,
        ]);
//getUserGroupIds
        $result = [];
        while($res = $dbTable->fetch()){
            $res['GROUPS'] = \Bitrix\Main\UserTable::getUserGroupIds($res['ID']);
            if(class_exists(\Bitrix\Main\UserPhoneAuthTable::class)){
                $phoneRow = \Bitrix\Main\UserPhoneAuthTable::getRow([
                    'select' => ['PHONE_NUMBER'],
                    'filter' => ['=USER_ID' => $res['ID']],
                ]);
                $res['PHONE_NUMBER'] = '';
                if ($phoneRow) {
                    $res['PHONE_NUMBER'] = $phoneRow['PHONE_NUMBER'];
                }
            }
            $result[$res['ID']] = $res;
        }

        return $result;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return UserCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    public function getNoAuthUserEntity()
    {
        $userArray = [
            'ID' => 0,
            'GROUPS' => [2],
        ];
        return $this->getEntityByResult($userArray);
    }

    /**
     * @param $id
     * @return UserEntity
     */
    public function getEntityById($id)
    {
        $id = (int)$id;
        if(!$id){
            return null;
        }
        $list = $this->getList(['ID' => $id]);
        if( $list ){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return UserEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}