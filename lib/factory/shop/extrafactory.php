<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\ExtraCollection;
use Wt\Core\Entity\Shop\ExtraEntity;
use Wt\Core\Factory\AFactory;

class ExtraFactory extends AFactory
{
    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __getEntityClass()
    {
        return ExtraEntity::class;
    }

    public function __getCollectionClass()
    {
        return ExtraCollection::class;
    }

    public function __getCachePrefix()
    {
        return 'shop/extra';
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {

        //p("real request ExtraFactory " . serialize($filter));

        if(!\Bitrix\Main\Loader::includeModule('catalog')) {
            return [];
        }

        $rs = \CExtra::getList(
            $order,
            $filter,
            false,
            $this->__getArNavStartParams($limit, $offset),
            $select
        );

        $list = [];
        while($arr = $rs->Fetch()) {
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return ExtraCollection|ExtraEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return ExtraEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return ExtraEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}