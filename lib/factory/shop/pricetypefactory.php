<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\PriceTypeCollection;
use Wt\Core\Entity\Shop\PriceTypeEntity;
use Wt\Core\Factory\AORMFactory;

/**
 * Типы цен
 */
class PriceTypeFactory extends AORMFactory
{
    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __getCachePrefix()
    {
        return 'shop/price_type';
    }

    public function __getEntityClass()
    {
        return PriceTypeEntity::class;
    }

    public function __getCollectionClass()
    {
        return PriceTypeCollection::class;
    }

    /**
     * @return string
     */
    protected function __getDataManagerClass()
    {
        return \Bitrix\Catalog\GroupTable::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        if(!$select){
            $select[] = '*';
            $select['LANG_'] = 'CURRENT_LANG';
        }

        return parent::getList($filter, $order, $select, $limit, $offset); // TODO: Change the autogenerated stub
    }

    /**
     * @param $filter
     * @param $order
     * @param $select
     * @param $limit
     * @param $offset
     * @return PriceTypeCollection|PriceTypeEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset); // TODO: Change the autogenerated stub
    }

    /**
     * @param $id
     * @return PriceTypeEntity
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id); // TODO: Change the autogenerated stub
    }

    /**
     * @param $xmlId
     * @return PriceTypeEntity
     */
    public function getEntityByXmlId($xmlId)
    {
        return parent::getEntityByXmlId($xmlId); // TODO: Change the autogenerated stub
    }

    /**
     * @param $code
     * @return PriceTypeEntity
     */
    public function getEntityByCode($code)
    {
        $list = $this->getList(['=NAME' => $code]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return PriceTypeCollection
     */
    public function getEntityByIds(array $arIds)
    {
        return parent::getEntityByIds($arIds); // TODO: Change the autogenerated stub
    }

    /**
     * @param $query \Bitrix\Main\ORM\Query\Query|\Bitrix\Main\Entity\Query|null
     * @return PriceTypeCollection
     */
    public function getEntityByQuery($query = null)
    {
        return parent::getEntityByQuery($query); // TODO: Change the autogenerated stub
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Result|\CDBResult|\mysqli_result $queryResult
     * @return PriceTypeCollection
     */
    public function getEntityByQueryResult($queryResult)
    {
        return parent::getEntityByQueryResult($queryResult); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $arResult
     * @return PriceTypeEntity
     * @throws \Exception
     */
    public function getEntityByResult(array $arResult)
    {
        $arResult['CODE'] = $arResult['NAME'];
        return parent::getEntityByResult($arResult);
    }

    /**
     * @param $types
     * @return PriceTypeFactory
     * @throws null
     */
    public function cache($types)
    {
        return parent::cache($types); // TODO: Change the autogenerated stub
    }
}