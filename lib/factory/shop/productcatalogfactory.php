<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Entity\Shop\ProductCatalogEntity;
use Wt\Core\Factory\AFactory;

/**
 * хз пока что тут зделать
 *
 * Class ProductCatalogFactory
 * @package Wt\Core\Factory
 */
class ProductCatalogFactory extends AFactory
{
    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __getCachePrefix()
    {
        return 'shop/product_catalog';
    }

    public function __getEntityClass()
    {
        return ProductCatalogEntity::class;
    }

    private function getCatalogProductSelectArray($arSelect = [])
    {
        if (class_exists('\Bitrix\Catalog\ProductTable') ) {
            $arSelectFull = array_keys(\Bitrix\Catalog\ProductTable::getMap());
        } else {
            $arSelectFull = ['ID','QUANTITY','QUANTITY_TRACE','QUANTITY_TRACE_ORIG','WEIGHT','TIMESTAMP_X',
                'PRICE_TYPE','RECUR_SCHEME_LENGTH','RECUR_SCHEME_TYPE','TRIAL_PRICE_ID','WITHOUT_ORDER',
                'SELECT_BEST_PRICE','VAT_ID','VAT_INCLUDED','CAN_BUY_ZERO','CAN_BUY_ZERO_ORIG','NEGATIVE_AMOUNT_TRACE',
                'NEGATIVE_AMOUNT_TRACE_ORIG','TMP_ID','PURCHASING_PRICE','PURCHASING_CURRENCY','BARCODE_MULTI',
                'QUANTITY_RESERVED','SUBSCRIBE','SUBSCRIBE_ORIG','WIDTH','LENGTH','HEIGHT','MEASURE','TYPE',
                'IBLOCK_ELEMENT'];
        }
        $arSelectDef = [ 'ID', 'QUANTITY', 'PRICE_TYPE', 'VAT_ID', 'VAT_INCLUDED',
            'PURCHASING_PRICE', 'PURCHASING_CURRENCY', 'QUANTITY_TRACE', 'CAN_BUY_ZERO',
            'NEGATIVE_AMOUNT_TRACE', 'MEASURE', 'TYPE', ];

        if (is_array($arSelect)) {
            if(empty($arSelect)) {
                return $arSelectDef;
            }
            $arSelect = array_unique(array_merge(['ID', 'QUANTITY', 'QUANTITY_TRACE', 'CAN_BUY_ZERO'],$arSelect));
            $arSelect = array_intersect($arSelectFull, $arSelect);
        } else {
            $arSelect = $arSelectDef;
        }
        return $arSelect;
    }

    private function getProductCatalog($PRODUCT_ID, $arSelect = [])
    {
        $data = [];

        $available = in_array('AVAILABLE', (array)$arSelect, true);

        $arSelect = $this->getCatalogProductSelectArray($arSelect);

        //p('real request CatalogFactory ' . $PRODUCT_ID);
        if (class_exists('\Bitrix\Catalog\ProductTable')) {
            $isD7 = true;
            $dbTable = \Bitrix\Catalog\ProductTable::getList([
                'filter' => ['=ID' => $PRODUCT_ID],
                'select' => $arSelect,
            ]);
        } else {
            $isD7 = false;
            $dbTable =  \CCatalogProduct::GetList(
                [],
                ['ID' => $PRODUCT_ID],
                false,
                false,
                $arSelect
            );
        }
        if($arParamsRaw = $dbTable->Fetch()) {
            $data = $arParamsRaw;
        }
        unset($dbTable);
        if(!isset($data['AVAILABLE']) && $available && $isD7) {
            $data['AVAILABLE'] = \Bitrix\Catalog\ProductTable::calculateAvailable($data);
        }

        return $data;
    }

    /**
     * @param $id
     * @return ProductCatalogEntity
     */
    public function getEntityById($id)
    {
        return $this->getEntityByResult($this->getProductCatalog($id));
    }

    /**
     * @param array $arResult
     * @return ProductCatalogEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @deprecated z
     */
    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        // TODO: Implement getList() method.
    }
}