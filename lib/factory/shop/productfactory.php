<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\ProductCollection;
use Wt\Core\Entity\Shop\ProductEntity;
use Wt\Core\Factory\Iblock\ElementFactory;

class ProductFactory extends ElementFactory
{

    protected function __getDepsModules()
    {
        return array_merge(parent::__getDepsModules(), ['currency', 'catalog']);
    }

    public function __getEntityClass()
    {
        return ProductEntity::class;
    }

    public function __getCollectionClass()
    {
        return ProductCollection::class;
    }

    /**
     * @param $id
     * @return ProductEntity
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param array $arResult
     * @return ProductEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    /**
     * @param array $arIds
     * @return ProductCollection|ProductEntity[]
     * @throws null
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['ID' => $arIds]);
        }
        return $this->getEntityByResults($res);
    }
}