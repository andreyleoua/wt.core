<?php

namespace Wt\Core\Factory\Shop;

use Wt\Core\Collection\Shop\ProductStoreCollection;
use Wt\Core\Entity\Shop\ProductStoreEntity;
use Wt\Core\Factory\AORMFactory;

class ProductStoreFactory extends AORMFactory
{
    protected $blockId;
    protected $elementId;

    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __construct($blockId, $elementId)
    {
        $this->blockId = $blockId;
        $this->elementId = $elementId;
        parent::__construct();
    }

    protected function __getDataManagerClass()
    {
        return \Bitrix\Catalog\StoreProductTable::class;
    }

    public function __getCachePrefix()
    {
        return 'iblock/'.$this->getBlockId().'/element/'.$this->getElementId().'/store';
    }

    public function __getEntityClass()
    {
        return ProductStoreEntity::class;
    }

    public function __getCollectionClass()
    {
        return ProductStoreCollection::class;
    }

    /**
     * @param array $arResult
     * @return ProductStoreEntity
     * @throws null
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {

        //p("real request ProductStoreFactory $id");

        unset($filter['PRODUCT_ID']);
        unset($filter['=PRODUCT_ID']);
        if(!$select){
            $select  = ['ID', 'AMOUNT', 'STORE_ID', 'PRODUCT_ID', 'UF_*' ];
        }

        if (class_exists('\Bitrix\Catalog\StoreProductTable') ) {

            $filter['=PRODUCT_ID'] = $this->getElementId();
            $dbStoreProductTable = \Bitrix\Catalog\StoreProductTable::getList([
                'filter' => $filter,
                'order' => $order,
                'select' => $select,
                'limit' => $limit,
                'offset' => $offset,
            ]);
        } else {
            $filter['PRODUCT_ID'] = $this->getElementId();
            $dbStoreProductTable =  \CCatalogStoreProduct::GetList(
                $order,
                $filter,
                false,
                $this->__getArNavStartParams($limit, $offset),
                $select
            );
        }
        $arProductStore = [];
        while($arStore = $dbStoreProductTable->Fetch()) {
            $arProductStore[$arStore['ID']] = $arStore;
        }

        unset($dbStoreProductTable);
        return $arProductStore;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return ProductStoreCollection|ProductStoreEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return ProductStoreEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['=ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * @return mixed
     */
    public function getBlockId()
    {
        return $this->blockId;
    }
}