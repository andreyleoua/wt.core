<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\MeasureCollection;
use Wt\Core\Entity\Shop\MeasureEntity;
use Wt\Core\Factory\AFactory;

class MeasureFactory extends AFactory
{
    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __getEntityClass()
    {
        return MeasureEntity::class;
    }

    public function __getCollectionClass()
    {
        return MeasureCollection::class;
    }

    public function __getCachePrefix()
    {
        return 'shop/measure';
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {

        //p("real request MeasureFactory " . serialize($filter));

        $rs = \CCatalogMeasure::getList(
            $order,
            $filter,
            false,
            $this->__getArNavStartParams($limit, $offset),
            $select
        );

        $list = [];
        while($arr = $rs->Fetch()) {
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return MeasureCollection|MeasureEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return MeasureEntity
     */
    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $code
     * @return MeasureEntity
     */
    public function getEntityByCode($code)
    {
        $list = $this->getList(['CODE' => $code]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return MeasureEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}