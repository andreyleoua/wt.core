<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\ProductPriceCollection;
use Wt\Core\Entity\Shop\ProductPriceEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Support\Result;

class ProductPriceFactory extends AFactory
{
    protected $blockId;
    protected $elementId;

    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    public function __construct($blockId, $elementId)
    {
        $this->blockId = $blockId;
        $this->elementId = $elementId;
        parent::__construct();
    }

    public function add($data)
    {
        $result = Result::make();
        $result->setData($data);

        if (class_exists('Bitrix\Catalog\PriceTable') ) {
            $bxResult = \Bitrix\Catalog\PriceTable::add($data);
            $result = Result::makeByBxResult($bxResult);
        }
        else {
            //\Bitrix\Catalog\Model\Price::getList
            $id = \CPrice::Add($data);
            if(!$id){
                global $APPLICATION;
                $result->setError($APPLICATION->LAST_ERROR->GetString());
            } else {
                $result->setId($id);
            }
        }
        return $result;
    }

    public function __getCachePrefix()
    {
        return 'iblock/'.$this->getBlockId().'/element/'.$this->getElementId().'/price';
    }

    public function __getEntityClass()
    {
        return ProductPriceEntity::class;
    }

    public function __getCollectionClass()
    {
        return ProductPriceCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request ProductPriceFactory " . serialize($filter));

        $filter['PRODUCT_ID'] = $this->getElementId();

        if(!$select){
            $select = ['ID', 'PRODUCT_ID', 'CATALOG_GROUP_ID', 'QUANTITY_FROM', 'QUANTITY_TO', 'EXTRA_ID', 'PRICE', 'CURRENCY', 'PRICE_SCALE', 'TIMESTAMP_X' ];
        }

        if (class_exists('Bitrix\Catalog\PriceTable') ) {
            $dbPriceTable = \Bitrix\Catalog\PriceTable::getList([
                'filter' => $filter,
                'order' => $order,
                'select' => $select,
                'limit' => $limit,
                'offset' => $offset,
            ]);
        } else {
            //\Bitrix\Catalog\Model\Price::getList
            $dbPriceTable = \CPrice::getList(
                $order,
                $filter,
                false,
                $this->__getArNavStartParams($limit, $offset),
                $select
            );
        }
        $list = [];
        while($arr = $dbPriceTable->Fetch()) {
            $list[$arr['ID']] = $arr;
        }
        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return ProductPriceCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return ProductPriceEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['=ID' => $id,]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return ProductPriceEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    /**
     * @return mixed
     */
    public function getElementId()
    {
        return $this->elementId;
    }

    /**
     * @return mixed
     */
    public function getBlockId()
    {
        return $this->blockId;
    }
}