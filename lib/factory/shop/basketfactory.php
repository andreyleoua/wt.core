<?php

namespace Wt\Core\Factory\Shop;

use Wt\Core\Collection\Shop\BasketCollection;
use Wt\Core\Entity\Shop\BasketEntity;
use Wt\Core\Factory\AORMFactory;

class BasketFactory extends AORMFactory
{
    protected function __getDepsModules()
    {
        return ['sale'];
    }

    public function __getCachePrefix()
    {
        return 'shop/basket';
    }

    public function __getEntityClass()
    {
        return BasketEntity::class;
    }

    public function __getCollectionClass()
    {
        return BasketCollection::class;
    }

    protected function __getDataManagerClass()
    {
        return \Bitrix\Sale\Internals\BasketTable::class;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return BasketCollection|BasketEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return BasketEntity|null
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param array $arIds
     * @return BasketCollection|BasketEntity[]
     */
    public function getEntityByIds(array $arIds)
    {
        return parent::getEntityByIds($arIds);
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Query|\Bitrix\Main\Entity\Query|null $query
     * @return BasketCollection|BasketEntity[]
     */
    public function getEntityByQuery($query = null)
    {
        return parent::getEntityByQuery($query);
    }

    /**
     * @param \Bitrix\Main\ORM\Query\Result|\CDBResult|\mysqli_result $queryResult
     * @return BasketCollection|BasketEntity[]
     */
    public function getEntityByQueryResult($queryResult)
    {
        return parent::getEntityByQueryResult($queryResult);
    }

    /**
     * @param array $arResult
     * @return BasketEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}