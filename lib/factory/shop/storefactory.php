<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\StoreCollection;
use Wt\Core\Entity\Shop\StoreEntity;
use Wt\Core\Factory\AORMFactory;
use Wt\Core\Support\Result;

class StoreFactory extends AORMFactory
{

    protected function __getDepsModules()
    {
        return ['catalog'];
    }

    protected function __getDataManagerClass()
    {
        return \Bitrix\Catalog\StoreTable::class;
    }

    public function __getEntityClass()
    {
        return StoreEntity::class;
    }

    public function __getCollectionClass()
    {
        return StoreCollection::class;
    }

    public function __getCachePrefix()
    {
        return 'shop/store';
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        // p("real request StoreFactory " . serialize($filter));

        if(!$select){
            $select = ['*', 'UF_*'];
        }

        $rs = \Bitrix\Catalog\StoreTable::getList([
            'filter' => $filter,
            'order' => $order,
            'select' => $select,
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $list = [];
        while($arr = $rs->Fetch()){
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return StoreCollection|StoreEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return StoreEntity|null
     */
    public function getEntityById($id)
    {
        return parent::getEntityById($id);
    }

    /**
     * @param $code
     * @return StoreEntity|null
     */
    public function getEntityByCode($code)
    {
        return parent::getEntityByCode($code);
    }

    /**
     * @param array $arResult
     * @return StoreEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }

    public function add($data)
    {
        unset($data['DATE_MODIFY']);
        unset($data['DATE_CREATE']);
        unset($data['MODIFIED_BY']);
        unset($data['USER_ID']);

        return parent::add($data);
    }

    public function update($id, $data)
    {
        unset($data['DATE_MODIFY']);
        unset($data['DATE_CREATE']);
        unset($data['MODIFIED_BY']);
        unset($data['USER_ID']);

        return parent::update($id, $data);
    }

    public function import($syncField, $list)
    {
        $result = Result::make();

        $result->addData(['syncField' => $syncField]);
        $collection = $this->getCollection();
        foreach ($list as $item){
            $entity = $collection->getByField($syncField, $item[$syncField]);

            if($entity){
                $rs = $entity->update($item);
            } else {
                $rs = $this->add($item);
            }
            if(!$rs->isSuccess()){
                $result->getErrors()->setErrors($rs->getErrors());
            }
        }

        return $result;
    }
}