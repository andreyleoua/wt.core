<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\CompanyCollection;
use Wt\Core\Entity\Shop\CompanyEntity;
use Wt\Core\Factory\AFactory;

class CompanyFactory extends AFactory
{

    public function __getCachePrefix()
    {
        return 'shop/company';
    }

    public function __getEntityClass()
    {
        return CompanyEntity::class;
    }

    public function __getCollectionClass()
    {
        return CompanyCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p('real request CompanyFactory ' .serialize($filter));
        $list  = [];
        if(!\Bitrix\Main\Loader::includeModule('sale')){
            return $list;
        }
        global $USER_FIELD_MANAGER;
        $fields = $USER_FIELD_MANAGER->GetUserFields(\Bitrix\Sale\Internals\CompanyTable::getUfId());
        $select = ['ID', 'NAME', 'LOCATION_ID', 'CODE', 'XML_ID', 'ACTIVE', 'ADDRESS', 'SORT'];
        foreach ($fields as $field)
            $select[] = $field['FIELD_NAME'];

        if(!$order){
            $order = ['SORT' => 'ASC'];
        }
        $rs = \Bitrix\Sale\Internals\CompanyTable::getList([
            'select' => $select,
            'filter' => $filter,
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset,
        ]);
        while($arr = $rs->fetch()) {
            $list[$arr['ID']] = $arr;
        }
        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return CompanyCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return CompanyEntity
     */
	public function getEntityById($id)
    {
        if(!$id){
            return null;
        }

        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }

        return null;
	}

    /**
     * @param $code
     * @return CompanyEntity
     */
    public function getEntityByCode($code)
    {
        if(!$code){
            return null;
        }

        $list = $this->getList(['CODE' => $code]);
        if($list) {
            return $this->getEntityByResult(reset($list));
        }

        return null;
    }

    /**
     * @param $xmlId
     * @return CompanyEntity
     */
	public function getEntityByXmlId($xmlId)
    {
        if(!$xmlId){
            return null;
        }

	    $list = $this->getList(['XML_ID' => $xmlId]);
	    if($list) {
            return $this->getEntityByResult(reset($list));
        }

		return null;
	}

    /**
     * @param array $arIds
     * @return CompanyCollection
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['@ID' => $arIds]);
        }
        return $this->getEntityByResults($res);
    }

    /**
     * @param array $arResult
     * @return CompanyEntity
     */
	public function getEntityByResult(array $arResult)
	{
        return parent::getEntityByResult($arResult);
	}
}