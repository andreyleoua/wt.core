<?php


namespace Wt\Core\Factory\Shop;


use Wt\Core\Collection\Shop\VatCollection;
use Wt\Core\Entity\Shop\VatEntity;
use Wt\Core\Factory\AFactory;

class VatFactory extends AFactory
{

    public function __getEntityClass()
    {
        return VatEntity::class;
    }

    public function __getCachePrefix()
    {
        return 'shop/vat';
    }

    public function __getCollectionClass()
    {
        return VatCollection::class;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request VatFactory " . serialize($filter));

        if(!\Bitrix\Main\Loader::includeModule('catalog')) {
            return [];
        }

        if(!$select){
            $select = ['*', 'UF_*'];
        }

        $rs = \Bitrix\Catalog\VatTable::getList([
            'filter' => $filter,
            'order' => $order,
            'select' => $select,
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $list = [];
        while($arr = $rs->Fetch()){
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return VatCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }
    /**
     * @param $id
     * @return VatEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(array('=ID'=>$id));
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return VatEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}