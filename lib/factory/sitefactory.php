<?php


namespace Wt\Core\Factory;


use Wt\Core\Collection\SiteCollection;
use Wt\Core\Entity\SiteEntity;

class SiteFactory extends AFactory
{

    public function __getEntityClass()
    {
        return SiteEntity::class;
    }

    public function __getCollectionClass()
    {
        return SiteCollection::class;
    }

    public function __getCachePrefix()
    {
        return 'main/site';
    }

    /**
     * @param array $filter
     * @param array $order
     * @return array
     */
    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request SiteFactory " . serialize($filter));

        $by = $order = null;

        if(is_array($order)){
            foreach ($order as $_by => $_order){
                $by = $_by;
                $order = $_order;
                break;
            }
        }
        if(is_null($by)){
            $by = 'sort';
        }
        if(is_null($order)){
            $order = 'asc';
        }

        $rs = \CSite::GetList(
            $by,
            $order,
            $filter
        );

        $list = [];
        while($arr = $rs->Fetch()){
            $list[$arr['ID']] = $arr;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @return SiteCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return SiteEntity
     */
    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return SiteCollection
     */
    public function getEntityByIds(array $arIds)
    {
        return $this->getCollection()->getByIds($arIds);
    }

    /**
     * @param array $arResult
     * @return SiteEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}