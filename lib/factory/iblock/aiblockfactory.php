<?php


namespace Wt\Core\Factory\IBlock;


use Wt\Core\Factory\AFactory;

abstract class AIBlockFactory extends AFactory
{
    protected function __getDepsModules()
    {
        return ['iblock'];
    }
}