<?php


namespace Wt\Core\Factory\IBlock;


use Wt\Core\Collection\Iblock\SectionCollection;
use Wt\Core\Entity\Iblock\SectionEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Result;

class SectionFactory extends AIBlockFactory
{
    /**
     * @var int $blockId
     */
    protected $blockId;

    public function __construct($iblockId)
    {
        $this->blockId = (int)$iblockId;
        parent::__construct();
    }

    public function getBlockId()
    {
        return $this->blockId;
    }

    public function __getCachePrefix()
    {
        return 'iblock/'.$this->getBlockId().'/section';
    }

    public function __getUserFieldEntityId()
    {
        return 'IBLOCK_'.$this->getBlockId().'_SECTION';
    }

    public function __getEntityClass()
    {
        return SectionEntity::class;
    }

    public function __getCollectionClass()
    {
        return SectionCollection::class;
    }

    /**
     * @see https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblocksection/update.php
     *
     * Изменить значения полей
     * GLOBAL_ACTIVE, DEPTH_LEVEL, LEFT_MARGIN, RIGHT_MARGIN, IBLOCK_ID, DATE_CREATE и CREATED_BY нельзя.
     * Значение первого определяется флагом активности раздела и его родителей. DEPTH_LEVEL, LEFT_MARGIN и RIGHT_MARGIN
     * расчитываются автоматически в зависимости от положения раздела в дереве.
     *
     * IBLOCK_SECTION_ID = 0 - Родительский верхний уровень
     *
     * @param $id
     * @param $data
     * @return Result
     */
    public function update($id, $data)
    {
        $result = Result::make();
        $result->setId($id);
        $result->setData(['fields' => $data]);
        $section = new \CIBlockSection();
        $r = $section->Update($id, $data);
        if(!$r){
            $result->addError($section->LAST_ERROR);
        }
        return $result;
    }

    /**
     * @param $id
     * @return SectionCollection|SectionEntity[]
     */
    public function getChildrenCollection($id)
    {
        $af = app()->factory()->iBlock()->getEntityById($this->blockId);
        return $af->getSectionFactory()->getCollection(['SECTION_ID' => $id]);
    }

    /**
     *
     * CHECK_PERMISSIONS = N
     *
     * @param $filter
     * @param $order
     * @param $select
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p('Real request SectionFactory ' . serialize($filter));

        if( $this->getBlockId() ){
            $filter['IBLOCK_ID'] = $this->getBlockId();
        }

        if(!$order){
            $order = ['SORT'=> 'ASC'];
        }

        if(!$select || $select == ['*'])
        {
            $select[] = 'UF_*';
        }

        $res = \CIBlockSection::GetList(
            $order,
            $filter,
            false,
            $select,
            $this->__getArNavStartParams($limit, $offset)
        );
        $result = [];
        while($arItem = $res->Fetch()){
            $result[$arItem['ID']] = $arItem;
        }

        return $result;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return SectionCollection|SectionEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return SectionEntity|null
     */
    public function getEntityById($id)
    {
        $id = (int)$id;
        if(!$id){
            return null;
        }

        $list = $this->getList(['ID' => $id]);
        if( $list ){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $code
     * @return SectionEntity|null
     */
    public function getEntityByCode($code)
    {
        if(!$code){
            return null;
        }
        $list = $this->getList(['=CODE' => $code]);
        if( $list ){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return SectionEntity|null
     */
    public function getEntityByXmlId($xmlId)
    {
        if(!$xmlId){
            return null;
        }
        $list = $this->getList(['=XML_ID' => $xmlId]);
        if( $list ){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return SectionCollection|SectionEntity[]
     */
    public function getEntityByIds(array $arIds)
    {
        $res = [];
        if( $arIds ){
            $res = $this->getList(['ID' => $arIds]);
        }

        return $this->getEntityByResults($res);
    }

    /**
     * @param array $arResults
     * @return SectionCollection|SectionEntity[]
     */
    public function getEntityByResults(array $arResults)
    {
        return parent::getEntityByResults($arResults); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $arResult
     * @return SectionEntity
     */
    public function getEntityByResult(array $arResult)
    {
        unset($arResult['SEARCHABLE_CONTENT']);
        return parent::getEntityByResult($arResult);
    }

    /**
     * @param $types
     * @return SectionFactory
     * @throws \Exception
     */
    public function cache($types)
    {
        return parent::cache($types); // TODO: Change the autogenerated stub
    }
}