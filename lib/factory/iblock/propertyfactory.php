<?php


namespace Wt\Core\Factory\IBlock;


use Wt\Core\Entity\Iblock\PropertyEntity;
use Wt\Core\Collection\Iblock\PropertyCollection;
use Wt\Core\Support\Result;
use Bitrix\Main;

class PropertyFactory extends AIBlockFactory
{
    /**
     * @var int
     */
    protected $blockId;

    public function __construct($iblockId)
    {
        $this->blockId = (int)$iblockId;
        parent::__construct();
    }

    public function __getCachePrefix()
    {
        return 'iblock/'.$this->getBlockId().'/property';
    }

    public function __getEntityClass()
    {
        return PropertyEntity::class;
    }

    public function __getCollectionClass()
    {
        return PropertyCollection::class;
    }

    public function getBlockId()
    {
        return $this->blockId;
    }

    /**
     * @param $types
     * @return PropertyFactory
     * @throws \Exception
     */
    public function cache($types)
    {
        return parent::cache($types); // TODO: Change the autogenerated stub
    }

    public function import($syncField, $list)
    {
        $result = Result::make();
        $acceptableFields = collect(['ID', 'NAME', 'CODE', 'XML_ID', 'TMP_ID']);
        if(!$acceptableFields->contains($syncField)){
            $result->addError('Invalid syncField');
            return $result;
        }
        $blockId = $this->getBlockId();
        if(!$blockId){
            $result->addError('IBlockId not defined');
            return $result;
        }
        $result->addData(['syncField' => $syncField, 'iBlockId' => $blockId, 'list' => []]);
        $collection = $this->getCollection();
        foreach ($list as $item){
            $entity = $collection->getByField($syncField, $item[$syncField]);

            $list = &$result->getData()->get('list');
            if($entity){
                $rs = $entity->update($item);
                $list[$rs->getId()] = [
                    'id' => $rs->getId(),
                    'do' => 'update',
                ];
            } else {
                $rs = $this->add($item);
                $list[$rs->getId()] = [
                    'id' => $rs->getId(),
                    'do' => 'add',
                ];
            }

            if(!$rs->isSuccess()){
                $result->getErrors()->setErrors($rs->getErrors());
            }
        }

        return $result;
    }

    public function add($data)
    {
        unset($data['ID']);
        unset($data['TIMESTAMP_X']);
        unset($data['VERSION']);
        unset($data['IBLOCK_ID']);

        $data['IBLOCK_ID'] = $this->getBlockId();

        if(version_compare(\Bitrix\Main\ModuleManager::getVersion('iblock'), '18.0.2') === -1){
            if(is_array($data['USER_TYPE_SETTINGS_LIST'])){
                $data['USER_TYPE_SETTINGS'] = $data['USER_TYPE_SETTINGS_LIST'];
            }
            unset($data['USER_TYPE_SETTINGS_LIST']);
            if(is_array($data['USER_TYPE_SETTINGS'])){
                $data['USER_TYPE_SETTINGS'] = serialize($data['USER_TYPE_SETTINGS']);
            }
        } elseif(!is_array($data['USER_TYPE_SETTINGS_LIST'])){
            if(!is_array($data['USER_TYPE_SETTINGS'])){
                $data['USER_TYPE_SETTINGS_LIST'] = array_filter((array)unserialize($data['USER_TYPE_SETTINGS']));
            } else {
                $data['USER_TYPE_SETTINGS_LIST'] = array_filter($data['USER_TYPE_SETTINGS']);
            }
        }

        $bxResult = \Bitrix\Iblock\PropertyTable::add($data);
        $result = Result::makeByBxResult($bxResult);
        $result->setData(['fields' => $data]);

        return $result;
    }

    public function update($id, $data)
    {
        unset($data['ID']);
        unset($data['TIMESTAMP_X']);
        unset($data['IBLOCK_ID']);
        unset($data['VERSION']);
        if(version_compare(\Bitrix\Main\ModuleManager::getVersion('iblock'), '18.0.2') === -1){
            if(is_array($data['USER_TYPE_SETTINGS_LIST'])){
                $data['USER_TYPE_SETTINGS'] = $data['USER_TYPE_SETTINGS_LIST'];
            }
            unset($data['USER_TYPE_SETTINGS_LIST']);
            if(is_array($data['USER_TYPE_SETTINGS'])){
                $data['USER_TYPE_SETTINGS'] = serialize($data['USER_TYPE_SETTINGS']);
            }
        } elseif(!is_array($data['USER_TYPE_SETTINGS_LIST'])){
            if(!is_array($data['USER_TYPE_SETTINGS'])){
                $data['USER_TYPE_SETTINGS_LIST'] = array_filter((array)unserialize($data['USER_TYPE_SETTINGS']));
            } else {
                $data['USER_TYPE_SETTINGS_LIST'] = array_filter($data['USER_TYPE_SETTINGS']);
            }
        }

        $bxResult = \Bitrix\Iblock\PropertyTable::update($id, $data);
        return Result::makeByBxResult($bxResult);
    }

    /**
     * @param $id
     * @return Result
     * @throws \Exception
     */
    public function delete($id)
    {
        $bxResult = \Bitrix\Iblock\PropertyTable::delete($id);
        return Result::makeByBxResult($bxResult);
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request IBlockPropertyFactory " . serialize($filter));

        //\CIBlockProperty::GetList

        if($this->getBlockId()) {
            unset($filter['IBLOCK_ID'], $filter['=IBLOCK_ID'], $filter['@IBLOCK_ID']);
            $filter['=IBLOCK_ID'] = $this->getBlockId();
        }

        if(!$select){
            $select = [ // 'TMP_ID','USER_TYPE_SETTINGS_LIST',
                'ID','TIMESTAMP_X','IBLOCK_ID','NAME','ACTIVE','SORT','CODE','DEFAULT_VALUE','PROPERTY_TYPE',
                'ROW_COUNT','COL_COUNT','LIST_TYPE','MULTIPLE','XML_ID','FILE_TYPE','MULTIPLE_CNT','LINK_IBLOCK_ID',
                'WITH_DESCRIPTION','SEARCHABLE','FILTRABLE','IS_REQUIRED','VERSION','USER_TYPE','USER_TYPE_SETTINGS','HINT'
            ];
        }

        if(!$order){
            $order = ['SORT' => 'ASC'];
        }
        $rsProperty = \Bitrix\Iblock\PropertyTable::getList([
            'filter' => $filter,
            'order' => $order,
            'select' => $select,
            'limit' => $limit,
            'offset' => $offset,
        ]);

        $list = [];
        while($item = $rsProperty->fetch())
        {
            $list[$item['ID']] = $item;
        }

        return $list;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return PropertyCollection|PropertyEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return PropertyEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['=ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $code
     * @return PropertyEntity
     */
    public function getEntityByCode($code)
    {
        $list = $this->getList(['=CODE' => $code]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return PropertyCollection|PropertyEntity[]
     */
    public function getEntityByIds(array $arIds)
    {
        // parent::getEntityByIds($arIds); // TODO: Change the autogenerated stub
        $res = [];
        if( $arIds ){
            $res = $this->getList(['@ID' => $arIds]);
        }
        return $this->getEntityByResults($res);
    }

    /**
     * @param array $arResults
     * @return PropertyCollection|PropertyEntity[]
     */
    public function getEntityByResults(array $arResults)
    {
        return parent::getEntityByResults($arResults); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $arResult
     * @return PropertyEntity
     */
    public function getEntityByResult(array $arResult)
    {
        unset($arResult['USER_TYPE_SETTINGS_LIST']);
        if ($arResult['TIMESTAMP_X'] instanceof Main\Type\DateTime)
        {
            $arResult['TIMESTAMP_X'] = $arResult['TIMESTAMP_X']->format('Y-m-d H:i:s');
        }
        return parent::getEntityByResult($arResult);
    }
}