<?php

namespace Wt\Core\Factory\IBlock;

use Wt\Core\Collection\IBlock\PropertyEnumCollection;
use Wt\Core\Entity\IBlock\PropertyEnumEntity;
use Wt\Core\Support\Result;

class PropertyEnumFactory extends AIBlockFactory
{
    /**
     * @var int
     */
    protected $blockId;
    protected $propertyId;

    public function __construct($iblockId, $propertyId)
    {
        $this->blockId = (int)$iblockId;
        $this->propertyId = (int)$propertyId;
        parent::__construct();
    }

    /**
     * @param $data [PROPERTY_ID, VALUE, DEF = N, SORT = 500, EXTERNAL_ID|XML_ID = md5(uniqid)]
     * @return Result
     */
    public function add($data)
    {
        $result = new Result();
        $result->setData($data);
        $id = \CIBlockPropertyEnum::Add($data);
        if($id){
            $result->setId($id);
        } else {
            $result->getErrors()->add('Error add enum');
        }
        return $result;
    }

    public function __getCachePrefix()
    {
        return 'iblock/'.$this->getBlockId() . '/property/'.$this->getPropertyId().'/enum';
    }

    public function __getEntityClass()
    {
        return PropertyEnumEntity::class;
    }

    public function __getCollectionClass()
    {
        return PropertyEnumCollection::class;
    }

    public function getBlockId()
    {
        return $this->blockId;
    }

    public function getPropertyId()
    {
        return $this->propertyId;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        //p("real request IBlock\PropertyEnumFactory " . serialize($filter));

        if($this->getBlockId()) {
            $filter['IBLOCK_ID'] = $this->getBlockId();
        }
        if($this->getPropertyId()) {
            $filter['PROPERTY_ID'] = $this->getPropertyId();
        }
        if(!$order){
            $order = ['SORT'=>'ASC', 'VALUE'=>'ASC'];
        }

        $rs = \CIBlockPropertyEnum::GetList($order, $filter);

        $result = [];
        while($ar = $rs->Fetch())
        {
            unset($ar['PROPERTY_SORT']);
            unset($ar['PROPERTY_CODE']);
            unset($ar['PROPERTY_NAME']);
            unset($ar['EXTERNAL_ID']);
            $result[$ar['ID']] = $ar;
        }

        return $result;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return PropertyEnumCollection|PropertyEnumEntity[]
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return PropertyEnumEntity
     */
    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return PropertyEnumEntity
     */
    public function getEntityByXmlId($xmlId)
    {
        $list = $this->getList(['XML_ID' => $xmlId]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arIds
     * @return PropertyEnumCollection|PropertyEnumEntity[]
     */
    public function getEntityByIds(array $arIds)
    {
        return $this->getCollection()->getByIds($arIds);
    }

    /**
     * @param array $arResult
     * @return PropertyEnumEntity
     */
    public function getEntityByResult(array $arResult)
    {
        unset($arResult['TMP_ID']);
        unset($arResult['PROPERTY_ID']);
        return parent::getEntityByResult($arResult);
    }
}