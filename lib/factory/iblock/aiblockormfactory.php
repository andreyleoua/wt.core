<?php


namespace Wt\Core\Factory\IBlock;


use Wt\Core\Factory\AORMFactory;

abstract class AIBlockORMFactory extends AORMFactory
{
    protected function __getDepsModules()
    {
        return ['iblock'];
    }
}