<?php


namespace Wt\Core\Factory\IBlock;


use Wt\Core\Entity\Iblock\PropertyValueEntity;
use Wt\Core\Collection\Iblock\PropertyValueCollection;
use Wt\Core\Factory\AFactory;

class PropertyValueFactory extends AIBlockFactory
{
    /**
     * @var int
     */
    protected $blockId;
    /**
     * @var int
     */
    protected $elementId;

    public function __getCachePrefix()
    {
        return "iblock/{$this->getBlockId()}/element/{$this->getElementId()}/property";
    }

    public function __getEntityClass()
    {
        return PropertyValueEntity::class;
    }

    public function __getCollectionClass()
    {
        return PropertyValueCollection::class;
    }

    public function __construct($iblockId, $elementId)
    {
        $this->blockId = (int)$iblockId;
        $this->elementId = (int)$elementId;
        parent::__construct();
    }

    public function getBlockId()
    {
        return $this->blockId;
    }

    public function getElementId()
    {
        return $this->elementId;
    }

    public function getList($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        global $DB;

        if(!$order){
            $order = ['SORT' => 'ASC'];
        }
        $filter['EMPTY'] = 'N';
        $arFilter = $filter;
        $arOrder = $order;

        $IBLOCK_ID = (int)$this->getBlockId();
        $ELEMENT_ID = (int)$this->getElementId();
        $VERSION = \CIBlockElement::GetIBVersion($IBLOCK_ID);

        $strSqlSearch = "";
        foreach($arFilter as $key=>$val)
        {
            switch(strtoupper($key))
            {
                case "ACTIVE":
                    if($val=="Y" || $val=="N")
                        $strSqlSearch .= "AND BP.ACTIVE='".$val."'\n";
                    break;
                case "SEARCHABLE":
                    if($val=="Y" || $val=="N")
                        $strSqlSearch .= "AND BP.SEARCHABLE='".$val."'\n";
                    break;
                case "NAME":
                    if(strlen($val) > 0)
                        $strSqlSearch .= "AND ".\CIBlock::_Upper("BP.NAME")." LIKE ".\CIBlock::_Upper("'".$DB->ForSql($val)."'")."\n";
                    break;
                case "ID":
                    if(is_array($val))
                    {
                        if(!empty($val))
                            $strSqlSearch .= "AND BP.ID in (".implode(", ", array_map("intval", $val)).")\n";
                    }
                    elseif(strlen($val) > 0)
                        $strSqlSearch .= "AND BP.ID=".IntVal($val)."\n";
                    break;
                case "PROPERTY_TYPE":
                    if(strlen($val) > 0)
                        $strSqlSearch .= "AND BP.PROPERTY_TYPE='".$DB->ForSql($val)."'\n";
                    break;
                case "CODE":
                    if(strlen($val) > 0)
                        $strSqlSearch .= "AND ".\CIBlock::_Upper("BP.CODE")." LIKE ".\CIBlock::_Upper("'".$DB->ForSql($val)."'")."\n";
                    break;
                case "EMPTY":
                    if(strlen($val) > 0)
                    {
                        if($val=="Y")
                            $strSqlSearch .= "AND BEP.ID IS NULL\n";
                        elseif($VERSION!=2)
                            $strSqlSearch .= "AND BEP.ID IS NOT NULL\n";
                    }
                    break;
            }
        }

        $arSqlOrder = array();
        if($arOrder)
        {
            foreach($arOrder as $by=>$order)
            {
                $order = strtolower($order);
                if($order!="desc")
                    $order = "asc";

                $by = strtolower($by);
                if($by == "sort")		$arSqlOrder["BP.SORT"]=$order;
                elseif($by == "id")		$arSqlOrder["BP.ID"]=$order;
                elseif($by == "name")		$arSqlOrder["BP.NAME"]=$order;
                elseif($by == "active")		$arSqlOrder["BP.ACTIVE"]=$order;
                elseif($by == "value_id")	$arSqlOrder["BEP.ID"]=$order;
                elseif($by == "enum_sort")	$arSqlOrder["BEPE.SORT"]=$order;
                else
                    $arSqlOrder["BP.SORT"]=$order;
            }
        }

        $strSqlOrder = "";
        foreach($arSqlOrder as $key=>$val)
            $strSqlOrder.=", ".$key." ".$val;

        if($strSqlOrder!="")
            $strSqlOrder = ' ORDER BY '.substr($strSqlOrder, 1);

        if($VERSION==2)
            $strTable = "b_iblock_element_prop_m".$IBLOCK_ID;
        else
            $strTable = "b_iblock_element_property";

        $strSql = "
			SELECT BP.ID as PROPERTY_ID, BP.MULTIPLE, BEP.*
			FROM b_iblock B
				INNER JOIN b_iblock_property BP ON B.ID=BP.IBLOCK_ID
				LEFT JOIN ".$strTable." BEP ON (BP.ID = BEP.IBLOCK_PROPERTY_ID AND BEP.IBLOCK_ELEMENT_ID = ".$ELEMENT_ID.")
			WHERE B.ID = ".$IBLOCK_ID."
				".$strSqlSearch."
			".$strSqlOrder;

        if($VERSION==2)
        {
            $result = array();
            $arElements = array();
            $rs = $DB->Query($strSql);
            while($ar = $rs->Fetch())
            {
                if($ar["MULTIPLE"]=="N")
                {
                    if(!array_key_exists($ELEMENT_ID, $arElements))
                    {
                        $strSql = "
							SELECT *
							FROM b_iblock_element_prop_s".$IBLOCK_ID."
							WHERE IBLOCK_ELEMENT_ID = ".$ELEMENT_ID."
						";
                        $rs2 = $DB->Query($strSql);
                        $arElements[$ELEMENT_ID] = $rs2->Fetch();
                    }
                    $ar["DESCRIPTION"] = $arElements[$ELEMENT_ID]["DESCRIPTION_".$ar["PROPERTY_ID"]];
                    $ar["VALUE"] = $arElements[$ELEMENT_ID]["PROPERTY_".$ar["PROPERTY_ID"]];
                }
                if($arFilter["EMPTY"]=="N" && mb_strlen($ar["VALUE"]) == 0)
                    continue;

                $result[]=$ar;
            }
        }
        else
        {
            //$rs = new \CIBlockPropertyResult($DB->Query($strSql));
            $result = [];
            $rs = $DB->Query($strSql);
            while($ar = $rs->Fetch())
            {
                $result[]=$ar;
            }
        }
        $r = [];

        foreach ($result as $item){
            if(!isset($r[$item['PROPERTY_ID']])){
                $r[$item['PROPERTY_ID']] = [
                    //'PROPERTY_ID' => $item['PROPERTY_ID'],
                ];
            }

            $r[$item['PROPERTY_ID']]['ID'] = $item['PROPERTY_ID'];
            $valueId = $item['ID'];
            if($valueId){
                $r[$item['PROPERTY_ID']]['VALUE'][$valueId] = $item['VALUE'];
                $r[$item['PROPERTY_ID']]['VALUE_ENUM'][$valueId] = (int)$item['VALUE_ENUM'];
                $r[$item['PROPERTY_ID']]['VALUE_NUM'][$valueId] = (float)$item['VALUE_NUM'];
                $r[$item['PROPERTY_ID']]['DESCRIPTION'][$valueId] = $item['DESCRIPTION'];
            } else {
                $r[$item['PROPERTY_ID']]['VALUE'][] = $item['VALUE'];
                $r[$item['PROPERTY_ID']]['VALUE_ENUM'][] = (int)$item['VALUE_ENUM'];
                $r[$item['PROPERTY_ID']]['VALUE_NUM'][] = (float)$item['VALUE_NUM'];
                $r[$item['PROPERTY_ID']]['DESCRIPTION'][] = $item['DESCRIPTION'];
            }
        }

        return $r;
    }

    /**
     * @param array $filter
     * @param array $order
     * @param array $select
     * @param null $limit
     * @param null $offset
     * @return PropertyValueCollection
     */
    public function getCollection($filter = [], $order = [], $select = [], $limit = null, $offset = null)
    {
        return parent::getCollection($filter, $order, $select, $limit, $offset);
    }

    /**
     * @param $id
     * @return PropertyValueEntity
     */
    public function getEntityById($id)
    {
        $list = $this->getList(['ID' => $id]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param $code
     * @return PropertyValueEntity
     */
    public function getEntityByCode($code)
    {
        $list = $this->getList(['CODE' => $code]);
        if($list){
            return $this->getEntityByResult(reset($list));
        }
        return null;
    }

    /**
     * @param array $arResult
     * @return PropertyValueEntity
     */
    public function getEntityByResult(array $arResult)
    {
        return parent::getEntityByResult($arResult);
    }
}