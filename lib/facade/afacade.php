<?php


namespace Wt\Core\Facade;


use Wt\Core\Interfaces\IApp;

class AFacade
{
    /**
     * @var IApp
     */
    protected $app;

    public function __construct(IApp $app)
    {
        $this->app = $app;
    }

    protected function getApp()
    {
        return $this->app;
    }

    protected function getConfig()
    {
        return $this->getApp()->config();
    }
}