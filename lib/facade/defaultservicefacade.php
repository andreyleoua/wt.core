<?php

namespace Wt\Core\Facade;

use Wt\Core\Entity\UserEntity;
use Wt\Core\Factory\UserFactory;
use Wt\Core\Assets\Assets;
use Wt\Core\Support\Cli;
use Wt\Core\Support\Collection;
use Wt\Core\Support\Logger;
use Wt\Core\Support\Registry;
use Wt\Core\Support\System;
use Wt\Core\Templater\Templater;

class DefaultServiceFacade extends AFacade
{

    /**
     * @return DefaultBitrixFacade
     */
    public function bitrix()
    {
        return $this->app->container()->make(DefaultBitrixFacade::class, [$this->app]);
    }

    /**
     * @return DefaultFormFacade
     */
    public function form()
    {
        return $this->app->container()->make(DefaultFormFacade::class, [$this->app]);
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function collection(array $data = [])
    {
        return $this->app->container()->make(Collection::class, [$data]);
    }

    /**
     * @return Logger
     */
    public function logger()
    {
        return $this->app->container()->make('logger:service');
    }

    /**
     * @return System
     */
    public function system()
    {
        return $this->app->container()->make(System::class);
    }

    /**
     * @return Cli
     */
    public function cli()
    {
        return $this->app->container()->make(Cli::class);
    }

    /**
     * @return Registry
     */
    public function registry()
    {
        return $this->app->container()->make('registry');
    }

    /**
     * @return Cli\Php
     */
    public function cliPhp()
    {
        $container = $this->app->container();
        $config = $this->app->config();
        return $container->make(Cli\Php::class, [
            $this->cli(),
            $config->get('php_path', 'php'),
            $_SERVER['DOCUMENT_ROOT']
        ]);
    }

    /**
     * @param null $returnTemplate
     * @return Templater|string
     */
    public function templater($returnTemplate = null)
    {
        if( $returnTemplate !== null ){
            return $this->app->container()->make('templater')->get($returnTemplate);
        }

        return $this->app->container()->make('templater');
    }

    /**
     * @return Assets
     */
    public function assets()
    {
        return $this->app->container()->make('assets');
    }

    /**
     * @return UserEntity
     */
    public function user()
    {
        if(!$this->app->container()->has('userService')) {
            /**
             * @var UserFactory $userFactory
             */
            if(!$GLOBALS['USER']){
                $GLOBALS['USER'] = new \CUser();
            }

            $userFactory = app()->factory()->main()->getUserFactory();
            $userId = (int)$GLOBALS['USER']->GetID();
            if($userId){
                $userEntity = $userFactory->getEntityById($userId);
            } else {
                $userEntity = $userFactory->getNoAuthUserEntity();
            }
            $this->app->container()->singleton('userService', $userEntity);
        }

        return $this->app->container()->make('userService');
    }
}