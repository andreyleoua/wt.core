<?php


namespace Wt\Core\Facade;


use Wt\Core\Form\Field;
use Wt\Core\Form\Field\DefaultField;

class DefaultFormFieldFacade extends AFacade
{
    protected $profileList;
    protected $profileId;

    protected function getParams($className)
    {
        if(is_null($this->profileList)){
            $this->profileList = $this->getConfig()->get('form_field_profile_list', []);
        }
        if(is_null($this->profileId)){
            $this->profileId = $this->getConfig()->get('form_field_profile_id', 'default');
        }
        $fieldConfig = $this->profileList->get($this->profileId, []);
        $params = $fieldConfig->get($className, []);
        if(!$params->count()){
            $params = $fieldConfig->get(Field\DefaultField::class, []);
        }
        return $params;
    }

    /**
     * @param $className
     * @param $data
     * @return mixed
     */
    public function make($className, $data)
    {
        $params = $this->getParams($className);
        return $this->app->container()->make($className, [$data, $params]);
    }

    /**
     * @param array $data
     * @return Field\ButtonField
     */
    public function button($data = [])
    {
        return $this->make(Field\ButtonField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\CheckboxField
     */
    public function checkbox($data = [])
    {
        return $this->make(Field\CheckboxField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\ColorField
     */
    public function color($data = [])
    {
        return $this->make(Field\ColorField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\DateField
     */
    public function date($data = [])
    {
        return $this->make(Field\DateField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\DatetimeLocalField
     */
    public function datetimeLocal($data = [])
    {
        return $this->make(Field\DatetimeLocalField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\EmailField
     */
    public function email($data = [])
    {
        return $this->make(Field\EmailField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\FileField
     */
    public function file($data = [])
    {
        return $this->make(Field\FileField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\HiddenField
     */
    public function hidden($data = [])
    {
        return $this->make(Field\HiddenField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\ImageField
     */
    public function image($data = [])
    {
        return $this->make(Field\ImageField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\MonthField
     */
    public function month($data = [])
    {
        return $this->make(Field\MonthField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\NumberField
     */
    public function number($data = [])
    {
        return $this->make(Field\NumberField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\PasswordField
     */
    public function password($data = [])
    {
        return $this->make(Field\PasswordField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\RadioField
     */
    public function radio($data = [])
    {
        return $this->make(Field\RadioField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\RangeField
     */
    public function range($data = [])
    {
        return $this->make(Field\RangeField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\ResetField
     */
    public function reset($data = [])
    {
        return $this->make(Field\ResetField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\SearchField
     */
    public function search($data = [])
    {
        return $this->make(Field\SearchField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\SelectField
     */
    public function select($data = [])
    {
        return $this->make(Field\SelectField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\SubmitField
     */
    public function submit($data = [])
    {
        return $this->make(Field\SubmitField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\TelField
     */
    public function tel($data = [])
    {
        return $this->make(Field\TelField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\TextField
     */
    public function text($data = [])
    {
        return $this->make(Field\TextField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\TextareaField
     */
    public function textarea($data = [])
    {
        return $this->make(Field\TextareaField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\TimeField
     */
    public function time($data = [])
    {
        return $this->make(Field\TimeField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\UrlField
     */
    public function url($data = [])
    {
        return $this->make(Field\UrlField::class, $data);
    }

    /**
     * @param array $data
     * @return Field\WeekField
     */
    public function week($data = [])
    {
        return $this->make(Field\WeekField::class, $data);
    }

}