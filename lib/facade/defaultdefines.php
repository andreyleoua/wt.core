<?php


namespace Wt\Core\Facade;


use Wt\Core\App\ADefines;

class DefaultDefines extends ADefines
{

    const IMAGE_RESOLUTION_LOWEST = 100;
    const IMAGE_RESOLUTION_LOW = 200;
    const IMAGE_RESOLUTION_MIDDLE = 400;
    const IMAGE_RESOLUTION_HIGH = 800;
    const IMAGE_RESOLUTION_PRIME = 1600;

}