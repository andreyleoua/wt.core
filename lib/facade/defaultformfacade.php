<?php


namespace Wt\Core\Facade;


use Wt\Core\Form\Form;

class DefaultFormFacade extends AFacade
{
    /**
     * @return DefaultFormFieldFacade
     */
    public function field()
    {
        return $this->app->container()->make(DefaultFormFieldFacade::class, [$this->app]);
    }

    /**
     * @param array $data
     * @return Form
     */
    public function make($data = [])
    {
        return $this->app->container()->make(Form::class, [$data]);
    }
}