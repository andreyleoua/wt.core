<?php


namespace Wt\Core\Facade;


use Bitrix\Main\DB\Connection;
use Bitrix\Main\EventManager;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\HttpResponse;
use Bitrix\Main\Server;
use CMain;
use CDatabase;

class DefaultBitrixFacade extends AFacade
{
    /**
     * @return HttpRequest
     */
    public function request()
    {
        return $this->app->container()->make('bxRequest');
    }

    /**
     * @return HttpResponse
     */
    public function response()
    {
        return $this->app->container()->make('bxResponse');
    }

    /**
     * @return EventManager
     */
    public function eventManager()
    {
        return $this->app->container()->make('bxEventManager');
    }

    /**
     * @return Server
     */
    public function server()
    {
        return $this->app->container()->make('bxServer');
    }

    /**
     * @return Connection
     */
    public function connection()
    {
        return $this->app->container()->make('bxConnection');
    }

    /**
     * @return CDatabase
     */
    public function gDb()
    {
        return $GLOBALS['DB'];
    }

    /**
     * @return HttpApplication
     */
    public function application()
    {
        return $this->app->container()->make('bxApp');
    }

    /**
     * @return CMain
     */
    public function gApplication()
    {
        return $GLOBALS['APPLICATION'];
    }

    public function restartBuffer()
    {
        return $this->gApplication()->RestartBuffer();
    }
}