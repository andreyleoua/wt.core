<?php

namespace Wt\Core\Facade;

use Wt\Core\AbstractFactory\ShopAbstractFactory;
use Wt\Core\ManualFactory\IBlockManualFactory;
use Wt\Core\ManualFactory\HlBlockManualFactory;
use Wt\Core\AbstractFactory\IBlockAbstractFactory;
use Wt\Core\AbstractFactory\MainAbstractFactory;

class DefaultFactoryFacade extends AFacade
{

    /**
     * @return IBlockAbstractFactory
     */
    public function catalog()
    {
        $catalogIblockId = $this->app->defines()->get('CATALOG_IBLOCK_ID');

        if($catalogIblockId !== null ){
            return $this->iBlock()->getEntityById($catalogIblockId);
        }
        return null;
    }

    /**
     * @return IBlockManualFactory
     */
    public function iBlock()
    {
        return $this->app->container()->make('iBlockManualFactory');
    }

    /**
     * фабрика абстрактных фабрик - блок
     *
     * @return HlBlockManualFactory
     */
    public function hlBlock()
    {
        return $this->app->container()->make('hlBlockManualFactory');
    }

    /**
     * @return MainAbstractFactory
     */
    public function main()
    {
        return $this->app->container()->make('mainAbstractFactory');
    }

    /**
     * @return ShopAbstractFactory
     */
    public function shop()
    {
        return $this->app->container()->make('shopAbstractFactory');
    }
}