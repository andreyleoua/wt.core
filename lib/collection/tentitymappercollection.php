<?php


namespace Wt\Core\Collection;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IFactory;

trait TEntityMapperCollection
{
    /**
     * @var AFactory
     */
    protected $factory;

    public function __construct(array $array)
    {

        parent::__construct($array, false);

        //$this->map([$this, 'setMapperByValue']);
    }

    public function offsetSet($offset, $value): void
    {
        parent::offsetSet($offset, $value);
//        if(is_null($offset)){
//            $offset = array_key_last($this->rawData);
//        }
//        $this->setMapperByValue($value);
    }

    public function offsetUnset($offset): void
    {
//        $this->unsetMapperByValue($this->get($offset));
        parent::offsetUnset($offset);
    }


    /**
     * @param array $ids
     * @return $this|AEntity[]
     */
    public function getByIds(array $ids)
    {
        return $this->only($ids);
    }

    /**
     * @param $code
     * @return AEntity|null
     */
    public function &getByCode($code)
    {
        $nullEntity = null;
        if(!$code) {
            return $nullEntity;
        }

        $id = $this->getFactory()->__mapper()->getId($code, 'code');

        if($id) {
            return $this->get($id);
        }


//        foreach ($this->all() as &$entity){
//            if($entity['CODE'] == $code) {
//                return $entity;
//            }
//        }
        return $nullEntity;
    }

    /**
     * @param $codeList
     * @return AEntityCollection|AEntity[]
     */
    public function getByCodeList($codeList)
    {
        $list = [];
        foreach ($codeList as $code){
            $entity = $this->getByCode($code);
            if($entity){
                $list[$entity->getId()] = $entity;
            }
        }
        return $this->newInstance($list);
        //        return $this->filter(function(PropertyEntity $entity) use ($codeList){
        //            return in_array($entity->getCode(), $codeList);
        //        });
    }

    /**
     * @param string|array $field
     * @param mixed $value
     * @return AEntity|null
     */
    public function &getByField($field, $value = null)
    {
        $filter = $field;
        if(!is_array($field)){
            $filter = [$field => $value];
        }

        foreach ($this->all() as &$entity){
            $bFind = true;
            foreach ($filter as $k => $v){
                if($entity[$k] !== $v){
                    $bFind = false;
                    break;
                }
            }
            if($bFind){
                return $entity;
            }
        }
        $nullEntity = null;
        return $nullEntity;
    }

    /**
     * @param $xmlId
     * @return AEntity|null
     */
    public function &getByXmlId($xmlId)
    {
        $nullEntity = null;
        if(!$xmlId) {
            return $nullEntity;
        }

        $id = $this->getFactory()->__mapper()->getId($xmlId, 'xml_id');

        if($id) {
            return $this->get($id);
        }

//        foreach ($this->all() as &$entity){
//            if($entity['XML_ID'] == $xmlId) {
//                return $entity;
//            }
//        }
        return $nullEntity;
    }

    /**
     * @return AFactory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @param AFactory $factory
     * @return $this
     */
    public function __setFactory(IFactory $factory)
    {
        $this->factory = $factory;
        return $this;
    }
}