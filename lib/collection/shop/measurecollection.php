<?php


namespace Wt\Core\Collection\Shop;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\Shop\MeasureEntity;

class MeasureCollection extends AEntityCollection
{

    /**
     * @return MeasureEntity|null
     */
    public function getDefault()
    {
        /** @var MeasureEntity $entity */
        foreach ($this->all() as &$entity){
            if($entity->isDefault()) {
                return $entity;
            }
        }
        return null;
    }

    /**
     * @param $id
     * @param null $default
     * @return MeasureEntity|null
     */
    public function &get($id, $default = null)
    {
        return parent::get($id, $default); // TODO: Change the autogenerated stub
    }

    /**
     * @return MeasureEntity
     */
    public function &first(callable $callback = null, $default = null)
    {
        return parent::first($callback, $default); // TODO: Change the autogenerated stub
    }

    /**
     * @return MeasureEntity
     */
    public function &last()
    {
        return parent::last(); // TODO: Change the autogenerated stub
    }

    /**
     * @return MeasureEntity
     */
    public function shift()
    {
        return parent::shift(); // TODO: Change the autogenerated stub
    }

    /**
     * @return MeasureEntity
     */
    public function pop()
    {
        return parent::pop(); // TODO: Change the autogenerated stub
    }
}