<?php


namespace Wt\Core\Collection\IBlock;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\IBlock\SectionEntity;

class SectionCollection extends AEntityCollection
{

    /**
     * @param $id
     * @param null $default
     * @return SectionEntity|null
     */
    public function &get($id, $default = null)
    {
        return parent::get($id, $default); // TODO: Change the autogenerated stub
    }

    /**
     * @return SectionEntity
     */
    public function &first(callable $callback = null, $default = null)
    {
        return parent::first($callback, $default); // TODO: Change the autogenerated stub
    }

    /**
     * @return SectionEntity
     */
    public function &last()
    {
        return parent::last(); // TODO: Change the autogenerated stub
    }

    /**
     * @return SectionEntity
     */
    public function shift()
    {
        return parent::shift(); // TODO: Change the autogenerated stub
    }

    /**
     * @return SectionEntity
     */
    public function pop()
    {
        return parent::pop(); // TODO: Change the autogenerated stub
    }
}