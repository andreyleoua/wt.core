<?php


namespace Wt\Core\Collection\IBlock;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\IBlock\PropertyValueEntity;
use Wt\Core\Factory\IBlock\PropertyValueFactory;

class PropertyValueCollection extends AEntityCollection
{
    /**
     * @param $id
     * @param null $default
     * @return PropertyValueEntity
     */
    public function &get($id, $default = null)
    {
        return parent::get($id, $default);
    }

    /**
     * @param $code
     * @return PropertyValueEntity|null
     */
    public function &getByCode($code)
    {
        $propertyEntity = app()->factory()->iBlock()->getEntityById($this->getFactory()->getBlockId())->getPropertyFactory()->getEntityByCode($code);
        if(!$propertyEntity){
            return null;
        }
        return parent::get($propertyEntity->getId());
    }

    /**
     * @return PropertyValueFactory
     */
    public function getFactory()
    {
        return parent::getFactory();
    }
}