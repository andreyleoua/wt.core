<?php

namespace Wt\Core\Migrations;

use Wt\Core\App\ModuleEntity;
use Wt\Core\Interfaces\IMigration;

class MigrationService implements IMigration
{
    /** @var ModuleEntity */
    protected ModuleEntity $module;

    /** @var IMigration[] */
    protected $migrations = [];

    public function __construct(ModuleEntity $module)
    {
        $this->module = $module;
    }

    public function setMigrations($migrations)
    {
        $this->migrations = $migrations;
    }

    /**
     * @return IMigration[]
     */
    public function getMigrations()
    {
        return $this->migrations;
    }

    public function up()
    {
        $app = $this->getModule()->getApp();
        collect($this->getMigrations())->map(function (AMigration $migration) use ($app){
            $migration->up();
            $app->setting()->set('migration.' . $migration->getType() . '.version', $this->getModule()->getVersion());
        });
        $app->setting()->set('migration.version', $this->getModule()->getVersion());
        $app->setting()->clearCache();
    }

    public function down()
    {
        $app = $this->getModule()->getApp();
        collect($this->getMigrations())->map(function (AMigration $migration) use ($app){
            $migration->down();
            $app->setting()->remove('migration.' . $migration->getType() . '.version');
        });
        $app->setting()->remove('migration.version');
        $app->setting()->clearCache();
    }

    public function isCanUp()
    {
        $app = $this->getModule()->getApp();
        $migrationVersion = $app->setting()->get('migration.version', '0.0.0');
        $currentVersion = $this->getModule()->getVersion();
        return version_compare($currentVersion, $migrationVersion) > 0;
    }

    /**
     * @return ModuleEntity
     */
    public function getModule(): ModuleEntity
    {
        return $this->module;
    }
}