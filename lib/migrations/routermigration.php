<?php

namespace Wt\Core\Migrations;

class RouterMigration extends AMigration
{

    public function up()
    {
        $installer = $this->getModule()->getInstaller();
        if (is_callable([$installer, 'InstallRouters'])) {
            $installer->InstallRouters();
        }
    }

    public function down()
    {
        $installer = $this->getModule()->getInstaller();
        if (is_callable([$installer, 'UnInstallRouters'])) {
            $installer->UnInstallRouters();
        }
    }

    public function getBaseClassName()
    {
        return null;
    }

    public function getType()
    {
        return 'router';
    }
}