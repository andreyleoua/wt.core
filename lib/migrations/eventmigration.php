<?php

namespace Wt\Core\Migrations;

class EventMigration extends AMigration
{

    public function up()
    {
        $this->getModule()->getInstaller()->InstallEvents();
    }

    public function down()
    {
        $this->getModule()->getInstaller()->UnInstallEvents();
    }

    public function getBaseClassName()
    {
        return null;
    }

    public function getType()
    {
        return 'event';
    }
}