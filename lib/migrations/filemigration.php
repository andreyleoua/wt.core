<?php

namespace Wt\Core\Migrations;

class FileMigration extends AMigration
{

    public function up()
    {
        $installer = $this->getModule()->getInstaller();
        if (is_callable([$installer, 'InstallFiles'])) {
            $installer->InstallFiles();
        }
    }

    public function down()
    {
        $installer = $this->getModule()->getInstaller();
        if (is_callable([$installer, 'UnInstallFiles'])) {
            $installer->UnInstallFiles();
        }
    }

    public function getBaseClassName()
    {
        return null;
    }

    public function getType()
    {
        return 'file';
    }
}