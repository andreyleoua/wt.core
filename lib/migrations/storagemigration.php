<?php

namespace Wt\Core\Migrations;

use Wt\Core\ORM\ADataManager;

class StorageMigration extends AMigration
{

    public function getBaseClassName()
    {
        return ADataManager::class;
    }

    public function up()
    {
        $classList = $this->getClassList();

        /** @var ADataManager $className */
        foreach ($classList as $className) {
            $entity = $className::getEntity();
            $connection = $entity->getConnection();
            $tableName = $entity->getDBTableName();

            if ($className::isTableExists()) {
                $className::addNewFields();
                $className::updateFieldsTypes();
            } else {
                $entity->createDbTable();
                $className::createIndexes();
            }
        }
    }

    public function down()
    {
        $classList = static::getClassList();

        /** @var ADataManager $className */
        foreach ($classList as $className) {
            $entity = $className::getEntity();
            $connection = $entity->getConnection();
            $tableName = $entity->getDBTableName();

            if ($connection->isTableExists($tableName)) {
                $connection->dropTable($tableName);
            }
        }
    }

    public function getType()
    {
        return 'storage';
    }
}
