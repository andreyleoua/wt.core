<?php

namespace Wt\Core\Migrations;

use DirectoryIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Wt\Core\App\ModuleEntity;
use Wt\Core\Interfaces\IMigration;
use Wt\Core\Templater\Shell;

abstract class AMigration implements IMigration
{
    /** @var ModuleEntity */
    protected ModuleEntity $module;

    public function __construct(ModuleEntity $module)
    {
        $this->module = $module;
    }

    abstract public function up();

    abstract public function down();

    abstract public function getBaseClassName();

    abstract public function getType();

    public function getClassList()
    {
        $result = [];
        if (is_null($this->getBaseClassName())) {
            return $result;
        }
        if (!class_exists($this->getBaseClassName())) {
            return $result;
        }

        $autoloadPath = $this->getModule()->getAutoloadPath();
        $moduleNamespace = $this->getModule()->getNamespace();
        $directory = new RecursiveDirectoryIterator($autoloadPath);
        $iterator = new RecursiveIteratorIterator($directory);

        /** @var DirectoryIterator $entry */
        foreach ($iterator as $entry) {
            $isValidFile = $entry->isFile() && $entry->isReadable() && $entry->getExtension() === 'php';
            if (!$isValidFile) {
                continue;
            }
            $relativePath = str_replace($autoloadPath, '', $entry->getPath());
            $namespace = $moduleNamespace . str_replace('/', '\\', $relativePath) . '\\';
            $className = $entry->getBasename('.php');

            $fullClassName = $namespace . $className;
            $shell = new Shell(null);
            $classExists = $shell->getCallResult(function () use ($fullClassName) {
                if (!class_exists($fullClassName)) {
                    return false;
                }
                $class = new \ReflectionClass($fullClassName);
                $isPseudoClass = $class->isAbstract() || $class->isInterface() || $class->isTrait();
                if ($isPseudoClass) {
                    return false;
                }
                return true;
            });
            if (
                $classExists->isSuccess()
                && $classExists->getReturn()
                && is_subclass_of($fullClassName, $this->getBaseClassName())
            ) {
                $result[] = $fullClassName;
            }
        }

        return $result;
    }

    /**
     * @return ModuleEntity
     */
    public function getModule(): ModuleEntity
    {
        return $this->module;
    }
}