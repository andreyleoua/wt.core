<?php


namespace Wt\Core\Media;


use Wt\Core\Support\Error;
use Wt\Core\Collection\ErrorCollection;
use Wt\Core\Tools as Tools;

class Webp
{
    public $errors;
    public $force = false;
    public $isDamaged = false;
    public $mime = '';

    private static $extension = 'webp';
    private $quality = 80;
    private $absPathIn = '';
    private $relPathIn = '';
    private $absPathOut = '';
    private $relPathOut = '';

    public function __construct($srcPath = '', $quality = 80)
    {
        $this->errors = new ErrorCollection();
        $this->isEnable();

        $this->setPathIn($srcPath)->setQuantity($quality);
    }

    public function setQuantity($quality)
    {
        $this->quality = (int)$quality;

        if($this->quality < 0) {
            $this->quality = 0;
        }

        if($this->quality > 100) {
            $this->quality = 100;
        }

        return $this;
    }

    /**
     * Проверка возможности конвертирования
     *
     * @return bool
     */
    private function isEnable()
    {
        if(!\function_exists('imagewebp')) {
            $this->errors->setError(new Error('Not found function imagewebp'));
            return false;
        }

        if(!\extension_loaded('gd')) {
            $this->errors->setError(new Error('Not found extension gd'));
            return false;
        }

        return true;
    }

    public static function onFileDelete($arFile)
    {
        $depFile = self::getDefaultPathOut(self::getBitrixFilePath($arFile));

        if(file_exists($depFile)){
            @unlink($depFile);
        }
    }

    public function setBitrixFileArray($arFile)
    {
        $this->setPathIn(self::getBitrixFilePath($arFile));
    }

    protected static function getBitrixFilePath($arFile)
    {
        $uploadDir = \Bitrix\Main\Config\Option::get('main', 'upload_dir', 'upload');
        return $_SERVER["DOCUMENT_ROOT"]."/$uploadDir/".$arFile["SUBDIR"]."/".$arFile["FILE_NAME"];
    }

    /**
     * Процесс оптимизации
     *
     * @return $this
     */
    public function make()
    {
        if(!$this->force && \file_exists($this->getPathOut())) {
            return $this;
        }

        if(!$this->isEnable()) {
            return $this;
        }

        if(!\is_file($this->absPathIn)) {
            $this->errors->setError(new Error('File not found'));
            return $this;
        }

        $dirname = \dirname($this->getPathOut());

        if(!\is_dir($dirname)) {
            \mkdir($dirname,0777, true);
        }

        $this->mime = \mime_content_type($this->absPathIn);
        try {
            switch($this->mime) {
                case 'image/png':
                    $this->makeImageByTypePng();
                    break;
                case 'image/jpeg':
                    $this->makeImageByTypeJpg();
                    break;
                case 'image/gif':
                    $this->makeImageByTypeGif();
                    break;
                default:
                    $this->errors->setError(new Error("Выбранный формат файла $this->mime не может быть сконвертирован в " . self::$extension));
            }
        } catch (\Exception $e) {
            $this->errors->setError(new Error('Exception: ' . $e->getMessage()));
        }

        return $this;
    }

    private function getReMakeImage()
    {
        if(!extension_loaded('imagick')){
            $this->errors->setError(new Error("extension imagick not loaded"));
            return false;
        }

        if (!class_exists('imagick')) {
            $this->errors->setError(new Error("class imagick not exists"));
            return false;
        }

        $fileInfo = \pathinfo($this->absPathIn);
        $tmpFile =  $fileInfo['dirname'] . '/' . $fileInfo['filename'] . '_tmp.' . $fileInfo['extension'];
        $r = false;
        try {
            $im = new \Imagick();
            $im->readimage($this->absPathIn);
            $im->coalesceImages();
            $im = $im->deconstructImages();
            if (file_exists($tmpFile)) {
                @unlink($tmpFile);
            }
            if (count($im) > 1) {
                $r = $im->writeImages($tmpFile, true);
            } else {
                $r = $im->writeImage($tmpFile);
            }
            $im->clear();
            $im->destroy();
        } catch (\Exception $e) {
            $this->errors->setError(new Error('Exception: ' . $e->getMessage()));
            return false;
        }

        if($r && (bool)\filesize($tmpFile)) {
            return $tmpFile;
        }

        return false;
    }

    private function makeVips()
    {
        if(!class_exists('\Jcupitt\Vips\Image')) {
            $this->errors->setError(new Error("class \Jcupitt\Vips\Image not exists"));
            return false;
        }

        try {
            $image = \Jcupitt\Vips\Image::newFromFile($this->absPathIn, [
                'n' => -1,
                'access' => \Jcupitt\Vips\Access::SEQUENTIAL,
            ]);
            $image->writeToFile($this->absPathOut);
        } catch (\Exception $e) {
            $this->isDamaged = true;
            return false;
        }

        return (bool)\filesize($this->absPathOut);
    }

    /**
     * Make images is successed
     *
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->errors->isEmpty();
    }

    /**
     * @param $rel
     * @return string
     */
    public function getPathOut($rel = false)
    {
        if(!$this->absPathOut) {
            $this->setPathOut(self::getDefaultPathOut($this->absPathIn));
        }
        if($rel) {
            return $this->relPathOut;
        }
        return $this->absPathOut;
    }

    private static function getDefaultPathOut($pathIn)
    {
        $fileInfo = \pathinfo($pathIn);
        return $fileInfo['dirname'] . '/' . $fileInfo['filename'] . '.' . self::$extension;
    }

    /**
     * @param $path
     * @param $rel
     * @return $this
     */
    public function setPathOut($path, $rel = false)
    {
        if($rel) {
            $this->relPathOut = $path;
            $this->absPathOut = $_SERVER['DOCUMENT_ROOT'] . $path;

            return $this;
        }

        $this->absPathOut = $path;
        $this->relPathOut = Tools::removeDocRoot($path);

        return $this;
    }

    /**
     * @param $path
     * @param $rel
     * @return $this
     */
    public function setPathIn($path, $rel = false)
    {
        if($rel) {
            $this->relPathIn = $path;
            $this->absPathIn = $_SERVER['DOCUMENT_ROOT'] . $path;

            return $this;
        }

        $this->absPathIn = $path;
        $this->relPathIn = Tools::removeDocRoot($path);

        return $this;
    }

    protected function makeImageByTypePng()
    {
        $pngImg = imagecreatefrompng($this->absPathIn);
        // get dimens of image
        $w = imagesx($pngImg);
        $h = imagesy($pngImg);;
        // create a canvas
        $img = imagecreatetruecolor ($w, $h);
        imageAlphaBlending($img, false);
        imageSaveAlpha($img, true);
        // By default, the canvas is black, so make it transparent
        $trans = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagefilledrectangle($img, 0, 0, $w - 1, $h - 1, $trans);
        // copy png to canvas
        imagecopy($img, $pngImg, 0, 0, 0, 0, $w, $h);
        // lastly, save canvas as a webp
        \imageWebp(
            $img,
            $this->getPathOut(),
            $this->quality
        );
        \imagedestroy($img);
    }

    protected function makeImageByTypeJpg()
    {
        $img = \imageCreateFromJpeg($this->absPathIn);
        \imageWebp(
            $img,
            $this->getPathOut(),
            $this->quality
        );
        \imagedestroy($img);
    }

    protected function makeImageByTypeGif()
    {
        \Bitrix\Main\Loader::includeModule('wt.media');

        if(!$this->makeVips()) {
            $tmpFile = $this->getReMakeImage();
            if($tmpFile) {
                $this->setPathIn($tmpFile);
                if(!$this->makeVips()) {
                    $this->errors->setError(new Error("Failed to process the gif. Image is damaged [2]"));
                }
            } else {
                $this->errors->setError(new Error("Failed to process the gif. Image is damaged"));
            }
            @unlink($tmpFile);
        }
    }
}