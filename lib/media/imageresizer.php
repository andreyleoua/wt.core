<?php


namespace Wt\Core\Media;


use Wt\Core\Entity\Main\FileEntity;

class ImageResizer
{
    protected $fileEntity;
    protected $imageResizerService;
    
    public function __construct(FileEntity $fileEntity, ImageResizerService $imageResizerService)
    {
        $this->fileEntity = $fileEntity;
        $this->imageResizerService = $imageResizerService;
    }

    /**
     * @return FileEntity
     */
    public function getFileEntity()
    {
        return $this->fileEntity;
    }

    /**
     * @return ImageResizerService
     */
    public function getImageResizerService()
    {
        return $this->imageResizerService;
    }

    public function getSrc($lvl = ResizeLevel::ORIGIN, $filter = false)
    {
        return $this->getImageResizerService()->getSrc($this->getFileEntity(), $lvl, $filter);
    }

    public function getImageData($lvl = ResizeLevel::ORIGIN, $filter = false)
    {
        return $this->getImageResizerService()->getResizeImageData($this->getFileEntity(), $lvl, $filter);
    }
}