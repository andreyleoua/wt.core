<?php


namespace Wt\Core\Media;


class ResizeLevel
{
    public const ORIGIN = 0;
    public const TINY = 1;
    public const SMALL = 2;
    public const MIDDLE = 3;
    public const BIG = 4;
    public const HUGE = 5;
}