<?php


namespace Wt\Core\Media;


class Tools
{

    /**
     * из ID изображения получаем массив со всей инфой в том числе ресайзы
     * на базе \CFile::getFileArray
     * альтернативное изображение будет добавлено в DB, при необходимости отресайзится
     *
     * @param int $id ID файла
     * @param array $arResize массив вида
     * [
     *  'SMALL' => ['width' => 100, 'height' => 100],
     *  'MIDDLE' => ['width' => 200, 'height' => 200],
     * ]
     * результат ['RESIZE']['SMALL'] = /upload/resize_cache ...
     * @param string $alt альтернативное изображение, путь от DOCUMENT_ROOT
     * @return array
     */
    public static function getPictureFields($id = 0, array $arResize = [], $alt = '')
    {
        $id = (int)$id;

        $fileData = \CFile::getFileArray($id);

        if( !$fileData ){
            $fileData = [];
        }

        $uploadDirName = \COption::GetOptionString("main", "upload_dir", "upload");

        if(!$fileData && !empty($alt)){
            $md5 = md5($_SERVER['DOCUMENT_ROOT'].$alt);
            $res = \CFile::GetList([], array('MODULE_ID'=>'alt_image', 'EXTERNAL_ID' => $md5));
            if($res_arr = $res->GetNext(true, false)) {
                $fileData = $res_arr;
            } else {
                $altFile = \CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . $alt);
                $altFile['MODULE_ID'] = 'alt_image';
                $altFile['external_id'] = $md5;
                $id = \CFile::SaveFile($altFile, 'alt_image');
                if ($id > 0) {
                    $fileData = \CFile::getFileArray($id);
                }
            }
            $fileData['SRC'] = "/" . $uploadDirName . "/" . $fileData["SUBDIR"] . "/" . $fileData["FILE_NAME"];
        }

        if ($fileData && $arResize) {
            if(\CFile::IsImage($fileData['FILE_NAME'], $fileData['CONTENT_TYPE'])){
                foreach ($arResize as $name => $arSize) {

                    if(is_numeric($arSize)) {
                        $arSize = [
                            'width' => $arSize,
                            'height' => $arSize,
                        ];
                    }

                    if (is_array($arSize) && array_key_exists('width', $arSize) && array_key_exists('height', $arSize)) {
                        $resResize = \CFile::ResizeImageGet(
                            $fileData['ID'],
                            $arSize,
                            BX_RESIZE_IMAGE_PROPORTIONAL
                        );
                        $fileData['RESIZE'][$name] = $resResize['src']?:$fileData['SRC'];
                    }
                }
            } elseif(strtolower(GetFileExtension($fileData['FILE_NAME'])) == 'svg'){
                foreach ($arResize as $name => $arSize) {
                    $fileData['RESIZE'][$name] = $fileData['SRC'];
                }
            }

        }

        return $fileData;
    }

}