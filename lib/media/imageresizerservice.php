<?php


namespace Wt\Core\Media;


use Wt\Core\Entity\Main\FileEntity;

class ImageResizerService
{
    protected array $map;

    public function __construct(array $map = [100, 200, 400, 800, 1600])
    {
        $this->setMap($map);
    }

    public function setMap($map)
    {
        $this->map = array_values($map);
    }

    public function getUpSize($size)
    {
        $value = 0;
        foreach ($this->getMap() as $value){
            if($size <= $value) {
                return $value;
            }
        }
        return $value;
    }

    public function getDownSize($size)
    {
        $value = 0;
        foreach (array_reverse($this->getMap()) as $value){
            if($size >= $value) {
                return $value;
            }
        }
        return $value;
    }

    public function getUpLevel($size)
    {
        $level = 0;
        foreach ($this->getMap() as $value){
            $level++;
            if($size <= $value) {
                return $level;
            }
        }
        return $level;
    }

    public function getDownLevel($size)
    {
        $level = count($this->getMap());
        foreach (array_reverse($this->getMap()) as $value){
            if($size >= $value) {
                return $level;
            }
            $level--;
        }
        return $level;
    }

    /**
     * @return array|int[]
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @param FileEntity $fileEntity
     * @param int $lvl
     * @param bool $filter
     * @return array
     */
    public function getResizeImageData(FileEntity $fileEntity, $lvl = 0, $filter = false)
    {
        $result = [
            'src' => $fileEntity->getSrc(),
            'width' => $fileEntity->getRaw('WIDTH'),
            'height' => $fileEntity->getRaw('HEIGHT'),
            'size' => $fileEntity->getSize(),
            'parent_id' => $fileEntity->getId(),
            'parent_src' => $fileEntity->getSrc(),
            'errors' => [],
        ];

        $lvl = min($lvl, count($this->getMap()));

        if(!$lvl || !isset($this->map[$lvl-1])){
            return $result;
        }
        $ext = $fileEntity->getExt();
        if($ext === 'svg') {
            return $result;
        }
        if(is_array($lvl)){
            $arSize = $lvl;
        } else {
            $arSize = [
                'width' => $this->map[$lvl-1],
                'height' => $this->map[$lvl-1],
            ];
        }
        $resResize = \CFile::ResizeImageGet(
            $fileEntity->getRaw(),
            $arSize,
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true,
            $filter
        );

        if($resResize){
            $result = $resResize;
            $result['parent_id'] = $fileEntity->getId();
            $result['parent_src'] = $fileEntity->getSrc();
        } else {
            $result['errors'][] = 'Resize error [1]';
        }

        return $result;
    }

    /**
     * @param int|FileEntity $file
     * @param int $lvl
     * @return array
     */
    protected function getResizeImageDataEx($file, $lvl = 0, $filter = false)
    {
        $fileEntity = $file;
        if((is_numeric($file)) && (int)$file > 0){
            $fileEntity = app()->factory()->main()->getFileFactory()->getEntityById($file);
        }
        elseif(is_array($file)){
            $fileEntity = app()->factory()->main()->getFileFactory()->cache([])->getEntityByResult($file);
        }

        return $this->getResizeImageData($fileEntity, $lvl);
    }

    /**
     * @param int|FileEntity $file
     * @param int|array $lvl
     * @param bool|array $filter
     * @return string
     */
    public function getSrc($file, $lvl = ResizeLevel::ORIGIN, $filter = false)
    {
        return $this->getResizeImageDataEx($file, $lvl, $filter)['src'];
    }
}