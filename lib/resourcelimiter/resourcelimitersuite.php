<?php


namespace Wt\Core\ResourceLimiter;


use Wt\Core\Interfaces\IResourceLimiter;

class ResourceLimiterSuite implements IResourceLimiter
{
    public $loops = 0;
    public $breakLoops = 0;


    /** @var IResourceLimiter[]  */
    protected $limiterList = [];
    public function addLimiterCase(IResourceLimiter $resourceLimiter)
    {
        $this->limiterList[] = $resourceLimiter;
        return $this;
    }

    public function reset()
    {
        $this->loops = 0;
        $this->breakLoops = 0;
        foreach ($this->limiterList as $limiter) {
            $limiter->reset();
        }
        return $this;
    }

    public function setForce($force)
    {
        foreach ($this->limiterList as $limiter) {
            $limiter->setForce($force);
        }
        return $this;
    }

    public function isValid()
    {
        $result = true;
        foreach ($this->limiterList as $limiter) {
            $result = $result && $limiter->isValid();
        }
        return $result;
    }

    public function next()
    {
        $this->loops++;

        $result = true;
        foreach ($this->limiterList as $limiter) {
            $result = $result && $limiter->next();
        }

        $valid = $this->isValid();
        if(!$valid && !$this->breakLoops) {
            $this->breakLoops = $this->loops;
        }

        return $result;
    }

    public function doWhileTrue($callback, $args = [])
    {
        if(!is_callable($callback)){
            throw new \Exception('callback is not a function');
        }
        $this->reset();

        $result = false;

        while ($this->next()){
            if(!(bool)call_user_func_array($callback, $args)){
                $result = true;
                break;
            }
        }

        return $result;
    }
}