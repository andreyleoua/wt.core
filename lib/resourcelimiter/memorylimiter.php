<?php


namespace Wt\Core\ResourceLimiter;


class MemoryLimiter extends AResourceLimiter
{

    /** memory vars */
    public $phpMemoryLimit = 0;
    public $availableMemory = 0;
    public $maxMemory = 0; // set user
    public $availableMaxMemory = 0;
    public $currentMemory = 0;
    public $currentPercentagePoint = 0;

    protected $ms = 4 * 1024 * 1024;


    function __construct($maxMemory)
    {
        $this->maxMemory = $maxMemory;
    }

    protected function initParams()
    {
        // TODO: Implement initParams() method.
    }

    protected function getCurrentValue()
    {
        return memory_get_usage(true) * 2;
    }


    public function isValid()
    {
        $this->phpMemoryLimit = $this->getPhpMemoryLimit();
        $this->availableMemory = floor($this->phpMemoryLimit / $this->ms) * $this->ms;
        $this->availableMaxMemory = floor($this->maxMemory / $this->ms) * $this->ms;
        $this->currentMemory = $this->getCurrentValue();
        $realMaxValue = min($this->availableMaxMemory, $this->availableMemory);
        $this->currentPercentagePoint = (($this->currentMemory) / $realMaxValue) * 100;
        $valid = $this->currentMemory < $realMaxValue;
        return $valid;
    }

    protected function makeStatistics()
    {
        parent::makeStatistics();
        $this->ms = max($this->ms, $this->__maxDifferenceValue);
    }

    function getPhpMemoryLimit($mem = null)
    {
        if(is_null($mem)){
            $mem = ini_get('memory_limit');
        }
        if (preg_match('/^(\d+)(.)$/', $mem, $matches)) {
            if ($matches[2] == 'G') {
                $mem = $matches[1] * 1024 * 1024 * 1024;
            } else if ($matches[2] == 'M') {
                $mem = $matches[1] * 1024 * 1024;
            } else if ($matches[2] == 'K') {
                $mem = $matches[1] * 1024;
            }
        }
        return (int)$mem;
    }
}