<?php


namespace Wt\Core\ResourceLimiter;


class PercentMemoryLimiter extends MemoryLimiter
{

    /** memory vars */
    public $criticalPercentagePoint = 0; // calc
    public $maxPercentagePoint = 0; // set user




    function __construct($maxPercentagePoint = 90)
    {
        $this->maxPercentagePoint = $maxPercentagePoint;
    }


    public function isValid()
    {
        $this->phpMemoryLimit = $this->getPhpMemoryLimit();
        $this->availableMemory = floor($this->phpMemoryLimit / $this->ms) * $this->ms;
        $this->criticalPercentagePoint = 100 - $this->ms / max($this->availableMemory, $this->ms) * 100;
        $this->maxMemory = floor($this->maxPercentagePoint * $this->availableMemory / 100);
        $this->currentMemory = $this->getCurrentValue();
        $this->currentPercentagePoint = (($this->currentMemory) / $this->availableMemory) * 100;
        $valid = $this->currentMemory < min($this->maxMemory, $this->availableMemory);
        return $valid;
    }
}