<?php


namespace Wt\Core\ResourceLimiter;


class CountLimiter extends AResourceLimiter
{
    public $count = 0;
    public $beginValue = 0;
    public $currentPercentagePoint = 0;

    function __construct($count)
    {
        $this->count = $count;
    }

    /** Выполняется в начале итерации */
    protected function initParams(){}

    protected function getCurrentValue()
    {
        return $this->loops;
    }

    /** Проверка на валидность текущего состояния. Проверка на доступность ресурса */
    public function isValid()
    {
        $this->currentPercentagePoint = 100;
        if($this->count > 0) {
            $this->currentPercentagePoint = $this->getCurrentValue() / $this->count * 100;
        }
        return $this->count >= $this->getCurrentValue();
    }
}