<?php


namespace Wt\Core\ResourceLimiter;


use Exception;
use Wt\Core\Interfaces\IResourceLimiter;

abstract class AResourceLimiter implements IResourceLimiter
{
    protected $force = false;
    public $loops = 0;
    public $breakLoops = 0;

    /** statistics */
    public $__step = 0;
    public $__previousValue = 0;
    public $__currentValue = 0;
    public $__differenceValue = 0;
    public $__maxValue = 0;
    public $__maxDifferenceValue = 0;
    public $__averageDifferenceValue = 0;

    /**
     * Игнор играничений
     * @param $force
     * @return $this
     */
    public function setForce($force)
    {
        $this->force = (bool)$force;
        return $this;
    }

    function reset()
    {
        $this->loops = 0;
        $this->breakLoops = 0;
        $this->__step = 0;
        $this->__previousValue = 0;
        $this->__currentValue = 0;
        $this->__differenceValue = 0;
        $this->__maxValue = 0;
        $this->__maxDifferenceValue = 0;
        $this->__averageDifferenceValue = 0;
        return $this;
    }

    /** Выполняется в начале итераций / до начала первого шага */
    abstract protected function initParams();
    /** Получение текущего контролируемого значения или значения на основании которого будет определяться валидность */
    abstract protected function getCurrentValue();
    /** Проверка на валидность текущего состояния. Проверка на доступность ресурса */
    abstract public function isValid();

    public function next()
    {
        if(!$this->loops) {
            $this->initParams();
        }
        $this->loops++;

        $valid = $this->isValid();
        if(!$valid && !$this->breakLoops) {
            $this->breakLoops = $this->loops;
        }

        $this->makeStatistics();

        return $valid || $this->force;
    }

    /**
     * @param callable $callback
     * @param array $args
     * @return bool true - закончено выполнение цикла, false - цикл прерван ограничением
     * @throws Exception
     */
    public function doWhileTrue($callback, $args = [])
    {
        if(!is_callable($callback)){
            throw new Exception('callback is not a function');
        }
        $this->reset();

        $result = false;

        while ($this->next()){
            if(!(bool)call_user_func_array($callback, $args)){
                $result = true;
                break;
            }
        }

        return $result;
    }

    protected function canNext()
    {
        $valid = $this->isValid();

        return $valid || $this->force;
    }

    /** Выполнение перед Основным исполняемым/итерируемым кодом */
    protected function makeStatistics()
    {
        $value = $this->getCurrentValue();

        $this->__step = $this->loops;
        $this->__previousValue = $this->__currentValue?:$value;
        $this->__currentValue = $value;
        $this->__differenceValue = $this->__currentValue - $this->__previousValue;
        $this->__maxValue = max($this->__currentValue, $this->__maxValue);
        $this->__maxDifferenceValue = max($this->__differenceValue, $this->__maxDifferenceValue);
        if($this->loops >= 2) {
            $this->__averageDifferenceValue = ($this->__averageDifferenceValue * ($this->loops - 2) + $this->__differenceValue) / ($this->loops-1);
        }
    }
}