<?php


namespace Wt\Core\ResourceLimiter;


class TimeLimiter extends AResourceLimiter
{
    public $time;
    public $beginValue;
    public $currentPercentagePoint;

    function __construct($seconds)
    {
        $this->time = $seconds;
    }

    /** Выполняется в начале итерации */
    protected function initParams()
    {
        $this->beginValue = $this->getCurrentValue();
    }

    protected function getCurrentValue()
    {
        $arHRTime = hrtime();
        return $arHRTime[0] + $arHRTime[1] / 1E9;
    }

    /** Проверка на валидность текущего состояния. Проверка на доступность ресурса */
    public function isValid()
    {
        $curVal = $this->getCurrentValue();
        $this->currentPercentagePoint = 100;
        if($this->time > 0) {
            $this->currentPercentagePoint = ($curVal - $this->beginValue) / $this->time * 100;
        }
        return $this->beginValue + $this->time - ($this->__maxDifferenceValue + $this->__averageDifferenceValue) / 2 >= $curVal;
    }
}