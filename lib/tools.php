<?php

namespace Wt\Core;

class Tools
{

    public static function normalizeUrl($url)
    {
        $url = str_replace(['\\'], ['/'], strval($url));
        $url = preg_replace('/(?<=[^:])(\/{2,})/', '/', $url);
        return $url;
    }

    public static function normalizePath($path)
    {
        $path = trim($path);
        $path = str_replace(['\\'],['/'], $path);
        $path = preg_replace('/\/{2,}/', '/',$path);
        $path = str_replace(['/./'],['/'], $path);
        $path = rtrim($path, '/');
        if(!strlen($path)) {
            $path = '/';
        }
        return $path;
    }

    /**
     * Функция для получения правильных окончаний слов на php
     *
     * @param int    $number      Число, к которому привязываемся
     * @param array  $titles      Массив слов для склонения пр.: ["Остался %d час", "Осталось %d часа", "Осталось %d часов"]
     * @return string
     **/
    public static function pluralize($number, array $titles)
    {
        $cases = [2, 0, 1, 1, 1, 2];
        return sprintf($titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ], $number);
    }

    public static function isTrue($value)
    {
        return strtolower($value) === 'y' || $value === '1' || $value === 1 || $value === true;
    }

    public static function isSuccess($value)
    {
        $_value = strtolower($value);
        return self::isTrue($value) ||
            $_value === 'yes' ||
            $_value === 'ok' ||
            $_value === 'success' ||
            $_value === 'valid';
    }

    //Bitrix\Main\HttpRequest::isAjaxRequest()
    public static function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') || $_SERVER['HTTP_BX_AJAX'] !== null;
    }

    public static function coalesce()
    {
        $args = func_get_args();

        while (count($args) && !($arg = array_shift($args)));

        return isset($arg) ? $arg : null;
    }

    public static function coalesceArray(array $args, $default = null)
    {
        while (count($args) && !($arg = array_shift($args)));

        return isset($arg) ? $arg : $default;
    }

    /**
     * Для корректной работы на разных ОС
     */
    public static function getDocumentRoot()
    {
        static $docRoot;

        if(is_null($docRoot)) {
            $docRoot = self::normalizePath($_SERVER['DOCUMENT_ROOT']);
        }

        return $docRoot;
    }

    public static function removeDocRoot($path)
    {
        $path = self::normalizePath($path);

        $len = strlen(self::getDocumentRoot());

        if (substr($path, 0, $len) == self::getDocumentRoot()){
            return '/' .ltrim(substr($path, $len), '/');
        }

        return $path;
    }

    public static function jsonSerialize($data)
    {
        if($data instanceof \JsonSerializable) {
            return $data->jsonSerialize();
        }
        if(is_callable([$data, 'toArray'])){
            $data = call_user_func([$data, 'toArray']);
        }
        if (is_iterable($data))
        {
            $rData = [];
            foreach ($data as $key => $value)
            {
                $rData[$key] = self::jsonSerialize($value);
            }
            $data = $rData;
        }
        if(is_callable([$data, '__toString'])) {
            return call_user_func([$data, '__toString']);
        }
        return $data;
    }

    public static function json($data)
    {
        return json_encode(self::jsonSerialize($data));
    }

    public static function getAbsoluteUri($rel = '', $host = null)
    {
        return static::getProtocol().'://'.static::getHttpHost($host) . $rel;
    }

    // \Bitrix\Main\Composite\Helper::getHttpHost();
    public static function getHttpHost($host = null)
    {
        if(is_null($host)){
            $host = $_SERVER['HTTP_HOST'];
            if (class_exists('\Bitrix\Main\Context') ) {
                $host = \Bitrix\Main\Context::getCurrent()->getServer()->getHttpHost();
            }
        }
        $host = $host??(defined('SITE_SERVER_NAME')?SITE_SERVER_NAME:null);

        return preg_replace('#^(.*?)($|:\d+$)#','$1', $host);
    }

    public static function getProtocol()
    {
        if($_SERVER['SERVER_PORT'] == 443)
        {
            return 'https';
        }

        $isHttps = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on');
        if (class_exists('\Bitrix\Main\Context')) {
            $request = \Bitrix\Main\Context::getCurrent()->getRequest();
            $isHttps = $request->isHttps();
        }
        return ($isHttps ? 'https' : 'http');
    }

    public static function getDataFromUrl($url, $params = [], $method = 'get', $headersOrCallback = [])
    {
        /** @var \Wt\Core\Exchange\EasyCurl $curl */
        $curl = resolve(\Wt\Core\Exchange\EasyCurl::class, [$url]);

        $curl->setMethod($method);
        $curl->setParams($params);

        if( $headersOrCallback && is_array($headersOrCallback) ){
            $curl->setHeaders($headersOrCallback);
        }

        if( is_callable($headersOrCallback) ){
            $headersOrCallback($curl->getCurlDescriptor(), $url, $params, $method);
        }

        return $curl->get();
    }

    public static function getJsonFromUrl($url, $params = [], $method = 'get', $headersOrCallback = [])
    {
        /** @var \Wt\Core\Exchange\EasyCurl $curl */
        $curl = resolve(\Wt\Core\Exchange\EasyCurl::class, [$url]);

        $curl->setMethod($method);
        $curl->setParams($params);
        $curl->setRequestType('json');

        if( $headersOrCallback && is_array($headersOrCallback) ){
            $curl->setHeaders($headersOrCallback);
        }

        if( is_callable($headersOrCallback) ){
            $headersOrCallback($curl->getCurlDescriptor(), $url, $params, $method);
        }

        return $curl->get();
    }


    public static function compress($string)
    {
        $supportGzip = (strpos( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) !== false)
            && extension_loaded('zlib');

        if ( !$supportGzip ) {
            return false;
        }

        $gzip = \gzencode($string, 9);

        return $gzip;
    }

    public static function sendJsonAnswer($result, $httpCode = null, $gzip = false)
    {
        global $APPLICATION;

        if(!is_null($httpCode)) {
            \CHTTP::SetStatus($httpCode);
        }

        $APPLICATION->RestartBuffer();
        while(ob_end_clean());

        $data = static::json($result);

        if($gzip && (($compressData = self::compress($data)) !== false)) {
            header('Content-Encoding: gzip');
            echo $compressData;
        } else {
            header('Content-type:application/json;charset=utf-8');
            echo $data;
        }
        if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
        {
            /**
             * Может дать ленги в конце
             */
        }
        /**
         * Может дать нежелательный эффект (хвост непонятных символов после json)
         */
        //\CMain::FinalActions();
        die();
    }

    public static function sendTextAnswer($result, $httpCode = null)
    {
        global $APPLICATION;

        if(!is_null($httpCode)) {
            \CHTTP::SetStatus($httpCode);
        }

        $APPLICATION->RestartBuffer();
        while(ob_end_clean());

        header('Content-Type: text/html; charset=UTF-8');

        echo print_r($result, true);

        /**
         * Может дать нежелательный эффект (хвост непонятных символов после json)
         */
        //\CMain::FinalActions();
        die();
    }

    /**
     * https://api.ipify.org/ 46.56.235.54
     *
     * https://api.myip.com/ {"ip":"46.56.235.54","country":"Belarus","cc":"BY"}
     *
     * http://ip-api.com/json/46.56.235.54 | http://ip-api.com/json/
     * {"status":"success","country":"Belarus","countryCode":"BY","region":"HM","regionName":"Minsk City",
     * "city":"Minsk","zip":"220094","lat":53.9007,"lon":27.5709,"timezone":"Europe/Minsk","isp":"VEL-MOBILE-SITE1",
     * "org":"","as":"AS42772 Unitary enterprise A1","query":"46.56.235.54"}
     *
     * @return string
     */
    public static function getServerIp()
    {
        static $r;
        if(!is_null($r)){
            return $r;
        }
        $r = (string)file_get_contents('https://api.ipify.org/');
        return $r;
    }

    /**
     * @see \Bitrix\Main\Service\GeoIp\Manager::getRealIp()
     * @return string|null
     */
    public static function getIp()
    {
        $keys = [
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR'
        ];
        foreach ($keys as $key) {
            $value = (string)$_SERVER[$key];
            if (!empty($value)) {
                $valueArray = explode(',', $value);
                if(!$valueArray){
                    $valueArray = [];
                }
                $ip = trim(end($valueArray));
                if (filter_var($ip, FILTER_VALIDATE_IP)) {
                    return $ip;
                }
            }
        }
        return null;
    }

    /**
     * @see \Bitrix\Main\Service\GeoIp\Manager::getRealIp()
     * @return string|null
     */
    public static function getClientIp()
    {
        return static::getIp();
    }

    public static function toArray($data)
    {
        if(is_object($data) && is_callable([$data, 'toArray'])){
            return call_user_func_array([$data, 'toArray'], []);
        }
        if(!is_array($data) && empty($data)){
            return [];
        }

        return (array)$data;
    }

    public static function isCli()
    {
        if(defined('STDIN') ){
            return true;
        }
        if(function_exists('php_sapi_name') && (Type\Str::getPosition($sapi = php_sapi_name(), 'cli') === 0))
        {
            return true;
        }
        if (defined('PHP_SAPI') && PHP_SAPI == 'cli'){
            return true;
        }
        if( defined('PHP_SAPI') && stristr(PHP_SAPI , 'cgi') and getenv('TERM') ){
            return true;
        }
        if (defined('BX_CRONTAB') && BX_CRONTAB === true)
        {
            return true;
        }
        if (defined('CHK_EVENT') && CHK_EVENT === true) // bitrix hack way
        {
            return true;
        }

        return false;
    }

    public static function isAgentUseCron()
    {
        return (
            \Bitrix\Main\Config\Option::get('main', 'agents_use_crontab', 'N') === 'Y' // agents use crontab
            || \Bitrix\Main\Config\Option::get('main', 'check_agents', 'Y') !== 'Y' // auto call agents disabled
            || (defined('BX_CRONTAB_SUPPORT') && BX_CRONTAB_SUPPORT === true)
        );
    }

    public static function getPrintSize(int $bytes)
    {
        if ($bytes < 0) {$m = '-';} else {$m = '';}
        $bytes = abs($bytes);
        if ($bytes<1024) return $m.$bytes.'B';
        else if ($bytes<1048576) return $m.round ($bytes/1024,2).'KiB';
        else if ($bytes<1073741824) return $m.round ($bytes/1048576,2).'MiB';
        else return $m.round($bytes/1073741824,2).'GiB';
    }

    public static function getPrintSize2(int $bytes)
    {
        if ($bytes < 0) {$m = '-';} else {$m = '';}
        $precision = 0;
        $bytes = abs($bytes);
        if ($bytes<10000) return $m.$bytes.'B';
        else if ($bytes<10240000-512) return $m.round($bytes/1024,$precision).'KiB';//2^10 * 1000 -2^9
        else if ($bytes<10485760000-524288) return $m.round($bytes/1048576,$precision).'MiB';//2^20 * 1000-2^19
        else if ($bytes<10737418240000-536870912) return $m.round($bytes/1073741824,$precision).'GiB';//2^30 * 1000 - 2^29
        else return $m.round($bytes/1099511627776,$precision).'TiB';
    }

    public static function getNiceMemorySize($bytes, $binaryPrefix=true) {
        if ($binaryPrefix) {
            $unit=array('B','KiB','MiB','GiB','TiB','PiB');
            if ($bytes==0) return '0 ' . $unit[0];
            return @round($bytes/pow(1024,($i=floor(log($bytes,1024)))),2) .' '. (isset($unit[$i]) ? $unit[$i] : 'B');
        } else {
            $unit=array('B','KB','MB','GB','TB','PB');
            if ($bytes==0) return '0 ' . $unit[0];
            return @round($bytes/pow(1000,($i=floor(log($bytes,1000)))),2) .' '. (isset($unit[$i]) ? $unit[$i] : 'B');
        }
    }

    public static function getPrintSeconds($seconds)
    {
        $res = '';

        $days = floor($seconds / 86400);
        $seconds = $seconds % 86400;
        if($days){
            $res .= $days . 'd ';
        }

        $hours = floor($seconds / 3600);
        $seconds = $seconds % 3600;
        $res .= str_pad($hours, 2, '0', STR_PAD_LEFT) . ':';

        $minutes = floor($seconds / 60);
        $seconds = $seconds % 60;
        $res .= str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':';

        $res .= str_pad($seconds, 2, '0', STR_PAD_LEFT);

        return $res;
    }


    /**
     * Prevent Mysql error 'MySQL server has gone away'
     *
     * @param null|int $intTimeout
     * @return \Bitrix\Main\DB\Result
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public static function setSqlWaitTimeout($intTimeout=null)
    {
        $intTimeout = is_numeric($intTimeout) && $intTimeout > 0 ? $intTimeout : 6*3600;
        $strSql = "SET SESSION `wait_timeout`={$intTimeout};";
        return \Bitrix\Main\Application::getConnection()->query($strSql);
    }

    /**
     * Prevent Mysql error 'MySQL server has gone away'
     *
     * @return \Bitrix\Main\DB\Result
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public static function preventSqlGoneAway()
    {
        $strSql = 'SELECT 1;';
        return \Bitrix\Main\Application::getConnection()->query($strSql);
    }

    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /** delete */
    public static function getCliPhpData()
    {
        $data = [];
        global $argv;
        parse_str(implode('&', array_slice($argv, 1)), $data);

        return $data;
    }

    /**
     * @return float
     */
    public static function getMicroTime()
    {
        list($uSec, $sec) = explode(' ', microtime());
        return ((float)$uSec + (float)$sec);
    }

    /**
     * @param $str
     * @return null|int
     * @throws null
     */
    public static function getMemoryBytes($str)
    {
        $str = trim($str);
        $re = '/([\d.,]+)\s*(?:([KMGTPE])i*B*)*/mi';
        $result = 0;
        $rPreg = preg_match($re, $str, $m);

        if(!$rPreg){
            return null;
        }
        $result = (float)str_replace(',', '.', $m[1]);

        $d = strtoupper($m[2]);
        $map = [
            'K' => 1024, // 3
            'M' => 1048576, // 6
            'G' => 1073741824, // 9
            'T' => 1099511627776, // 12
            'P' => 1125899906842624, // 15
            'E' => 1152921504606846976, // 18
        ];
        if($d && $map[$d]){
            $result = $result * $map[$d];
        }

        return (int)$result;
    }
}
