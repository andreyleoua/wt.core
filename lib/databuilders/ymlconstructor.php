<?php


namespace Wt\Core\DataBuilders;


use Wt\Core\Templater\Tools;

class YmlConstructor
{
	protected $type;
	protected $value;
	protected $root = false;
	protected $attributes = [];

    protected $header = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
	protected $extHeader = ''; // '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';

	private $items = [];
	private $depthLevel = 0;

    public function __construct($type, $value = null)
	{
		$this->type = $type;
		if( $value !== null && $value !== "" ){
			$this->setValue($value);
		}
	}

	public function isRoot($root = null)
	{
		if( $root !== null )
			$this->root = (bool)$root;

		return $this->root;
	}

	public function setHeader($str)
    {
        $this->header = $str;
    }

	public function setExtHeader($str)
    {
        $this->extHeader = (string)$str;
    }

	public function setValue($value)
	{
		$this->value = $value;
	}

	public function add(YmlConstructor $item)
	{
		$this->items[] = $item;
	}

	public function remove($id)
	{
		unset($this->items[$id]);
	}

	public function addAttribute($name, $value)
	{
		$this->attributes[$name] = $value;
	}

	public function clearAttribute($name = null)
	{
		if( $name !== null ){
            $this->attributes = [];
		    return;
        }

        unset($this->attributes[(string)$name]);
	}

    /**
     * @return YmlConstructor[]
     */
	public function getChilds()
	{
		return $this->items;
	}

	public function getHeaderString()
    {
        $str = $this->header;
        if( $this->extHeader ){
            $str .= $this->extHeader;
        }

        return $str;
    }

	public function __toString()
	{
        $str = "";

        if( $this->getChilds() ){
            $str .= sprintf($this->getTabs()."<%s%s>".PHP_EOL, $this->type, $this->attrToStr());
            foreach ($this->getChilds() as $child) {
                if($child instanceof YmlConstructor){
                    $child->depthLevel = $this->depthLevel+1;
                }
                $str .= $child->__toString();
            }

            $str .= sprintf($this->getTabs()."</%s>".PHP_EOL, $this->type);
        } else {
            if( $this->value !== "" && $this->attributes ){
                $str .= sprintf($this->getTabs()."<%s%s/>".PHP_EOL, $this->type, $this->attrToStr());
            } elseif( $this->attributes ) {
                $str .= sprintf($this->getTabs()."<%s%s>%s</%s>".PHP_EOL, $this->type, $this->attrToStr(), $this->value, $this->type);
            } else {
                $str .= sprintf($this->getTabs()."<%s>%s</%s>".PHP_EOL, $this->type, $this->value, $this->type);
            }
        }

		return $str;
	}

	protected function attrToStr()
	{
		return Tools::getAttrByArray($this->attributes);
	}

	protected function getTabs()
	{
	    return str_repeat("\t", $this->depthLevel);
	}

    public function print($return = false)
    {
        if($return){
            return $this->__toString();
        }

        echo $this->__toString();
        return null;
    }


    public function exportToFile($file)
    {
        $str = '';

        if( $this->isRoot() ){
            $str = $this->getHeaderString();
        }

        $str .= $this->__toString();

        return \file_put_contents($file, $str);
    }

    /**
     * @todo
     */
    public function send()
    {

    }
}