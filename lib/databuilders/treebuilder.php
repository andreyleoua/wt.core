<?php

namespace Wt\Core\DataBuilders;

class TreeBuilder
{
    // баг рекурсии когда один элемент ссылается на другой - надо как-то это лечить
    public function buildFromArray($arItems, $parentKey, $key = 'id')
    {
       $rootNode = new Node([$key => 0], $key);
       $arPrimaryItems = [];
       foreach ($arItems as $arItem){
           $arPrimaryItems[$arItem[$key]] = $arItem;
       }

       foreach ($arItems as $item){
           if( !$item[$parentKey] ){
               $rootNode->add(new Node($item, $key));
           } else {
               $n = $rootNode->findInChild($item[$parentKey]);
               if( $n && !$n->hasChildren($item[$key]) ){
                   $n->add(new Node($item, $key));
               } else {
                   $trig = false;
                   $n = new Node($item, $key);
                   while(!$trig && ++$i < 100){
                       $pKv = $n->getKey($parentKey);
                       if( $nss = $rootNode->findInChild($pKv) ){
                           $nss->add($n);
                           $pn = false;
                           $trig = true;
                       } else {
                           $pn = $this->getParentNode($arPrimaryItems, $n->getKey($parentKey));
                           if( !$pn ){
                               $rootNode->add($n);
                               $trig = true;
                           }
                       }
                       if( $pn ){
                           $pn->add($n);
                           if( $pn->getKey($parentKey) && ($tn = $rootNode->findInChild($pn->getKey($parentKey))) ){
                               $tn->add($pn);
                               $trig = true;
                           } elseif( $pn->getKey($parentKey) ) {
                               $n = $pn;
                           } else {
                               $rootNode->add($pn);
                               $trig = true;
                           }
                       } else {
                           $trig = true;
                       }
                   }
               }
           }
       }

       return $rootNode;
    }

    public function getParentNode($arItems, $key)
    {
        if( isset($arItems[$key]) ){
            return new Node($arItems[$key], 'id');
        }

        return false;
    }
}
