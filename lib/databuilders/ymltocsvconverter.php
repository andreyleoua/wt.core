<?php
/**
 * Created by PhpStorm.
 * Company: ittown.by
 * Project: module.webteam.by
 * User: Andrey Leo
 * Date: 6/12/21
 * Time: 1:26 AM
 * All rights reserved
 */

namespace Wt\Core\DataBuilders;


class YmlToCsvConverter
{
    /**
     * @var YmlConstructor
     */
    protected $yml;
    protected $delimiter = ';';
    protected $headers = [];
    protected $headersName = [];

    public function __construct(YmlConstructor $yumBuilder)
    {
        $this->yml = $yumBuilder;
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    public function initHeaders()
    {
        $this->headers = [];
        $item = $this->yml->getChilds()[0];
        $childs = $item->getChilds();
        foreach ($childs as $child) {
            $this->headers[$child->getType()] = $child->getType();
        }
    }

    public function getHeaders()
    {
        if(!$this->headers){
            $this->initHeaders();
        }

        return $this->headers;
    }

    public function setHeadersName(array $arHeaders = [])
    {
        $headers = $this->getHeaders();
        foreach ($headers as $headerKey => $headerName){
            if( isset($arHeaders[$headerKey]) && $arHeaders[$headerKey] ){
                $headers[$headerKey] = $arHeaders[$headerKey];
            }
        }
        $this->headers = $headers;
    }

    public function convert()
    {
        $arHeaders = $this->getHeaders();
        $strDoc = '';
        $strDoc .= implode($this->delimiter, $arHeaders).PHP_EOL;
        foreach ($this->yml->getChilds() as $item) {
            $arLine = [];
            foreach ($arHeaders as $type => $headerName) {
                $child = $item->getChildByType($type);
                $arLine[$type] = '';
                if( $child ){
                    $arLine[$type] = $this->safeString($child->getValue());
                }
            }
            $strDoc .= implode($this->delimiter, $arLine).PHP_EOL;
        }

        return $strDoc;
    }

    public function safeString($string, $safeChar = " ")
    {
        return '"'.str_replace([$this->delimiter, '"'], $safeChar, $string).'"';
    }
}