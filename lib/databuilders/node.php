<?php

namespace Wt\Core\DataBuilders;

class Node
{
    protected $data = [];
    protected $primaryKey = 'id';

    protected $children = [];
    protected $parent;
    protected $depthLevel = 0;

    public function __construct(array $arData, $primaryKey)
    {
        $this->data = $arData;
        $this->primaryKey = $primaryKey;
    }

    public function getKey($key)
    {
        if( is_array($this->data) ){
            return $this->data[$key];
        }

        if( is_object($this->data) ){
            return $this->data->$key;
        }

        return null;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function getPrimary()
    {
        return $this->data[$this->getPrimaryKey()];
    }

    public function getRaw()
    {
        return $this->data;
    }

    public function add(Node $node)
    {
        $k = $node->getKey($this->getPrimaryKey());
        if( !$this->hasChildren($k) ){
            $this->children[$k] = $node;
        }
        $node->setParent($this);
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getParents()
    {
        $node = $this;

        $result = [];

        while( !$node->isRoot() ){
            $node = $node->getParent();
            $result[] = $node;
        }

        return $result;
    }

    public function getRoot()
    {
        if( !$this->isRoot() ){
            return $this->getParent()->getRoot();
        }
    }

    public function getChildrens()
    {
        return $this->children;
    }

    public function hasChildren($key = false)
    {
        if( $key === false ){
            return (bool)$this->children;
        }

        return isset($this->children[$key]);
    }

    public function getLevel()
    {
        return (int)$this->depthLevel;
    }

    public function isRoot()
    {
        return $this->getLevel() == 0;
    }

    public function findInChild($primaryValue)
    {
        if( $this->hasChildren() && isset($this->children[$primaryValue]) ){
            return $this->children[$primaryValue];
        }

        if( $this->hasChildren() ){
            foreach ($this->getChildrens() as $childNode){
                $val = $childNode->findInChild($primaryValue);
                if( $val ){
                    return $val;
                }
            }
        }

        return false;
    }

    protected function setParent(Node $node)
    {
        $oldDepth = $this->getLevel();
        $this->parent = $node;
        $this->depthLevel = $node->getLevel()+1;
        if( $this->getLevel() != $oldDepth ){
            $this->recalcChildDepthLevel();
        }
    }

    protected function setChildrens(array $arNodes)
    {
        $this->children = $arNodes;
        $this->recalcChildDepthLevel();
    }

    protected function recalcChildDepthLevel()
    {
        if( $this->hasChildren() ){
            $level = $this->getLevel();
            foreach ($this->getChildrens() as $children){
                $children->depthLevel = $level+1;
                $children->recalcChildDepthLevel();
            }
        }
    }

    public function map($callableFunctionAction)
    {
        if( is_callable($callableFunctionAction) ){
            foreach ($this->getChildrens() as $item){
                $newData = call_user_func_array($callableFunctionAction, [$item->getRaw(), $item]);
                if( $newData && is_array($newData) ){
                    $item->data = $newData;
                }
                if( $item->hasChildren() ){
                    $item->map($callableFunctionAction);
                }
            }
        }

        return $this;
    }

    public function render($filePathName = '')
    {
        if( $filePathName ){
            $GLOBALS['APPLICATION']->IncludeFile($filePathName, ['arResult' => $this, 'tpl' => $filePathName]);
            return;
        }

        echo "<div class='items'>";
        echo "<span>".$this->getKey('id').": ".$this->getKey('name')."</span>";
        if( $this->hasChildren() ){
            foreach ($this->getChildrens() as $child){
                $child->render();
            }
        }
        echo "</div>";
    }

    public function toArray()
    {
        $result = $this->getRaw();

        if( $this->hasChildren() ){
            foreach ($this->getChildrens() as $child){
                $result['childrens'][] = $child->toArray();
            }
        }

        return $result;
    }

    public function toLineArray()
    {
        $result[] = $this->getRaw();

        if( $this->hasChildren() ){
            foreach ($this->getChildrens() as $child){
                $childRes = $child->toLineArray();
                foreach ($childRes as $childItem){
                    $result[] = $childItem;
                }
            }
        }

        return $result;
    }
}