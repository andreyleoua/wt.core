<?php


namespace Wt\Core\PropertyAdapter;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Form\Interfaces\IOptionsAttribute;
use Wt\Core\Interfaces\IPropertyValueEntity;

class DefaultPropertyAdapter extends APropertyAdapter
{
    /** @return AFactory|null */
    public function getValueFactory()
    {
        return null;
    }

    /**
     * @param mixed $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return mixed
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }

    /**
     * @return array
     */
    public function getFormFieldData()
    {
        $propertyEntity = $this->getPropertyEntity();
        $fields = [];
        $fields['multiple'] = $propertyEntity->isMultiple();
        $fields['required'] = $propertyEntity->isRequired();
        $fields['data-property-full-type'] = $propertyEntity->__getDevType();

        $fields['label'] = $propertyEntity->getName();
        $fields['name'] = "PROPERTY[{$propertyEntity->getId()}]";
        $fields['value'] = $propertyEntity->getDefaultValue();
        $fields['id'] = "property_{$propertyEntity->getId()}]";

        if(is_a($this->getFormFieldClass(), IOptionsAttribute::class, true) && $this->getValueFactory()){
            $options = $this->getValueFactory()->getCollection([], [], [], 100)->map(function (AEntity $entity){
                return "[{$entity->getId()}] {$entity->getName()}";
            });
            $fields['options'] = $options->prepend('not select');
        }

        return $fields;
    }

    public function getValue(array $restoredValue)
    {
        return $restoredValue['VALUE'];
    }
}