<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Collection\HlBlock\ElementCollection;
use Wt\Core\Entity\HlBlock\ElementEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class HlBlockPropertyAdapter extends AUserFieldPropertyAdapter
{

    /**
     * @param IPropertyValueEntity $propertyValue
     * @return ElementEntity|ElementCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        return parent::getValueEntity($propertyValue);
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        $blockId = $this->getPropertyEntity()->getSettings()['HLBLOCK_ID'];

        if(!$blockId) {
            throw new \Exception("BlockId is empty in property settings {$this->getPropertyEntity()->getFullType()}");
            return null;
        }
        $af = app()->factory()->hlBlock()->getEntityById($blockId);
        return $af->getElementFactory();
    }

    /**
     * @param ElementEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return float
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->fields()->name;
    }
}