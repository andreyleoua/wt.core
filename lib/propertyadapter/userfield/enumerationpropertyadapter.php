<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Collection\UserFieldEnumCollection;
use Wt\Core\Entity\UserFieldEntity;
use Wt\Core\Entity\UserFieldEnumEntity;
use Wt\Core\Factory\UserFieldEnumFactory;
use Wt\Core\Factory\IBlock\PropertyEnumFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class EnumerationPropertyAdapter extends AUserFieldPropertyAdapter
{

    /**
     * @return PropertyEnumFactory
     */
    public function getValueFactory()
    {
        $blockId = $this->getPropertyEntity()->getFactory()->getEntityId();
        $propertyId = $this->getPropertyEntity()->getId();

        $block = app()->factory()->hlBlock()->getEntityById($blockId);
        return $block->getFactoryInstance(UserFieldEnumFactory::class, [$blockId, $propertyId]);
    }

    /**
     * @return UserFieldEntity
     */
    public function getPropertyEntity()
    {
        return parent::getPropertyEntity();
    }

    /**
     * @param IPropertyValueEntity $propertyValue
     * @return UserFieldEnumEntity|UserFieldEnumCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        return parent::getValueEntity($propertyValue);
    }

    /**
     * @param UserFieldEnumEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return float
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getName();
    }
}