<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Entity\Main\FileEntity;
use Wt\Core\Factory\Main\FileFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class FilePropertyAdapter extends AUserFieldPropertyAdapter
{

    /** @return FileFactory */
    public function getValueFactory()
    {
        return app()->factory()->main()->getFileFactory();
    }

    /**
     * @param FileEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string|int
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getSrc();
    }
}