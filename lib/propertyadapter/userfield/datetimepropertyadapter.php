<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class DateTimePropertyAdapter extends AUserFieldPropertyAdapter
{

    /** @return null */
    public function getValueFactory()
    {
        return null;
    }

    /**
     * @param int|string|array $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string|int
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }
}