<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Collection\IBlock\ElementCollection;
use Wt\Core\Collection\Shop\ProductCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\IBlock\ElementEntity;
use Wt\Core\Entity\Shop\ProductEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class IBlockElementPropertyAdapter extends AUserFieldPropertyAdapter
{
    /**
     * @param IPropertyValueEntity $propertyValue
     * @return ElementEntity|ProductEntity|ElementCollection|ProductCollection
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        return parent::getValueEntity($propertyValue);
    }

    /**
     * @param ElementEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        if(!($value instanceof AEntity)){
            /**
             * @todo throw
             */
            $iBlockId = \CIBlockElement::GetList(
                ['SORT' => 'ASC'],
                ['=ID' => $value],
                false,
                false,
                ['ID', 'IBLOCK_ID']
            )->Fetch()['IBLOCK_ID'];

            $value = $this->__getFactory($iBlockId)->getEntityById($value);
        }
        if($value instanceof AEntity){
            return $value->getName();
        }
        return $value;
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        $iBlockId = $this->getPropertyEntity()->getSettings()['IBLOCK_ID'];
        if(!$iBlockId) {
            //throw new \Exception("IBlockId is empty in property settings {$this->getPropertyEntity()->getFullType()}");
            return null;
        }
        return $this->__getFactory($iBlockId);
    }

    protected function __getFactory($iBlockId)
    {
        return app()->factory()->iBlock()->getEntityById($iBlockId)->getElementFactory();
    }
}