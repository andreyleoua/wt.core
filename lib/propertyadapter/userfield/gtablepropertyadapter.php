<?php


namespace Wt\Core\PropertyAdapter\UserField;


use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AUserFieldPropertyAdapter;

class GTablePropertyAdapter extends AUserFieldPropertyAdapter
{
    protected function __getDepsModules()
    {
        return ['grain.tables'];
    }

    /** @return null */
    public function getValueFactory()
    {
        return null;
    }
    /**
     * @param array $restoredValue
     * @return array
     */
    public function getValue(array $restoredValue)
    {
        $value = $restoredValue['VALUE'];
        if($this->getPropertyEntity()->isMultiple()){
            foreach ($value as $k => $v){
                if (is_string($v) && strlen($v) > 0) $v = unserialize($v);
                $value[$k] = $v ?: [];
            }
            return $value;
        }

        if (is_string($value) && strlen($value) > 0) $value = unserialize($value);
        return $value ?: [];
    }

    /**
     * @param int|string|array $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string|int
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }
}