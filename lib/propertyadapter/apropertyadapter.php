<?php


namespace Wt\Core\PropertyAdapter;


use Bitrix\Main\Loader;
use Wt\Core\Assets\Assets;
use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\APropertyEntity;
use Wt\Core\Form\Field\DefaultField;
use Wt\Core\Interfaces\IPropertyAdapter;
use Wt\Core\Interfaces\IPropertyEntity;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\Templater\Templater;

abstract class APropertyAdapter implements IPropertyAdapter
{
    /**
     * @var APropertyEntity
     */
    protected $propertyEntity;
    /**
     * @var Templater
     */
    protected $templater;
    /**
     * @var Assets
     */
    protected $assets;
    /** @var iterable */
    protected $params;
    protected $formFieldClass;
    protected $isBitrixModulesLoaded;

    /**
     * @param AEntity|string|array $value
     * @param IPropertyValueEntity $propertyValue
     * @return string|int
     */
    abstract protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null);

    abstract public function getFormFieldData();

    public function __construct(IPropertyEntity $propertyEntity, $params, Templater $templater, Assets $assets)
    {
        $this->propertyEntity = $propertyEntity;
        $this->params = $params;
        $this->templater = $templater;
        $this->assets = $assets;
        $this->formFieldClass = $this->params['form_field_class'];
        $this->__includeDeps();
    }

    /**
     * @return array
     */
    protected function __getDepsModules()
    {
        return [];
    }

    protected function __includeDeps()
    {
        if(!is_null($this->isBitrixModulesLoaded)){
            return $this->isBitrixModulesLoaded;
        }
        $this->isBitrixModulesLoaded = true;
        foreach ($this->__getDepsModules() as $module){
            if(!Loader::includeModule($module)){
                $this->isBitrixModulesLoaded = false;
                /**
                 * @todo throw
                 */
                break;
            }
        }
        return $this->isBitrixModulesLoaded;
    }

    public function getFormFieldClass()
    {
        return $this->formFieldClass;
    }

    /**
     * @return DefaultField
     * @throws null
     */
    public function getFormField()
    {
        if(!$this->getFormFieldClass()){
            throw new \Exception('Empty FormFieldClass in ' . static::class);
        }
        $field = app()->service()->form()->field()->make($this->getFormFieldClass(), $this->getFormFieldData());
        $field->setPropertyEntity($this->getPropertyEntity());
        return $field;
    }

    /**
     * @param IPropertyValueEntity $propertyValue
     * @return AEntity|AEntityCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        $valueFactory = $this->getValueFactory();
        if(!$valueFactory) {
            return null;
        }

        if ($this->getPropertyEntity()->isMultiple()) {
            return $valueFactory->getEntityByIds($propertyValue->getValue());
        }

        return $valueFactory->getEntityById($propertyValue->getValue());
    }

    /**
     * @return Templater
     */
    public function getTemplater()
    {
        return $this->templater;
    }

    public function templater($relTemplateName)
    {
        return $this->getTemplater()->get('elementproperty/' . $this->getPropertyEntity()->getFullType(true) . '/' . $relTemplateName);
    }

    public function render($relTemplateName)
    {
        echo $this->templater($relTemplateName);
    }

    /**
     * @return APropertyEntity
     */
    public function getPropertyEntity()
    {
        return $this->propertyEntity;
    }

    public function getDisplayValue(IPropertyValueEntity $propertyValue)
    {
        return $this->displayValueProcessing([$this, 'getDisplayValueBySingleValue'], $propertyValue);
    }

    /**
     * @param callable $callback
     * @param IPropertyValueEntity $propertyValue
     * @return mixed
     */
    protected function displayValueProcessing(callable $callback, IPropertyValueEntity $propertyValue)
    {
        /** Has valueEntity */
        if($valueEntity = $this->getValueEntity($propertyValue)) {
            if($this->getPropertyEntity()->isMultiple()){
                return $valueEntity->map(function($value, $key)use($callback, $propertyValue){
                    return call_user_func_array($callback, [$value, $propertyValue]);
                })->toArray();
            } else {
                return call_user_func_array($callback, [$valueEntity, $propertyValue]);
            }
        }
        /** Has not valueEntity */
        if($this->getPropertyEntity()->isMultiple()){
            return collect($propertyValue->getValue())->map(function($value, $key)use($callback, $propertyValue){
                return call_user_func_array($callback, [$value, $propertyValue]);
            })->toArray();
        } else {
            return call_user_func_array($callback, [$propertyValue->getValue(), $propertyValue]);
        }
    }

    /**
     * @param AEntity|AEntityCollection $valueEntity
     */
    public function getStoredValue($valueEntity)
    {
        if($this->getValueFactory()) {
            if(!$valueEntity){
                return null;
            }
            if ($this->getPropertyEntity()->isMultiple()) {
                /** @var AEntityCollection $valueEntity */
                return $valueEntity->map(function (AEntity $entity) {
                    return $entity->getId();
                });
            }

            /** @var AEntity $valueEntity */
            return $valueEntity->getId();
        }

        if ($this->getPropertyEntity()->isMultiple()) {
            /** @var AEntityCollection $valueEntity */
            return (array)$valueEntity;
        }

        return $valueEntity;
    }
}