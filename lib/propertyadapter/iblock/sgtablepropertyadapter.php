<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SGTablePropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @param string $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }
}