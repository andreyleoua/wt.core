<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Collection\IBlock\SectionCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\IBlock\ElementEntity;
use Wt\Core\Entity\IBlock\SectionEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class GPropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @param IPropertyValueEntity $propertyValue
     * @return SectionEntity|SectionCollection
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        return parent::getValueEntity($propertyValue);
    }

    /**
     * @param ElementEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        if(!($value instanceof AEntity)){
            $iBlockId = \CIBlockSection::GetList(
                ['SORT' => 'ASC'],
                ['=ID' => $value],
                false,
                ['ID', 'IBLOCK_ID'],
                false
            )->Fetch()['IBLOCK_ID'];

            $value = $this->__getFactory($iBlockId)->getEntityById($value);
        }

        return $value->getName();
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        $iBlockId = $this->getPropertyEntity()->getRaw('LINK_IBLOCK_ID');
        if(!$iBlockId) {
            //throw new \Exception("IBlockId is empty in property settings {$this->getPropertyEntity()->getType()}:{$this->getPropertyEntity()->getUserType()}");
            return null;
        }
        return $this->__getFactory($iBlockId);
    }

    protected function __getFactory($iBlockId)
    {
        $iBlockAF = app()->factory()->iBlock()->getEntityById($iBlockId);
        $factory = $iBlockAF->getSectionFactory();
        return $factory;
    }
}