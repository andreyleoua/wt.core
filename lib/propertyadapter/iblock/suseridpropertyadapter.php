<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\UserEntity;
use Wt\Core\Factory\UserFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SUserIdPropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @param IPropertyValueEntity $propertyValue
     * @return AEntity|AEntityCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        $valueFactory = $this->getValueFactory();
        if(!$valueFactory) {
            return null;
        }

        if ($this->getPropertyEntity()->isMultiple()) {
            return $entity = $valueFactory->getCollection([
                '@ID' => $propertyValue->getValue(), // multiple
            ]);
        }

        return $valueFactory->getEntityById($propertyValue->getValue());
    }

    /**
     * @param UserEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getFullName();
    }

    /** @return UserFactory */
    public function getValueFactory()
    {
        return app()->factory()->main()->getUserFactory();
    }
}