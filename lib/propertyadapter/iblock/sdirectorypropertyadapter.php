<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Collection\AEntityCollection;
use Wt\Core\Collection\HlBlock\ElementCollection;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\HlBlock\ElementEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SDirectoryPropertyAdapter extends AIBlockPropertyAdapter
{

    /**
     * @param AEntity|AEntityCollection $valueEntity
     */
    public function getStoredValue($valueEntity)
    {
        if(!$valueEntity){
            return null;
        }

        if($this->getPropertyEntity()->isMultiple()){
            /** @var AEntityCollection|ElementCollection $valueEntity */
            return $valueEntity->map(function(AEntity $entity){
                /** @var AEntity|ElementEntity $entity */
                return $entity->getXmlId();
            });
        }

        /** @var AEntity|ElementEntity $valueEntity */
        return $valueEntity->getXmlId();
    }

    /**
     * @param IPropertyValueEntity $propertyValue
     * @return AEntity|AEntityCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        $valueFactory = $this->getValueFactory();
        if(!$valueFactory) {
            return null;
        }

        if ($this->getPropertyEntity()->isMultiple()) {
            return $entity = $valueFactory->getCollection([
                '@UF_XML_ID' => $propertyValue->getValue(), // multiple
            ]);
        }

        return $valueFactory->getEntityByXmlId($propertyValue->getValue());
    }

    /**
     * @param ElementEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        if($value){
            return $value->getName();
        }
        return '';
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        $tableName = $this->getPropertyEntity()->getSettings()['TABLE_NAME'];
        $hlBlockElementFactory = app()->factory()->hlBlock()->getEntityByXmlId($tableName)->getElementFactory();
        return $hlBlockElementFactory;
    }
}