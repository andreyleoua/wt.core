<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Interfaces\IPropertyValueEntity;

class SIBlockSectionMetaTemplateListPropertyAdapter extends SIBlockTemplatePropertyAdapter
{
    /**
     * @param string $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        $value = (array)$value;
        foreach ($value as $key => &$item){
            $item = parent::getDisplayValueBySingleValue($item, $propertyValue);
        }

        return $value;
    }
}