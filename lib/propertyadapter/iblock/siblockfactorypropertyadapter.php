<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\AbstractFactory\AAbstractFactory;
use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\IBlock\ElementEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SIBlockFactoryPropertyAdapter extends AIBlockPropertyAdapter
{

    /**
     * @param ElementEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        if(!($value instanceof AEntity)){
            $value = $this->getValueFactory()->getEntityById($value);
        }
        if($value instanceof AEntity){
            return $value->getName();
        }
        return $value;
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        $getNormalizeArgs = function($factoryArgs){
            end($factoryArgs);
            while (!is_null($i = key($factoryArgs))) {
                if($factoryArgs[$i] !== '' && $factoryArgs[$i] !== null) {
                    break;
                }
                unset($factoryArgs[$i]);
                prev($factoryArgs);
            }
            reset($factoryArgs);

            return $factoryArgs;
        };
        $manualFactoryClass = $this->getPropertyEntity()->getSettings('MANUAL_FACTORY_CLASS');
        $manualFactoryEntityId = $this->getPropertyEntity()->getSettings('MANUAL_FACTORY_ENTITY_ID');
        $abstractFactoryClass = $this->getPropertyEntity()->getSettings('ABSTRACT_FACTORY_CLASS');
        $abstractFactoryArgs = $this->getPropertyEntity()->getSettings('ABSTRACT_FACTORY_ARGS');
        $abstractFactoryArgs = $getNormalizeArgs($abstractFactoryArgs);
        /**
         * @var AAbstractFactory $abstractFactory
         */
        $abstractFactory = resolve($abstractFactoryClass, (array)$abstractFactoryArgs);
        $factoryClass = $this->getPropertyEntity()->getSettings('FACTORY_CLASS');
        $factoryArgs = $this->getPropertyEntity()->getSettings('FACTORY_ARGS');
        $factoryArgs = $getNormalizeArgs($factoryArgs);



        $factory = $abstractFactory->getFactoryInstance($factoryClass, $factoryArgs);

        return $factory;
    }
}