<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Entity\Main\FileEntity;
use Wt\Core\Factory\Main\FileFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class FPropertyAdapter extends AIBlockPropertyAdapter
{

    /** @return FileFactory */
    public function getValueFactory()
    {
        return app()->factory()->main()->getFileFactory();
    }

    /**
     * @param FileEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string|int
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getSrc();
    }
}