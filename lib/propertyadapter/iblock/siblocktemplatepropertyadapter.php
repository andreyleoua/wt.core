<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Factory\IBlock\PropertyValueFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SIBlockTemplatePropertyAdapter extends AIBlockPropertyAdapter
{

    /**
     * @param string $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        $template = \Bitrix\Iblock\Template\Helper::convertArrayToModifiers(
            array(
                'TEMPLATE' => $value,
                'INHERITED' => 'N',
            )
        );
        $iblockId = $this->getPropertyEntity()->getSettings()['IBLOCK_ID'];

        if(!$iblockId){
            return $value;
        }
        /** @var PropertyValueFactory $propertyValueFactory */
        $propertyValueFactory = $propertyValue->getFactory();
        $elementId = $propertyValueFactory->getElementId();
        $iBlockAF = app()->factory()->iBlock()->getEntityById($iblockId);

        if(!$iBlockAF){
            return $value;
        }

        $elementEntity = $iBlockAF->getElementFactory()->getEntityById($elementId);
        $arFields = array(
            'ID' => $elementId,
            'IBLOCK_ID' => $iblockId,
        );
        if($elementEntity){
            $arFields = $elementEntity->toArray();
        }

        $iPropTemplates = new \Bitrix\Iblock\InheritedProperty\ElementTemplates($iblockId, $elementId);
        if($iPropTemplates) {
            $values = $iPropTemplates->getValuesEntity();
            $entity = $values->createTemplateEntity();
            $entity->setFields($arFields);
            $templates = $iPropTemplates->findTemplates();

            $value = htmlspecialchars_decode(\Bitrix\Main\Text\HtmlFilter::encode(\Bitrix\Iblock\Template\Engine::process($entity, $template)));
        }


        return $value;
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }
}