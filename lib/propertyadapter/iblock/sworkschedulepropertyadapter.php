<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SWorkSchedulePropertyAdapter extends AIBlockPropertyAdapter
{

    /**
     * @param string $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }


    /**
     * @see https://disk.yandex.ru/i/2XyyegIxUs5Btg
     *
     * @param $val
     * @return array
     */
    public function getGroupList($val)
    {
        $result = [];
        $groupIndex = 0;
        $prevData = [];
        foreach ($val as $day => $data){
            if($data['FROM'] && $data['TO']){
                if($prevData == $data){
                    if(isset($result[$groupIndex])){
                        $result[$groupIndex]['INTERVAL']['TO'] = $day;
                    } else {
                        $result[$groupIndex]['INTERVAL']['FROM'] = $day;
                    }
                } else {
                    $groupIndex++;
                    $result[$groupIndex]['INTERVAL']['FROM'] = $day;
                    $result[$groupIndex]['TIME'] = $data;
                }
            } else {
                $groupIndex++;
                $result[$groupIndex]['INTERVAL']['FROM'] = $day;
                $result[$groupIndex]['TIME'] = [];
            }

            $prevData = $data;
        }
        foreach ($result as &$value){
            if(isset($value['INTERVAL']['TO'])){
                $dayLangFrom = GetMessage('DOW_'.$value['INTERVAL']['FROM']);
                $dayLangTo = GetMessage('DOW_'.$value['INTERVAL']['TO']);
                $dayLang = $dayLangFrom . '-' . $dayLangTo;
            } else {
                $dayLangFrom = GetMessage('DOW_'.$value['INTERVAL']['FROM']);
                $dayLang = $dayLangFrom;
            }
            $value['LANG'] = $dayLang;
        }

        return $result;
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }
}