<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class NPropertyAdapter extends AIBlockPropertyAdapter
{
    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }

    /**
     * @param int $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return float
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        $value = str_replace(',', '.', $value);
        return (float)$value;
    }
}