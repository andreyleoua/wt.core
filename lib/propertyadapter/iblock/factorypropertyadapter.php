<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Entity\AEntity;
use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class FactoryPropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @param AEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getName();
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }
}