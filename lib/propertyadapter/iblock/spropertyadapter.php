<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SPropertyAdapter extends AIBlockPropertyAdapter
{
    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }

    /**
     * @param string $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value;
    }
}