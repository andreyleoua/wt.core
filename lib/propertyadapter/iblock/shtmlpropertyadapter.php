<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class SHtmlPropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @param array $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return string
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value['TEXT'];
    }

    /** @return AFactory */
    public function getValueFactory()
    {
        return null;
    }
}