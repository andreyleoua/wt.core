<?php


namespace Wt\Core\PropertyAdapter\IBlock;


use Wt\Core\Collection\IBlock\PropertyEnumCollection;
use Wt\Core\Entity\IBlock\PropertyEnumEntity;
use Wt\Core\Factory\IBlock\PropertyEnumFactory;
use Wt\Core\Interfaces\IPropertyValueEntity;
use Wt\Core\Support\Result;
use Wt\Core\PropertyAdapter\AIBlockPropertyAdapter;

class LPropertyAdapter extends AIBlockPropertyAdapter
{
    /**
     * @return PropertyEnumFactory
     */
    public function getValueFactory()
    {
        $iBlockId = $this->getPropertyEntity()->getFactory()->getBlockId();
        $propertyId = $this->getPropertyEntity()->getId();

        $iblock = app()->factory()->iBlock()->getEntityById($iBlockId);
        return $iblock->getFactoryInstance(PropertyEnumFactory::class, [$iBlockId, $propertyId]);
    }

    /**
     * @param IPropertyValueEntity $propertyValue
     * @return PropertyEnumEntity|PropertyEnumCollection|null
     */
    public function getValueEntity(IPropertyValueEntity $propertyValue)
    {
        return parent::getValueEntity($propertyValue);
    }

    /**
     * @param $elementId
     * @param $value
     * @param null $fieldCode = ID | XML_ID | VALUE
     * @throws null
     * @deprecated test
     */
    public function setValue($elementId, $value, $fieldCode = null)
    {
        $result = Result::make();
        $fields = ['ID', 'XML_ID', 'VALUE'];
        \Bitrix\Main\Loader::includeModule('iblock');
        $fieldCode = in_array($fieldCode, $fields)?$fieldCode:'ID';

        $propertyEnumCollection = $this->getValueFactory()->getCollection()->filter(function (PropertyEnumEntity $entity) use ($value, $fieldCode){
            return $entity->has($fieldCode) && ($entity[$fieldCode] == $value);
        });
        $blockId = $this->getPropertyEntity()->getFactory()->getBlockId();
        if(!$propertyEnumCollection->count()){
            $result->setError('Values not found');
            return $result;
        }
        $data = [
            $this->getPropertyEntity()->getId() => $propertyEnumCollection->first()->getId(),
        ];
        \CIBlockElement::SetPropertyValuesEx($elementId, $blockId, $data);
        $result->setData([
            'IBLOCK_ID' => $blockId,
            'ELEMENT_ID' => $elementId,
            'PROPERTY_VALUES' => $data,
        ]);
        return $result;
    }

    /**
     * @param PropertyEnumEntity $value
     * @param IPropertyValueEntity|null $propertyValue
     * @return float
     */
    protected function getDisplayValueBySingleValue($value, IPropertyValueEntity $propertyValue = null)
    {
        return $value->getName();
    }
}