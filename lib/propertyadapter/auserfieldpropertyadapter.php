<?php


namespace Wt\Core\PropertyAdapter;


use Wt\Core\Entity\AEntity;
use Wt\Core\Entity\UserFieldEntity;
use Wt\Core\Form\Interfaces\IOptionsAttribute;
use Wt\Core\Form\Interfaces\IRowsColsAttribute;

abstract class AUserFieldPropertyAdapter extends APropertyAdapter
{

    /**
     * @return UserFieldEntity
     */
    public function getPropertyEntity()
    {
        return parent::getPropertyEntity();
    }

    /**
     * @param array $restoredValue
     * @return array
     */
    public function getValue(array $restoredValue)
    {
        $result = $restoredValue;
        $result['VALUE'] = $this->getValueConvertFromDB($result['VALUE']);

        return $result['VALUE'];
    }

    /**
     *
     * [USER_TYPE_ID] => string
     * [CLASS_NAME] => Bitrix\Main\UserField\Types\IntegerType
     * [EDIT_CALLBACK] => callable
     * [VIEW_CALLBACK] => callable
     * [USE_FIELD_COMPONENT] => 1
     * [DESCRIPTION] => string
     * [BASE_TYPE] => string
     *
     * @return array
     */
    public function getUserTypeBuild()
    {
        global $USER_FIELD_MANAGER;
        $arType = $USER_FIELD_MANAGER->GetUserType($this->getPropertyEntity()->getUserType());
        if(is_array($arType)){
            return $arType;
        }
        return [];
    }

    protected function getValueConvertFromDB($storageValue)
    {
        if($this->getPropertyEntity()->isMultiple()){
            $values = [];
            foreach ($storageValue as $key => $value) {
                $values[$key] = $this->convertFromDB($value)['VALUE'];
            }
            return $values;
        }
        return $this->convertFromDB($storageValue)['VALUE'];
    }

    protected function convertFromDB($value, $description = '')
    {
        $valueEx = ['VALUE' => $value, 'DESCRIPTION' => $description];

        $arUserType = $this->getUserTypeBuild();

        if (isset($arUserType['ConvertFromDB']) && is_callable($arUserType['ConvertFromDB']))
        {
            $valueEx = call_user_func_array($arUserType['ConvertFromDB'], [$this->getPropertyEntity()->getRaw(), $valueEx]);
        } elseif(
            isset($arUserType['CLASS_NAME']) &&
            class_exists($arUserType['CLASS_NAME']) &&
            method_exists($arUserType['CLASS_NAME'], 'ConvertFromDB')
        ) {
            $valueEx = call_user_func_array([$arUserType['CLASS_NAME'], 'ConvertFromDB'], [$this->getPropertyEntity()->getRaw(), $valueEx]);
        }

        return $valueEx;
    }

    public function getFormFieldData()
    {
        $propertyEntity = $this->getPropertyEntity();
        $fields = [];
        $fields['multiple'] = $propertyEntity->isMultiple();
        $fields['required'] = $propertyEntity->isRequired();
        $fields['data-property-full-type'] = $propertyEntity->__getDevType();

        $fields['label'] = $propertyEntity->getName();
        $fields['name'] = "BLOCK[{$propertyEntity->getFactory()->getEntityId()}][USER_FIELD][{$propertyEntity->getId()}]";
        $fields['value'] = $propertyEntity->getDefaultValue();
        $id = str_replace('][', '_', $fields['name']);
        $id = str_replace('[', '_', $id);
        $id = str_replace(']', '', $id);
        $fields['id'] = $id;
        $fields['data-property-sort'] = $propertyEntity->getRaw('SORT', 500);


        if(is_a($this->getFormFieldClass(), IRowsColsAttribute::class, true)){
            $fields['rows'] = $propertyEntity->getRaw('ROW_COUNT', 1);
            $fields['cols'] = $propertyEntity->getRaw('COL_COUNT', 30);
        }
        if(is_a($this->getFormFieldClass(), IOptionsAttribute::class, true) && $this->getValueFactory()){
            $options = $this->getValueFactory()->getCollection([], [], [], 100)->map(function (AEntity $entity){
                return "[{$entity->getId()}] {$entity->getName()}";
            });
            $fields['options'] = $options->prepend('not select');
        }

        return $fields;
    }
 }