<?php


namespace Wt\Core\Support;


use Bitrix\Main\Error as BxError;
use Throwable;

class Error extends Entity
{
	public function __construct($message, $code = '', $data = [])
	{
	    $bxErrorClass = 'Bitrix\Main\Error';
        if(class_exists($bxErrorClass, false) && is_a($message, $bxErrorClass)){
            /**
             * @var BxError $bxError
             */
            $bxError = $message;
            $customData = [];
            if(is_callable([$bxError, 'getCustomData'])){
                $customData = $bxError->getCustomData();
                if(!is_array($customData)){
                    $customData = [];
                }
            }
            $entity = [
                'message' => $bxError->getMessage(),
                'code' => $bxError->getCode(),
                'data' => array_merge(
                    $customData,
                    $data
                ),
            ];
        } elseif(is_a($message, Throwable::class) || is_a($message, \Exception::class)){
            /** @var Throwable $e */
	        $e = $message;
            $entity = [
                'type' => get_class($e),
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace(),
                'data' => $data,
            ];
        } else {
            $entity = [
                'message' => $message,
                'code' => $code,
                'data' => $data,
            ];
        }
        parent::__construct($entity);
	}

	public function getFile()
    {
        return $this->get('file', 0);
    }

    public function getType()
    {
        return $this->get('type', '');
    }

	public function getLine()
    {
        return $this->get('line', 0);
    }

	public function getTrace()
    {
        return $this->get('trace', []);
    }
    //Error: Call to a member function getLabel() on null in E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\shell.php(118) : eval()'d code:9
    //Stack trace:
    //#0 E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\shell.php(118): eval()
    //#1 E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\templater.php(111): Wt\Core\Templater\Shell->getFileResult()
    //#2 E:\OSP\domains\b.bx\local\modules\wt.core\lib\form\field\defaultfield.php(212): Wt\Core\Templater\Templater->getFileTemplate()
    //#3 E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\shell.php(118) : eval()'d code(10): Wt\Core\Form\Field\DefaultField->render()
    //#4 E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\shell.php(118): eval()
    //#5 E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\templater.php(111): Wt\Core\Templater\Shell->getFileResult()
    //#6 E:\OSP\domains\b.bx\local\modules\wt.core\lib\form\form.php(178): Wt\Core\Templater\Templater->getFileTemplate()
    //#7 E:\OSP\domains\b.bx\_utils\form\test.php(32): Wt\Core\Form\Form->render()
    //#8 {main}

    // trace
    //[file] => E:\OSP\domains\b.bx\local\modules\wt.core\lib\templater\templater.php
    //[line] => 111
    //[function] => getFileResult
    //[class] => Wt\Core\Templater\Shell
    //[type] => ->
    protected function getErrorMsg($lineBreak = PHP_EOL, $inHtml = false, $bTrace = true)
    {
        $traceAsString = '';

        $strCollect = collect();
//        $strCollect->push($this->getMessage() . ($this->getCode()?('['.$this->getCode().']'):''));
        $strCollect->push(
            ($this->getType()?"{$this->getType()}: ":'')
            . $this->getMessage()
        );
        if($this->getFile()){
            $strCollect->push($this->getFile().':'.$this->getLine());
        }
        if($bTrace && $this->getTrace()){
            $strCollect->push('Stack trace:');
            foreach ($this->getTrace() as $i => $trace){
                $strCollect->push("#{$i} {$trace['file']}({$trace['line']}): {$trace['class']}{$trace['type']}{$trace['function']}");
            }
        }
        $result = $strCollect->join($lineBreak);

        if ($inHtml) {
            $result =
                '<div class="text-danger" style="font-size:11px;font-family:monospace;color:black;line-height:11px;overflow:visible;margin: 0 0 10px;white-space:pre-wrap;word-break:break-word;">'
                .$result
                .'</div>';
        }
        return $result;
    }

	public function render()
    {
        echo $this->getErrorMsg(PHP_EOL, true, true);
        return $this;
    }

	/**
	 * @return int|string
	 */
	public function getCode()
	{
		return $this->get('code', 0);
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->get('message');
	}

    /**
     * @param $value
     * @return $this
     */
	public function setMessage($value)
	{
		$this->set('message', $value);
		return $this;
	}

    /**
     * @param $value
     * @return $this
     */
	public function setFile($value)
	{
		$this->set('file', $value);
		return $this;
	}

	public function getData()
	{
		return $this->get('data');
	}

	public function __toString()
	{
		return $this->getMessage();
	}
}
