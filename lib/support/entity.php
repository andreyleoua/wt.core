<?php


namespace Wt\Core\Support;


use Wt\Core\Interfaces\IPsr;
use Wt\Core\Interfaces\IEntity;
use Wt\Core\Interfaces\IFields;
use Wt\Core\Interfaces\TPsr;

class Entity extends IterableAccess implements IPsr, IEntity
{
    use TPsr;

    public function has($name)
    {
        return $this->__isset($name);
    }

    public function &get($name, $default = null)
    {
        if( !$this->has($name) ){
            return $default;
        }

        return $this->__get($name);
    }

    /**
     * @param bool $raw
     * @return $this
     */
    public function pre($raw = true)
    {
        pre($raw?$this->getRaw():$this);
        return $this;
    }

    protected function getDefaultRaw()
    {
        return [];
    }

    protected function setRaw($raw)
    {
        $raw = array_merge($this->getDefaultRaw(), $raw);
        return parent::setRaw($raw);
    }
}
