<?php
/**
 * Created by PhpStorm.
 * Company: ittown.by
 * Project: module.webteam.by
 * User: Andrey Leo
 * Date: 6/12/21
 * Time: 1:35 AM
 * All rights reserved
 */

namespace Wt\Core\Support;

use Bitrix\Main\Page\Asset;

class Assets
{
    protected $includeJs = [];

    protected $includeCss = [];

    public function registerCss(){
        foreach ($this->includeCss as $cssPath){
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.$cssPath);
        }
    }

    public function registerJs(){
        foreach ($this->includeJs as $jsPath){
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.$jsPath);
        }
    }

    public function init(){
        $this->registerCss();
        $this->registerJs();
    }

    public function getCss(){
        return $this->includeCss;
    }

    public function getJs(){
        return $this->includeJs;
    }

    public function setCss(array $css = []){
        $this->includeCss = $css;
    }

    public function setJs(array $js = []){
        $this->includeJs = $js;
    }

    public function pushJs($js){
        $this->includeJs[] = $js;
    }

    public function pushCss($css){
        $this->includeCss[] = $css;
    }
}