<?php


namespace Wt\Core\Support;


use Wt\Core\Factory\AFactory;
use Wt\Core\Interfaces\IFactory;

class FactoryMapper extends Mapper
{
    /**
     * @var $factory AFactory
     */
    protected $factory;


    protected $index = [];

    public function put($cacheKey, $id)
    {
        parent::put($cacheKey, $id);
        $this->index[$id][$cacheKey] = $id;
    }

    public function hasId($id)
    {
        return isset($this->index[$id]);
    }

    public function removeId($id)
    {
        if(!$this->hasId($id)){
            return $this;
        }

        foreach ($this->index[$id] as $cacheKey){
            unset($this->map[$cacheKey]);
        }
        unset($this->index[$id]);
        return $this;
    }

    public function clear()
    {
        parent::clear();
        $this->index = [];
    }

    /**
     * @return AFactory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @param AFactory $factory
     */
    public function setFactory(IFactory $factory)
    {
        $this->factory = $factory;
    }


    public function getCacheKey($value, $type = 'id')
    {
        return $this->getFactory()->__getCachePrefix().':'.$type.':'.$value;
    }

    public function getId($value, $type)
    {
        $cacheKey = $this->getCacheKey($value, $type);
        return $this->get($cacheKey);
    }

    public function getIdCacheKey($key, $type)
    {
        $id = $this->getId($key, $type);
        if($id){
            return $this->getCacheKey($id);
        }
        return null;
    }

    public function makeMapper($raw)
    {
        if($raw instanceof Entity){
            $raw = $raw->getRaw();
        }
        $id = $raw['ID'];

        if( !$id ){
            return $this;
        }

        if($this->hasId($id)){
            return $this;
        }

        $this->index[$id] = [];

        if( isset($raw['CODE']) && $raw['CODE'] != ''){
            $this->put($key = $this->getCacheKey($raw['CODE'], 'code'), $id);
        }
        elseif( isset($raw['UF_CODE']) && $raw['UF_CODE'] != ''){
            $this->put($key = $this->getCacheKey($raw['UF_CODE'], 'code'), $id);
        }
        if( isset($raw['XML_ID']) && $raw['XML_ID'] != '' ){
            $this->put($key = $this->getCacheKey($raw['XML_ID'], 'xml_id'), $id);
        }
        elseif( isset($raw['UF_XML_ID']) && $raw['UF_XML_ID'] != '' ){
            $this->put($key = $this->getCacheKey($raw['UF_XML_ID'], 'xml_id'), $id);
        }

        return $this;
    }

    public function getIndexValues($id)
    {
        return (array)$this->index[$id];
    }
}