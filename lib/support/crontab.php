<?


namespace Wt\Core\Support;


class Crontab
{
    const RE_SCHEDULE_UNIT = '[\*\d\/\,]+';
    const RE = '/^(?=[^#])(?:\s*)(?<schedule>(?<m>[\*\d\/\,]+)\s+(?<h>[\*\d\/\,]+)\s+(?<dom>[\*\d\/\,]+)\s+(?<mon>[\*\d\/\,]+)\s+(?<dow>[\*\d\/\,]+))(?:\s+)(?<command>.+)/';
    const RE_SCHEDULE = '/(?:\s*)(?<m>[\*\d\/\,]+)\s+(?<h>[\*\d\/\,]+)\s+(?<dom>[\*\d\/\,]+)\s+(?<mon>[\*\d\/\,]+)\s+(?<dow>[\*\d\/\,]+)(?:\s*)/';
    const RE_FIND = '/^(?=[^#])(?:\s*)(?<schedule>(?<m>%s)\s+(?<h>%s)\s+(?<dom>%s)\s+(?<mon>%s)\s+(?<dow>%s))(?:\s+)(?<command>%s)/';
    const RE_VALID = '/^(?=[^#])(?:\s*)(?<schedule>(?:[\*\d\/\,]+\s+){5})(?<command>.+)/';

	/**
	 *	Check if crontab can be managed by this script (only if Linux OS)
	 */
	public function isInstalled()
    {
        $system = new System();
		return $system->crontabInstalled();
	}

    /**
     * @param array $filter
     * @return string
     */
	protected function getPattern($filter = [])
    {
        $m = $h = $dom = $mon = $dow = static::RE_SCHEDULE_UNIT;
        $command = '.+';

        $prepare = function (&$var, $filter, $key){
            if(isset($filter[$key]) && strlen(trim($filter[$key]))){
                $var = preg_quote(trim($filter[$key]), '/');
            }
        };

        $prepare($command, $filter, 'command');

        $scheduleProvider = $filter;
        if(isset($filter['schedule'])){
            $scheduleMatches = [];
            if(preg_match(static::RE_SCHEDULE, $filter['schedule'], $scheduleMatches)){
                $scheduleProvider = $scheduleMatches;
            }
        }
        $prepare($m, $scheduleProvider, 'm');
        $prepare($h, $scheduleProvider, 'h');
        $prepare($dom, $scheduleProvider, 'dom');
        $prepare($mon, $scheduleProvider, 'mon');
        $prepare($dow, $scheduleProvider, 'dow');

        $re = sprintf(static::RE_FIND, $m, $h, $dom, $mon, $dow, $command);

        return $re;
    }

    protected function getDataByMatches($matches)
    {
        return [
            'cron' => $matches[0],
            'm' => trim($matches['m']),
            'h' => trim($matches['h']),
            'dom' => trim($matches['dom']),
            'mon' => trim($matches['mon']),
            'dow' => trim($matches['dow']),
            'command' => trim($matches['command']),
        ];
    }

    protected function getDataByCronString($str)
    {
        $data = [];
        $matches = [];
        if(preg_match($this->getPattern(), $str, $matches) !== 1) {
            return $data;
        }
        $data = $this->getDataByMatches($matches);
        return $data;
    }

    /**
     * Get cron jobs
     * @param array $filter [(m h dom mon dow)|schedule command]
     * @return array
     */
    public function getList($filter = [])
    {
        $re = $this->getPattern($filter);

        $commandShell = 'crontab -l;';
        exec($commandShell, $rs);
        $list = [];

        foreach ((array)$rs as $line){
            $matches = [];
            if(preg_match($re, $line, $matches) !== 1) {
                continue;
            }
            $list[] = $this->getDataByMatches($matches);
        }

        return $list;
    }

    /**
     * Check cron job exists
     *
     * @param $command
     * @return bool
     */
    public function isExists($command)
    {
        $list = $this->getList(['command' => $command]);
        return !!count($list);
    }

    /**
     * @param $data
     * @return string
     */
    protected function getCronStringByData($data)
    {
        $command = $data['command'];
        if(isset($data['schedule'])){
            $schedule = $data['schedule'];
            $stringData = "$schedule $command";
        } else {
            $m = strlen(trim($data['m']))?trim($data['m']):'*';
            $h = strlen(trim($data['h']))?trim($data['h']):'*';
            $dom = strlen(trim($data['dom']))?trim($data['dom']):'*';
            $mon = strlen(trim($data['mon']))?trim($data['mon']):'*';
            $dow = strlen(trim($data['dow']))?trim($data['dow']):'*';

            $stringData = "$m $h $dom $mon $dow $command";
        }

        return $stringData;
    }



    /**
     * Add cron job
     * @param string|array|iterable $data
     * @return bool
     */
	public function add($data)
    {
        if(is_string($data)){
            $data = $this->getDataByCronString($data);
        }

		if(!$this->getList($data)) {
			exec('(crontab -l 2>/dev/null; echo "'.$this->getCronStringByData($data).'") | crontab -', $result);
		}
		return !!$this->getList($data);
	}

    /**
     * Delete cron job
     *
     * @param $command
     * @return bool
     */
	public function delete($command)
    {
        $preg = function($str){
            $str = trim($str);
            $str = preg_quote($str);
            $str = preg_replace('/\s+/', '\s+', $str);
            $str = '\s*'.$str.'\s*$';
            return $str;
        };
        if($list = $this->getList(['command' => $command])){
            foreach ($list as $entity){
                /**
                 * Команда grep: опции, регулярные выражения и примеры использования
                 * @see https://zalinux.ru/?p=1270
                 */
                exec('crontab -l | grep -v -F "'.$entity['cron'].'" | crontab -', $Result);
                /**
                 * Варинт поиска строок по регулярному выражению
                 * не чувствителен к множеству пробелов
                 */
                //exec('crontab -l | grep -v -P "'.$preg($entity['cron']).'" | crontab -', $Result);
            }
            return !$this->isExists($command);
        }
		return false;
	}
}