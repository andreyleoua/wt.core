<?php

namespace Wt\Core\Support;

class Registry
{
    protected $registry = [];
    protected $immutableKeys = [];

    public function set($key, $value, $immutable = false)
    {
        if( in_array($key, $this->immutableKeys) ){
            return;
        }

        if( $immutable ){
            $this->immutableKeys[] = $key;
        }

        $this->registry[$key] = $value;
    }

    public function put($key, $value, $immutable = false)
    {
        $this->set($key,$value, $immutable);
    }

    public function get($key, $default = null)
    {
        if( !isset($this->registry[$key]) ){
            return $default;
        }

        return $this->registry[$key];
    }

    public function has($key)
    {
        return isset($this->registry[$key]) || array_key_exists($key, $this->registry);
    }
}