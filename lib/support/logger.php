<?php

namespace Wt\Core\Support;

use Psr\Log;

class Logger extends Log\AbstractLogger
{
    public const CONTENT_ARRAY = 'array';
    public const CONTENT_JSON = 'json';
    public const CONTENT_JSON_PRETTY = 'json-pretty';

    public $printToStdOut = false;

    protected string $root = '/';
    protected $folder = null;
    protected $file = null;
    protected array $config = [
        'ext' => 'log',
        'filename' => '',
        'data_format' => 'Y-m-d',
    ];

    protected string $contentType = 'array';

    public function setContentType($value)
    {
        $this->contentType = $value;
    }

    public function config($folder = '', $filename = '', $formatData = 'Y-m-d')
    {
        $this->config['data_format'] = $formatData;

        $this->setFilename($filename);
        $this->setFolder($folder);

        return $this;
    }

    public function setFilename($filename)
    {
        $this->config['filename'] = $filename;
        $this->makeBasename();
        return $this;
    }

    protected function makeBasename()
    {
        $fileNameArray = [];
        if($this->config['data_format']){
            $fileNameArray[] = date($this->config['data_format']);
        }
        if($this->config['filename']){
            $fileNameArray[] = $this->config['filename'];
        }
        $basename = implode('_', $fileNameArray);

        if( !str_ends_with($basename, '.' . $this->config['ext']) ){
            $basename .= '.' . $this->config['ext'];
        }
        $this->file = $basename;
    }

    public function setFolder($folderName)
    {
        $this->folder = trim($folderName, '/\\');
        return $this;
    }

    public function setRoot($path)
    {
        if( $path ){
            if( $path[0] !== DIRECTORY_SEPARATOR ){
                $path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $path;
            }

            $this->root = rtrim($path, '/\\');
        }
    }

    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $formattedMessage = $this->formattedMessage($level, $message, $context, $this->contentType);
        $this->writeline($formattedMessage);
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     * @param string $contextType json|array
     * @return string
     */
    protected function formattedMessage($level, $message, array $context = [], string $contextType = 'json')
    {
        $time = date('Y-m-d H:i:s');
        $message = trim($message);
        if( !$context ) {
            $context = '';
        } elseif( $contextType == 'array' ){
            $context = "\n" . var_export($context,true) . "\n";
        } elseif( $contextType == 'json-pretty' ){
            $context = "\n" . json_encode($context, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE) . "\n";
        } else {
            $context = "\n    ".json_encode($context, JSON_UNESCAPED_UNICODE);
        }

        return trim("$time [$level]: $message$context") . "\n";
    }

    protected function getNormalizePath(...$p)
    {
        $result = array_shift($p);
        foreach ($p as $item){
            if(strlen($item[0])){
                $result .= DIRECTORY_SEPARATOR . $item;
            }
        }
        return $result;
    }

    public function getPath()
    {
        if(is_null($this->file)){
            $this->makeBasename();
        }
        return $this->getNormalizePath($this->root, $this->folder, $this->file);
    }

    protected function writeline($message)
    {
        if( $this->printToStdOut ){
            echo $message.PHP_EOL;
        }
        if($this->createFolder() && $handler = @fopen($this->getPath(),'ab')){
            $result = fwrite($handler, $message);
                fclose($handler);
            return $result > 0;
        }
        return false;

    }

    protected function createFolder()
    {
        $path = $this->getNormalizePath($this->root, $this->folder);
        if (!file_exists($path))
        {
            $folder = mkdir($path, 0755, true);
                $htaccessHandler = fopen($this->getNormalizePath($path, '.htaccess'), 'w');
                    fwrite($htaccessHandler, 'deny from all');
                        fclose($htaccessHandler);
            return $folder;
        }
        return true;
    }

    /**
     * Clear log file
     */
    public function clearLog()
    {
        $path = $this->getPath();
        if($this->createFolder() && is_writable($path)){
            file_put_contents($path, '');
        }
    }

    /**
     * Clear log file, alias clearLog
     */
    public function clear()
    {
        $this->clearLog();
    }

    /**
     * Clear log file, alias clearLog
     */
    public function clean()
    {
        $this->clearLog();
    }

    /**
     * Remove log file
     */
    public function removeLog()
    {
        $path = $this->getPath();
        if($this->createFolder() && is_writable($path)){
            unlink($path);
        }
    }

    public function remove()
    {
        $this->removeLog();
    }

    public function delete()
    {
        $this->removeLog();
    }
}