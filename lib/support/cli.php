<?php


namespace Wt\Core\Support;


use Wt\Core\Entity\CliExecResultEntity;

/**
 * @see https://wp-kama.ru/note/what-is-dev-null | https://linuxhint.com/what_is_dev_null/
 * @see https://wiki.dieg.info/chto_oznachaet_dev_null_2_1_ili_perenapravlenie_stdin_stdout_i_stderr
 * @see https://www.opennet.ru/docs/RUS/bash_scripting_guide/c11620.html
 *
 * смену директирий необходимо осуществлять через методы класса, в противном случае возможна некорректная работа скриптов
 */
class Cli
{
    protected $dir;
    protected $saveResults = true;
    protected $defaultRedirection = '2>&1';
    /**
     * @var CliExecResultEntity[]
     */
    protected $results;

    public static function create()
    {
        return new static();
    }

    /**
     * @param null $value
     * @return $this|bool
     */
    protected function saveResults($value = null)
    {
        if(is_null($value)){
            return $this->saveResults;
        }
        $this->saveResults = (bool)$value;
        return $this;
    }

    public function __construct()
    {
        $this->results = [];
    }

    public function home()
    {
        $dir = '~';
        if($this->isWinOS()){
            $dir = '%USERPROFILE%';
        }
        return $this->cd($dir);
    }

    public function username()
    {
        $command = 'whoami';
        if($this->isWinOS()){
            $command = 'echo %UserName%';
        }
        return $this->exec($command);
    }

    public function cd($dir)
    {
        $pwdCommand = 'pwd';
        if($this->isWinOS()){
            $pwdCommand = 'cd';
        }
        $command = 'cd';
        if($this->isWinOS()){
            $command = 'cd /D';
        }

        $r = $this->exec("$command \"$dir\"");
        if($r->isSuccess()){
            $this->dir = $r->getOutputString();
        }
        return $r;
    }

    public function pwd()
    {
        $command = 'pwd';
        if($this->isWinOS()){
            $command = 'cd';
        }

        return $this->exec($command);
    }

    public function comment($value = PHP_EOL)
    {
        $rs = [
            'comment' => $value,
        ];
        /**
         * @var CliExecResultEntity $resultEntity
         */
        $resultEntity = resolve(CliExecResultEntity::class, [$rs]);
        $resultEntity->convertTo('UTF-8');
        if($this->saveResults){
            $this->results[] = $resultEntity;
        }
        return $resultEntity;
    }

    public function detached($command, $redirection = null, $comment = '')
    {
        $redirection = $redirection??$this->defaultRedirection;
        return $this->exec("screen -dm bash -c \"$command $redirection\"", '', $comment);
    }

    /**
     * @param $command
     * @param $redirection
     * @param string $comment
     * @return CliExecResultEntity
     * @throws null
     */
    public function exec($command, $redirection = null, $comment = '')
    {
        $outputArray = null;
        $code = null;
        $preCommand = '';
        if($this->dir){
            $preCommand = "cd \"{$this->dir}\" && ";
            if($this->isWinOS()){
                $preCommand = "cd /D \"{$this->dir}\" && ";
            }
        }

        $redirection = $redirection??$this->defaultRedirection;
        if($this->isWinOS()){
            $preCommand .= 'echo | set /p="" & ';
        } else {
            $preCommand .= 'echo "" | ';
        }

        exec($realCommand = "$preCommand ($command $redirection)", $outputArray, $code);//escapeshellcmd

        $rs = [
            'command' => $command,
            'realCommand' => $realCommand,
            'outputArray' => $outputArray,
            'code' => $code,
            'comment' => $comment,
            'dir' => $this->dir,
        ];
        /**
         * @var CliExecResultEntity $resultEntity
         */
        $resultEntity = resolve(CliExecResultEntity::class, [$rs]);
        $resultEntity->convertTo('UTF-8');
        if($this->saveResults){
            $this->results[] = $resultEntity;
        }
        return $resultEntity;
    }

    public function execWhileSuccess($commandList)
    {
        $bResult = true;
        foreach ($commandList as $command){
            /** @var CliExecResultEntity $rs */
            $rs = call_user_func_array([$this, 'exec'], (array)$command);
            if(!$rs->isSuccess()){
                $bResult = false;
                break;
            }
        }
        return $bResult;
    }

    public function script($dir, $commands = [])
    {
        $this->cd($dir);
        foreach ($commands as $command) {
            $this->exec($command);
        }
        return $this;
    }

    public function reset()
    {
        $this->results = [];
        return $this;
    }

    /**
     * @return CliExecResultEntity[]
     */
    public function getResults()
    {
        return $this->results;
    }

    public function render($return = false)
    {
        $body = '';
        foreach ($this->getResults() as $resultEntity) {
            $cursor = '<span style="color: #6BE234;">$</span>';
            $code = !$resultEntity->isSuccess()?'<span style="color: grey;"> ['.$resultEntity->getCode().']</span>':'';
            $command = '';
            if($resultEntity->getCommand()){
                $command = '<div>'.$cursor.' <span style="color: #729FCF;">'.htmlspecialchars($resultEntity->getCommand()).'</span>'.$code.'</div>';
            }
            $outputString = '<div style="padding-left:1.1em;white-space: pre-wrap;">' . $resultEntity->getOutputString() . '</div>';//htmlentities
            $comment = $resultEntity->getComment()? '<div style="color: #248b24;white-space: pre-wrap;"># ' .$resultEntity->getComment().'</div> ':'';
            $color = $resultEntity->isSuccess()?'transparent':'#890000';
            $body .= "<div style=\"background-color: $color\">$comment$command$outputString</div>";
        }
        $html = '<div style="color: #fff; background-color: #000; border: 1px solid #666; padding: 5px;font-family: monospace;">'.$body.'</div>';
        if($return){
            return $html;
        }
        echo $html;
        return $this;
    }

    protected function isWinOS()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }
}


