<?php

namespace Wt\Core\Support;

use Wt\Core\Collection\ErrorCollection;
use Wt\Core\IFields\ResponseFields;
use Wt\Core\Interfaces\IResponse;
use Wt\Core\Tools;

class Response extends Entity implements IResponse
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';
    const STATUS_WARNING = 'warning';

    protected $compress = false;

    public function __construct($data = [])
    {
        $data = array_merge([
            'httpCode' => 200,
            'message' => null,
            'status' => null,
            'data' => [],
            'errors' => [],
        ], $data);

        parent::__construct($data);
        if(is_array($this->rawData['data'])) {
            $this->rawData['data'] = new Collection($this->rawData['data']);
        } else {
            $this->rawData['data'] = new Collection();
        }
        if(is_array($this->rawData['errors'])) {
            $this->rawData['errors'] = new ErrorCollection($this->rawData['errors']);
        } else {
            $this->rawData['errors'] = new ErrorCollection();
        }
    }

    public function getMessage()
    {
        return $this->get('message', '');
    }

    public function setMessage($value)
    {
        $this->set('message', (string)$value);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        return $this->get('data');
    }

    public function addData($data)
    {
        $this->set('data', $this->getData()->merge($data));
        return $this;
    }

    public function setData($data)
    {
        $this->set('data', $this->getData()->clear()->merge($data));
        return $this;
    }

    /**
     * @return ErrorCollection
     */
    public function getErrors()
    {
        return $this->get('errors');
    }

    public function hasErrors()
    {
        return $this->getErrors()->count() > 0;
    }

    /**
     * Adds an array of errors to the collection.
     * @param Error|Error[]|iterable|string $data
     * @deprecated not use
     * @return $this
     */
    public function addError($data)
    {
        $this->getErrors()->add($data);
        return $this;
    }

    /**
     * Adds an array of errors to the collection.
     * @param Error|Error[]|iterable|string $data
     * @return $this
     */
    public function setError($data)
    {
        $this->getErrors()->add($data);
        return $this;
    }

    public function isSuccess()
    {
        if($this->getStatus()){
            return $this->getStatus() === static::STATUS_SUCCESS;
        }
        return $this->getErrors()->isEmpty();
    }

    public function getStatus()
    {
        return $this->get('status');
    }

    public function setStatus($value)
    {
        if( is_bool($value) ){
            $this->set('status', $value?static::STATUS_SUCCESS:static::STATUS_ERROR);
            return $this;
        }
        if( is_int($value) ){
            switch ($value){
                case 1:
                    $this->set('status', static::STATUS_SUCCESS);
                    break;
                case 0:
                    $this->set('status', static::STATUS_WARNING);
                    break;
                case -1:
                    $this->set('status', static::STATUS_ERROR);
                    break;
            }
            return $this;
        }
        $this->set('status', $value);
        return $this;
    }

    public function getHttpCode()
    {
        return $this->get('httpCode', 200);
    }

    /**
     * @param int $code
     * @return self
     */
    public function setHttpCode($code)
    {
        $this->set('httpCode', (int)$code);
        return $this;
    }

    /**
     * @param $message
     * @return self
     */
    public function setErrorMessage($message)
    {
        $this->setStatus(static::STATUS_ERROR);
        $this->setMessage($message);
        return $this;
    }

    public function setSuccessMessage($message)
    {
        $this->setStatus(static::STATUS_SUCCESS);
        $this->setMessage($message);
        return $this;
    }

    /**
     * @return ResponseFields
     */
    public function fields()
    {
        return parent::fields();
    }

    public function send()
    {
        Tools::sendJsonAnswer($this->getRaw(), $this->getHttpCode(), $this->compress);
    }

    /**
     * @param $message
     * @return Response
     * @deprecated not use
     */
    public function error($message)
    {
        return $this->setErrorMessage($message);
    }


    /**
     * @param $message
     * @return Response
     * @deprecated not use
     */
    public function success($message)
    {
        return $this->setSuccessMessage($message);
    }

    /**
     * @param bool $compress
     * @return Response
     */
    public function setCompress(bool $compress)
    {
        $this->compress = $compress;
        return $this;
    }
}