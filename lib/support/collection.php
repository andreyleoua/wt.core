<?php

namespace Wt\Core\Support;

/**
 * @template TKey of array-key
 *
 * @template-covariant TValue
 *
 * Base collection class
 */
class Collection extends IterableAccess
{
    protected $recursion = false;

    /**
     * Collection constructor.
     * @param iterable $array
     * @param bool $recursion
     * @throws null
     */
    public function __construct($array = [], $recursion = false)
    {
        if(!is_iterable($array)) {
            throw new \Exception('Collection must accept only iterators as value', 1);
        }

        if(($array instanceof \ArrayAccess) && method_exists($array, 'all')){
            $array = call_user_func_array([$array, 'all'], []);
        }
        if(is_object($array) && method_exists($array, 'toArray')){
            $array = call_user_func_array([$array, 'toArray'], []);
        }

        parent::__construct($array);

        $this->recursion = (bool)$recursion;
        if( $this->recursion ){
            foreach ($this->rawData as &$item){
                if( is_array($item) ){
                    $item = $this->newInstance($item);
                }
            }
        }
    }

    public function all()
    {
        return $this->rawData;
    }

    /**
     * @param array $array
     * @return $this
     * @throws null
     */
    protected function newInstance(array $array = [])
    {
        return resolve(static::class, func_get_args());
    }

    /**
     * @param array $array
     * @return Collection
     * @throws null
     */
    protected function newCollection(array $array = [])
    {
        return resolve(Collection::class, [$array]);
    }

    /**
     * @todo The value method retrieves a given value from the first element of the collection
     * @deprecated ERROR !!!
     * @return static
     */
    public function value()
    {
        $c = $this->newInstance();
        foreach ($this->all() as $value) {
            $c->push($value);
        }
        return $c;
    }

    protected function collectionToArray($collect)
    {
        if(is_array($collect)){
            return $collect;
        }
        $isArrayAccess = $collect instanceof \ArrayAccess;
        if($isArrayAccess && method_exists($collect, 'all')){
            return $collect->all();
        } elseif($isArrayAccess && method_exists($collect, 'getRaw')){
            return $collect->getRaw();
        } elseif(is_object($collect) && method_exists($collect, 'toArray')){
            return $collect->toArray();
        }
        return $collect;
    }

    /**
     * @param array|IterableAccess $array
     * @param array|IterableAccess $array2
     * @return $this
     */
    public function merge($array, ...$array2)
    {
        $_args = func_get_args();
        $args = [];
        foreach ($_args as $arg){
            $args[] = $this->collectionToArray($arg);
        }
        array_unshift($args, $this->all());
        $items = call_user_func_array('array_merge', $args);
        if( !is_array($items) ){
            $items = [];
        }

        return $this->newInstance($items);
    }

    /**
     * Исходный массив всегда неизменный
     * добавляются только ключи и их значения которых нет в исходном
     * числовые значения не инкриментятся и работают как строковые ключи
     *
     * @param $array
     * @return $this
     */
    public function plus($array)
    {
        return $this->newInstance($this->all() + $array);
    }

    /**
     * @return $this
     */
    public function lowerKeys()
    {
        return $this->newInstance(array_change_key_case($this->all(), CASE_LOWER));
    }

    /**
     * @return $this
     */
    public function upperKeys()
    {
        return $this->newInstance(array_change_key_case($this->all(), CASE_UPPER));
    }

    /**
     * Дополнить массив определённым значением до указанной длины
     *
     * @param $length
     * @param $value
     * @return Collection
     */
    public function pad($length, $value)
    {
        $items = array_pad($this->all(), $length, $value);

        return $this->newCollection($items);
    }

    /**
     * Возвращаяет указанный срез массива
     * Не изменяет исходную коллекцию
     *
     * @param $offset
     * @param $length
     * @param $preserve_keys
     * @return static
     */
    public function slice($offset, $length = null, $preserve_keys = true)
    {
        $items = array_slice($this->all(), $offset, $length, $preserve_keys);

        return $this->newInstance($items);
    }

    public function contains($value, $strict = false)
    {
        if(is_object($value) && is_callable($value)){
            return (bool)$this->filter($value)->count();
        }

        return in_array($value, $this->all(), $strict);
    }

    public function hasAnyValue($list, $strict = false)
    {
        $list = $this->newCollection($list);
        $res = false;
        $this->each(function($v, $k, $arr) use (&$list, &$res, $strict){
            if ($list->contains($v, $strict)) {
                $res = true;
                return false;
            }
            return true;
        });
        return $res;
    }

    public function hasAnyKey($list)
    {
        $list = $this->newCollection($list);
        $res = false;
        $this->each(function($v, $k, $arr) use (&$list, &$res){
            if ($list->contains($k, false)) {
                $res = true;
                return false;
            }
            return true;
        });
        return $res;
    }

    public function &eq($n)
    {
        $null = null;
        $n = (int)$n;
        $c = $this->count();
        if(!$c || ($n >= $c) || ($n < -$c) ) {
            return $null;
        }
        $i = ($n >= 0)?$n:($c+$n);

        $it = 0;
        foreach ($this->all() as $name => $value) {
            if($it === $i) return $this->offsetGet($name);
            ++$it;
        }
        return $null;
    }

    /**
     * @param $name
     * @param $default
     * @return mixed|null
     */
    public function &get($name, $default = null)
    {
        if( !$this->has($name) ){
            return $default;
        }

        return $this->offsetGet($name);
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function set($name, $value)
    {
        $this->offsetSet($name, $value);
        return $this;
    }

    public function has($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * удаляет из текущей коллекии по ключу, возвращает текущую коллекцию
     *
     * @param string|array $keys
     * @return $this
     */
    public function forget($keys)
    {
        foreach ($this->getArrayableItems($keys) as $key) {
            $this->offsetUnset($key);
        }

        return $this;
    }

    /**
     * удаляет из текущей коллекии по ключу, возвращает текущую коллекцию
     *
     * @param $key
     * @return $this
     */
    public function delete($key)
    {
        if( is_iterable($key) ){
            foreach ($key as $k) {
                $this->offsetUnset($k);
            }
            return $this;
        }

        $this->offsetUnset($key);
        return $this;
    }

    /**
     * удаляет из текущей коллекии по значению, возвращает текущую коллекцию
     *
     * @param $value
     * @return $this
     */
    public function remove($value)
    {
        $values = func_get_args();
        foreach ($this->all() as $k => $v){
            foreach ($values as $value){
                if($value == $v){
                    $this->offsetUnset($k);
                    break;
                }
            }
        }
        return $this;
    }


    /**
     * @param $key
     * @return $this
     */
    public function unset($key)
    {
        $this->offsetUnset($key);
        return $this;
    }

    public function setArray($ar, $saveIntKey = false)
    {
        $ar = $this->collectionToArray($ar);
        if($saveIntKey){
            $data = $this->all();
            foreach ($ar as $key => $value){
                $data[$key] = $value;
            }
            $this->setRaw($data);
            return $this;
        }
        $this->setRaw(array_merge($this->all(), $ar));
        return $this;
    }

    public function &first(callable $callback = null, $default = null)
    {
        if (is_null($callback)) {
            if (empty($this->all())) {
                $defaultValue = value($default);
                return $defaultValue;
            }


            foreach ($this->all() as $key => $item) {
                return $this->offsetGet($key);
            }
        }

        foreach ($this->all() as $key => $value) {
            if ($callback($value, $key)) {
                return $value;
            }
        }

        $defaultValue = value($default);
        return $defaultValue;
    }

    public function &last()
    {
        if (empty($this->rawData)) {
            $null = null;
            return $null;
        }
        end($this->rawData);

        return $this->offsetGet(key($this->rawData));
    }

    /**
     * @param $value
     * @return $this
     */
    public function push($value)
    {
        $this->offsetSet(null, $value);
        return $this;
    }

    /**
     * извлекает и возвращает значение последнего элемента collection, уменьшая размер collection на один элемент
     * Не изменяет значения ключей в исходной коллекции!
     * @return mixed
     */
    public function pop()
    {
        $i = array_pop($this->rawData);
        $this->onChangeConsistentState();
        return $i;
    }

    /**
     * извлекает первое значение collection и возвращает его, сокращая размер array на один элемент.
     * Не изменяет значения ключей в исходной коллекции!
     * @return mixed
     */
    public function shift()
    {
        if (empty($this->rawData)) return null;
        reset($this->rawData);
        $r = $this->rawData[key($this->rawData)];
        $this->offsetUnset(key($this->rawData));
        //$r = array_shift($this->collection);
        $this->onChangeConsistentState();
        return $r;
    }

    public function put($key, $value)
    {
        $this->offsetSet($key, $value);
    }

    public function pull($key, $default = null)
    {
        if( $this->offsetExists($key) ){
            $v = $this->rawData[$key];
            $this->offsetUnset($key);
            return $v;
        }

        return $default;
    }

    /**
     * Добавляет в начало коллекции значение. Может быть передан ключ
     * Не изменяет значения ключей в исходной коллекции!
     *
     * @param $value
     * @param $key
     * @return $this
     */
    public function prepend($value, $key = null)
    {
        if(is_null($key)){
            if(isset($this->rawData[0])){
                array_unshift($this->rawData, $value);
            } else {
                $this->rawData = [$value] + $this->rawData;
            }
        } else {
            $this->rawData = [$key => $value] + $this->rawData;
        }
        /**
         * добавляет переданные в качестве аргументов элементы в начало массива array.
         * Обратите внимание, что список элементов добавляется целиком, то есть порядок элементов сохраняется.
         * Все числовые ключи будут изменены таким образом, что нумерация массива будет начинаться с нуля,
         * в то время как строковые ключи останутся прежними.
         */
        //array_unshift($this->rawData, $value);
        $this->onChangeConsistentState();
        return $this;
    }

    /**
     * Search the collection for a given value and return the corresponding key if successful.
     *
     * @param  TValue|(callable(TValue,TKey): bool)  $value
     * @param  bool  $strict
     * @return TKey|false
     */
    public function search($value, $strict = false)
    {
        if (! $this->useAsCallable($value)) {
            return array_search($value, $this->rawData, $strict);
        }

        foreach ($this->rawData as $key => $item) {
            if ($value($item, $key)) {
                return $key;
            }
        }

        return false;
    }

    /**
     * Splice a portion of the underlying collection array.
     * Заменяет указанный срез на значения из $replacement
     * Не сохранят ключи $replacement
     * Возвращает удаленный массив
     *
     * @param  int  $offset
     * @param  int|null  $length
     * @param  mixed  $replacement
     * @return static
     */
    public function splice($offset, $length = null, $replacement = [])
    {
        if (func_num_args() == 1) {
            return $this->newInstance(array_splice($this->rawData, $offset));
        }

        return $this->newInstance(array_splice($this->rawData, $offset, $length, $replacement));
    }

    /**
     * https://wp-kama.ru/note/add-array-element-to-position
     * ассоциативный массив!
     * Не меняет текущюю коллекцию
     *
     * @param $afterKey
     * @param $array
     * @return $this|Collection
     */
    public function insertAfterKey($afterKey, $array)
    {
        $items = $this->all();
        $index = array_search( $afterKey, array_keys( $items ) );
        if($index === false){
            return $this->newInstance($items + $array);
        }
        return $this->newInstance(array_slice( $items, 0, $index + 1 ) + $array + $items);
    }

    /**
     * @param callable|null $callback
     * @return $this
     */
    public function filter($callback = null)
    {
        if(is_null($callback)){
            $callback = function ($value){
                return !empty($value);
            };
        }
        $arr = array_filter($this->rawData, $callback, ARRAY_FILTER_USE_BOTH);

        return $this->newInstance($arr);
    }

    /**
     * Создает новую коллекцию без значений удовлетворяющих колбеку
     *
     * @param callable $callback
     * @return $this
     */
    public function reject($callback = null)
    {
        if(is_null($callback)){
            $callback = function ($value){
                return !empty($value);
            };
        }

        $arr = array_filter($this->rawData, function ($value, $key) use ($callback){
            return !call_user_func_array($callback, func_get_args());
        }, ARRAY_FILTER_USE_BOTH);
        return $this->newInstance($arr);
    }

    /**
     * Determine if the given value is callable, but not a string.
     *
     * @param  mixed  $value
     * @return bool
     */
    protected function useAsCallable($value)
    {
        return ! is_string($value) && is_callable($value);
    }

    /**
     * Get an operator checker callback.
     *
     * @param  callable|string  $key
     * @param  string|null  $operator
     * @param  mixed  $value
     * @return \Closure
     */
    protected function operatorForWhere($key, $operator = null, $value = null)
    {
        if ($this->useAsCallable($key)) {
            return $key;
        }

        if (func_num_args() === 1) {
            $value = true;

            $operator = '=';
        }

        if (func_num_args() === 2) {
            $value = $operator;

            $operator = '=';
        }

        return function ($item) use ($key, $operator, $value) {
            $retrieved = data_get($item, $key);

            $strings = array_filter([$retrieved, $value], function ($value) {
                return is_string($value) || (is_object($value) && method_exists($value, '__toString'));
            });

            if (count($strings) < 2 && count(array_filter([$retrieved, $value], 'is_object')) == 1) {
                return in_array($operator, ['!=', '<>', '!==']);
            }

            switch ($operator) {
                default:
                case '=':
                case '==':  return $retrieved == $value;
                case '!=':
                case '<>':  return $retrieved != $value;
                case '<':   return $retrieved < $value;
                case '>':   return $retrieved > $value;
                case '<=':  return $retrieved <= $value;
                case '>=':  return $retrieved >= $value;
                case '===': return $retrieved === $value;
                case '!==': return $retrieved !== $value;
                case '<=>': return $retrieved <=> $value;
            }
        };
    }

    /**
     * Filter items by the given key value pair.
     *
     * @param  callable|string  $key
     * @param  mixed  $operator
     * @param  mixed  $value
     * @return static
     */
    public function where($key, $operator = null, $value = null)
    {
        return $this->filter($this->operatorForWhere(...func_get_args()));
    }

    /**
     * Get a value retrieving callback.
     *
     * @param  callable|string|null  $value
     * @return callable
     */
    protected function valueRetriever($value)
    {
        if(is_null($value)){
            return function ($item, $key){
                return $item;
            };
        }
        if ($this->useAsCallable($value)) {
            return $value;
        }
        return function ($item) use ($value) {
            return is_iterable($item)?$item[$value]:null;
        };
    }

    /**
     * Return only unique items from the collection array.
     *
     * @param  (callable(TValue, TKey): mixed)|string|null  $key
     * @param  bool  $strict
     * @return static
     */
    public function unique($key = null, $strict = false)
    {
        $callback = $this->valueRetriever($key);

        $exists = [];

        return $this->reject(function ($item, $key) use ($callback, $strict, &$exists) {
            if (in_array($id = $callback($item, $key), $exists, $strict)) {
                return true;
            }

            $exists[] = $id;
            return false;
        });
    }

    /**
     * Return only unique items from the collection array using strict comparison.
     *
     * @param  (callable(TValue, TKey): mixed)|string|null  $key
     * @return static
     */
    public function uniqueStrict($key = null)
    {
        return $this->unique($key, true);
    }

    /**
     * @param callable|string|int $key
     * @return $this
     */
    public function keyBy($key)
    {
        $collection = $this->newInstance();

        if( in_array(gettype($key), ['object', 'array']) && is_callable($key)) {
            foreach ($this as $k => $item) {
                $collection->put(call_user_func($key, $item, $k), $item);
            }
            return $collection;
        }

        foreach ($this as $item) {
            if( is_array($item) ) {
                $collection->put($item[$key], $item);
            }
            elseif( is_object($item) ) {
                $collection->put($item->{$key}, $item);
            }
        }

        return $collection;
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function mapWithKeys($callback)
    {
        $collection = $this->newInstance();

        if(!is_callable($callback)) {
            return $collection;
        }

        $res = [];
        foreach ($this->rawData as $key => $item) {
            $data = call_user_func($callback, $item, $key);
            if(is_array($data)) {
                $res[] = $data;
            }
        }

        $collection = call_user_func_array([$collection, 'merge'], $res);

        return $collection;
    }


    /**
     * The only method returns the items in the collection with the specified keys
     * @param $keys
     * @return $this
     */
    public function only($keys)
    {
        $collection = $this->newInstance();

        $keys = (array)$keys;

        foreach ($keys as $key) {
            if( $this->has($key) ){
                $collection->set($key, $this->get($key));
            }
        }

        return $collection;
    }

    /**
     * @return Collection
     */
    public function keys()
    {
        return $this->newCollection(array_keys($this->rawData));
    }

    /**
     * @return $this
     */
    public function values()
    {
        return $this->newInstance(array_values($this->rawData));
    }

    public function implode($glue = '')
    {
        return implode($glue, $this->all());
    }

    /**
     * @param int $flag
     * @return $this
     */
    public function sort($flag = SORT_REGULAR)
    {
        $c = $this->all();
        asort($c, $flag);
        return $this->newInstance($c);
    }

    /**
     * @param int $flag
     * @return $this
     */
    public function sortDesc($flag = SORT_REGULAR)
    {
        $c = $this->rawData;
        arsort($c, $flag);
        return $this->newInstance($c);
    }

    /**
     * @param $key
     * @param string $type
     * @return $this
     */
    public function sortBy($key, $type = 'asc')
    {
        $type = strtolower($type);
        if( $type != 'desc' ){
            $type = 'asc';
        }
        $c = $this->all();
        uasort($c, function($a, $b) use ( $key, $type )
        {
            if( is_array($a) ){
                $_a = $a[$key];
                $_b = $b[$key];
            }elseif( is_object($a) ){
                $_a = $a->$key;
                $_b = $b->$key;
            }

            if ($_a == $_b) {
                return 0;
            }

            if( $type == 'desc' ){
                return ($_a < $_b) ? 1 : -1;
            }

            return ($_a < $_b) ? -1 : 1;
        });

        return $this->newInstance($c);
    }

    /**
     * @param callable $callback
     * @return Collection
     * @throws null
     */
    public function map($callback)
    {
        if( !is_callable($callback) ){
            throw new \Exception('callback is not valid');
        }

        $numberOfParametrs = 2;
        if($callback instanceof \Closure || is_string($callback)){
            $r = new \ReflectionFunction($callback);
            $numberOfParametrs = $r->getNumberOfParameters();
        }

        $c = $this->newCollection();

        foreach ($this->all() as $key => $value) {
            $args = [$value, $key];
            if($numberOfParametrs == 1){
                $args = [$value];
            } elseif($numberOfParametrs == 0){
                $args = [];
            }
            $data = call_user_func_array($callback, $args);
            $c[$key] = $data;
        }

        return $c;
    }

    /**
     * @param callable $callback
     * @return Collection
     * @throws null
     */
    public function obMap($callback)
    {
        $c = $this->newCollection();

        foreach ($this->all() as $key => $value) {
            if(defined('BX_BUFFER_SHUTDOWN') && BX_BUFFER_SHUTDOWN === true) {
                //throw new \Exception('bitrix buffer shutdown');
                $c[$key] = '';
                continue;
            }
            ob_start();
            $data = call_user_func($callback, $value, $key);
            $content = ob_get_clean();
            $c[$key] = $content;
        }

        return $c;
    }

    /**
     * @param callable $callback
     * @return $this
     * @throws null
     */
    public function each($callback)
    {
        if( !is_callable($callback) ){
            throw new \Exception('callback is not valid');
        }

        foreach ($this->all() as $key => $value) {
            $res = call_user_func($callback, $value, $key);
            if($res === false){
                break;
            }
        }

        return $this;
    }

    /**
     * Reduce the collection to a single value.
     *
     * @template TReduceInitial
     * @template TReduceReturnType
     *
     * @param  callable(TReduceInitial|TReduceReturnType, TValue, TKey): TReduceReturnType  $callback
     * @param  TReduceInitial  $initial
     * @return TReduceInitial|TReduceReturnType
     */
    public function reduce(callable $callback, $initial = null)
    {
        $result = $initial;

        foreach ($this as $key => $value) {
            $result = $callback($result, $value, $key);
        }

        return $result;
    }

    /**
     * Flip the items in the collection.
     *
     * @return static<TValue, TKey>
     */
    public function flip()
    {
        return $this->newCollection(array_flip($this->all()));
    }

    /**
     * @param $name
     * @param null $indexKey
     * @return Collection
     */
    public function pluck($name, $indexKey = null)
    {
        $res = array_column($this->rawData, $name, $indexKey);
        return $this->newCollection($res);
    }

    /**
     * @return $this
     */
    public function reverse()
    {
        return $this->newInstance(array_reverse($this->all()));
    }

    /**
     * Изменяет значения коллекции
     * Возвращает туже коллекцию с измененными значениями
     * @param $callback callable f(&$value, $key, $arg3) может быть без ссылочного значения аргумента $value
     * @param null $arg3
     * @return $this
     */
    public function walk($callback, $arg3 = null)
    {
        $callbackWithLink = function (&$value, $key, $arg3 = null) use($callback) {
            if(is_null($arg3)){
                $r = call_user_func_array($callback, [&$value, $key, $arg3]);
            } else {
                $r = call_user_func_array($callback, [&$value, $key]);
            }
            if(!is_null($r)){
                $value = $r;
            }
        };
        array_walk($this->rawData, $callbackWithLink, $arg3);
        return $this;
    }

    /**
     * Join all items using a string. The final items can use a separate glue string.
     *
     * @param  string  $glue
     * @param  string  $finalGlue
     * @return string
     */
    public function join($glue, $finalGlue = '')
    {
        $array = $this->all();

        if ($finalGlue === '') {
            return implode($glue, $array);
        }

        if (count($array) === 0) {
            return '';
        }

        if (count($array) === 1) {
            return end($array);
        }

        $finalItem = array_pop($array);

        return implode($glue, $array).$finalGlue.$finalItem;
    }

    protected function getArrayableItems($items)
    {
        if (is_array($items)) {
            return $items;
        }
        
        switch (true) {
            case $items instanceof Entity:
                return $items->getRaw();
            case $items instanceof Collection:
                return $items->all();
            case $items instanceof \Traversable:
                return iterator_to_array($items);
            case $items instanceof \JsonSerializable:
                return (array)$items->jsonSerialize();
            default:
                return (array)$items;
        }
    }

    /**
     * Метод diff сравнивает коллекцию с другой коллекцией или простым массивом PHP на основе его значений.
     * Этот метод вернет значения из исходной коллекции, которых нет в переданной коллекции
     *
     * @param  $items
     * @return static
     */
    public function diff($items)
    {
        return $this->newInstance(array_diff($this->all(), $this->getArrayableItems($items)));
    }

    /**
     * Get the items in the collection that are not present in the given items, using the callback.
     *
     * @param  $items
     * @param  callable(TValue, TValue): int  $callback
     * @return static
     */
    public function diffUsing($items, callable $callback)
    {
        return $this->newInstance(array_udiff($this->all(), $this->getArrayableItems($items), $callback));
    }

    /**
     * Метод diffAssoc сравнивает коллекцию с другой коллекцией или простым массивом PHP на основе его ключей и значений.
     * Этот метод вернет пары ключ / значение из исходной коллекции, которых нет в переданной коллекции
     *
     * @param  $items
     * @return static
     */
    public function diffAssoc($items)
    {
        return $this->newInstance(array_diff_assoc($this->all(), $this->getArrayableItems($items)));
    }

    /**
     * В отличие от diffAssoc, diffAssocUsing принимает пользовательскую функцию обратного вызова для сравнения индексов
     *
     * @param  $items
     * @param  callable(TKey, TKey): int  $callback
     * @return static
     */
    public function diffAssocUsing($items, callable $callback)
    {
        return $this->newInstance(array_diff_uassoc($this->all(), $this->getArrayableItems($items), $callback));
    }

    /**
     * Метод diffKeys сравнивает коллекцию с другой коллекцией или простым массивом PHP на основе его ключей.
     * Этот метод вернет пары ключ / значение из исходной коллекции, которых нет в переданной коллекции
     *
     * @param  $items
     * @return static
     */
    public function diffKeys($items)
    {
        return $this->newInstance(array_diff_key($this->all(), $this->getArrayableItems($items)));
    }

    /**
     * @param bool $raw
     * @return $this
     */
    public function pre($raw = true)
    {
        pre($raw?$this->getRaw():$this);
        return $this;
    }
}