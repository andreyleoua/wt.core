<?php


namespace Wt\Core\Support;


class FileLocker
{
    protected $lockFileName = '';
    protected $lockFile = '';
    protected $lockExt = '.lock';
    protected $lockSemaphoreExt = '.s.lock';

    protected $dirPath = '';

    private $fileRes;
    private $lockData;

    /**
     * @var resource
     */
    protected $stream;

    /**
     * CronLocker constructor.
     * @param string $scriptName | full path to script or key name
     */
    public function __construct($scriptName = '')
    {
        if( !$scriptName ){
            $scriptName = get_included_files()[0];
        }
        $pathInfo = (object)pathinfo($scriptName);
        $this->lockFileName = $pathInfo->filename;
        $this->lockFile = $this->lockFileName.$this->lockExt;

        if($pathInfo->dirname != '.'){
            $this->dirPath = $this->setDirPath($pathInfo->dirname);
        } else {
            $scriptName = get_included_files()[0];
            $pathInfo = (object)pathinfo($scriptName);
            $this->dirPath = $this->setDirPath($pathInfo->dirname);
        }
    }

    public function setDirPath($dirPath)
    {
        if( $this->dirPath && $this->isLocked() ){
            return false;
        }

        if( !preg_match("~/$~", $dirPath) ){
            $dirPath .= '/';
        }

        if( is_dir($dirPath) ){
            $this->dirPath = $dirPath;
        }

        if( $this->fileRes ) {
            fclose($this->fileRes);
            $this->fileRes = null;
        }

        return $this->dirPath;
    }

    public function getLockFilePath()
    {
        return $this->dirPath.$this->lockFile;
    }

    protected function getPathForFile($fileName)
    {
        return $this->dirPath.$fileName;
    }

    public function lock()
    {
        $this->initFileRes();

        if ( $this->fileRes === false ) {
            return false;
        }

        if ( flock($this->fileRes, LOCK_EX | LOCK_NB) ) {
            ftruncate($this->fileRes, 0);
            rewind($this->fileRes);
            fwrite($this->fileRes, $this->getPid().' '.microtime(true));
            return true;
        }

        return false;
    }

    public function isLocked()
    {
        $this->initFileRes();

        if ( !$this->fileRes ) {
            return false;
        }

        // Script already running - abort
        if(!flock($this->fileRes, LOCK_EX | LOCK_NB)){
            flock($this->fileRes, LOCK_UN);
            return true;
        }

        return false;
    }

    public function unlock()
    {
        if( !$this->isLocked() && $this->fileRes ){
            $this->lockData = null;
            flock($this->fileRes, LOCK_UN);
            $this->unlink();
            return true;
        }

        return false;
    }

    public function unlink()
    {
        if(
            file_exists($this->getLockFilePath()) &&
            is_writable($this->getLockFilePath())
        ){
            if( $this->fileRes ){
                fclose($this->fileRes);
                $this->fileRes = null;
            }
            unlink($this->getLockFilePath());
        }
    }

    public function lockByFile($fileName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $fileName ){
            $filePath = $this->getPathForFile($fileName);
        }

        $fp = fopen($filePath, 'w+');
        fclose($fp);
    }

    public function isLockedByFile($fileName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $fileName ){
            $filePath = $this->getPathForFile($fileName);
        }
        return file_exists($filePath);
    }

    public function unlockByFile($fileName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $fileName ){
            $filePath = $this->getPathForFile($fileName);
        }
        if( $this->isLockedByFile($fileName) ){
            unlink($filePath);
        }
    }

    /**
     * without problem TOCTOU
     */
    public function lockByDirectory($dirName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $dirName ){
            $filePath = $this->getPathForFile($dirName);
        }
        mkdir($filePath, 0700);
    }

    public function isLockedByDirectory($dirName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $dirName ){
            $filePath = $this->getPathForFile($dirName);
        }
        return is_dir($filePath);
    }

    public function unlockByDirectory($dirName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $dirName ){
            $filePath = $this->getPathForFile($dirName);
        }
        if( $this->isLockedByDirectory($dirName) ){
            rmdir($filePath);
        }
    }

    public function lockBySemaphore($filename = '')
    {
        $filePath = $this->getLockFilePath();
        if( $filename ){
            $filePath = $this->getPathForFile($fileName);
        }
        $filePath = str_replace($this->lockExt, $this->lockSemaphoreExt, $filePath);

        $key = ftok($filePath, 'L');
        $semaphore = sem_get($key, 1);

        if (sem_acquire($semaphore, 1) !== false) {
            //sem_release($semaphore);
        }
    }

    public function isLockedBySemaphore($fileName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $fileName ){
            $filePath = $this->getPathForFile($fileName);
        }
        $filePath = str_replace($this->lockExt, $this->lockSemaphoreExt, $filePath);

        $key = ftok($filePath, 'L');
        $semaphore = sem_get($key, 1);

        return (sem_acquire($semaphore, 1) === false);
    }

    public function unlockBySemaphore($fileName = '')
    {
        $filePath = $this->getLockFilePath();
        if( $fileName ){
            $filePath = $this->getPathForFile($fileName);
        }
        $filePath = str_replace($this->lockExt, $this->lockSemaphoreExt, $filePath);

        $key = ftok($filePath, 'L');
        $semaphore = sem_get($key, 1);
        sem_release($semaphore);
    }

    public function isCli()
    {
        return \Wt\Core\Tools::isCli();
    }

    public function getPid()
    {
        return getmypid();
    }

    public function getLockData()
    {
        $arRes = [
            'pid' => 0,
            'timestamp_lock' => 0,
            'microtime_lock' => 0
        ];
        $this->initFileRes();
        if( $this->fileRes && !$this->lockData ){
            $data = fgets($this->fileRes, 1024);
            $data = trim($data);
            if( $data ){
                list($pid, $microtime) = explode(' ', $data);
                $arRes['pid'] = $pid;
                $arRes['timestamp_lock'] = floor($microtime);
                $arRes['microtime_lock'] = $microtime;

                $this->lockData = $arRes;
            }
        }
        if( $this->lockData ){
            $arRes = $this->lockData;
        }

        return $arRes;
    }

    public function getLockPid()
    {
        return $this->getLockData()['pid'];
    }

    public function getTimeExecute()
    {
        return time() - $this->getLockData()['timestamp_lock'];
    }

    public function getMicrotimeExecution()
    {
        $t = $this->getLockData();
        return round(microtime(true) - $t['microtime_lock'], 4);
    }

    public function checkPidExecuting()
    {
        if( $this->isLocked() && $this->getLockPid() && $this->getPid() != $this->getLockPid() ){
            return shell_exec("ps aux | grep " . $this->getLockPid() . " | wc -l") > 2;
        }
    }

    protected function initFileRes()
    {
        if( $this->fileRes !== null ) {
            return $this->fileRes;
        }

        $mode = "r+";
        if( !file_exists($this->getLockFilePath()) ){
            $mode = "w+";
        }

        $this->fileRes = fopen($this->getLockFilePath(), $mode);

        return $this->fileRes;
    }

    public function __destruct()
    {
        $this->unlock();
    }
}