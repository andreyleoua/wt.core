<?php


namespace Wt\Core\Support;


class Mapper
{
    protected $map = [];

    public function put($key, $value)
    {
        $this->map[$key] = $value;
    }

    public function add($key, $value = null)
    {
        if( $value && !is_array($key) ){
            $this->put($key, (string)$value);
            return;
        }

        if( is_array($key) ){
            $this->extendFromArray($key);
        }
    }

    public function set($key, $value)
    {
        $this->put($key, $value);
    }

    public function get($key, $default = null)
    {
        return $this->getByKey($key, $default);
    }

    public function getByKey($key, $default = null)
    {
        if( isset($this->map[$key]) || array_key_exists($key, $this->map) ){
            return $this->map[$key];
        }

        return $default;
    }

    public function getByValue($value, $default = null)
    {
        if( ($idx = array_search($value, $this->map)) !== false ){
            return $idx;
        }

        return $default;
    }

    public function findInLeft($value, $default = null)
    {
        return $this->getByKey($value, $default);
    }

    public function findInRight($value, $default = null)
    {
        return $this->getByValue($value, $default);
    }

    public function keys()
    {
        return array_keys($this->map);
    }

    public function getLeft()
    {
        return $this->keys();
    }

    public function values()
    {
        return array_values($this->map);
    }

    public function getRight()
    {
        return $this->values();
    }

    public function remove($key)
    {
        unset($this->map[$key]);
    }

    public function mapFromArray(array $arData)
    {
        if( $arData ){
            $this->map = $arData;
        }
    }

    public function fromArray(array $arData)
    {
        $this->mapFromArray($arData);
    }

    public function extendFromArray(array $arData)
    {
        foreach ($arData as $k => $v){
            $this->put($k, $v);
        }
    }

    public function setMap(array $map)
    {
        $this->map = $map;
    }

    public function toArray()
    {
        return $this->map;
    }

    public function all()
    {
        return $this->map;
    }

    public function toArrayFill()
    {
        return array_combine($this->toArray(), $this->keys());
    }

    public function toFillArray()
    {
        return $this->toArrayFill();
    }

    public function fill()
    {
        $m = new static();
        $m->mapFromArray($this->toArrayFill());
        return $m;
    }

    public function flip()
    {
        return $this->fill();
    }

    public function clear()
    {
        $this->map = [];
    }
}