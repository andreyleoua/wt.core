<?php

namespace Wt\Core\Support;

use Wt\Core\Interfaces\ILexer;
use Wt\Core\Interfaces\ILexerParser;

class LexerCollection extends Collection implements ILexerParser, ILexer
{
    public function __construct(array $array = [], $recursion = true)
    {
        parent::__construct($array, $recursion);
    }

	public function lexerCallNext($currentService, $next, $args = [])
    {
		if($currentService->has($next)){
            return $currentService->offsetGet($next);
		}

		return null;
	}

    /**
     * @param string $name
     * @param null $default
     * @return mixed|null|$this
     */
	public function &get($name = '', $default = null)
    {
        return $this->lexer($name, $default);
    }

	public function &lexer($path, $default = null)
    {
        $lexer = new Lexer;
        $lexer->setDelimiter('.');

        $res = $lexer->callNextStr($path, $this);

        if( $res === null && $default !== null ){
            if( is_array($default) ){
                $default = new $this($default);
            }
            return $default;
        }

        return $res;
    }

    public function &__get($name)
    {
        return $this->get($name);
    }
}