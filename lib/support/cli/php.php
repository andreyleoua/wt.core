<?php

namespace Wt\Core\Support\Cli;

use Wt\Core\Support\Cli;

class Php
{
    protected $docRoot;
    protected $phpPath = 'php';
    /** @var Cli */
    protected $cli;

    public function __construct(Cli $cli, $phpPath = null, $docRoot = null)
    {
        if(!is_null($phpPath)){
            $this->phpPath = $phpPath;
        }
        if(!is_null($docRoot)){
            $this->docRoot = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $docRoot);
        }
        $this->cli = $cli;
    }

    public function codeExec($code)
    {
        $attr = [];
        $attr['code'] = "-r \"{$code}\"";
        $command = $this->getPhpPath() . ' ' . implode(' ', $attr);
        return $this->getCli()->exec($command);
    }

    public function getExtensions()
    {
        return explode(',', $this->codeExec("echo implode(',',(array)@get_loaded_extensions());")->getOutputString());
    }

    public function getIniAll($extension = null, $details = true)
    {
        $json = $this->codeExec(
            'echo json_encode(ini_get_all(' . var_export($extension, true) . ', ' . var_export($details, true) . '));'
        )->getOutputString();
        return json_decode($json, true);
    }

    public function fileExec($file, $data = null)
    {
        $attr = [];
        if(!is_null($this->getDocRoot())){
            $attr['docRoot'] = "-B \"\$_SERVER['DOCUMENT_ROOT'] = '{$this->getDocRoot()}';\"";
        }
        $attr['file'] = "-F \"{$file}\"";

        $command = $this->getPhpPath() . ' ' . implode(' ', $attr);
        return $this->getCli()->exec($command);
    }

    public function optionExec($optionString)
    {
        $command = $this->getPhpPath() . ' ' . $optionString;
        return $this->getCli()->exec($command);
    }

    /**
     * @return string|null
     */
    public function getPhpPath()
    {
        return $this->phpPath;
    }

    public function getDocRoot()
    {
        return $this->docRoot;
    }

    /**
     * @return Cli
     */
    public function getCli(): Cli
    {
        return $this->cli;
    }
}


