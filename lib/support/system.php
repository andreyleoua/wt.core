<?php

namespace Wt\Core\Support;

use Wt\Core\Entity\CliExecResultEntity;

class System
{
    public function __construct()
    {
    }

    public function getDocumentRoot()
    {
        return $_SERVER['DOCUMENT_ROOT'] ?: realpath(__DIR__ . '/../../../../..');
    }

    public function getHomeDir()
    {
        $result = $_SERVER['HOME'] ?? getenv('HOME');

        if($result){
            return $result;
        }

        if($this->isWinOS()) {
            $result = exec('echo %userprofile%');
        } else {
            $result = exec('echo ~');
        }

        return $result;
    }

    /**
     *    Get path to php binary
     * @return false|string
     */
    public function getPhpPath()
    {
        $strPhpPath = 'php';
        if ($this->isUnixOS()) {
            exec('which php', $strResult);
            if (is_array($strResult) && !empty($strResult[0])) {
                $strPhpPath = $strResult[0];
            }
        }
        return app()->config()->get('php_path', $strPhpPath);
    }

    public function getPhpScannedIniFiles()
    {
        return array_map('trim', explode(',', php_ini_scanned_files()));
    }

    public function getPhpLoadedIniFile()
    {
        return php_ini_loaded_file();
    }

    public function getPhpSApiName()
    {
        return php_sapi_name();
    }

    /**
     *    Check if crontab can be managed by this script (only if Linux OS)
     */
    public function crontabInstalled()
    {
        if (!$this->isUnixOS()) {
            return false;
        }
        exec('which crontab', $arExec);
        if (!is_array($arExec) || empty($arExec[0])) {
            return false;
        }
        return true;
    }

    public function gitInstalled()
    {
        exec('git --version', $arExec);
        return is_array($arExec) && stripos($arExec[0], 'git version') !== false;
    }

    public function getGitVersion()
    {
        exec('git --version', $arExec);
        if (is_array($arExec) && stripos($arExec[0], 'git version') !== false) {
            return trim(str_ireplace('git version', '', $arExec[0]));
        }
        return null;
    }

    public function msmtpInstalled()
    {
        $shell = new Cli();
        return $shell->exec('msmtp --version')->isSuccess();
    }

    public function getMsmtpVersion()
    {
        $shell = new Cli();
        $str = $shell->exec('msmtp --version')->getOutputString();
        if (preg_match('/version\s+(?<version>.*)$/m', $str, $matches)) {
            return $matches[1];
        }
        return null;
    }

    public function mysqlInstalled()
    {
        $shell = new Cli();
        $r = $shell->exec('mysql --version');
        return $r->isSuccess();
    }

    public function getMysqlVersion()
    {
        $shell = new Cli();
        $r = $shell->exec('mysql --version');
        return $r->getOutputString();
    }

    public function isWinOS()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    public function isUnixOS()
    {
        return stripos(PHP_OS, 'linux') !== false;
    }

    public function getPhpVersion()
    {
        return phpversion();
    }

    public function getPhpExtensions()
    {
        return get_loaded_extensions();
    }

    public function getPhpIniAll($extension = null, $details = true)
    {
        return ini_get_all($extension, $details);
    }

    /**
     * Return BitrixVM version
     *
     * @return string
     */
    public function getBitrixVMVersion()
    {
        $result = getenv('BITRIX_VA_VER');
        if (!$result)
            $result = '';
        return $result;
    }

    public function getOS()
    {
        return constant('PHP_OS');
    }

    public function getCurlVersion()
    {
        if (function_exists('curl_version')) {
            $curl = curl_version();
            return $curl['version'];
        }
        return null;
    }

    public function getTlsVersion()
    {
        $httpClient = new \Bitrix\Main\Web\HttpClient();
        $data = $httpClient->get('https://www.howsmyssl.com/a/check');
        $json = json_decode($data);
        if (is_object($json)) {
            return $json->tls_version;
        }
        return null;
    }

    public function getOpenSSLVersionText()
    {
        return constant('OPENSSL_VERSION_TEXT');
    }

    public function getOpenSSLVersionNumber()
    {
        return constant('OPENSSL_VERSION_NUMBER');
    }

    /**
     *
     * 'a': По умолчанию. Содержит все режимы в следующей последовательности "s n r v m".
     * 's': Название операционной системы, например, FreeBSD.
     * 'n': Имя хоста, например, localhost.example.com.
     * 'r': Номер релиза, например, 5.1.2-RELEASE.
     * 'v': Информация о версии. Может сильно различаться в разных ОС.
     * 'm': Архитектура процессора, например, i386.
     *
     * @param null $mode
     * @return string
     */
    public function uName($mode = null)
    {
        return php_uname($mode);
    }

    public function whoAmI($mode = null)
    {
        if (!$this->isUnixOS()) {
            return false;
        }
        exec('whoami', $arExec);
        if (!is_array($arExec) || empty($arExec[0])) {
            return false;
        }
        return $arExec[0];
    }

    /**
     * @see https://habr.com/ru/post/316806/
     *
     * @param int $niceness Любезность [-20, 19]
     * @param null $pid
     * @return CliExecResultEntity
     */
    public function setNice($niceness, $pid = null)
    {
        if (is_null($pid)) {
            $pid = getmypid();
        }
        $shell = new Cli();
        return $shell->exec("renice -n $niceness -p $pid");
    }

    /**
     * @see https://losst.ru/kak-osvobodit-pamyat-linux
     *
     * 1 - Очистка кэша PageCache
     * 2 - Очистка inode и dentrie
     * 3 - Очистка inode и dentrie и PageCache
     *
     * @param $lvl
     * @return CliExecResultEntity
     */
    public function vmDropCaches($lvl)
    {
        $shell = new Cli();
        return $shell->exec("sync; echo $lvl > /proc/sys/vm/drop_caches");
    }

    /**
     *
     * Значение в байтах
     *
     *
     * [MemTotal] => 16471908352
     * [MemFree] => 160333824
     * [MemAvailable] => 733872128
     * [Buffers] => 61440
     * [Cached] => 2723340288
     * [SwapCached] => 2043904
     * [Active] => 12666650624
     * [Inactive] => 2135912448
     * [Active(anon)] => 12286550016
     * [Inactive(anon)] => 1768091648
     * [Active(file)] => 380100608
     * [Inactive(file)] => 367820800
     * [Unevictable] => 0
     * [Mlocked] => 0
     * [SwapTotal] => 2144333824
     * [SwapFree] => 0
     * [Dirty] => 13619200
     * [Writeback] => 0
     * [AnonPages] => 12079185920
     * [Mapped] => 1587007488
     * [Shmem] => 1974853632
     * [Slab] => 310034432
     * [SReclaimable] => 175693824
     * [SUnreclaim] => 134340608
     * [KernelStack] => 5488640
     * [PageTables] => 272855040
     * [NFS_Unstable] => 0
     * [Bounce] => 0
     * [WritebackTmp] => 0
     * [CommitLimit] => 10380288000
     * [Committed_AS] => 20727734272
     * [VmallocTotal] => 35184372087808
     * [VmallocUsed] => 309354496
     * [VmallocChunk] => 35183562584064
     * [Percpu] => 1441792
     * [HardwareCorrupted] => 0
     * [AnonHugePages] => 903872512
     * [CmaTotal] => 0
     * [CmaFree] => 0
     * [HugePages_Total] => 0
     * [HugePages_Free] => 0
     * [HugePages_Rsvd] => 0
     * [HugePages_Surp] => 0
     * [Hugepagesize] => 2097152
     * [DirectMap4k] => 134299648
     * [DirectMap2M] => 10416553984
     * [DirectMap1G] => 6442450944
     *
     * @return int[]
     */
    public function getUnixMemoryArray()
    {
        if (!$this->isUnixOS()) {
            return [];
        }
        $callback = function ($m) {
            return $m[1] * 1024;
        };
        $data = explode("\n", file_get_contents('/proc/meminfo'));
        $meminfo = [];
        foreach ($data as $line) {
            list($key, $val) = explode(':', $line);
            if ($key) {
                $val = preg_replace_callback('/(\d+)\s*(kB)/', $callback, trim($val));
                $meminfo[$key] = (int)$val;
            }
        }
        return $meminfo;
    }

    /**
     * @return int
     */
    public function getUsedPhysicalMemory()
    {
        return (int)($this->getTotalPhysicalMemory() - $this->getAvailablePhysicalMemory());
    }

    /**
     * @return int
     */
    public function getUsedPhysicalMemoryPercent()
    {
        if (!$this->getTotalPhysicalMemory()) {
            return 0;
        }
        return round($this->getUsedPhysicalMemory() / $this->getTotalPhysicalMemory() * 100, 0);
    }

    /**
     * @return int
     */
    public function getAvailablePhysicalMemory()
    {
        $memory = 0;
        if ($this->isUnixOS()) {
            $memory = (int)$this->getUnixMemoryArray()['MemAvailable'];
        } elseif ($this->isWinOS()) {
            exec('wmic OS get FreePhysicalMemory /Value 2>&1', $output, $code);
            $successExec = (int)$code == 0;
            if ($successExec) {
                parse_str(implode('&', array_filter($output)), $data);
                $memory = $data['FreePhysicalMemory'] * 1024;
            }
        }
        return $memory;
    }

    /**
     * @return int
     */
    public function getFreePhysicalMemory()
    {
        $memory = 0;
        if ($this->isUnixOS()) {
            $memory = (int)$this->getUnixMemoryArray()['MemFree'];
        } elseif ($this->isWinOS()) {
            exec('wmic OS get FreePhysicalMemory /Value 2>&1', $output, $code);
            $successExec = (int)$code == 0;
            if ($successExec) {
                parse_str(implode('&', array_filter($output)), $data);
                $memory = $data['FreePhysicalMemory'] * 1024;
            }
        }
        return $memory;
    }

    /**
     * @return int
     */
    public function getTotalPhysicalMemory()
    {
        if ($this->isUnixOS()) {
            return (int)$this->getUnixMemoryArray()['MemTotal'];
        } elseif ($this->isWinOS()) {
            exec('wmic ComputerSystem get TotalPhysicalMemory 2>&1', $output, $code);
            $successExec = (int)$code == 0;
            return $successExec ? (int)$output[1] : null;
        }
        return null;
    }

    /**
     * @return int
     */
    public function getUsedVirtualMemory()
    {
        return (int)($this->getTotalVirtualMemory() - (int)$this->getFreeVirtualMemory());
    }

    /**
     * @return int
     */
    public function getTotalVirtualMemory()
    {
        $memory = 0;
        if ($this->isUnixOS()) {
            /** @todo */
        } elseif ($this->isWinOS()) {
            exec('wmic OS get TotalVirtualMemorySize, FreeVirtualMemory /Value 2>&1', $output, $code);
            $successExec = (int)$code == 0;
            if ($successExec) {
                parse_str(implode('&', array_filter($output)), $data);
                $memory = $data['TotalVirtualMemorySize'] * 1024;
            }
        }
        return $memory;
    }

    /**
     * @return int
     */
    public function getFreeVirtualMemory()
    {
        $memory = 0;
        if ($this->isUnixOS()) {
            /** @todo */
        } elseif ($this->isWinOS()) {
            exec('wmic OS get TotalVirtualMemorySize, FreeVirtualMemory /Value 2>&1', $output, $code);
            $successExec = (int)$code == 0;
            if ($successExec) {
                parse_str(implode('&', array_filter($output)), $data);
                $memory = $data['FreeVirtualMemory'] * 1024;
            }
        }
        return $memory;
    }

    public function getUsedVirtualMemoryPercent()
    {
        if (!$this->getTotalVirtualMemory()) {
            return 0;
        }
        return round($this->getUsedVirtualMemory() / $this->getTotalVirtualMemory() * 100, 0);
    }

    /**
     * @param null $mem
     * @return int
     */
    public function getPhpMemoryLimit($mem = null)
    {
        if (is_null($mem)) {
            $mem = ini_get('memory_limit');
        }
        if (preg_match('/^(\d+)(.)$/', $mem, $matches)) {
            if ($matches[2] == 'G') {
                $mem = $matches[1] * 1024 * 1024 * 1024;
            } elseif ($matches[2] == 'M') {
                $mem = $matches[1] * 1024 * 1024;
            } elseif ($matches[2] == 'K') {
                $mem = $matches[1] * 1024;
            }
        }
        return (int)$mem;
    }

    public function isProcessRunning($pid)
    {
        $windowsCommand = 'tasklist /FI "PID eq ' . $pid . '"';
        $linuxCommand = 'ps --pid ' . $pid;

        $command = $windowsCommand;

        if ($this->isUnixOS()) {
            $command = $linuxCommand;
        }

        $shell = new Cli();
        $r = $shell->exec($command);

        return $r->isSuccess();
    }
}