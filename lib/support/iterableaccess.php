<?php


namespace Wt\Core\Support;


use Wt\Core\Entity\TIterableAccess;

class IterableAccess implements \ArrayAccess, \IteratorAggregate, \Countable, \JsonSerializable, \Serializable
{
    use TIterableAccess;

    public function __construct($data = [])
    {
        $this->setRaw($data);
    }

    public function __isset($offset)
    {
        return $this->offsetExists($offset);
    }

    public function &__get($offset)
    {
        return $this->offsetGet($offset);
    }

    public function __unset($name)
    {
        $this->offsetUnset($name);
    }

    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    public function has($name)
    {
        return $this->offsetExists($name);
    }

    public function set($name, $value)
    {
        $this->offsetSet($name, $value);
        return $this;
    }

    public function &get($name, $default = null)
    {
        if( !$this->offsetExists($name) ){
            return $default;
        }

        return $this->offsetGet($name);
    }
}