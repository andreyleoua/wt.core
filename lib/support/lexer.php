<?php

namespace Wt\Core\Support;

use Wt\Core\Interfaces\ILexerParser;

class Lexer
{
    protected $delimiter = ':';
    protected $aliases = [];

    public function setDelimiter($strDelimiter)
    {
        $this->delimiter = $strDelimiter;
    }

    public function callNext($arParams, $currService, $lastArgs = [])
    {
        if( !$arParams || !is_array($arParams) ){
            return $currService;
        }

        $next = array_shift($arParams);

        if( $next !== "" ){
            // Передаем управление сервису реализующему разбор
            if( $currService instanceof ILexerParser ){
                $nextService = $currService->lexerCallNext($currService, $next, $lastArgs);
            }

            // Не определил следующий сервис, пробуем выполнить на текущем
            if( !$nextService && method_exists($currService, $next) ){
                if( $arParams ){
                    $nextService = call_user_func_array([$currService, $next], []);
                } else {
                    $nextService = call_user_func_array([$currService, $next], $lastArgs);
                }
            }

			if( is_null($nextService) ){
				if( property_exists($currService, $next) || isset($currService->$next) || $currService->$next ){
					$nextService = $currService->$next;
				}
			}

            if( $arParams && $nextService ){
                return $this->callNext($arParams, $nextService, $lastArgs);
            }

            return $nextService;
        }

        return $currService;
    }

    public function callNextStr($strParams, $currService, $lastArgs = [])
    {
        $strParams = $this->resolveStrAliases($strParams);

        $arStr = explode($this->delimiter, $strParams);

        return $this->callNext($arStr, $currService, $lastArgs);
    }

    protected function resolveStrAliases($strAliases)
    {
        if( isset($this->aliases[$strAliases]) ){
            $strAliases = $this->aliases[$strAliases];
        } elseif( strpos($strAliases, $this->delimiter) !== false ) {
            $arChunks = explode($this->delimiter, $strAliases);
            $arNewChunks = [];
            foreach ( $arChunks as $name ){
                $_name = $name;
                if( isset($this->aliases[$name]) ){
                    $_name = $this->aliases[$name];
                }
                $arNewChunks[] = $_name;
            }
            $strAliases = implode($this->delimiter, $arNewChunks);
        }
        return $strAliases;
    }

    public function registerAliases($realStr, $aliasStr)
    {
        $this->aliases[$aliasStr] = $realStr;
    }
}