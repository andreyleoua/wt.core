<?php


namespace Wt\Core\Support;


use Wt\Core\Interfaces\IPsr;
use Wt\Core\Interfaces\TPsr;

class ArrayToPsr implements IPsr
{
	use TPsr;

	public function __construct(array $data, $recursion = true)
	{
		$this->rawData = $data;
		$this->recursion = $recursion;
	}
}