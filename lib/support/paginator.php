<?php

namespace Wt\Core\Support;

/**
 * 
 */
class Paginator
{
    protected $itemsTotal;
    protected $itemsPerPage = 10;

    protected $pageCurrent = 1;

    protected $templateLink = '?PAGEN=%s';
    protected $totalPages = 0;

    public function __construct($totalCntItems)
    {
        $this->itemsTotal = (int)$totalCntItems;
    }

    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = (int)$itemsPerPage;
        $this->totalPages = 0;

        return $this;
    }

    public function setCurrentPage($page)
    {
        $this->pageCurrent = (int)$page;

        if( $this->pageCurrent > $this->getTotalPages() ){
            $this->pageCurrent = 1;
        }

        return $this;
    }

    public function setPageLinkTemplate($linkTemplate)
    {
        $this->templateLink = $linkTemplate;

        return $this;
    }

    public function getCurrentPage()
    {
        return $this->pageCurrent;
    }

    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    public function getTotalCountItems()
    {
        return $this->itemsTotal;
    }

    public function getTotalPages()
    {
        if( !$this->totalPages ){
            $this->totalPages = ceil($this->getTotalCountItems() / $this->getItemsPerPage());
        }
        
        return $this->totalPages;
    }

    public function getOffset()
    {
        return $this->getOffsetForPage($this->getCurrentPage());
    }

    public function getOffsetForPage($pageNum)
    {
        if( $pageNum > $this->getLast() || $pageNum < $this->getFirst() ){
            throw new \Exception('Error page num');
        }

        return ($pageNum-1) * $this->getItemsPerPage();
    }

    public function getFirst()
    {
        return 1;
    }

    public function getLast()
    {
        return $this->getTotalPages();
    }

    public function getCurrent()
    {
        return $this->getCurrentPage();
    }

    public function getNextPage()
    {
        $next = $this->getCurrentPage() + 1;
        if( $next > $this->getTotalPages() ){
            return $this->getTotalPages();
        }
        return $next;
    }

    public function getPrevPage()
    {
        $prev = $this->getCurrentPage() - 1;
        if( $prev < $this->getFirst() ){
            return $this->getFirst();
        }

        return $prev;
    }

    public function getPagenInfo()
    {
        $result = [
            'first' => $this->getFirst(),
            'last' => $this->getLast(),
            'current' => $this->getCurrentPage(),
            'next' => $this->getNextPage(),
            'prev' => $this->getPrevPage(),
            'total_items' => $this->getTotalCountItems(),
            'offset' => $this->getOffset()
        ];

        return $result;
    }

    public function getSmartPagenInfo($window = 3)
    {
        $result = $this->getPagenInfo();

        $result['show_first'] = $this->getFirst() != $this->getCurrentPage();
        $result['show_last'] = $this->getLast() != $this->getCurrentPage();
        $result['show_next'] = $this->getNextPage() != $this->getLast();
        $result['show_prev'] = $this->getPrevPage() != $this->getFirst();

        if( ($window % 2) == 0 ){
            --$window;
        }
        if( $window < 3 ){
            $window = 3;
        }
        $result['smart_window'] = $window;

        $result['items'] = [];
        $halfWindow = ($window-1)/2;
        $start = $this->getCurrentPage()-$halfWindow;
        $end = $this->getCurrentPage()+$halfWindow;

        if( $start < $this->getFirst() ){
            $end += $this->getFirst() - $start;
            $start = $this->getFirst();
        }

        if( $end > $this->getLast() ){
            $_s = $end - $this->getLast();
            $end = $this->getLast();
            if( $start-$_s > $this->getFirst() ){
                $start = $start-$_s;
            } else {
                $start = $this->getFirst();
            }
        }

        for($i=$start;$i<=$end;++$i){
            $result['items'][] = $i;
        }

        $result['smart_items'] = [];
        if( !in_array($this->getFirst(), $result['items']) ){
            $result['smart_items'][] = $this->generatePageInfo($this->getFirst());
        }
        if( !in_array($this->getFirst()+1, $result['items']) ){
            $firstSmart = $result['items'][0];
            $spages = $firstSmart - $this->getFirst();
            $spages = $this->getFirst() + floor($spages/2); // ...
            $result['smart_items'][] = $this->generatePageInfo($spages, '...');
        }
        foreach ($result['items'] as $item) {
            $result['smart_items'][] = $this->generatePageInfo($item);
        }
        if( !in_array($this->getLast()-1, $result['items']) ){
            $lastSmart = end($result['items']);
            $spages = $this->getLast() - $lastSmart;
            $spages = $this->getLast() - floor($spages/2); // ...
            $result['smart_items'][] = $this->generatePageInfo($spages, '...');
        }
        if( !in_array($this->getLast(), $result['items']) ){
            $result['smart_items'][] = $this->generatePageInfo($this->getLast());
        }

        return $result;
    }

    public function getSmartPagenInfo1($window = 3)
    {
        $result = $this->getPagenInfo();

        $result['show_first'] = $this->getFirst() != $this->getCurrentPage();
        $result['show_last'] = $this->getLast() != $this->getCurrentPage();
        $result['show_next'] = $this->getNextPage() != $this->getLast();
        $result['show_prev'] = $this->getPrevPage() != $this->getFirst();

        if( ($window % 2) == 0 ){
            --$window;
        }
        if( $window < 3 ){
            $window = 3;
        }
        $result['smart_window'] = $window;

        $result['items'] = [];
        $halfWindow = ($window-1)/2;
        $start = $this->getCurrentPage()-$halfWindow;
        $end = $this->getCurrentPage()+$halfWindow;
        
        $rangeEnd = $window+2;
        if( $rangeEnd > $this->getLast() ){
            $rangeEnd = $this->getLast();
        }
        $rangeStart = $this->getLast()-$window-1;
        if( $rangeStart < $this->getFirst() ){
            $rangeStart = $this->getFirst();
        }
        $startRange = range($this->getFirst(), $rangeEnd);
        $endRange = range($rangeStart, $this->getLast());

        if( $startRange == $endRange ){
            $start = $this->getFirst();
            $end = $this->getLast();
        }

        if( in_array($this->getCurrentPage(), $startRange) ){
            $start = min($startRange);
            $end = max($startRange);
            if( $end == $this->getCurrentPage() ){
                ++$end;
            }
        }
        if( in_array($this->getCurrentPage(), $endRange) ){
            $start = min($endRange);
            $end = max($endRange);
            if( $start == $this->getCurrentPage() ){
                --$start;
            }
        }

        if( $start < $this->getFirst() ){
            $start = $this->getFirst();
        }
        if( $end > $this->getLast() ){
            $end = $this->getLast();
        }

        for($i=$start;$i<=$end;++$i){
            $result['items'][] = $i;
        }

        $result['smart_items'] = [];
        if( !in_array($this->getFirst(), $result['items']) ){
            $result['smart_items'][] = $this->generatePageInfo($this->getFirst());
        }
        if( !in_array($this->getFirst()+1, $result['items']) ){
            $firstSmart = $result['items'][0];
            $spages = $firstSmart - $this->getFirst();
            $spages = $this->getFirst() + floor($spages/2); // ...
            $result['smart_items'][] = $this->generatePageInfo($spages, '...');
        }
        foreach ($result['items'] as $item) {
            $result['smart_items'][] = $this->generatePageInfo($item);
        }
        if( !in_array($this->getLast()-1, $result['items']) ){
            $lastSmart = end($result['items']);
            $spages = $this->getLast() - $lastSmart;
            $spages = $this->getLast() - floor($spages/2); // ...
            $result['smart_items'][] = $this->generatePageInfo($spages, '...');
        }
        if( !in_array($this->getLast(), $result['items']) ){
            $result['smart_items'][] = $this->generatePageInfo($this->getLast());
        }

        return $result;
    }

    public function calculateSmartPaginate()
    {
        $topC = true;
        $bottomC = true;
        
        if( $this->getTotalPages() && $this->getCurrentPage() > 3 && $this->getCurrentPage() <= ($this->getTotalPages()-2)){
            $center = $this->getCurrentPage()-1;
            $start = $this->getCurrentPage()-1;
            $end = $start + 2;
            $center = ceil($center / 2);
            $center2 = $end+ceil(($this->getTotalPages()-$end)/2);
        } elseif($this->getTotalPages() > 9 && $this->getCurrentPage() <= 3) {
            $start = 1;
            $end = 4;
            $center2 = $end+ceil(($this->getTotalPages()-$end)/2);
        } elseif( $this->getCurrentPage() >= ($this->getTotalPages()-2) && $this->getTotalPages() > 9 ){
            $start = $this->getTotalPages()-2;
            $center = ceil($start / 2);
            $end = $this->getTotalPages();
        } else {
            $start = 1;
            $end = $this->getTotalPages();
        }

        if( $this->getCurrentPage() == 1 || $this->getCurrentPage() < 4 ){
            $topC = false;
        }
        if( $this->getCurrentPage() == $this->getTotalPages() || $this->getCurrentPage() < 4 ){
            $bottomC = false;
        }

        $s0 = ($this->getCurrentPage() - 1) * $this->getItemsPerPage() + 1;
        $s1 = $s0 + $this->getItemsPerPage() - 1;
        if($s1 > $this->getTotalCountItems()){
            $s1 = $this->getTotalCountItems();
        }

        return [
            'show_first' => $topC,
            'first_page' => 1,
            'center_left' => $center,
            'start' => $start,
            'current' => $this->getCurrentPage(),
            'end' => $end,
            'center_right' => $center2,
            'last_page' => $this->getTotalPages(),
            'show_last' => $bottomC,
            'next' => ($this->getCurrentPage() < $this->getTotalPages())?$this->getCurrentPage()+1:'',
            'prev' => ($this->getCurrentPage() > 1)?$this->getCurrentPage()-1:'',
            'show_items' => [$s0, $s1],
            'total_items' => $this->getTotalCountItems(),
        ];
    }

    public function getLinksSmartPaginate(array $smartPaginate)
    {
        if( $smartPaginate['end'] == 1 ){
            return ['pages' => []];
        }

        $_smartPaginate['pages'] = [];
        if( $smartPaginate['show_first'] ){
            $_smartPaginate['first'] = $this->generatePageInfo($smartPaginate['first_page']);
        }
        if( $smartPaginate['show_last'] ){
            $_smartPaginate['last'] = $this->generatePageInfo($smartPaginate['last_page']);
        }

        if( $smartPaginate['show_first'] && $smartPaginate['center_left'] ){
            $_smartPaginate['pages'][] = $this->generatePageInfo($smartPaginate['first_page']);
            $_smartPaginate['pages'][] = $this->generatePageInfo($smartPaginate['center_left'], '...');
        }

        for($i=$smartPaginate['start'];$i<=$smartPaginate['end'];++$i){
            $_smartPaginate['pages'][] = $this->generatePageInfo($i);
        }

        if( $smartPaginate['show_last'] && $smartPaginate['center_right'] ){
            $_smartPaginate['pages'][] = $this->generatePageInfo($smartPaginate['center_right'], '...');
            $_smartPaginate['pages'][] = $this->generatePageInfo($smartPaginate['last_page']);
        }

        if( $smartPaginate['next'] ){
            $_smartPaginate['next'] = $this->generatePageInfo($smartPaginate['last_page'], 'Load next page');
        }
        if( $smartPaginate['prev'] ){
            $_smartPaginate['prev'] = $this->generatePageInfo($smartPaginate['last_page'], 'Load preview page');
        }

        return $_smartPaginate;
    }

    public function generatePageInfo($page = 1, $text = '')
    {
        $page = (int)$page;
        if( !$text ){
            $text = $page;
        }
        $current = true;
        $link = 'javascript:void(0);';
        if( $this->getCurrentPage() != $page ){
            $current = false;
            $link = sprintf($this->templateLink, $page);
        }

        return (object)[
            'text' => $text,
            'page' => $page,
            'link' => $link,
            'current' => $current
        ];
    }

    public function initCurrentPageFromRequest()
    {
        $page = 1;
        if(strpos($this->templateLink, '?') === false){
            preg_match("~".str_replace("%s", "(\d+)", $this->templateLink)."~", $_SERVER['REQUEST_URI'], $m);
            $_page = $m[1];
        } else {
            preg_match("/\?(.*?)=/", $this->templateLink, $m);
            $_page = $_REQUEST[$m[1]];
        }
        if( $_page > 0 ){
            $page = $_page;
        }

        $this->setCurrentPage($page);

        return $this;
    }
}