<?php


namespace Wt\Core\Support;


use Wt\Core\Interfaces\TPsr;
use Wt\Core\Interfaces\IPsr;

class CollectionToPsr extends Collection implements IPsr
{
	use TPsr;

	public function __construct($data = [], $recursion = false)
	{
		parent::__construct($data, $recursion);
		//$this->initPsr($recursion);
	}
}