<?php

namespace Wt\Core\Support;

use Bitrix\Main\Entity\DeleteResult;
use Bitrix\Main\ORM\Data\DeleteResult as ORMDeleteResult;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\ORM\Data\AddResult as ORMAddResult;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\ORM\Data\UpdateResult as ORMUpdateResult;
use Wt\Core\Collection\ErrorCollection;

class Result extends Entity
{

    /**
     * @param UpdateResult|ORMUpdateResult|AddResult|ORMAddResult|DeleteResult|ORMDeleteResult $bxResult
     * @return Result
     */
    public static function makeByBxResult($bxResult)
    {
        $result = static::make();
        if(is_callable([$bxResult, 'getPrimary'])){
            $result->setPrimary($bxResult->getPrimary());
        } elseif(is_callable([$bxResult, 'getId'])){
            $result->setId($bxResult->getId());
        }
        if(is_callable([$bxResult, 'getData'])) {
            $result->setData($bxResult->getData());
        }
        if(is_callable([$bxResult, 'getErrors'])) {
            $result->setErrors($bxResult->getErrors());
        }
        if(is_callable([$bxResult, 'getObject'])) {
            $result->setObject($bxResult->getObject());
        }

        return $result;
    }

    /**
     * @return Result
     * @throws null
     */
    public static function make()
    {
        return resolve(Result::class, func_get_args());
    }

    public function __construct($data = [])
    {
        parent::__construct($data);

        $this->set('primary', []);
        $this->set('data', new Collection());
        $this->set('errors', new ErrorCollection());
        $this->set('object', null);
    }

    public function isSuccess()
    {
        return $this->getErrors()->isEmpty();
    }

    /**
     * @return ErrorCollection|Error[]
     */
    public function getErrors()
    {
        return $this->get('errors');
    }

    /**
     * @param $errors Error[]|iterable
     * @return $this
     */
    public function setErrors($errors)
    {
        $this->getErrors()->setErrors($errors);
        return $this;
    }

    public function addError($error)
    {
        return $this->setError($error);
    }

    public function setError($error)
    {
        $this->getErrors()->add($error);
        return $this;
    }

    public function getErrorMessages()
    {
        return $this->getErrors()->getMessages();
    }

    public function setData(array $data)
    {
        $this->set('data', $this->getData()->merge($data));
        return $this;
    }

    public function addData(array $data)
    {
        $this->set('data', $this->getData()->merge($data));
        return $this;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        return $this->get('data');
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->get('object');
    }

    /**
     * @param mixed
     */
    public function setObject($object)
    {
        $this->set('object', $object);
    }

    public function setId($id)
    {
        $this->set('primary', array('ID' => $id));
    }

    /**
     * Returns id of added record
     * @return int|array
     */
    public function getId()
    {
        $primary = $this->get('primary');
        if (is_array($primary) && count($primary) == 1)
        {
            return end($primary);
        }

        return $primary;
    }

    /**
     * @param array $primary
     */
    public function setPrimary($primary)
    {
        $this->set('primary', $primary);
    }

    /**
     * @return array
     */
    public function getPrimary()
    {
        return $this->get('primary');
    }
}