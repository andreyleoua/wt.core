<?php


namespace Wt\Core\ManualFactory;


use Wt\Core\AbstractFactory\HlBlockAbstractFactory;

class HlBlockManualFactory extends AManualFactory
{
    public function __getEntityClass()
    {
        return HlBlockAbstractFactory::class;
    }

    /**
     * @param $hlBlockId
     * @return HlBlockAbstractFactory
     */
    public function getEntityById($hlBlockId)
    {
        $factoryId = $this->__getEntityClass() . '_' . $hlBlockId;
        if(!app()->container()->hasIn($factoryId)){
            if($this->config->has($hlBlockId)){
                $config = $this->config->get($hlBlockId, []);
            } else {
                $config = $this->config->get(0, []);
            }
            app()->container()->singleton($factoryId, parent::getEntityById($hlBlockId, $config));
        }
        return app()->container()->make($factoryId);
    }

    /**
     * @param $className
     * @return HlBlockAbstractFactory|null
     */
    public function getEntityByCode($className)
    {
        $hlBlockEntity = app()->factory()->main()->getHlBlockFactory()->getEntityByCode($className);
        if($hlBlockEntity) {
            $hlBlockId = $hlBlockEntity->getId();
            return $this->getEntityById($hlBlockId);
        }
        return null;
    }

    /**
     * @param $tableName
     * @return HlBlockAbstractFactory|null
     */
    public function getEntityByXmlId($tableName)
    {
        $hlBlockEntity = app()->factory()->main()->getHlBlockFactory()->getEntityByXmlId($tableName);
        if($hlBlockEntity) {
            $hlBlockId = $hlBlockEntity->getId();
            return $this->getEntityById($hlBlockId);
        }
        return null;
    }
}