<?php


namespace Wt\Core\ManualFactory;


use Wt\Core\AbstractFactory\IBlockAbstractFactory;

class IBlockManualFactory extends AManualFactory
{
    public function __getEntityClass()
    {
        return IBlockAbstractFactory::class;
    }

    /**
     * @param $iBlockId
     * @return IBlockAbstractFactory
     */
    public function getEntityById($iBlockId)
    {
        $factoryId = $this->__getEntityClass() . '_' . $iBlockId;
        if(!app()->container()->hasIn($factoryId)){
//            if($this->config->has($iBlockId)){
//                $config = $this->config->get($iBlockId, []);
//            } else
//                /**
//                 * смотрит конфиг ИБ по его коду - долго время X 2
//                 */
////                if(
////                ($iBlockEntity = app()->factory()->main()->getIBlockFactory()->getEntityById($iBlockId)) &&
////                ($iBlockCode = $iBlockEntity->getRaw()['CODE']) &&
////                $this->config->has($iBlockCode)
////            ) {
////                $config = $this->config->get($iBlockCode, []);
////            } else
//            {
//                $config = $this->config->get(0, []);
//            }
            $config = $this->config->get(0, [])->merge($this->config->get($iBlockId, []));
            $abstractFactory = parent::getEntityById($iBlockId, $config);
            if($abstractFactory) {
                app()->container()->singleton($factoryId, $abstractFactory);
            } else {
                return null;
            }
        }
        return app()->container()->make($factoryId);
    }

    /**
     * @param $code
     * @return IBlockAbstractFactory|null
     * @throws \Exception
     */
    public function getEntityByCode($code)
    {
        $iBlockEntity = app()->factory()->main()->getIBlockFactory()->getEntityByCode($code);
        if($iBlockEntity) {
            $hlBlockId = $iBlockEntity->getId();
            return $this->getEntityById($hlBlockId);
        }
        return null;
    }

    /**
     * @param $xmlId
     * @return IBlockAbstractFactory|null
     * @throws \Exception
     */
    public function getEntityByXmlId($xmlId)
    {
        $iBlockEntity = app()->factory()->main()->getIBlockFactory()->getEntityByXmlId($xmlId);
        if($iBlockEntity) {
            $hlBlockId = $iBlockEntity->getId();
            return $this->getEntityById($hlBlockId);
        }
        return null;
    }
}