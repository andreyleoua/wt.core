<?php


namespace Wt\Core\ManualFactory;


use Wt\Core\AbstractFactory\AAbstractFactory;
use Wt\Core\Interfaces\IFactory;
use Wt\Core\Support\LexerCollection;

abstract class AManualFactory implements IFactory
{
    /** @var LexerCollection */
    protected $config;

    /**
     * @param LexerCollection $config
     */
    public function __construct(LexerCollection $config)
    {
        $this->config = $config;
    }

    abstract public function __getEntityClass();

    /**
     * @param $id
     * @return AAbstractFactory|null
     * @throws \Exception
     */
    public function getEntityById($id)
    {
        if(!$id){
            return null;
        }
        $entity = resolve($this->__getEntityClass(), func_get_args());
        return $entity;
    }
}