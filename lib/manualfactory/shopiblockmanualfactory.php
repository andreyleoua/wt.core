<?php


namespace Wt\Core\ManualFactory;


use Wt\Core\AbstractFactory\ShopIBlockAbstractFactory;

class ShopIBlockManualFactory extends IBlockManualFactory
{
    public function __getEntityClass()
    {
        return ShopIBlockAbstractFactory::class;
    }

    /**
     * @param $iBlockId
     * @return ShopIBlockAbstractFactory|null
     */
    public function getEntityById($iBlockId)
    {
        return parent::getEntityById($iBlockId);
    }

    /**
     * @param $code
     * @return ShopIBlockAbstractFactory|null
     * @throws \Exception
     */
    public function getEntityByCode($code)
    {
        return parent::getEntityByCode($code);
    }

    /**
     * @param $xmlId
     * @return ShopIBlockAbstractFactory|null
     * @throws \Exception
     */
    public function getEntityByXmlId($xmlId)
    {
        return parent::getEntityByXmlId($xmlId);
    }
}