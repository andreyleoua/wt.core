<?php


namespace Wt\Core\Reference\Storage;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;

class Tools
{

    public static function convertFormFieldsToDBFields(DataManager $dataManager, $arFields, $lang = false)
    {
        global $DB;
        $result = [];
        $strTableName = $dataManager::getTableName();
        $arColumns = $DB->GetTableFields($strTableName);
        foreach($arColumns as $strColumnName => $arColumnInfo)
        {
            $type = $arColumnInfo["TYPE"];
            if(isset($arFields[$strColumnName]))
            {
                $value = $arFields[$strColumnName];

                switch ($type)
                {
                    case "int":
                        $value = intval($value);
                        break;
                    case "real":
                        $value = doubleval($value);
                        if(!is_finite($value))
                        {
                            $value = 0;
                        }
                        break;
                    case "datetime":
                    case "timestamp":
                        if(strlen($value)<=0)
                            $value = "NULL";
                        else
                            $value = \CDatabase::CharToDateFunction($value, "FULL", $lang);
                        break;
                    case "date":
                        if(strlen($value)<=0)
                            $value = "NULL";
                        else
                            $value = \CDatabase::CharToDateFunction($value, "SHORT", $lang);
                        break;
                    default:
                        $value = $DB->ForSql($value);
                }
                $result[$strColumnName] = $value;
            }
            elseif(is_set($arFields, "~".$strColumnName))
            {
                $result[$strColumnName] = "~".$strColumnName;
            }
        }

        return $result;
    }

    public static function convertValuesToFormValues(DataManager $dataManager, $data, $lang = false)
    {
        $result = [];
        $fields = $dataManager::getEntity()->getFields();

        foreach ($fields as $field){
            if(isset($data[$field->getName()])){
                $result[$field->getName()] = $field->modifyValueBeforeSave($data[$field->getName()], $data);
                if($result[$field->getName()] instanceof DateTime){
                    /** @var DateTime $value */
                    $value = $result[$field->getName()];
                    // $result[$field->getName()] = $result[$field->getName()]->format("Y-m-d H:i:s");
                    $result[$field->getName()] = ConvertTimeStamp($value->getTimestamp(), "FULL");
                }
                if($result[$field->getName()] instanceof Date){
                    /** @var Date $value */
                    $value = $result[$field->getName()];
                    $result[$field->getName()] = ConvertTimeStamp($value->getTimestamp(), "SHORT");
                }
            }
        }
        return $result;
    }

    public static function convertFormValuesToValues(DataManager $dataManager, $data, $lang = false, $option = [])
    {
        $result = $data;
        $fields = $dataManager::getEntity()->getFields();

        foreach ($fields as $field){
            if(isset($data[$field->getName()])){
                foreach ($field->getFetchDataModifiers() as $modifier)
                {
                    $result[$field->getName()] = call_user_func_array($modifier, array($result[$field->getName()], null, null, $field->getName()));
                }
                if($field->getDataType() == 'datetime'){
                    /** @var DateTime $value */
                    $result[$field->getName()] = new DateTime($result[$field->getName()], $option['DateTimeFormat']??null);
                }
                if($field->getDataType() == 'date'){
                    /** @var DateTime $value */
                    $result[$field->getName()] = new Date($result[$field->getName()], $option['DateFormat']??null);
                }
            }
        }
        return $result;
    }
}