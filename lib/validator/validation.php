<?php


namespace Wt\Core\Validator;


use Wt\Core\Validator\Rule\ARule;

class Validation extends AValidation
{
    /**
     * @var ARule $rule
     */
    public $rule;

    public function __construct($rule, $errorMsg = '', $code = 0)
    {
        parent::__construct($rule, $errorMsg, $code);
        $this->setRule($rule);
    }

    public function setRule($rule)
    {
        $this->rule = $rule;
    }

    public function validate($value)
    {
        $this->errors->clear();
        $valid = $this->rule->validate($value);
        if(!$valid) {
            $this->addError($this->errorMsg, $this->code, ['value' => $value]);
        }
        return $valid;
    }
}