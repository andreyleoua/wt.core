<?php


namespace Wt\Core\Validator;


class ValidationLogicOr extends AValidation
{
    /**
     * @var $rule Validation
     */
    public $validations = [];

    public function __construct($validations, $errorMsg = '', $code = 0)
    {
        parent::__construct($validations, $errorMsg, $code);
        $this->setValidations($validations);
    }

    public function setValidations($validations)
    {
        $this->validations = $validations;
    }

    public function validate($value)
    {
        $this->errors->clear();

        $result = false;
        $count = 0;
        /**
         * @var $validation Validation
         */
        foreach ($this->validations as $validation) {
            ++$count;
            $errors = $validation->getErrors($value);
            $valid = $errors->isEmpty();
            $result = $result || $valid;
            if($result) {
                break;
            }
        }
        $result = $result || $count === 0;
        if(!$result) {
            $this->addError($this->errorMsg, $this->code, ['value' => $value]);
        }
        return $result;
    }
}