<?php


namespace Wt\Core\Validator;


use Wt\Core\Collection\ErrorCollection;
use Wt\Core\Validator\Rule\IRule;

abstract class ARestrictions implements IValidation, IRule
{

    public $validations = [];

    /**
     * массив значений для проведения валидации
     * @return array
     */
    abstract public function getValidationValue();

    /**
     * @param $action
     * @return ValidationForm
     */
    public function getValidation($action)
    {
        if(isset($this->validations[$action])) {
            return $this->validations[$action];
        }
        return new ValidationForm();
    }
    
    /**
     * Массив вида
     *      name => IValidation
     * @param $validations array
     */
    public function setValidations($validations)
    {
        $this->validations = (array)$validations;
    }
    
    /**
     * @param $action
     * @param $validation ValidationForm|AValidation
     */
    public function setValidation($action, $validation)
    {
        $this->validations[$action] = $validation;
    }

    /**
     * @param $action
     * @return bool
     */
    public function validate($action)
    {
        return $this->getErrors($action)->isEmpty();
    }

    /**
     * @param $action
     * @return ErrorCollection
     */
    public function getErrors($action)
    {
        return $this->getValidation($action)->getErrors($this->getValidationValue());
    }

}