<?php


namespace Wt\Core\Validator;


interface IValidation
{
    public function validate($value);
    public function getErrors($value);
}