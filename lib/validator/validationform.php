<?php

namespace Wt\Core\Validator;

class ValidationForm extends AValidation
{
    /**
     * @var AValidation[]
     */
    public $validations = [];

    public function __construct($validations = [], $errorMsg = '', $code = 0)
    {
        parent::__construct($validations, $errorMsg, $code);
        $this->setValidations($validations);
    }

    /**
     * Массив вида
     *      name => IValidation
     * @param AValidation[] $validations
     */
    public function setValidations($validations)
    {
        $this->validations = (array)$validations;
    }

    /**
     * @param $name
     * @param IValidation $validation
     */
    public function setValidation($name, $validation)
    {
        $this->validations[$name] = $validation;
    }

    public function validate($value)
    {
        $this->errors->clear();

        $result = true;
        /**
         * @var Validation $validation
         */
        foreach ($this->validations as $name => $validation) {
            $errors = $validation->getErrors($value[$name]);
            $valid = $errors->isEmpty();
            $result = $result && $valid;
            if(!$this->code) {
                foreach ($errors as $key => $error) {
                    $this->errors->set($name, $error);
                    break;
                }
            } else {
                if(!$result) {
                    $this->addError($this->errorMsg, $this->code, ['value' => $value]);
                    break;
                }
            }
        }
        return $result;
    }
}