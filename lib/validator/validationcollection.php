<?php

namespace Wt\Core\Validator;

class ValidationCollection extends AValidation
{
    /**
     * @var AValidation[]
     */
    public $validations = [];

    /**
     * @param AValidation[] $validations
     * @param $errorMsg
     * @param $code
     */
    public function __construct($validations, $errorMsg = '', $code = 0)
    {
        parent::__construct($validations, $errorMsg, $code);
        $this->setValidations($validations);
    }

    public function setValidations($validations)
    {
        $this->validations = $validations;
    }

    public function validate($value = null)
    {
        $this->errors->clear();

        $result = true;
        foreach ($this->validations as $validation) {
            $errors = $validation->getErrors($value);
            $valid = $errors->isEmpty();
            $result = $result && $valid;
            if(!$this->code) {
                foreach ($errors as $key => $error) {
                    $this->errors->push($error);
                }
            } else {
                if(!$result) {
                    $this->addError($this->errorMsg, $this->code, ['value' => $value]);
                    break;
                }
            }
        }

        return $result;
    }
}