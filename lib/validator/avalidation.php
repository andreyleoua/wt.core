<?php


namespace Wt\Core\Validator;


use Wt\Core\Support\Error;
use Wt\Core\Collection\ErrorCollection;
use Wt\Core\Validator\Rule\IRule;

IncludeModuleLangFile(__FILE__);

abstract class AValidation implements IValidation, IRule
{
    public $code = 0;
    public $errorMsg = '';
    /**
     * @var ErrorCollection $errors
     */
    public $errors;

    public function __construct($rule, $errorMsg = '', $code = 0)
    {
        $this->code = $code;
        $this->errors = new ErrorCollection();
        $this->errorMsg = (string)$errorMsg;
        if($this->errorMsg == '') {
            $this->errorMsg = GetMessage('VALIDATION_ERROR_MESSAGE_DEFAULT');
        }
    }

    abstract public function validate($value);

    /**
     * @param $value
     * @return ErrorCollection
     */
    public function getErrors($value = null)
    {
        $this->validate($value);
        return $this->errors;
    }

    public function addError($errorMsg, $code = 0, $customData = null)
    {
        $errorMsg = (string)$errorMsg;
        $this->errors->setError(new Error($errorMsg, $code, $customData));
    }
}