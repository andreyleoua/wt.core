<?php


namespace Wt\Core\Validator\Rule;


class Callback extends ARule
{
    /**
     * @var $callback callable
     */
    private $callback;

    /**
     * @param callable $callback
     */
    public function __construct($callback)
    {
        $this->callback = $callback;
    }

    public function validate($value)
    {
        return (bool)call_user_func_array($this->callback, func_get_args());
    }
}