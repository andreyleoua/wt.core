<?php


namespace Wt\Core\Validator\Rule;


class InArray extends ARule
{
    private $array;
    private $strict;

    public function __construct($array, $strict = false)
    {
        $this->array = (array)$array;
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        return in_array($value, $this->array, $this->strict);
    }
}