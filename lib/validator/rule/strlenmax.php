<?php


namespace Wt\Core\Validator\Rule;


class StrLenMax extends ARule
{
    private $max = 0;
    private $strict = false;

    public function __construct($max = 0, $strict = false)
    {
        $this->max = (float)$max;
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        if($this->strict) {
            return strlen((string)$value) < $this->max;
        }
        return strlen((string)$value) <= $this->max;
    }
}