<?php


namespace Wt\Core\Validator\Rule;


class StrLenFromTo extends ARule
{
    private $min = 0;
    private $max = 0;
    private $strict = false;

    public function __construct($min = 0, $max = 0, $strict = false)
    {
        $this->min = (int)$min;
        $this->max = (int)$max;
        if($this->min > $this->max) {
            throw new \Exception(__CLASS__ . ': the maximum value must be greater than the minimum');
        }
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        $count = \mb_strlen((string)$value);

        if($this->strict) {
            return $this->min < $count && $count < $this->max;
        }
        return $this->min <= $count && $count <= $this->max;
    }
}