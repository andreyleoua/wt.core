<?php


namespace Wt\Core\Validator\Rule;


class NotEmpty extends ARule
{
    public function __construct()
    {
    }

    public function validate($value)
    {
        return !empty($value);
    }
}