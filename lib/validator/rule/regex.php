<?php


namespace Wt\Core\Validator\Rule;


class Regex extends ARule
{
    private $pattern;
    private $matches = [];

    public function __construct($pattern)
    {
        $this->pattern = (string)$pattern;
    }

    public function validate($value)
    {
        return 1 === preg_match($this->pattern, $value, $this->matches);
    }
}