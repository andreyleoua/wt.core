<?php


namespace Wt\Core\Validator\Rule;


interface IRule
{
    public function validate($value);
}