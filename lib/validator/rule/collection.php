<?php


namespace Wt\Core\Validator\Rule;


class Collection extends ARule
{
    public $collection = [];

    public function __construct($collection = [])
    {
        $collection = (array)$collection;
        foreach ($collection as $key => $rule) {
            if(is_a($rule, IRule::class)) {
                $this->collection[$key] = $rule;
            }
        }
    }

    public function validate($value)
    {
        $result = true;
        foreach ($this->collection as $key => $rule) {
            /**
             * @var $rule ARule
             */
            $result = $result && $rule->validate($value);
        }

        return $result;
    }
}