<?php


namespace Wt\Core\Validator\Rule;


class Count extends ARule
{
    private $count = 0;

    public function __construct($count = 0)
    {
        $this->count = (int)$count;
    }

    public function validate($value)
    {
        if (null === $value) {
            return false;
        }
        if (!\is_array($value) && !$value instanceof \Countable) {
            throw new \Exception('Count: value must be type array');
        }

        $count = \count($value);

        return $count == $this->count;
    }
}