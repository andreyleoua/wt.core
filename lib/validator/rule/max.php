<?php


namespace Wt\Core\Validator\Rule;


class Max extends ARule
{
    private $max = 0;
    private $strict = false;

    public function __construct($max = 0, $strict = false)
    {
        $this->max = (float)$max;
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        if($this->strict) {
            return (float)$value < $this->max;
        }
        return (float)$value <= $this->max;
    }
}