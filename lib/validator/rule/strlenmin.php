<?php


namespace Wt\Core\Validator\Rule;


class StrLenMin extends ARule
{
    private $min = 0;
    private $strict = false;

    public function __construct($min = 0, $strict = false)
    {
        $this->min = (float)$min;
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        if($this->strict) {
            return strlen((string)$value) > $this->min;
        }
        return strlen((string)$value) >= $this->min;
    }
}