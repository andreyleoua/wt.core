<?php


namespace Wt\Core\Validator\Rule;


class Required extends ARule
{
    public function __construct()
    {
    }

    public function validate($value)
    {
        if(is_null($value)) {
            return false;
        }
        if(is_array($value)) {
            return (bool)$value;
        }
        if(is_string($value)) {
            return isset($value[0]);
        }
        return true;
    }
}