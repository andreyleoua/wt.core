<?php


namespace Wt\Core\Validator\Rule;


class LogicOr extends ARule
{
    public $collection = [];

    public function __construct($collection = [])
    {
        $collection = (array)$collection;
        foreach ($collection as $key => $rule) {
            if(is_a($rule, IRule::class)) {
                $this->collection[$key] = $rule;
            }
        }
    }

    public function validate($value)
    {
        $result = false;
        $count = 0;
        foreach ($this->collection as $key => $rule) {
            ++$count;
            /**
             * @var $rule ARule
             */
            $result = $result || $rule->validate($value);
            if($result) {
                break;
            }
        }

        return $result || $count === 0;
    }
}