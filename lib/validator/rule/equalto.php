<?php


namespace Wt\Core\Validator\Rule;


class EqualTo extends ARule
{
    private $value;
    private $strict = false;

    public function __construct($value, $strict = false)
    {
        $this->value = $value;
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        if($this->strict) {
            return $value === $this->value;
        }
        return $value == $this->value;
    }
}