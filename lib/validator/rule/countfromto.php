<?php


namespace Wt\Core\Validator\Rule;


class CountFromTo extends ARule
{
    private $min = 0;
    private $max = 0;
    private $strict = false;

    public function __construct($min = 0, $max = 0, $strict = false)
    {
        $this->min = (int)$min;
        $this->max = (int)$max;
        if($this->min > $this->max) {
            throw new \Exception('CountFromTo: the maximum value must be greater than the minimum');
        }
        $this->strict = (bool)$strict;
    }

    public function validate($value)
    {
        if (null === $value) {
            return false;
        }
        if (!\is_array($value) && !$value instanceof \Countable) {
            return false;
        }

        $count = \count($value);

        if($this->strict) {
            return $this->min < $count && $count < $this->max;
        }
        return $this->min <= $count && $count <= $this->max;
    }
}