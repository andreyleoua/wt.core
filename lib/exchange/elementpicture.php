<?php


class ElementPictureExchanger
{
    /**
     * Download picture to local tmp file
     *
     * @param $url
     * @return string
     */
    public function downloadPicture($url)
    {
        $pathInfo = pathinfo($url);
        $tmpFileName = $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/'.$pathInfo['filename'].'.'.$pathInfo['extension'];

        $ch = curl_init($url);
        $fp = fopen($tmpFileName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return $tmpFileName;
    }

    /**
     * Delete local tmp files after download and set to element
     *
     * @param $arTmpFileImages
     */
    public function unlinkTmpPictures($arTmpFileImages)
    {
        if( is_array($arTmpFileImages) ){
            array_map('unlink', $arTmpFileImages);
        }
    }

    /**
     * Добавить изображения к элементу
     *
     * @param array $arPictures - url to picture
     * @param $elementId
     * @param bool $prevDelete
     */
    public function appendImagesToElement(array $arPictures, $elementId, $prevDelete = false)
    {
        if( !$arPictures || (int)$elementId < 1 ) {
            return;
        }

        if( $prevDelete ){
            \CIBlockElement::SetPropertyValuesEx($elementId, \Wt\Core\Defines::CATALOG_IBLOCK_ID, array("IMAGES" => array("VALUE" => array("del" => "Y"))));
        }

        $arFiles = [];
        $arTmpImages = [];
        foreach ($arPictures as $picture){
            $localTmpImage = $this->downloadPicture($picture);
            $arTmpImages[] = $localTmpImage;
            $arFileInfo = \CFile::MakeFileArray($localTmpImage);
            $arFileInfo["MODULE_ID"] = "iblock";
            $arFiles[] = array(
                "VALUE" => $arFileInfo,
                //"DESCRIPTION"=>"",
            );
        }

        \CIBlockElement::SetPropertyValueCode($elementId, "IMAGES", $arFiles);
        $this->unlinkTmpPictures($arTmpImages);
    }
}