<?php


namespace Wt\Core\Exchange;


class EasyCurl
{
    protected $url;
    protected $method = 'get';

    protected $ch;

    protected $params = [];
    protected $headers = [];
    protected $requestType = 'blob';

    /**
     * EasyCurl constructor.
     * @param $url
     * @param array $params
     */
    public function __construct($url, $params = [])
    {
        $this->url = $url;

        $this->params = $params;

        $this->ch = $this->_getCurlDescriptor();
    }

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    public function setMethod($method)
    {
        $method = strtolower($method);
        if( in_array($method, ['get', 'post', 'put']) ){
            $this->method = $method;
        }

        return $this;
    }

    public function setRequestType($type)
    {
        if( !in_array($type, ['blob', 'json', 'text']) ){
            return $this;
        }

        if( $type == 'text' ){
            $type = 'blob';
        }

        $this->requestType = $type;

        return $this;
    }

    /**
     * @return resource
     */
    public function getResource()
    {
        return $this->ch;
    }

    /**
     * @return resource
     */
    public function getCurlDescriptor()
    {
        return $this->getResource();
    }

    /**
     * @return resource
     */
    public function getCurlResource()
    {
        return $this->getResource();
    }

    /**
     * @return resource
     */
    protected function _getCurlDescriptor()
    {
        $ch = \curl_init();

        \curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        \curl_setopt($ch, CURLOPT_HEADER, 0);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        \curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);

        return $ch;
    }

    public function getData(array $params = [])
    {
        $this->setParams($params);
        $this->setRequestType('blob');

        return $this->get();
    }

    public function getJson(array $params = [])
    {
        $this->setParams($params);
        $this->setRequestType('json');

        return $this->get();
    }

    public function get()
    {
        $this->makeRequest();

        $data = \curl_exec($this->ch);

        \curl_close($this->ch);

        $res = null;
        if( $this->requestType == 'json' ){
            $res = json_decode($data, JSON_OBJECT_AS_ARRAY);
        }
        if( $res === null ){
            $res = $data;
        }

        return $res;
    }

    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    public function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
    }

    public function addHeaders(array $headers)
    {
        $this->headers = array_merge($this->headers, $headers);
    }

    protected function makeRequest()
    {
        $requestUrl = $this->url;
        if( $this->params && $this->method == 'get' ){
            $requestUrl = $this->url . '?' . http_build_query($this->params);
        }

        if( $this->method == 'post' || $this->method == 'put' ){
            $data = $this->params;
            if( $data ){
                if( $this->requestType == 'blob' ){
                    $data = http_build_query($data);
                } elseif( $this->requestType == 'json' ) {
                    $data = json_encode($data);
                }
            }
            \curl_setopt($this->ch, CURLOPT_POST, true);
            \curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        }

        \curl_setopt($this->ch, CURLOPT_URL, $requestUrl);

        if( $this->headers ){
            \curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        }
    }
}