<?php


namespace Wt\Core\Exchange;


class CsvReader
{
    protected $filePath = '';

    protected $fileExists = null;

    protected $headers = [];

    protected $data = [];

    protected $delimiter = ";";
    protected $trimChars = "\" \r\t\n";

    /**
     * @var int
     */
    protected $headersCnt = 0;

    protected $errors = [];

    protected $firstLineAsHeader = false;

    /**
     * @var int
     */
    protected $limit = 0;

    /**
     * @var int
     */
    protected $lastLine = 0;

    private $readedLineNum;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        if( $this->filePath[0] != "/" ){
            $this->filePath = $_SERVER['DOCUMENT_ROOT'].'/'.$this->filePath;
        }

        $this->data = collect();
        $this->errors = collect();
    }

    public function setLimit($cntLines)
    {
        $this->limit = (int)$cntLines;
        if( $this->limit < 1 ){
            $this->limit = 0;
        }
    }

    public function lastLine($lastLineNum)
    {
        $this->lastLine = (int)$lastLineNum;
        if( $this->lastLine < 1 ){
            $this->lastLine = 0;
        }
    }

    public function setDelimiter($char)
    {
        $this->delimiter = $char;
    }

    public function setFirstLineAsHeaders()
    {
        $this->checkExistsFile();

        $f = fopen($this->filePath, 'r');
        $line = fgets($f);
        fclose($f);

        $this->firstLineAsHeader = true;

        $this->setHeaders($this->parseDataFromLine($line));
    }

    public function setHeaders(array $arHeaders)
    {
        $this->headers = $arHeaders;
        $this->headersCnt = count($arHeaders);
    }

    /**
     * @return array|\Wt\Core\Support\Collection
     */
    public function read()
    {
        if( !$this->headersCnt ){
            $this->setFirstLineAsHeaders();
        }

        $this->fileRead();

        return $this->getData();
    }

    /**
     * @return array|\Wt\Core\Support\Collection
     */
    public function getData()
    {
        return $this->data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getLastLineNum()
    {
        return $this->readedLineNum;
    }

    public function getLastLine()
    {
        return $this->getLastLineNum();
    }

    protected function checkExistsFile()
    {
        if( $this->fileExists === null ){
            $this->fileExists = is_readable($this->filePath);
            if( !$this->fileExists ){
                throw new \Exception('File not found: '.$this->filePath);
            }
        }

        return $this->fileExists;
    }

    protected function parseDataFromLine($line)
    {
        $arData = explode($this->delimiter, $line);

        return array_map([$this, 'trimData'], $arData);
    }

    protected function trimData($str)
    {
        return trim($str, $this->trimChars);
    }

    protected function fileRead()
    {
        $f = fopen($this->filePath, 'r');

        // skip first line!
        if( $this->firstLineAsHeader ){
            fgets($f);
        }

        $lineNum = 0;
        $this->readedLineNum = 0;
        while (($line = fgets($f)) !== false) {
            if( ++$lineNum <= $this->lastLine ){
                continue;
            }
            $data = $this->parseDataFromLine($line);

            if( $this->headersCnt && $this->headersCnt == count($data) ){
                $data = array_combine($this->headers, $data);
                $this->data->push($data);
            } elseif( $this->headersCnt && $this->headersCnt != count($data) ) {
                $this->errors->push($data);
            } else {
                $this->data->push($data);
            }

            if( ++$this->readedLineNum == $this->limit ){
                $this->readedLineNum = $lineNum;
                break;
            }
        }

        fclose($f);
    }
}