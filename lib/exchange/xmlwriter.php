<?php


namespace Wt\Core\Exchange;


use Wt\Core\DataBuilders\YmlConstructor;
use Wt\Core\Support\Collection;

class XmlWriter
{
    protected $rawData = [];
    protected $filePath;

    protected $strData = '';


    public function __construct(iterable $data)
    {
        $this->rawData = $data;

        $this->setFileName($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.'_tmp.csv');
    }

    public function setFileName($filePathName)
    {
        $this->filePath = $filePathName;
        if( $this->filePath[0] != '/' ){
            $this->filePath = $_SERVER['DOCUMENT_ROOT'].'/'.$this->filePath;
        }
    }

    public function getFileName()
    {
        return $this->filePath;
    }

    public function save($filePath = '')
    {
        if( $filePath ){
            $this->setFileName($filePath);
        }

        file_put_contents($filePath, $this->getStrData());
    }

    public function getStrData()
    {
        if( !$this->strData ){
            $this->strData = $this->makeStrData();
        }

        return $this->strData;
    }

    public function getData()
    {
        return $this->rawData;
    }

    protected function makeStrData()
    {
        $str = '';

        if( isset($this->rawData[0]) ){
            $xmlItems = resolve(YmlConstructor::class, ['items']);
            $xmlItems->isRoot(true);
            foreach ($this->rawData as $item) {
                $xmlItems->add($this->getItem($item));
            }
        } else {
            $xmlItems = $this->getItem($this->rawData);
            $xmlItems->isRoot(true);
        }

        return $xmlItems->prettyPrint();
    }

    protected function getItem($item)
    {
        $xmlItem = resolve(YmlConstructor::class, ['item']);
        foreach ($item as $k => $v){
            $xmlItem->add(resolve(YmlConstructor::class, [htmlspecialchars($k), htmlspecialchars($v)]));
        }

        return $xmlItem;
    }
}