<?php


namespace Wt\Core\Exchange;


use Wt\Core\Support\Collection;

class CsvWriter
{
    protected $rawData = [];
    protected $filePath;

    protected $strData = '';

    protected $headers = [];
    protected $delimiter = ';';

    public function __construct(iterable $data)
    {
        $this->rawData = $data;

        $this->setFileName($_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.'_tmp.csv');
    }

    public function setFileName($filePathName)
    {
        $this->filePath = $filePathName;
        if( $this->filePath[0] != '/' ){
            $this->filePath = $_SERVER['DOCUMENT_ROOT'].'/'.$this->filePath;
        }
    }

    public function getFileName()
    {
        return $this->filePath;
    }

    public function setHeaders(array $arHeaders)
    {
        $this->headers = $arHeaders;
    }

    public function setDelimiter($char)
    {
        $this->delimiter = $char;
    }

    public function save($filePath = '')
    {
        if( $filePath ){
            $this->setFileName($filePath);
        }

        file_put_contents($filePath, $this->getStrData());
    }

    public function getStrData()
    {
        if( !$this->strData ){
            if( !$this->headers ){
                $item = current($this->rawData);
                if( $this->rawData instanceof Collection ){
                    $item = $this->rawData->first();
                }
                $this->setHeaders(array_keys($item));
            }
            $this->strData = $this->makeStrData();
        }
        return $this->strData;
    }

    public function getData()
    {
        return $this->rawData;
    }

    protected function makeStrData()
    {
        $str = $this->makeHeaderLine();

        foreach ($this->rawData as $item){
            $str .= $this->makeLine($item);
        }

        return $str;
    }

    protected function makeHeaderLine()
    {
        return $this->makeLine($this->headers);
    }

    protected function makeLine($arData)
    {
        $fData = array_map([$this, 'filterData'], $arData);
        $str = '"'.implode('"'.$this->delimiter.'"', $fData).'"';

        return $str.PHP_EOL;
    }

    protected function filterData($item)
    {
        return str_replace([$this->delimiter, '"'], ['', ''], $item);
    }
}