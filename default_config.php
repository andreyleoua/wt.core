<?php

use Bitrix\Main\Page\AssetLocation;
use Wt\Core\FactoryProxy\ArrayCacheFactory;
use Wt\Core\FactoryProxy\BitrixCacheFactory;

return [
    'root_menu' => [
        'label' => 'CoreLion Studio',
        'sort' => 200,
        'background_image_url' => 'data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgMzIwIDMyMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzIwIDMyMDsgZmlsbDojYzRjY2QwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQoNCgk8cGF0aCBkPSJNMjkyLjY5NywxOS45OTVIMjcuMzAzQzEyLjIyMywxOS45OTUsMCwzMi4yMTksMCw0Ny4yOTl2MTYzLjgyMmMwLDE1LjA3OSwxMi4yMjMsMjcuMzA0LDI3LjMwMywyNy4zMDRIMTMzLjc5djI0LjcNCgkJaC0yMS41NDRjLTEwLjE4NSwwLTE4LjQzOSw4LjI1NS0xOC40MzksMTguNDRjMCwxMC4xODQsOC4yNTUsMTguNDQsMTguNDM5LDE4LjQ0aDk1LjUwOGMxMC4xODYsMCwxOC40NC04LjI1NiwxOC40NC0xOC40NA0KCQljMC0xMC4xODUtOC4yNTQtMTguNDQtMTguNDQtMTguNDRIMTg2LjIxdi0yNC43aDEwNi40ODdjMTUuMDgsMCwyNy4zMDMtMTIuMjI1LDI3LjMwMy0yNy4zMDRWNDcuMjk5DQoJCUMzMjAsMzIuMjE5LDMwNy43NzcsMTkuOTk1LDI5Mi42OTcsMTkuOTk1eiBNMzIuMTgxLDE3OS45NmMwLTE0LjQsMTEuNzE1LTI2LjExNiwyNi4xMTUtMjYuMTE2DQoJCWMxNC40MDEsMCwyNi4xMTUsMTEuNzE2LDI2LjExNSwyNi4xMTZjMCwxNC40LTExLjcxMywyNi4xMTYtMjYuMTE1LDI2LjExNkM0My44OTYsMjA2LjA3NiwzMi4xODEsMTk0LjM2LDMyLjE4MSwxNzkuOTZ6DQoJCSBNMjg4LjMyOCwyMDYuNzUySDEyNy41NzljMC43MzQtMS44NzQsMC4yNS00LjA0LTEuMjkyLTUuNDA5bC0xMC40NzktOS4zMDhjMC44MTctMy44OTgsMS4yNTEtNy45MzgsMS4yNTEtMTIuMDc1DQoJCWMwLTQuMTQyLTAuNDM0LTguMTgyLTEuMjUzLTEyLjA4NWwxMC40NzgtOS4zMDVjMS43MjctMS41MzUsMi4xMzgtNC4wNzEsMC45ODMtNi4wNzJsLTEwLjcwNi0xOC41NDQNCgkJYy0xLjE1NC0yLjAwMi0zLjU1OS0yLjkxMi01Ljc1LTIuMTg0bC0xMy4zMzgsNC40MzRjLTUuOTg2LTUuMzY2LTEzLjA3Ny05LjUyMy0yMC44ODQtMTIuMDg2bC0yLjgyMS0xMy43Mw0KCQljLTAuNDY1LTIuMjYzLTIuNDU4LTMuODg3LTQuNzY5LTMuODg3SDQ3LjU4NmMtMi4zMTEsMC00LjMwMSwxLjYyNC00Ljc2OCwzLjg4N2wtMi44MiwxMy43MzMNCgkJYy0yLjg4MiwwLjk0Ny01LjY1OSwyLjEyMy04LjMyNiwzLjQ4NFY1MS42NjdoMjU2LjY1NVYyMDYuNzUyeiIvPg0KCTxwYXRoIGQ9Ik0xNDMuODczLDEyNy43NzhjLTEuMjkyLTAuNDMtMi43MDgsMC4xMDktMy4zODksMS4yODhsLTYuMzEyLDEwLjkzMWMtMC42NzksMS4xNzgtMC40MzgsMi42NzQsMC41NzksMy41NzhsNi4xNzcsNS40ODYNCgkJYy0wLjQ4MiwyLjI5Ny0wLjczNyw0LjY3Ny0wLjczNyw3LjExNmMwLDIuNDQxLDAuMjU2LDQuODIzLDAuNzM5LDcuMTIzbC02LjE3Niw1LjQ4NGMtMS4wMTgsMC45MDQtMS4yNiwyLjQtMC41OCwzLjU3OQ0KCQlsNi4zMTEsMTAuOTNjMC42ODEsMS4xNzksMi4wOTYsMS43MTcsMy4zODgsMS4yODdsNy44NjEtMi42MTNjMy41MjksMy4xNjMsNy43MDgsNS42MTIsMTIuMzEsNy4xMjRsMS42NjMsOC4wOTMNCgkJYzAuMjc1LDEuMzMyLDEuNDQ5LDIuMjg5LDIuODExLDIuMjg5aDEyLjYyYzEuMzYyLDAuMDAxLDIuNTM1LTAuOTU3LDIuODEtMi4yODlsMS42NjMtOC4wOTVjNC42MDItMS41MTIsOC43OC0zLjk2MywxMi4zMDktNy4xMjYNCgkJbDcuODU5LDIuNjEzYzEuMjkyLDAuNDI4LDIuNzA3LTAuMTA5LDMuMzg5LTEuMjg4bDYuMzExLTEwLjkzYzAuNjc5LTEuMTc5LDAuNDM4LTIuNjc0LTAuNTgtMy41NzlsLTYuMTc3LTUuNDg2DQoJCWMwLjQ4Mi0yLjI5OCwwLjczOC00LjY3OSwwLjczOC03LjExN2MwLTIuNDQxLTAuMjU1LTQuODIzLTAuNzM5LTcuMTIybDYuMTc1LTUuNDg0YzEuMDE4LTAuOTA1LDEuMjYtMi40LDAuNTc5LTMuNThsLTYuMzEtMTAuOTI5DQoJCWMtMC42ODEtMS4xODEtMi4wOTgtMS43MTctMy4zODktMS4yODdsLTcuODYxLDIuNjEzYy0zLjUyOS0zLjE2My03LjcwOS01LjYxMi0xMi4zMTEtNy4xMjNsLTEuNjYzLTguMDkzDQoJCWMtMC4yNzMtMS4zMzMtMS40NDgtMi4yOTEtMi44MDktMi4yOTFoLTEyLjYyMWMtMS4zNjIsMC0yLjUzNSwwLjk1OC0yLjgxLDIuMjkxbC0xLjY2Myw4LjA5NQ0KCQljLTQuNjAyLDEuNTExLTguNzgxLDMuOTYzLTEyLjMwOSw3LjEyNkwxNDMuODczLDEyNy43Nzh6IE0xNzQuODI3LDE0MC43ODVjOC40ODcsMCwxNS4zOTIsNi45MDUsMTUuMzkyLDE1LjM5Mw0KCQljMCw4LjQ4OC02LjkwNSwxNS4zOTQtMTUuMzkyLDE1LjM5NGMtOC40ODcsMC0xNS4zOTQtNi45MDUtMTUuMzk0LTE1LjM5NEMxNTkuNDM0LDE0Ny42OSwxNjYuMzQsMTQwLjc4NSwxNzQuODI3LDE0MC43ODV6Ii8+DQoJPHBhdGggZD0iTTEwNC42ODgsOTEuOTM4Yy0wLjMzLDEuNTc1LTAuNTA1LDMuMjA2LTAuNTA1LDQuODc4YzAsMS42NzMsMC4xNzUsMy4zMDYsMC41MDYsNC44ODJsLTQuMjMyLDMuNzU4DQoJCWMtMC42OTcsMC42MjEtMC44NjMsMS42NDUtMC4zOTYsMi40NTNsNC4zMjYsNy40OTJjMC40NjYsMC44MDgsMS40MzcsMS4xNzcsMi4zMjIsMC44ODJsNS4zODgtMS43OTINCgkJYzIuNDE4LDIuMTY4LDUuMjgzLDMuODQ4LDguNDM2LDQuODg0bDEuMTQsNS41NDZjMC4xODgsMC45MTMsMC45OTQsMS41NjksMS45MjUsMS41NjloOC42NTFjMC45MzMsMC4wMDEsMS43MzctMC42NTYsMS45MjUtMS41NjkNCgkJbDEuMTQxLTUuNTQ4YzMuMTUyLTEuMDM1LDYuMDE3LTIuNzE2LDguNDM2LTQuODg0bDUuMzg3LDEuNzkyYzAuODg1LDAuMjk0LDEuODU2LTAuMDc1LDIuMzIyLTAuODgzbDQuMzI2LTcuNDkxDQoJCWMwLjQ2NS0wLjgwOCwwLjI5OS0xLjgzMy0wLjM5Ny0yLjQ1M2wtNC4yMzMtMy43NmMwLjMzLTEuNTc0LDAuNTA1LTMuMjA2LDAuNTA1LTQuODc4YzAtMS42NzQtMC4xNzUtMy4zMDYtMC41MDYtNC44ODINCgkJbDQuMjMyLTMuNzU5YzAuNjk2LTAuNjIsMC44NjQtMS42NDUsMC4zOTgtMi40NTNsLTQuMzI2LTcuNDkxYy0wLjQ2Ny0wLjgwOS0xLjQzNy0xLjE3Ny0yLjMyMi0wLjg4M2wtNS4zODgsMS43OTENCgkJYy0yLjQxOS0yLjE2OC01LjI4My0zLjg0Ny04LjQzNy00Ljg4M2wtMS4xNC01LjU0N2MtMC4xODgtMC45MTQtMC45OTMtMS41Ny0xLjkyNS0xLjU3aC04LjY1Yy0wLjkzNCwwLTEuNzM4LDAuNjU2LTEuOTI2LDEuNTcNCgkJbC0xLjE0LDUuNTQ5Yy0zLjE1NCwxLjAzNi02LjAxOCwyLjcxNS04LjQzNSw0Ljg4M2wtNS4zODgtMS43OTJjLTAuODg1LTAuMjkzLTEuODU2LDAuMDc1LTIuMzIyLDAuODgzbC00LjMyNSw3LjQ5MQ0KCQljLTAuNDY3LDAuODA4LTAuMywxLjgzMywwLjM5NiwyLjQ1MkwxMDQuNjg4LDkxLjkzOHogTTEyNy45MjIsODYuMjY1YzUuODE2LDAsMTAuNTQ5LDQuNzMyLDEwLjU0OSwxMC41NQ0KCQljMCw1LjgxOC00LjczMiwxMC41NDktMTAuNTQ5LDEwLjU0OXMtMTAuNTUtNC43MzEtMTAuNTUtMTAuNTQ5QzExNy4zNzIsOTAuOTk4LDEyMi4xMDUsODYuMjY1LDEyNy45MjIsODYuMjY1eiIvPg0KPC9zdmc+DQo=',
    ],

    'logger' => [
        'root' => $_SERVER['DOCUMENT_ROOT'] . '/logs',
    ],
    'default_logger_folder' => 'logs',

    'php_path' => 'php',
    'ajax' => [
        'default' => Wt\Core\Ajax\ErrorHandler::class,
        'namespaces_autoresolve' => [
            'Wt\\Core\\Ajax'
        ],
        'alias' => [

        ]
    ],

    'templater' => [
        'path' => '/local/templater.dev',
        'namespace' => 'main',
    ],

    'assets' => [
        'path' => '/local/templater.dev',
        'namespace' => 'main',
    ],

    'image_resizer_map' => [100, 200, 400, 800, 1600],

    'set_admin_plugins' => [
        'kit:core',
    ],

    'plugins' => [
        'main' => [
            'path' => '/local/templates/main/../',
            'css' => [

            ],
            'js' => [

            ],
            'rel' => [

            ]
        ],
        'config' => [
            'jsString' => [
                '/config.js'.'?'.(intdiv(time(), 3600) * 3600),
            ],
            'css' => [
                '/toolkit.config.css',
            ],
            'sort' => class_exists('Bitrix\Main\Page\AssetLocation')? AssetLocation::AFTER_JS_KERNEL:100,
        ],
        'core' => [
            'jsString' => [
                '/core.plugins.config.min.js'.'?'.(intdiv(time(), 3600) * 3600),
                '/core.min.js'.'?'.(intdiv(time(), 3600) * 3600),
            ],
            'sort' => class_exists('Bitrix\Main\Page\AssetLocation')? AssetLocation::AFTER_JS_KERNEL:100,
        ],
        'flex-kit' => [
            'cssString' => [
                '/flex-kit.css'.'?'.(intdiv(time(), 3600) * 3600)
            ],
            'sort' => class_exists('Bitrix\Main\Page\AssetLocation')? AssetLocation::BEFORE_CSS:100,
        ],
        'bsCore' => [
            'js' => [
                '/bsCore.js',
            ],
            'css' => [
                '/bsCore.css',
            ],
        ],
        'collapse' => [
            'js' => [
                //'/collapse.js',
            ],
            'css' => [
                '/collapse.css',
            ],
        ],
        'site' => [
            'css' => [
                '/site.css'
            ],
        ],
        'toolkit' => [
            'js' => [
                '/toolkit.min.js'
            ],
            'css' => [
                '/toolkit.css'
            ],
        ],
        'theme' => [
            'css' => [
                '/theme.css'
            ],
        ],
        'datepicker' => [
            'js' => [
                '/js/datepicker.js',
            ],
            'css' => [
                '/css/datepicker.css',
            ],
        ],
        'jquery' => [
            'js' => [
                '/jquery.min.js',
            ],
        ],
    ],

    'templaterParams' => [
        'button' => [
            'class' => 'kit-button--primary kit-button--radius'
        ]
    ],

    /**
     * Register event handlers in system
     *
     * #module_name#:#event_name# => [#class_hanlder#, #method_handler#] or
     * #module_name#:#event_name# => [ [#class_hanlder#, #method_handler#], ... ]
     *
     * Example: "main:OnPageStart" => [ ['Wt\\Core\\Handlers\\Autoloader', 'OnPageStart'], ... ]
     **/
    'events' => [
        'main:OnPageStart' => [
            ['Wt\\Core\\Handlers\\Autoloader', 'OnPageStart']
        ],
        'main:OnProlog' => [
            ['Wt\\Core\\Handlers\\Autoloader', 'OnProlog']
        ],
        'main:OnEpilog' => [
            ['Wt\\Core\\Handlers\\Autoloader', 'OnEpilog']
        ],
        'main:OnBeforeResizeImage' => [
            ['Wt\\Core\\Handlers\\FileHandler', 'OnBeforeResizeImage'],
        ],
        'main:OnAfterResizeImage' => [
            ['Wt\\Core\\Handlers\\FileHandler', 'OnAfterResizeImage'],
        ],
        'main:OnGetFileSRC' => [
            ['Wt\\Core\\Handlers\\FileHandler', 'OnGetFileSRC'],
        ],
        'main:OnFileDelete' => [
            ['Wt\\Core\\Handlers\\FileHandler', 'OnFileDelete'],
        ],
        'main:OnAutoBackupStart' => [
            ['Wt\Core\Handlers\AutoBackupHandler', 'onAutoBackupStart'],
        ],
        'main:OnAutoBackupSuccess' => [
            ['Wt\Core\Handlers\AutoBackupHandler', 'onAutoBackupSuccess'],
        ],
        'main:OnAutoBackupError' => [
            ['Wt\Core\Handlers\AutoBackupHandler', 'onAutoBackupError'],
        ],
        'main:OnAutoBackupUnknownError' => [
            ['Wt\Core\Handlers\AutoBackupHandler', 'onAutoBackupUnknownError'],
        ],
    ],

    /**
     * Register autoload module for system
     *
     * All module names autoloading in event "main:OnPageStart"
     **/
    'autoload_modules' => [

    ],

    'cache_factory' => [
        'hit' => ArrayCacheFactory::class,
        'bx_1hour' => [
            'class' => BitrixCacheFactory::class,
            'ttl' => 3600,
        ],
        'bx_1day' => [
            'class' => BitrixCacheFactory::class,
            'ttl' => 86400,
        ],
        'bx_1week' => [
            'class' => BitrixCacheFactory::class,
            'ttl' => 86400*7,
        ],
//        'bxa_1hour' => [
//            'class' => \Wt\Core\FactoryProxy\BitrixIblockAutoCacheFactory::class,
//            'ttl' => 3600,
//        ],
    ],

    'loadCss' => [],
    'loadJs' => [],

    /**
     * Include styles and script for page or fill template pattern pages
     *
     * Example format:
    [
    'pattern' => '^/(\?|$)', // index page
    'css' => [
    38 => '/css/jquery.bxslider.css',
    '/css/flexslider.css'
    ],
    'js' => [
    12 => '/js/jquery.bxslider.min.js',
    18 => '/js/jquery.flexslider-min.js',
    ]
    ],
     **/
    'loadForPage' => [

    ],

    'singleton' => [
        'ajaxFactory' => Wt\Core\Factory\AjaxFactory::class
    ],

    'aliases' => [
    ],

    'providers' => [
    ],

    'MainAbstractFactory' => [
        'Wt\\Core\\Factory\\AgentFactory' => ['cache' => []],
        'Wt\\Core\\Factory\\CultureFactory' => ['cache' => ['hit']],
        'Wt\\Core\\Factory\\Main\\FileFactory' => ['cache' => ['hit']],
        'Wt\\Core\\Factory\\UserFactory' => ['cache' => ['hit']],
        'Wt\\Core\\Factory\\UserGroupFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\SiteFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\LanguageFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],

        'Wt\\Core\\Factory\\HlBlockFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\IBlockFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\IBlockTypeFactory' => ['cache' => ['hit', 'bx_1hour'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\UserFieldEnumFactory' => ['cache' => ['hit', 'bx_1day']],
        'Wt\\Core\\Factory\\UserFieldFactory' => ['cache' => ['hit', 'bx_1day']],
    ],
    'ShopAbstractFactory' => [
        'Wt\\Core\\Factory\\Shop\\BasketFactory' => ['cache' => []],
        'Wt\\Core\\Factory\\Shop\\CatalogFactory' => ['cache' => ['hit', 'bx_1day']],
        'Wt\\Core\\Factory\\Shop\\CatalogExportFactory' => ['cache' => []],
        'Wt\\Core\\Factory\\Shop\\CompanyFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true], //is_collection_cache
        'Wt\\Core\\Factory\\Shop\\CurrencyFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\ExtraFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\PriceTypeFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\MeasureFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\StoreFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\VatFactory' => ['cache' => ['hit', 'bx_1day'], 'is_collection_query' => true],

        'Wt\\Core\\Factory\\Shop\\ProductCatalogFactory' => ['cache' => ['hit']],
        'Wt\\Core\\Factory\\Shop\\ProductPriceFactory' => ['cache' => ['hit'], 'is_collection_query' => true],
        'Wt\\Core\\Factory\\Shop\\ProductStoreFactory' => ['cache' => ['hit']],
    ],
    /**
     * key 0 - default config | iBlockId | iBlockCode
     */
    'IBlockManualFactory' => [
        0 => [
            'Wt\\Core\\Factory\\IBlock\\ElementFactory' => ['cache' => ['hit'], 'moreImagePropertyCode' => 'MORE_IMAGE'],
            'Wt\\Core\\Factory\\IBlock\\PropertyFactory' => ['cache' => ['hit', 'bx_1day']],
            'Wt\\Core\\Factory\\IBlock\\PropertyEnumFactory' => ['cache' => ['hit', 'bx_1day']],
            'Wt\\Core\\Factory\\IBlock\\SectionFactory' => ['cache' => ['hit']],
            'Wt\\Core\\Factory\\IBlock\\SectionPropertyFactory' => ['cache' => ['hit']],
            'Wt\\Core\\Factory\\IBlock\\PropertyValueFactory' => ['cache' => ['hit'], 'is_collection_query' => true],
        ],
    ],
    'HlBlockManualFactory' => [
        0 => [
            'Wt\\Core\\Factory\\HlBlock\\ElementFactory' => ['cache' => ['hit']],
        ],
    ],

    'form_field_profile_id' => 'default',
    'form_field_profile_list' => [
        'default' => [
            'Wt\Core\Form\Field\DefaultField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/input/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\TextareaField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/textarea/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\SelectField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/select/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\IBlockPropertyField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/iBlockProperty/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\UserFieldField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/UserField/edit',
                    'filter' => '',
                ],
            ],
        ],
        'material' => [
            'Wt\Core\Form\Field\DefaultField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/material/input/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\TextareaField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/material/textarea/edit',
                    'filter' => '',
                ],
            ],
            'Wt\Core\Form\Field\SelectField' => [
                'templates' => [
                    'view' => '',
                    'edit' => 'kit:form/field/material/select/edit',
                    'filter' => '',
                ],
            ],
        ],
        'bitrix' => [
        ],
    ],

    'property_adapter_profile_id' => 'bitrix',
    'property_adapter_profile_list' => [
        'default' => [],
        'bitrix' => [
            'Wt\Core\PropertyAdapter\DefaultPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\DefaultField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\EPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\FPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\GPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\LPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\NPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SDirectoryPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SGTablePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SHtmlPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SIBlockElementMetaTemplateListPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SIBlockSectionMetaTemplateListPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SIBlockTemplateListPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SPhonePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SUserIdPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SWorkSchedulePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],
            'Wt\Core\PropertyAdapter\IBlock\SDateTimePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\IBlockPropertyField',
            ],


            'Wt\Core\PropertyAdapter\UserField\BooleanPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\DatePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\DateTimePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\EnumerationPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\FilePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\GTablePropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\HlBlockPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\IBlockElementPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\IBlockSectionPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\IntegerPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\StringPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
            'Wt\Core\PropertyAdapter\UserField\UfMultipleHtmlPropertyAdapter' => [
                'form_field_class' => 'Wt\Core\Form\Field\UserFieldField',
            ],
        ],
    ],
];