<?php

use Wt\Core\App;

include __DIR__ . '/functions/helpers.php';
include __DIR__ . '/functions/dev.php';
include __DIR__ . '/functions/app.php';

if(!defined('BX_CUSTOM_TO_LOWER_FUNC')){
    define('BX_CUSTOM_TO_LOWER_FUNC', 'mb_strtolower');
}
if(!defined('BX_CUSTOM_TO_UPPER_FUNC')){
    define('BX_CUSTOM_TO_UPPER_FUNC', 'mb_strtoupper');
}

$module = __getModuleEntity(__DIR__);
App\AppLoader::load(App\App::make($module, new App\ServiceContainer()));

if(class_exists('Bitrix\Main\EventManager', false)){
    $eventManager = Bitrix\Main\EventManager::getInstance();
    $eventManager->addEventHandler(
        'main',
        'OnLocalRedirect',
        ['Wt\Core\Handlers\Autoloader', 'OnLocalRedirect']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\IBlock\FactoryProperty', 'onIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\Custom\IBlockTemplateProperty', 'OnIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\Custom\IBlockElementMetaTemplateListProperty', 'OnIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\Custom\IBlockSectionMetaTemplateListProperty', 'OnIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\Custom\PhoneProperty', 'onIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'iblock',
        'OnIBlockPropertyBuildList',
        ['Wt\Core\Property\Custom\WorkScheduleProperty', 'OnIBlockPropertyBuildList']
    );
    $eventManager->addEventHandler(
        'main',
        'OnUserTypeBuildList',
        ['Wt\Core\Property\UserField\HtmlUserField', 'getUserTypeDescription']
    );
//    $eventManager->addEventHandler(
//        'main',
//        'OnUserTypeBuildList',
//        ['Wt\Core\Property\Bitrix\UserField\Factory', 'getUserTypeDescription']
//    );
}