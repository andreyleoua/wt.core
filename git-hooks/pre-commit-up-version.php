<?php

include __DIR__ . '/../include.php';

$module = app()->module();
$oldVersion = $module->getVersion();
$module->__upVersion()->save();

echo "UP $oldVersion to {$module->getVersion()}" . PHP_EOL;