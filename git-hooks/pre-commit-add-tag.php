<?php

use Wt\Core\Support\Cli;

include __DIR__ . '/../include.php';

$version = app()->module()->getVersion();

$cli = new Cli();
$cli->cd(app()->module()->getPath());
if($cli->exec("git tag v$version}")->isSuccess()) {
    echo "set tag v$version" . PHP_EOL;
}