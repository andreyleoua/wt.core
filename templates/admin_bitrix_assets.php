<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$isAdminSection = defined('ADMIN_SECTION') && ADMIN_SECTION === true;
global $APPLICATION;
use Bitrix\Main\Page\Asset;
//require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . '/modules/iblock/prolog.php');
try {
    $iblockInclude = \Bitrix\Main\Loader::includeModule('iblock');
} catch (\Bitrix\Main\LoaderException $e) {
    $iblockInclude = false;
}
if ($iblockInclude) {
    require_once($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/iblock/admin_tools.php');
    Asset::getInstance()->addJs(BX_ROOT . '/js/iblock/iblock_edit.js');
}
require_once($_SERVER['DOCUMENT_ROOT'] . BX_ROOT. '/modules/main/interface/admin_lib.php');
Asset::getInstance()->addJs(BX_ROOT . '/js/main/core/core.js');
$extension = ['popup', 'ajax', 'ui.buttons', 'ui.forms', 'main.polyfill.core', 'color_picker'];
if(class_exists('Bitrix\Main\UI\Extension')){
    try {
        \Bitrix\Main\UI\Extension::load($extension);
    } catch (\Bitrix\Main\LoaderException $e) {
    }
} else {
    CJSCore::Init($extension);
}
if ($isAdminSection)
{
    $APPLICATION->SetAdditionalCSS(BX_ROOT . '/themes/.default/pubstyles.css');
    $APPLICATION->SetAdditionalCSS(BX_ROOT . '/themes/.default/jspopup.css');
    $APPLICATION->SetAdditionalCSS(BX_ROOT . '/themes/.default/calendar.css');
    $APPLICATION->SetAdditionalCSS(BX_ROOT . '/panel/main/admin.css');
    $APPLICATION->SetAdditionalCSS(BX_ROOT . '/panel/main/admin-public.css');
}
else
{
    Asset::getInstance()->addCss(BX_ROOT . '/themes/.default/pubstyles.css');
    Asset::getInstance()->addCss(BX_ROOT . '/themes/.default/jspopup.css');
    Asset::getInstance()->addCss(BX_ROOT . '/themes/.default/calendar.css');
    Asset::getInstance()->addCss(BX_ROOT . '/panel/main/admin.css');
    Asset::getInstance()->addCss(BX_ROOT . '/panel/main/admin-public.css');
}

global $adminPage, $adminSidePanelHelper;
if(!is_object($adminPage)){
    $adminPage = new \CAdminPage();
}
if(!is_object($adminSidePanelHelper)){
    $adminSidePanelHelper = new \CAdminSidePanelHelper();
}
/** @global CUserTypeManager $USER_FIELD_MANAGER */
global $USER_FIELD_MANAGER;
if(method_exists($USER_FIELD_MANAGER, 'showscript'))
    echo $USER_FIELD_MANAGER->ShowScript();
CAdminCalendar::ShowScript();