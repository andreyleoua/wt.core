<?php
//MAX_REGION_PHONE_PROP_TITLE
$MESS[app()->module()->getLangCode('PHONE_PROPERTY_DESCRIPTION')] = 'Bx.App: Телефон';
$MESS[app()->module()->getLangCode('PHONE_ICON_TITLE')] = 'Иконка';
$MESS[app()->module()->getLangCode('PHONE_PHONE_TITLE')] = 'Заголовок *';
$MESS[app()->module()->getLangCode('PHONE_HREF_TITLE')] = 'Ссылка';
$MESS[app()->module()->getLangCode('PHONE_DESCRIPTION_TITLE')] = 'Описание';
$MESS[app()->module()->getLangCode('PHONE_DESCRIPTION_FIELD_DISABLED_NOTE')] = 'без описания (вывод поля отключен)';
$MESS[app()->module()->getLangCode('PHONE_DRAG_TITLE')] = 'Переместить';
$MESS[app()->module()->getLangCode('PHONE_DELETE_TITLE')] = 'Удалить';