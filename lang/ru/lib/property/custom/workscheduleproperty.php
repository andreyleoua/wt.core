<?php
$MESS[app()->module()->getLangCode('WORK_SCHEDULE_PROPERTY_DESCRIPTION')] = 'Bx.App: График работы';
$MESS[app()->module()->getLangCode('WORK_SCHEDULE_PROPERTY_NOTE')] = 'Время указывается в формате: от 10:00 до 20:00<br>Обязателньо указание двух значений <br>Если время не указано, то день считается не рабочим (выходным)';