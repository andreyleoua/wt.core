<?php

$MESS ['WT_CORE_IBLOCK_SECTION_META_TEMPLATE_LIST_DESCRIPTION'] = 'Bx.App: Шаблоны meta-тегов для раздела IB';
$MESS['IBINHERITED_PROP_ENTITY_TYPE_TITLE'] = 'Тип сущности';
$MESS['IBINHERITED_PROP_ENTITY_TYPE_ELEMENT'] = 'Элемент инфоблока';
$MESS['IBINHERITED_PROP_ENTITY_TYPE_SECTION'] = 'Раздел инфоблока';
$MESS['IBINHERITED_PROP_MENU_ITEM_IPV_TITLE'] = 'Вычисленное значение шаблона';
$MESS['IBINHERITED_PROP_ERROR_EMPTY_IBLOCK'] = 'Укажите информационный блок в дополнительных параметрах свойства';