<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;

IncludeModuleLangFile(__FILE__);

class wt_core extends CModule
{
    var $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        $module = [];
        if(file_exists(__DIR__.'/../.module.json')){
            $module = json_decode(file_get_contents(__DIR__.'/../.module.json'), true);
        }

        $this->MODULE_ID = $module['id'];
        $this->MODULE_VERSION = $module['version'];
        $this->MODULE_VERSION_DATE = $module['date'];

        $langPrefix = strtoupper($module['prefix']);

        $this->PARTNER_NAME = GetMessage("{$langPrefix}_PARTNER_NAME");
        $this->PARTNER_URI  = GetMessage("{$langPrefix}_PARTNER_URI");
        $this->MODULE_NAME = GetMessage("{$langPrefix}_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("{$langPrefix}_MODULE_DESCRIPTION");
    }

    function DoInstall()
    {
        $this->InstallEvents();
        $this->InstallRouters();
        $this->InstallFiles();
        ModuleManager::registerModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $this->UnInstallEvents();
        $this->UnInstallRouters();
        $this->UnInstallFiles();
        Option::delete($this->MODULE_ID, []);
    }

    public function InstallEvents()
    {
        if( class_exists('\Bitrix\Main\EventManager') ){
            $eventManager = EventManager::getInstance();
            $eventManager->registerEventHandler(
                'main', 'OnPageStart',
                $this->MODULE_ID, 'Wt\Core\Handlers\Autoloader', 'loadCurrentModule',
                1
            );
            $eventManager->registerEventHandler(
                'main', 'OnBuildGlobalMenu',
                $this->MODULE_ID, 'Wt\Core\Admin\EventHandler', 'onBuildGlobalMenu'
            );
            $eventManager->registerEventHandlerCompatible(
                'main', 'OnAfterUserTypeAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterAdd'
            );
            $eventManager->registerEventHandlerCompatible(
                'main', 'OnAfterUserTypeUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterUpdate'
            );
            $eventManager->registerEventHandlerCompatible(
                'main', 'OnAfterUserTypeDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterDelete'
            );
            $eventManager->registerEventHandlerCompatible(
                'iblock', 'OnAfterIBlockPropertyAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onAfterAdd'
            );
            $eventManager->registerEventHandlerCompatible(
                'iblock', 'OnAfterIBlockPropertyUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onAfterUpdate'
            );
            $eventManager->registerEventHandlerCompatible(
                'iblock', 'OnIBlockPropertyDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onDelete'
            );

            $eventManager->registerEventHandlerCompatible(
                'main', 'OnAfterGroupAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onAfterAdd'
            );
            $eventManager->registerEventHandlerCompatible(
                'main', 'OnAfterGroupUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onAfterUpdate'
            );
            $eventManager->registerEventHandlerCompatible(
                'main', 'OnGroupDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onDelete'
            );

        }
        return true;
    }

    public function UnInstallEvents()
    {
        if( class_exists('\Bitrix\Main\EventManager') ){
            $eventManager = \Bitrix\Main\EventManager::getInstance();
            /** old */
            $eventManager->unRegisterEventHandler(
                'iblock', 'OnIBlockPropertyBuildList',
                $this->MODULE_ID, 'Wt\Core\Property\Custom\IBInheritedCustomProperty', 'OnIBlockPropertyBuildList'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnPageStart',
                $this->MODULE_ID, '\Wt\Core\Handlers\Autoloader', 'loadCurrentModule'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnBuildGlobalMenu',
                $this->MODULE_ID, '\Wt\Core\Admin\EventHandler', 'onBuildGlobalMenu'
            );
            /** actual */
            $eventManager->unRegisterEventHandler(
                'main', 'OnPageStart',
                $this->MODULE_ID, 'Wt\Core\Handlers\Autoloader', 'loadCurrentModule'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnBuildGlobalMenu',
                $this->MODULE_ID, 'Wt\Core\Admin\EventHandler', 'onBuildGlobalMenu'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnAfterUserTypeAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterAdd'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnAfterUserTypeUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterUpdate'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnAfterUserTypeDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\UserFieldHandler', 'onAfterDelete'
            );
            $eventManager->unRegisterEventHandler(
                'iblock', 'OnAfterIBlockPropertyAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onAfterAdd'
            );
            $eventManager->unRegisterEventHandler(
                'iblock', 'OnAfterIBlockPropertyUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onAfterUpdate'
            );
            $eventManager->unRegisterEventHandler(
                'iblock', 'OnIBlockPropertyDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\IBlock\PropertyHandler', 'onDelete'
            );

            $eventManager->unRegisterEventHandler(
                'main', 'OnAfterGroupAdd',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onAfterAdd'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnAfterGroupUpdate',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onAfterUpdate'
            );
            $eventManager->unRegisterEventHandler(
                'main', 'OnGroupDelete',
                $this->MODULE_ID, 'Wt\Core\Handlers\Main\GroupHandler', 'onDelete'
            );
        }

        return true;
    }

    function InstallRouters()
    {
        /** \CUrlRewriter */
        \Bitrix\Main\UrlRewriter::add(SITE_ID, [
            'CONDITION' => '#^/ajax/([^?]+)\\??(.*)#',
            'RULE' => 'ajaxRoutePath=$1',
            'ID' => 'wt_core_ajax_router',
            'PATH' => '/ajax/index.php',
            'SORT' => 10,
        ]);
    }

    function UnInstallRouters()
    {
        /** \CUrlRewriter */
        \Bitrix\Main\UrlRewriter::delete(SITE_ID, [
            'PATH' => '/ajax/index.php',
        ]);
        /** \CUrlRewriter */
        \Bitrix\Main\UrlRewriter::delete(SITE_ID, [
            'ID' => 'wt_core_ajax_router',
        ]);
    }

    function InstallFiles()
    {
        \CopyDirFiles(__DIR__.DIRECTORY_SEPARATOR.'public', $_SERVER['DOCUMENT_ROOT'], true, true);
        \CopyDirFiles(__DIR__.DIRECTORY_SEPARATOR.'bitrix', $_SERVER['DOCUMENT_ROOT'].BX_ROOT, true, true);
    }

    function UnInstallFiles()
    {
    }
}