<?php

use Wt\Core\Factory\AjaxFactory;
use Wt\Core\Interfaces\IResponse;
use Wt\Core\Tools;

define('STOP_STATISTICS', true);
define('NO_AGENT_CHECK', true);
define('NOT_CHECK_PERMISSIONS',true);
define('BX_NO_ACCELERATOR_RESET', true);
//define('ADMIN_AJAX_MODE', true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/prolog_before.php');

/** @var AjaxFactory $ajaxFactory */
$ajaxFactory = resolve(AjaxFactory::class);
$response = $ajaxFactory->getEntity($_REQUEST['ajaxRoutePath'])->start();

if($response instanceof IResponse){
    $response->send();
}

Tools::sendTextAnswer($response);
